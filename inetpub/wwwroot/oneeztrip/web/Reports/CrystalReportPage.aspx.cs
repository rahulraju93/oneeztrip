﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace eztripStart
{
    public partial class CrystalReportPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ReportDocument crystalReport = new ReportDocument();
            crystalReport.Load(Server.MapPath("~/DailyPaymentReport.rpt"));
            DataSet1 reportfields = GetData();
            crystalReport.SetDataSource(reportfields);
            CrystalReportViewer1.ReportSource = crystalReport;
        }

        private DataSet1 GetData()
        {
            string conString = ConfigurationManager.ConnectionStrings["mycon"].ConnectionString;
            SqlCommand cmd = new SqlCommand("Proc_Report");
            using (SqlConnection con = new SqlConnection(conString))
            {
                using (SqlDataAdapter sda = new SqlDataAdapter())
                {
                    cmd.Connection = con;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Mode", 1);
                    cmd.Parameters.AddWithValue("@UserId", 2);
                    sda.SelectCommand = cmd;
                    using (DataSet1 dsReport = new DataSet1())
                    {
                        sda.Fill(dsReport, "DataTable1");
                        return dsReport;
                    }
                }
            }
        }
    }
}