﻿using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace eztripStart.Reports
{
    public partial class MonthlyReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["FromDate"]) && !string.IsNullOrEmpty(Request.QueryString["ToDate"]) && !string.IsNullOrEmpty(Request.QueryString["Status"]))
            {
                string FromDate = Request.QueryString["FromDate"];
                string ToDate = Request.QueryString["ToDate"];
                string PaymentStatus = Request.QueryString["Status"];
                ReportDocument crystalReport = new ReportDocument();
                crystalReport.Load(Server.MapPath("~/Reports/MonthlyPaymentReport.rpt"));
                DataSet1 reportfields = GetData(FromDate, ToDate, PaymentStatus);
                crystalReport.SetDataSource(reportfields);
                MonthlyReportViewer.ReportSource = crystalReport;
            }
        }

        private DataSet1 GetData(string FromDate, string ToDate, string PaymentStatus)
        {
            string conString = ConfigurationManager.ConnectionStrings["mycon"].ConnectionString;
            SqlCommand cmd = new SqlCommand("Proc_Report");
            string Status = "";
            if (PaymentStatus == "AllReports")
            {
                Status = "All";
            }
            else if (PaymentStatus == "SuccessReports")
            {
                Status = "Yes";
            }
            else if (PaymentStatus == "FailureReports")
            {
                Status = "No";
            }
            using (SqlConnection con = new SqlConnection(conString))
            {
                using (SqlDataAdapter sda = new SqlDataAdapter())
                {
                    cmd.Connection = con;
                    cmd.CommandType = CommandType.StoredProcedure;
                    
                    //DateTime FromDateTime = DateTime.Parse(FromDate);
                    //DateTime ToDateTime = DateTime.Parse(ToDate);
                    if (Status == "All")
                    {
                        cmd.Parameters.AddWithValue("@Mode", 11);
                        cmd.Parameters.AddWithValue("@StartDate", FromDate);
                        cmd.Parameters.AddWithValue("@EndDate", ToDate);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@Mode", 16);
                        cmd.Parameters.AddWithValue("@StartDate", FromDate);
                        cmd.Parameters.AddWithValue("@EndDate", ToDate);
                        cmd.Parameters.AddWithValue("@Status", Status);
                    }
                    sda.SelectCommand = cmd;
                    using (DataSet1 dsReport = new DataSet1())
                    {
                        sda.Fill(dsReport, "DataTable1");
                        return dsReport;
                    }
                }
            }
        }
    }
}