﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.CrystalReports.Engine;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Globalization;

namespace eztripStart.Reports
{
    public partial class SelectedUserReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["UserId"]) && !string.IsNullOrEmpty(Request.QueryString["FromDate"]) && !string.IsNullOrEmpty(Request.QueryString["ToDate"]) && !string.IsNullOrEmpty(Request.QueryString["PaymentStatus"]))
            {
                long UserId = Convert.ToInt64(Request.QueryString["UserId"]);
                string FromDate = Request.QueryString["FromDate"];
                string ToDate = Request.QueryString["ToDate"];
                string PaymentStatus = Request.QueryString["PaymentStatus"];
                ReportDocument crystalReport = new ReportDocument();
                crystalReport.Load(Server.MapPath("~/Reports/SelectedUserPaymentReport.rpt"));
                DataSet1 reportfields = GetData(UserId,FromDate,ToDate,PaymentStatus);
                crystalReport.SetDataSource(reportfields);
                IndividualUsersReport.ReportSource = crystalReport;
            }
        }

        private DataSet1 GetData(long UserId,string FromDate,string ToDate,string PaymentStatus)
        {
            string conString = ConfigurationManager.ConnectionStrings["mycon"].ConnectionString;
            SqlCommand cmd = new SqlCommand("Proc_Report");
            string Status = "";
            if (PaymentStatus == "AllReports")
            {
                Status = "All";
            }
            else if (PaymentStatus == "SuccessReports")
            {
                Status = "Yes";
            }
            else if (PaymentStatus == "FailureReports")
            {
                Status = "No";
            }
            using (SqlConnection con = new SqlConnection(conString))
            {
                using (SqlDataAdapter sda = new SqlDataAdapter())
                {
                    cmd.Connection = con;
                    cmd.CommandType = CommandType.StoredProcedure;
                    if (FromDate == "Nodate" && ToDate == "Nodate")
                    {
                        if (Status == "All")
                        {
                            cmd.Parameters.AddWithValue("@Mode", 7);
                            cmd.Parameters.AddWithValue("@UserId", UserId);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@Mode", 12);
                            cmd.Parameters.AddWithValue("@UserId", UserId);
                            cmd.Parameters.AddWithValue("@Status", Status);
                        }
                    }
                    else if (FromDate != "Nodate" && ToDate == "Nodate")
                    {
                        if (Status == "All")
                        {
                        cmd.Parameters.AddWithValue("@Mode", 8);
                        cmd.Parameters.AddWithValue("@UserId", UserId);
                        DateTime FromDateTime=DateTime.Parse(FromDate);
                        cmd.Parameters.AddWithValue("@StartDate",FromDateTime);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@Mode", 13);
                        cmd.Parameters.AddWithValue("@UserId", UserId);
                        DateTime FromDateTime=DateTime.Parse(FromDate);
                        cmd.Parameters.AddWithValue("@StartDate",FromDateTime);
                        cmd.Parameters.AddWithValue("@Status", Status);
                        }
                    }
                    else
                    {
                        if (Status == "All")
                        {
                            cmd.Parameters.AddWithValue("@Mode", 9);
                            cmd.Parameters.AddWithValue("@UserId", UserId);
                            DateTime FromDateTime = DateTime.Parse(FromDate);
                            DateTime ToDateTime = DateTime.Parse(ToDate);
                            cmd.Parameters.AddWithValue("@StartDate", FromDateTime);
                            cmd.Parameters.AddWithValue("@EndDate", ToDateTime);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@Mode", 14);
                            cmd.Parameters.AddWithValue("@UserId", UserId);
                            DateTime FromDateTime = DateTime.Parse(FromDate);
                            DateTime ToDateTime = DateTime.Parse(ToDate);
                            cmd.Parameters.AddWithValue("@StartDate", FromDateTime);
                            cmd.Parameters.AddWithValue("@EndDate", ToDateTime);
                            cmd.Parameters.AddWithValue("@Status", Status);
                        }
                    }
                    sda.SelectCommand = cmd;
                    using (DataSet1 dsReport = new DataSet1())
                    {
                        sda.Fill(dsReport, "DataTable1");
                        return dsReport;
                    }
                }
            }
        }
    }
}