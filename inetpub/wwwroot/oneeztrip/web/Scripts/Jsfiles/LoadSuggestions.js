﻿var CopyOfHotls = [], Hotels_forsort =[],HotelArr = [];

$('#classchangeid li').click(function () {
    var sprice = $('#hidsprice').val();
    var eprice = $('#hideprice').val();
    checknamearray = [], checkidarray = [];
    var c = 0, nam = "", inf = "";
    if (PreferenceType == "") {
        PreferenceType = $('#classchangeid li.last').attr('id');
        if (PreferenceType == "Attractions") {
            PreferenceType = "Attraction Preferences";
        }
        else if (PreferenceType == "Activities") {
            PreferenceType ="Activity Preferences";
        }
        else if (PreferenceType == "Restaurant") {
            PreferenceType = "Restaurant Preferences";
        }
        else if (PreferenceType == "Hotel") {
            PreferenceType = "Hotel Preferences";
        }
        else if (PreferenceType == "Others") {
            PreferenceType = "Custom Places";
        }
    }
    var cityname = document.getElementById('selectedcity').innerHTML;
    if (cityname == "") {
        var notify_alert = noty({
            layout: 'topRight',
            text: 'Please select a city',
            type: 'warning',
            timeout: 3000,
            maxVisible: 1,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            }
        });
        return false;
    }
    else {
        if (prevSugCity =="")
        {
            prevSugCity = $('#selectedcity').html();
        }
        var RHAAId = this.id;
        if (this.id == "Hotel") {
           
            $('#RightSidePart').block({
                message: 'Please wait while data is loading...',
                css: {
                    border: 'none',
                    padding: '15px',
                    float: 'right',
                    'margin-right': '0%',
                    backgroundColor: '#033363',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
            });
           
            c = 1;            
            $('#Spnsortbyid').show();
            $('.bg-style-heading').show();
            $('#SpnFilteredbyid').show();
            $("#NoResBlock").hide();
            $('#hotel-dropdown-img').show();
            $('.dropdown-pad').show();
            $('#cus_plc_lbl').hide();
            $(".ho tel-dropdown").show();
            $('#cus_tm_lbl').hide();
            $('#divbtnothers').hide();
            nam = "Hotel Preferences";
            $('.dropdown-pad').remove();
            $('.drob-style-h3').show();
            $('#show_hide_ressort').hide();
            $('#show_hide_attrsort').hide();
            $('#show_hide_hotelsort').show();
            $('#show_hide_actsort').hide();
            $('.drob-style-h3').next('.rating-vote').show();
            $("#rhaa1").css('display', 'block');
            $("#divplace").css('display', 'none');
            $("#divmode").css('display', 'none');
            apiStatus = 0;
            $("#rhaa1").html("");
            var itho = "<div class='dropdown-pad drob-style' id='applyScroll'><h4 style='font-size:medium; font-weight:600;'>Filter By</h4>";
            itho += "<span><h3>Hotel Class</h3><div class='rating-vote'><input type='checkbox' class='check-box ignore_click' name='hotel_class' id='hc_5' value='5' title='5-Star Hotels'/>";
            itho += "<label for='hc_5'>";
            itho += "<div class='ratings-yellow'></div><div class='ratings-yellow'></div><div class='ratings-yellow'></div>";
            itho += "<div class='ratings-yellow'></div><div class='ratings-yellow'></div></label></div>";
            itho += "<div class='rating-vote'><input type='checkbox' class='check-box ignore_click' name='hotel_class' id='hc_4' value='4' title='4-Star Hotels'/><label for='hc_4'><div class='ratings-yellow'></div>";
            itho += "<div class='ratings-yellow'></div><div class='ratings-yellow'></div><div class='ratings-yellow'></div></label></div>";
            itho += "<div class='rating-vote'><input type='checkbox' class='check-box ignore_click' name='hotel_class' id='hc_3' value='3' title='3-Star Hotels'/><label for='hc_3'><div class='ratings-yellow'></div>";
            itho += "<div class='ratings-yellow'></div><div class='ratings-yellow'></div></label></div>";
            itho += "<div class='rating-vote'><input type='checkbox' class='check-box ignore_click' name='hotel_class' id='hc_2' value='2' title='2-Star Hotels'/><label for='hc_2'><div class='ratings-yellow'></div>";
            itho += "<div class='ratings-yellow'></div></label></div>";
            itho += "<div class='rating-vote'><input type='checkbox' class='check-box ignore_click' name='hotel_class' id='hc_1' value='1' title='1-Star Hotels'/><label for='hc_1'><div class='ratings-yellow'></div></label></div></span>";
            //itho += "<span><h3>Guest Rating</h3>";
            //itho += "<div class='rating-vote'><input type='checkbox' class='check-box ignore_click'  name='Popular' id='hr_5' value='5' /><label for='hr_5'><div class='HotelPopularImg'></div>";
            //itho += "<div class='HotelPopularImg'></div><div class='HotelPopularImg'></div><div class='HotelPopularImg'></div>";
            //itho += "<div class='HotelPopularImg'></div></label></div>";
            //itho += "<div class='rating-vote'><input type='checkbox' class='check-box ignore_click' name='Popular' id='hr_4' value='4' /><label for='hr_4'><div class='HotelPopularImg'></div>";
            //itho += "<div class='HotelPopularImg'></div><div class='HotelPopularImg'></div><div class='HotelPopularImg'></div></label></div>";
            //itho += "<div class='rating-vote'><input type='checkbox' class='check-box ignore_click' name='Popular' id='hr_3' value='3' /><label for='hr_3'><div class='HotelPopularImg'></div>";
            //itho += "<div class='HotelPopularImg'></div><div class='HotelPopularImg'></div></label></div>";
            //itho += "<div class='rating-vote'><input type='checkbox' class='check-box ignore_click' name='Popular' id='hr_2' value='2' /><label for='hr_2'><div class='HotelPopularImg'></div>";
            //itho += "<div class='HotelPopularImg'></div></label></div>";
            //itho += "<div class='rating-vote'><input type='checkbox' class='check-box ignore_click' name='Popular' id='hr_1' value='1'/><label for='hr_1'><div class='HotelPopularImg'></div></label></div></span>";
            itho += "<span class='last'>";
            itho += "<h3>Amenities</h3>";
            itho += "<div class='rating-vote'><input type='checkbox' class='check-box ignore_click' name='HP' id='hp_8' value='4096' /><label for='hp_8'>Babysitting</label></div>";
            itho += "<div class='rating-vote'><input type='checkbox' class='check-box ignore_click' name='HP' id='hp_12' value='2048' /><label for='hp_12'>Breakfast</label></div>";
            itho += "<div class='rating-vote'><input type='checkbox' class='check-box ignore_click' name='HP' id='hp_10' value='1' /><label for='hp_10'>Business Center</label></div>";
            itho += "<div class='rating-vote'><input type='checkbox' class='check-box ignore_click' name='HP' id='hp_7' value='2' /><label for='hp_7'>Fitness Center</label></div>";
            itho += "<div class='rating-vote'><input type='checkbox' class='check-box ignore_click' name='HP' id='hp_1' value='134217728' /><label for='hp_1'>Free Parking</label></div>";
            itho += "<div class='rating-vote'><input type='checkbox' class='check-box ignore_click' name='HP' id='hp_5' value='8' /><label for='hp_5'>Internet</label></div>";
            itho += "<div class='rating-vote'><input type='checkbox' class='check-box ignore_click' name='HP' id='hp_4' value='32' /><label for='hp_4'>Kitchen</label></div>";
            itho += "<div class='rating-vote'><input type='checkbox' class='check-box ignore_click' name='HP' id='hp_9' value='64' /><label for='hp_9'>Pets Allowed</label></div>";
            itho += "<div class='rating-vote'><input type='checkbox' class='check-box ignore_click' name='HP' id='hp_3' value='128' /><label for='hp_3'>Pool</label></div>";
            itho += "<div class='rating-vote'><input type='checkbox' class='check-box ignore_click' name='HP' id='hp_2' value='256' /><label for='hp_2'>Restaurant</label></div>";
            itho += "<div class='rating-vote'><input type='checkbox' class='check-box ignore_click' name='HP' id='hp_6' value='32768' /><label for='hp_6'>Room Service</label></div>";
            itho += "<div class='rating-vote'><input type='checkbox' class='check-box ignore_click' name='HP' id='hp_11' value='512' /><label for='hp_11'>Spa</label></div>";
            itho += "</span>";
            itho += "<div class='price-bar' id='sliderhotels'> <label for='amount'>Budget</label><input type='text' class='amount' id='price' /> <div id='slider-range' class='slider-pad'></div> </div>";
            itho += "<div class='chn-pref'><input type='button' value='Apply' id='btnsubmit_hotel'></div>";
            itho += "</div>";
            $('#hotel-dropdown-img').after(itho);
            $("#slider-range").slider({
                range: true,
                min: 10,
                max: 3000,
                values: [sprice, eprice],
                //min: sprice,
                //max: eprice,
                //values: [sprice, eprice],
                stop: function (event, ui) {
                    $("#price").val("$" + $("#slider-range").slider("values", 0) + " - $" + $("#slider-range").slider("values", 1));
                    price1 = "$" + ui.values[0];
                    price2 = "$" + ui.values[1];
                }
            });
            $("#price").val("$" + $("#slider-range").slider("values", 0) + " - $" + $("#slider-range").slider("values", 1));
            GetHotels("listHotels");
            //var prsntFlag = 0;
            //for (var loopvarHotels = 0, CitiesHotelsLength = ActsForCities.length; loopvarHotels < CitiesHotelsLength; loopvarHotels++) {
            //    if (ActsForCities[loopvarHotels].cityName.trim() == $('#selectedcity').html().trim()) {
            //        if (ActsForCities[loopvarHotels].Acts.Manual) {
            //            prsntFlag = 1;
            //        }
            //    }
            //}
            //if (prsntFlag == 0) {
            //    GetManualEntry('4', $('#latarray').val(), $('#lngarray').val());
            //}

            //LoadFilterHotels();
            //var Budgetstart = $("#slider-range").slider("values", 0);
            //var Budgetend = $("#slider-range").slider("values", 1);
            //if ($("input[class='check-box']:checked").length > 0 ||(Budgetstart!=10 && Budgetend!=3000)) {
            //    ApplyHotelsFilter();
            //}
            //$("#btnsubmit_hotel").on('click', function () {
            //    $("#hotel-dropdown-img").trigger("click");
            //    ApplyHotelsFilter();
            //});
            //intheTripRHAA(RHAAId);
        }
        else if (this.id == "Restaurant") {            
            cuisineele = 0;
            apiStatus = 0;
            IsFilter = 0;
            $("#rhaa1").css('display', 'block');
            $("#NoResBlock").hide();
            $('.bg-style-heading').show();
            $(".ho tel-dropdown").show();
            $('.dropdown-pad').show();
            $('#show_hide_actsort').hide();
            $('#show_hide_ressort').show();
            $('#show_hide_attrsort').hide();
            $("#divplace").css('display', 'none');
            $("#divmode").css('display', 'none');
            $('#divbtnothers').hide();
            $('#rhaa1').html("");
            c = 2;
            $('#Spnsortbyid').show();
            $('#SpnFilteredbyid').show();
            $('#hotel-dropdown-img').show();
            $('#show_hide_hotelsort').hide();
            $('#cus_plc_lbl').hide();
            $('#cus_tm_lbl').hide();
            nam = "Restaurant Preferences";
            $('.dropdown-pad').remove();
            allrestaurants_toleft.length = 0;
            var itho = "<div class='dropdown-pad drob-style' id='applyScroll'><h4 style='font-size:medium; font-weight:600;'>Filter By </h4><span><input type='hidden' value='0' id='dynamicresrating'/><h3>Rating</h3>";
            itho += "<div class='rating-vote'><input type='checkbox' class='check-box' id='chkresrating5' name='RestRating' value='5'/><label for='chkresrating5'><div class='ratings-yellow'></div><div class='ratings-yellow'></div>";
            itho += "<div class='ratings-yellow'></div><div class='ratings-yellow'></div><div class='ratings-yellow'></div></label></div>";
            itho += "<div class='rating-vote'><input type='checkbox' class='check-box' id='chkresrating4' name='RestRating' value='4'/><label for='chkresrating4'><div class='ratings-yellow'></div>";
            itho += "<div class='ratings-yellow'></div><div class='ratings-yellow'></div><div class='ratings-yellow'></div></label></div>";
            itho += "<div class='rating-vote'><input type='checkbox' class='check-box' id='chkresrating3' name='RestRating' value='3'/><label for='chkresrating3'><div class='ratings-yellow'></div>";
            itho += "<div class='ratings-yellow'></div><div class='ratings-yellow'></div></label></div>";
            itho += "<div class='rating-vote'><input type='checkbox' class='check-box' id='chkresrating2' name='RestRating' value='2'/><label for='chkresrating2'><div class='ratings-yellow'></div>";
            itho += "<div class='ratings-yellow'></div></label></div>";
            itho += "<div class='rating-vote'><input type='checkbox' class='check-box' id='chkresrating1' name='RestRating' value='1'/><label for='chkresrating1'><div class='ratings-yellow'></div></label></div></span>";
            itho += "<span><h3>Price Level</h3>";
            itho += "<div class='rating-vote'><input type='checkbox' class='check-box' id='chkrestprice1' name='RestPrice' value='1' title='Free and Inexpensive'/>";
            itho += "<label for='chkrestprice1'>$</label></div>";
            itho += "<div class='rating-vote'><input type='checkbox' class='check-box' id='chkrestprice2' name='RestPrice' value='2' title='Moderate'/>";
            itho += "<label for='chkrestprice2'>$$</label></div>";
            itho += "<div class='rating-vote'><input type='checkbox' class='check-box' id='chkrestprice3' name='RestPrice' value='3' title='Exprensive'/>";
            itho += "<label for='chkrestprice3'>$$$</label></div>";
            itho += "<div class='rating-vote'><input type='checkbox' class='check-box' id='chkrestprice4' name='RestPrice' value='4' title='Very Expensive'/>";
            itho += "<label for='chkrestprice4'>$$$$</label></div></span>";
            itho += "<form><span><input type='hidden' id='dynamicrestype' value='asian'/><h3>Cuisine Type</h3>";
            try {
                if (OptRstr.length > 0)
                {
                    var totalLength = parseInt(Number(OptRstr.length) / 2);
                    $.each(OptRstr, function (indexValue, dataValue) {
                        var id = dataValue.ID;
                        var type = dataValue.Keyword;
                        itho += "<div class='rating-vote' ><input type='checkbox' id='chkres" + id + "' class='RestTypeCl' name='chkRestaurents' value='0'/><input type='hidden' value='" + type + "'/><label for='chkres" + id + "'>" + type + "</label></div>";
                        if (indexValue == totalLength) {
                            itho += "</span><span><br/><br/>"
                        }
                    });
                    itho += "</span></form>";
                    itho += "<div class='chn-pref'><input type='button' value='Apply' id='btnsubmit_rest' onclick='applyRestaurents()'></div>";
                    itho += "</div>";
                    $('#hotel-dropdown-img').after(itho);
                    $('#rhaa1').html("");
                    var restaurant = new Array();
                    $.getJSON('../Trip/Getuserpreference', function (data) {
                        $('#RightSidePart').block({
                            message: 'Please wait while data is loading...',
                            css: {
                                border: 'none',
                                padding: '15px',
                                float: 'right',
                                'margin-right': '0%',
                                backgroundColor: '#033363',
                                '-webkit-border-radius': '10px',
                                '-moz-border-radius': '10px',
                                opacity: .5,
                                color: '#fff'
                            }
                        });
                        $.each(data, function (keys, values) {
                            if (prevRestTypes == "") {
                                restaurant = values.cuisine.split(",");
                                prevRestTypes = values.cuisine;
                            }
                            else {
                                restaurant = prevRestTypes.split(",");
                            }
                            CheckArraySort = restaurant.length;
                            $("#hidcuisine").val(restaurant);
                            var varRestaurantLength = restaurant.length;
                            var chckStatus = 0;
                            for (var res_loopvar = 0; res_loopvar < varRestaurantLength; res_loopvar++) {
                                $('input[name="chkRestaurents"]').each(function () {
                                    var curRsType = $(this).next('input[type=hidden]').val();
                                    if (restaurant[res_loopvar] == curRsType) {
                                        $(this).val("1");
                                        $(this).attr("checked", true);
                                        LoadRestaurant(curRsType);
                                    }
                                });
                                if (restaurant[res_loopvar] == "Nooption") {
                                    var callRestaurantsIncr = 0;
                                    var newarrayTmp = "";
                                    restaurant.length = 0;
                                    restaurant.push("mediterranean", "european", "mexican", "american", "french", "spanish", "italian", "indian", "japanese", "chinese", "thai", "asian", "breakfast", "pizza", "dessert", "bakery");
                                    var RestaurantsOptions = restaurant.length;
                                    var tmpLengthRest = Math.ceil(RestaurantsOptions / 4);
                                    CheckArraySort = tmpLengthRest;
                                    var increCnt = 0;
                                    for (var loopvar_rest = 0; loopvar_rest < RestaurantsOptions; loopvar_rest += 4) {
                                        increCnt++;
                                        if (increCnt < tmpLengthRest || RestaurantsOptions % 4 == 0) {
                                            newarrayTmp = "(" + restaurant[loopvar_rest + 3] + " OR " + restaurant[loopvar_rest + 2] + " OR " + restaurant[loopvar_rest + 1] + " OR " + restaurant[loopvar_rest] + ")";
                                            LoadRestaurant(newarrayTmp);
                                        }
                                    }
                                }
                            }
                        });
                        getMnalRestTyp();
                        //GetManualEntry('3', $('#latarray').val(), $('#lngarray').val());
                        setTimeout(function () { intheTripRHAA(RHAAId); }, 5000);
                    }).fail(function () {
                        $('#RightSidePart').block({
                            message: 'Please wait while data is loading...',
                            css: {
                                border: 'none',
                                padding: '15px',
                                float: 'right',
                                'margin-right': '0%',
                                backgroundColor: '#033363',
                                '-webkit-border-radius': '10px',
                                '-moz-border-radius': '10px',
                                opacity: .5,
                                color: '#fff'
                            }
                        });
                        restaurant = prevRestTypes.split(",");
                        CheckArraySort = restaurant.length;
                        $("#hidcuisine").val(restaurant);
                        var varRestaurantLength = restaurant.length;
                        var chckStatus = 0;
                        for (var res_loopvar = 0; res_loopvar < varRestaurantLength; res_loopvar++) {
                            $('input[name="chkRestaurents"]').each(function () {
                                var curRsType = $(this).next('input[type=hidden]').val();
                                if (restaurant[res_loopvar] == curRsType) {
                                    $(this).val("1");
                                    $(this).attr("checked", true);
                                    LoadRestaurant(curRsType);
                                }
                            });
                            if (restaurant[res_loopvar] == "Nooption" || restaurant[res_loopvar] == "") {
                                var callRestaurantsIncr = 0;
                                var newarrayTmp = "";
                                restaurant.length = 0;
                                restaurant.push("mediterranean", "european", "mexican", "american", "french", "spanish", "italian", "indian", "japanese", "chinese", "thai", "asian", "breakfast", "pizza", "dessert", "bakery");
                                var RestaurantsOptions = restaurant.length;
                                var tmpLengthRest = Math.ceil(RestaurantsOptions / 4);
                                CheckArraySort = tmpLengthRest;
                                var increCnt = 0;
                                for (var loopvar_rest = 0; loopvar_rest < RestaurantsOptions; loopvar_rest += 4) {
                                    increCnt++;
                                    if (increCnt < tmpLengthRest || RestaurantsOptions % 4 == 0) {
                                        newarrayTmp = "(" + restaurant[loopvar_rest + 3] + " OR " + restaurant[loopvar_rest + 2] + " OR " + restaurant[loopvar_rest + 1] + " OR " + restaurant[loopvar_rest] + ")";
                                        LoadRestaurant(newarrayTmp);
                                    }
                                }                                
                            }
                        }
                        getMnalRestTyp();
                        //GetManualEntry('3', $('#latarray').val(), $('#lngarray').val());
                    });
                }
                else
                    {
                    $.getJSON('../Trip/RetreivePreferrence', { Mode: 3 }, function (data) {                        
                    var totalLength = parseInt(Number(data.length) / 2);
                    $.each(data, function (indexValue, dataValue) {
                        OptRstr.push(dataValue);
                        var id = dataValue.ID;
                        var type = dataValue.Keyword;
                        itho += "<div class='rating-vote' ><input type='checkbox' id='chkres" + id + "' class='RestTypeCl' name='chkRestaurents' value='0'/><input type='hidden' value='" + type + "'/><label for='chkres" + id + "'>" + type + "</label></div>";
                        if (indexValue == totalLength) {
                            itho += "</span><span><br/><br/>";
                        }
                    });
                    itho += "</span></form>";
                    itho += "<div class='chn-pref'><input type='button' value='Apply' id='btnsubmit_rest' onclick='applyRestaurents()'></div>";
                    itho += "</div>";
                    $('#hotel-dropdown-img').after(itho);
                    $('#rhaa1').html("");
                    var restaurant = new Array();
                        $.getJSON('../Trip/Getuserpreference', function (data) {
                            $('#RightSidePart').block({
                                message: 'Please wait while data is loading...',
                                css: {
                                    border: 'none',
                                    padding: '15px',
                                    float: 'right',
                                    'margin-right': '0%',
                                    backgroundColor: '#033363',
                                    '-webkit-border-radius': '10px',
                                    '-moz-border-radius': '10px',
                                    opacity: .5,
                                    color: '#fff'
                                }
                            });
                        $.each(data, function (keys, values) {
                            if (prevRestTypes == "") {
                                restaurant = values.cuisine.split(",");
                                prevRestTypes = values.cuisine;
                            }
                            else {
                                restaurant = prevRestTypes.split(",");
                            }
                            CheckArraySort = restaurant.length;
                            $("#hidcuisine").val(restaurant);
                            var varRestaurantLength = restaurant.length;
                            var chckStatus = 0;
                            for (var res_loopvar = 0; res_loopvar < varRestaurantLength; res_loopvar++) {
                                $('input[name="chkRestaurents"]').each(function () {
                                    var curRsType = $(this).next('input[type=hidden]').val();
                                    if (restaurant[res_loopvar] == curRsType) {
                                        $(this).val("1");
                                        $(this).attr("checked", true);
                                        LoadRestaurant(curRsType);
                                    }
                                });
                                if (restaurant[res_loopvar] == "Nooption" || restaurant[res_loopvar] == "") {
                                    var callRestaurantsIncr = 0;
                                    var newarrayTmp = "";
                                    restaurant.length = 0;
                                    restaurant.push("mediterranean", "european", "mexican", "american", "french", "spanish", "italian", "indian", "japanese", "chinese", "thai", "asian", "breakfast", "pizza", "dessert", "bakery");
                                    var RestaurantsOptions = restaurant.length;
                                    var tmpLengthRest = Math.ceil(RestaurantsOptions / 4);
                                    CheckArraySort = tmpLengthRest;
                                    var increCnt = 0;
                                    for (var loopvar_rest = 0; loopvar_rest < RestaurantsOptions; loopvar_rest+=4) {
                                        increCnt++;
                                        if (increCnt < tmpLengthRest || RestaurantsOptions % 4 == 0) {
                                            newarrayTmp = "(" + restaurant[loopvar_rest + 3] + " OR " + restaurant[loopvar_rest + 2] + " OR " + restaurant[loopvar_rest + 1] + " OR " + restaurant[loopvar_rest] + ")";
                                            LoadRestaurant(newarrayTmp);
                                        }
                                    }
                                }
                            }
                        });
                        getMnalRestTyp();
                        //GetManualEntry('3', $('#latarray').val(), $('#lngarray').val());
                        setTimeout(function () { intheTripRHAA(RHAAId); }, 5000);
                        }).fail(function () {
                            $('#RightSidePart').block({
                                message: 'Please wait while data is loading...',
                                css: {
                                    border: 'none',
                                    padding: '15px',
                                    float: 'right',
                                    'margin-right': '0%',
                                    backgroundColor: '#033363',
                                    '-webkit-border-radius': '10px',
                                    '-moz-border-radius': '10px',
                                    opacity: .5,
                                    color: '#fff'
                                }
                            });
                        restaurant = prevRestTypes.split(",");
                        CheckArraySort = restaurant.length;
                        $("#hidcuisine").val(restaurant);
                        var varRestaurantLength = restaurant.length;
                        var chckStatus = 0;
                        for (var res_loopvar = 0; res_loopvar < varRestaurantLength; res_loopvar++) {
                            $('input[name="chkRestaurents"]').each(function () {
                                var curRsType = $(this).next('input[type=hidden]').val();
                                if (restaurant[res_loopvar] == curRsType) {
                                    $(this).val("1");
                                    $(this).attr("checked", true);
                                    LoadRestaurant(curRsType);
                                }
                            });
                            if (restaurant[res_loopvar] == "Nooption" || restaurant[res_loopvar] == "") {
                                var callRestaurantsIncr = 0;
                                var newarrayTmp = "";
                                restaurant.length = 0;
                                restaurant.push("mediterranean", "european", "mexican", "american", "french", "spanish", "italian", "indian", "japanese", "chinese", "thai", "asian", "breakfast", "pizza", "dessert", "bakery");
                                var RestaurantsOptions = restaurant.length;
                                var tmpLengthRest = Math.ceil(RestaurantsOptions / 4);
                                CheckArraySort = tmpLengthRest;
                                var increCnt = 0;
                                for (var loopvar_rest = 0; loopvar_rest < RestaurantsOptions; loopvar_rest += 4) {
                                    increCnt++;
                                    if (increCnt < tmpLengthRest || RestaurantsOptions % 4 == 0) {
                                        newarrayTmp = "(" + restaurant[loopvar_rest + 3] + " OR " + restaurant[loopvar_rest + 2] + " OR " + restaurant[loopvar_rest + 1] + " OR " + restaurant[loopvar_rest] + ")";
                                        LoadRestaurant(newarrayTmp);
                                    }
                                }                                
                            }
                        }
                        getMnalRestTyp();
                        //GetManualEntry('3', $('#latarray').val(), $('#lngarray').val());
                    });
                });
            }
            } catch (err) {
            }
            $("#btnsubmit_rest").on('click', function () {
                //$("#hotel-dropdown-img").trigger("click");
                $(".dropdown-pad").hide();
            });
        }
        else if (this.id == "Attractions") {
            
            touristele = 0;
            gotattrDet = 0;
            increAttrDet = 0;
            incrDet2 = 0;
            gotDet2 = 0;
            apiStatus = 0;
            tmpincreValue = 0;
            IsFilter = 0;
            c = 3;
            $("#rhaa1").css('display', 'block');
            $("#NoResBlock").hide();
            $('.bg-style-heading').show();
            $('#show_hide_actsort').hide();
            $('.dropdown-pad').show();
            $("#divplace").css('display', 'none');
            $(".ho tel-dropdown").show();
            $("#divmode").css('display', 'none');
            $('#divbtnothers').hide();
            $('#rhaa1').html("");
            $('#Spnsortbyid').show();
            $('#SpnFilteredbyid').show();
            $('#hotel-dropdown-img').show();
            $('#cus_plc_lbl').hide();
            $('#cus_tm_lbl').hide();
            $('.price-bar').hide();
            $('#show_hide_hotelsort').hide();
            $('#show_hide_ressort').hide();
            $('#show_hide_attrsort').show();
            $('.rating-vote').hide();
            nam = "Attraction Preferences";
            $('.dropdown-pad').remove();
            allattractions_toleft.length = 0;
            var itho = "<div class='dropdown-pad drob-style' id='applyScroll'><h4 style='font-size:medium; font-weight:600;'>Filter By </h4><span id='getattrchkid'><input type='hidden' id='dynamicattrating' value='0'/><h3>Popularity rating</h3>";
            itho += "<div class='rating-vote'><input type='checkbox' class='check-box' name='AttrRating' id='chkattrating5' value='5'/><label for='chkattrating5'><div class='ratings-yellow'></div><div class='ratings-yellow'></div>";
            itho += "<div class='ratings-yellow'></div><div class='ratings-yellow'></div><div class='ratings-yellow'></div></label></div>";
            itho += "<div class='rating-vote'><input type='checkbox' class='check-box' name='AttrRating' id='chkattrating4' value='4' /><label for='chkattrating4'><div class='ratings-yellow'></div><div class='ratings-yellow'></div>";
            itho += "<div class='ratings-yellow'></div><div class='ratings-yellow'></div></label></div>";
            itho += "<div class='rating-vote'><input type='checkbox' class='check-box' name='AttrRating' id='chkattrating3' value='3'/><label for='chkattrating3'><div class='ratings-yellow'></div><div class='ratings-yellow'></div>";
            itho += "<div class='ratings-yellow'></div></label></div>";
            itho += "<div class='rating-vote'><input type='checkbox' class='check-box' name='AttrRating' id='chkattrating2' value='2' /><label for='chkattrating2'><div class='ratings-yellow'></div><div class='ratings-yellow'></div></label></div>";
            itho += "<div class='rating-vote'><input type='checkbox' class='check-box' name='AttrRating' id='chkattrating1' value='1'/><label for='chkattrating1'><div class='ratings-yellow'></div></label></div></span>";
            itho += "<span><h3>Price Level</h3>";
            itho += "<div class='rating-vote'><input type='checkbox' class='check-box' id='chkattrprice1' name='AttrPrice' value='1' title='Free and Inexpensive'/>";
            itho += "<label for='chkattrprice1'>$</label></div>";
            itho += "<div class='rating-vote'><input type='checkbox' class='check-box' id='chkattrprice2' name='AttrPrice' value='2' title='Moderate'/>";
            itho += "<label for='chkattrprice2'>$$</label></div>";
            itho += "<div class='rating-vote'><input type='checkbox' class='check-box' id='chkattrprice3' name='AttrPrice' value='3' title='Exprensive'/>";
            itho += "<label for='chkattrprice3'>$$$</label></div>";
            itho += "<div class='rating-vote'><input type='checkbox' class='check-box' id='chkattrprice4' name='AttrPrice' value='4' title='Very Expensive'/>";
            itho += "<label for='chkattrprice4'>$$$$</label></div></span>";
            itho += "<span><input type='hidden' id='dynamicatttype' value='zoo'/><h3>Categories</h3>";
            try {
                if (OptAttr.length > 0) {
                    var totalLength = parseInt(Number(OptAttr.length) / 2);
                    $.each(OptAttr, function (indexValue, dataValue) {
                        var id = dataValue.ID;
                        var type = dataValue.Keyword;
                        itho += "<div class='rating-vote'><input type='checkbox' class='AttrTypeCl' name='chkAttractions'  id='chkatt_" + id + "' value='0'  /><input type='hidden' value='" + type + "'/><label for='chkatt_" + id + "'>" + type + "</label></div>";
                        if (indexValue == totalLength) {
                            itho += "</span><span><br/>";
                        }
                    });
                    itho += "</span></form>";
                    itho += "<div class='chn-pref'><input type='button' value='Apply' id='btnsubmit_attr' onclick='applyAttractions()'></div>";
                    itho += "</div>";
                    $('#hotel-dropdown-img').after(itho);
                    $('#rhaa1').html("");
                    var Attraction = new Array();
                    $.getJSON('../Trip/Getuserpreference', function (data) {
                        $('#RightSidePart').block({
                            message: 'Please wait while data is loading...',
                            css: {
                                border: 'none',
                                padding: '15px',
                                float: 'right',
                                'margin-right': '0%',
                                backgroundColor: '#033363',
                                '-webkit-border-radius': '10px',
                                '-moz-border-radius': '10px',
                                opacity: .5,
                                color: '#fff'
                            }
                        });
                        $.each(data, function (keys, values) {
                            var attraction = values.attraction;
                            $("#hidattraction").val(attraction);
                            if (prevAttrTypes == "") {
                                Attraction = values.attraction.split(",");
                                prevAttrTypes = values.attraction;
                            }
                            else {
                                Attraction = prevAttrTypes.split(",");
                            }
                            CheckArraySort = Attraction.length;
                            for (var loopvar_attraction = 0, AttractionsLength = Attraction.length; loopvar_attraction < AttractionsLength; loopvar_attraction++) {
                                $('input[name="chkAttractions"]').each(function () {
                                    var curAtType = $(this).next('input[type=hidden]').val();
                                    if (Attraction[loopvar_attraction] == curAtType) {
                                        $(this).val("1");
                                        $(this).attr("checked", true);
                                        initialize(curAtType);
                                    }
                                });
                                if (Attraction[loopvar_attraction] == "Nooption") {
                                    var attraction_pref = [];
                                    attraction_pref.push("aquarium", "place_of_worship", "museum", "art_gallery", "amusement_park", "park");
                                    var AttractionOptions = attraction_pref.length;
                                    var attraction_pref1 = [];
                                    attraction_pref1.push("palace", "botanical garden", "stadium", "bridge", "cathedral", "museum", "beach", "island", "zoo", "square", "statue", "harbour", "pier", "tower", "historical building");
                                    var AttractionOptions1 = attraction_pref1.length;
                                    touristele = 0;
                                    var tmpLengthAttr = Math.ceil(AttractionOptions / 6);
                                    CheckArraySort = tmpLengthAttr + Math.ceil(AttractionOptions1 / 6);
                                    var increCnt = 0;
                                    var newarrayTmp = "";
                                    for (var loopvar = 0; loopvar < AttractionOptions; loopvar += 6) {
                                        increCnt++;
                                        if (increCnt < tmpLengthAttr || AttractionOptions % 6 == 0) {
                                            newarrayTmp = attraction_pref[loopvar + 5] + "|" + attraction_pref[loopvar + 4] + "|" + attraction_pref[loopvar + 3] + "|" + attraction_pref[loopvar + 2] + "|" + attraction_pref[loopvar + 1] + "|" + attraction_pref[loopvar];
                                            initialize(newarrayTmp);
                                        }
                                    }
                                    increCnt = 0;
                                    newarrayTmp = "";
                                    tmpLengthAttr = Math.ceil(AttractionOptions1 / 5);
                                    for (var loopvar = 0; loopvar < AttractionOptions1; loopvar += 5) {
                                        increCnt++;
                                        if (increCnt < tmpLengthAttr || AttractionOptions1 % 5 == 0) {
                                            newarrayTmp = "(" + attraction_pref1[loopvar + 4] + " OR " + attraction_pref1[loopvar + 3] + " OR " + attraction_pref1[loopvar + 2] + " OR " + attraction_pref1[loopvar + 1] + " OR " + attraction_pref1[loopvar] + ")";
                                            initialize_call_withkeyword(newarrayTmp);
                                        }
                                    }
                                }
                            }
                        });
                        getMnalAttrTyp();
                       // GetManualEntry('2', $('#latarray').val(), $('#lngarray').val());
                    }).fail(function () {
                        $('#RightSidePart').block({
                            message: 'Please wait while data is loading...',
                            css: {
                                border: 'none',
                                padding: '15px',
                                float: 'right',
                                'margin-right': '0%',
                                backgroundColor: '#033363',
                                '-webkit-border-radius': '10px',
                                '-moz-border-radius': '10px',
                                opacity: .5,
                                color: '#fff'
                            }
                        });
                        Attraction = prevAttrTypes.split(",");
                        CheckArraySort = Attraction.length;
                        for (var loopvar_attraction = 0, AttractionsLength = Attraction.length; loopvar_attraction < AttractionsLength; loopvar_attraction++) {
                            $('input[name="chkAttractions"]').each(function () {
                                var curAtType = $(this).next('input[type=hidden]').val();
                                if (Attraction[loopvar_attraction] == curAtType) {
                                    $(this).val("1");
                                    $(this).attr("checked", true);
                                    initialize(curAtType);
                                }
                            });
                            if (Attraction[loopvar_attraction] == "Nooption" || Attraction[loopvar_attraction] == "") {
                                var attraction_pref = [];
                                attraction_pref.push("aquarium", "place_of_worship", "museum", "art_gallery", "amusement_park", "park");
                                var AttractionOptions = attraction_pref.length;
                                var attraction_pref1 = [];
                                attraction_pref1.push("palace", "botanical garden", "stadium", "bridge", "cathedral", "museum", "beach", "island", "zoo", "square", "statue", "harbour", "pier", "tower", "historical building");
                                var AttractionOptions1 = attraction_pref1.length;
                                touristele = 0;
                                var tmpLengthAttr = Math.ceil(AttractionOptions / 6);
                                CheckArraySort = tmpLengthAttr + Math.ceil(AttractionOptions1 / 6);
                                var increCnt = 0;
                                var newarrayTmp = "";
                                for (var loopvar = 0; loopvar < AttractionOptions; loopvar += 6) {
                                    increCnt++;
                                    if (increCnt < tmpLengthAttr || AttractionOptions % 6 == 0) {
                                        newarrayTmp = attraction_pref[loopvar + 5] + "|" + attraction_pref[loopvar + 4] + "|" + attraction_pref[loopvar + 3] + "|" + attraction_pref[loopvar + 2] + "|" + attraction_pref[loopvar + 1] + "|" + attraction_pref[loopvar];
                                        initialize(newarrayTmp);
                                    }
                                }
                                increCnt = 0;
                                newarrayTmp = "";
                                tmpLengthAttr = Math.ceil(AttractionOptions1 / 5);
                                for (var loopvar = 0; loopvar < AttractionOptions1; loopvar += 5) {
                                    increCnt++;
                                    if (increCnt < tmpLengthAttr || AttractionOptions1 % 5 == 0) {
                                        newarrayTmp = "(" + attraction_pref1[loopvar + 4] + " OR " + attraction_pref1[loopvar + 3] + " OR " + attraction_pref1[loopvar + 2] + " OR " + attraction_pref1[loopvar + 1] + " OR " + attraction_pref1[loopvar] + ")";
                                        initialize_call_withkeyword(newarrayTmp);
                                    }
                                }                                
                            }
                        }
                        getMnalAttrTyp();
                        //GetManualEntry('2', $('#latarray').val(), $('#lngarray').val());                        
                    });                    
                }
                else {
                    $.getJSON('../Trip/RetreivePreferrence', { Mode: 2 }, function (data) {
                        var totalLength = parseInt(Number(data.length) / 2);
                        $.each(data, function (indexValue, dataValue) {
                            OptAttr.push(dataValue);
                            var id = dataValue.ID;
                            var type = dataValue.Keyword;
                            itho += "<div class='rating-vote'><input type='checkbox' class='AttrTypeCl' name='chkAttractions'  id='chkatt_" + id + "' value='0'  /><input type='hidden' value='" + type + "'/><label for='chkatt_" + id + "'>" + type + "</label></div>";
                            if (indexValue == totalLength) {
                                itho += "</span><span><br/>";
                            }
                        });
                        itho += "</span></form>";
                        itho += "<div class='chn-pref'><input type='button' value='Apply' id='btnsubmit_attr' onclick='applyAttractions()'></div>";
                        itho += "</div>";
                        $('#hotel-dropdown-img').after(itho);
                        $('#rhaa1').html("");
                        var Attraction = new Array();
                        $.getJSON('../Trip/Getuserpreference', function (data) {
                            $('#RightSidePart').block({
                                message: 'Please wait while data is loading...',
                                css: {
                                    border: 'none',
                                    padding: '15px',
                                    float: 'right',
                                    'margin-right': '0%',
                                    backgroundColor: '#033363',
                                    '-webkit-border-radius': '10px',
                                    '-moz-border-radius': '10px',
                                    opacity: .5,
                                    color: '#fff'
                                }
                            });
                            $.each(data, function (keys, values) {
                                var attraction = values.attraction;
                                $("#hidattraction").val(attraction);
                                if (prevAttrTypes == "") {
                                    Attraction = values.attraction.split(",");
                                    prevAttrTypes = values.attraction;
                                }
                                else {
                                    Attraction = prevAttrTypes.split(",");
                                }
                                CheckArraySort = Attraction.length;
                                for (var loopvar_attraction = 0, AttractionsLength = Attraction.length; loopvar_attraction < AttractionsLength; loopvar_attraction++) {
                                    $('input[name="chkAttractions"]').each(function () {
                                        var curAtType = $(this).next('input[type=hidden]').val();
                                        if (Attraction[loopvar_attraction] == curAtType) {
                                            $(this).val("1");
                                            $(this).attr("checked", true);
                                            initialize(curAtType);
                                        }
                                    });
                                    if (Attraction[loopvar_attraction] == "Nooption") {
                                        var attraction_pref = [];
                                        attraction_pref.push("aquarium", "place_of_worship", "museum", "art_gallery", "amusement_park", "park");
                                        var AttractionOptions = attraction_pref.length;
                                        var attraction_pref1 = [];
                                        attraction_pref1.push("palace", "botanical garden", "stadium", "bridge", "cathedral", "museum", "beach", "island", "zoo", "square", "statue", "harbour", "pier", "tower", "historical building");
                                        var AttractionOptions1 = attraction_pref1.length;
                                        touristele = 0;
                                        var tmpLengthAttr = Math.ceil(AttractionOptions / 6);
                                        CheckArraySort = tmpLengthAttr + Math.ceil(AttractionOptions1 / 6);
                                        var increCnt = 0;
                                        var newarrayTmp = "";
                                        for (var loopvar = 0; loopvar < AttractionOptions; loopvar += 6) {
                                            increCnt++;
                                            if (increCnt < tmpLengthAttr || AttractionOptions % 6 == 0) {
                                                newarrayTmp = attraction_pref[loopvar + 5] + "|" + attraction_pref[loopvar + 4] + "|" + attraction_pref[loopvar + 3] + "|" + attraction_pref[loopvar + 2] + "|" + attraction_pref[loopvar + 1] + "|" + attraction_pref[loopvar];
                                                initialize(newarrayTmp);
                                            }
                                        }
                                        increCnt = 0;
                                        newarrayTmp = "";
                                        tmpLengthAttr = Math.ceil(AttractionOptions1 / 5);
                                        for (var loopvar = 0; loopvar < AttractionOptions1; loopvar += 5) {
                                            increCnt++;
                                            if (increCnt < tmpLengthAttr || AttractionOptions1 % 5 == 0) {
                                                newarrayTmp = "(" + attraction_pref1[loopvar + 4] + " OR " + attraction_pref1[loopvar + 3] + " OR " + attraction_pref1[loopvar + 2] + " OR " + attraction_pref1[loopvar + 1] + " OR " + attraction_pref1[loopvar] + ")";
                                                initialize_call_withkeyword(newarrayTmp);
                                            }
                                        }
                                    }
                                }
                            });
                            getMnalAttrTyp();
                            //GetManualEntry('2', $('#latarray').val(), $('#lngarray').val());
                            //setTimeout(function () { intheTripRHAA(RHAAId); }, 12000);
                        }).fail(function () {
                            $('#RightSidePart').block({
                                message: 'Please wait while data is loading...',
                                css: {
                                    border: 'none',
                                    padding: '15px',
                                    float: 'right',
                                    'margin-right': '0%',
                                    backgroundColor: '#033363',
                                    '-webkit-border-radius': '10px',
                                    '-moz-border-radius': '10px',
                                    opacity: .5,
                                    color: '#fff'
                                }
                            });
                            Attraction = prevAttrTypes.split(",");
                            CheckArraySort = Attraction.length;
                            for (var loopvar_attraction = 0, AttractionsLength = Attraction.length; loopvar_attraction < AttractionsLength; loopvar_attraction++) {
                                $('input[name="chkAttractions"]').each(function () {
                                    var curAtType = $(this).next('input[type=hidden]').val();
                                    if (Attraction[loopvar_attraction] == curAtType) {
                                        $(this).val("1");
                                        $(this).attr("checked", true);
                                        initialize(curAtType);
                                    }
                                });
                                if (Attraction[loopvar_attraction] == "Nooption" || Attraction[loopvar_attraction] == "") {
                                    var attraction_pref = [];
                                    attraction_pref.push("aquarium", "place_of_worship", "museum", "art_gallery", "amusement_park", "park");
                                    var AttractionOptions = attraction_pref.length;
                                    var attraction_pref1 = [];
                                    attraction_pref1.push("palace", "botanical garden", "stadium", "bridge", "cathedral", "museum", "beach", "island", "zoo", "square", "statue", "harbour", "pier", "tower", "historical building");
                                    var AttractionOptions1 = attraction_pref1.length;
                                    touristele = 0;
                                    var tmpLengthAttr = Math.ceil(AttractionOptions / 6);
                                    CheckArraySort = tmpLengthAttr + Math.ceil(AttractionOptions1 / 6);
                                    var increCnt = 0;
                                    var newarrayTmp = "";
                                    for (var loopvar = 0; loopvar < AttractionOptions; loopvar += 6) {
                                        increCnt++;
                                        if (increCnt < tmpLengthAttr || AttractionOptions % 6 == 0) {
                                            newarrayTmp = attraction_pref[loopvar + 5] + "|" + attraction_pref[loopvar + 4] + "|" + attraction_pref[loopvar + 3] + "|" + attraction_pref[loopvar + 2] + "|" + attraction_pref[loopvar + 1] + "|" + attraction_pref[loopvar];
                                            initialize(newarrayTmp);
                                        }
                                    }
                                    increCnt = 0;
                                    newarrayTmp = "";
                                    tmpLengthAttr = Math.ceil(AttractionOptions1 / 5);
                                    for (var loopvar = 0; loopvar < AttractionOptions1; loopvar += 5) {
                                        increCnt++;
                                        if (increCnt < tmpLengthAttr || AttractionOptions1 % 5 == 0) {
                                            newarrayTmp = "(" + attraction_pref1[loopvar + 4] + " OR " + attraction_pref1[loopvar + 3] + " OR " + attraction_pref1[loopvar + 2] + " OR " + attraction_pref1[loopvar + 1] + " OR " + attraction_pref1[loopvar] + ")";
                                            initialize_call_withkeyword(newarrayTmp);
                                        }
                                    }                                    
                                    //GetManualEntry('2', $('#latarray').val(), $('#lngarray').val());
                                    //setTimeout(function () { intheTripRHAA(RHAAId); }, 12000);
                                }
                            }
                            getMnalAttrTyp();
                        });
                    });
                }
            } catch (err) {
            }
            $("#btnsubmit_attr").on('click', function () {
                //$("#hotel-dropdown-img").trigger("click");
                $(".dropdown-pad").hide();
            });
        }
        else if (this.id == "Activities") {
            //debugger;
            $('#RightSidePart').block({
                message: 'Please wait while data is loading...',
                css: {
                    border: 'none',
                    padding: '15px',
                    float: 'right',
                    'margin-right': '0%',

                    backgroundColor: '#033363',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
            });
            c = 4;
            apiStatus = 0;
            $("#NoResBlock").hide();
            $("#rhaa1").css('display', 'block');
            $('.bg-style-heading').show();
            $(".ho tel-dropdown").show();
            $('.dropdown-pad').show();
            $("#divplace").css('display', 'none');
            $("#divmode").css('display', 'none');
            $('#divbtnothers').hide();
            $('#Spnsortbyid').show();
            $('#SpnFilteredbyid').show();
            $('#hotel-dropdown-img').show();
            nam = "Activity Preferences";
            $('.dropdown-pad').remove();
            $('#show_hide_ressort').hide();
            $('#show_hide_attrsort').hide();
            $('#show_hide_hotelsort').hide();
            $('#show_hide_actsort').show();
            $('#cus_plc_lbl').hide();
            $('#cus_tm_lbl').hide();
            $("#rhaa1").html("");
            $('.drob-style-h3').next('.rating-vote').hide();
            var itho = "<div class='dropdown-pad drob-style' id='applyScroll'><h4 style='font-size:medium; font-weight:600;'>Filtering By </h4><span><h3>Popularity rating</h3>";
            itho += "<div class='rating-vote'><input type='checkbox' style='float:left;margin-right:5px;cursor:pointer;width:15px;' class='check-box1 ignore_click' id='chckActRat1' name='Act_Rating' value='5'/>";
            itho += "<label for='chckActRat1'><div class='ratings-yellow'></div><div class='ratings-yellow'></div><div class='ratings-yellow'></div><div class='ratings-yellow'></div><div class='ratings-yellow'></div></label></div>";
            itho += "<div class='rating-vote'><input type='checkbox' style='float:left;margin-right:5px;cursor:pointer;width:15px;' class='check-box1 ignore_click' id='chckActRat2' name='Act_Rating' value='4'/>";
            itho += "<label for='chckActRat2'><div class='ratings-yellow'></div><div class='ratings-yellow'></div><div class='ratings-yellow'></div><div class='ratings-yellow'></div></label></div>";
            itho += "<div class='rating-vote'><input type='checkbox' style='float:left;margin-right:5px;cursor:pointer;width:15px;' class='check-box1 ignore_click' id='chckActRat3' name='Act_Rating' value='3'/>";
            itho += "<label for='chckActRat3'><div class='ratings-yellow'></div><div class='ratings-yellow'></div><div class='ratings-yellow'></div></label></div>";
            itho += "<div class='rating-vote'><input type='checkbox' style='float:left;margin-right:5px;cursor:pointer;width:15px;' class='check-box1 ignore_click' id='chckActRat4' name='Act_Rating' value='2'/>";
            itho += "<label for='chckActRat4'><div class='ratings-yellow'></div><div class='ratings-yellow'></label></div></div>";
            itho += "<div class='rating-vote'><input type='checkbox' style='float:left;margin-right:5px;cursor:pointer;width:15px;' class='check-box1 ignore_click' id='chckActRat5' name='Act_Rating' value='1'/>";
            itho += "<label for='chckActRat5'><div class='ratings-yellow'></div></label></div></span>";
            itho += "<span class='last'><h3>Categories</h3><div class='rating-vote'><input type='checkbox' style='float:left;margin-right:5px;cursor:pointer;width:15px;' name='Act_Cat' value='Adventures' class='check-box1 ignore_click' /><p>Adventures</p></div>";
            itho += "<div class='rating-vote'><input type='checkbox' style='float:left;margin-right:5px;cursor:pointer;width:15px;' name='Act_Cat' value='Attractions' class='check-box1 ignore_click' /><p>Attractions</p></div>";
            itho += "<div class='rating-vote'><input type='checkbox' style='float:left;margin-right:5px;cursor:pointer;width:15px;' name='Act_Cat' value='Day Trips & Excursions' class='check-box1 ignore_click' /><p>Day Trips & Excursions</p></div>";
            itho += "<div class='rating-vote'><input type='checkbox' style='float:left;margin-right:5px;cursor:pointer;width:15px;' name='Act_Cat' value='Food & Drink' class='check-box1 ignore_click' /><p>Food & Drink</p></div>";
            itho += "<div class='rating-vote'><input type='checkbox' style='float:left;margin-right:5px;cursor:pointer;width:15px;' name='Act_Cat' value='Hop-on Hop-off' class='check-box1 ignore_click' /><p>Hop-on Hop-off</p></div>";
            itho += "<div class='rating-vote'><input type='checkbox' style='float:left;margin-right:5px;cursor:pointer;width:15px;' name='Act_Cat' value='Multi-Day & Extended Tours' class='check-box1 ignore_click' /><p>Multi-Day & Extended Tours</p></div>";
            itho += "</span><span><br/>";
            itho += "<div class='rating-vote'><input type='checkbox' style='float:left;margin-right:5px;cursor:pointer;width:15px;' name='Act_Cat' value='Nightlife' class='check-box1 ignore_click' /><p>Nightlife</p></div>";
            itho += "<div class='rating-vote'><input type='checkbox' style='float:left;margin-right:5px;cursor:pointer;width:15px;' name='Act_Cat' value='Private Tours' class='check-box1 ignore_click' /><p>Private Tours</p></div>";
            itho += "<div class='rating-vote'><input type='checkbox' style='float:left;margin-right:5px;cursor:pointer;width:15px;' name='Act_Cat' value='Sightseeing Passes' class='check-box1 ignore_click' /><p>Sightseeing Passes</p></div>";
            itho += "<div class='rating-vote'><input type='checkbox' style='float:left;margin-right:5px;cursor:pointer;width:15px;' name='Act_Cat' value='Tours & Sightseeing' class='check-box1 ignore_click' /><p>Tours & Sightseeing</p></div>";
            itho += "<div class='rating-vote'><input type='checkbox' style='float:left;margin-right:5px;cursor:pointer;width:15px;' name='Act_Cat' value='Walking & Bike Tours' class='check-box1 ignore_click' /><p>Walking & Bike Tours</p></div>";
            itho += "</span>";
            itho += "<div class='price-bar' id='slideractivity'> <label for='amount'>Budget</label><input type='text' class='amount' id='price1' /> <div id='slider-range1' class='slider-pad'></div> </div>";
            itho += "<div class='chn-pref'><input type='button' value='Apply' id='btnsubmit_acti'></div>";
            itho += "</div>";
            $('#hotel-dropdown-img').after(itho);
            activities_forsort.length = 0;
            activities_foradd.length = 0;
            apiStatus = 1;
            $("#slider-range1").slider({
                range: true,
                min: 10,
                max: 2000,
                values: [10, 2000],
                stop: function (event, ui) {
                    $("#price1").val("$" + $("#slider-range1").slider("values", 0) + " - $" + $("#slider-range1").slider("values", 1));
                    actprice1 = "$" + ui.values[0];
                    actprice2 = "$" + ui.values[1];
                }
            });
            $("#price1").val("$" + $("#slider-range1").slider("values", 0) + " - $" + $("#slider-range1").slider("values", 1));
            LoadFilterActivities();
            GetActivities("list");
            var prsntFlag = 0;
                for (var loopvarHotels = 0, CitiesHotelsLength = ActsForCities.length; loopvarHotels < CitiesHotelsLength; loopvarHotels++) {
                    if (ActsForCities[loopvarHotels].cityName.trim() == $('#selectedcity').html().trim()) {
                        if (ActsForCities[loopvarHotels].Acts.Manual) {
                            prsntFlag = 1;                            
                        }
                    }
                }
                if (prsntFlag == 0)
                {
                    GetManualEntry('4', $('#latarray').val(), $('#lngarray').val());
                }
            if (getmanualEntryVal == 1 && apiStatus == 1)
            {
                $("#rhaa1").html("No results found..");
            }
            //$("#btnsubmit_acti").on('click', function () {
            //    $("#hotel-dropdown-img").trigger("click");
            //});
            //$('#RightSidePart').unblock();
        }
        else if (this.id == "Others") {
            $('#RightSidePart').block({
                message: 'Please wait,your saved custom places and travel modes are loading...',
                css: {
                    border: 'none',
                    padding: '15px',
                    float: 'right',
                    'margin-right': '0%',
                    backgroundColor: '#033363',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
            });
            $('#show_hide_ressort').hide();
            $('#show_hide_attrsort').hide();
            $('#show_hide_hotelsort').hide();
            $('#show_hide_actsort').hide();
            $('.dropdown-pad').hide();
            $('.drob-style-h3').next('.rating-vote').hide();
            $('#Spnsortbyid').hide();
            $('#SpnFilteredbyid').hide();
            nam = "Custom Places";
            PreferenceType = nam;
            $('#divbtnothers').show();
            $('#divcustom_place').hide();
            $('#divcustom_mode').hide();
            $('.bg-style-heading').hide();
            $('#rhaa1').hide();
            $('#cus_plc_lbl').show();
            $('#cus_tm_lbl').show();
            $('#divplace').show();
            $('#divmode').show();
            LoadCustomplace();
            LoadCustomMode();
            var sortElementid = "", divCopy = "";
            var trackId = [];
            setTimeout(function () { intheTripRHAA(RHAAId); }, 2000);
            $(".OtherContent-resort_left").sortable({
                connectWith: ".place-box",
                cursor:"move",
                helper: "clone",
                revert: "invalid",
                handle: ".resort-img",
                appendTo: "#mCSB_2_container",
                cancel: ".portlet-toggle,.resort-row2",
                placeholder: "portlet-placeholder ui-corner-all",
                start: function (event, ui) {
                    sortElementid = $(ui.item).attr('id');
                    trackId = sortElementid.split('_');
                    divCopy = $('#' + sortElementid).html();
                    $(ui.helper).css({ 'width': '260px', 'height': '60px' });
                    $(ui.helper).find('.resort-row1').find('.resort-img').find('img').css({ 'width': '50px', 'height': '50px' });
                    $(ui.helper).find('.resort-row1').find('.resort-img').find('img').css({ 'float': 'left', 'width': '59px !important', 'height': '59px !important', 'khtml-border-radius': '3px', 'moz-border-radius': '3px', 'webkit-border-radius': '3px', 'border-radius': '3px !important' });
                    $(ui.helper).find('.resort-row1').find('.resort-img').find('span').css('display', 'none');
                    $(ui.helper).find('.resort-row1').find('.resort-details').find('ul').css('display', 'none');
                    $(ui.helper).find('.resort-row1').find('.resort-details').find('.r-left').find('.book-add').css('display', 'none');
                    $(ui.helper).find('.resort-row1').find('.r-right').find('ul').css('display', 'block');
                    ui.item.show();
                },
                sort: function (event, ui) {
                    var id = ui.item[0].id;
                    var offsetPos = $('#mCSB_2_container').offset();
                    $(ui.helper).css("left", event.pageX - offsetPos.left);
                    $(ui.helper).css("top", event.pageY - offsetPos.top);
                    var width1 = $("#mCSB_1").width();
                    var ulWidth = $('#mCSB_1_container').width() - width1;
                    var Mscb_1Height = $('#mCSB_1_container').height();
                    var divLeft = $("#mCSB_1").offset().left;
                    var left1 = event.pageX - divLeft;
                    var percent1 = left1 / width1;
                    var totalcal = percent1 * ulWidth;
                    var draggableDivLeft = ui.position.left;
                    var draggableDivRight = (ui.position.left);
                    var MouseOnContainer = $("#mCSB_1").offset();
                    var DivPositionLeft = MouseOnContainer.left - draggableDivLeft;
                    IncreasePointLeft = IncreasePointLeft * 1.02;
                    if ((percent1 * ulWidth) < ulWidth) {
                        if (IncreasePointLeft > 1.35) {
                            IncreasePointLeft = 1.35;
                        }
                        $("#mCSB_1_container").css('left', -(percent1 * ulWidth));
                    }
                    else if (-(percent1 * ulWidth) > ulWidth) {
                        $("#mCSB_1_container").css('left', -(ulWidth));
                    }
                    var cityRowHeight = $(".city-row").height();
                    var height1 = $("#mCSB_2").height();
                    var mCSBHeight = $('#mCSB_2_container').height();
                    var ulheight = mCSBHeight - height1;
                    var draggableDivTop = ui.position.top;
                    var divTop = $("#mCSB_2").offset().top;
                    var top1 = event.pageY - divTop;
                    var percent2 = (top1 / ulheight);
                    var TopPosition = (percent2 * ulheight);
                    var divTopPos = -(percent2 * ulheight) - 100;
                    var placeboxheight = ($(".place-box").height());
                    divTopPos = divTopPos + 150;
                    var HalfHeight = height1 / 4;
                    var remainingheight = height1 - HalfHeight;
                    IncreasePoint = IncreasePoint * 1.02;
                    if ((-(divTopPos) <= HalfHeight) && (-(divTopPos) <= 0)) {
                        $("#mCSB_2_container").css("top", "0px");
                    }
                    else if ((-(divTopPos) >= height1 - 250)) {
                        $("#mCSB_2_container").css("top", -(ulheight));
                    }
                    else {
                        if (IncreasePoint > 5) {
                            IncreasePoint = 5;
                        }
                        $("#mCSB_2_container").css("top", ((divTopPos) * IncreasePoint));
                    }

                    var timeoutId;
                    $(".travel>ul,.travel>ul").hover(function () {
                        if (!timeoutId) {
                            timeoutId = window.setTimeout(function () {
                                timeoutId = null;
                                $(".travel").removeClass("expandTmTop");
                                $(".travel").not(this).removeClass("expandTmBtm");
                                $(this).parents('.travel').addClass('expandTmBtm');
                                // $(this).parents('.travel').css('margin-bottom', '50px');
                            }, 1000);
                        }
                    },
            function () {
                if (timeoutId) {
                    window.clearTimeout(timeoutId);
                    timeoutId = null;
                }
                else {
                    $(".travel").removeClass("expandTmTop");
                    $(".travel").not(this).removeClass("expandTmBtm");
                    $(this).parents('.travel').addClass('expandTmBtm');
                }
            });
                    $(".travel>p").hover(function () {
                        if (!timeoutId) {
                            timeoutId = window.setTimeout(function () {
                                timeoutId = null;
                                $(".travel").not(this).removeClass("expandTmTop");
                                $(".travel").removeClass("expandTmBtm");
                                $(this).parents('.travel').addClass('expandTmTop');
                            }, 500);
                        }
                    },
               function () {
                   if (timeoutId) {
                       window.clearTimeout(timeoutId);
                       timeoutId = null;
                   }
                   else {
                       $(".travel").not(this).removeClass("expandTmTop");
                       $(".travel").removeClass("expandTmBtm");
                       $(this).parents('.travel').addClass('expandTmTop');
                   }
               });
                    $(".custom_travel>ul,.custom_travel>ul>li").hover(function () {
                        if (!timeoutId) {
                            timeoutId = window.setTimeout(function () {
                                timeoutId = null;
                                $(".custom_travel").removeClass("expandTmTop");
                                $(".custom_travel").not(this).removeClass("expandTmBtm");
                                $(this).parents('.custom_travel').addClass('expandTmBtm');
                                // $(this).parents('.travel').css('margin-bottom', '50px');
                            }, 1000);
                        }
                    },
            function () {
                if (timeoutId) {
                    window.clearTimeout(timeoutId);
                    timeoutId = null;
                }
                else {
                    $(".custom_travel").removeClass("expandTmTop");
                    $(".custom_travel").not(this).removeClass("expandTmBtm");
                    $(this).parents('.custom_travel').addClass('expandTmBtm');
                }
            });
                    $(".custom_travel>p").hover(function () {
                        if (!timeoutId) {
                            timeoutId = window.setTimeout(function () {
                                timeoutId = null;
                                $(".custom_travel").not(this).removeClass("expandTmTop");
                                $(".custom_travel").removeClass("expandTmBtm");
                                $(this).parents('.custom_travel').addClass('expandTmTop');
                            }, 500);
                        }
                    },
               function () {
                   if (timeoutId) {
                       window.clearTimeout(timeoutId);
                       timeoutId = null;
                   }
                   else {
                       $(".custom_travel").not(this).removeClass("expandTmTop");
                       $(".custom_travel").removeClass("expandTmBtm");
                       $(this).parents('.custom_travel').addClass('expandTmTop');
                   }
               });
                },
                stop: function (event, ui) {
                    $(".travel,.custom_travel").removeClass("expandTmTop");
                    $(".travel,.custom_travel").removeClass("expandTmBtm");
                    $('.travel>ul>li,.travel>ul,.travel>p,.custom_travel>ul,.custom_travel>ul>li,.custom_travel>p').unbind('mouseenter mouseleave');
                    if ($(ui.item).parents(".place-box-right").length) {
                        if ($(ui.item).parent(".place-box").length) {
                            if ($(ui.item).parent().attr('id') == "" || $(ui.item).parent().attr('id') == null || $(ui.item).parent().attr('id') == "undefined") {
                                $(ui.item).parent().replaceWith($(ui.item));
                            }
                        }
                        if ($(ui.item).next(".place-box").length) {
                            $(ui.item).nextUntil(".place", ".place-box").each(function () {
                                $(this).remove();
                            });
                        }

                        var plcdivId = $(ui.item).parents('.place-box-right').attr('id');

                        var incr_num_plcbx = 0, PlcsTm_height = 0;
                        $("#plc_" + plcdivId).find(".place-box").each(function () {
                            incr_num_plcbx++;
                        });
                        $("#plc_" + plcdivId).find(".place").each(function () {
                            PlcsTm_height = PlcsTm_height + $(this).height();
                        });
                        $("#plc_" + plcdivId).find(".travel").each(function () {
                            PlcsTm_height = PlcsTm_height + $(this).height();
                        });
                        $("#plc_" + plcdivId).find(".custom_travel").each(function () {
                            PlcsTm_height = PlcsTm_height + $(this).height();
                        });
                        PlcsTm_height = PlcsTm_height + ((incr_num_plcbx - 1) * 50);
                        if ((PlcsTm_height + 100) > 2000) {
                            $(this).sortable('cancel');
                            var notify_alert = noty({
                                layout: 'topRight',
                                text: 'Exceeds day time limit,cannot be added..',
                                type: 'warning',
                                timeout: 3000,
                                maxVisible: 1,
                                animation: {
                                    open: { height: 'toggle' },
                                    close: { height: 'toggle' },
                                    easing: 'swing',
                                    speed: 500
                                }
                            });
                        }
                        else {
                            if ($(ui.item).prev(".travel").length) {
                                $(ui.item).prev(".travel").remove();
                            }
                            if ($(ui.item).next(".travel").length) {
                                $(ui.item).next(".travel").remove();
                            }
                            change_all_to_place(sortElementid);
                            $('#divplace').append(divCopy);
                            $('#divplace').find("#divres_" + trackId[1]).wrap("<aside class='resort-row' id='res_" + trackId[1] + "'></aside>");
                            $('#divplace').find("#res_" + trackId[1]).find(".resort-details").find(".add-trip").html("In the trip");
                            $('#divplace').find("#res_" + trackId[1]).addClass("resort-row2").removeClass("resort-row");
                        }
                    }
                    else {
                        $(this).sortable('cancel');
                        var notify_alert = noty({
                            layout: 'topRight',
                            text: 'Exceeds day time limit,cannot be added..',
                            type: 'warning',
                            timeout: 2000,
                            maxVisible: 1,
                            animation: {
                                open: { height: 'toggle' },
                                close: { height: 'toggle' },
                                easing: 'swing',
                                speed: 500
                            }
                        });
                    }
                }
            });
        }
        if (this.id == "classchangeidfirst") {
            nam = PreferenceType;
            PreferenceType = "";
        }
    }
    $('#classchangeid li').removeClass('last');
    if (nam == "Hotel Preferences") {
        $('#classchangeid #Hotel').removeClass('first');
        $('#classchangeid #Hotel').addClass('last');
    }
    else if (nam == "Restaurant Preferences") {
        $('#classchangeid #Restaurant').removeClass('first');
        $('#classchangeid #Restaurant').addClass('last');
    }
    else if (nam == "Attraction Preferences") {
        $('#classchangeid #Attractions').removeClass('first');
        $('#classchangeid #Attractions').addClass('last');
    }
    else if (nam == "Activity Preferences") {
        $('#classchangeid #Activities').removeClass('first');
        $('#classchangeid #Activities').addClass('last');
    }
    else if (nam == "Custom Places") {
        $('#classchangeid #Others').removeClass('first');
        $('#classchangeid #Others').addClass('last');
    }
    $('#prefer').html(nam);
});

$('#slider-range').slider(function () {
    $("#slider-range").slider({
        range: true,
        min: 10,
        max: 3000,
        values: [10, 3000],
        stop: function (event, ui) {
            $("#price").val("$" + $("#slider-range").slider("values", 0) + " - $" + $("#slider-range").slider("values", 1));
            price1 = "$" + ui.values[0];
            price2 = "$" + ui.values[1];
        }
    });
    $("#price").val("$" + $("#slider-range").slider("values", 0) + " - $" + $("#slider-range").slider("values", 1));

});

function applyRestaurents() {
    IsFilter = 1;
    TotalRests_SugPart = 0;
    incrRests_SugPart = 0;
    filtRestCnt = 0;
    gotfiltRestCnt = 0;
    AllRestColSugPart.length = 0;
    //$("#hotel-dropdown-img").trigger("click");
    $(".dropdown-pad").hide();
    var chckdIncr = 0;
    var SelectedRestaurents = $('input[name="chkRestaurents"]:checked').each(function (values) {
        var id = values.id;
        var type = $(id).next().val();
        var obj = { 'id': id, 'type': type };
        var value = [];
        value.push(obj);
        return value;
    });
    $('input[name="RestRating"]:checked').each(function () {
        chckdIncr++;
    });
    if (SelectedRestaurents.length == 0 || SelectedRestaurents == null) {
        $('#rhaa1').html("");
        prevRestTypes = "Nooption";
        Displayallrestaurant();
        //GetManualEntry('3', $('#latarray').val(), $('#lngarray').val());
    } else {
        prevRestTypes = "";
        var tmpcntArray = [];
        filtRestCnt = SelectedRestaurents.length;
        $("input:checkbox[name=chkRestaurents]:checked").each(function () {
            tmpcntArray.push($(this).next('input').val());
        });
        prevRestTypes = tmpcntArray.join(",");
        $('#rhaa1').html("");
        $.each(SelectedRestaurents, function (index, values) {
            var Id = parseInt(new String(values.id).match(/\d+/));
            var type = $('#' + values.id).next('input[type=hidden]').val();
            filterRestaurantType(Id, type);
        });
    }
}

function applyAttractions() {
    
    apiStatus = 0;
    IsFilter = 1;
    increAttrDet = 0;
    gotattrDet = 0;
    tmpincreValue = 0;
    incrDet2 = 0;
    gotDet2 = 0;
    var chckdIncr = 0;
    var ckckdprice = 0;
    gotfiltAttrCnt = 0;
    filtAttrCnt = 0;
    AllAttrColSugPart.length = 0;
    TotalAttrs_SugPart =0;
    incrAttrs_SugPart = 0;
    var SelectedAttractions = $('input[name="chkAttractions"]:checked').each(function (values) {
        return values;
    });    
    //$("#hotel-dropdown-img").trigger("click");
    IsFilter = 1;
    if (SelectedAttractions.length == 0 || SelectedAttractions == null) {
        $('#rhaa1').html("");
        prevAttrTypes = "Nooption";
        Displayall();
       // GetManualEntry('2', $('#latarray').val(), $('#lngarray').val());
    }
    else {
        prevAttrTypes = "";
        var tmpcntArray = [];
        filtAttrCnt = SelectedAttractions.length;
        $("input:checkbox[name=chkAttractions]:checked").each(function () {
            tmpcntArray.push($(this).next('input').val());
        });
        prevAttrTypes = tmpcntArray.join(",");
        $('#rhaa1').html("");
        $.each(SelectedAttractions, function (index, values) {
            var Id = new String(values.id).match(/\d+/);
            var type = $('#' + values.id).next('input[type=hidden]').val();
            filterAttractionType(Id, type);
        });
    }
}

function Loadminmax() {
    $("#slider-range").slider({
        range: true,
        min: 10,
        max: 2000,
        values: [10, 3000]
    });
    $("#price").val("$" + $("#slider-range").slider("values", 0) + " - $" + $("#slider-range").slider("values", 1));
    LoadFilterHotels();
}

function GetActivities(status) {
    apiStatus = 0;
    var CityForActsReq = $('#selectedcity').html();
    var chkForActdetails = 0;
    var CitiesHotelsLength = 0;
    for (var loopvarHotels = 0, CitiesHotelsLength = ActsForCities.length; loopvarHotels < CitiesHotelsLength; loopvarHotels++) {
        if (ActsForCities[loopvarHotels].cityName.trim() == CityForActsReq.trim()) {
            chkForActdetails = 1;
        }
    }
    if (chkForActdetails == 0) {
        $.ajax({
            type: 'GET',
            url: '../Trip/LocalExpertAPI',
            dataType: 'json',
            data: { 'cityname': CityForActsReq },
            success: function (response) {
                for (var loopActs = 0, actsLen = response.activities.length; loopActs < actsLen; loopActs++)
                {
                    response.activities[loopActs].Mnual = "0";
                }
                if (status == "get")
                {
                    activitiesCopy = response.activities.slice(0);
                    activitiesfull_array = response.activities.slice(0);
                }
                ActsForCities.push({
                    cityName: CityForActsReq,
                    Acts: response
                });
                for (var loopvarHotels = 0, CitiesHotelsLength = ActsForCities.length; loopvarHotels < CitiesHotelsLength; loopvarHotels++) {
                    if (ActsForCities[loopvarHotels].cityName.trim() == CityForActsReq.trim()) {
                        if (ActsForCities[loopvarHotels].Acts.activities) {
                            if (!(ActsForCities[loopvarHotels].Acts.Manual)) {
                                for (var indx = 0, lmt = manualdata.length; indx < lmt; indx++) {
                                    if (manualdata[indx].ctyName == CityForActsReq) {
                                        if (manualdata[indx].actidata) {
                                            var data = manualdata[indx].actidata;
                                            if (data != "No Result") {
                                                $.each(data, function (keys, values) {
                                                    ActsForCities[loopvarHotels].Acts.activities.push(data[keys]);
                                                });
                                            }
                                            ActsForCities[loopvarHotels].Acts.Manual = "1";
                                            break;
                                        }
                                    }
                                }
                            }
                            CopyOfActs = ActsForCities[loopvarHotels].Acts.activities.slice();
                        }
                    }
                }
                if (status == "list") {
                    DisplayActs();
                }
            },
            error:function(response)
            {
                var errResponse = response;
            }
        });
    }
    else
    {
        for (var loopvarHotels = 0, CitiesHotelsLength = ActsForCities.length; loopvarHotels < CitiesHotelsLength; loopvarHotels++) {
            if (ActsForCities[loopvarHotels].cityName.trim() == CityForActsReq.trim()) {
                if (ActsForCities[loopvarHotels].Acts.activities) {
                    if (!(ActsForCities[loopvarHotels].Acts.Manual)) {
                        for (var indx = 0, lmt = manualdata.length; indx < lmt; indx++) {
                            if (manualdata[indx].ctyName == CityForActsReq) {
                                if (manualdata[indx].actidata) {
                                    var data = manualdata[indx].actidata;
                                    if (data != "No Result") {
                                        $.each(data, function (keys, values) {
                                            ActsForCities[loopvarHotels].Acts.activities.push(data[keys]);
                                        });
                                    }
                                    ActsForCities[loopvarHotels].Acts.Manual = "1";
                                    break;
                                }
                            }
                        }
                    }
                    CopyOfActs = ActsForCities[loopvarHotels].Acts.activities.slice();
                }
            }
        }
        if (status == "list") {
            DisplayActs();
        }
    }
}

function DisplayActs()
{
    ActivtyArr.length = 0;
    $('#rhaa1').html("");
    $('#RightSidePart').unblock();
    if (CopyOfActs.length) {
        activities_forsort=CopyOfActs.slice();
        ActivtyArr = CopyOfActs.slice();
        //$.each(CopyOfActs, function (keys, values) {
        //    activities_forsort.push(CopyOfActs[keys]);
        //    ActivtyArr.push(CopyOfActs[keys]);
        //    var items = "",descri = "";
        //    var ltlnSplt = values.latLng.split(',');
        //    items += "<aside class='resort-row' id='res_" + values.id + "'><div class='resort-row1' id='divres_" + values.id + "'>";
        //    items += "<input type='hidden' class='hide'  id=" + values.id + "/><input type='hidden' id='latarray" + values.id + "' value='" + ltlnSplt[0] + "' class='reslat'/>";
        //    items += "<input type='hidden' id='lngarray" + values.id + "' value='" + ltlnSplt[1] + "' class='reslng'/>";
        //    items += "<input type='hidden' id='isME" + values.id + "' value='0' class='resME'>";
        //    items += "<figure class='resort-img'>";
        //    items += "<div class='tooltip'>Drag to add</div><img id='img_" + values.id + "' src='" + values.imageUrl + "' alt='' />";
        //    items += "<span>Price from <strong id='s" + values.id + "'>" + values.fromPrice + "</strong></span></figure>";
        //    items += "<div class='resort-details'><div class='r-left'><span class='hotel_identify' style='display:none'></span>";
        //    items += "<h3 id='h" + values.id + "'>" + values.title + "  </h3>";
        //    items += "<p>Recommendation Score : " + values.recommendationScore + "</p>";
        //    if (values.shortDescription && values.shortDescription!=null) {
        //        items += "<p id='pho_" + values.id + "' style='height:40px; overflow-y:hidden;'>" + values.shortDescription + "</p>";
        //    }
        //    if (values.duration && values.duration != null) {
        //        items += "<p id='pho_" + values.id + "' style='height:20px; overflow-y:hidden;'>" + values.duration + "</p>";
        //    }
        //    items += "<div class='book-add'><span><a href='#' id='inner" + values.id + "' class='add-trip'  onclick='return addtoleft(" + values.id + ")'>Add Trip</a></span>";
        //    items += "<span id='book" + values.id + "' style='display:none;'><a href='#' class='book'>Book</a></span></div></div> ";
        //    items += "<div class='r-right'>";
        //    items += " <ul><li><a href='#'><img src='../Content/images/del-icon.png' alt='' class='del-show' onclick=''/></a></li>";
        //    items += "<li><a  class='fancybox' href='#infowindow_hotels_activities' onclick=getActivityInfo('" + values.id + "')><img src='../Content/images/info-icon.png' alt='' class='example1demo' /></a></li></ul>";
        //    items += "</div></div></div>";
        //    items += "</aside>";
        //    $('#rhaa1').append(items);
        //});
        sortby_for_activities($('#activity_sortid_select').val());
    }
}

function GetHotels(status) {
    
    apiStatus = 0;
    var CityForHotelsReq = $('#selectedcity').html();
    var chkForHoteldetails = 0,CitiesHotelsLength = 0;
    for (var loopvarHotels = 0, CitiesHotelsLength = hotelsForCities.length; loopvarHotels < CitiesHotelsLength; loopvarHotels++) {
        if (hotelsForCities[loopvarHotels].cityName.trim() == CityForHotelsReq.trim()) {
            chkForHoteldetails = 1;
            break;
        }
    }
    if (chkForHoteldetails == 0) {
        addtoleft_hotelsfull.length = 0;
        var hotelsortv = $('#hotel_sortid_select').val();
        $.ajax({
            type: 'GET',
            url: '../Trip/EanHotelRequest',
            dataType: 'json',
            data: { 'lat': $('#latarray').val(), 'lng': $('#lngarray').val() },
            success: function (response) {
                got_hotelResult = 1;
                if (response.HotelListResponse) {
                    if (response.HotelListResponse.HotelList) {
                        for (var loopHotls = 0, actsLen = response.HotelListResponse.HotelList.length; loopHotls < actsLen; loopHotls++) {
                            response.HotelListResponse.HotelList[loopHotls].Mnual = "0";
                        }
                    }
                }
                hotelsForCities.push({
                    cityName: CityForHotelsReq,
                    Hotels: response
                });

                for (var loopvarHotels = 0, CitiesHotelsLength = hotelsForCities.length; loopvarHotels < CitiesHotelsLength; loopvarHotels++) {
                    if (hotelsForCities[loopvarHotels].cityName.trim() == CityForHotelsReq.trim()) {
                        if (hotelsForCities[loopvarHotels].Hotels.HotelListResponse && hotelsForCities[loopvarHotels].Hotels.HotelListResponse.HotelList) {
                            if (!(hotelsForCities[loopvarHotels].Hotels.HotelListResponse.HotelList.Manual)) {
                                for (var indx = 0, lmt = manualdata.length; indx < lmt; indx++) {
                                    if (manualdata[indx].ctyName == CityForHotelsReq) {
                                        if (manualdata[indx].hotldata) {
                                            var data = manualdata[indx].hotldata;
                                            if (data != "No Result") {
                                                $.each(data, function (keys, values) {
                                                    hotelsForCities[loopvarHotels].Hotels.HotelListResponse.HotelList['HotelSummary'].push(data[keys]);
                                                });
                                            }
                                            hotelsForCities[loopvarHotels].Hotels.HotelListResponse.HotelList.Manual = "1";
                                            break;
                                        }
                                    }
                                }
                            }
                            CopyOfHotels = hotelsForCities[loopvarHotels].Hotels.HotelListResponse.HotelList['HotelSummary'].slice();
                        }
                    }
                }
                if (status == "listHotels") {
                    DisplayHotelsList();
                }
            }
        });
    }
    for (var loopvarHotels = 0, CitiesHotelsLength = hotelsForCities.length; loopvarHotels < CitiesHotelsLength; loopvarHotels++) {
        if (hotelsForCities[loopvarHotels].cityName.trim() == CityForHotelsReq.trim()) {
            if (hotelsForCities[loopvarHotels].Hotels.HotelListResponse) {
                if (hotelsForCities[loopvarHotels].Hotels.HotelListResponse.HotelList) {
                    if (hotelsForCities[loopvarHotels].Hotels.HotelListResponse.HotelList["@size"] == 1) {
                        if (!(hotelsForCities[loopvarHotels].Hotels.HotelListResponse.HotelList.Manual)) {
                            for (var indx = 0, lmt = manualdata.length; indx < lmt; indx++) {
                                if (manualdata[indx].ctyName == CityForHotelsReq) {
                                    if (manualdata[indx].hotldata) {
                                        var data = manualdata[indx].hotldata;
                                        if (data != "No Result") {
                                            $.each(data, function (keys, values) {
                                                hotelsForCities[loopvarHotels].Hotels.HotelListResponse.HotelList['HotelSummary'].push(data[keys]);
                                            });
                                        }
                                        hotelsForCities[loopvarHotels].Hotels.HotelListResponse.HotelList.Manual = "1";
                                        break;
                                    }
                                }
                            }
                        }
                        CopyOfHotels = hotelsForCities[loopvarHotels].Hotels.HotelListResponse.HotelList['HotelSummary'];
                        if (CopyOfHotels.length > 0) {
                            apiStatus = 0;
                        }
                        else {
                            apiStatus = 1;
                        }
                    }
                    else {
                        if (!(hotelsForCities[loopvarHotels].Hotels.HotelListResponse.HotelList.Manual)) {
                            for (var indx = 0, lmt = manualdata.length; indx < lmt; indx++) {
                                if (manualdata[indx].ctyName == CityForHotelsReq) {
                                    if (manualdata[indx].hotldata) {
                                        var data = manualdata[indx].hotldata;
                                        if (data != "No Result") {
                                            $.each(data, function (keys, values) {
                                                hotelsForCities[loopvarHotels].Hotels.HotelListResponse.HotelList['HotelSummary'].push(data[keys]);
                                            });
                                        }
                                        hotelsForCities[loopvarHotels].Hotels.HotelListResponse.HotelList.Manual = "1";
                                        break;
                                    }
                                }
                            }
                        }
                        CopyOfHotels = hotelsForCities[loopvarHotels].Hotels.HotelListResponse.HotelList['HotelSummary'].slice();
                        if (CopyOfHotels.length > 0) {
                            apiStatus = 0;
                        }
                        else {
                            apiStatus = 1;
                        }
                    }
                }
            }
        }
    }
    if (status == "listHotels") {
        DisplayHotelsList();
        //var sorthelpsp = $("#sorthelp").val().split('_');
        //var ChangeIntheTripHotels = 0;
        //for (var hotelInTrip_Index = 0, ChangeIntheTripHotels = sorthelpsp.length; hotelInTrip_Index < ChangeIntheTripHotels; hotelInTrip_Index++) {
        //    $('#rhaa1 .resort-row').each(function () {
        //        if ($(this).attr('id') === "res_" + sorthelpsp[hotelInTrip_Index]) {
        //            $(this).find('.resort-row1').find('.resort-details').find('.book-add').find('.add-trip').html("In the trip");
        //            $(this).find('.resort-row1').find('.resort-details').find('.book-add').find('.book').parent().css('display', 'block');
        //        }
        //    });
        //}
        //var Budgetstart = $("#slider-range").slider("values", 0);
        //var Budgetend = $("#slider-range").slider("values", 1);
        //if ($("input[class='check-box']:checked").length > 0 || (Budgetstart != 10 && Budgetend != 3000)) {
        //    ApplyHotelsFilter();
        //}
    }
}

function DisplayHotelsList() {
    addtoleft_hotelsfull.length = 0;
   // if (HtlSrtArr.length == 0 && prevHtlSrt != $("#hotel_sortid_select").val() && prevSugCity != $('#selectedcity').html()) {
        prevSugCity = $('#selectedcity').html();
        $('#rhaa1').html("");
        getMnalHotelType();
        $('#RightSidePart').unblock();
        if (CopyOfHotels.length) {
            Hotels_forsort = CopyOfHotels.slice();
            HotelArr = CopyOfHotels.slice();
            //$.each(CopyOfHotels, function (keys, values) {
            //    addtoleft_hotelsfull.push(CopyOfHotels[keys]);
            //    var items = "";
            //    var descri = "";
            //    var amenitiesare = "";
            //    var multiam = 0;
            //    items += "<aside class='resort-row' id='res_" + values.hotelId + "'><div class='resort-row1' id='divres_" + values.hotelId + "'>";
            //    items += "<input type='hidden' id='" + values.hotelId + "' value='" + values.hotelId + "' class='hide'><input type='hidden' id='latarray" + values.hotelId + "' value='" + values.latitude + "' class='reslat'/><input type='hidden' id='lngarray" + values.hotelId + "' value='" + values.longitude + "' class='reslng'/>";
            //    items += "<input type='hidden' id='isME" + values.hotelId + "' value='0' class='resME'>";
            //    items += "<input type='hidden' id='htlRt" + values.hotelId + "' value='0' class='htlrt'><input type='hidden' id='htlCls" + values.hotelId + "' value='" + values.hotelRating + "' class='htlCls'>";
            //    items += "<input type='hidden' id='htlPrc" + values.hotelId + "' value='" + values.highRate + "' class='htlPrc'>";
            //    items += "<figure class='resort-img'>";
            //    var imgurl_split = values.thumbNailUrl.split('_');
            //    imgurl_split[imgurl_split.length - 1] = imgurl_split[imgurl_split.length - 1].replace('t.jpg', 'b.jpg');
            //    var imgurl = imgurl_split.join('_');
            //    items += "<div class='tooltip'>Drag to add</div><img src='http://media3.expedia.com" + imgurl + "' alt='' />";
            //    items += "<span>Night from $<strong id='s" + values.hotelId + "'>" + values.highRate + "</strong></span></figure>";
            //    items += "<div class='resort-details'><div class='r-left'><span class='hotel_identify' style='display:none'></span>";
            //    items += "<h3 id='h" + values.hotelId + "'>" + values.name + "  </h3><ul><b>Hotel class :</b>";
            //    for (var loopvar_hc = 1; loopvar_hc <= 5; loopvar_hc++) {
            //        if (values.hotelRating >= loopvar_hc) {
            //            items += "<li><img src='../Content/images/yellow-star1.png' /></li>";
            //        }
            //        else if (values.hotelRating > (loopvar_hc - 1) && values.hotelRating < loopvar_hc) {
            //            items += "<li><img src='../Content/images/yellow-gray-starNew.png' /></li>";
            //        }
            //        else {
            //            items += "<li><img src='../Content/images/grey-starNew.png' /></li>";
            //        }
            //    }
            //    items += "(" + values.hotelRating + ")</ul>";
            //    var desc = (values.shortDescription).replace('&lt;p&gt;&lt;b&gt;Property Location&lt;/b&gt; &lt;br /&gt;', '') + "...";
            //    items += "<p id='pho_" + values.hotelId + "' style='height:40px; overflow-y:hidden;'>" + desc + "</p>";
            //    items += "<div class='book-add'><span><a href='#' id='inner" + values.hotelId + "' class='add-trip'  onclick='return addtoleft(" + values.hotelId + ")'>Add Trip</a></span><span id='book" + values.hotelId + "' style='display:none;'><a href='#' class='book'>Book</a></span></div></div> ";
            //    items += "<div class='r-right'>";
            //    items += " <ul><li><a href='#'><img src='../Content/images/del-icon.png' alt='' class='del-show' onclick=''/></a></li>";
            //    items += "<li><a  class='fancybox' href='#info_" + values.hotelId + "'><img src='../Content/images/info-icon.png' alt='' class='example1demo' /></a></li></ul>";
            //    items += "</div></div></div>";
            //    items += "</aside>";

            //    $('#rhaa1').append(items);
            //});
            //intheTripRHAA("Hotel");

            sortby_for_Hotels($('#hotel_sortid_select').val());
        }
        else {
            apiStatus = 1;
            if (getmanualEntryVal == 1 && apiStatus == 1) {
                $('#rhaa1').html("No results Found..");
            }
        }
  ////  }
    //sortHotels();
}

function sortby_for_Hotels(hotelsortval)
{
    $('#rhaa1').html("");
    //if (hotelsortval == "ALPHA") {
    //    Hotels_forsort.sort(function (a, b) {
    //        if (a.name < b.name)
    //            return -1;
    //        if (a.name > b.name)
    //            return 1;
    //        return 0;
    //    });
    //}
    //else if (hotelsortval == "ALPHA_REVERSE") {
    //    Hotels_forsort.sort(function (a, b) {
    //        if (a.name > b.name)
    //            return -1;
    //        if (a.name < b.name)
    //            return 1;
    //        return 0;
    //    });
    //}
    //else if (hotelsortval == "PRICE") {
    //    Hotels_forsort.sort(function (a, b) {
    //        if (a.highRate < b.highRate)
    //            return -1;
    //        if (a.highRate > b.highRate)
    //            return 1;
    //        return 0;
    //    });
    //}
    //else if (hotelsortval == "PRICE_REVERSE") {
    //    Hotels_forsort.sort(function (a, b) {
    //        if (a.highRate > b.highRate)
    //            return -1;
    //        if (a.highRate < b.highRate)
    //            return 1;
    //        return 0;
    //    });
    //}
    //else if (hotelsortval == "RATING") {
    //    Hotels_forsort.sort(function (a, b) {
    //        if (a.hotelRating > b.hotelRating)
    //            return -1;
    //        if (a.hotelRating < b.hotelRating)
    //            return 1;
    //        return 0;
    //    });
    //}
    //else if (hotelsortval == "RATING_REVERSE") {
    //    Hotels_forsort.sort(function (a, b) {
    //        if (a.hotelRating < b.hotelRating)
    //            return -1;
    //        if (a.hotelRating > b.hotelRating)
    //            return 1;
    //        return 0;
    //    });
    //}
    HotelArr = Hotels_forsort.slice();
    filterHotels();
}

function filterHotels()
{
  
    $('#rhaa1').html("");

    //***Modified by Yuvapriya on 27Sep16

    //var tmpdata = [];
    //for (var indx = 0, lmt = manualdata.length; indx < lmt; indx++) {
    //    if (manualdata[indx].ctyName == $('#selectedcity').html()) {
    //        if (manualdata[indx].attrdata) {
    //            existsVal = 1;
    //            tmpdata = manualdata[indx].hotldata;
    //            break;
    //        }
    //    }
    //}

    //if (tmpdata.length > 0) {
    //    for (var tempcnt = 0; tempcnt < tmpdata.length; tempcnt++) {
    //        var latt = 0, longt = 0;           
           
    //        latt = tmpdata[tempcnt].latitude;
    //        longt = tmpdata[tempcnt].longitude
    //        var cnt = 0;
    //        $(".place-box-right").each(function () {
    //            $(this).find(".place").each(function () {
    //                if ($(this).id == tmpdata[tempcnt].hotelId) {
    //                    cnt = 1;
    //                }
    //            });
    //        });
    //        var IsTrip = "Add Trip";
    //        if (cnt == 0) {
    //            IsTrip = " Add Trip";
    //        }
    //        else {
    //            IsTrip = "In the Trip";
    //        }

    //        if (tmpdata[tempcnt].address1 == "undefined") {
    //            tmpdata[tempcnt].address1 = " ";
    //        }
    //        if (tmpdata[tempcnt].thumbNailUrl == "undefined") {
    //            tmpdata[tempcnt].thumbNailUrl = " ";
    //        }
    //        if (tmpdata[tempcnt].deepLink == "undefined") {
    //            tmpdata[tempcnt].deepLink = " ";
    //        }
    //        //highRate

    //        HotelArr.push({
    //            name: tmpdata[tempcnt].name,
    //            hotelId: tmpdata[tempcnt].hotelId,
    //            hotelRating: tmpdata[tempcnt].hotelRating,            
    //            latitude: latt,
    //            longitude: longt,
    //            address1: tmpdata[tempcnt].address1,
    //            thumbNailUrl: "../Content/images/HotelImages/" + tmpdata[tempcnt].thumbNailUrl,
    //            shortDescription: tmpdata[tempcnt].shortDescription,                
    //            highRate: tmpdata[tempcnt].highRate,
    //            IsManualHotl : 1
                
    //        });
    //    }
    //}


    //***End

    var filthc = "";
    var filterstr = [], filterstrRate = [], filteramenitiesStr = [];
    $("input:checkbox[name=hotel_class]:checked").each(function () {
        if ($(this).val() == 1) {
            filterstr.push("el.hotelRating < 2 && el.hotelRating >= 1");
        }
        else if ($(this).val() == 2) {
            filterstr.push("el.hotelRating < 3 && el.hotelRating >= 2");
        }
        else if ($(this).val() == 3) {
            filterstr.push("el.hotelRating < 4 && el.hotelRating >= 3");
        }
        else if ($(this).val() == 4) {
            filterstr.push("el.hotelRating < 5 && el.hotelRating >= 4");
        }
        else if ($(this).val() == 5) {
            filterstr.push("el.hotelRating == 5");
        }
    });

    $("input:checkbox[name=HP]:checked").each(function () {
        if ($(this).val() == 1) {
            filteramenitiesStr.push("el.amenityMask & 1");
        }
        else if ($(this).val() == 2) {
            filteramenitiesStr.push("el.amenityMask & 2");
        }
        else if ($(this).val() == 8) {
            filteramenitiesStr.push("el.amenityMask & 8");
        }
        else if ($(this).val() == 32) {
            filteramenitiesStr.push("el.amenityMask & 32");
        }
        else if ($(this).val() == 64) {
            filteramenitiesStr.push("el.amenityMask & 64");
        }
        else if ($(this).val() == 128) {
            filteramenitiesStr.push("el.amenityMask & 128");
        }
        else if ($(this).val() == 256) {
            filteramenitiesStr.push("el.amenityMask & 256");
        }
        else if ($(this).val() == 512) {
            filteramenitiesStr.push("el.amenityMask & 512");
        }
        else if ($(this).val() == 2048) {
            filteramenitiesStr.push("el.amenityMask & 2048");
        }
        else if ($(this).val() == 4096) {
            filteramenitiesStr.push("el.amenityMask & 4096");
        }
        else if ($(this).val() == 32768) {
            filteramenitiesStr.push("el.amenityMask & 32768");
        }
        else if ($(this).val() == 134217728) {
            filteramenitiesStr.push("el.amenityMask & 134217728");
        }
    });

    var slival1 = $("#slider-range").slider("values", 0);
    var slival2 = $("#slider-range").slider("values", 1);

    filthc += "(el.highRate>=" + slival1 + " && el.highRate<=" + slival2 + ")";

    var querystr1 = filterstrRate.join("||");
    var querystr = filterstr.join("||");
    var querystr2 = filteramenitiesStr.join(" && ");
    var fullquerystr = "", finalquerystr = "";

    if (querystr != "" && querystr1 != "" && querystr2 != "") {
        fullquerystr = "(" + querystr + ") && (" + querystr1 + ") && (" + querystr2 + ")";
    }
    else if (querystr == "" && querystr1 != "" && querystr2 != "") {
        fullquerystr = "(" + querystr1 + ") && (" + querystr2 + ")";
    }
    else if (querystr != "" && querystr1 == "" && querystr2 != "") {
        fullquerystr = "(" + querystr + ") && (" + querystr2 + ")";
    }
    else if (querystr != "" && querystr1 != "" && querystr2 == "") {
        fullquerystr = "(" + querystr + ") && (" + querystr1 + ")";
    }
    else if (querystr != "" && querystr1 == "" && querystr2 == "") {
        fullquerystr = querystr;
    }
    else if (querystr == "" && querystr1 != "" && querystr2 == "") {
        fullquerystr = querystr1;
    }
    else if (querystr == "" && querystr1 == "" && querystr2 != "") {
        fullquerystr = querystr2;
    }
    if (fullquerystr != "") {
        finalquerystr = fullquerystr + " && " + filthc;
        Hotels_forsort = HotelArr.filter(function (el) {
            return eval(finalquerystr);
        });
    }
    else {
        Hotels_forsort = HotelArr.filter(function (el) {
            return eval(filthc);
        });
    }

    var hotelsortval = $('#hotel_sortid_select').val();
    if (hotelsortval == "ALPHA") {
        Hotels_forsort.sort(function (a, b) {
            if (a.name < b.name)
                return -1;
            if (a.name > b.name)
                return 1;
            return 0;
        });
    }
    else if (hotelsortval == "ALPHA_REVERSE") {
        Hotels_forsort.sort(function (a, b) {
            if (a.name > b.name)
                return -1;
            if (a.name < b.name)
                return 1;
            return 0;
        });
    }
    else if (hotelsortval == "PRICE") {
        Hotels_forsort.sort(function (a, b) {
            if (a.highRate < b.highRate)
                return -1;
            if (a.highRate > b.highRate)
                return 1;
            return 0;
        });
    }
    else if (hotelsortval == "PRICE_REVERSE") {
        Hotels_forsort.sort(function (a, b) {
            if (a.highRate > b.highRate)
                return -1;
            if (a.highRate < b.highRate)
                return 1;
            return 0;
        });
    }
    else if (hotelsortval == "QUALITY") {
        Hotels_forsort.sort(function (a, b) {
            if (a.hotelRating > b.hotelRating)
                return -1;
            if (a.hotelRating < b.hotelRating)
                return 1;
            return 0;
        });
    }
    else if (hotelsortval == "QUALITY_REVERSE") {
        Hotels_forsort.sort(function (a, b) {
            if (a.hotelRating < b.hotelRating)
                return -1;
            if (a.hotelRating > b.hotelRating)
                return 1;
            return 0;
        });
    }

    $.each(Hotels_forsort, function (keys, values) {
        //debugger;
        addtoleft_hotelsfull.push(Hotels_forsort[keys]);
        var items = "";
        var descri = "";
        var amenitiesare = "";
        var multiam = 0;
        items += "<aside class='resort-row' id='res_" + values.hotelId + "'><div class='resort-row1' id='divres_" + values.hotelId + "'>";
        items += "<input type='hidden' id='" + values.hotelId + "' value='" + values.hotelId + "' class='hide'><input type='hidden' id='latarray" + values.hotelId + "' value='" + values.latitude + "' class='reslat'/><input type='hidden' id='lngarray" + values.hotelId + "' value='" + values.longitude + "' class='reslng'/>";
        items += "<input type='hidden' id='isME" + values.hotelId + "' value='0' class='resME'>";
        items += "<input type='hidden' id='htlRt" + values.hotelId + "' value='0' class='htlrt'><input type='hidden' id='htlCls" + values.hotelId + "' value='" + values.hotelRating + "' class='htlCls'>";
        items += "<input type='hidden' id='htlPrc" + values.hotelId + "' value='" + values.highRate + "' class='htlPrc'>";
        items += "<figure class='resort-img'>";
        if (values.IsManualHotl) {
            items += "<div class='tooltip'>Drag to add</div><img src=" + values.thumbNailUrl + " alt='' />";
        }
        else {
            var imgurl_split = values.thumbNailUrl.split('_');
            imgurl_split[imgurl_split.length - 1] = imgurl_split[imgurl_split.length - 1].replace('t.jpg', 'b.jpg');
            var imgurl = imgurl_split.join('_');
            var iurl = '"http://media3.expedia.com"' + imgurl + '"';
          
            var obj = new Image();
            obj.src = iurl;

            if (obj.complete) {
                items += "<div class='tooltip'>Drag to add</div><img src='http://media3.expedia.com" + imgurl + "' onerror='../Content/images/HotelImages/af5e-hotel.jpg' alt='' />";// Do something now you know the image exists.
                //alert('worked');
            } else {
                items += "<div class='tooltip'>Drag to add</div><img src='../Content/images/HotelImages/af5e-hotel.jpg' alt='' />";
                //alert('doesnt work');
            }
            //items += "<div class='tooltip'>Drag to add</div><img src='http://media3.expedia.com" + imgurl + "' onerror='../Content/images/HotelImages/af5e-hotel.jpg' alt='' />";// Do something now you know the image exists.

         //items += "<div class='tooltip'>Drag to add</div><img src='../Content/images/HotelImages/af5e-hotel.jpg' alt='' />";
       
            }
        if (values.IsManualHotl) {
            items += "<span style='display:block'>Night from $<strong id='s" + values.hotelId + "'>" + values.highRate + "</strong></span></figure>";
        }
        else {
            items += "<span>Night from $<strong id='s" + values.hotelId + "'>" + values.highRate + "</strong></span></figure>";
        }        
        items += "<div class='resort-details'><div class='r-left'><span class='hotel_identify' style='display:none'></span>";
        items += "<h3 id='h" + values.hotelId + "'>" + values.name + "  </h3><ul><b>Hotel class :</b>";
        for (var loopvar_hc = 1; loopvar_hc <= 5; loopvar_hc++) {
            if (values.hotelRating >= loopvar_hc) {
                items += "<li><img src='../Content/images/yellow-star1.png' /></li>";
            }
            else if (values.hotelRating > (loopvar_hc - 1) && values.hotelRating < loopvar_hc) {
                items += "<li><img src='../Content/images/yellow-gray-starNew.png' /></li>";
            }
            else {
                items += "<li><img src='../Content/images/grey-starNew.png' /></li>";
            }
        }
        items += "(" + parseFloat(values.hotelRating).toFixed(1) + ")</ul>";
        var desc = (values.shortDescription).replace('&lt;p&gt;&lt;b&gt;Property Location&lt;/b&gt; &lt;br /&gt;', '') + "...";
        items += "<p id='pho_" + values.hotelId + "' style='height:40px; overflow-y:hidden;'>" + desc + "</p>";


        //***Modified by Yuvapriya on 20 Sep 2016 To Remove Hote Book Remove (To add hotel Book Btn uncomment below line and comment next line)
        // items += "<div class='book-add'><span><a href='#' id='inner" + values.hotelId + "' class='add-trip'  onclick='return addtoleft(" + values.hotelId + ")'>Add Trip</a></span><span id='book" + values.hotelId + "' style='display:none;'><a href='#' class='book'>Book</a></span></div></div> ";
        items += "<div class='book-add'><span><a href='#' id='inner" + values.hotelId + "' class='add-trip'  onclick='return addtoleft(" + values.hotelId + ")'>Add Trip</a></span></div></div> ";
        //***End***

        items += "<div class='r-right'>";
        items += " <ul><li><a href='#'><img src='../Content/images/del-icon.png' alt='' class='del-show' onclick=''/></a></li>";
        //items += "<li><a  class='fancybox' href='#info_" + values.hotelId + "'><img src='../Content/images/info-icon.png' alt='' class='example1demo' /></a></li></ul>";
        items += "<li><a onclick = 'return infohotel(\"" + values.hotelId + "\");' class='fancybox' href='#infowindow'><img src='../Content/images/info-icon.png' alt='' class='example1demo' /></a></li></ul>";
        items += "</div></div></div>";
        items += "</aside>";
        $('#rhaa1').append(items);
    });
    if (!Hotels_forsort.length) {
        $('#rhaa1').html('No Results found..');
    }
    else {
        intheTripRHAA("Hotel");
    }
}

function LoadFilterHotels() {
    var filterrating = "", filter_class = "";
    var incre_filthotel = 0;
    $.getJSON('../Trip/Getuserpreference', function (data) {
        $.each(data, function (key, values) {
            if (values.star != null && values.star != "") {
                filterrating = values.star.toString();
            }
            else {
                filterrating = "";
            }
            if (values.Hotel_class != null && values.Hotel_class != "") {
                filter_class = values.Hotel_class.toString();
            }
            else {
                filter_class = "";
            }
            var filtersprating = filterrating.split(",");
            var filterspclass = filter_class.split(",");
            $("input[name='Popular']").each(function () {
                for (var loopvar = 0, hotelRatingArr = filtersprating.length; loopvar < hotelRatingArr; loopvar++) {
                    if (this.value === filtersprating[loopvar]) {
                        $(this).attr('checked', true);
                        incre_filthotel++;
                    }
                }
            });
            $("input[name='hotel_class']").each(function () {
                for (var loopvari = 0, hotelClassArr = filterspclass.length; loopvari < hotelClassArr; loopvari++) {
                    if (this.value === filterspclass[loopvari]) {
                        $(this).attr('checked', true);
                        incre_filthotel++;
                    }
                }
            });
        });
        if (incre_filthotel > 0) {
            ApplyHotelsFilter();            
        }        
    });
    //getMnalHotelType();
    //GetManualEntry('1', $('#latarray').val(), $('#lngarray').val());
}

function Loadminmaxact() {
    $("#slider-range1").slider({
        range: true,
        min: 10,
        max: 2000,
        values: [10, 2000]
    });
    $("#price1").val("$" + $("#slider-range1").slider("values", 0) + " - $" + $("#slider-range1").slider("values", 1));
}

function checkbox1changefn() {
    $('.check-box1').change(function () {
        var cntfil = 0;
        var filtactrat = "";
        var filtactcat = "";
        var catcount = 0;
        var ratcount = 0;
        $("input:checkbox[name=Act_Cat]:checked").each(function () {
            catcount++;
        });
        $("input:checkbox[name=Act_Cat]:checked").each(function () {
            if ($(this).val() === "Tours") {
                filtactcat += "1,172";
            }
            else if ($(this).val() === "Concerts_Operas") {

                filtactcat += "137";
            }
            else if ($(this).val() === "Shows_Musicals") {
                filtactcat += "124,127,128";
            }
            else if ($(this).val() === "Meet_the_Locals") {

                filtactcat += "34";
            }
            else if ($(this).val() === "Desert_Safaris") {

                filtactcat += "37";
            }
            else if ($(this).val() === "Ski_Snowboard") {

                filtactcat += "146";
            }
            else if ($(this).val() === "Sport_Events") {

                filtactcat += "138";
            }
            else if ($(this).val() === "Shop") {
                filtactcat += "20";
            }
            else if ($(this).val() === "Spa") {
                filtactcat += "92,93";
            }
            else if ($(this).val() === "Movie") {
                filtactcat += "126,22";
            }
            else if ($(this).val() === "Night") {
                filtactcat += "111,112";
            }
            else if ($(this).val() === "Park") {
                filtactcat += "36,131,136,159,192";
            }
            catcount--;
            if (catcount != 0) {
                filtactcat += ",";
            }
        });
        $("input:checkbox[name=Act_Rating]:checked").each(function () {
            ratcount++;
            cntfil = 1;
        });
        $("input:checkbox[name=Act_Rating]:checked").each(function () {
            if ($(this).val() == 1) {
                filtactrat += "rating>='1' AND rating<='1.9'";
            }
            else if ($(this).val() == 2) {
                filtactrat += "rating>='2' AND rating<='2.9'";
            }
            else if ($(this).val() == 3) {
                filtactrat += "rating>='3' AND rating<='3.9'";
            }
            else if ($(this).val() == 4) {
                filtactrat += "rating>='4' AND rating<='4.9'";
            }
            else if ($(this).val() == 5) {
                filtactrat += "rating>='5'";
            }
            ratcount--;
            if (ratcount != 0) {
                filtactrat += " OR ";
            }
        });

        if (cntfil == 1) {
            filtactrat += " AND ";
        }
        var slivalact1 = $("#slider-range1").slider("values", 0);
        var slivalact2 = $("#slider-range1").slider("values", 1);
        filtactrat += "Cost>=\'" + slivalact1 + "\' AND Cost<=\'" + slivalact2 + "\'";
        activities_foradd.length = 0;
        activities_forsort.length = 0;
        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: { filterdata: filtactcat, filteratdata: filtactrat },
            url: '../Trip/FilterActivities',
            success: function (response) {
                var itss = "";
                var tableData = JSON.stringify(response);
                var resdata = $.parseJSON(tableData);
                var infowin = "", actdiv = "", desc = "";
                $('#rhaa1').html("");
                $.each(resdata, function (keys, values) {
                    activities_forsort.push(resdata[keys]);
                    activities_foradd.push(resdata[keys]);
                    actdiv += "<aside class='resort-row2' id='rh_" + values.id + "'></aside>";
                    actdiv += "<aside class='resort-row' id='res_" + values.id + "'><div class='resort-row1' id='divres_" + values.id + "'>";
                    actdiv += "<input type='hidden' class='hide'  id=" + values.id + "><input type='hidden' id='latarray" + values.id + "' value='" + values.start_Latitude + "' class='reslat'><input type='hidden' id='lngarray" + values.id + "' value='" + values.start_Longitude + "' class='reslng'>";
                    actdiv += "<input type='hidden' id='isME" + values.id + "' value='0' class='resME'>";
                    if (values.rating != "") {
                        actdiv += "<input type='hidden' id='rates_" + values.id + "' value='" + values.rating + "'>";
                    }
                    else {
                        actdiv += "<input type='hidden' id='rates_" + values.id + "' value='0'>"
                    }
                    actdiv += "<figure class='resort-img'><div class='tooltip'>Drag to add</div><img src='" + values.Images + "' alt='' />";
                    actdiv += "<span>From <strong id='s" + values.id + "'>$" + values.Cost + "</strong></span></figure>";
                    actdiv += "<div class='resort-details'><div class='r-left'>";
                    actdiv += "<h3 id='h" + values.id + "'>" + values.title + "  </h3><br/>";
                    actdiv += "<p id='pho_" + values.id + "' style='height:40px; overflow-y:hidden;'>" + values.abstract + "</p>";
                    actdiv += "<div class='book-add'><span><a href='#' id='inner" + values.id + "' class='add-trip'  onclick='return addtoleft(" + values.id + ")'>Add Trip</a></span></div></div>";
                    actdiv += "<div class='r-right'>";
                    actdiv += "<ul><li><a href='#'><img src='../Content/images/del-icon.png' alt='' class='del-show' onclick=''/></a></li>";
                    actdiv += "<li><a  class='fancybox' href='#info_" + values.id + "'><img src='../Content/images/info-icon.png' alt='' class='example1demo' /></a></li></ul>";
                    actdiv += "</div></div></div>";
                    actdiv += "</aside>";
                    actdiv += "<div id='info_" + values.id + "' style='display: none;'>";
                    actdiv += "<div class='info-left popup-bg' style='width: 94%;'><figure class='resort-img'><img src='" + values.Images + "' alt='' /><span>From <strong>$" + values.Cost + "</strong><br/>(" + values.Price_Description + ")</span></figure>";
                    actdiv += "<h2>" + values.title + "</h2>";
                    if (values.rating != "") {
                        var actrating = parseFloat(values.rating).toFixed(1);
                        actdiv += "<ul><b>Ratings : </b>";
                        for (var loopvar_rate = 1; loopvar_rate <= 5; loopvar_rate++) {
                            if (actrating >= loopvar_rate) {
                                actdiv += "<li><img src='../Content/images/tennisball_full.png' /></li>";
                            }
                            else if (actrating > (loopvar_rate - 1) && actrating < loopvar_rate) {
                                actdiv += "<li><img src='../Content/images/tennisball_half.png' /></li>";
                            }
                            else {
                                actdiv += "<li><img src='../Content/images/tennisball_grey_full.png' /></li>";
                            }
                        }
                        actdiv += "<li>(" + actrating + "/5)</li></ul>";
                    }
                    actdiv += "<p><b>Starting location : </b>" + values.starting_location + "</p>";
                    actdiv += "<p><b>Locations : </b>" + values.locations_text + "</p>";
                    actdiv += "<p><b>Description : </b>" + values.abstract + "</p>";
                    actdiv += "<p><b>For booking : </b><a href='" + values.url + "'>Click here</a></p><br/>";
                    actdiv += "</div>";
                    actdiv += "</div>";
                });
                $('#rhaa1').append(actdiv);

                var activitysortval = $("#activity_sortid_select").val();
                sortby_for_activities(activitysortval);

                var sorthelpsp = $("#sorthelpact").val().split('_');
                for (var spval = 0, ActivitiesInTrip = sorthelpsp.length; spval < ActivitiesInTrip; spval++) {
                    $('#rhaa1 .resort-row').each(function () {
                        if ($(this).attr('id') === "res_" + sorthelpsp[spval]) {
                            $(this).find('.resort-row1').find('.resort-details').find('.book-add').find('a').text("In the trip");
                            $(this).addClass("resort-row2").removeClass("resort-row");
                        }
                    });
                }
                inthetripact();
            },
            error: function (response) {
                $('#rhaa1').html("");
                $('#rhaa1').html("No Results found");
            }
        });
    });
}

function callGYGAPI() {
    var citynameact = $("#selectedcity").html();
    current_city = citynameact;
    var sm = citynameact.split(',');
    sortedarrayall.length = 0;
    var murl = "&q=product_list&where=" + sm[0].trim();
    $.ajax({
        type: "GET",
        dataType: "json",
        data: { moreurl: murl },
        url: '../Trip/GYGMethod',
        success: function (response) { },
        error: function (response) { }
    });
}

function LoadActivities() {
    var citynameact = $("#selectedcity").html();
    var sm = citynameact.split(',');
    $('#rhaa1').html("");
    var murl = "&q=product_list&where=" + sm[0];
    $.ajax({
        type: "GET",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        data: { moreurl: murl },
        url: '../Trip/GYGMethod',
        success: function (response) {
            var itss = "";
            var tableData = JSON.stringify(response);
            var resdata = $.parseJSON(tableData);
            var desc = "", infowin = "", actdiv = "";
            if (!resdata.Message) {
                $.each(resdata, function (keys, values) {
                    activities_foradd.push(resdata[keys]);
                    activities_forsort.push(resdata[keys]);
                    actdiv += "<aside class='resort-row2' id='rh_" + values.id + "'></aside><aside class='resort-row' id='res_" + values.id + "'><div class='resort-row1' id='divres_" + values.id + "'>";
                    actdiv += "<input type='hidden' class='hide'  id=" + values.id + "><input type='hidden' id='latarray" + values.id + "' value='" + values.start_Latitude + "' class='reslat'><input type='hidden' id='lngarray" + values.id + "' value='" + values.start_Longitude + "' class='reslng'>";
                    actdiv += "<input type='hidden' id='isME" + values.id + "' value='0' class='resME'>";
                    actdiv += "<figure class='resort-img'><img src='" + values.Images + "' alt='' />";
                    actdiv += "<span>From <strong id='s" + values.id + "'>$" + values.Cost + "</strong></span></figure>";
                    actdiv += "<div class='resort-details'><div class='r-left'>";
                    actdiv += "<h3 id='h" + values.id + "'>" + values.title + "  </h3><br/>";
                    actdiv += "<p id='pho_" + values.id + "' style='height:40px; overflow-y:hidden;'>" + values.abstract + "</p>";
                    actdiv += "<div class='book-add'><span><a href='#' id='inner" + values.id + "' class='add-trip'  onclick='return addtoleft(" + values.id + ")'>Add Trip</a></span></div></div>";
                    actdiv += "<div class='r-right'>";
                    actdiv += "<ul><li><a href='#'><img src='../Content/images/del-icon.png' alt='' class='del-show' onclick=''/></a></li>";
                    actdiv += "<li><a  class='fancybox' href='#info_" + values.id + "'><img src='../Content/images/info-icon.png' alt='' class='example1demo' /></a></li></ul>";
                    actdiv += "</div></div></div>";
                    actdiv += "</aside>";
                    actdiv += "<div id='info_" + values.id + "' style='display: none;'>";
                    actdiv += "<div class='info-left popup-bg'  style='width: 94%;'><figure class='resort-img'><img src='" + values.Images + "' alt='' /><span>From <strong>$" + values.Cost + "</strong><br/>(" + values.Price_Description + ")</span></figure>";
                    actdiv += "<h2>" + values.title + "</h2>";
                    if (values.rating != "") {
                        var actrating = parseFloat(values.rating).toFixed(1);
                        actdiv += "<ul><b>Ratings : </b>";
                        for (var loopvar_rate = 1; loopvar_rate <= 5; loopvar_rate++) {
                            if (actrating >= loopvar_rate) {
                                actdiv += "<li><img src='../Content/images/tennisball_full.png' /></li>";
                            }
                            else if (actrating > (loopvar_rate - 1) && actrating < loopvar_rate) {
                                actdiv += "<li><img src='../Content/images/tennisball_half.png' /></li>";
                            }
                            else {
                                actdiv += "<li><img src='../Content/images/tennisball_grey_full.png' /></li>";
                            }
                        }

                        actdiv += "<li>(" + actrating + "/5)</li></ul>";
                    }
                    actdiv += "<p><b>Starting location : </b>" + values.starting_location + "</p>";
                    actdiv += "<p><b>Locations : </b>" + values.locations_text + "</p>";
                    actdiv += "<p><b>Description : </b>" + values.abstract + "</p>";
                    actdiv += "<p><b>For booking : </b><a href='" + values.url + "'>Click here</a></p><br/>";
                    actdiv += "</div>";
                    actdiv += "</div>";
                });
            }
            else {
                actdiv += "No results found..";
            }
            $('#rhaa1').append(actdiv);
            Loadminmaxact();
            var activitysortval = $("#activity_sortid_select").val();
            sortby_for_activities(activitysortval);
            LoadFilterActivities();
            var sorthelpsp = $("#sorthelpact").val().split('_');
            for (var spval = 0, ActivitiesInTrip = sorthelpsp.length; spval < ActivitiesInTrip; spval++) {
                $('#rhaa1 .resort-row').each(function () {
                    if ($(this).attr('id') === "res_" + sorthelpsp[spval]) {
                        $(this).find('.resort-row1').find('.resort-details').find('.book-add').find('a').text("In the trip");
                        $(this).addClass("resort-row2").removeClass("resort-row");
                    }
                });
            }
        },
        error: function (response) {
            $("#rhaa1").html("No results found..");
        }
    });
}

function LoadFilterActivities() {
    var filteractivity = "",incre_filter_act = 0;
    $.getJSON('../Trip/Getuserpreference', function (data) {
        $.each(data, function (key, values) {
            filteractivity = values.activity.toString();
            var filterspactivity = filteractivity.split(",");
            $("input[name='Act_Cat']").each(function () {
                for (var loopvar = 0; loopvar < filterspactivity.length; loopvar++) {
                    if (this.value === filterspactivity[loopvar]) {
                        $(this).attr('checked', true);
                        incre_filter_act++;
                    }
                }
            });
        });        
        if (incre_filter_act > 0) {
            //$('.check-box1').trigger('change');
            sortby_for_activities($('#activity_sortid_select').val());
        }       

        setTimeout(function () { GetManualEntry('4', $('#latarray').val(), $('#lngarray').val()); }, 3000);

    });
}

function sortby_for_activities(activitysortval) {
    $('#rhaa1').html("");
    if (activitysortval == "actrating_h_to_l") {
        activities_forsort.sort(function (obj1, obj2) {
            return obj2.recommendationScore - obj1.recommendationScore;
        });
    }
    else if (activitysortval == "actrating_l_to_h") {
        activities_forsort.sort(function (obj1, obj2) {
            return obj1.recommendationScore - obj2.recommendationScore;
        });
    }
    else if (activitysortval == "actprice_h_to_l") {
        activities_forsort.sort(function (obj1, obj2) {
            return obj2.fromPriceValue - obj1.fromPriceValue;
        });
    }
    else if (activitysortval == "actprice_l_to_h") {
        activities_forsort.sort(function (obj1, obj2) {
            return obj1.fromPriceValue - obj2.fromPriceValue;
        });
    }
    else if (activitysortval == "actalphabetical") {
        activities_forsort.sort(function (obj1, obj2) {
            var nameA = obj1.title.toLowerCase(), nameB = obj2.title.toLowerCase()
            if (nameA < nameB)
                return -1;
            if (nameA > nameB)
                return 1;
            return 0;
        });
    }
    else if (activitysortval == "actalphabetical_rev") {
        activities_forsort.sort(function (obj1, obj2) {
            var nameA = obj1.title.toLowerCase(), nameB = obj2.title.toLowerCase()
            if (nameA < nameB)
                return 1;
            if (nameA > nameB)
                return -1;
            return 0;
        });
    }
    ActivtyArr = activities_forsort.slice();
    //var actdiv = "";
    //for (var loopvar = 0, ActivitiesLength = activities_forsort.length; loopvar < ActivitiesLength; loopvar++) {
    //    ActivtyArr.push(activities_forsort[loopvar]);
    //    actdiv += "<aside class='resort-row' id='res_" + activities_forsort[loopvar].id + "'><div class='resort-row1' id='divres_" + activities_forsort[loopvar].id + "'>";
    //    actdiv += "<input type='hidden' class='hide'  id=" + activities_forsort[loopvar].id + "><input type='hidden' id='latarray" + activities_forsort[loopvar].id + "' value='" + activities_forsort[loopvar].start_Latitude + "' class='reslat'><input type='hidden' id='lngarray" + activities_forsort[loopvar].id + "' value='" + activities_forsort[loopvar].start_Longitude + "' class='reslng'>";
    //    actdiv += "<input type='hidden' id='isME" + activities_forsort[loopvar].id + "' value='0' class='resME'>";
    //    if (activities_forsort[loopvar].rating != "") {
    //        actdiv += "<input type='hidden' id='rates_" + activities_forsort[loopvar].id + "' value='" + activities_forsort[loopvar].rating + "'>";
    //    }
    //    else {
    //        actdiv += "<input type='hidden' id='rates_" + activities_forsort[loopvar].id + "' value='0'>";
    //    }
    //    actdiv += "<figure class='resort-img'><div class='tooltip'>Drag to add</div><img src='" + activities_forsort[loopvar].imageUrl + "' alt='' />";
    //    actdiv += "<span>From <strong id='s" + activities_forsort[loopvar].id + "'>" + activities_forsort[loopvar].fromPrice + "</strong></span></figure>";
    //    actdiv += "<div class='resort-details'><div class='r-left'>";
    //    actdiv += "<h3 id='h" + activities_forsort[loopvar].id + "'>" + activities_forsort[loopvar].title + "  </h3><br/>";
    //    actdiv += "<p>Recommendation Score : " + activities_forsort[loopvar].recommendationScore + "</p>";
    //    if (activities_forsort[loopvar].shortDescription && activities_forsort[loopvar].shortDescription != null) {
    //        actdiv += "<p id='pho_" + activities_forsort[loopvar].id + "' style='height:40px; overflow-y:hidden;'>" + activities_forsort[loopvar].shortDescription + "</p>";
    //    }
    //    if (activities_forsort[loopvar].duration && activities_forsort[loopvar].duration != null) {
    //        actdiv += "<p id='pho_" + activities_forsort[loopvar].id + "' style='height:20px; overflow-y:hidden;'>" + activities_forsort[loopvar].duration + "</p>";
    //    }        
    //    actdiv += "<div class='book-add'><span><a href='#' id='inner" + activities_forsort[loopvar].id + "' class='add-trip'  onclick='return addtoleft(" + activities_forsort[loopvar].id + ")'>Add Trip</a></span></div></div>";
    //    actdiv += "<div class='r-right'>";
    //    actdiv += "<ul><li><a href='#'><img src='../Content/images/del-icon.png' alt='' class='del-show' onclick=''/></a></li>";
    //    actdiv += "<li><a  class='fancybox' href='#infowindow_hotels_activities' onclick=getActivityInfo('" + activities_forsort[loopvar].id + "')><img src='../Content/images/info-icon.png' alt='' class='example1demo' /></a></li></ul>";
    //    actdiv += "</div></div></div>";
    //    actdiv += "</aside>";        
    //}
    //$('#rhaa1').append(actdiv);
    filterActivities();
}

function filterActivities() {
    var selectedCityis = $("#selectedcity").html();
    $('#rhaa1').html('');
    $("#applyScroll").hide();
    var filtrStrAct = "", flterActRat = [], filterPrice = "";
    $("input:checkbox[name=Act_Rating]:checked").each(function () {
        if ($(this).val() == 1) {
            flterActRat.push("el.recommendationScore < 40 && el.recommendationScore >= 20");
        }
        else if ($(this).val() == 2) {
            flterActRat.push("el.recommendationScore < 60 && el.recommendationScore >= 40");
        }
        else if ($(this).val() == 3) {
            flterActRat.push("el.recommendationScore < 80 && el.recommendationScore >= 60");
        }
        else if ($(this).val() == 4) {
            flterActRat.push("el.recommendationScore < 100 && el.recommendationScore >= 80");
        }
        else if ($(this).val() == 5) {
            flterActRat.push("el.recommendationScore==100");
        }
    });

    var arrayFilter = [];
    $("input:checkbox[name=Act_Cat]:checked").each(function () {
        arrayFilter.push($(this).val());
    });


    filtrStrAct = flterActRat.join("||");
    var slival1 = $("#slider-range1").slider("values", 0);
    var slival2 = $("#slider-range1").slider("values", 1);
    if (filtrStrAct != "")
    {
        filtrStrAct+=" && "
    }
    filtrStrAct += "(el.fromPriceValue>=" + slival1 + " && el.fromPriceValue<=" + slival2 + ")";    
    activities_forsort = ActivtyArr.filter(function (el) {
        return eval(filtrStrAct);
    });

    var tmpactivities_forsort = [];

    for (var actIndx = 0, actLeng = activities_forsort.length; actIndx < actLeng; actIndx++) {
        for (var filtIndx = 0, filtLeng = arrayFilter.length; filtIndx < filtLeng; filtIndx++) {
            var matchAct = false;
            var categories = activities_forsort[actIndx].categories;
            for (var cateIndx = 0, cateLen = categories.length; cateIndx < cateLen; cateIndx++) {
                if (categories[cateIndx] == arrayFilter[filtIndx]) {
                    matchAct = true;
                }
            }
            if (matchAct) {//Modified by Yuvapriya to avoid duplicate items in trip preferrences...
                var chkid = activities_forsort[actIndx].id;

                if (tmpactivities_forsort.length == 0) {
                    tmpactivities_forsort.push(activities_forsort[actIndx]);
                }
                else {
                    var cnt = 0;
                    for (var i = 0, iLen = tmpactivities_forsort.length; i < iLen; i++) {

                        if (tmpactivities_forsort[i].id == activities_forsort[actIndx].id) {
                            cnt += 1;
                        }
                    }
                    if (cnt == 0) {
                        tmpactivities_forsort.push(activities_forsort[actIndx]);
                    }
                }
            }//end...
        }
    }
    if (arrayFilter.length > 0) {
        activities_forsort = tmpactivities_forsort.slice(0);
    }

    activities_foradd=activities_forsort.slice();
    var actdiv = "";
    for (var loopvar = 0, ActivitiesLength = activities_forsort.length; loopvar < ActivitiesLength; loopvar++) {
        var ltlng = activities_forsort[loopvar].latLng.split(',');
        actdiv += "<aside class='resort-row' id='res_" + activities_forsort[loopvar].id + "'><div class='resort-row1' id='divres_" + activities_forsort[loopvar].id + "'>";
        actdiv += "<input type='hidden' class='hide'  id='hide" + activities_forsort[loopvar].id + "'><input type='hidden' id='latarray" + activities_forsort[loopvar].id + "' value='" + ltlng[0] + "' class='reslat'><input type='hidden' id='lngarray" + activities_forsort[loopvar].id + "' value='" + ltlng[1] + "' class='reslng'>";
        actdiv += "<input type='hidden' id='isME" + activities_forsort[loopvar].id + "' value='0' class='resME'>";
        if (activities_forsort[loopvar].rating != "") {
            actdiv += "<input type='hidden' id='rates_" + activities_forsort[loopvar].id + "' value='" + activities_forsort[loopvar].rating + "'>";
        }
        else {
            actdiv += "<input type='hidden' id='rates_" + activities_forsort[loopvar].id + "' value='0'>";
        }
        actdiv += "<figure class='resort-img'><div class='tooltip'>Drag to add</div>";
        //if (activities_forsort[loopvar].Mnual == "1") {
        //    actdiv += "<img src='" + activities_forsort[loopvar].imageUrl + "' alt='' />";
        //}
        //else {
            actdiv += "<img src='" + activities_forsort[loopvar].imageUrl + "' alt='' />";
        //}
            actdiv += "<span>From <strong id='s" + activities_forsort[loopvar].id + "'>" + activities_forsort[loopvar].fromPrice + "</strong></span></figure>";
        actdiv += "<div class='resort-details'><div class='r-left'>";
        actdiv += "<h3 id='h" + activities_forsort[loopvar].id + "'>" + activities_forsort[loopvar].title + " </h3><br/>";
        actdiv += "<ul><b>Ratings : </b>";
        var rating = (parseFloat(activities_forsort[loopvar].recommendationScore) / 20).toFixed(1);
        for (var loopvar_rate = 1; loopvar_rate <= 5; loopvar_rate++) {
            if (rating >= loopvar_rate) {
                actdiv += "<li><img src='../Content/images/tennisball_full.png' /></li>";
            }
            else if (rating > (loopvar_rate - 1) && rating < loopvar_rate) {
                actdiv += "<li><img src='../Content/images/tennisball_half.png' /></li>";
            }
            else {
                actdiv += "<li><img src='../Content/images/tennisball_grey_full.png' /></li>";
            }
        }
        actdiv += "<li>(" + rating + ")</li></ul>";
        actdiv += "<p>Recommendation Score : " + activities_forsort[loopvar].recommendationScore + "</p>";
        if (activities_forsort[loopvar].shortDescription && activities_forsort[loopvar].shortDescription != null) {
            actdiv += "<p id='pho_" + activities_forsort[loopvar].id + "' style='height:40px; overflow-y:hidden;'>" + activities_forsort[loopvar].shortDescription + "</p>";
        }
        if (activities_forsort[loopvar].duration && activities_forsort[loopvar].duration != null) {
            actdiv += "<p id='pho_" + activities_forsort[loopvar].id + "' style='height:20px; overflow-y:hidden;'>" + activities_forsort[loopvar].duration + "</p>";
        }
        var strId = activities_forsort[loopvar].id.toString();
        actdiv += "<div class='book-add'><span><a href='#' id='inner" + activities_forsort[loopvar].id + "' class='add-trip'  onclick='return addtoleft(\"" + strId + "\")'>Add Trip</a></span>";
        actdiv += "<span id='" + activities_forsort[loopvar].id + "' style='display:none;'><a href='#' class='Actbook'>Book</a></span>";
        actdiv += "</div></div>";
        actdiv += "<div class='r-right'>";
        actdiv += "<ul><li><a href='#'><img src='../Content/images/del-icon.png' alt='' class='del-show' onclick=''/></a></li>";
        actdiv += "<li><a  class='fancybox' href='#infowindow_hotels_activities' onclick='getActivityInfo(\"" + activities_forsort[loopvar].id + "\",\"" + selectedCityis + "\");'><img src='../Content/images/info-icon.png' alt='' class='example1demo' /></a></li></ul>";
        actdiv += "</div></div></div>";
        actdiv += "</aside>";        
    }
    $('#rhaa1').append(actdiv);
    if(!activities_forsort.length)
    {
        $('#rhaa1').html('No Results found..')
    }
    else
    {
        intheTripRHAA("Activities");
    }
}

function Displayall() {
    
    $('#RightSidePart').block({
        message: 'Please wait while data is loading...',
        css: {
            border: 'none',
            padding: '15px',
            float: 'right',
            'margin-right': '0%',
            backgroundColor: '#033363',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        }
    });
    checkidarray = [], checknamearray = [];
    var attraction_pref = [];
    attraction_pref.push("aquarium", "place_of_worship", "museum", "art_gallery", "amusement_park", "park");
    var AttractionOptions = attraction_pref.length;
    var attraction_pref1 = [];    
    attraction_pref1.push("palace", "botanical garden", "stadium", "bridge", "cathedral", "museum", "beach", "island", "zoo", "square", "statue", "harbour", "pier", "tower", "historical building");
    var AttractionOptions1 = attraction_pref1.length;
    touristele = 0;
    var tmpLengthAttr = Math.ceil(AttractionOptions / 6);
    CheckArraySort = tmpLengthAttr + Math.ceil(AttractionOptions1 / 5);
    var increCnt = 0;
    var newarrayTmp = "";
    for (var loopvar = 0; loopvar < AttractionOptions; loopvar += 6) {
        increCnt++;
        if (increCnt < tmpLengthAttr || AttractionOptions % 6 == 0) {
            newarrayTmp = attraction_pref[loopvar + 5] + "|" + attraction_pref[loopvar + 4] + "|" + attraction_pref[loopvar + 3] + "|" + attraction_pref[loopvar + 2] + "|" + attraction_pref[loopvar + 1] + "|" + attraction_pref[loopvar];
            initialize(newarrayTmp);
        }
    }
    increCnt = 0;    
    newarrayTmp = "";
    tmpLengthAttr = Math.ceil(AttractionOptions1 / 5);
    for (var loopvar = 0; loopvar < AttractionOptions1; loopvar += 5) {
        increCnt++;
        if (increCnt < tmpLengthAttr || AttractionOptions1 % 5 == 0) {
            newarrayTmp = "(" + attraction_pref1[loopvar + 4] + " OR " + attraction_pref1[loopvar + 3] + " OR " + attraction_pref1[loopvar + 2] + " OR " + attraction_pref1[loopvar + 1] + " OR " + attraction_pref1[loopvar] + ")";
            initialize_call_withkeyword(newarrayTmp);
        }
    }
}

function Displayallrestaurant() {
    $('#RightSidePart').block({
        message: 'Please wait while data is loading...',
        css: {
            border: 'none',
            padding: '15px',
            float: 'right',
            'margin-right': '0%',
            backgroundColor: '#033363',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        }
    });
    cuisineele = 0;
    var newarrayTmp = "";
    restaurant.length = [];
    cuisinetype1 = [];
    checkidarray = [], checknamearray = [];
    restaurant.push("mediterranean", "european", "mexican", "american", "french", "spanish", "italian", "indian", "japanese", "chinese", "thai", "asian", "breakfast", "pizza", "dessert", "bakery");
    var RestaurantsOptions = restaurant.length;
    var tmpLengthRest = Math.ceil(RestaurantsOptions / 4);
    CheckArraySort = tmpLengthRest;
    var increCnt = 0;
    for (var loopvar_rest = 0; loopvar_rest < RestaurantsOptions; loopvar_rest+=4) {
        increCnt++;
        if (increCnt < tmpLengthRest || RestaurantsOptions % 4 == 0) {
            newarrayTmp = "(" + restaurant[loopvar_rest + 3] + " OR " + restaurant[loopvar_rest + 2] + " OR " + restaurant[loopvar_rest + 1] + " OR " + restaurant[loopvar_rest] + ")";
            LoadRestaurant(newarrayTmp);
        }
    }
}

function initialize(attractiontype) {
    var tmpAttractions = [];
    var setFlag = false;
    var foundFlag = false;
    var someFlag = false;
    for (var loopvar = 0, LengthAttr = ListAttractions.length; loopvar < LengthAttr; loopvar++) {
        if (ListAttractions[loopvar].attrType == attractiontype && ListAttractions[loopvar].cityname == $('#selectedcity').html()) {
            tmpAttractions = (ListAttractions[loopvar].attractions).slice(0);
            setFlag = true;
            break;
        }
    }
    if (setFlag) {
        ++touristele;
        var arrTmp = [];
        for (var AttractionsResult = 0, ResultsLength = tmpAttractions.length; AttractionsResult < ResultsLength; AttractionsResult++) {
            foundFlag = false;
            for (var findLoop = 0, ListLength = attrDetails.length; findLoop < ListLength; findLoop++)
            {
                if(tmpAttractions[AttractionsResult].place_id==attrDetails[findLoop].plc_id && tmpAttractions[AttractionsResult].rating)
                {
                    var needsExclusions = [];
                    needsExclusions = tmpAttractions[AttractionsResult].types;
                    someFlag = true;
                    for (var index1 = 0; index1 < excludeMe.length; index1++) {
                        if (needsExclusions.indexOf(excludeMe[index1]) !== -1) {
                            someFlag = false;
                            break;
                        }
                    }
                    if (someFlag) {
                        if ((tmpAttractions[AttractionsResult].name).toLowerCase().indexOf("college") >= 0) {
                            someFlag = false;
                        }
                    }
                    if (someFlag) {
                        if (attrDetails[findLoop].details) {
                            arrTmp = attrDetails[findLoop].details;
                            foundFlag = true;
                            break;
                        }
                    }
                }
            }
            if (foundFlag) {
                addResul(arrTmp, 0, touristtype1[touristele - 1]);
                allattractions_toleft.push(arrTmp[0]);
            }
        }
        if (touristele == CheckArraySort) {
            SortRestaurant($('#show_hide_attrsort').find('.selectContainerStyled').find('.select_box').val(), '2');
        }
    }
    else {
        touristtype1.push(attractiontype);
        $('#info').html("");
        var pyrmont = new google.maps.LatLng($('#latarray').val(), $('#lngarray').val());
        map = new google.maps.Map(document.getElementById('map-canvas'), {
            center: pyrmont,
            zoom: 15
        });
        var request = {
            location: pyrmont,
            radius: 20000,
            rankby: ['prominence'],
            types: [attractiontype]
        };
        infowindow = new google.maps.InfoWindow();
        var service = new google.maps.places.PlacesService(map);
        service.nearbySearch(request, function (response, status) { callbac(response, status, attractiontype) });
    }
}

function initialize_call_withkeyword(attractiontype) {
    var tmpAttractions = [];
    var setFlag = false;
    var someFlag = false;
    var foundFlag = false;
    for (var loopvar = 0, LengthAttr = ListAttractions.length; loopvar < LengthAttr; loopvar++) {
        if (ListAttractions[loopvar].attrType == attractiontype && ListAttractions[loopvar].cityname == $('#selectedcity').html()) {
            tmpAttractions = (ListAttractions[loopvar].attractions).slice(0);
            setFlag = true;
            break;
        }
    }
    if (setFlag) {
        ++touristele;
        var arrTmp = [];
        for (var AttractionsResult = 0, ResultsLength = tmpAttractions.length; AttractionsResult < ResultsLength; AttractionsResult++) {
            foundFlag = false;
            for (var findLoop = 0, ListLength = attrDetails.length; findLoop < ListLength; findLoop++) {
                if (tmpAttractions[AttractionsResult].place_id == attrDetails[findLoop].plc_id && tmpAttractions[AttractionsResult].rating) {
                    var needsExclusions = [];
                    needsExclusions = tmpAttractions[AttractionsResult].types;
                    someFlag = true;
                    for (var index1 = 0; index1 < excludeMe.length; index1++) {
                        if (needsExclusions.indexOf(excludeMe[index1]) !== -1) {
                            someFlag = false;
                            break;
                        }
                    }
                    if (someFlag) {
                        if ((tmpAttractions[AttractionsResult].name).toLowerCase().indexOf("college") >= 0) {
                            someFlag = false;
                        }
                    }
                    if (someFlag) {
                        if (attrDetails[findLoop].details) {
                            arrTmp = attrDetails[findLoop].details;
                            foundFlag = true;
                            break;
                        }
                    }
                }
            }
            if (foundFlag) {
                    addResul(arrTmp, 0, touristtype1[touristele - 1]);
                    allattractions_toleft.push(arrTmp[0]);
            }
        }
        if (touristele == CheckArraySort) {
            //setTimeout(function () { SortRestaurant($('#show_hide_attrsort').find('.selectContainerStyled').find('.select_box').val(), '2'); }, 2000);
            SortRestaurant($('#show_hide_attrsort').find('.selectContainerStyled').find('.select_box').val(), '2');
        }
    }
    else {
        touristtype1.push(attractiontype);
        $('#info').html("");
        var pyrmont = new google.maps.LatLng($('#latarray').val(), $('#lngarray').val());
        map = new google.maps.Map(document.getElementById('map-canvas'), {
            center: pyrmont,
            zoom: 15
        });
        var request = {
            location: pyrmont,
            radius: 20000,
            rankby: ['prominence'],
            keyword: [attractiontype]
        };
        infowindow = new google.maps.InfoWindow();
        var service = new google.maps.places.PlacesService(map);
        service.nearbySearch(request, function (response, status) { callbac(response, status, attractiontype) });
    }
}

function callbac(results, status, attractiontype) {
    ++touristele;
    if (status == google.maps.places.PlacesServiceStatus.OK) {
        var needsExclusions = [];
        ListAttractions.push({
            attrType: attractiontype,
            attractions: results,
            cityname: $('#selectedcity').html()
        });
        for (var indexValue = 0, ResultsLength = results.length; indexValue < ResultsLength; indexValue++) {
                needsExclusions = results[indexValue].types;
                someFlag = true;
                for (var index1 = 0; index1 < excludeMe.length; index1++) {
                    if (needsExclusions.indexOf(excludeMe[index1]) !== -1) {
                        someFlag = false;
                        break;
                    }
                }
                if (someFlag) {
                    if ((results[indexValue].name).toLowerCase().indexOf("college") >= 0) {
                        someFlag = false;
                    }
                }
                if (someFlag) {
                    if (results[indexValue].rating) {
                        if (results[indexValue].rating >= 1) {
                            AllAttrColSugPart.push({
                                plc:results[indexValue],
                                attrType:attractiontype
                            });
                            attrDetails.push({
                                plc_id: results[indexValue].place_id,
                                attrType: attractiontype,
                                details: results[indexValue]
                            });
                        }
                    }
                }
        }
    }  
    if (touristele == CheckArraySort) {
        TotalAttrs_SugPart = AllAttrColSugPart.length;
        SortRestaurant($('#show_hide_attrsort').find('.selectContainerStyled').find('.select_box').val(), '2');
        //GetAllAttrDetOnSuggest();
    }
}

function GetAllAttrDetOnSuggest()
{
    if (TotalAttrs_SugPart == incrAttrs_SugPart) {
        //setTimeout(function () { SortRestaurant($('#show_hide_attrsort').find('.selectContainerStyled').find('.select_box').val(), '2'); }, 2000);
        SortRestaurant($('#show_hide_attrsort').find('.selectContainerStyled').find('.select_box').val(), '2');
    }
    else
    {
        var plcId = AllAttrColSugPart[incrAttrs_SugPart].plc.place_id;
        var attractiontype = AllAttrColSugPart[incrAttrs_SugPart].attrType;
        var request = {
            placeId: plcId
        };
        var service = new google.maps.places.PlacesService(map);
        service.getDetails(request, function (place, status) {
            if (status == google.maps.places.PlacesServiceStatus.OK) {
                if (place.user_ratings_total) {
                    var userreview = place.user_ratings_total;
                    for (var indxVal = 0, attrDetVal = attrDetails.length; indxVal < attrDetVal; indxVal++) {
                        if (attrDetails[indxVal].plc_id == place.place_id) {
                            attrDetails[indxVal].UserReviews = userreview;
                            attrDetails[indxVal].details = place;
                            break;
                        }
                    }
                    addResul(place, 0, attractiontype);
                    allattractions_toleft.push(place);
                }
                incrAttrs_SugPart++;
                GetAllAttrDetOnSuggest();
            }
            else
            {
                if (status == google.maps.GeocoderStatus.OVER_QUERY_LIMIT) {
                    sleep(1000);
                    GetAllAttrDetOnSuggest();
                }
                else {
                    incrAttrs_SugPart++;
                    GetAllAttrDetOnSuggest();
                }
            }
        });
    }
}

function addResul(place, indexValue, types) {
    if ((jQuery.inArray(place.place_id, checkidarray) !== -1) || (jQuery.inArray(place.name, checknamearray) !== -1)) {
    }
    else {
        if (jQuery.inArray(place.place_id, leftsidearray) !== -1) {
            var value = $.isArray(place.photos, place);
            if (value == true) {
                var rating = Math.round(place.rating);
                var items = '';
                if (place.rating) {
                    rating = place.rating;
                    var ratefilter = parseInt(place.rating);
                    items += "<div id='rh" + place.place_id + "'></div><input type='hidden' class='hide'  id=" + place.place_id + " value='" + types + "'><input type='hidden' id='rates_" + place.place_id + "' value='" + rating + "'><input type='hidden' id='ratefilter_" + place.place_id + "' value='" + ratefilter + "'><input type='hidden' id='latarray" + place.place_id + "' value='" + place.geometry.location.lat() + "'><input type='hidden' id='lngarray" + place.place_id + "' value='" + place.geometry.location.lng() + "'><aside class='resort-row2' id='res_" + place.place_id + "'><div class='resort-row1' id='divres_" + place.place_id + "'>";
                    if (place.user_ratings_total) {
                        items += "<input type='hidden' class='reviews' value='" + place.user_ratings_total + "'/>";
                    }
                    else {
                        items += "<input type='hidden' class='reviews' value='No rev'/>";
                    }
                    if (place.price_level)
                    {
                        items += "<input type='hidden' class='priceLev' value='" + place.price_level + "'/>";
                    }
                    else
                    {
                        items += "<input type='hidden' class='priceLev' value='0'/>";
                    }
                    if (place.website) {
                        items += "<input type='hidden' class='webhidden' value='" + place.website + "'>";
                    }
                    else {
                        items += "<input type='hidden' class='webhidden' value='No site'>";
                    }
                    items += "<input type='hidden' id='latarray" + place.place_id + "' value='" + place.geometry.location.lat() + "' class='reslat'><input type='hidden' id='lngarray" + place.place_id + "' value='" + place.geometry.location.lng() + "' class='reslng'>";
                    items += "<input type='hidden' id='isME" + place.place_id + "' value='0' class='resME'>";
                    items += "<figure class='resort-img'><div class='tooltip'>Drag to add</div><img src='" + place.photos[0].getUrl({ 'maxWidth': 182, 'maxHeight': 120 }) + "' alt='' id='img_" + place.place_id + "' />";
                    items += "<span>Rating <strong id='s" + place.place_id + "'>" + place.rating + "</strong></span>";
                }
                else {
                    rating = 1;
                    var ratefilter = rating;
                    items += "<div id='rh" + place.place_id + "'></div><input type='hidden' class='hide'  id=" + place.place_id + " value='" + types + "'><input type='hidden' id='rates_" + place.place_id + "' value='1'><input type='hidden' id='ratefilter_" + place.place_id + "' value='" + ratefilter + "'><input type='hidden' id='latarray" + place.place_id + "' value='" + place.geometry.location.lat() + "'><input type='hidden' id='lngarray" + place.place_id + "' value='" + place.geometry.location.lng() + "'><aside class='resort-row2' id='res_" + place.place_id + "'><div class='resort-row1' id='divres_" + place.place_id + "'>";
                    if (place.user_ratings_total) {
                        items += "<input type='hidden' class='reviews' value='" + place.user_ratings_total + "'/>";
                    }
                    else {
                        items += "<input type='hidden' class='reviews' value='No rev'/>";
                    }
                    if (place.price_level) {
                        items += "<input type='hidden' class='priceLev' value='" + place.price_level + "'/>";
                    }
                    else {
                        items += "<input type='hidden' class='priceLev' value='0'/>";
                    }
                    if (place.website) {
                        items += "<input type='hidden' class='webhidden' value='"+place.website+"'>";
                    }
                    else
                    {
                        items += "<input type='hidden' class='webhidden' value='No site'>";
                    }
                    items += "<input type='hidden' id='latarray" + place.place_id + "' value='" + place.geometry.location.lat() + "' class='reslat'><input type='hidden' id='lngarray" + place.place_id + "' value='" + place.geometry.location.lng() + "' class='reslng'>";
                    items += "<input type='hidden' id='isME" + place.place_id + "' value='0' class='resME'>";
                    items += "<figure class='resort-img'><div class='tooltip'>Drag to add</div><img src='" + place.photos[0].getUrl({ 'maxWidth': 182, 'maxHeight': 120 }) + "' alt='' id='img_" + place.place_id + "' />";
                }
                items += "</figure>";
                items += "<div class='resort-details'><div class='r-left'>";
                items += "<h3 id='h" + place.place_id + "'>" + place.name + "  </h3><br/>";
                items += "<p style='height:40px; overflow-y:hidden;' id='p" + place.place_id + "'>" + place.vicinity + "</p>";
                if (place.website) {
                    items += "<p style='height:18px; overflow-y:hidden;width:100%;text-overflow: ellipsis;white-space: nowrap;overflow: hidden;' id='web" + place.place_id + "'><b>Website : </b><a target='_blank' href='" + place.website + "' style='text-decoration: underline;'>" + place.website + "</a></p>";
                }
                items += "<ul id='rat" + place.place_id + "'><b>Ratings : </b>";
                for (var loopvar_rate = 1; loopvar_rate <= 5; loopvar_rate++) {
                    if (rating >= loopvar_rate) {
                        items += "<li><img src='../Content/images/tower_full.png' /></li>";
                    }
                    else if (rating > (loopvar_rate - 1) && rating < loopvar_rate) {
                        items += "<li><img src='../Content/images/tower_half.png' /></li>";
                    }
                    else {
                        items += "<li><img src='../Content/images/tower_grey_full.png' /></li>";
                    }
                }
                //items += "<li>(" + place.user_ratings_total + " reviews)</li></ul>";
                if (place.price_level) {
                    if (place.price_level == 0) {
                        items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b> $</p> ";
                    }
                    else if (place.price_level == 1) {
                        items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b> $</p> ";
                    }
                    else if (place.price_level == 2) {
                        items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b>  $$</p> ";
                    }
                    else if (place.price_level == 3) {
                        items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b>  $$$</p> ";
                    }
                    else if (place.price_level == 4) {
                        items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b>  $$$$</p> ";
                    }
                }
                else {
                    items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b> $</p> ";
                }
                items += "<div class='book-add'><span><a href='#' id='inner" + place.place_id + "' class='add-trip' onclick='return AttractionToleft(\"" + place.place_id + "\");'>In the trip</a></span></div></div> ";
                items += "<div class='r-right'>";
                items += " <ul><li><a href='#'><img src='../Content/images/del-icon.png' alt='' class='del-show' onclick='fun2'/></a></li>";
                items += "<li><a  class='fancybox' href='#infowindow' onclick='return info(\"" + place.place_id + "\",\"" + place.geometry.location.lat() + "\",\"" + place.geometry.location.lng() + "\",\"" + types + "\",2);'><img src='../Content/images/info-icon.png' alt='' class='example1demo' /></a></li></ul>";
                items += "</div></div></div>";
                items += "</aside>";
                checknamearray.push(place.name);
                checkidarray.push(place.place_id);
                var appendcount = 0;
                var Count = $('#rhaa1').find('.resort-row').length;
                if (Count != 0) {
                    for (var ResortRowCount = 0; ResortRowCount < Count; ResortRowCount++) {
                        var id = $('#rhaa1').find('.resort-row').eq(ResortRowCount).attr("id");
                        id = id.split("res_");
                        if (appendcount == 0) {
                            if (rating >= $('#rates_' + id[1]).val()) {
                                ++appendcount;
                                $('#rh' + id[1]).before(items);
                            }
                        }
                    }
                    if (appendcount == 0) {
                        $('#rhaa1').append(items);
                    }
                }
                else {
                    $('#rhaa1').append(items);
                }
            }
            else {
                var rating = Math.round(place.rating);
                var items = '';
                if (place.rating) {
                    rating = place.rating;
                    var ratefilter = parseInt(place.rating);
                    items += "<div id='rh" + place.place_id + "'></div><input type='hidden' class='hide'  id=" + place.place_id + " value='" + types + "'><input type='hidden' id='rates_" + place.place_id + "' value='" + rating + "'><input type='hidden' id='ratefilter_" + place.place_id + "' value='" + ratefilter + "'><input type='hidden' id='latarray" + place.place_id + "' value='" + place.geometry.location.lat() + "'><input type='hidden' id='lngarray" + place.place_id + "' value='" + place.geometry.location.lng() + "'><aside class='resort-row2' id='res_" + place.place_id + "'><div class='resort-row1' id='divres_" + place.place_id + "'>";
                    if (place.user_ratings_total) {
                        items += "<input type='hidden' class='reviews' value='" + place.user_ratings_total + "'/>";
                    }
                    else {
                        items += "<input type='hidden' class='reviews' value='No rev'/>";
                    }
                    if (place.price_level) {
                        items += "<input type='hidden' class='priceLev' value='" + place.price_level + "'/>";
                    }
                    else {
                        items += "<input type='hidden' class='priceLev' value='0'/>";
                    }
                    if (place.website) {
                        items += "<input type='hidden' class='webhidden' value='" + place.website + "'>";
                    }
                    else {
                        items += "<input type='hidden' class='webhidden' value='No site'>";
                    }
                    items += "<input type='hidden' id='latarray" + place.place_id + "' value='" + place.geometry.location.lat() + "' class='reslat'><input type='hidden' id='lngarray" + place.place_id + "' value='" + place.geometry.location.lng() + "' class='reslng'>";
                    items += "<input type='hidden' id='isME" + place.place_id + "' value='0' class='resME'>";
                    items += "<figure class='resort-img'><div class='tooltip'>Drag to add</div><img src='../Content/images/AppImages/attractions.jpg' alt='' id='img_" + place.place_id + "'  />";
                    items += "<span>Rating <strong id='s" + place.place_id + "'>" + place.rating + "</strong></span>";
                }
                else {
                    rating = 1;
                    var ratefilter = rating;
                    items += "<div id='rh" + place.place_id + "'></div><input type='hidden' class='hide'  id=" + place.place_id + " value='" + types + "'><input type='hidden' id='rates_" + place.place_id + "' value='1'><input type='hidden' id='ratefilter_" + place.place_id + "' value='" + ratefilter + "'><input type='hidden' id='latarray" + place.place_id + "' value='" + place.geometry.location.lat() + "'><input type='hidden' id='lngarray" + place.place_id + "' value='" + place.geometry.location.lng() + "'><aside class='resort-row2' id='res_" + place.place_id + "'><div class='resort-row1' id='divres_" + place.place_id + "'>";
                    if (place.user_ratings_total) {
                        items += "<input type='hidden' class='reviews' value='" + place.user_ratings_total + "'/>";
                    }
                    else {
                        items += "<input type='hidden' class='reviews' value='No rev'/>";
                    }
                    if (place.price_level) {
                        items += "<input type='hidden' class='priceLev' value='" + place.price_level + "'/>";
                    }
                    else {
                        items += "<input type='hidden' class='priceLev' value='0'/>";
                    }
                    if (place.website) {
                        items += "<input type='hidden' class='webhidden' value='" + place.website + "'>";
                    }
                    else {
                        items += "<input type='hidden' class='webhidden' value='No site'>";
                    }
                    items += "<input type='hidden' id='latarray" + place.place_id + "' value='" + place.geometry.location.lat() + "' class='reslat'><input type='hidden' id='lngarray" + place.place_id + "' value='" + place.geometry.location.lng() + "' class='reslng'>";
                    items += "<input type='hidden' id='isME" + place.place_id + "' value='0' class='resME'>";
                    items += "<figure class='resort-img'><div class='tooltip'>Drag to add</div><img src='../Content/images/AppImages/attractions.jpg' alt='' id='img_" + place.place_id + "'  />";
                }
                items += "</figure>";
                items += "<div class='resort-details'><div class='r-left'>";
                items += "<h3 id='h" + place.place_id + "'>" + place.name + "  </h3><br/>";
                items += "<p style='height:40px; overflow-y:hidden;' id='p" + place.place_id + "'>" + place.vicinity + "</p>";
                if (place.website) {
                    items += "<p style='height:18px; overflow-y:hidden;width:100%;text-overflow: ellipsis;white-space: nowrap;overflow: hidden;' id='web" + place.place_id + "'><b>Website : </b><a target='_blank' href='" + place.website + "' style='text-decoration: underline;'>" + place.website + "</a></p>";
                }
                items += "<ul id='rat" + place.place_id + "'><b>Ratings : </b>";
                for (var loopvar_rate = 1; loopvar_rate <= 5; loopvar_rate++) {
                    if (rating >= loopvar_rate) {
                        items += "<li><img src='../Content/images/tower_full.png' /></li>";
                    }
                    else if (rating > (loopvar_rate - 1) && rating < loopvar_rate) {
                        items += "<li><img src='../Content/images/tower_half.png' /></li>";
                    }
                    else {
                        items += "<li><img src='../Content/images/tower_grey_full.png' /></li>";
                    }
                }
                //items += "<li>(" + place.user_ratings_total + " reviews)</li></ul>";
                if (place.price_level) {
                    if (place.price_level == 0) {
                        items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b>$</p>";
                    }
                    else if (place.price_level == 1) {
                        items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b>$</p>";
                    }
                    else if (place.price_level == 2) {
                        items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b>$$</p>";
                    }
                    else if (place.price_level == 3) {
                        items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b>$$$</p>";
                    }
                    else if (place.price_level == 4) {
                        items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b>$$$$</p>";
                    }
                }
                else {
                    items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b>$</p> ";
                }
                items += "<div class='book-add'><span><a href='#' id='inner" + place.place_id + "' class='add-trip' onclick='return AttractionToleft(\"" + place.place_id + "\");'>In the trip</a></span></div></div> ";
                items += "<div class='r-right'>";
                items += " <ul><li><a href='#'><img src='../Content/images/del-icon.png' alt='' class='del-show' onclick='fun2'/></a></li>";
                items += "<li><a  class='fancybox' href='#infowindow' onclick='return info(\"" + place.place_id + "\",\"" + place.geometry.location.lat() + "\",\"" + place.geometry.location.lng() + "\",\"" + types + "\",2);'><img src='../Content/images/info-icon.png' alt='' class='example1demo' /></a></li></ul>";
                items += "</div></div></div>";
                items += "</aside>";
                checknamearray.push(place.name);
                checkidarray.push(place.place_id);
                var appendcount = 0;
                var Count = $('#rhaa1').find('.resort-row').length;
                if (Count != 0) {
                    for (var ResortRowCount = 0; ResortRowCount < Count; ResortRowCount++) {
                        var id = $('#rhaa1').find('.resort-row').eq(ResortRowCount).attr("id");
                        id = id.split("res_");
                        if (appendcount == 0) {
                            if (rating >= $('#rates_' + id[1]).val()) {
                                ++appendcount;
                                $('#rh' + id[1]).before(items);
                            }
                        }
                    }
                    if (appendcount == 0) {
                        $('#rhaa1').append(items);
                    }
                }
                else {
                    $('#rhaa1').append(items);
                }
            }
        }
        else {
            var value = $.isArray(place.photos, place);
            if (value == true) {
                var rating = Math.round(place.rating);
                var items = '';
                if (place.rating) {
                    rating = place.rating;
                    var ratefilter = parseInt(place.rating);
                    items += "<div id='rh" + place.place_id + "'></div><input type='hidden' class='hide'  id=" + place.place_id + " value='" + types + "'><input type='hidden' id='rates_" + place.place_id + "' value='" + rating + "'><input type='hidden' id='ratefilter_" + place.place_id + "' value='" + ratefilter + "'><input type='hidden' id='latarray" + place.place_id + "' value='" + place.geometry.location.lat() + "'><input type='hidden' id='lngarray" + place.place_id + "' value='" + place.geometry.location.lng() + "'><aside class='resort-row' id='res_" + place.place_id + "'><div class='resort-row1' id='divres_" + place.place_id + "'>";
                    if (place.user_ratings_total) {
                        items += "<input type='hidden' class='reviews' value='" + place.user_ratings_total + "'/>";
                    }
                    else {
                        items += "<input type='hidden' class='reviews' value='No rev'/>";
                    }
                    if (place.price_level) {
                        items += "<input type='hidden' class='priceLev' value='" + place.price_level + "'/>";
                    }
                    else {
                        items += "<input type='hidden' class='priceLev' value='0'/>";
                    }
                    if (place.website) {
                        items += "<input type='hidden' class='webhidden' value='" + place.website + "'>";
                    }
                    else {
                        items += "<input type='hidden' class='webhidden' value='No site'>";
                    }
                    items += "<input type='hidden' id='latarray" + place.place_id + "' value='" + place.geometry.location.lat() + "' class='reslat'><input type='hidden' id='lngarray" + place.place_id + "' value='" + place.geometry.location.lng() + "' class='reslng'>";
                    items += "<input type='hidden' id='isME" + place.place_id + "' value='0' class='resME'>";
                    items += "<figure class='resort-img'><div class='tooltip'>Drag to add</div><img src='" + place.photos[0].getUrl({ 'maxWidth': 182, 'maxHeight': 120 }) + "' alt='' id='img_" + place.place_id + "' />";
                }
                else {
                    rating = 1;
                    var ratefilter = rating;
                    items += "<div id='rh" + place.place_id + "'></div><input type='hidden' class='hide'  id=" + place.place_id + " value='" + types + "'><input type='hidden' id='rates_" + place.place_id + "' value='1'><input type='hidden' id='ratefilter_" + place.place_id + "' value='" + ratefilter + "'><input type='hidden' id='latarray" + place.place_id + "' value='" + place.geometry.location.lat() + "'><input type='hidden' id='lngarray" + place.place_id + "' value='" + place.geometry.location.lng() + "'><aside class='resort-row' id='res_" + place.place_id + "'><div class='resort-row1' id='divres_" + place.place_id + "'>";
                    if (place.user_ratings_total) {
                        items += "<input type='hidden' class='reviews' value='" + place.user_ratings_total + "'/>";
                    }
                    else {
                        items += "<input type='hidden' class='reviews' value='No rev'/>";
                    }
                    if (place.price_level) {
                        items += "<input type='hidden' class='priceLev' value='" + place.price_level + "'/>";
                    }
                    else {
                        items += "<input type='hidden' class='priceLev' value='0'/>";
                    }
                    if (place.website) {
                        items += "<input type='hidden' class='webhidden' value='" + place.website + "'>";
                    }
                    else {
                        items += "<input type='hidden' class='webhidden' value='No site'>";
                    }
                    items += "<input type='hidden' id='latarray" + place.place_id + "' value='" + place.geometry.location.lat() + "' class='reslat'><input type='hidden' id='lngarray" + place.place_id + "' value='" + place.geometry.location.lng() + "' class='reslng'>";
                    items += "<input type='hidden' id='isME" + place.place_id + "' value='0' class='resME'>";
                    items += "<figure class='resort-img'><div class='tooltip'>Drag to add</div><img src='" + place.photos[0].getUrl({ 'maxWidth': 182, 'maxHeight': 120 }) + "' alt='' id='img_" + place.place_id + "' />";
                }
                items += "</figure>";
                items += "<div class='resort-details'><div class='r-left'>";
                items += "<h3 id='h" + place.place_id + "'>" + place.name + "  </h3><br/>";
                items += "<p style='height:40px; overflow-y:hidden;' id='p" + place.place_id + "'>" + place.vicinity + "</p>";
                if (place.website) {
                    items += "<p style='height:18px; overflow-y:hidden;width:100%;text-overflow: ellipsis;white-space: nowrap;overflow: hidden;' id='web" + place.place_id + "'><b>Website : </b><a target='_blank' href='" + place.website + "' style='text-decoration: underline;'>" + place.website + "</a></p>";
                }
                items += "<ul id='rat" + place.place_id + "'><b>Ratings : </b>";
                for (var loopvar_rate = 1; loopvar_rate <= 5; loopvar_rate++) {
                    if (rating >= loopvar_rate) {
                        items += "<li><img src='../Content/images/tower_full.png' /></li>";
                    }
                    else if (rating > (loopvar_rate - 1) && rating < loopvar_rate) {
                        items += "<li><img src='../Content/images/tower_half.png' /></li>";
                    }
                    else {
                        items += "<li><img src='../Content/images/tower_grey_full.png' /></li>";
                    }
                }
                //items += "<li>(" + place.user_ratings_total + " reviews)</li></ul>";
                if (place.price_level) {
                    if (place.price_level == 0) {
                        items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b>$</p>";
                    }
                    else if (place.price_level == 1) {
                        items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b>$</p>";
                    }
                    else if (place.price_level == 2) {
                        items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b>$$</p>";
                    }
                    else if (place.price_level == 3) {
                        items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b>$$$</p>";
                    }
                    else if (place.price_level == 4) {
                        items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b>$$$$</p>";
                    }
                }
                else {
                    items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b>$</p>";
                }
                items += "<div class='book-add'><span><a href='#' id='inner" + place.place_id + "' class='add-trip' onclick='return AttractionToleft(\"" + place.place_id + "\");'>Add Trip</a></span></div></div> ";
                items += "<div class='r-right'>";
                items += " <ul><li><a href='#'><img src='../Content/images/del-icon.png' alt='' class='del-show' onclick='fun2'/></a></li>";
                items += "<li><a  class='fancybox' href='#infowindow' onclick='return info(\"" + place.place_id + "\",\"" + place.geometry.location.lat() + "\",\"" + place.geometry.location.lng() + "\",\"" + types + "\",2);'><img src='../Content/images/info-icon.png' alt='' class='example1demo' /></a></li></ul>";
                items += "</div></div></div>";
                items += "</aside>";
                checknamearray.push(place.name);
                checkidarray.push(place.place_id);
                var appendcount = 0;
                var Count = $('#rhaa1').find('.resort-row').length;
                if (Count != 0) {
                    for (var ResortRowCount = 0; ResortRowCount < Count; ResortRowCount++) {
                        var id = $('#rhaa1').find('.resort-row').eq(ResortRowCount).attr("id");
                        id = id.split("res_");
                        if (appendcount == 0) {
                            if (rating >= $('#rates_' + id[1]).val()) {
                                ++appendcount;
                                $('#rh' + id[1]).before(items);
                            }
                        }
                    }
                    if (appendcount == 0) {
                        $('#rhaa1').append(items);
                    }
                }
                else {
                    $('#rhaa1').append(items);
                }
            }
            else {
                var rating = Math.round(place.rating);
                var items = '';
                if (place.rating) {
                    rating = place.rating;
                    var ratefilter = parseInt(place.rating);
                    items += "<div id='rh" + place.place_id + "'></div><input type='hidden' class='hide'  id=" + place.place_id + " value='" + types + "'><input type='hidden' id='rates_" + place.place_id + "' value='" + rating + "'><input type='hidden' id='ratefilter_" + place.place_id + "' value='" + ratefilter + "'><input type='hidden' id='latarray" + place.place_id + "' value='" + place.geometry.location.lat() + "'><input type='hidden' id='lngarray" + place.place_id + "' value='" + place.geometry.location.lng() + "'><aside class='resort-row' id='res_" + place.place_id + "'><div class='resort-row1' id='divres_" + place.place_id + "'>";
                    if (place.user_ratings_total) {
                        items += "<input type='hidden' class='reviews' value='" + place.user_ratings_total + "'/>";
                    }
                    else {
                        items += "<input type='hidden' class='reviews' value='No rev'/>";
                    }
                    if (place.price_level) {
                        items += "<input type='hidden' class='priceLev' value='" + place.price_level + "'/>";
                    }
                    else {
                        items += "<input type='hidden' class='priceLev' value='0'/>";
                    }
                    if (place.website) {
                        items += "<input type='hidden' class='webhidden' value='" + place.website + "'>";
                    }
                    else {
                        items += "<input type='hidden' class='webhidden' value='No site'>";
                    }
                    items += "<input type='hidden' id='latarray" + place.place_id + "' value='" + place.geometry.location.lat() + "' class='reslat'><input type='hidden' id='lngarray" + place.place_id + "' value='" + place.geometry.location.lng() + "' class='reslng'>";
                    items += "<input type='hidden' id='isME" + place.place_id + "' value='0' class='resME'>";
                    items += "<figure class='resort-img'><div class='tooltip'>Drag to add</div><img src='../Content/images/AppImages/attractions.jpg' alt='' id='img_" + place.place_id + "' />";
                    items += "<span>Rating <strong id='s" + place.place_id + "'>" + place.rating + "</strong></span>";
                }
                else {
                    rating = 1;
                    var ratefilter = rating;
                    items += "<div id='rh" + place.place_id + "'></div><input type='hidden' class='hide'  id=" + place.place_id + " value='" + types + "'><input type='hidden' id='rates_" + place.place_id + "' value='1'><input type='hidden' id='ratefilter_" + place.place_id + "' value='" + ratefilter + "'><input type='hidden' id='latarray" + place.place_id + "' value='" + place.geometry.location.lat() + "'><input type='hidden' id='lngarray" + place.place_id + "' value='" + place.geometry.location.lng() + "'><aside class='resort-row' id='res_" + place.place_id + "'><div class='resort-row1' id='divres_" + place.place_id + "'>";
                    if (place.user_ratings_total) {
                        items += "<input type='hidden' class='reviews' value='" + place.user_ratings_total + "'/>";
                    }
                    else {
                        items += "<input type='hidden' class='reviews' value='No rev'/>";
                    }
                    if (place.price_level) {
                        items += "<input type='hidden' class='priceLev' value='" + place.price_level + "'/>";
                    }
                    else {
                        items += "<input type='hidden' class='priceLev' value='0'/>";
                    }
                    if (place.website) {
                        items += "<input type='hidden' class='webhidden' value='" + place.website + "'>";
                    }
                    else {
                        items += "<input type='hidden' class='webhidden' value='No site'>";
                    }
                    items += "<input type='hidden' id='latarray" + place.place_id + "' value='" + place.geometry.location.lat() + "' class='reslat'><input type='hidden' id='lngarray" + place.place_id + "' value='" + place.geometry.location.lng() + "' class='reslng'>";
                    items += "<input type='hidden' id='isME" + place.place_id + "' value='0' class='resME'>";
                    items += "<figure class='resort-img'><div class='tooltip'>Drag to add</div><img src='../Content/images/AppImages/attractions.jpg' alt='' id='img_" + place.place_id + "' />";
                    items += "<span>Rating <strong id='s" + place.place_id + "'>" + rating + "</strong></span>";
                }
                items += "</figure>";
                items += "<div class='resort-details'><div class='r-left'>";
                items += "<h3 id='h" + place.place_id + "'>" + place.name + "  </h3><br/>";
                items += "<p style='height:40px; overflow-y:hidden;' id='p" + place.place_id + "'>" + place.vicinity + "</p>";
                items += "<div class='book-add'><span><a href='#' id='inner" + place.place_id + "' class='add-trip' onclick='return AttractionToleft(\"" + place.place_id + "\");'>Add Trip</a></span></div></div> ";
                items += "<div class='r-right'>";
                items += " <ul><li><a href='#'><img src='../Content/images/del-icon.png' alt='' class='del-show' onclick='fun2'/></a></li>";
                items += "<li><a  class='fancybox' href='#infowindow' onclick='return info(\"" + place.place_id + "\",\"" + place.geometry.location.lat() + "\",\"" + place.geometry.location.lng() + "\",\"" + types + "\",2);'><img src='../Content/images/info-icon.png' alt='' class='example1demo' /></a></li></ul>";
                items += "</div></div></div>";
                items += "</aside>";
                checknamearray.push(place.name);
                checkidarray.push(place.place_id);
                var appendcount = 0;
                var Count = $('#rhaa1').find('.resort-row').length;
                if (Count != 0) {
                    for (var ResortRowCount = 0; ResortRowCount < Count; ResortRowCount++) {
                        var id = $('#rhaa1').find('.resort-row').eq(ResortRowCount).attr("id");
                        id = id.split("res_");
                        if (appendcount == 0) {
                            if (rating >= $('#rates_' + id[1]).val()) {
                                ++appendcount;
                                $('#rh' + id[1]).before(items);
                            }
                        }
                    }
                    if (appendcount == 0) {
                        $('#rhaa1').append(items);
                    }
                }
                else {
                    $('#rhaa1').append(items);
                }
            }
        }
    }
}

function LoadRestaurant(cuisinetype) {
    var tmpRestaurants = [];
    var setFlag = false;
    var foundFlag = false;
    for (var loopvar = 0, LengthRes = ListRestaurants.length; loopvar < LengthRes; loopvar++) {
        if (ListRestaurants[loopvar].resType == cuisinetype && ListRestaurants[loopvar].cityname == $('#selectedcity').html()) {
            tmpRestaurants=(ListRestaurants[loopvar].restaurants).slice(0);
            setFlag = true;
            break;
        }
    }
    if (setFlag) {
        cuisineele++;
        var arrTmp = [];
        for (var RestaurantsResult = 0, ResultsLength = tmpRestaurants.length; RestaurantsResult < ResultsLength; RestaurantsResult++) {
            foundFlag = false;
            for (var findLoop = 0, ListLength = restDetails.length; findLoop < ListLength; findLoop++) {
                if (tmpRestaurants[RestaurantsResult].place_id == restDetails[findLoop].plc_id && tmpRestaurants[RestaurantsResult].rating) {
                    if (restDetails[findLoop].details) {
                        //if (restDetails[findLoop].details.user_ratings_total) {
                            arrTmp = restDetails[findLoop].details;
                            foundFlag = true;
                            break;
                        //}
                    }
                }
            }
            if (foundFlag) {
                addResultrestaurant(arrTmp, 0, cuisinetype1[cuisineele - 1]);
                allrestaurants_toleft.push(arrTmp);
            }
        }
        if (cuisineele == CheckArraySort) {
            SortRestaurant($('#show_hide_ressort').find('.selectContainerStyled').find('.select_box').val(), '1');
        }
    }
    else {
        cuisinetype1.push(cuisinetype);
        $('#info').html("");
        var pyrmont = new google.maps.LatLng($('#latarray').val(), $('#lngarray').val());
        map = new google.maps.Map(document.getElementById('map-canvas'), {
            center: pyrmont,
            zoom: 15
        });
        var request = {
            location: pyrmont,
            radius: 5000,
            rankby: ['prominence'],
            types: ['restaurant|food|bakery'],
            keyword: [cuisinetype]
        };
        infowindow = new google.maps.InfoWindow();
        var service = new google.maps.places.PlacesService(map);
        service.nearbySearch(request, function (response, status) { callbackrestaurant(response, status, cuisinetype) });
    }
}

function callbackrestaurant(results, status, cuisinetype) {
    ++cuisineele;
    if (status == google.maps.places.PlacesServiceStatus.OK) {
        ListRestaurants.push({
            resType: cuisinetype,
            restaurants: results,
            cityname: $('#selectedcity').html()
        });
        for (var RestaurantsResult = 0, ResultsLength = results.length; RestaurantsResult < ResultsLength; RestaurantsResult++) {
            if (results[RestaurantsResult].rating) {
                if (results[RestaurantsResult].rating >= 1) {
                    AllRestColSugPart.push({
                        plc: results[RestaurantsResult],
                        resType: cuisinetype,
                        details: results[RestaurantsResult]
                    });
                    restDetails.push({
                        plc_id: results[RestaurantsResult].place_id,
                        resType: cuisinetype,
                        details: results[RestaurantsResult]
                    });
                    addResultrestaurant(results[RestaurantsResult], 0, cuisinetype);
                }
            }
        }
    }
    if (cuisineele == CheckArraySort) {
        TotalRests_SugPart = AllRestColSugPart.length;
        SortRestaurant($('#show_hide_ressort').find('.selectContainerStyled').find('.select_box').val(), '1');
        //GetAllRestDetOnSuggest();
    }
}

function GetAllRestDetOnSuggest() {
    if (TotalRests_SugPart == incrRests_SugPart) {
        SortRestaurant($('#show_hide_ressort').find('.selectContainerStyled').find('.select_box').val(), '1');
        //setTimeout(function () { SortRestaurant($('#show_hide_ressort').find('.selectContainerStyled').find('.select_box').val(), '1'); }, 3000);
    }
    else {
        var plcId = AllRestColSugPart[incrRests_SugPart].plc.place_id;
        var restauranttype = AllRestColSugPart[incrRests_SugPart].restType;
        var request = {
            placeId: plcId
        };
        var service = new google.maps.places.PlacesService(map);
        service.getDetails(request, function (place, status) {
            if (status == google.maps.places.PlacesServiceStatus.OK) {
                if (place.user_ratings_total) {
                    var userreview = place.user_ratings_total;
                    for (var indxVal = 0, restDetVal = restDetails.length; indxVal < restDetVal; indxVal++) {
                        if (restDetails[indxVal].plc_id == place.place_id) {
                            restDetails[indxVal].UserReviews = userreview;
                            restDetails[indxVal].details = place;
                            break;
                        }
                    }
                    addResultrestaurant(place, 0, restauranttype);
                    allrestaurants_toleft.push(place);
                }
                incrRests_SugPart++;
                GetAllRestDetOnSuggest();
            }
            else {
                if (status == google.maps.GeocoderStatus.OVER_QUERY_LIMIT) {
                    sleep(1000);
                    GetAllRestDetOnSuggest();
                }
                else {
                    incrRests_SugPart++;
                    GetAllRestDetOnSuggest();
                }
            }
        });
    }
}

function addResultrestaurant(place, indexValue, types) {
    if ((jQuery.inArray(place.place_id, checkidarray) !== -1) || (jQuery.inArray(place.name, checknamearray) !== -1)) {
    }
    else {
        if (jQuery.inArray(place.place_id, leftsidearray) !== -1) {
            var value = $.isArray(place.photos, place);
            if (value == true) {
                var rating = Math.round(place.rating);
                var items = '';
                if (place.rating) {
                    rating = place.rating;
                    var ratefilter = parseInt(place.rating);
                    items += "<div id='rh" + place.place_id + "'></div><input type='hidden' class='hide'  id=" + place.place_id + " value='" + types + "'><input type='hidden' id='rates_" + place.place_id + "' value='" + rating + "'><input type='hidden' id='ratefilter_" + place.place_id + "' value='" + ratefilter + "'><input type='hidden' id='latarray" + place.place_id + "' value='" + place.geometry.location.lat() + "'><input type='hidden' id='lngarray" + place.place_id + "' value='" + place.geometry.location.lng() + "'><aside class='resort-row' id='res_" + place.place_id + "'><div class='resort-row1' id='divres_" + place.place_id + "'>";
                    items += "<input type='hidden' id='latarray" + place.place_id + "' value='" + place.geometry.location.lat() + "' class='reslat'><input type='hidden' id='lngarray" + place.place_id + "' value='" + place.geometry.location.lng() + "' class='reslng'>";
                    items += "<input type='hidden' id='isME" + place.place_id + "' value='0' class='resME'>";
                    if (place.user_ratings_total) {
                        items += "<input type='hidden' class='reviews' value='" + place.user_ratings_total + "'/>";
                    }
                    else
                    {
                        items += "<input type='hidden' class='reviews' value='No rev'/>";
                    }
                    if (place.price_level) {
                        items += "<input type='hidden' class='priceLev' value='" + place.price_level + "'/>";
                    }
                    else {
                        items += "<input type='hidden' class='priceLev' value='0'/>";
                    }
                    if (place.website) {
                        items += "<input type='hidden' class='webhidden' value='" + place.website + "'>";
                    }
                    else {
                        items += "<input type='hidden' class='webhidden' value='No site'>";
                    }
                    items += "<figure class='resort-img'><div class='tooltip'>Drag to add</div><img src='" + place.photos[0].getUrl({ 'maxWidth': 180, 'maxHeight': 120 }) + "' alt='' id='img_" + place.place_id + "' />";
                    items += "<span>Rating <strong id='s" + place.place_id + "'>" + place.rating + "</strong></span>";
                    items += "</figure>";
                }
                else {
                    rating = 1;
                    var ratefilter = rating;
                    items += "<div id='rh" + place.place_id + "'></div><input type='hidden' class='hide'  id=" + place.place_id + " value='" + types + "'><input type='hidden' id='rates_" + place.place_id + "' value='" + rating + "'><input type='hidden' id='ratefilter_" + place.place_id + "' value='" + ratefilter + "'><input type='hidden' id='latarray" + place.place_id + "' value='" + place.geometry.location.lat() + "'><input type='hidden' id='lngarray" + place.place_id + "' value='" + place.geometry.location.lng() + "'><aside class='resort-row' id='res_" + place.place_id + "'><div class='resort-row1' id='divres_" + place.place_id + "'>";
                    items += "<input type='hidden' id='latarray" + place.place_id + "' value='" + place.geometry.location.lat() + "' class='reslat'><input type='hidden' id='lngarray" + place.place_id + "' value='" + place.geometry.location.lng() + "' class='reslng'>";
                    items += "<input type='hidden' id='isME" + place.place_id + "' value='0' class='resME'>";
                    if (place.user_ratings_total) {
                        items += "<input type='hidden' class='reviews' value='" + place.user_ratings_total + "'/>";
                    }
                    else {
                        items += "<input type='hidden' class='reviews' value='No rev'/>";
                    }
                    if (place.price_level) {
                        items += "<input type='hidden' class='priceLev' value='" + place.price_level + "'/>";
                    }
                    else {
                        items += "<input type='hidden' class='priceLev' value='0'/>";
                    }
                    if (place.website) {
                        items += "<input type='hidden' class='webhidden' value='" + place.website + "'>";
                    }
                    else {
                        items += "<input type='hidden' class='webhidden' value='No site'>";
                    }
                    items += "<figure class='resort-img'><div class='tooltip'>Drag to add</div><img src='" + place.photos[0].getUrl({ 'maxWidth': 180, 'maxHeight': 120 }) + "' alt='' id='img_" + place.place_id + "' />";
                    items += "<span>Rating <strong id='s" + place.place_id + "'>1</strong></span>";
                    items += "</figure>";
                }
                items += "<div class='resort-details'><div class='r-left'>";
                items += "<h3 id='h" + place.place_id + "'>" + place.name + "  </h3><br/>";
                items += "<p style='height:40px; overflow-y:hidden;' id='p" + place.place_id + "'>" + place.vicinity + "</p>";
                if (place.website) {
                    items += "<p style='height:18px; overflow-y:hidden;width:100%;text-overflow: ellipsis;white-space: nowrap;overflow: hidden;' id='web" + place.place_id + "'><b>Website : </b><a target='_blank' href='" + place.website + "' style='text-decoration: underline;'>" + place.website + "</a></p>";
                }
                items += "<ul id='rat" + place.place_id + "'><b>Ratings : </b>";
                for (var loopvar_rate = 1; loopvar_rate <= 5; loopvar_rate++) {
                    if (rating >= loopvar_rate) {
                        items += "<li><img src='../Content/images/coffee_full.png' /></li>";
                    }
                    else if (rating > (loopvar_rate - 1) && rating < loopvar_rate) {
                        items += "<li><img src='../Content/images/coffee_half.png' /></li>";
                    }
                    else {
                        items += "<li><img src='../Content/images/coffee_grey_full.png' /></li>";
                    }
                }
                if (place.user_ratings_total) {
                    //items += "<li>(" + place.user_ratings_total + " reviews)</li>";
                }
                items += "</ul>";
                if (place.price_level) {
                    if (place.price_level == 0) {
                        items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b>$</p>";
                    }
                    else if (place.price_level == 1) {
                        items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b>$</p>";
                    }
                    else if (place.price_level == 2) {
                        items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b>$$</p>";
                    }
                    else if (place.price_level == 3) {
                        items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b>$$$</p>";
                    }
                    else if (place.price_level == 4) {
                        items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b>$$$$</p>";
                    }
                }
                else {
                    items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b>$</p>";
                }
                items += "<div class='book-add'><span><a href='#' id='inner" + place.place_id + "' class='add-trip' onclick='return RestaurantToleft(\"" + place.place_id + "\");'>In the trip</a></span></div></div> ";
                items += "<div class='r-right'>";
                items += " <ul><li><a href='#'><img src='../Content/images/del-icon.png' alt='' class='del-show' onclick='fun2'/></a></li>";
                items += "<li><a  class='fancybox' href='#infowindow' onclick='return info(\"" + place.place_id + "\",\"" + place.geometry.location.lat() + "\",\"" + place.geometry.location.lng() + "\",\"" + types + "\",1);'><img src='../Content/images/info-icon.png' alt='' class='example1demo' /></a></li></ul>";
                items += "</div></div></div>";
                items += "</aside>";
                checknamearray.push(place.name);
                checkidarray.push(place.place_id);
                var appendcount = 0;
                var Count = $('#rhaa1').find('.resort-row').length;
                if (Count != 0) {
                    for (var ResortRowCount = 0; ResortRowCount < Count; ResortRowCount++) {
                        var id = $('#rhaa1').find('.resort-row').eq(ResortRowCount).attr("id");
                        id = id.split("res_");
                        if (appendcount == 0) {
                            if (rating >= $('#rates_' + id[1]).val()) {
                                ++appendcount;
                                $('#rh' + id[1]).before(items);
                            }
                        }
                    }
                    if (appendcount == 0) {
                        $('#rhaa1').append(items);
                    }
                }
                else {
                    $('#rhaa1').append(items);
                }
            }
            else {
                var rating = Math.round(place.rating);
                var items = '';
                if (place.rating) {
                    rating = place.rating;
                    var ratefilter = parseInt(place.rating);
                    items += "<div id='rh" + place.place_id + "'></div><input type='hidden' class='hide'  id=" + place.place_id + " value='" + types + "'><input type='hidden' id='rates_" + place.place_id + "' value='" + rating + "'><input type='hidden' id='ratefilter_" + place.place_id + "' value='" + ratefilter + "'><input type='hidden' id='latarray" + place.place_id + "' value='" + place.geometry.location.lat() + "'><input type='hidden' id='lngarray" + place.place_id + "' value='" + place.geometry.location.lng() + "'><aside class='resort-row' id='res_" + place.place_id + "'><div class='resort-row1' id='divres_" + place.place_id + "'>";
                    items += "<input type='hidden' id='latarray" + place.place_id + "' value='" + place.geometry.location.lat() + "' class='reslat'><input type='hidden' id='lngarray" + place.place_id + "' value='" + place.geometry.location.lng() + "' class='reslng'>";
                    items += "<input type='hidden' id='isME" + place.place_id + "' value='0' class='resME'>";
                    if (place.user_ratings_total) {
                        items += "<input type='hidden' class='reviews' value='" + place.user_ratings_total + "'/>";
                    }
                    else {
                        items += "<input type='hidden' class='reviews' value='No rev'/>";
                    }
                    if (place.price_level) {
                        items += "<input type='hidden' class='priceLev' value='" + place.price_level + "'/>";
                    }
                    else {
                        items += "<input type='hidden' class='priceLev' value='0'/>";
                    }
                    if (place.website) {
                        items += "<input type='hidden' class='webhidden' value='" + place.website + "'>";
                    }
                    else {
                        items += "<input type='hidden' class='webhidden' value='No site'>";
                    }
                    items += "<figure class='resort-img'><div class='tooltip'>Drag to add</div><img src='../Content/images/AppImages/restaurant.png' alt='' id='img_" + place.place_id + "'  />";
                    items += "<span>Rating <strong id='s" + place.place_id + "'>" + place.rating + "</strong></span>";
                    items += "</figure>";
                }
                else {
                    rating = 1;
                    var ratefilter = rating;
                    items += "<div id='rh" + place.place_id + "'></div><input type='hidden' class='hide'  id=" + place.place_id + " value='" + types + "'><input type='hidden' id='rates_" + place.place_id + "' value='" + rating + "'><input type='hidden' id='ratefilter_" + place.place_id + "' value='" + ratefilter + "'><input type='hidden' id='latarray" + place.place_id + "' value='" + place.geometry.location.lat() + "'><input type='hidden' id='lngarray" + place.place_id + "' value='" + place.geometry.location.lng() + "'><aside class='resort-row' id='res_" + place.place_id + "'><div class='resort-row1' id='divres_" + place.place_id + "'>";
                    items += "<input type='hidden' id='latarray" + place.place_id + "' value='" + place.geometry.location.lat() + "' class='reslat'><input type='hidden' id='lngarray" + place.place_id + "' value='" + place.geometry.location.lng() + "' class='reslng'>";
                    items += "<input type='hidden' id='isME" + place.place_id + "' value='0' class='resME'>";
                    if (place.user_ratings_total) {
                        items += "<input type='hidden' class='reviews' value='" + place.user_ratings_total + "'/>";
                    }
                    else {
                        items += "<input type='hidden' class='reviews' value='No rev'/>";
                    }
                    if (place.price_level) {
                        items += "<input type='hidden' class='priceLev' value='" + place.price_level + "'/>";
                    }
                    else {
                        items += "<input type='hidden' class='priceLev' value='0'/>";
                    }
                    if (place.website) {
                        items += "<input type='hidden' class='webhidden' value='" + place.website + "'>";
                    }
                    else {
                        items += "<input type='hidden' class='webhidden' value='No site'>";
                    }
                    items += "<figure class='resort-img'><div class='tooltip'>Drag to add</div><img src='../Content/images/AppImages/restaurant.png' alt='' id='img_" + place.place_id + "'  />";
                    items += "<span>Rating <strong id='s" + place.place_id + "'>1</strong></span>";
                    items += "</figure>";
                }
                items += "<div class='resort-details'><div class='r-left'>";
                items += "<h3 id='h" + place.place_id + "'>" + place.name + "  </h3><br/>";
                items += "<p style='height:40px; overflow-y:hidden;' id='p" + place.place_id + "'>" + place.vicinity + "</p>";
                if (place.website) {
                    items += "<p style='height:18px; overflow-y:hidden;width:100%;text-overflow: ellipsis;white-space: nowrap;overflow: hidden;' id='web" + place.place_id + "'><b>Website : </b><a target='_blank' href='" + place.website + "' style='text-decoration: underline;'>" + place.website + "</a></p>";
                }
                items += "<ul id='rat" + place.place_id + "'><b>Ratings : </b>";
                for (var loopvar_rate = 1; loopvar_rate <= 5; loopvar_rate++) {
                    if (rating >= loopvar_rate) {
                        items += "<li><img src='../Content/images/coffee_full.png' /></li>";
                    }
                    else if (rating > (loopvar_rate - 1) && rating < loopvar_rate) {
                        items += "<li><img src='../Content/images/coffee_half.png' /></li>";
                    }
                    else {
                        items += "<li><img src='../Content/images/coffee_grey_full.png' /></li>";
                    }
                }
                if (place.user_ratings_total) {
                    //items += "<li>(" + place.user_ratings_total + " reviews)</li>";
                }
                items += "</ul>";
                if (place.price_level) {
                    if (place.price_level == 0) {
                        items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b>$</p> ";
                    }
                    else if (place.price_level == 1) {
                        items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b>$</p> ";
                    }
                    else if (place.price_level == 2) {
                        items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b>$$</p> ";
                    }
                    else if (place.price_level == 3) {
                        items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b>$$$</p> ";
                    }
                    else if (place.price_level == 4) {
                        items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b>$$$$</p> ";
                    }
                }
                else {
                    items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b>$</p> ";
                }
                items += "<div class='book-add'><span><a href='#' id='inner" + place.place_id + "' class='add-trip' onclick='return RestaurantToleft(\"" + place.place_id + "\");'>In the trip</a></span></div></div> ";
                items += "<div class='r-right'>";
                items += " <ul><li><a href='#'><img src='../Content/images/del-icon.png' alt='' class='del-show' onclick='fun2'/></a></li>";
                items += "<li><a  class='fancybox' href='#infowindow' onclick='return info(\"" + place.place_id + "\",\"" + place.geometry.location.lat() + "\",\"" + place.geometry.location.lng() + "\",\"" + place.geometry.location.lng() + "\",1);'><img src='../Content/images/info-icon.png' alt='' class='example1demo' /></a></li></ul>";
                items += "</div></div></div>";
                items += "</aside>";
                checknamearray.push(place.name);
                checkidarray.push(place.place_id);
                var appendcount = 0;
                var Count = $('#rhaa1').find('.resort-row').length;
                if (Count != 0) {
                    for (var ResortRowCount = 0; ResortRowCount < Count; ResortRowCount++) {
                        var id = $('#rhaa1').find('.resort-row').eq(ResortRowCount).attr("id");
                        id = id.split("res_");
                        if (appendcount == 0) {
                            if (rating >= $('#rates_' + id[1]).val()) {
                                ++appendcount;
                                $('#rh' + id[1]).before(items);
                            }
                        }
                    }
                    if (appendcount == 0) {
                        $('#rhaa1').append(items);
                    }
                }
                else {
                    $('#rhaa1').append(items);
                }
            }
        }
        else {
            var value = $.isArray(place.photos, place);
            if (value == true) {
                var rating = Math.round(place.rating);
                var items = '';
                if (place.rating) {
                    rating = place.rating;
                    var ratefilter = parseInt(place.rating);
                    items += "<div id='rh" + place.place_id + "'></div><input type='hidden' class='hide'  id=" + place.place_id + " value='" + types + "'><input type='hidden' id='rates_" + place.place_id + "' value='" + rating + "'><input type='hidden' id='ratefilter_" + place.place_id + "' value='" + ratefilter + "'><input type='hidden' id='latarray" + place.place_id + "' value='" + place.geometry.location.lat() + "'><input type='hidden' id='lngarray" + place.place_id + "' value='" + place.geometry.location.lng() + "'><aside class='resort-row' id='res_" + place.place_id + "'><div class='resort-row1' id='divres_" + place.place_id + "'>";
                    items += "<input type='hidden' id='latarray" + place.place_id + "' value='" + place.geometry.location.lat() + "' class='reslat'><input type='hidden' id='lngarray" + place.place_id + "' value='" + place.geometry.location.lng() + "' class='reslng'>";
                    items += "<input type='hidden' id='isME" + place.place_id + "' value='0' class='resME'>";
                    if (place.user_ratings_total) {
                        items += "<input type='hidden' class='reviews' value='" + place.user_ratings_total + "'/>";
                    }
                    else {
                        items += "<input type='hidden' class='reviews' value='No rev'/>";
                    }
                    if (place.price_level) {
                        items += "<input type='hidden' class='priceLev' value='" + place.price_level + "'/>";
                    }
                    else {
                        items += "<input type='hidden' class='priceLev' value='0'/>";
                    }
                    if (place.website) {
                        items += "<input type='hidden' class='webhidden' value='" + place.website + "'>";
                    }
                    else {
                        items += "<input type='hidden' class='webhidden' value='No site'>";
                    }
                    items += "<figure class='resort-img'><div class='tooltip'>Drag to add</div><img src='" + place.photos[0].getUrl({ 'maxWidth': 182, 'maxHeight': 120 }) + "' alt='' id='img_" + place.place_id + "' />";
                    items += "<span>Rating <strong id='s" + place.place_id + "'>" + place.rating + "</strong></span>";
                    items += "</figure>";
                }
                else {
                    rating = 1;
                    var ratefilter = rating;
                    items += "<div id='rh" + place.place_id + "'></div><input type='hidden' class='hide'  id=" + place.place_id + " value='" + types + "'><input type='hidden' id='rates_" + place.place_id + "' value='" + rating + "'><input type='hidden' id='ratefilter_" + place.place_id + "' value='" + ratefilter + "'><input type='hidden' id='latarray" + place.place_id + "' value='" + place.geometry.location.lat() + "'><input type='hidden' id='lngarray" + place.place_id + "' value='" + place.geometry.location.lng() + "'><aside class='resort-row' id='res_" + place.place_id + "'><div class='resort-row1' id='divres_" + place.place_id + "'>";
                    items += "<input type='hidden' id='latarray" + place.place_id + "' value='" + place.geometry.location.lat() + "' class='reslat'><input type='hidden' id='lngarray" + place.place_id + "' value='" + place.geometry.location.lng() + "' class='reslng'>";
                    items += "<input type='hidden' id='isME" + place.place_id + "' value='0' class='resME'>";
                    if (place.user_ratings_total) {
                        items += "<input type='hidden' class='reviews' value='" + place.user_ratings_total + "'/>";
                    }
                    else {
                        items += "<input type='hidden' class='reviews' value='No rev'/>";
                    }
                    if (place.price_level) {
                        items += "<input type='hidden' class='priceLev' value='" + place.price_level + "'/>";
                    }
                    else {
                        items += "<input type='hidden' class='priceLev' value='0'/>";
                    }
                    if (place.website) {
                        items += "<input type='hidden' class='webhidden' value='" + place.website + "'>";
                    }
                    else {
                        items += "<input type='hidden' class='webhidden' value='No site'>";
                    }
                    items += "<figure class='resort-img'><div class='tooltip'>Drag to add</div><img src='" + place.photos[0].getUrl({ 'maxWidth': 182, 'maxHeight': 120 }) + "' alt='' id='img_" + place.place_id + "' />";
                    items += "<span>Rating <strong id='s" + place.place_id + "'>" + ratefilter + "</strong></span>";
                    items += "</figure>";
                }
                items += "<div class='resort-details'><div class='r-left'>";
                items += "<h3 id='h" + place.place_id + "'>" + place.name + "  </h3><br/>";
                items += "<p style='height:40px; overflow-y:hidden;' id='p" + place.place_id + "'>" + place.vicinity + "</p>";
                if (place.website) {
                    items += "<p style='height:18px; overflow-y:hidden;width:100%;text-overflow: ellipsis;white-space: nowrap;overflow: hidden;' id='web" + place.place_id + "'><b>Website : </b><a target='_blank' href='" + place.website + "' style='text-decoration: underline;'>" + place.website + "</a></p>";
                }
                items += "<ul id='rat" + place.place_id + "'><b>Ratings : </b>";
                for (var loopvar_rate = 1; loopvar_rate <= 5; loopvar_rate++) {                    
                    if (rating >= loopvar_rate) {
                        items += "<li><img src='../Content/images/coffee_full.png' /></li>";
                    }
                    else if (rating > (loopvar_rate - 1) && rating < loopvar_rate) {
                        items += "<li><img src='../Content/images/coffee_half.png' /></li>";
                    }
                    else {
                        items += "<li><img src='../Content/images/coffee_grey_full.png' /></li>";
                    }
                }
                if (place.user_ratings_total) {
                    //items += "<li>(" + place.user_ratings_total + " reviews)</li>";
                }
                items += "</ul>";
                if (place.price_level) {
                    if (place.price_level == 0) {
                        items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b>$</p>";
                    }
                    else if (place.price_level == 1) {
                        items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b>$</p>";
                    }
                    else if (place.price_level == 2) {
                        items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b>$$</p>";
                    }
                    else if (place.price_level == 3) {
                        items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b>$$$</p>";
                    }
                    else if (place.price_level == 4) {
                        items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b>$$$$</p>";
                    }
                }
                else {
                    items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b>$</p>";
                }
                items += "<div class='book-add'><span><a href='#' id='inner" + place.place_id + "' class='add-trip' onclick='return RestaurantToleft(\"" + place.place_id + "\");'>Add Trip</a></span></div></div> ";
                items += "<div class='r-right'>";
                items += " <ul><li><a href='#'><img src='../Content/images/del-icon.png' alt='' class='del-show' onclick='fun2'/></a></li>";
                items += "<li><a  class='fancybox' href='#infowindow' onclick='return info(\"" + place.place_id + "\",\"" + place.geometry.location.lat() + "\",\"" + place.geometry.location.lng() + "\",\"" + types + "\",1);'><img src='../Content/images/info-icon.png' alt='' class='example1demo' /></a></li></ul>";
                items += "</div></div></div>";
                items += "</aside>";
                checknamearray.push(place.name);
                checkidarray.push(place.place_id);
                var appendcount = 0;
                var Count = $('#rhaa1').find('.resort-row').length;
                if (Count != 0) {
                    for (var ResortRowCount = 0; ResortRowCount < Count; ResortRowCount++) {
                        var id = $('#rhaa1').find('.resort-row').eq(ResortRowCount).attr("id");
                        id = id.split("res_");
                        if (appendcount == 0) {
                            if (rating >= $('#rates_' + id[1]).val()) {
                                ++appendcount;
                                $('#rh' + id[1]).before(items);
                            }
                        }
                    }
                    if (appendcount == 0) {
                        $('#rhaa1').append(items);
                    }
                }
                else {
                    $('#rhaa1').append(items);
                }
            }
            else {
                var rating = Math.round(place.rating);
                var items = '';
                if (place.rating) {
                    rating = place.rating;
                    var ratefilter = parseInt(place.rating);
                    items += "<div id='rh" + place.place_id + "'></div><input type='hidden' class='hide'  id=" + place.place_id + " value='" + types + "'><input type='hidden' id='rates_" + place.place_id + "' value='" + rating + "'><input type='hidden' id='ratefilter_" + place.place_id + "' value='" + ratefilter + "'><input type='hidden' id='latarray" + place.place_id + "' value='" + place.geometry.location.lat() + "'><input type='hidden' id='lngarray" + place.place_id + "' value='" + place.geometry.location.lng() + "'><aside class='resort-row' id='res_" + place.place_id + "'><div class='resort-row1' id='divres_" + place.place_id + "'>";
                    items += "<input type='hidden' id='latarray" + place.place_id + "' value='" + place.geometry.location.lat() + "' class='reslat'><input type='hidden' id='lngarray" + place.place_id + "' value='" + place.geometry.location.lng() + "' class='reslng'>";
                    items += "<input type='hidden' id='isME" + place.place_id + "' value='0' class='resME'>";
                    if (place.user_ratings_total) {
                        items += "<input type='hidden' class='reviews' value='" + place.user_ratings_total + "'/>";
                    }
                    else {
                        items += "<input type='hidden' class='reviews' value='No rev'/>";
                    }
                    if (place.price_level) {
                        items += "<input type='hidden' class='priceLev' value='" + place.price_level + "'/>";
                    }
                    else {
                        items += "<input type='hidden' class='priceLev' value='0'/>";
                    }
                    if (place.website) {
                        items += "<input type='hidden' class='webhidden' value='" + place.website + "'>";
                    }
                    else {
                        items += "<input type='hidden' class='webhidden' value='No site'>";
                    }
                    items += "<figure class='resort-img'><div class='tooltip'>Drag to add</div><img src='../Content/images/AppImages/restaurant.png' alt='' id='img_" + place.place_id + "'  />";
                    items += "<span>Rating <strong id='s" + place.place_id + "'>" + place.rating + "</strong></span>";
                    items += "</figure>";
                }
                else {
                    rating = 1;
                    var ratefilter = rating;
                    items += "<div id='rh" + place.place_id + "'></div><input type='hidden' class='hide'  id=" + place.place_id + " value='" + types + "'><input type='hidden' id='rates_" + place.place_id + "' value='" + rating + "'><input type='hidden' id='ratefilter_" + place.place_id + "' value='" + ratefilter + "'><input type='hidden' id='latarray" + place.place_id + "' value='" + place.geometry.location.lat() + "'><input type='hidden' id='lngarray" + place.place_id + "' value='" + place.geometry.location.lng() + "'><aside class='resort-row' id='res_" + place.place_id + "'><div class='resort-row1' id='divres_" + place.place_id + "'>";
                    items += "<input type='hidden' id='latarray" + place.place_id + "' value='" + place.geometry.location.lat() + "' class='reslat'><input type='hidden' id='lngarray" + place.place_id + "' value='" + place.geometry.location.lng() + "' class='reslng'>";
                    items += "<input type='hidden' id='isME" + place.place_id + "' value='0' class='resME'>";
                    if (place.user_ratings_total) {
                        items += "<input type='hidden' class='reviews' value='" + place.user_ratings_total + "'/>";
                    }
                    else {
                        items += "<input type='hidden' class='reviews' value='No rev'/>";
                    }
                    if (place.price_level) {
                        items += "<input type='hidden' class='priceLev' value='" + place.price_level + "'/>";
                    }
                    else {
                        items += "<input type='hidden' class='priceLev' value='0'/>";
                    }
                    if (place.website) {
                        items += "<input type='hidden' class='webhidden' value='" + place.website + "'>";
                    }
                    else {
                        items += "<input type='hidden' class='webhidden' value='No site'>";
                    }
                    items += "<figure class='resort-img'><div class='tooltip'>Drag to add</div><img src='../Content/images/AppImages/restaurant.png' alt='' id='img_" + place.place_id + "'  />";
                    items += "<span>Rating <strong id='s" + place.place_id + "'>" + ratefilter + "</strong></span>";
                    items += "</figure>";
                }
                items += "<div class='resort-details'><div class='r-left'>";
                items += "<h3 id='h" + place.place_id + "'>" + place.name + "  </h3><br/>";
                items += "<p style='height:40px; overflow-y:hidden;' id='p" + place.place_id + "'>" + place.vicinity + "</p>";
                if (place.website) {
                    items += "<p style='height:18px; overflow-y:hidden;width:100%;text-overflow: ellipsis;white-space: nowrap;overflow: hidden;' id='web" + place.place_id + "'><b>Website : </b><a target='_blank' href='" + place.website + "' style='text-decoration: underline;'>" + place.website + "</a></p>";
                }
                items += "<ul id='rat" + place.place_id + "'><b>Ratings : </b>";
                for (var loopvar_rate = 1; loopvar_rate <= 5; loopvar_rate++) {
                    if (rating >= loopvar_rate) {
                        items += "<li><img src='../Content/images/coffee_full.png' /></li>";
                    }
                    else if (rating > (loopvar_rate - 1) && rating < loopvar_rate) {
                        items += "<li><img src='../Content/images/coffee_half.png' /></li>";
                    }
                    else {
                        items += "<li><img src='../Content/images/coffee_grey_full.png' /></li>";
                    }
                }
                if (place.user_ratings_total) {
                    //items += "<li>(" + place.user_ratings_total + " reviews)</li>";
                }
                items += "</ul>";
                if (place.price_level) {
                    if (place.price_level == 0) {
                        items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b> $</p> ";
                    }
                    else if (place.price_level == 1) {
                        items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b> $</p> ";
                    }
                    else if (place.price_level == 2) {
                        items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b>  $$</p> ";
                    }
                    else if (place.price_level == 3) {
                        items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b>  $$$</p> ";
                    }
                    else if (place.price_level == 4) {
                        items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b>  $$$$</p> ";
                    }
                }
                else {
                    items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b> $</p> ";
                }
                items += "<div class='book-add'><span><a href='#' id='inner" + place.place_id + "' class='add-trip' onclick='return RestaurantToleft(\"" + place.place_id + "\");'>Add Trip</a></span></div></div> ";
                items += "<div class='r-right'>";
                items += " <ul><li><a href='#'><img src='../Content/images/del-icon.png' alt='' class='del-show' onclick='fun2'/></a></li>";
                items += "<li><a  class='fancybox' href='#infowindow' onclick='return info(\"" + place.place_id + "\",\"" + place.geometry.location.lat() + "\",\"" + place.geometry.location.lng() + "\",\"" + types + "\",1);'><img src='../Content/images/info-icon.png' alt='' class='example1demo' /></a></li></ul>";
                items += "</div></div></div>";

                items += "</aside>";
                checknamearray.push(place.name);
                checkidarray.push(place.place_id);
                var appendcount = 0;
                var Count = $('#rhaa1').find('.resort-row').length;
                if (Count != 0) {
                    for (var ResortRowCount = 0; ResortRowCount < Count; ResortRowCount++) {
                        var id = $('#rhaa1').find('.resort-row').eq(ResortRowCount).attr("id");
                        id = id.split("res_");
                        if (appendcount == 0) {
                            if (rating >= $('#rates_' + id[1]).val()) {
                                ++appendcount;
                                $('#rh' + id[1]).before(items);
                            }
                        }
                    }
                    if (appendcount == 0) {
                        $('#rhaa1').append(items);
                    }
                }
                else {
                    $('#rhaa1').append(items);
                }
            }
        }
    }
}

function info_for_hotels(hotels_id, plcbx_id) {
    $('#infowindow_hotels_activities').html("");
    var cityname_for_info = $("#" + plcbx_id).parents('.place-box-right').data('value');
    var array_of_hotels = [], place = [], placediv1 = "";
    for (var loopvar = 0, AddDayLength = foraddday_array.length; loopvar < AddDayLength; loopvar++) {
        if ((foraddday_array[loopvar].city_name).trim() === cityname_for_info.trim()) {
            array_of_hotels = foraddday_array[loopvar].hotelsfull.slice(0);
            place = array_of_hotels.filter(function (obj) {
                return obj.hotelId == hotels_id;
            });
            var descri = "", amenitiesare = "";
            var amenityvalues = place[0].amenityMask;
            if (place[0].amenityMask & 1) {
                amenitiesare += "<li>Business Center</li>"
            }
            if (place[0].amenityMask & 2) {
                amenitiesare += "<li>Fitness Center</li>";
            }
            if (place[0].amenityMask & 8) {
                amenitiesare += "<li>Internet Access Available</li>";
            }
            if (place[0].amenityMask & 16) {
                amenitiesare += "<li>Kids' Activities</li>";
            }
            if (place[0].amenityMask & 32) {
                amenitiesare += "<li>Kitchen or Kitchenette</li>";
            }
            if (place[0].amenityMask & 64) {
                amenitiesare += "<li>Pets Allowed</li>";
            }
            if (place[0].amenityMask & 128) {
                amenitiesare += "<li>Pool</li>";
            }
            if (place[0].amenityMask & 256) {
                amenitiesare += "<li>Restaurant On-site</li>";
            }
            if (place[0].amenityMask & 512) {
                amenitiesare += "<li>Spa On-site</li>";
            }
            if (place[0].amenityMask & 2048) {
                amenitiesare += "<li>Breakfast</li>";
            }
            if (place[0].amenityMask & 4096) {
                amenitiesare += "<li>Babysitting</li>";
            }
            if (place[0].amenityMask & 16384) {
                amenitiesare += "<li>Parking</li>";
            }
            if (place[0].amenityMask & 32768) {
                amenitiesare += "<li>Room Service </li>";
            }
            if (place[0].amenityMask & 262144) {
                amenitiesare += "<li>Roll-in Shower</li>";
            }
            if (place[0].amenityMask & 524288) {
                amenitiesare += "<li>Handicapped Parking</li>";
            }
            if (place[0].amenityMask & 2097152) {
                amenitiesare += "<li>Accessibility Equipment for the Deaf</li>";
            }
            if (place[0].amenityMask & 4194304) {
                amenitiesare += "<li>Braille or Raised Signage</li>";
            }
            if (place[0].amenityMask & 8388608) {
                amenitiesare += "<li>Free Airport Shuttle</li>";
            }
            if (place[0].amenityMask & 16777216) {
                amenitiesare += "<li>Indoor Pool</li>";
            }
            if (place[0].amenityMask & 33554432) {
                amenitiesare += "<li>Outdoor Pool</li>";
            }
            placediv1 += "<figure class='PopUp-CloseBtn' onclick='$.fancybox.close();'><img src='../Content/images/del_travel-bkp-7-5-15.png'></figure>";
            placediv1 += "<figure class='resort-img'>";
            var imgurl_split = place[0].thumbNailUrl.split('_');
            imgurl_split[imgurl_split.length - 1] = imgurl_split[imgurl_split.length - 1].replace('t.jpg', 'b.jpg');
            var imgurl = imgurl_split.join('_');
            placediv1 += "<img src='http://media3.expedia.com" + imgurl + "' alt='' />";
            placediv1 += "<span>Night from <strong>$" + place[0].highRate + "</strong></span></figure>";
            placediv1 += "<h2>" + place[0].name + "</h2>";
            //placediv1 += "<ul><b>Guest Rating : </b>";
            //for (var loopvar2 = 1; loopvar2 <= 5; loopvar2++) {
            //    if (place[0].tripAdvisorRating >= loopvar2) {
            //        placediv1 += "<li><img src='../Content/images/hotel-color.png' /></li>";
            //    }
            //    else if (place[0].tripAdvisorRating > (loopvar2 - 1) && place[0].tripAdvisorRating < loopvar2) {
            //        placediv1 += "<li><img src='../Content/images/hotel-half.png' /></li>";
            //    }
            //    else {
            //        placediv1 += "<li><img src='../Content/images/hotel-gray.png' /></li>";
            //    }
            //}
            //placediv1 += "<li>(" + place[0].tripAdvisorRating + "/5)</li></ul>";
            descri = "<p><b>Short Description : </b></p>" + (place[0].shortDescription).replace('&lt;p&gt;&lt;b&gt;Property Location&lt;/b&gt; &lt;br /&gt;', '') + "...";
            placediv1 += "<p><b>Address : </b>" + place[0].address1;
            if (place[0].address2 != null && place[0].address2 != "") {
                placediv1 += "," + place[0].address2;
            }
            if (place[0].city != null && place[0].city != "") {
                placediv1 += "," + place[0].city;
            }
            if (place[0].postalCode != null && place[0].postalCode != "") {
                placediv1 += "-" + place[0].postalCode;
            }
            placediv1 += "</p><p><b>Landmark : </b>" + place[0].locationDescription + "</p><br/><p>" + descri + " </p>";
            var propertycat = "";
            if (place[0].propertyCategory == 1) {
                propertycat = "hotel";
            }
            else if (place[0].propertyCategory == 2) {
                propertycat = "suite";
            }
            else if (place[0].propertyCategory == 3) {
                propertycat = "resort";
            }
            else if (place[0].propertyCategory == 4) {
                propertycat = "vacation rental/condo";
            }
            else if (place[0].propertyCategory == 5) {
                propertycat = "bed & breakfast";
            }
            else if (place[0].propertyCategory == 6) {
                propertycat = "all-inclusive ";
            }
            if (propertycat != "hotel") {
                placediv1 += "</p><p><b>Property Category : </b>" + propertycat + "</p>";
            }
            placediv1 += "<p><b>Rate starts from : </b>$" + place[0].highRate + "</p>";
            placediv1 += "<p><b>For booking : </b><a href='" + place[0].deepLink + "'>Click here</a></p>";
            if (amenitiesare != "") {
                placediv1 += "<br/><p><b>Hotel Amenities : </b>" + amenitiesare + "</p>";
            }
            placediv1 += "</div> </div> </div>";
            $('#infowindow_hotels_activities').append(placediv1);
        }
    }
}

function info_for_Activities(activities_id, plcbx_id) {
    $('#infowindow_hotels_activities').html("");
    var cityname_for_info = $("#" + plcbx_id).parents('.place-box-right').data('value');
    var array_of_activities = [], place = [],placediv1 = "";
    for (var loopvar = 0, AddDayLength = foraddday_array.length; loopvar < AddDayLength; loopvar++) {
        if ((foraddday_array[loopvar].city_name).trim() === cityname_for_info.trim()) {
            array_of_activities = foraddday_array[loopvar].activitiesfull.slice(0);
            place = array_of_activities.filter(function (obj) {
                return obj.id == activities_id;
            });
            placediv1 += "<figure class='resort-img'><img src='" + place[0].Images + "' alt='' /><span>From <strong>$" + place[0].Cost + "</strong><br/>(" + place[0].Price_Description + ")</span></figure>";
            placediv1 += "<h3>" + place[0].title + "</h3>";
            if (place[0].rating != "") {
                var actrating = parseFloat(place[0].rating).toFixed(1);
                placediv1 += "<ul><b>Ratings : </b>";
                for (var loopvaria1 = 0; loopvaria1 < 5; loopvaria1++) {
                    if (place[0].rating < (loopvaria1 + 0.5)) {
                        placediv1 += "<li><img src='../Content/images/grey-star.png' alt='' /></li>";
                    }
                    else {
                        placediv1 += "<li><img src='../Content/images/yellow-star.png' alt='' /></li>";
                    }
                }
                placediv1 += "<li>(" + actrating + "/5)</li></ul>";
            }
            placediv1 += "<p><b>Starting location : </b>" + place[0].starting_location + "</p>";
            placediv1 += "<p><b>Locations : </b>" + place[0].locations_text + "</p>";
            placediv1 += "<p><b>Url : </b><a href='" + place[0].url + "'>" + place[0].url + "</a></p>";
            placediv1 += "<p><b>Description : </b>" + place[0].abstract + "</p>";
            $('#infowindow_hotels_activities').append(placediv1);
        }
    }
}

function GetAttractionForFirst()
{   
    var tmpOpts = [];
    for(var loopvar=0,optLen=ListAttractions.length;loopvar<optLen;loopvar++)
    {
        if (ListAttractions[loopvar].cityname == $('#selectedcity').html()) {
            tmpOpts.push((ListAttractions[loopvar].attrType).slice(0));
        }
    }
    setOptLen = tmpOpts.length;
    for (tmpOptsLoop = 0, optionsLen = setOptLen; tmpOptsLoop < optionsLen; tmpOptsLoop++)
    {
        DisplayAttractionForFirst(tmpOpts[tmpOptsLoop]);
    }
}

function DisplayAttractionForFirst(FirstattrOpt) {
    getOptLen++
    var tmpAttractions = [];
    var setFlag = false, foundFlag = false, someFlag = false;
    for (var loopvar = 0, LengthAttr = ListAttractions.length; loopvar < LengthAttr; loopvar++) {
        if (ListAttractions[loopvar].attrType == FirstattrOpt && ListAttractions[loopvar].cityname == $('#selectedcity').html()) {
            tmpAttractions = (ListAttractions[loopvar].attractions).slice(0);
            setFlag = true;
            break;
        }        
    }
    if (setFlag) {
        ++touristele;
        var arrTmp = [];
        for (var AttractionsResult = 0, ResultsLength = tmpAttractions.length; AttractionsResult < ResultsLength; AttractionsResult++) {
            foundFlag = false;
            for (var findLoop = 0, ListLength = attrDetails.length; findLoop < ListLength; findLoop++) {
                if (tmpAttractions[AttractionsResult].place_id == attrDetails[findLoop].plc_id && tmpAttractions[AttractionsResult].rating >= 1) {
                    var needsExclusions = [];
                    needsExclusions = tmpAttractions[AttractionsResult].types;
                    someFlag = true;
                    for (var index1 = 0; index1 < excludeMe.length; index1++) {
                        if (needsExclusions.indexOf(excludeMe[index1]) !== -1) {
                            someFlag = false;
                            break;
                        }
                    }
                    if (someFlag) {
                        if ((tmpAttractions[AttractionsResult].name).toLowerCase().indexOf("college") >= 0) {
                            someFlag = false;
                        }
                    }
                    if (someFlag) {
                        if (attrDetails[findLoop].details) {
                            arrTmp = attrDetails[findLoop].details;
                            foundFlag = true;
                            break;
                        }
                    }
                }
            }
            if (foundFlag) {
                addResul(arrTmp, 0, touristtype1[touristele - 1]);
                allattractions_toleft.push(arrTmp[0]);
            }
        }
    }
    if (setOptLen == getOptLen) {
        getOptLen = 0;
        setOptLen = 0;
        SortRestaurant($('#show_hide_attrsort').find('.selectContainerStyled').find('.select_box').val(), '2');
        setTimeout(function () { intheTripRHAA("Attractions") }, 1000);
        //setTimeout(function () { SortRestaurant($('#show_hide_attrsort').find('.selectContainerStyled').find('.select_box').val(), '2'); }, 2000);
    }
}

function ApplyHotelsFilter()
{
    var filthc = "";
    for (var loopvarHotels = 0, CitiesHotelsLength = hotelsForCities.length; loopvarHotels < CitiesHotelsLength; loopvarHotels++) {
        if (hotelsForCities[loopvarHotels].cityName.trim() == $('#selectedcity').html().trim()) {
            if (hotelsForCities[loopvarHotels].Hotels.HotelListResponse.HotelList) {
                filterHotelsArray = hotelsForCities[loopvarHotels].Hotels.HotelListResponse.HotelList['HotelSummary'].slice();
            }
        }
    }
    var filterstr = [],filterstrRate = [],filteramenitiesStr = [];
    finalfilterArray.length = 0, finalfilterArrayfull.length = 0;
    $("input:checkbox[name=hotel_class]:checked").each(function () {
        if ($(this).val() == 1) {
            filterstr.push("el.hotelRating < 2 && el.hotelRating >= 1");
        }
        else if ($(this).val() == 2) {
            filterstr.push("el.hotelRating < 3 && el.hotelRating >= 2");
        }
        else if ($(this).val() == 3) {
            filterstr.push("el.hotelRating < 4 && el.hotelRating >= 3");
        }
        else if ($(this).val() == 4) {
            filterstr.push("el.hotelRating < 5 && el.hotelRating >= 4");
        }
        else if ($(this).val() == 5) {
            filterstr.push("el.hotelRating == 5");
        }
    });

    $("input:checkbox[name=Popular]:checked").each(function () {
        if ($(this).val() == 1) {
            filterstrRate.push("el.tripAdvisorRating < 2 && el.tripAdvisorRating >= 1");
        }
        else if ($(this).val() == 2) {
            filterstrRate.push("el.tripAdvisorRating < 3 && el.tripAdvisorRating >= 2");
        }
        else if ($(this).val() == 3) {
            filterstrRate.push("el.tripAdvisorRating < 4 && el.tripAdvisorRating >= 3");
        }
        else if ($(this).val() == 4) {
            filterstrRate.push("el.tripAdvisorRating < 5 && el.tripAdvisorRating >= 4");
        }
        else if ($(this).val() == 5) {
            filterstrRate.push("el.tripAdvisorRating == 5");
        }
    });

    $("input:checkbox[name=HP]:checked").each(function () {
        if ($(this).val() == 1) {
            filteramenitiesStr.push("el.amenityMask & 1");
        }
        else if ($(this).val() == 2) {
            filteramenitiesStr.push("el.amenityMask & 2");
        }
        else if ($(this).val() == 8) {
            filteramenitiesStr.push("el.amenityMask & 8");
        }
        else if ($(this).val() == 32) {
            filteramenitiesStr.push("el.amenityMask & 32");
        }
        else if ($(this).val() == 64) {
            filteramenitiesStr.push("el.amenityMask & 64");
        }
        else if ($(this).val() == 128) {
            filteramenitiesStr.push("el.amenityMask & 128");
        }
        else if ($(this).val() == 256) {
            filteramenitiesStr.push("el.amenityMask & 256");
        }
        else if ($(this).val() == 512) {
            filteramenitiesStr.push("el.amenityMask & 512");
        }
        else if ($(this).val() == 2048) {
            filteramenitiesStr.push("el.amenityMask & 2048");
        }
        else if ($(this).val() == 4096) {
            filteramenitiesStr.push("el.amenityMask & 4096");
        }
        else if ($(this).val() == 32768) {
            filteramenitiesStr.push("el.amenityMask & 32768");
        }
        else if ($(this).val() == 134217728) {
            filteramenitiesStr.push("el.amenityMask & 134217728");
        }
    });

    var slival1 = $("#slider-range").slider("values", 0);
    var slival2 = $("#slider-range").slider("values", 1);

    filthc += "(el.highRate>=" + slival1 + " && el.highRate<=" + slival2 + ")";

    var querystr1 = filterstrRate.join("||");
    var querystr = filterstr.join("||");
    var querystr2 = filteramenitiesStr.join(" && ");
    var fullquerystr = "", finalquerystr = "";

    if (querystr != "" && querystr1 != "" && querystr2 != "") {
        fullquerystr = "(" + querystr + ") && (" + querystr1 + ") && (" + querystr2 + ")";
    }
    else if (querystr == "" && querystr1 != "" && querystr2 != "") {
        fullquerystr = "(" + querystr1 + ") && (" + querystr2 + ")";
    }
    else if (querystr != "" && querystr1 == "" && querystr2 != "") {
        fullquerystr = "(" + querystr + ") && (" + querystr2 + ")";
    }
    else if (querystr != "" && querystr1 != "" && querystr2 == "") {
        fullquerystr = "(" + querystr + ") && (" + querystr1 + ")";
    }
    else if (querystr != "" && querystr1 == "" && querystr2 == "") {
        fullquerystr = querystr;
    }
    else if (querystr == "" && querystr1 != "" && querystr2 == "") {
        fullquerystr = querystr1;
    }
    else if (querystr == "" && querystr1 == "" && querystr2 != "") {
        fullquerystr = querystr2;
    }
    if (fullquerystr != "") {
        finalquerystr = fullquerystr + " && " + filthc;
        finalfilterArrayfull = filterHotelsArray.filter(function (el) {
            return eval(finalquerystr);
        });
    }
    else {
        finalfilterArrayfull = filterHotelsArray.filter(function (el) {
            return eval(filthc);
        });
    }
    var hotelsortval = $("#hotel_sortid_select").val();
    if (hotelsortval == "ALPHA") {
        sortHotelArray = finalfilterArrayfull.slice();
        sortHotelArray.sort(function (a, b) {
            if (a.name < b.name)
                return -1;
            if (a.name > b.name)
                return 1;
            return 0;
        });
    }
    else if (hotelsortval == "ALPHA_REVERSE") {
        sortHotelArray = finalfilterArrayfull.slice();
        sortHotelArray.sort(function (a, b) {
            if (a.name > b.name)
                return -1;
            if (a.name < b.name)
                return 1;
            return 0;
        });
    }
    else if (hotelsortval == "PRICE") {
        sortHotelArray = finalfilterArrayfull.slice();
        sortHotelArray.sort(function (a, b) {
            if (a.highRate < b.highRate)
                return -1;
            if (a.highRate > b.highRate)
                return 1;
            return 0;
        });
    }
    else if (hotelsortval == "PRICE_REVERSE") {
        sortHotelArray = finalfilterArrayfull.slice();
        sortHotelArray.sort(function (a, b) {
            if (a.highRate > b.highRate)
                return -1;
            if (a.highRate < b.highRate)
                return 1;
            return 0;
        });
    }
    else if (hotelsortval == "QUALITY_REVERSE") {
        sortHotelArray = finalfilterArrayfull.slice();
        sortHotelArray.sort(function (a, b) {
            if (a.hotelRating < b.hotelRating)
                return -1;
            if (a.hotelRating > b.hotelRating)
                return 1;
            return 0;
        });
    }
    else if (hotelsortval == "QUALITY") {
        sortHotelArray = finalfilterArrayfull.slice();
        sortHotelArray.sort(function (a, b) {
            if (a.hotelRating > b.hotelRating)
                return -1;
            if (a.hotelRating < b.hotelRating)
                return 1;
            return 0;
        });
    }
    else if (hotelsortval == "RATING") {
        sortHotelArray = finalfilterArrayfull.slice();
        sortHotelArray.sort(function (a, b) {
            if (a.tripAdvisorRating > b.tripAdvisorRating)
                return -1;
            if (a.tripAdvisorRating < b.tripAdvisorRating)
                return 1;
            return 0;
        });
    }
    else if (hotelsortval == "RATING_REVERSE") {
        sortHotelArray = finalfilterArrayfull.slice();
        sortHotelArray.sort(function (a, b) {
            if (a.tripAdvisorRating < b.tripAdvisorRating)
                return -1;
            if (a.tripAdvisorRating > b.tripAdvisorRating)
                return 1;
            return 0;
        });
    }
    CopyOfHotels = sortHotelArray.slice();
    DisplayHotelsList();    
    inthetripfn();
}

function AttractionFilters(restaurant_forsort,att_or_res)
{   
    var filterstr = [],filterstrRate = [],filteredResult = [];
    var filtAttr = "",typOfPlc="";
    if (att_or_res == '1')
    {
        $("input:checkbox[name=RestRating]:checked").each(function () {
            if ($(this).val() == 1) {
                filterstr.push("el.plc_Roundof == 1");
            }
            else if ($(this).val() == 2) {
                filterstr.push("el.plc_Roundof == 2");
            }
            else if ($(this).val() == 3) {
                filterstr.push("el.plc_Roundof == 3");
            }
            else if ($(this).val() == 4) {
                filterstr.push("el.plc_Roundof == 4");
            }
            else if ($(this).val() == 5) {
                filterstr.push("el.plc_Roundof == 5");
            }
        });
        $("input:checkbox[name=RestPrice]:checked").each(function () {
            if ($(this).val() == 1) {
                filterstrRate.push("el.plc_price == 1 || el.plc_price == 0");
            }
            else if ($(this).val() == 2) {
                filterstrRate.push("el.plc_price == 2");
            }
            else if ($(this).val() == 3) {
                filterstrRate.push("el.plc_price == 3");
            }
            else if ($(this).val() == 4) {
                filterstrRate.push("el.plc_price == 4");
            }
        });
        typOfPlc = "Restaurant";
    }
    else
    {
        $("input:checkbox[name=AttrRating]:checked").each(function () {
            if ($(this).val() == 1) {
                filterstr.push("el.plc_Roundof == 1");
            }
            else if ($(this).val() == 2) {
                filterstr.push("el.plc_Roundof == 2");
            }
            else if ($(this).val() == 3) {
                filterstr.push("el.plc_Roundof == 3");
            }
            else if ($(this).val() == 4) {
                filterstr.push("el.plc_Roundof == 4");
            }
            else if ($(this).val() == 5) {
                filterstr.push("el.plc_Roundof == 5");
            }
        });
        $("input:checkbox[name=AttrPrice]:checked").each(function () {
            if ($(this).val() == 1) {
                filterstrRate.push("el.plc_price == 1 || el.plc_price == 0");
            }
            else if ($(this).val() == 2) {
                filterstrRate.push("el.plc_price == 2");
            }
            else if ($(this).val() == 3) {
                filterstrRate.push("el.plc_price == 3");
            }
            else if ($(this).val() == 4) {
                filterstrRate.push("el.plc_price == 4");
            }
        });
        typOfPlc = "Attractions";
    }
    var fullquerystr = "", querystr1 = "", querystr = "";
    if(filterstrRate.length>0)
    {
        querystr1 = filterstrRate.join("||");
    }
    if(filterstr.length>0)
    {
        querystr = filterstr.join("||");
    }
    if (querystr1 != "" && querystr != "")
    {
        fullquerystr = "(" + querystr + ") && (" + querystr1 + ")";
    }
    else if(querystr1 != "" && querystr == "")
    {
        fullquerystr = querystr1;
    }
    else if (querystr1 == "" && querystr != "") {
        fullquerystr = querystr;
    }
    else
    {
        fullquerystr = "";
    }

    //***Modified by Yuvapriya on 26 Sep 16
  
    var tmpdata = [];
    for (var indx = 0, lmt = manualdata.length; indx < lmt; indx++) {
        if (manualdata[indx].ctyName == $('#selectedcity').html()) {
            if (typOfPlc = "Attractions") {
                if (manualdata[indx].attrdata) {
                    existsVal = 1;
                    tmpdata = manualdata[indx].attrdata;
                    break;
                }
            }
            else if (typOfPlc = "Restaurant") {
                if (manualdata[indx].restdata) {
                    tmpdata = manualdata[indx].restdata;
                    break;
                }
            }
        }
    }

    if (tmpdata.length > 0) {
        for (var tempcnt = 0; tempcnt < tmpdata.length; tempcnt++) {
            var latt = 0, longt = 0;
            if (typOfPlc = "Attractions") {
                latt = tmpdata[tempcnt].AttrLat;
                longt = tmpdata[tempcnt].AttrLng;
            }
            else if (typOfPlc = "Restaurant") {
                latt = tmpdata[tempcnt].RestLat;
                longt = tmpdata[tempcnt].RestLng;
            }
            var cnt = 0;
            $(".place-box-right").each(function () {
                $(this).find(".place").each(function () {
                    if ($(this).id == tmpdata[tempcnt].Id) {                        
                        cnt = 1;
                    }
                });
            });
            var IsTrip = "Add Trip";
            if (cnt == 0) {
                IsTrip = " Add Trip";
            }
            else {
                IsTrip = "In the Trip";
            }
           
            if (tmpdata[tempcnt].Street == "undefined") {
                tmpdata[tempcnt].Street = " ";
            }
            if (tmpdata[tempcnt].Photo == "undefined") {
                tmpdata[tempcnt].Photo = " ";
            } 
            if (tmpdata[tempcnt].Website == "undefined") {
                tmpdata[tempcnt].Website = " ";
            }
           
            restaurant_forsort.push({
                plc_name: tmpdata[tempcnt].Name,
                plc_id: tmpdata[tempcnt].Id,
                plc_rating: tmpdata[tempcnt].Rating,
                plc_Roundof: " ",
                plc_lat: latt,
                plc_lng: longt,
                plc_address: tmpdata[tempcnt].Street,
                plc_photo: "../Content/images/AttractionImage/" + tmpdata[tempcnt].Photo,
                plc_type: tmpdata[tempcnt].Type,
                plc_trip: IsTrip,
                plc_reviews: 0 ,
                plc_price: tmpdata[tempcnt].Price,
                plc_web: tmpdata[tempcnt].Website,
                plc_manual: 1
            });
        }
    }



    //***END

    if (fullquerystr != "")
    {
        filteredResult = restaurant_forsort.filter(function (el) {
            return eval(fullquerystr);
        });
        if (restaurant_forsort.length > 0)
        {
            apiStatus = 0;
        }
        $('#rhaa1').html("");
        for (var position = 0, ResultsLength = filteredResult.length; position < ResultsLength; position++) {
            var id = filteredResult[position].plc_id;
            var Roundof = filteredResult[position].plc_Roundof;
            var Rating = filteredResult[position].plc_rating;
            var lat = filteredResult[position].plc_lat;
            var lng = filteredResult[position].plc_lng;
            var Name = filteredResult[position].plc_name;
            var Address = filteredResult[position].plc_address;
            var Photo = filteredResult[position].plc_photo;
            var Trip = filteredResult[position].plc_trip;
            var types = filteredResult[position].plc_type;
            var reviews = filteredResult[position].plc_reviews;
            var pricelevel = filteredResult[position].plc_price;
            var weblink = filteredResult[position].plc_web;
            var manentry = filteredResult[position].plc_manual;
            var items = '';
            alert('loadsugg - attr fileter');
            alert(Photo);
            items += "<div id='rh" + id + "'></div><input type='hidden' class='hide'  id=" + id + " value='" + types + "'><input type='hidden' id='rates_" + id + "' value='" + Rating + "'><input type='hidden' id='ratefilter_" + id + "' value='" + Roundof + "'><input type='hidden' id='latarray" + id + "' value='" + lat + "'><input type='hidden' id='lngarray" + id + "' value='" + lng + "'><aside class='resort-row' id='res_" + id + "'><div class='resort-row1' id='divres_" + id + "'>";
            items += "<input type='hidden' class='reviews' value='" + reviews + "'/>";
            items += "<input type='hidden' class='priceLev' value='" + pricelevel + "'/>";
            items += "<input type='hidden' class='webhidden' value='" + weblink + "'/>";
            items += "<input type='hidden' id='latarray" + id + "' value='" + lat + "' class='reslat'><input type='hidden' id='lngarray" + id + "' value='" + lng + "' class='reslng'>";
            items += "<input type='hidden' id='isME" + id + "' value='"+manentry+"' class='resME'>";
            items += "<figure class='resort-img'><div class='tooltip'>Drag to add</div><img src='" + Photo + "' alt='' id='img_" + id + "' />";
            items += "</figure>";
            items += "<div class='resort-details'><div class='r-left'>";
            items += "<h3 id='h" + id + "'>" + Name + "  </h3><br/>";
            items += "<p style='height:40px; overflow-y:hidden;' id='p" + id + "'>" + Address + "</p>";
            if (weblink != "No site") {
                items += "<p style='height:18px; overflow-y:hidden;width:100%;text-overflow: ellipsis;white-space: nowrap;overflow: hidden;' id='web" + id + "'><b>Website : </b><a target='_blank' href='" + weblink + "' style='text-decoration: underline;'>" + weblink + "</a></p>";
            }
            items += "<ul id='rat" + id + "'><b>Ratings : </b>";
            if (att_or_res == '1')
            {
                for (var loopvar_rate = 1; loopvar_rate <= 5; loopvar_rate++) {
                    if (Rating >= loopvar_rate) {
                        items += "<li><img src='../Content/images/coffee_full.png' /></li>";
                    }
                    else if (Rating > (loopvar_rate - 1) && Rating < loopvar_rate) {
                        items += "<li><img src='../Content/images/coffee_half.png' /></li>";
                    }
                    else {
                        items += "<li><img src='../Content/images/coffee_grey_full.png' /></li>";
                    }
                }
            }
            else
            {
                for (var loopvar_rate = 1; loopvar_rate <= 5; loopvar_rate++) {
                    if (Rating >= loopvar_rate) {
                        items += "<li><img src='../Content/images/tower_full.png' /></li>";
                    }
                    else if (Rating > (loopvar_rate - 1) && Rating < loopvar_rate) {
                        items += "<li><img src='../Content/images/tower_half.png' /></li>";
                    }
                    else {
                        items += "<li><img src='../Content/images/tower_grey_full.png' /></li>";
                    }
                }
            }
            if (reviews != "No rev") {
                //items += "<li>(" + reviews + " reviews)</li>";
            }
            items += "</ul>";
            if (pricelevel==0) {
                items += "<p style='height: 18px;' id='price" + id + "'><b> Price Level:</b>$</p>";
            }
            else if (pricelevel==1) {
                items += "<p style='height: 18px;' id='price" + id + "'><b> Price Level:</b>$</p>";
            }
            else if (pricelevel==2) {
                items += "<p style='height: 18px;' id='price" + id + "'><b> Price Level:</b>$$</p>";
            }
            else if (pricelevel==3) {
                items += "<p style='height: 18px;' id='price" + id + "'><b> Price Level:</b>$$$</p>";
            }
            else if (pricelevel==4) {
                items += "<p style='height: 18px;' id='price" + id + "'><b> Price Level:</b>$$$$</p>";
            }
            if (att_or_res == '1') {
                items += "<div class='book-add'><span><a href='#' id='inner" + id + "' class='add-trip' onclick='return RestaurantToleft(\"" + id + "\");'>" + Trip + "</a></span></div>";
            }
            else if (att_or_res == '2') {
                items += "<div class='book-add'><span><a href='#' id='inner" + id + "' class='add-trip' onclick='return AttractionToleft(\"" + id + "\");'>" + Trip + "</a></span></div>";
            }
            items += "</div> ";
            items += "<div class='r-right'>";
            items += " <ul><li><a href='#'><img src='../Content/images/del-icon.png' alt='' class='del-show' onclick='fun2'/></a></li>";
            if (manentry == '0') {
                items += "<li><a class='fancybox' href='#infowindow' onclick='return info(\"" + id + "\",\"" + lat + "\",\"" + lng + "\",\"" + types + "\",\"" + att_or_res + "\");'><img src='../Content/images/info-icon.png' alt='' class='example1demo' /></a></li></ul>";
            }
            else if (manentry == '1') {
                items += "<li><a class='fancybox' href='#infowindow' onclick='return infomanual(\"2\",\"" + id + "\");'><img src='../Content/images/info-icon.png' alt='' class='example1demo' /></a></li></ul>";
            }
            items += "</div></div></div>";
            items += "</aside>";
            $('#rhaa1').append(items);
        }
    }
    $(".dropdown-pad").hide();
    intheTripRHAA(typOfPlc);
}

function isUrlExists(url, cb) {
    jQuery.ajax({
        url: url,
        dataType: 'text',
        type: 'GET',
        complete: function (xhr) {
            if (typeof cb === 'function')
                cb.apply(this, [xhr.status]);
        }
    });
}
