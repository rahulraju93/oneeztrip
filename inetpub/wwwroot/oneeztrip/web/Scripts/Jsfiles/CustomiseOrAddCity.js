﻿$(document).ready("popup.html #register", function () {
    var notify_alert = noty({
        layout: 'topRight',
        text: 'Load was performed.',
        type: 'success',
        timeout: 3000,
        maxVisible: 1,
        animation: {
            open: { height: 'toggle' },
            close: { height: 'toggle' },
            easing: 'swing',
            speed: 500
        }
    });
});

(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=744549145580822&version=v2.0";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

$('#PayButton').on('click', function () {
    var name = "";
    var latitude = "";
    var longitude = "";
    var Days = "", Save = "";
    var Count = $('#SelectedCities').find('.row1').length;
    var TripId = $('#LoggedTripId').val();
    var Var_TotalNoOfDays = 0, VarTotal_NoOfCity = 0;
    if ($('#SaveMovementAddCity').val() != 1 && $('#PrevView').val() != 'Firstday') {
        for (var i = 0; i < Count; i++) {
            latitude += "|" + $('#SelectedCities').find('.row1').eq(i).find('.cls_lat').val();
            longitude += "|" + $('#SelectedCities').find('.row1').eq(i).find('.cls_lng').val();
            name += "|" + $('#SelectedCities').find('.row1').eq(i).find('.cus').find('h3').html();
            Days += "|" + 1;
            Save += "|" + 0;
        }
        Var_TotalNoOfDays = Count;
        VarTotal_NoOfCity = Count;
    }
    else {
        for (var i = 0; i < Count; i++) {
            if (i == 0) {
                latitude += $('#SelectedCities').find('.row1').eq(i).find('.cls_lat').val();
                longitude += $('#SelectedCities').find('.row1').eq(i).find('.cls_lng').val();
                name += $('#SelectedCities').find('.row1').eq(i).find('.cus').find('h3').html();
                Days += $('#SelectedCities').find('.row1').eq(i).find('.cls_date').val();
                Save += 0;
                Var_TotalNoOfDays += Number($('#SelectedCities').find('.row1').eq(i).find('.cls_date').val());
                VarTotal_NoOfCity = ++VarTotal_NoOfCity;
            }
            else {
                latitude += "|" + $('#SelectedCities').find('.row1').eq(i).find('.cls_lat').val();
                longitude += "|" + $('#SelectedCities').find('.row1').eq(i).find('.cls_lng').val();
                name += "|" + $('#SelectedCities').find('.row1').eq(i).find('.cus').find('h3').html();
                Days += "|" + $('#SelectedCities').find('.row1').eq(i).find('.cls_date').val();
                Save += "|" + 0;
                Var_TotalNoOfDays += Number($('#SelectedCities').find('.row1').eq(i).find('.cls_date').val());
                VarTotal_NoOfCity = ++VarTotal_NoOfCity;
            }
        }
    }
        $.getJSON('../Paypal/getAddCityDetails', { 'firstcity': name, 'lat': latitude, 'lng': longitude, 'TofDays': Days, 'SaveFlag': Save, 'Amount': $('#PriceForSingleCity').val(), 'noday': Var_TotalNoOfDays, 'NofDays': VarTotal_NoOfCity, 'ReturnPageName': $('#PrevView').val(), 'IsAddCity': 'Yes' }, function () {
            var url = "../Paypal/ValidateCommand?citiesName=" + name + "&totalPrice=" + $('#PriceForSingleCity').val();
            window.location.href = url;
        });
return false;
});

function test() {
    var dataObject ={
                           TripId: $('#LoggedTripId').val()
                       };
    $.ajax({
        url: '../Trip/UpdateTripStatus',
        type: "POST",
        data: dataObject,
        dataType: "json",
        success: function (result) {
        },
        error: function () {
        }
    });
    $.ajax({
        type: "POST",
        url: '../Trip/ClearLoginSession',
        success: function () {
        }
    });
    FB.logout();
}

function LeaveFromCurrentPage(Option) {
    noty({
        layout: 'topRight',
        type: 'confirm',
        maxVisible: 1,
        modal: true,
        text: 'Do you want to leave this page?',
        buttons: [
            {
                addClass: 'btn btn-primary', text: 'Ok', onClick: function ($noty) {
                    var dataObject =
                        {
                            TripId: $('#LoggedTripId').val()
                        };
                    $.ajax({
                        url: '../Trip/UpdateTripStatus',
                        type: "POST",
                        data: dataObject,
                        dataType: "json",
                        success: function (result) {
                            if (Option == "1") {
                                window.location.href = "../Trip/Index";
                            }
                            else if(Option == "2"){
                                window.location.href = "../Trip/MyTrips";
                            }
                            else if(Option=="3")
                            {
                                window.location.href = "../Trip/ViewMyProfile";
                            }
                        },
                        error: function () {
                        }
                    });
                }
            },
            {
                addClass: 'btn btn-danger', text: 'Cancel', onClick: function ($noty) {
                    $noty.close();
                    return false;
                }
            }
        ]
    });
}