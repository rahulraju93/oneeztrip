﻿$('#content-y').on('click', '.travedelimg', function () {
    keepTrackForsave++;
    var findTrMode = $(this).parent().parent().attr('id');
    noty({
        layout: 'topRight',
        type: 'confirm',
        maxVisible: 1,
        modal: true,
        text: 'Are you sure you want to delete the selected Travelmode?',
        buttons: [
            {
                addClass: 'btn btn-primary', text: 'Ok', onClick: function ($noty) {
                    $('#' + findTrMode).fadeOut('slow', function () { $(this).remove() });
                    $noty.close();
                }
            },
            {
                addClass: 'btn btn-danger', text: 'Cancel', onClick: function ($noty) {
                    $noty.close();
                }
            }
        ]
    });
        

   // $(this).parent().parent().fadeOut('slow', function () { $(this).remove() });
});

$('#content-y').on('click', '.travel li a', function () {
    keepTrackForsave++;
    previous_travelmode = $(this).parent().parent().parent().find('.active').data('value');
    previous_travelduration = $(this).parent().parent().parent().find('p').html();
    previous_height = $(this).parent().parent().parent().height();
    var mode = $(this).data('value');
    var travel_id = $(this).parent().parent().parent().attr('id');
    $('#' + travel_id + ' li a').removeClass('active');
    $(this).addClass('active');
    $('#' + travel_id).find('p').html("<img src='../Content/images/edit.png' alt='' class='editimg' style='height: 15px;width: 15px;'>");
    travelmodeid = travel_id;
    travemode = mode;
    traveldur();
});

$('#content-y').on('click', '.editimg', function () {
    var fndPlcbx = $(this).parents('.place-box-right').attr('id');
    keepTrackForsave++;
    var txtvalis = "", final_mins = "";
    var previous_travelmodetext = $(this).parents(".travel").find("p").text();
    var prevTmHit = $(this).parents(".travel").height();
    var modefortxt = "";
    modefortxt = $(this).parent().parent().find('ul li .active').data('value'); //Get the Active travel mode..
    var tpid = "tpid1123";
    $(this).parent().attr('id', tpid);
    $(this).parent().html('Travel time by ' + modefortxt + ' is <input type="text" value="" placeholder="time" style="width:30px;height:16px;">');
    $('#tpid1123').find('input').focus();
    $("#tpid1123").keypress(function (eve) {
     if (eve.which != 8 && eve.which != 0 && (eve.which < 48 || eve.which > 57)) {
         var notify_alert = noty({
             layout: 'topRight',
             text: 'Digits only. Enter time in mins',
             type: 'warning',
             timeout: 2000,
             maxVisible: 1,
             animation: {
                 open: { height: 'toggle' },
                 close: { height: 'toggle' },
                 easing: 'swing',
                 speed: 500
             }
         });
               return false;
    }
});

    $('#tpid1123').find('input').blur(function () {
        if ($(this).val() == "") {
            var notify_alert = noty({
                layout: 'topRight',
                text: 'Please enter the travel time',
                type: 'warning',
                timeout: 3000,
                maxVisible: 1,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                }
            });
            $(this).focus();
        }
        else {

            txtvalis = $(this).val();
            if (parseInt(txtvalis) <= 180) {
                if (parseInt(txtvalis) >= 60) {
                    var cal_hrs = Math.floor(txtvalis / 60);
                    var cal_mins = txtvalis % 60;
                    if (cal_hrs > 1) {
                        if (cal_mins > 1) {
                            final_mins = cal_hrs + " hours " + cal_mins + " mins";
                        }
                        else if(cal_mins == 1)
                        {
                            final_mins = cal_hrs + " hours " + cal_mins + " min";
                        }
                        else if(cal_mins == 0)
                        {
                            final_mins = cal_hrs + " hours ";
                        }
                    }
                    else {
                        if (cal_mins > 1) {
                            final_mins = cal_hrs + " hour " + cal_mins + " mins";
                        }
                        else if (cal_mins == 1) {
                            final_mins = cal_hrs + " hours " + cal_mins + " min";
                        }
                        else if (cal_mins == 0) {
                            final_mins = cal_hrs + " hours ";
                        }
                    }
                }
                else {
                    if (txtvalis > 1) {
                        final_mins = txtvalis + " mins";
                    }
                    else {
                        final_mins = txtvalis + " min";
                    }
                }
                $('#tpid1123').html("Travel time by " + modefortxt + " is " + final_mins + " <img src='../Content/images/edit.png' alt='' class='editimg' style='height: 15px;width: 15px;'>");
                if (modefortxt == "car")
                {
                    $('#tpid1123').parent(".travel").find(".hidcar").val(final_mins);
                }
                else if (modefortxt == "train")
                {
                    $('#tpid1123').parent(".travel").find(".hidtrain").val(final_mins);
                }
                else if (modefortxt == "plane")
                {
                    $('#tpid1123').parent(".travel").find(".hidplane").val(final_mins);
                }
                else if (modefortxt == "walk")
                {
                    $('#tpid1123').parent(".travel").find(".hidwalk").val(final_mins);
                }
                else if (modefortxt == "bicycle")
                {
                    $('#tpid1123').parent(".travel").find(".hidcycle").val(final_mins);
                }
                var travelem_id = $('#tpid1123').parent().attr('id');
                $('#compl').find('#' + travelem_id).css("height", "");
                var day = "day";
                var days = "days";
                var hours = "hours";
                var hour = "hour";
                var mins = "mins";
                if (final_mins.indexOf(hour) != -1) {
                    var array = final_mins.split(' ');
                    var amount = array[0] / .50;
                    if (array[0] < 50) {
                        amount = array[0] / .50;
                    }
                    else {
                        amount = 50 / .50;
                    }
                    var timespent = amount * 58;
                    if (final_mins.indexOf(mins) != -1) {
                            timespent = array[2] / .50 + timespent;
                            $('#compl').find('#' + travelem_id).css("height", timespent);
                    }
                    else {
                        $('#compl').find('#' + travelem_id).css("height", timespent);
                    }
                }
                else if (final_mins.indexOf(mins) != -1) {
                    var array = final_mins.split(' ');
                    if (array[0] > 20) {
                        var amount = array[0] / 30;
                        var timespent = amount * 58;

                        $('#compl').find('#' + travelem_id).css("height", timespent);
                    }
                }
                var FitOrNot = 0;
                var incr_num_plcbx = 0, PlcsTm_height = 0;
                $("#" + fndPlcbx).find(".place-box").each(function () {
                    incr_num_plcbx++;
                });
                $("#" + fndPlcbx).find(".place").each(function () {
                    PlcsTm_height = PlcsTm_height + $(this).height();
                });
                $("#" + fndPlcbx).find(".travel").each(function () {
                    PlcsTm_height = PlcsTm_height + $(this).height();
                });
                PlcsTm_height = PlcsTm_height + ((incr_num_plcbx - 1) * 50);
                if (PlcsTm_height > 2100) {
                    FitOrNot = 0;
                }
                else {
                    FitOrNot= 1;
                }
                if (!FitOrNot)
                {
                    var notify_alert = noty({
                        layout: 'topRight',
                        text: 'Exceeds day limit can\'t set the time you have enter..',
                        type: 'warning',
                        timeout: 3000,
                        maxVisible: 1,
                        animation: {
                            open: { height: 'toggle' },
                            close: { height: 'toggle' },
                            easing: 'swing',
                            speed: 500
                        }
                    });
                    $('#compl').find('#' + travelem_id).find("p").html(previous_travelmodetext+" <img src='../Content/images/edit.png' alt='' class='editimg' style='height: 15px;width: 15px;'>");
                    $('#compl').find('#' + travelem_id).css("height", prevTmHit);

                }
                $('#tpid1123').removeAttr('id');                
            }
            else
            {
                var notify_alert = noty({
                    layout: 'topRight',
                    text: 'The minutes that you entered should less than 180(3 hours)',
                    type: 'warning',
                    timeout: 3000,
                    maxVisible: 1,
                    animation: {
                        open: { height: 'toggle' },
                        close: { height: 'toggle' },
                        easing: 'swing',
                        speed: 500
                    }
                });
                $(this).val("");
                $(this).focus();
            }
        }
    });
});

function traveldur() {
    $('#LogTripId').val('');
    var preplace = "";
    var nexplace = "";
    if ($("#" + travelmodeid).prev(".place").length) {
        preplace = $("#" + travelmodeid).prev(".place").find(".p-left h4").text() + "," + $(this).prev('.place').find(".p-left p").text();
    }
    if ($("#" + travelmodeid).next(".place").length) {
        nexplace = $("#" + travelmodeid).next(".place").find(".p-left h4").text() + "," + $(this).next('.place').find(".p-left p").text();
    }
    var checkexists = 0;
    if (travemode === "walk") {
        if ($('#' + travelmodeid).find('.hidwalk').val() != "") {
            var walkDur = $('#' + travelmodeid).find('.hidwalk').val();
            $('#' + travelmodeid).find("p").text("Time taken by walk is " + walkDur);
            $('#' + travelmodeid).find("p").append("<img src='../Content/images/edit.png' alt='' class='editimg' style='height: 15px;width: 15px;'>");
        }
        else {
            var origin = preplace,
        destination = nexplace,
        tservice = new google.maps.DistanceMatrixService();
            var wdur = tservice.getDistanceMatrix({
                origins: [origin],
                destinations: [destination],
                travelMode: google.maps.TravelMode.WALKING,
            }, callbackwtm);
        }
    }
    else if (travemode === "bicycle") {
        if ($('#' + travelmodeid).find('.hidcycle').val() != "") {
            var cycleDur = $('#' + travelmodeid).find('.hidcycle').val();
            $('#' + travelmodeid).find("p").text("Time taken by bicycle is " + cycleDur);
            $('#' + travelmodeid).find("p").append("<img src='../Content/images/edit.png' alt='' class='editimg' style='height: 15px;width: 15px;'>");
        }
        else {
            var origin = preplace,
            destination = nexplace,
            tservice = new google.maps.DistanceMatrixService();
            var wdur = tservice.getDistanceMatrix({
                origins: [origin],
                destinations: [destination],
                travelMode: google.maps.TravelMode.BICYCLING,
            }, callbackbtm);
        }
    }
    else if (travemode === "car") {
        //if ($('#' + travelmodeid).find('.hidcar').val() != "") {
        //    var carDur = $('#' + travelmodeid).find('.hidcar').val();
        //    $('#' + travelmodeid).find("p").text("Time taken by car is " + carDur);
        //    $('#' + travelmodeid).find("p").append("<img src='../Content/images/edit.png' alt='' class='editimg' style='height: 15px;width: 15px;'>");
        //    $('#' + travelmodeid).css("height", "");
        //    var hours = "hours";
        //    var hour = "hour";
        //    var mins = "mins";
        //    if (carDur.indexOf(hours) != -1) {
        //        carDur = "2 hours";
        //    }
        //    if (carDur.indexOf(hour) != -1) {
        //        var array = carDur.split(' ');
        //        var amount = array[0] / .50;
        //        if (array[0] < 50) {
        //            amount = array[0] / .50;
        //        }
        //        else {
        //            amount = 50 / .50;
        //        }
        //        var timespent = amount * 58;
        //        if (carDur.indexOf(mins) != -1) {
        //            timespent = array[2] / .50 + timespent;
        //            $('#' + travelmodeid).css("height", timespent);
        //        }
        //        else {
        //            $('#' + travelmodeid).css("height", timespent);
        //        }
        //    }
        //    else if (carDur.indexOf(mins) != -1) {
        //        var array = carDur.split(' ');
        //        if (array[0] > 20) {
        //            var amount = array[0] / 30;
        //            var timespent = amount * 58;
        //            $('#' + travelmodeid).css("height", timespent);
        //        }
        //    }
        //}
        //else {
        //alert('car get response');
        var origin = preplace,
        destination = nexplace,
        tservice = new google.maps.DistanceMatrixService();
        var wdur = tservice.getDistanceMatrix({
            origins: [origin],
            destinations: [destination],
            travelMode: google.maps.TravelMode.DRIVING,
        }, callbackc2tm); //callbackd2tm);

        //}
    }  //--Added by Yuvapriya To get travel mode - Train duretion - on 13Sep 16
    else if (travemode == "train") {
        // alert('mycode');
        
        if ($('#' + travelmodeid).find('.hidtrain').val() != "") {
            var walkDur = $('#' + travelmodeid).find('.hidtrain').val();
            $('#' + travelmodeid).find("p").text("Time taken by Train is " + walkDur);
            $('#' + travelmodeid).find("p").append("<img src='../Content/images/edit.png' alt='' class='editimg' style='height: 15px;width: 15px;'>");
        }
        else {
            var origin = preplace,
        destination = nexplace,
        tservice = new google.maps.DistanceMatrixService();
            //alert('tservice: ' + tservice);
            var wdur = tservice.getDistanceMatrix({
                origins: [origin],
                destinations: [destination],
                travelMode: google.maps.TravelMode.TRANSIT,
                transitOptions: {
                    modes: ['TRAIN']
                },

            }, callbacktt2tm);
            // alert('wdur: ' + wdur);
        }
    }//END
    else if (travemode === "train1") {
        if ($('#' + travelmodeid).find('.hidtrain').val() != "") {
            var trainDur = $('#' + travelmodeid).find('.hidtrain').val();
            $('#' + travelmodeid).find("p").text("Time taken by train is " + trainDur);
            $('#' + travelmodeid).find("p").append("<img src='../Content/images/edit.png' alt='' class='editimg' style='height: 15px;width: 15px;'>");
        }
        else {
            $('#' + travelmodeid).find("p").text("No data available for this travelmode");
            $('#' + travelmodeid).find("p").append("<img src='../Content/images/edit.png' alt='' class='editimg' style='height: 15px;width: 15px;'>");
        }
    }
    else if (travemode === "plane") {
        if ($('#' + travelmodeid).find('.hidplane').val() != "") {
            var planeDur = $('#' + travelmodeid).find('.hidplane').val();
            $('#' + travelmodeid).find("p").text("Time taken by plane is " + planeDur);
            $('#' + travelmodeid).find("p").append("<img src='../Content/images/edit.png' alt='' class='editimg' style='height: 15px;width: 15px;'>");
        }
        else {
            $('#' + travelmodeid).find("p").text("No data available for this travelmode");
            $('#' + travelmodeid).find("p").append("<img src='../Content/images/edit.png' alt='' class='editimg' style='height: 15px;width: 15px;'>");
        }
    }
}

function callbackd2tm(response, status) {
    var traveltime_drive = $('#' + travelmodeid).find('.hidcar').val();
    var cal_height = traveltime_drive;
    $('#' + travelmodeid).find("p").text("Time taken by car is " + traveltime_drive);
    $('#' + travelmodeid).find("p").append("<img src='../Content/images/edit.png' alt='' class='editimg' style='height: 15px;width: 15px;'>");
    $('#compl').find('#' + travelmodeid).css("height", "");
    var hours = "hours";
    var hour = "hour";
    var mins = "mins";
    if (traveltime_drive.indexOf(hours) != -1) {
        traveltime_drive = "2 hours";
    }
    if (traveltime_drive.indexOf(hours) != -1 || traveltime_drive.indexOf(hour) != -1) {
        var array = traveltime_drive.split(' ');
        var amount = array[0] / .50;
        if (array[0] < 50) {
            amount = array[0] / .50;
        }
        else {
            amount = 50 / .50;
        }
        var timespent = amount * 58;
        if (traveltime_drive.indexOf(mins) != -1) {
                timespent = array[2] / .50 + timespent;
                $('#compl').find('#' + travelmodeid).css("height", timespent);
        }
        else {
            $('#compl').find('#' + travelmodeid).css("height", timespent);
        }
    }
    else if (traveltime_drive.indexOf(mins) != -1) {
        var array = traveltime_drive.split(' ');
        if (array[0] > 20) {
            var amount = array[0] / 30;
            var timespent = amount * 58;
            $('#compl').find('#' + travelmodeid).css("height", timespent);
        }
    }
}

function callbacktt2tm(response, status) {
    
    var dura = "--";
    if (status == "OK") {
        var dest = response.destinationAddresses[0];
        var orig = response.originAddresses[0];
        if (response.rows[0].elements[0].duration) {
            dura = response.rows[0].elements[0].duration.text;

            var hours = "hours";
            var hour = "hour";
            var mins = "mins";
            if (dura.indexOf(hours) != -1) {
                dura = "2 hours";
            }
            if (dura.indexOf(hours) != -1 || dura.indexOf(hour) != -1) {
                var array = dura.split(' ');
                var amount = array[0] / .50;
                if (array[0] < 50) {
                    amount = array[0] / .50;
                }
                else {
                    amount = 50 / .50;
                }
                var timespent = amount * 58;
                if (dura.indexOf(mins) != -1) {
                    var array = dura.split(' ');
                    if (array[0] < 15) {
                        dura = '15 mins';
                    }
                }                
            }
            else if (dura.indexOf(mins) != -1) {
                var array = dura.split(' ');
                if (array[0] < 15) {
                    dura = '15 mins';
                }
            }
            
            $('#' + travelmodeid).find('.hidtrain').val(dura);
            $('#' + travelmodeid).find("p").text("Time taken by train is " + dura);
            $('#' + travelmodeid).find("p").append("<img src='../Content/images/edit.png' alt='' class='editimg' style='height: 15px;width: 15px;'>");

        }
        else {
            dura = "No dura..";
        }
       
        $('#compl').find('#' + travelmodeid).css("height", "");
    }
    else {
        dura = "No dura..";
    }

    if (dura === "No dura..") {
        $('#' + travelmodeid).find("p").text("No data available for this travelmode");
        $('#' + travelmodeid).find("p").append("<img src='../Content/images/edit.png' alt='' class='editimg' style='height: 15px;width: 15px;'>");

    }

}

function callbackc2tm(response, status) {
   
  
    var dura = "--";
    
    if (status == "OK") {
      
        var dest = response.destinationAddresses[0];
        var orig = response.originAddresses[0];
        if (response.rows[0].elements[0].duration) {
          
            dura = response.rows[0].elements[0].duration.text;
            var splitTraveltime = dura.split(' ');
            for (var loopthr = 0, TravelTimeFind = splitTraveltime.length; loopthr < TravelTimeFind; loopthr++) {
                if (splitTraveltime[loopthr] == "min" || splitTraveltime[loopthr] == "mins") {
                    splitTraveltime[loopthr - 1] = Math.ceil(splitTraveltime[loopthr - 1] / 5) * 5;
                }
            }
            dura = splitTraveltime.join(" ");
        }
        else {
            
            dura = "No dura..";
        }
        $('#compl').find('#' + travelmodeid).css("height", "");
       
        var day = "day";
        var days = "days";
        var hours = "hours";
        var hour = "hour";
        var mins = "mins";
        ;
    }
    else {
        dura = "No dura..";
    }
    if (dura === "No dura..") {
       
        var pre_lat = "", pre_lng = "", next_lat = "", next_lng = "";
        if ($("#" + travelmodeid).prev('.place').length) {
            pre_lat = $("#" + travelmodeid).prev('.place').find('.hidlat').val();
            pre_lng = $("#" + travelmodeid).prev('.place').find('.hidlng').val();
        }
        else if ($("#" + travelmodeid).prev('.resort-row').length) {
            pre_lat = $("#" + travelmodeid).prev('.resort-row').find('.reslat').val();
            pre_lng = $("#" + travelmodeid).prev('.resort-row').find('.reslng').val();
        }
        if ($("#" + travelmodeid).next('.place').length) {
            next_lat = $("#" + travelmodeid).next('.place').find('.hidlat').val();
            next_lng = $("#" + travelmodeid).next('.place').find('.hidlng').val();
        }
        else if ($("#" + travelmodeid).next('.resort-row').length) {
            next_lat = $("#" + travelmodeid).next('.resort-row').find('.reslat').val();
            next_lng = $("#" + travelmodeid).next('.resort-row').find('.reslng').val();
        }

        var dista = Haversine(pre_lat, pre_lng, next_lat, next_lng);

       
        var kmtomin1 = dista / 20;
        var kmtomin2 = kmtomin1 * 60;
        var kmtomin3 = kmtomin2.toFixed();
        
        var final_time = "";
        if (kmtomin3 > 60) {
            var kmtohrs = Math.floor(kmtomin3 / 60);
            var kmtomints = kmtomin3 % 60;

            if (kmtohrs == 1) {
                kmtomints = Math.ceil(kmtomints / 5) * 5;
                final_time = kmtohrs + " hour " + kmtomints + " mins";
            }
            else if (kmtohrs > 1) {
                kmtomints = Math.ceil(kmtomints / 5) * 5;
                final_time = kmtohrs + " hours " + kmtomints + " mins";
            }
            else {
                final_time = kmtomints + " mins";
            }

           
            $('#' + travelmodeid).find("p").text("Time taken by car is " + final_time);
            $('#' + travelmodeid).find("p").append("<img src='../Content/images/edit.png' alt='' class='editimg' style='height: 15px;width: 15px;'>");
        }
        else {
            var traveltime_drive = $('#' + travelmodeid).find('.hidcar').val();
            var cal_height = traveltime_drive;
            $('#' + travelmodeid).find("p").text("Time taken by car is " + traveltime_drive);
            $('#' + travelmodeid).find("p").append("<img src='../Content/images/edit.png' alt='' class='editimg' style='height: 15px;width: 15px;'>");
            $('#compl').find('#' + travelmodeid).css("height", "");
            var hours = "hours";
            var hour = "hour";
            var mins = "mins";
            if (traveltime_drive.indexOf(hours) != -1) {
                traveltime_drive = "2 hours";
            }
            if (traveltime_drive.indexOf(hours) != -1 || traveltime_drive.indexOf(hour) != -1) {
                var array = traveltime_drive.split(' ');
                var amount = array[0] / .50;
                if (array[0] < 50) {
                    amount = array[0] / .50;
                }
                else {
                    amount = 50 / .50;
                }
                var timespent = amount * 58;
                if (traveltime_drive.indexOf(mins) != -1) {
                    timespent = array[2] / .50 + timespent;
                    $('#compl').find('#' + travelmodeid).css("height", timespent);
                }
                else {
                    $('#compl').find('#' + travelmodeid).css("height", timespent);
                }
            }
            else if (traveltime_drive.indexOf(mins) != -1) {
                var array = traveltime_drive.split(' ');
                if (array[0] > 20) {
                    var amount = array[0] / 30;
                    var timespent = amount * 58;
                    $('#compl').find('#' + travelmodeid).css("height", timespent);
                }
            }
        }
    }
    else
    {
        $('#' + travelmodeid).find("p").text("Time taken by car is " + dura);
        $('#' + travelmodeid).find("p").append("<img src='../Content/images/edit.png' alt='' class='editimg' style='height: 15px;width: 15px;'>");

    }
}

function callbackwtm(response, status) {
   
    var dura = "--";
    
    if (status == "OK") {
        
        var dest = response.destinationAddresses[0];
        var orig = response.originAddresses[0];
        if (response.rows[0].elements[0].duration) {
           
            dura = response.rows[0].elements[0].duration.text;
            var splitTraveltime = dura.split(' ');
            for (var loopthr = 0, TravelTimeFind = splitTraveltime.length; loopthr < TravelTimeFind; loopthr++) {
                if (splitTraveltime[loopthr] == "min" || splitTraveltime[loopthr] == "mins") {
                    splitTraveltime[loopthr - 1] = Math.ceil(splitTraveltime[loopthr - 1] / 5) * 5;
                }
            }
            dura = splitTraveltime.join(" ");
        }
        else {
            
            dura = "No dura..";
        }
        $('#compl').find('#' + travelmodeid).css("height", "");
       
        var day = "day";
        var days = "days";
        var hours = "hours";
        var hour = "hour";
        var mins = "mins";
        ;
        if (dura.indexOf(day) != -1 || dura.indexOf(days) != -1 || dura.indexOf(hours) != -1) {
            $('#' + travelmodeid + ' ul li a').removeClass('active');
            $("#" + travelmodeid + " ul li a[data-value='" + previous_travelmode + "']").addClass('active');
            var strdur = previous_travelduration;
            $('#' + travelmodeid).find('p').html(strdur);
            $('#compl').find('#' + travelmodeid).css("height", previous_height);
            var notify_alert = noty({
                layout: 'topRight',
                text: 'You cannot choose this. This travel mode takes too much of your time..',
                type: 'warning',
                timeout: 2000,
                maxVisible: 1,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                }
            });
        }
        else if (dura.indexOf(hour) != -1) {
            var strdur = "Travel time by " + travemode + " is " + dura;
            $('#' + travelmodeid).find('p').html(strdur);
            $('#' + travelmodeid).find("p").append("<img src='../Content/images/edit.png' alt='' class='editimg' style='height: 15px;width: 15px;'>");
            var array = dura.split(' ');
            var amount = array[0] / .50;
            if (array[0] < 50) {
                amount = array[0] / .50;
            }
            else {
                amount = 50 / .50;
            }
            var timespent = amount * 58;
            if (dura.indexOf(mins) != -1) {
                timespent = array[2] / .50 + timespent;
                $('#compl').find('#' + travelmodeid).css("height", timespent);
            }
            else {
                $('#compl').find('#' + travelmodeid).css("height", timespent);
            }
        }
        else if (dura.indexOf(mins) != -1) {
            var array = dura.split(' ');
            if (array[0] > 20) {
                var amount = array[0] / 30;
                var timespent = amount * 58;
                var strdur = "Travel time by " + travemode + " is " + dura;
                $('#' + travelmodeid).find('p').html(strdur);
                $('#' + travelmodeid).find("p").append("<img src='../Content/images/edit.png' alt='' class='editimg' style='height: 15px;width: 15px;'>");
                $('#compl').find('#' + travelmodeid).css("height", timespent);
            }
            else {
                var strdur = "Travel time by " + travemode + " is " + dura;
                $('#' + travelmodeid).find('p').html(strdur);
                $('#' + travelmodeid).find("p").append("<img src='../Content/images/edit.png' alt='' class='editimg' style='height: 15px;width: 15px;'>");
            }
        }
    }
    else {
        dura = "No dura..";
    }
    if (dura === "No dura..") {
       
        var pre_lat = "", pre_lng = "", next_lat = "", next_lng = "";
        if ($("#" + travelmodeid).prev('.place').length) {
            pre_lat = $("#" + travelmodeid).prev('.place').find('.hidlat').val();
            pre_lng = $("#" + travelmodeid).prev('.place').find('.hidlng').val();
        }
        else if ($("#" + travelmodeid).prev('.resort-row').length) {
            pre_lat = $("#" + travelmodeid).prev('.resort-row').find('.reslat').val();
            pre_lng = $("#" + travelmodeid).prev('.resort-row').find('.reslng').val();
        }
        if ($("#" + travelmodeid).next('.place').length) {
            next_lat = $("#" + travelmodeid).next('.place').find('.hidlat').val();
            next_lng = $("#" + travelmodeid).next('.place').find('.hidlng').val();
        }
        else if ($("#" + travelmodeid).next('.resort-row').length) {
            next_lat = $("#" + travelmodeid).next('.resort-row').find('.reslat').val();
            next_lng = $("#" + travelmodeid).next('.resort-row').find('.reslng').val();
        }

        
        var dista = Haversine(pre_lat, pre_lng, next_lat, next_lng);
        
        var kmtomin1 = dista / 20;
        var kmtomin2 = kmtomin1 * 60;
        var kmtomin3 = kmtomin2.toFixed();
       
        var final_time = "";
        if (kmtomin3 > 60) {
            var kmtohrs = Math.floor(kmtomin3 / 60);
            var kmtomints = kmtomin3 % 60;
            if (kmtohrs > 1 && kmtohrs <= 2) {
                if (kmtohrs == 1) {
                    kmtomints = Math.ceil(kmtomints / 5) * 5;
                    final_time = kmtohrs + " hour " + kmtomints + " mins";
                }
                else if (kmtohrs == 2) {
                    kmtomints = Math.ceil(kmtomints / 5) * 5;
                    final_time = kmtohrs + " hours " + kmtomints + " mins";
                }
            }
            else if (kmtohrs > 2) {
                final_time = "Message..";
            }
            else {
                kmtomints = Math.ceil(kmtomints / 5) * 5;
                final_time = kmtohrs + " hours " + kmtomints + " mins";
            }
        }
        else {
            if (kmtomin3 <= 1) {
                final_time = "NoData.."
              
            }
            else {
                kmtomin3 = Math.ceil(kmtomin3 / 5) * 5;
                final_time = parseInt(kmtomin3) + " mins";
            }
        }
       
        if (final_time == "Message..") {
            $('#' + travelmodeid + ' ul li a').removeClass('active');
            $("#" + travelmodeid + " ul li a[data-value='" + previous_travelmode + "']").addClass('active');
            var strdur = previous_travelduration;
            $('#' + travelmodeid).find('p').html(strdur);
            $('#compl').find('#' + travelmodeid).css("height", previous_height);
            var notify_alert = noty({
                layout: 'topRight',
                text: 'You cannot choose this. This travel mode takes too much of your time..',
                type: 'warning',
                timeout: 2000,
                maxVisible: 1,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                }
            });
        }
        else if (final_time == "NoData..") {
            $('#' + travelmodeid).find("p").text("No data available for this travelmode");
            $('#' + travelmodeid).find("p").append("<img src='../Content/images/edit.png' alt='' class='editimg' style='height: 15px;width: 15px;'>");
        }
        else {
            var ftime = parseInt(kmtomin3);
            var ctime = $('#' + travelmodeid).find('.hidcar').val();

            if (ctime.indexOf(day) != -1 || ctime.indexOf(days) != -1 || ctime.indexOf(hours) != -1 || ctime.indexOf(hour) != -1) {
                $('#' + travelmodeid).find("p").text("No data available for this travelmode");
                $('#' + travelmodeid).find("p").append("<img src='../Content/images/edit.png' alt='' class='editimg' style='height: 15px;width: 15px;'>");
            }
            else {
                var array = ctime.split(' ');
                if (ctime.indexOf(mins) != -1 && array[0] < ftime) {
                    $("#" + travelmodeid).find("p").html('Time taken by ' + travemode + ' is ' + final_time);
                    $("#" + travelmodeid).find("p").append(' <img src="../Content/images/edit.png" alt="" class="editimg" style="height: 15px;width: 15px;">');
                    
                    $('#compl').find('#' + travelmodeid).css("height", "");
                    var hours = "hours";
                    var hour = "hour";
                    var mins = "mins";
                    if (final_time.indexOf(hours) != -1 || final_time.indexOf(hour) != -1) {
                        var array = final_time.split(' ');
                        var amount = array[0] / .50;
                        if (array[0] < 50) {
                            amount = array[0] / .50;
                        }
                        else {
                            amount = 50 / .50;
                        }
                        var timespent = amount * 58;
                        if (final_time.indexOf(mins) != -1) {
                            timespent = array[2] / .50 + timespent;
                            $('#compl').find('#' + travelmodeid).css("height", timespent);
                        }
                        else {
                            $('#compl').find('#' + travelmodeid).css("height", timespent);
                        }
                    }
                    else if (final_time.indexOf(mins) != -1) {
                        var array = final_time.split(' ');
                        if (array[0] > 20) {
                            var amount = array[0] / 30;
                            var timespent = amount * 58;
                            $('#compl').find('#' + travelmodeid).css("height", timespent);
                        }
                    }
                }
                else {
                    $('#' + travelmodeid).find("p").text("No data available for this travelmode");
                    $('#' + travelmodeid).find("p").append("<img src='../Content/images/edit.png' alt='' class='editimg' style='height: 15px;width: 15px;'>");
                }
            }
        }
    }
}

function callbackbtm(response, status) {
    var dura = "--";
    if (status == "OK") {
        var dest = response.destinationAddresses[0];
        var orig = response.originAddresses[0];
        if (response.rows[0].elements[0].duration) {
            dura = response.rows[0].elements[0].duration.text;
            var splitTraveltime = dura.split(' ');
            for (var loopthr = 0, TravelTimeFind = splitTraveltime.length; loopthr < TravelTimeFind; loopthr++) {
                if (splitTraveltime[loopthr] == "min" || splitTraveltime[loopthr] == "mins") {
                    splitTraveltime[loopthr - 1] = Math.ceil(splitTraveltime[loopthr - 1] / 5) * 5;
                }
            }
            dura = splitTraveltime.join(" ");
        }
        else {
            dura = "No dura..";
        }
        $('#compl').find('#' + travelmodeid).css("height", "");
        var day = "day";
        var days = "days";
        var hours = "hours";
        var hour = "hour";
        var mins = "mins";
        if (dura.indexOf(day) != -1 || dura.indexOf(days) != -1 || dura.indexOf(hours) != -1) {
            $('#' + travelmodeid + ' ul li a').removeClass('active');
            $("#" + travelmodeid + " ul li a[data-value='" + previous_travelmode + "']").addClass('active');

            var strdur = previous_travelduration;
            $('#' + travelmodeid).find('p').html(strdur);
            $('#compl').find('#' + travelmodeid).css("height", previous_height);
            var notify_alert = noty({
                layout: 'topRight',
                text: 'You cannot choose this. This travel mode takes too much of your time..',
                type: 'warning',
                timeout: 2000,
                maxVisible: 1,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                }
            });
        }
        else if (dura.indexOf(hour) != -1) {
            var strdur = "Travel time by " + travemode + " is " + dura;
            $('#' + travelmodeid).find('p').html(strdur);
            $('#' + travelmodeid).find("p").append("<img src='../Content/images/edit.png' alt='' class='editimg' style='height: 15px;width: 15px;'>");
            var array = dura.split(' ');
            var amount = array[0] / .50;
            if (array[0] < 50) {
                amount = array[0] / .50;
            }
            else {
                amount = 50 / .50;
            }
            var timespent = amount * 58;
            if (dura.indexOf(mins) != -1) {
                    timespent = array[2] / .50 + timespent;
                    $('#compl').find('#' + travelmodeid).css("height", timespent);
            }
            else {
                $('#compl').find('#' + travelmodeid).css("height", timespent);
            }
        }
        else if (dura.indexOf(mins) != -1) {
            var array = dura.split(' ');
            if (array[0] > 20) {
                var amount = array[0] / 30;
                var timespent = amount * 58;
                var strdur = "Travel time by " + travemode + " is " + dura;
                $('#' + travelmodeid).find('p').html(strdur);
                $('#' + travelmodeid).find("p").append("<img src='../Content/images/edit.png' alt='' class='editimg' style='height: 15px;width: 15px;'>");

                $('#compl').find('#' + travelmodeid).css("height", timespent);
            }
            else {
                var strdur = "Travel time by " + travemode + " is " + dura;
                $('#' + travelmodeid).find('p').html(strdur);
                $('#' + travelmodeid).find("p").append("<img src='../Content/images/edit.png' alt='' class='editimg' style='height: 15px;width: 15px;'>");
            }
        }
    }
    else {
        dura = "No dura..";
    }
    if (dura === "No dura..") {
        var pre_lat = "", pre_lng = "", next_lat = "", next_lng = "";
        if ($("#" + travelmodeid).prev('.place').length) {
            pre_lat = $("#" + travelmodeid).prev('.place').find('.hidlat').val();
            pre_lng = $("#" + travelmodeid).prev('.place').find('.hidlng').val();
        }
        else if ($("#" + travelmodeid).prev('.resort-row').length) {
            pre_lat = $("#" + travelmodeid).prev('.resort-row').find('.reslat').val();
            pre_lng = $("#" + travelmodeid).prev('.resort-row').find('.reslng').val();
        }
        if ($("#" + travelmodeid).next('.place').length) {
            next_lat = $("#" + travelmodeid).next('.place').find('.hidlat').val();
            next_lng = $("#" + travelmodeid).next('.place').find('.hidlng').val();
        }
        else if ($("#" + travelmodeid).next('.resort-row').length) {
            next_lat = $("#" + travelmodeid).next('.resort-row').find('.reslat').val();
            next_lng = $("#" + travelmodeid).next('.resort-row').find('.reslng').val();
        }
        var dista = Haversine(pre_lat, pre_lng, next_lat, next_lng);
        var kmtomin1 = dista / 40;
        var kmtomin2 = kmtomin1 * 60;
        var kmtomin3 = kmtomin2.toFixed();
        var final_time = "";
        if (kmtomin3 > 60) {
            var kmtohrs = Math.floor(kmtomin3 / 60);
            var kmtomints = kmtomin3 % 60;
            if (kmtohrs > 1 && kmtohrs <= 2) {
                if (kmtohrs == 1) {
                    kmtomints = Math.ceil(kmtomints / 5) * 5;
                    final_time = kmtohrs + " hour " + kmtomints + " mins";
                }
                else if (kmtohrs == 2) {
                    kmtomints = Math.ceil(kmtomints / 5) * 5;
                    final_time = kmtohrs + " hours " + kmtomints + " mins";
                }
            }
            else if (kmtohrs > 2) {
                final_time = "Message..";
            }
            else {
                kmtomints = Math.ceil(kmtomints / 5) * 5;
                final_time = kmtohrs + " hours " + kmtomints + " mins";
            }
        }
        else {
            if (kmtomin3 <= 1) {
                kmtomin3 = kmtomin3 + 4;
            }
            kmtomin3 = Math.ceil(kmtomin3 / 5) * 5;
            final_time = parseInt(kmtomin3) + " mins";
        }
        if (final_time == "Message..") {
            $('#' + travelmodeid + ' ul li a').removeClass('active');
            $("#" + travelmodeid + " ul li a[data-value='" + previous_travelmode + "']").addClass('active');
            var strdur = previous_travelduration;
            $('#' + travelmodeid).find('p').html(strdur);
            $('#compl').find('#' + travelmodeid).css("height", previous_height);

            var notify_alert = noty({
                layout: 'topRight',
                text: 'You cannot choose this. This travel mode takes too much of your time..',
                type: 'information',
                timeout: 2000,
                maxVisible: 1,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                }
            });
        }
        else {
            $("#" + travelmodeid).find("p").html('Time taken by ' + travemode + ' is ' + final_time);
            $("#" + travelmodeid).find("p").append(' <img src="../Content/images/edit.png" alt="" class="editimg" style="height: 15px;width: 15px;">');

            $('#compl').find('#' + travelmodeid).css("height", "");
            var hours = "hours";
            var hour = "hour";
            var mins = "mins";
            if (final_time.indexOf(hours) != -1 || final_time.indexOf(hour) != -1) {
                var array = final_time.split(' ');
                var amount = array[0] / .50;
                if (array[0] < 50) {
                    amount = array[0] / .50;
                }
                else {
                    amount = 50 / .50;
                }
                var timespent = amount * 58;
                if (final_time.indexOf(mins) != -1) {
                        timespent = array[2] / .50 + timespent;
                        $('#compl').find('#' + travelmodeid).css("height", timespent);
                }
                else {
                    $('#compl').find('#' + travelmodeid).css("height", timespent);
                }
            }
            else if (final_time.indexOf(mins) != -1) {
                var array = final_time.split(' ');
                if (array[0] > 20) {
                    var amount = array[0] / 30;
                    var timespent = amount * 58;
                    $('#compl').find('#' + travelmodeid).css("height", timespent);
                }
            }
        }
    }
}