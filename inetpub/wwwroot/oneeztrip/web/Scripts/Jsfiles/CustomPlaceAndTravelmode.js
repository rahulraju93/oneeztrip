﻿var chkTimLin = "",elemIndx=0;
$('.hotel-details').on('click', '.travedelimg1', function () {
    var CustomModeId = $(this).parents('.custom_travel').attr('id');
    var CustomModeId_Split = CustomModeId.split('_');
    noty({
        layout: 'topRight',
        type: 'confirm',
        maxVisible: 1,
        modal: true,
        text: 'Are you sure you want to delete the selected Travelmode?',
        buttons: [
            {
                addClass: 'btn btn-primary', text: 'Ok', onClick: function ($noty) {
                    $.getJSON('../Trip/DeleteCustomTravelMode', { travelmodeId: CustomModeId_Split[1] }, function (response) {
                        var notify_alert = noty({
                            layout: 'topRight',
                            text: 'Travel mode deleted successfully!!',
                            type: 'success',
                            timeout: 3000,
                            maxVisible: 1,
                            animation: {
                                open: { height: 'toggle' },
                                close: { height: 'toggle' },
                                easing: 'swing',
                                speed: 500
                            }
                        });
                        $('.hotel-details').find("#" + CustomModeId).remove();
                    });
                    $noty.close();
                }
            },
            {
                addClass: 'btn btn-danger', text: 'Cancel', onClick: function ($noty) {
                    $noty.close();
                }
            }
        ]
    });
});

$('#content-y').on('click', '.travedelimg1', function () {
    keepTrackForsave++;
    var cusTMid = $(this).parent().parent().attr('id');
    var parId = $(this).parent().parent().parents('.place-box-right').attr('id');
    var indx=$('#'+parId).children().index(this);
    noty({
        layout: 'topRight',
        type: 'confirm',
        maxVisible: 1,
        modal: true,
        text: 'Are you sure you want to delete the selected TravelMode?',
        buttons: [
            {
                addClass: 'btn btn-primary', text: 'Ok', onClick: function ($noty) {
                    // $(this).parent().parent().fadeOut('slow', function () { $(this).remove() });
                    $('#' + parId).find('#' + cusTMid).eq(indx).fadeOut('slow', function () { $(this).remove() });
                    $noty.close();
                }
            },
            {
                addClass: 'btn btn-danger', text: 'Cancel', onClick: function ($noty) {
                    $noty.close();
                }
            }
        ]
    });
});

$("#txtTime_customtm").keypress(function (eve) {

    if (eve.which != 8 && eve.which != 13 && eve.which != 0 && (eve.which < 48 || eve.which > 57)) {
        var notify_alert = noty({
            layout: 'topRight',
            text: 'Digits only. Enter time in mins',
            type: 'warning',
            timeout: 2000,
            maxVisible: 1,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            }
        });
        return false;
    }
    if (eve.which == 13) {

        $('#btnaddtravel_submit').focus().click();
        return true;
    }
});

$("#add_customplc").click(function () {
    $("#divcustom_place").slideToggle("slow");
});

$("#addcustomplc_btnclose").click(function () {
    $('#add_customplc').trigger('click');
});

$("#add_travelmode").click(function () {
    $("#divcustom_mode").slideToggle("slow");
});

$("#addtravelmode_btnclose").click(function () {
    $('#add_travelmode').trigger('click');
});

$("#btncustomplcclear").on('click',function () {
    $('#txtplace_customplc').val("");
    $('#txtaddress_customplc').val("");
    $('#txtdescription_customplc').val("");
});

$("#btnaddtravel_submit").on('click', function () {
    var customTravelTime = $('#txtTime_customtm').val();    
    var customTravelMode = $('#txttravelmode_customtm').val();    
    if (customTravelTime == "")
    {
        var notify_alert = noty({
            layout: 'topRight',
            text: 'Please enter the travel time',
            type: 'warning',
            timeout: 3000,
            maxVisible: 1,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            }
        });
        $('#txtTime_customtm').focus()
    }
    else if (parseInt(customTravelTime) > 180)
    {
        var notify_alert = noty({
            layout: 'topRight',
            text: 'The minutes that you entered should less than 180(3 hours)',
            type: 'warning',
            timeout: 3000,
            maxVisible: 1,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            }
        });
        $('#txtTime_customtm').val("");
        $('#txtTime_customtm').focus();
    }
    else {
        $('#add_travelmode').trigger('click');
        var dataObject = {
            traveltime: customTravelTime,
            travelmode: customTravelMode,
            TripId: $('#LoggedTripId').val()
        }
        $.ajax({
            url: "../Trip/CustomTravelMode",
            type: "POST",
            data: dataObject,
            datatype: "json",
            success: function (result) {
                var notify_alert = noty({
                    layout: 'topRight',
                    text: 'Travel mode added successfully',
                    type: 'success',
                    timeout: 3000,
                    maxVisible: 1,
                    animation: {
                        open: { height: 'toggle' },
                        close: { height: 'toggle' },
                        easing: 'swing',
                        speed: 500
                    }
                });
                $('#txtTime_customtm').val("");
                LoadCustomMode();
            },
            error: function (result) {
                var notify_alert = noty({
                    layout: 'topRight',
                    text: 'Oops!! Something went wrong, try adding it one more time..',
                    type: 'warning',
                    timeout: 3000,
                    maxVisible: 1,
                    animation: {
                        open: { height: 'toggle' },
                        close: { height: 'toggle' },
                        easing: 'swing',
                        speed: 500
                    }
                });
            }
        });
    }
});

$('#btncustomise_submit').on('click', function () {
    var customPlcName=$("#txtplace_customplc").val();
    var customPlcAddr=$('#txtaddress_customplc').val();
    var customPlcDesc=$('#txtdescription_customplc').val();
    if (customPlcName == "")
    {
        var notify_alert = noty({
            layout: 'topRight',
            text: 'Please enter custom place name',
            type: 'warning',
            timeout: 3000,
            maxVisible: 1,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            }
        });
    }
    else if (customPlcAddr == "") {
        var notify_alert = noty({
            layout: 'topRight',
            text: 'Please enter custom place address',
            type: 'warning',
            timeout: 3000,
            maxVisible: 1,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            }
        });
    }
    //else if (customPlcDesc == "") {
    //    var notify_alert = noty({
    //        layout: 'topRight',
    //        text: 'Please enter custom place description',
    //        type: 'warning',
    //        timeout: 3000,
    //        maxVisible: 1,
    //        animation: {
    //            open: { height: 'toggle' },
    //            close: { height: 'toggle' },
    //            easing: 'swing',
    //            speed: 500
    //        }
    //    });
    //}
    else {
        $('#add_customplc').trigger('click');
        var dataObject = {
            customplace: customPlcName,
            address: customPlcAddr,
            description: customPlcDesc,
            cityname: document.getElementById('selectedcity').innerHTML,
            TripId: $('#LoggedTripId').val()
        };
        $.ajax({
            url: "../Trip/CustomPlace",
            type: "POST",
            data: dataObject,
            dataType: "json",
            success: function (result) {
                var notify_alert = noty({
                    layout: 'topRight',
                    text: 'Custom place added successfully!',
                    type: 'success',
                    timeout: 3000,
                    maxVisible: 1,
                    animation: {
                        open: { height: 'toggle' },
                        close: { height: 'toggle' },
                        easing: 'swing',
                        speed: 500
                    }
                });
                $('#txtplace_customplc').val("");
                $('#txtaddress_customplc').val("");
                $('#txtdescription_customplc').val("");
                LoadCustomplace();
            },
            error: function (result) {
                var notify_alert = noty({
                    layout: 'topRight',
                    text: 'Oops!! Something went wrong, try adding it one more time..',
                    type: 'warning',
                    timeout: 3000,
                    maxVisible: 1,
                    animation: {
                        open: { height: 'toggle' },
                        close: { height: 'toggle' },
                        easing: 'swing',
                        speed: 500
                    }
                });
            }
        });
    }
});

function LoadCustomplace() {
    $.getJSON('../Trip/BindPlacevalues', { TripId: $('#LoggedTripId').val() }, function (data) {       
        var actdiv = "";
        $('#divplace').html("");
        CustomPlacesArray.length = 0;
        $.each(data, function (keys, values) {
            CustomPlacesArray.push(values);
            //alert(values.Description_);
            //
            if (values.Description_ == null) {                
                values.Description_ = "";
            }


            actdiv += "<aside class='resort-row' id='res_" + values.PlaceId + "'><div class='resort-row1' id='divres_" + values.PlaceId + "'>";
            actdiv += "<figure class='resort-img'><img src='../Content/images/AppImages/small-img5.jpg' alt='' />";
            //actdiv += "<span><strong id='s" + values.PlaceId + "'></strong></span>";
            actdiv += "</figure>";
            actdiv += "<div class='resort-details'><div class='r-left'>";
            actdiv += "<h3 id='h" + values.PlaceId + "'>" + values.Place_Name + "  </h3><br/>";
            actdiv += "<p id='pho_" + values.PlaceId + "' style='height:40px; overflow-y:hidden;'>" + values.Description_ + "</p>";

            //actdiv += "<p id='pho_" + values.PlaceId + "' style='height:40px; overflow-y:hidden;'>" + values.Place_Name + "</p>";

            actdiv += "<div class='book-add'><span><a href='#' id='inner" + values.PlaceId + "' class='add-trip'  onclick='return CustomPlacetoleft(" + values.PlaceId + ")'>Add Trip</a></span></div></div>";
            actdiv += "<div class='r-right'>";
            actdiv += "<ul><li><a href='#'><img src='../Content/images/del-icon.png' class='deleteplace' alt='' onclick='return DeleteCustomePlace(" + values.PlaceId + ")'/></a></li>";
            actdiv += "<ul><li><a href='#'><img src='../Content/images/del-icon.png' alt='' class='del-show' onclick=''/></a></li>";
            actdiv += "<li><a  class='fancybox' href='#info_" + values.PlaceId + "'><img src='../Content/images/info-icon.png' alt='' class='example1demo' /></a></li></ul>";
            actdiv += "</div></div></div>";
            actdiv += "</aside>";
            actdiv += "<div id='info_" + values.PlaceId + "' style='display: none;'>";
            actdiv += "<div class='info-left popup-bg' style='width: 94%;'><figure class='resort-img'><img src='../Content/images/AppImages/small-img5.jpg' alt='' /></figure>";
            actdiv += "<h3>" + values.Place_Name + "</h3>";
            actdiv += "<p><b>Place : </b>" + values.Place_Name + "</p>";
            actdiv += "<p><b>Address : </b>" + values.Address_ + "</p>";
            actdiv += "<p><b>Description : </b>" + values.Description_ + "</p>";
            actdiv += "</div>";
            actdiv += "</div>";
        });
        $('#divplace').append(actdiv);
    });
}

function CustomPlacetoleft(id) {
    keepTrackForsave++;
    var CustomPlaceId = '"' + id + '"';
    var Name = $('#h' + id).html();
    var Description = $('#pho_' + id).html();
    var count = $('#orderarray').val();

    var ToponeId = [];
    $('.city-row-details').each(function (data) {
            ToponeId.push({
                id: $(this).find('.place-box-right').attr('id')
            });
    });
    var PreviousValue = -1;
    var PlcaeDivId = -1
    var incr_num_plcbx = 0, PlcsTm_height = 0;
    for (var PlacesIndex = 0, PlacesLength = ToponeId.length; PlacesIndex < PlacesLength; PlacesIndex++) {
        incr_num_plcbx = 0;
        PlcsTm_height = 0;
        $("#plc_" + ToponeId[PlacesIndex].id).find(".place-box").each(function () {
            incr_num_plcbx++;
        });
        $("#plc_" + ToponeId[PlacesIndex].id).find(".place").each(function () {
            PlcsTm_height = PlcsTm_height + $(this).height();
        });
        $("#plc_" + ToponeId[PlacesIndex].id).find(".travel").each(function () {
            PlcsTm_height = PlcsTm_height + $(this).height();
        });
        $("#plc_" + ToponeId[PlacesIndex].id).find(".custom_travel").each(function () {
            PlcsTm_height = PlcsTm_height + $(this).height();
        });
        PlcsTm_height = PlcsTm_height + ((incr_num_plcbx - 1) * 50);

        if (PlcsTm_height < 1850) {
            var CurrentValue = PlcsTm_height;
            if (PreviousValue == -1) {
                PreviousValue = CurrentValue;
                PlcaeDivId = PlacesIndex;
            }
            else {
                if (PreviousValue > CurrentValue) {
                    PreviousValue = CurrentValue;
                    PlcaeDivId = PlacesIndex;
                }
            }
        }
    }
    if (PlcaeDivId != -1) {
        var image = $('#divres_' + id).find('.resort-img').find('img').attr("src");
        var test = $('#ccol_' + count + '0').find('.place-box-right').attr("id");
        var htmldata = "<div id='place" + id + "' class='place'><input type='hidden'/><input type='hidden' value='1' class='frmApi'/>";
        htmldata += "<div class='place-details' id=" + id + " style='height:100px;'>";
        htmldata += "<figure class='place-img'><img src='" + image + "' alt='' /></figure>";
        htmldata += "<div class='place-cont'><div class='p-left'><h4>" + Name + "</h4><p style='height:26px;overflow-y:hidden;'>" + Description + " .</p>";
        htmldata += "<span>Custom place.</span><br><span id='sp_" + id + "' class='sphrs'></div>";
        htmldata += "<div class='p-right'><ul>";
        htmldata += "<li><a href='#'><img src='../Content/images/del-icon.png' alt=''";
        //htmldata += " onclick='deletediv(" + CustomPlaceId + "," + CustomPlaceId + ",3)'";
        htmldata += " class='deleteplace'/></a></li>";
        htmldata += "<li><a href='#info_" + id + "' class='fancybox'><img src='../Content/images/info-icon.png' alt='' /></a></li>";
        htmldata += "</ul></div></div>";
        htmldata += "</div></div>";
        $('#plc_' + ToponeId[PlcaeDivId].id).append(htmldata);
        onresizingplc();
        var spId = (ToponeId[PlcaeDivId].id).split('_');
        var sp_Id = spId[1].split('');
        var parId = parseInt(sp_Id[1]) + 1;
        var getDayNum = $('#cit_' + sp_Id[0] + parId).find('.trip-date').find('.btn').html();
        var notify_alert = noty({
            layout: 'topRight',
            text: 'Your custom place is added at the bottom of the Day' + getDayNum + '. Move around to your desired day and time',
            type: 'success',
            timeout: 4000,
            maxVisible: 1,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 0
            }
        });
        return false;
    }
    else
    {
        var notify_alert = noty({
            layout: 'topRight',
            text: 'Exceeds day time limit,cannot be added..',
            type: 'warning',
            timeout: 3000,
            maxVisible: 1,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            }
        });
        return false;
    }
}

function DeleteCustomePlace(id) {
    noty({
        layout: 'topRight',
        type: 'confirm',
        maxVisible: 1,
        modal: true,
        text: 'Are you sure you want to delete this custom place?',
        buttons: [
            {
                addClass: 'btn btn-primary', text: 'Ok', onClick: function ($noty) {
                    $noty.close();
                    $.ajax({
                        type: "POST",
                        url: '../Trip/tbl_CustomPlace',
                        data: { 'Mode': id },
                        success: function () {
                            $('#res_' + id).remove();
                            $('#info_' + id).remove();
                        }
                    });

                }
            },
            {
                addClass: 'btn btn-danger', text: 'Cancel', onClick: function ($noty) {
                    $noty.close();
                    return false;
                }
            }
        ]
    });
}

function LoadCustomMode() {
    $.getJSON('../Trip/BindModevalues', { TripId: $('#LoggedTripId').val() }, function (data) {
        var values = '';
        var desc = "";
        var infowin = "";
        var modediv = "";
        $('#divmode').html("");
        $.each(data, function (keys, values) {
            var hours = Math.floor(values.Traveling_time / 60);
            var minutes = values.Traveling_time % 60;
            var hrsMins = "";
            if (hours >= 1) {
                if (hours == 1) {
                    if (minutes > 0) {
                        hrsMins = hours + " hour " + minutes + " mins";
                    }
                    else
                    {
                        hrsMins = hours + " hour";
                    }
                }
                else
                {
                    if (minutes > 0) {
                        hrsMins = hours + " hours " + minutes + " mins";
                    }
                    else
                    {
                        hrsMins = hours + " hour";
                    }
                }
            }
            else
            {
                hrsMins = minutes+" mins";
            }
            var selectedCustomTravel = values.TravelMode;
            if (selectedCustomTravel == "car") {
                modediv += '<div id="Rho_' + values.TM_Id + '" class="custom_travel"><a href="#" style="float:right;"><img src="../Content/images/del_travel.png" class="travedelimg1"/></a><p>Time taken by ' + values.TravelMode + ' is ' + hrsMins + '</p><ul><li><a href="#" data-value="car" class="active"  ><img src="../Content/images/car-icon.png" alt=""/></a></li><li><a href="#" data-value="train"><img src="../Content/images/train-icon.png" alt="" /></a></li><li><a href="#" data-value="plane"><img src="../Content/images/plane-icon.png" alt="" /></a></li><li><a href="#" data-value="walk"><img src="../Content/images/walk-icon.png" alt="" /></a></li><li class="last"><a href="#" data-value="bicycle"><img src="../Content/images/scooter-icon.png" alt="" /></a></li><input type="hidden" class="hiddriving" value=""/></ul></div>';
            }
            if (selectedCustomTravel == "train") {
                modediv += '<div id="Rho_' + values.TM_Id + '" class="custom_travel"><a href="#" style="float:right;"><img src="../Content/images/del_travel.png" class="travedelimg1"/></a><p>Time taken by ' + values.TravelMode + ' is ' + hrsMins + '</p><ul><li><a href="#" data-value="car"   ><img src="../Content/images/car-icon.png" alt=""/></a></li><li><a href="#" data-value="train" class="active"><img src="../Content/images/train-icon.png" alt="" /></a></li><li><a href="#" data-value="plane"><img src="../Content/images/plane-icon.png" alt="" /></a></li><li><a href="#" data-value="walk"><img src="../Content/images/walk-icon.png" alt="" /></a></li><li class="last"><a href="#" data-value="bicycle"><img src="../Content/images/scooter-icon.png" alt="" /></a></li><input type="hidden" class="hiddriving" value=""/></ul></div>';
            }
            if (selectedCustomTravel == "plane") {
                modediv += '<div id="Rho_' + values.TM_Id + '" class="custom_travel"><a href="#" style="float:right;"><img src="../Content/images/del_travel.png" class="travedelimg1"/></a><p>Time taken by ' + values.TravelMode + ' is ' + hrsMins + '</p><ul><li><a href="#" data-value="car"   ><img src="../Content/images/car-icon.png" alt=""/></a></li><li><a href="#" data-value="train" ><img src="../Content/images/train-icon.png" alt="" /></a></li><li><a href="#" data-value="plane" class="active"><img src="../Content/images/plane-icon.png" alt="" /></a></li><li><a href="#" data-value="walk"><img src="../Content/images/walk-icon.png" alt="" /></a></li><li class="last"><a href="#" data-value="bicycle"><img src="../Content/images/scooter-icon.png" alt="" /></a></li><input type="hidden" class="hiddriving" value=""/></ul></div>';
            }
            if (selectedCustomTravel == "walk") {
                modediv += '<div id="Rho_' + values.TM_Id + '" class="custom_travel"><a href="#" style="float:right;"><img src="../Content/images/del_travel.png" class="travedelimg1"/></a><p>Time taken by ' + values.TravelMode + ' is ' + hrsMins + '</p><ul><li><a href="#" data-value="car"   ><img src="../Content/images/car-icon.png" alt=""/></a></li><li><a href="#" data-value="train" ><img src="../Content/images/train-icon.png" alt="" /></a></li><li><a href="#" data-value="plane" ><img src="../Content/images/plane-icon.png" alt="" /></a></li><li><a href="#" data-value="walk" class="active"><img src="../Content/images/walk-icon.png" alt="" /></a></li><li class="last"><a href="#" data-value="bicycle"><img src="../Content/images/scooter-icon.png" alt="" /></a></li><input type="hidden" class="hiddriving" value=""/></ul></div>';
            }
            if (selectedCustomTravel == "scooter") {
                modediv += '<div id="Rho_' + values.TM_Id + '" class="custom_travel"><a href="#" style="float:right;"><img src="../Content/images/del_travel.png" class="travedelimg1"/></a><p>Time taken by ' + values.TravelMode + ' is ' + hrsMins + '</p><ul><li><a href="#" data-value="car"   ><img src="../Content/images/car-icon.png" alt=""/></a></li><li><a href="#" data-value="train" ><img src="../Content/images/train-icon.png" alt="" /></a></li><li><a href="#" data-value="plane" ><img src="../Content/images/plane-icon.png" alt="" /></a></li><li><a href="#" data-value="walk"><img src="../Content/images/walk-icon.png" alt="" /></a></li><li class="last"><a href="#" data-value="bicycle" class="active"><img src="../Content/images/scooter-icon.png" alt="" /></a></li><input type="hidden" class="hiddriving" value=""/></ul></div>';
            }
        });
        $('#divmode').append(modediv);
        var sortElementid = "", trackId = "", divCopy = "";
        $(".OtherContent-resort_right").sortable({
            connectWith: ".place-box",
            cursor: "move",
            handle1: ".custom_travel",
            helper: "clone",
            appendTo: "#mCSB_2_container",
            revert: "invalid",            
            placeholder: "portlet-placeholder ui-corner-all",
            start: function (event, ui) {
                keepTrackForsave++;
                sortElementid = $(ui.item).attr('id');
                divCopy = $('#' + sortElementid).html();
                $(ui.item).css('z-index', 10000); 
            },
            sort: function (event, ui) {
                var id = ui.item[0].id;
                var offsetPos = $('#mCSB_2_container').offset();
                $(ui.helper).css("left", event.pageX - offsetPos.left);
                $(ui.helper).css("top", event.pageY - offsetPos.top);
                var width1 = $("#mCSB_1").width();
                var ulWidth = $('#mCSB_1_container').width() - width1;
                var Mscb_1Height = $('#mCSB_1_container').height();
                var divLeft = $("#mCSB_1").offset().left;
                var left1 = event.pageX - divLeft;
                var percent1 = left1 / width1;
                var totalcal = percent1 * ulWidth;
                var draggableDivLeft = ui.position.left;
                var draggableDivRight = (ui.position.left);
                var MouseOnContainer = $("#mCSB_1").offset();
                var DivPositionLeft = MouseOnContainer.left - draggableDivLeft;
                IncreasePointLeft = IncreasePointLeft * 1.02;
                if ((percent1 * ulWidth) < ulWidth) {
                    if (IncreasePointLeft > 1.35) {
                        IncreasePointLeft = 1.35;
                    }
                    $("#mCSB_1_container").css('left', -(percent1 * ulWidth));
                }
                else if (-(percent1 * ulWidth) > ulWidth) {
                    $("#mCSB_1_container").css('left', -(ulWidth));
                }
                var cityRowHeight = $(".city-row").height();
                var height1 = $("#mCSB_2").height();
                var mCSBHeight = $('#mCSB_2_container').height();
                var ulheight = mCSBHeight - height1;
                var draggableDivTop = ui.position.top;
                var divTop = $("#mCSB_2").offset().top;
                var top1 = event.pageY - divTop;
                var percent2 = (top1 / ulheight);
                var TopPosition = (percent2 * ulheight);
                var divTopPos = -(percent2 * ulheight) - 100;
                var placeboxheight = ($(".place-box").height());
                divTopPos = divTopPos + 150;
                var HalfHeight = height1 / 4;
                var remainingheight = height1 - HalfHeight;
                IncreasePoint = IncreasePoint * 1.02;
                if ((-(divTopPos) <= HalfHeight) && (-(divTopPos) <= 0)) {
                    $("#mCSB_2_container").css("top", "0px");
                }
                else if ((-(divTopPos) >= height1 - 250)) {
                    $("#mCSB_2_container").css("top", -(ulheight));
                }
                else {
                    if (IncreasePoint > 5) {
                        IncreasePoint = 5;
                    }
                    $("#mCSB_2_container").css("top", ((divTopPos) * IncreasePoint));
                }

                var timeoutId;
                $(".travel>ul,.travel>ul").hover(function () {
                    if (!timeoutId) {
                        timeoutId = window.setTimeout(function () {
                            timeoutId = null;
                            $(".travel").removeClass("expandTmTop");
                            $(".travel").not(this).removeClass("expandTmBtm");
                            $(this).parents('.travel').addClass('expandTmBtm');
                            // $(this).parents('.travel').css('margin-bottom', '50px');
                        }, 1000);
                    }
                },
        function () {
            if (timeoutId) {
                window.clearTimeout(timeoutId);
                timeoutId = null;
            }
            else {
                $(".travel").removeClass("expandTmTop");
                $(".travel").not(this).removeClass("expandTmBtm");
                $(this).parents('.travel').addClass('expandTmBtm');
            }
        });

                $(".travel>p").hover(function () {
                    if (!timeoutId) {
                        timeoutId = window.setTimeout(function () {
                            timeoutId = null;
                            $(".travel").not(this).removeClass("expandTmTop");
                            $(".travel").removeClass("expandTmBtm");
                            $(this).parents('.travel').addClass('expandTmTop');
                        }, 500);
                    }
                },
           function () {
               if (timeoutId) {
                   window.clearTimeout(timeoutId);
                   timeoutId = null;
               }
               else {
                   $(".travel").not(this).removeClass("expandTmTop");
                   $(".travel").removeClass("expandTmBtm");
                   $(this).parents('.travel').addClass('expandTmTop');
               }
           });

                $(".custom_travel>ul,.custom_travel>ul>li").hover(function () {
                    if (!timeoutId) {
                        timeoutId = window.setTimeout(function () {
                            timeoutId = null;
                            $(".custom_travel").removeClass("expandTmTop");
                            $(".custom_travel").not(this).removeClass("expandTmBtm");
                            $(this).parents('.custom_travel').addClass('expandTmBtm');
                            // $(this).parents('.travel').css('margin-bottom', '50px');
                        }, 1000);
                    }
                },
            function () {
                if (timeoutId) {
                    window.clearTimeout(timeoutId);
                    timeoutId = null;
                }
                else {
                    $(".custom_travel").removeClass("expandTmTop");
                    $(".custom_travel").not(this).removeClass("expandTmBtm");
                    $(this).parents('.custom_travel').addClass('expandTmBtm');
                }
            });

                $(".custom_travel>p").hover(function () {
                    if (!timeoutId) {
                        timeoutId = window.setTimeout(function () {
                            timeoutId = null;
                            $(".custom_travel").not(this).removeClass("expandTmTop");
                            $(".custom_travel").removeClass("expandTmBtm");
                            $(this).parents('.custom_travel').addClass('expandTmTop');
                        }, 500);
                    }
                },
           function () {
               if (timeoutId) {
                   window.clearTimeout(timeoutId);
                   timeoutId = null;
               }
               else {
                   $(".custom_travel").not(this).removeClass("expandTmTop");
                   $(".custom_travel").removeClass("expandTmBtm");
                   $(this).parents('.custom_travel').addClass('expandTmTop');
               }
           });

            },
            stop: function (event, ui) {

                $(".travel,.custom_travel").removeClass("expandTmTop");
                $(".travel,.custom_travel").removeClass("expandTmBtm");
                $('.travel>ul>li,.travel>ul,.travel>p,.custom_travel>ul,.custom_travel>ul>li,.custom_travel>p').unbind('mouseenter mouseleave');

                if ($(ui.item).parents(".place-box-right").length) {
                    LoadCustomMode();
                    if ($(ui.item).prev('.travel').length) {
                        $(ui.item).css('margin-top', '10px');
                    }
                    if ($(ui.item).next('.travel').length) {
                        $(ui.item).css('margin-bottom', '10px');
                    }
                    if ($(ui.item).prev('.custom_travel').length) {
                        $(ui.item).css('margin-top', '10px');
                    }
                    if ($(ui.item).next('.custom_travel').length) {
                        $(ui.item).css('margin-bottom', '10px');
                    }

                    var noofday = 0;

                    $('.city-row:visible').each(function () {
                        noofday++;
                    });
                    
                    if (noofday == 1) {
                        $(ui.item).css("width", "124%").css("margin-left","13%");
                    }

                    chkTimLin = $(ui.item).parents('.place-box-right').attr('id');
                    var time = $(ui.item).find("p").html();
                    var hours = "hours";
                    var hour = "hour";
                    var mins = "mins";
                    var timespent = 0;
                    if (time.indexOf(hour) != -1) {
                        var array = time.split(' ');
                        var amount = array[5] / .50;
                        timespent = amount * 58;
                        if (time.indexOf(mins) != -1) {
                            timespent = array[7] / .50 + timespent;
                        }
                    }
                    else if (time.indexOf(hours) != -1) {
                        var array = time.split(' ');
                        var amount = array[5] / .50;
                        timespent = amount * 58;
                        if (time.indexOf(mins) != -1) {
                            timespent = array[7] / .50 + timespent;
                        }
                    }
                    else if (time.indexOf(mins) != -1) {
                        var array = time.split(' ');
                        if (array[5] > 20) {
                            var amount = array[5] / 30;
                            timespent = amount * 58;
                        }
                    }

                    $('#plc_' + chkTimLin + ' div').click(function (event) {
                        elemIndx = $(ui.item).index();
                    });

                    var incr_num_plcbx = 0, PlcsTm_height = 0;
                    $("#plc_" + chkTimLin).find(".place-box").each(function () {
                        incr_num_plcbx++;
                    });
                    $("#plc_" + chkTimLin).find(".place").each(function () {
                        PlcsTm_height = PlcsTm_height + $(this).height();
                    });
                    $("#plc_" + chkTimLin).find(".travel").each(function () {
                        PlcsTm_height = PlcsTm_height + $(this).height();
                    });
                    $("#plc_" + chkTimLin).find(".custom_travel").each(function () {
                        PlcsTm_height = PlcsTm_height + $(this).height();
                    });
                    PlcsTm_height = PlcsTm_height + ((incr_num_plcbx - 1) * 50) + timespent;
                    if ((PlcsTm_height - 43) > 2000) {
                        $(this).sortable('cancel');
                        var notify_alert = noty({
                            layout: 'topRight',
                            text: 'Exceeds day time limit,cannot be added..',
                            type: 'warning',
                            timeout: 3000,
                            maxVisible: 1,
                            animation: {
                                open: { height: 'toggle' },
                                close: { height: 'toggle' },
                                easing: 'swing',
                                speed: 500
                            }
                        });
                    }
                    else {
                        customTravelModeResizeOnDrop();
                    }
                }
                else {
                    $(this).sortable('cancel');
                }
            }
        });
    });
    $('#RightSidePart').unblock();
}

function customTravelModeResizeOnDrop()
{
    var customTravelsize = $('#compl').find('.custom_travel').length;
    CustomTravelDivResize(customTravelsize);    
}