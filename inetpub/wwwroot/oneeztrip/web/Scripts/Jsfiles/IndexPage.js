﻿var latitude = "", longitude = "", city = "";

$(window).load(function () { $('.flexslider').flexslider(); });

$("#itineries").owlCarousel({
    navigation: true,
    pagination: false,
    responsive: true,
	items :3, //10 items above 1000px browser width
		itemsDesktop : [1199,3],
		itemsDesktopSmall : [980,3],
		itemsTablet: [800,2],
		itemsTabletSmall: [600,2],
		itemsMobile : [479,1],
});

$(document).ready(function () {
    $(".accordian-popup").resp_Accordion();
        $.getJSON('../Trip/GetDestinationPlace', {}, function (data) {
            $.each(data, function (i, d) {
                var HtmlData = '';
                HtmlData += "<li style='text-overflow: ellipsis;white-space: nowrap;overflow: hidden;width: 85px;'><a href='#' class='ignore_click' title='" + d.cityname + "' onclick='NaviagteToNextPage(\"" + d.lat + "\",\"" + d.lng + "\",\"" + d.cityname + "\")'><img src='../Content/images/CityImages/" + d.Photo + "' alt='' class='ignore_click' style='width:83px;height:83px;border-radius:4px;' />" + d.cityname + " </a></li>";
                var Id = d.customplace;
                Id = Id.replace(/\s/g, '');
                $('#' + Id).append(HtmlData);
            });
        });
    var id = getUrlValue('id');
    if (id == 1) {
        var noty_alert = noty({
            layout: 'topRight',
            text: 'if existing user login otherwise new user register to view notifications',
            type: 'warning',
            timeout: 4000,
            maxVisible: 1,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            }
        });
    }
    $.getJSON('../Trip/GetAmazingGetDestination', {}, function (data) {
        var incrementDestination = 0;
        var HtmlData = "";
        var arrayDestinations = [];        
        $.each(data, function (keys, values) {
            incrementDestination++;            
            var appendDivId = "";            
            if (incrementDestination <= 5)
            {
                arrayDestinations.push({
                    cityname: values.cityname,
                    latitude: values.lat,
                    longitude: values.lng,
                    cityimg: values.Photo
                });
            }
            else
            {
                HtmlData += "<li onclick='NaviagteToNextPage(\"" + values.lat + "\",\"" + values.lng + "\",\"" + values.cityname + "\")' style='height:402px;'><img src='../Content/images/CityImages/" + values.Photo + "' class='bximg' style='width:100%;height:94%;border-radius:2px;' /><span onclick='NaviagteToNextPage(\"" + values.lat + "\",\"" + values.lng + "\",\"" + values.cityname + "\")'>" + values.cityname + "</span></li>";
                appendDivId = "bxSliderDestinations";                
            }
        });
        
        $("#bxSliderDestinations").html(HtmlData);
        $('.bxslider').bxSlider({
            buildPager: function (slideIndex) {
                switch (slideIndex) {
                    case 0:
                        return '<img src="../Content/images/CityImages/' + arrayDestinations[0].cityimg + '" class="bxslidrimg" style="border-radius:2px;width:100%;height:100%;" onclick="NaviagteToNextPage(\'' + arrayDestinations[0].latitude + '\',\'' + arrayDestinations[0].longitude + '\',\'' + arrayDestinations[0].cityname + '\')"> <span class="a" onclick="NaviagteToNextPage(\'' + arrayDestinations[0].latitude + '\',\'' + arrayDestinations[0].longitude + '\',\'' + arrayDestinations[0].cityname + '\')">' + arrayDestinations[0].cityname + '</span>';
                    case 1:
                        return '<img src="../Content/images/CityImages/' + arrayDestinations[1].cityimg + '" class="bxslidrimg" style="border-radius:2px;width:100%;height:100%;" onclick="NaviagteToNextPage(\'' + arrayDestinations[1].latitude + '\',\'' + arrayDestinations[1].longitude + '\',\'' + arrayDestinations[1].cityname + '\')"> <span class="b" onclick="NaviagteToNextPage(\'' + arrayDestinations[1].latitude + '\',\'' + arrayDestinations[1].longitude + '\',\'' + arrayDestinations[1].cityname + '\')">' + arrayDestinations[1].cityname + '</span>';
                    case 2:
                        return '<img src="../Content/images/CityImages/' + arrayDestinations[2].cityimg + '" class="bxslidrimg" style="border-radius:2px;width:100%;height:100%;" onclick="NaviagteToNextPage(\'' + arrayDestinations[2].latitude + '\',\'' + arrayDestinations[2].longitude + '\',\'' + arrayDestinations[2].cityname + '\')"> <span class="c" onclick="NaviagteToNextPage(\'' + arrayDestinations[2].latitude + '\',\'' + arrayDestinations[2].longitude + '\',\'' + arrayDestinations[2].cityname + '\')">' + arrayDestinations[2].cityname + '</span>';
                    case 3:
                        return '<img src="../Content/images/CityImages/' + arrayDestinations[3].cityimg + '" class="bxslidrimg" style="border-radius:2px;width:100%;height:100%;" onclick="NaviagteToNextPage(\'' + arrayDestinations[3].latitude + '\',\'' + arrayDestinations[3].longitude + '\',\'' + arrayDestinations[3].cityname + '\')"> <span class="d" onclick="NaviagteToNextPage(\'' + arrayDestinations[3].latitude + '\',\'' + arrayDestinations[3].longitude + '\',\'' + arrayDestinations[3].cityname + '\')">' + arrayDestinations[3].cityname + '</span>';
                    case 4:
                        return '<img src="../Content/images/CityImages/' + arrayDestinations[4].cityimg + '" class="bxslidrimg" style="border-radius:2px;width:100%;height:100%;" onclick="NaviagteToNextPage(\'' + arrayDestinations[4].latitude + '\',\'' + arrayDestinations[4].longitude + '\',\'' + arrayDestinations[4].cityname + '\')"> <span class="d" onclick="NaviagteToNextPage(\'' + arrayDestinations[4].latitude + '\',\'' + arrayDestinations[4].longitude + '\',\'' + arrayDestinations[4].cityname + '\')">' + arrayDestinations[4].cityname + '</span>';
                     default: return " ";
                }
            }
        });
    });
    $.getJSON('../Trip/GetPopularItineraries', {}, function (data) {
        var appendData = "";
        var listData = "";
        $(".owl-wrapper").html("");
        $.each(data, function (keys, values) {
            if (keys < 6) {
                appendData += "<div class='owl-item' style='width: 245px;'><div class='item' onclick='NavigateToTrip(\"" + values.lat + "\",\"" + values.lng + "\",\"" + values.cityname + "\",\"" + values.ItineraryId + "\")'><img src='../Content/images/CityImages/" + values.Photo + "' height='180px' alt=''><strong>" + values.ItineraryName + "</strong> <span>" + values.ItineraryDesc + "</span></div></div>";
            }
            else
            {
                listData += "<li style='text-overflow: ellipsis;white-space: nowrap;overflow: hidden;'><a href='#' class='ignore_click' title='" + values.ItineraryDesc + "' onclick='NavigateToTrip(\"" + values.lat + "\",\"" + values.lng + "\",\"" + values.cityname + "\",\"" + values.ItineraryId + "\")'><img src='../Content/images/CityImages/" + values.Photo + "' alt='' class='ignore_click' />" + values.ItineraryName + " <span>" + values.ItineraryDesc + "<span></a></li>";
            }
        });
        $(".owl-wrapper").html(appendData);
        $("#TopItin").html(listData);
    });
    localStorage.removeItem('IsSavedinDB5');

    $element1 = $("#slide")[0];
    $element2 = $("#view")[0];
    $element3 = $("#view1")[0];
    elements_to_be_ignored = ["ignore_click", "home-login", "label2", "label", "clsh2", "clear", "clsPagespan", "clsPageCheckbox", "clsEmplyDiv", "slide-close slide1", "acc_head", "acc_content", "acc_dest", "acc_dest", "acc_dest p", "acc-dest ul", "acc-dest ul li", "acc-dest ul li a", "acc-dest ul li img", "acc-dest ul li a img", "acc_icon_expand", "slide1", "acc_head last", "last ignore_click"];
    elementsId_to_be_ignored = ["loginpopup", "registerpopup", "newuserpopup", "FBregisterpopup", "slide", "lblpwd", "lblCheckRem"];
    $('body').click(function (e) {
       
        body_class = $(e.target).attr("class");
        body_Id = $(e.target).attr("id");
     
        if (elements_to_be_ignored.indexOf(body_class) == -1 && elementsId_to_be_ignored.indexOf(body_Id) == -1) {
            $('.slide-close').slideUp("slow");
        }
        e.stopPropagation();
    });
    GroupIdConfirmation();
    if ($.trim($('[id$=lblusername]').text()) == "") {        
        $('#LoggedUserLi').hide();
        $('#LogdUsrMyTrip').hide();
        $("#liMyAccount").show();
        $("#wel-mem").hide();
        return false;
    }
    else {
        $('#LoggedUserLi').show();
        $('#LogdUsrMyTrip').show();
        notificationCount();
        $("#liMyAccount").hide();
        $("#wel-mem").show();
        return false;
    }

    $('#FacebookLogout_Button').trigger("click");

});

$(document).ready("popup.html #register", function () {
    var notify_alert = noty({
        layout: 'topRight',
        text: 'Load was performed.',
        type: 'success',
        timeout: 3000,
        maxVisible: 1,
        animation: {
            open: { height: 'toggle' },
            close: { height: 'toggle' },
            easing: 'swing',
            speed: 500
        }
    });
});

$('.zoomMeImage').hover(function () {
    $(this).addClass('transition');

}, function () {
    $(this).removeClass('transition');
});

function GroupIdConfirmation() {
    var id = getUrlValue('id');
    var GroupMail = getUrlValue('Group');
    if (id != null && GroupMail != null && id != "" && GroupMail != "") {
        $.ajax({
            url: '../Trip/DecryptMailValues',
            type: "POST",
            data: { cipherText: GroupMail },
            dataType: "json",
            async: false,
            success: function (result) {
                GroupMail = result;
            }, error: function (err) {
                alert(err.responseText);
            }
        });
        $.ajax({
            url: '../Trip/DecryptMailValues',
            type: "POST",
            data: { cipherText: id },
            dataType: "json",
            async: false,
            success: function (result) {
                id = result;
            }, error: function () {
            }
        });
    }
    if (id == null || id == undefined || id == "") {
    }
    else {
        if ($.trim($('[id$=LoggedUserEmail]').text()) != "" && GroupMail == $.trim($('[id$=LoggedUserEmail]').text())) {
            $.ajax({
                url: '../Trip/AssignSessionValues',
                type: "POST",
                data: { GroupId: id, GroupMail: GroupMail },
                dataType: "json",
                success: function (result) {
                    window.location.href = "../Trip/Notification";
                }, error: function () {
                }
            });
        }
    }
}

function getUrlValue(name) {
    var value = "";
    $.ajax({
        url: '../Trip/QueryStringValues',
        type: "POST",
        data: { value: name },
        dataType: "json",
        async: false,
        success: function (result) {
            value = result;
        }, error: function (err) {
        }
    });
    return value;
}

$('#txtloginpwd').keypress(function (e) {
    var key = e.which;
    if (key == 13) 
    {
        $('input[id = btnlogin]').click();
        return false;
    }
});

$('#txtloginpwd').bind("cut copy paste", function (e) {
    e.preventDefault();
});

$('#txtregpwd').bind("cut copy paste", function (e) {
    e.preventDefault();
});

$('#txtregconpwd').bind("cut copy paste", function (e) {
    e.preventDefault();
});

$('#txtregconpwd').keypress(function (e) {
    var key = e.which;
    if (key == 13) {
        $('input[id = btnregsubmit]').click();
        return false;
    }
});

$("#btnregsubmit").click(function () {
    
    if (($.trim($('[id$=regmail]').val() == "")) && ($.trim($('[id$=txtregpwd]').val() == "")) && ($.trim($('[id$=txtreglastname]').val() == "")) && ($.trim($('[id$=txtregfirstname]').val() == ""))) {
        if ($.trim($('[id$=txtregfirstname]').val()) == "") {
            var noty_alert = noty({
                layout: 'topRight',
                text: 'Please enter first name!',
                type: 'warning',
                timeout: 3000,
                maxVisible: 1,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                }
            });
            $('[id$=txtregfirstname]').focus();
            $('[id$=txtregfirstname]').val("");
            return false;
        }
        if ($.trim($('[id$=txtreglastname]').val()) == "") {
            var noty_alert = noty({
                layout: 'topRight',
                text: 'Please enter last name!',
                type: 'warning',
                timeout: 3000,
                maxVisible: 1,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                }
            });
            $('[id$=txtreglastname]').focus();
            $('[id$=txtreglastname]').val("");
            return false;
        }
        if ($.trim($('[id$=txtregmail]').val()) == "") {
            var noty_alert = noty({
                layout: 'topRight',
                text: 'Please enter Email-Id!',
                type: 'warning',
                timeout: 3000,
                maxVisible: 1,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                }
            });
            $('[id$=txtregmail]').focus();
            $('[id$=txtregmail]').val("");
            return false;
        }
        if ($.trim($('[id$=txtregpwd]').val()) == "") {
            var noty_alert = noty({
                layout: 'topRight',
                text: 'Please enter password',
                type: 'warning',
                timeout: 3000,
                maxVisible: 1,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                }
            });
            $('[id$=txtregpwd]').focus();
            $('[id$=txtregpwd]').val("");
            return false;
        }
        if ($.trim($('[id$=txtregconpwd]').val()) == "") {
            var noty_alert = noty({
                layout: 'topRight',
                text: 'Please enter confirm password',
                type: 'warning',
                timeout: 3000,
                maxVisible: 1,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                }
            });
            $('[id$=txtregconpwd]').focus();
            $('[id$=txtregconpwd]').val("");
            return false;
        }
        if ($.trim($('[id$=txtregpwd]').val()) != $.trim($('[id$=txtregconpwd]').val())) {
            var noty_alert = noty({
                layout: 'topRight',
                text: 'Password doesn\'t match..',
                type: 'warning',
                timeout: 3000,
                maxVisible: 1,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                }
            });
            $('[id$=txtregpwd]').focus();
            $('[id$=txtregconpwd]').val("");
            return false;
        }
    }
    var dataObject = {
        mail: $("#txtregmail").val().toLowerCase(),
        password: $("#txtregpwd").val(),
        firstname: $("#txtregfirstname").val(),
        lastname: $("#txtreglastname").val()
    };
    $('#WholePart').block({
        message: 'Checking Email-Id and Password..',
        css: {
            border: 'none',
            padding: '15px',
            float: 'right',
            'margin-right': '0%',
            backgroundColor: '#033363',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        }
    });
    $.ajax({
        url: '../Trip/Save',
        type: "POST",
        data: dataObject,
        dataType: "json",
        success: function (result) {

            //alert("IntimateConfirmEmail");


            $('#WholePart').unblock();
            var url = '../Trip/IntimateConfirmEmail';
            window.location.href = url;
        },
        error: function () {
            $('#WholePart').unblock();
            var noty_alert = noty({
                layout: 'topRight',
                text: 'Not registered, please try once again..',
                type: 'warning',
                timeout: 3000,
                maxVisible: 1,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                }
            });
        }
    });
    $("#txtregmail").val('');
    $("#txtregpwd").val('');
    $("#txtregfirstname").val('');
    $("#txtreglastname").val('');
    $('[id$=txtregconpwd]').val("");
    return false;
});

$("#btnfbregsubmit").click(function () {
    if (($.trim($('[id$=txtfbregmail]').val() == "")) && ($.trim($('[id$=txtfbregpwd]').val() == "")) && ($.trim($('[id$=txtfbregfirstname]').val() == "")) && ($.trim($('[id$=txtfbreglastname]').val() == ""))) {
        if ($.trim($('[id$=txtfbregfirstname]').val()) == "") {
            var noty_alert = noty({
                layout: 'topRight',
                text: 'Please enter first name!',
                type: 'warning',
                timeout: 3000,
                maxVisible: 1,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                }
            });
            $('[id$=txtfbregfirstname]').focus();
            $('[id$=txtfbregfirstname]').val("");
            return false;
        }
        if ($.trim($('[id$=txtfbreglastname]').val()) == "") {
            var noty_alert = noty({
                layout: 'topRight',
                text: 'Please enter last name!',
                type: 'warning',
                timeout: 3000,
                maxVisible: 1,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                }
            });
            $('[id$=txtfbreglastname]').focus();
            $('[id$=txtfbreglastname]').val("");
            return false;
        }
        if ($.trim($('[id$=txtfbregmail]').val()) == "") {
            var noty_alert = noty({
                layout: 'topRight',
                text: 'Please enter Email-Id!',
                type: 'warning',
                timeout: 3000,
                maxVisible: 1,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                }
            });
            $('[id$=txtfbregmail]').focus();
            $('[id$=txtfbregmail]').val("");
            return false;
        }
        if ($.trim($('[id$=txtfbregpwd]').val()) == "") {
            var noty_alert = noty({
                layout: 'topRight',
                text: 'Please enter password',
                type: 'warning',
                timeout: 3000,
                maxVisible: 1,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                }
            });
            $('[id$=txtfbregpwd]').focus();
            $('[id$=txtfbregpwd]').val("");
            return false;
        }

        if ($.trim($('[id$=txtfbregconpwd]').val()) == "") {
            var noty_alert = noty({
                layout: 'topRight',
                text: 'Please enter confirm password',
                type: 'warning',
                timeout: 3000,
                maxVisible: 1,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                }
            });
            $('[id$=txtfbregconpwd]').focus();
            $('[id$=txtfbregconpwd]').val("");
            return false;
        }
        if ($.trim($('[id$=txtfbregpwd]').val()) != $.trim($('[id$=txtfbregconpwd]').val())) {
            var noty_alert = noty({
                layout: 'topRight',
                text: 'Password doesn\'t match',
                type: 'warning',
                timeout: 3000,
                maxVisible: 1,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                }
            });
            $('[id$=txtfbregpwd]').focus();
            $('[id$=txtfbregconpwd]').val("");
            return false;
        }
    }
    var dataObject = {
        mail: $("#txtfbregmail").val().toLowerCase(),
        password: $("#txtfbregpwd").val(),
        firstname: $("#txtfbregfirstname").val(),
        lastname: $("#txtfbreglastname").val(),
        FacebookId: $('#FacebookId').val()
    };
    $('#WholePart').block({
        message: 'Checking Email-Id and Password..',
        css: {
            border: 'none',
            padding: '15px',
            float: 'right',
            'margin-right': '0%',
            backgroundColor: '#033363',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        }
    });
    $.ajax({
        url: '../Trip/Save',
        type: "POST",
        data: dataObject,
        dataType: "json",
        success: function (result) {
            $('#WholePart').unblock();
            var url = '../Trip/IntimateConfirmEmail';
            window.location.href = url;
        },
        error: function () {
            $('#WholePart').unblock();
            var noty_alert = noty({
                layout: 'topRight',
                text: 'Not registered, please try once again..',
                type: 'warning',
                timeout: 3000,
                maxVisible: 1,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                }
            });
        }
    });
    $("#txtfbregmail").val('');
    $("#txtfbregpwd").val('');
    $("#txtfbregfirstname").val('');
    $("#txtfbreglastname").val('');
    $('[id$=txtfbregconpwd]').val("");
    return false;
});

$("#btnregclear").on('click', function () {
    $("#txtregfirstname").val('');
    $("#txtreglastname").val('');
    $("#txtregmail").val('');
    $("#txtregpwd").val('');
    $("#txtregconpwd").val('');
    return false;
});

$("#btnfbregclear").on('click', function () {
    $("#txtfbregmail").val('');
    $("#txtfbregpwd").val('');
    $("#txtfbregfirstname").val('');
    $("#txtfbreglastname").val('');
    $('[id$=txtfbregconpwd]').val("");
    return false;
});

$("#txtloginmail").change(function () {
    var EmailTxt = $('#txtloginmail').val().toLowerCase();
    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    if (filter.test(EmailTxt)) {
        $.getJSON('../Trip/CheckEmailStatus', { Email: EmailTxt }, function (data) {
            if (data == "2") {
                $('#txtloginmail').val("");
                $('#txtloginmail').focus();
                var noty_alert = noty({
                    layout: 'topRight',
                    text: 'Your account is deactivated. Please contact 1eztrip support team..',
                    type: 'warning',
                    timeout: 3000,
                    maxVisible: 1,
                    animation: {
                        open: { height: 'toggle' },
                        close: { height: 'toggle' },
                        easing: 'swing',
                        speed: 500
                    }
                });
            }
            //else if (data == "0") {
            //    $('#txtloginmail').val('');
            //    $('#txtloginmail').focus();
            //    var noty_alert = noty({
            //        layout: 'topRight',
            //        text: 'Email-Id doesn\'t exist!',
            //        type: 'warning',
            //        timeout: 3000,
            //        maxVisible: 1,
            //        animation: {
            //            open: { height: 'toggle' },
            //            close: { height: 'toggle' },
            //            easing: 'swing',
            //            speed: 500
            //        }
            //    });
            //}
            else {
                $.getJSON('../Trip/GetRememberedPassword', { Email: EmailTxt }, function (data) {
                    if (data.Success != "false") {
                        $("#txtloginpwd").val(data.Success);
                    }
                });
                return false;
            }
        });
    }
    else {
        var noty_alert = noty({
            layout: 'topRight',
            text: 'In-Valid Email address..',
            type: 'warning',
            timeout: 3000,
            maxVisible: 1,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            }
        });
        $('#txtregmail').focus();
        $('#txtregmail').val("");
        return false;
    }
});

$("#txtreglastname").change(function () {
    var LastName = $("#txtreglastname").val().length;
    if (LastName <= 1) {
        var noty_alert = noty({
            layout: 'topRight',
            text: 'Last name should contain more than one character!',
            type: 'warning',
            timeout: 3000,
            maxVisible: 1,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            }
        });
        $('[id$=txtreglastname]').focus();
        $('[id$=txtreglastname]').val("");
        return false;
    }
});

$("#txtregfirstname").change(function () {
    var LastName = $("#txtregfirstname").val().length;
    if (LastName <= 1) {
        var noty_alert = noty({
            layout: 'topRight',
            text: 'First name should contain more than one character!',
            type: 'warning',
            timeout: 3000,
            maxVisible: 1,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            }
        });
        $('[id$=txtregfirstname]').focus();
        $('[id$=txtregfirstname]').val("");
        return false;
    }
});

$("#txtregpwd").change(function () {
    var regPassword = $("#txtregpwd").val();
    if (!MatchPassword(regPassword)) {
        var noty_alert = noty({
            layout: 'topRight',
            text: 'Password should contain alphabets and numbers and should have minimum 7 characters!',
            type: 'warning',
            timeout: 3000,
            maxVisible: 1,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            }
        });
        $('[id$=txtregpwd]').focus();
        $('[id$=txtregpwd]').val("");
        return false;
    }
});

$("#txtforgotmail").change(function () {
    var EmailTxt = $('#txtforgotmail').val().toLowerCase();
    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    if (filter.test(EmailTxt)) {
        $.getJSON('../Trip/CheckEmailStatus', { Email: EmailTxt }, function (data) {
            if (data == "1") {
                return false;
            }
            else {
                var noty_alert = noty({
                    layout: 'topRight',
                    text: 'Your Mail-Id doen\'t exist!',
                    type: 'warning',
                    timeout: 3000,
                    maxVisible: 1,
                    animation: {
                        open: { height: 'toggle' },
                        close: { height: 'toggle' },
                        easing: 'swing',
                        speed: 500
                    }
                });
                $('#txtforgotmail').focus();
                $('#txtforgotmail').val("");
            }
        });
    }
    else {
        var noty_alert = noty({
            layout: 'topRight',
            text: 'In-Valid Email address..',
            type: 'warning',
            timeout: 3000,
            maxVisible: 1,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            }
        });
        $('#txtforgotmail').focus();
        $('#txtforgotmail').val("");
        return false;
    }
});

$("#txtregmail").change(function () {
    var EmailTxt = $('#txtregmail').val().toLowerCase();
    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    if (filter.test(EmailTxt)) {
        $.getJSON('../Trip/CheckEmailStatus', { Email: EmailTxt }, function (data) {
            if (data == "1") {
                var noty_alert = noty({
                    layout: 'topRight',
                    text: 'Email-Id already exist',
                    type: 'warning',
                    timeout: 3000,
                    maxVisible: 1,
                    animation: {
                        open: { height: 'toggle' },
                        close: { height: 'toggle' },
                        easing: 'swing',
                        speed: 500
                    }
                });
                $('#txtregmail').focus();
                $('#txtregmail').val("");
                return false;
            }
            else if (data == "2") {
                var noty_alert = noty({
                    layout: 'topRight',
                    text: 'Your account is deactivated. Please contact 1eztrip support team..',
                    type: 'warning',
                    timeout: 3000,
                    maxVisible: 1,
                    animation: {
                        open: { height: 'toggle' },
                        close: { height: 'toggle' },
                        easing: 'swing',
                        speed: 500
                    }
                });
                $('#txtregmail').focus();
                $('#txtregmail').val("");
                return false;
            }
        });
    }
    else {
        var noty_alert = noty({
            layout: 'topRight',
            text: 'In-Valid Email address..',
            type: 'warning',
            timeout: 3000,
            maxVisible: 1,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            }
        });
        $('#txtregmail').focus();
        $('#txtregmail').val("");
        return false;
    }
});

$("#txtfbregmail").change(function () {
    var EmailTxt = $('#txtfbregmail').val().toLowerCase();
    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    if (filter.test(EmailTxt)) {
        $.getJSON('../Trip/CheckEmailStatus', { Email: EmailTxt }, function (data) {
            if (data == "1") {
                var noty_alert = noty({
                    layout: 'topRight',
                    text: 'Email-Id already exist',
                    type: 'warning',
                    timeout: 3000,
                    maxVisible: 1,
                    animation: {
                        open: { height: 'toggle' },
                        close: { height: 'toggle' },
                        easing: 'swing',
                        speed: 500
                    }
                });
                $('#txtfbregmail').focus();
                $('#txtfbregmail').val("");
                return false;
            }
            else if (data == "2") {
                var noty_alert = noty({
                    layout: 'topRight',
                    text: 'Your account is deactivated. Please contact 1eztrip support team..',
                    type: 'warning',
                    timeout: 3000,
                    maxVisible: 1,
                    animation: {
                        open: { height: 'toggle' },
                        close: { height: 'toggle' },
                        easing: 'swing',
                        speed: 500
                    }
                });
                $('#txtfbregmail').focus();
                $('#txtfbregmail').val("");
                return false;
            }
        });
    }
    else {
        var noty_alert = noty({
            layout: 'topRight',
            text: 'In-Valid Email address..',
            type: 'warning',
            timeout: 3000,
            maxVisible: 1,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            }
        });
        $('#txtfbregmail').focus();
        $('#txtfbregmail').val("");
        return false;
    }
});

$('#txtregfirstname').keydown(function (e) {
    var pressedkey = e.keyCode;
    if (!((pressedkey == 8) || (pressedkey == 32) || (pressedkey == 46) || (pressedkey >= 35 && pressedkey <= 40) || (pressedkey == 17) || (pressedkey == 18) || (pressedkey == 19) || (pressedkey == 9) || (pressedkey == 17) || (pressedkey == 116) || (pressedkey >= 65 && pressedkey <= 90))) {
        e.preventDefault();
    }
});

$('#txtreglastname').keydown(function (e) {
    var pressedkey = e.keyCode;
    if (!((pressedkey == 8) || (pressedkey == 32) || (pressedkey == 46) || (pressedkey >= 35 && pressedkey <= 40) || (pressedkey == 17) || (pressedkey == 18) || (pressedkey == 19) || (pressedkey == 9) || (pressedkey == 17) || (pressedkey == 116) || (pressedkey >= 65 && pressedkey <= 90))) {
        e.preventDefault();
    }
});

$("#btntxtclear").on('click', function () {
    $("#txtloginmail").val('');
    $("#txtloginpwd").val('');
});

$("#btnpwdclear").on('click', function () {
    $("#txtforgotmail").val('');
});

$("#btnlogin").on('click', function () {
    var value;
    if ($('#chkremember').prop("checked") == true) {
        value = 1;
    }
    else {
        value = 0;
    }
    if ($.trim($('[id$=txtloginmail]').val()) == "") {
        var noty_alert = noty({
            layout: 'topRight',
            text: 'Please enter email id',
            type: 'warning',
            timeout: 3000,
            maxVisible: 1,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            }
        });
        $('[id$=txtloginmail]').focus();
        $('[id$=txtloginmail]').val("");
        return false;
    }
    if ($.trim($('[id$=txtloginpwd]').val()) == "") {
        var noty_alert = noty({
            layout: 'topRight',
            text: 'Please enter password',
            type: 'warning',
            timeout: 3000,
            maxVisible: 1,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            }
        });
        $('[id$=txtloginpwd]').focus();
        $('[id$=txtloginpwd]').val("");
        return false;
    }
    var dataObject = {
        username: $("#txtloginmail").val().toLowerCase(),
        pwd: $("#txtloginpwd").val(),
        rememberOpt: value
    };
    $('#WholePart').block({
        message: 'Checking Email-Id and Password..',
        css: {
            border: 'none',
            padding: '15px',
            float: 'right',
            'margin-right': '0%',
            backgroundColor: '#033363',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        }
    });
    if ($("#hidmyacc").val() == 1) {
        $.ajax({
            url: '../Trip/login',
            type: "POST",
            data: dataObject,
            dataType: "json",
            success: function (result) {
                notificationCount();
                $('#WholePart').unblock();
                if (result == "Not confirm")
                {
                    var noty_alert = noty({
                        layout: 'topRight',
                        text: 'Login Failed! Your Email-Id is not confirmed!!',
                        type: 'warning',
                        timeout: 3000,
                        maxVisible: 1,
                        animation: {
                            open: { height: 'toggle' },
                            close: { height: 'toggle' },
                            easing: 'swing',
                            speed: 500
                        }
                    });
                    $("#hidcustomizeid").val("0");
                    //$("#txtloginmail").val('');
                    $("#txtloginpwd").val('');
                }
                else if (result == null) {
                    var noty_alert = noty({
                        layout: 'topRight',
                        text: 'Login Failed! Your Email-Id and Password doesn\'t match!!',
                        type: 'warning',
                        timeout: 3000,
                        maxVisible: 1,
                        animation: {
                            open: { height: 'toggle' },
                            close: { height: 'toggle' },
                            easing: 'swing',
                            speed: 500
                        }
                    });
                    $("#hidcustomizeid").val("0");
                    //$("#txtloginmail").val('');
                    $("#txtloginpwd").val('');
                }
                else
                {
                    var noty_alert = noty({
                        layout: 'topRight',
                        text: 'You have successfully logged in',
                        type: 'success',
                        timeout: 3000,
                        maxVisible: 1,
                        animation: {
                            open: { height: 'toggle' },
                            close: { height: 'toggle' },
                            easing: 'swing',
                            speed: 500
                        }
                    });
                    $("#LoggedUserEmail").text($("#txtloginmail").val());
                    GroupIdConfirmation();
                    $('#LoggedUserLi').show();
                    $('#LogdUsrMyTrip').show();
                    $("#txtloginmail").val('');
                    $("#txtloginpwd").val('');
                    $("#liMyAccount").hide();
                    $("#register").hide();
                    $(".fancybox-overlay").hide();
                    $("#hidcustomizeid").val("1");
                    $("#hidcityid").val("1");
                    $("#wel-mem").show();
                    $("#liMyAccount").hide();
                    $("#lblusername").text(result[0].username);
                    $('#LoggedUserId').val(result[0].Id);
                }              
            },
            error: function (error) {
                $('#WholePart').unblock();
                var noty_alert = noty({
                    layout: 'topRight',
                    text: 'Login Failed! Your Email-id and Password doesn\'t match!!',
                    type: 'warning',
                    timeout: 3000,
                    maxVisible: 1,
                    animation: {
                        open: { height: 'toggle' },
                        close: { height: 'toggle' },
                        easing: 'swing',
                        speed: 500
                    }
                });
                $("#hidcustomizeid").val("0");
                //$("#txtloginmail").val('');
                $("#txtloginpwd").val('');
            }
        });
        return false;
    }
    else {
        $.ajax({
            url: '../Trip/login',
            type: "POST",
            data: dataObject,
            dataType: "json",
            success: function (result) {
                notificationCount();
                $('#WholePart').unblock();
                if (result == "Not confirm") {
                    var noty_alert = noty({
                        layout: 'topRight',
                        text: 'Login Failed! Your Email-Id is not confirmed!!',
                        type: 'warning',
                        timeout: 3000,
                        maxVisible: 1,
                        animation: {
                            open: { height: 'toggle' },
                            close: { height: 'toggle' },
                            easing: 'swing',
                            speed: 500
                        }
                    });
                    $("#hidcustomizeid").val("0");
                    //$("#txtloginmail").val('');
                    $("#txtloginpwd").val('');
                }
                else if (result == null) {
                    var noty_alert = noty({
                        layout: 'topRight',
                        text: 'Login Failed! Your Email-Id and Password doesn\'t match!!',
                        type: 'warning',
                        timeout: 3000,
                        maxVisible: 1,
                        animation: {
                            open: { height: 'toggle' },
                            close: { height: 'toggle' },
                            easing: 'swing',
                            speed: 500
                        }
                    });
                    $("#hidcustomizeid").val("0");
                    //$("#txtloginmail").val('');
                    $("#txtloginpwd").val('');
                }
                else {
                    var noty_alert = noty({
                        layout: 'topRight',
                        text: 'You have successfully logged in..',
                        type: 'success',
                        timeout: 3000,
                        maxVisible: 1,
                        animation: {
                            open: { height: 'toggle' },
                            close: { height: 'toggle' },
                            easing: 'swing',
                            speed: 500
                        }
                    });
                    $("#LoggedUserEmail").text($("#txtloginmail").val());
                    GroupIdConfirmation();
                    $('#LoggedUserLi').show();
                    $('#LogdUsrMyTrip').show();
                    $("#txtloginmail").val('');
                    $("#txtloginpwd").val('');
                    $("#liMyAccount").hide();
                    $("#register").hide();
                    $(".fancybox-overlay").hide();
                    $("#hidcustomizeid").val("1");
                    $("#hidcityid").val("1");
                    $("#hlcity").click();
                    $("#hlcity").attr("href", "#dest-popup");
                    $("#wel-mem").show();
                    $("#liMyAccount").hide();
                    $("#lblusername").text(result[0].username);
                    $('#LoggedUserId').val(result[0].Id);
                }
            },
            error: function (error) {
                $('#WholePart').unblock();
                var noty_alert = noty({
                    layout: 'topRight',
                    text: 'Login failed! Your Email-id and Password doesn\'t match!!',
                    type: 'warning',
                    timeout: 3000,
                    maxVisible: 1,
                    animation: {
                        open: { height: 'toggle' },
                        close: { height: 'toggle' },
                        easing: 'swing',
                        speed: 500
                    }
                });
                $("#hidcustomizeid").val("0");
                //$("#txtloginmail").val('');
                $("#txtloginpwd").val('');
            }
        });
        return false;
    }
});

$("#btnsend").on('click', function () {
    if ($.trim($('[id$=txtforgotmail]').val()) == "") {
        var noty_alert = noty({
            layout: 'topRight',
            text: 'Please enter mail-id',
            type: 'warning',
            timeout: 3000,
            maxVisible: 1,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            }
        });
        $('[id$=txtforgotmail]').focus();
        $('[id$=txtforgotmail]').val("");
        return false;
    }
    else {
        var dataObject = {
            username: $("#txtforgotmail").val().toLowerCase()
        };
        $('#WholePart').block({
            message: 'Sending Email..',
            css: {
                border: 'none',
                padding: '15px',
                float: 'right',
                'margin-right': '0%',
                backgroundColor: '#033363',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });
        $.ajax({
            url: '../Trip/forgot',
            type: "POST",
            data: dataObject,
            dataType: "json",
            success: function (result) {
                $('#WholePart').unblock();
                if (result.Success) {
                    if (result.Success == "false") {
                        var noty_alert = noty({
                            layout: 'topRight',
                            text: 'Oops!! something went wrong, mail is not delivered..',
                            type: 'warning',
                            timeout: 3000,
                            maxVisible: 1,
                            animation: {
                                open: { height: 'toggle' },
                                close: { height: 'toggle' },
                                easing: 'swing',
                                speed: 500
                            }
                        });
                        $("#txtforgotmail").val('');
                    }
                }
                else {
                    var noty_alert = noty({
                        layout: 'topRight',
                        text: 'Mail delivered successfully',
                        type: 'success',
                        timeout: 3000,
                        maxVisible: 1,
                        animation: {
                            open: { height: 'toggle' },
                            close: { height: 'toggle' },
                            easing: 'swing',
                            speed: 500
                        }
                    });
                    $("#txtforgotmail").val('');
                }
            },
            error: function (result) {
                $('#WholePart').unblock();
                var noty_alert = noty({
                    layout: 'topRight',
                    text: 'Mail delivery failed',
                    type: 'warning',
                    timeout: 3000,
                    maxVisible: 1,
                    animation: {
                        open: { height: 'toggle' },
                        close: { height: 'toggle' },
                        easing: 'swing',
                        speed: 500
                    }
                });
            }
        });
    }
});

$('#btnsubmit').on('click', function () {
    if (city != "" && city != null) {
        $.getJSON('../Trip/latlng', { lat: latitude, lng: longitude, indexcity: city,IsTopItin:'0',TopItinId:'0' }, function (data) {
            window.location.href = "../Trip/FirstDay";
        });
    }
    else {
        var noty_alert = noty({
            layout: 'topRight',
            text: 'Enter a city name!',
            type: 'warning',
            maxVisible: 1,
            timeout: 3000,
            maxVisible: 1,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            }
        });
    }
});

$("#slide").click(function () {
    $(".slide1").slideToggle("slow");
    $("#txtloginmail").focus();
});

$("#view").click(function () {
    $("#dest-popup2").slideDown("slow");
});

$("#view1").click(function () {
    $("#Itin-popup").slideToggle("slow");
});

$("#scrollToDestinations").click(function () {
    $('html, body').animate({
        scrollTop: $("#Destinations").offset().top
    }, 500);
});

$("#scrollToItineraries").click(function () {
    $('html, body').animate({
        scrollTop: $("#Itineraries").offset().top
    }, 500);
});

function select(city, latitude, longitude) {
    $("#desttext").val('');
    $("#desttext").val(city);
    $.getJSON('../Trip/latlng', { lat: latitude, lng: longitude, indexcity: city }, function (data) {
        window.location.href = "../Trip/FirstDay";
    });
}

function NaviagteToNextPage(Latitude, Longitude, city) {
    var noty_alert = noty({
        layout: 'topRight',
        text: 'Enjoy your trip to '+ city,
        type: 'success',
        timeout: 3000,
        maxVisible: 1,
        animation: {
            open: { height: 'toggle' },
            close: { height: 'toggle' },
            easing: 'swing',
            speed: 500
        }
    });
    $.getJSON('../Trip/latlng', { lat: Latitude, lng: Longitude, indexcity: city, IsTopItin: '0', TopItinId: '0' }, function (data) {
        window.location.href = "../Trip/FirstDay";
    });
}

function Logout() {
    noty({
        layout: 'topRight',
        type: 'confirm',
        maxVisible: 1,
        text: 'Do you want to logout?',
        buttons: [
            {
                addClass: 'btn btn-primary', text: 'Ok', onClick: function ($noty) {
                    $noty.close();
                    $('#lblusername').text('');
                    $.ajax({
                        type: "POST",
                        url: '../Trip/ClearLoginSession',
                        success: function () {
                            window.location.href = "../Trip/Index";
                        }
                    })
                    FB.logout();
                }
            },
            {
                addClass: 'btn btn-danger', text: 'Cancel', onClick: function ($noty) {
                    $noty.close();
                    return false;
                }
            }
        ]
    });
}

function fblogout() {
    noty({
        layout: 'topRight',
        type: 'confirm',
        maxVisible: 1,
        text: 'This will also log you out of Facebook. Proceed?',
        buttons: [
            {
                addClass: 'btn btn-primary', text: 'Ok', onClick: function ($noty) {
                    $noty.close();
                    FB.logout(function (response) {
                    });
                }
            },
            {
                addClass: 'btn btn-danger', text: 'Cancel', onClick: function ($noty) {
                    $noty.close();
                    e.PreventDefault();
                }
            }
        ]
    });
}

function onlyAlphabets(e, t) {
    try {
        if (window.event) {
            var charCode = window.event.keyCode;
        }
        else if (e) {
            var charCode = e.which;
        }
        else { return true; }
        if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123))
            return true;
        else
            return false;
    }
    catch (err) {
        var notify_alert = noty({
            layout: 'topRight',
            text: err.Description,
            type: 'warning',
            timeout: 3000,
            maxVisible: 1,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            }
        });        
    }
}

function MatchPassword(password) {
    var Regex = /^[a-zA-Z0-9]{7,}$/;
    return Regex.test(password);
}

google.maps.event.addDomListener(window, 'load', function () {  
    var options = {
        types: ['(cities)']
    };
    var places = new google.maps.places.Autocomplete(document.getElementById('desttext'), options);
    google.maps.event.addListener(places, 'place_changed', function () {
        $('#btnsubmit').prop('disabled', true);       
        var place = places.getPlace();
        var address = place.formatted_address;
        latitude = place.geometry.location.lat();
        longitude = place.geometry.location.lng();
        city = $('#desttext').val();
        $('#btnsubmit').prop('disabled', false);
    });
});

function notificationCount() {
    var totalNotificationCount;
    $.ajax({
        url: '../Trip/GetNotificationCount',
        type: "POST",
        async: false,
        dataType: "json",
        success: function (result) {
            totalNotificationCount = result;
            if (totalNotificationCount > 0) {
                $("#notificationCount").show();
                $("#notificationCount").text(totalNotificationCount);
            } else {
                $("#notificationCount").hide();
            }
        }
    });
}

function NavigateToTrip(Latitude, Longitude, city,TopItinId)
{
    var noty_alert = noty({
        layout: 'topRight',
        text: 'Enjoy your trip to ' + city,
        type: 'success',
        timeout: 3000,
        maxVisible: 1,
        animation: {
            open: { height: 'toggle' },
            close: { height: 'toggle' },
            easing: 'swing',
            speed: 500
        }
    });
    $.getJSON('../Trip/latlng', { lat: Latitude, lng: Longitude, indexcity: city, IsTopItin: '1', TopItinId: TopItinId }, function (response) {
        window.location.href = "../Trip/FirstDay";
    });
}