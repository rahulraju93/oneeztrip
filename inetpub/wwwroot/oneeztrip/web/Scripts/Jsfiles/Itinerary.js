﻿function CreatePackage() {
    if (identify_city == 0) {
        Getcitieslist();
    }
    else {
        UserpreferenceAttraction();
    }
}

function Getcitieslist() {
    var incre_cities_tmp = 0;
    $("#citypref li").each(function () {
        incre_cities_tmp++;
    });
    $("#citypref li").each(function () {
        var getcityname = $(this).find("a").text();
        var citylatitude = "", citylongitude = "";
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'address': getcityname }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                citylatitude = results[0].geometry.location.lat();
                citylongitude = results[0].geometry.location.lng();
                citieslist_array.push({
                    city_name: getcityname,
                    city_latitude: citylatitude,
                    city_longitude: citylongitude
                });
                incre_for_cities = (citieslist_array.length) - 1;
                incre_cities_tmp--;

            }
            else {
                incre_cities_tmp--;
            }
            if (incre_cities_tmp < 1) {
                UserpreferenceAttraction();
            }
        });
    });
    if (isaddnewcity > 0) {
        citieslist_array.reverse();
    }
}

function UserpreferenceAttraction() {
    oldatt_actarray.length = 0, oldotherattraction.length = 0;
    att_actarray.length = 0, otherattraction.length = 0;
    secondClassAttr.length = 0;
    TotalRests = 0;
    AllRestCollect.length = 0;
    incrRests = 0;
    oldrest_array.length = 0;
    sortedarrayall.length = 0;
    tmpOtherAttr.length = 0;
    attus_arr_len=0,attus_arr_len1=0;
    var filtattcat = "", filtcntatt = 0;
    incrAttrs = 0;
    AllAttrCollect.length = 0;
    increAttrDet = 0, gotattrDet = 0, increOthDet = 0, gotOthDet = 0;
    restStatus = 0;
    attrStatus = 0;
    if (isaddnewcity == 0) {
        forOrderingCityArray = citieslist_array.slice();
        citieslist_array.length = 0;
        $("#citypref li").each(function () {
            var getcityname = $(this).find("a").text();
            for (var tmpLoop = 0, OrderCityLength = forOrderingCityArray.length; tmpLoop < OrderCityLength; tmpLoop++) {
                if (getcityname.trim() == forOrderingCityArray[tmpLoop].city_name.trim()) {
                    citieslist_array.push(forOrderingCityArray[tmpLoop]);
                }
            }
        });
        $.getJSON('../Trip/Getuserpreference', function (data) {
            $.each(data, function (key, values) {
                var useratt = (values.attraction).toString();
                user_attr_pref = (values.attraction).toString();
                user_act = (values.activity).toString();
                user_rest = (values.cuisine).toString();
                user_numofdays = (values.No_of_days).toString();
                if (values.Hotel_class != null && values.Hotel_class != "") {
                    user_hotelcls = (values.Hotel_class).toString();
                }
                else {
                    user_hotelcls = "";
                }
                if (values.star != null && values.star != "") {
                    user_hotelrating = (values.star).toString();
                }
                else {
                    user_hotelrating = "";
                }
                user_hStartprice = (values.startprice).toString();
                user_hEndprice = (values.endprice).toString();
                if (useratt != "Nooption") {
                    var spatt_new = useratt.split(',');
                    AttractionOptions = spatt_new.length;
                    attus_arr_len=0;
                    var increCnt = 1;
                    var newarrayTmp = "";
                    var pushToArray = [];
                    for (var loopvar = 0; loopvar < AttractionOptions; loopvar ++) {
                        pushToArray.push(spatt_new[loopvar]);
                        attus_arr_len++;
                        Attractionfunctioncall(spatt_new[loopvar]);
                    }
                    //for (var loopvar = 0; loopvar < AttractionOptions; loopvar++) {
                    //    if((loopvar+1)%2==0 && loopvar>0 && (loopvar+1)<AttractionOptions)
                    //    {
                    //        newarrayTmp = pushToArray[loopvar] + "|" + pushToArray[loopvar - 1];
                    //        attus_arr_len++;
                    //        Attractionfunctioncall(newarrayTmp);
                    //    }
                    //    else if((loopvar+1)==AttractionOptions)
                    //    {
                    //        newarrayTmp = pushToArray[loopvar];
                    //        attus_arr_len++;
                    //        Attractionfunctioncall(newarrayTmp);
                    //    }
                    //}
                    //newarrayTmp = pushToArray.join("|");
                    //Attractionfunctioncall(pushToArray[0]);
                }
                else {
                    var increCnt = 0;
                    var spatt_new = [];
                    spatt_new.push("aquarium", "place_of_worship", "museum", "art_gallery", "amusement_park","park");
                    var AttractionOptions = spatt_new.length;
                    attus_arr_len = Math.ceil(AttractionOptions / 6);
                    var newarrayTmp = "";
                    for (var loopvar = 0; loopvar < AttractionOptions; loopvar+=6) {
                        increCnt++;
                        if (increCnt < attus_arr_len || AttractionOptions % 6 == 0) {
                            newarrayTmp = spatt_new[loopvar + 5] + "|" + spatt_new[loopvar + 4] + "|" + spatt_new[loopvar + 3] + "|" + spatt_new[loopvar + 2] + "|" + spatt_new[loopvar + 1] + "|" + spatt_new[loopvar];
                            Attractionfunctioncall(newarrayTmp);
                        }
                    }
                        increCnt = 0;
                        var spatt_new1 = [];                        
                        spatt_new1.push("palace", "botanical garden", "stadium", "bridge", "cathedral", "museum", "beach", "island", "zoo", "square", "statue", "harbour", "pier", "tower", "historical building");
                        AttractionOptions = spatt_new1.length;
                        attus_arr_len1= Math.ceil(AttractionOptions / 5);
                        for (var loopvar = 0; loopvar < AttractionOptions; loopvar+=5) {
                        increCnt++;
                        if (increCnt < attus_arr_len1 || AttractionOptions % 5 == 0) {                            
                            newarrayTmp = "(" + spatt_new1[loopvar + 4] + " OR " + spatt_new1[loopvar + 3] + " OR " + spatt_new1[loopvar + 2] + " OR " + spatt_new1[loopvar + 1] + " OR " + spatt_new1[loopvar] + ")";
                            Attractionfunctioncall1(newarrayTmp);
                        }
                    }
                }
            });
        });
    }
    else {
        user_hotelcls = "";
        user_hotelrating = "";
        user_hStartprice = 10;
        user_hEndprice = 2000;
        var spatt_new = [];
        user_attr_pref = "Nooption";
        spatt_new.push("aquarium", "place_of_worship", "museum", "art_gallery", "amusement_park","park");
        var AttractionOptions = spatt_new.length;
        attus_arr_len= Math.ceil(AttractionOptions / 6);
        var increCnt = 0;
        var newarrayTmp = "";
        for (var loopvar = 0; loopvar < AttractionOptions; loopvar+=6) {
            increCnt++;
            if (increCnt < attus_arr_len || AttractionOptions % 6 == 0) {                
                newarrayTmp = spatt_new[loopvar + 5] + "|" + spatt_new[loopvar + 4] + "|" + spatt_new[loopvar + 3] + "|" + spatt_new[loopvar + 2] + "|" + spatt_new[loopvar + 1] + "|" + spatt_new[loopvar];
                Attractionfunctioncall(newarrayTmp);
            }
        }
        var spatt_new1 = [];        
        spatt_new1.push("palace", "botanical garden", "stadium", "bridge", "cathedral", "museum", "beach", "island", "zoo", "square", "statue", "harbour", "pier", "tower", "historical building");
        AttractionOptions = spatt_new1.length;
        attus_arr_len1 = Math.ceil(AttractionOptions / 5);
        increCnt = 0;
        for (var loopvar = 0; loopvar < AttractionOptions; loopvar+=5) {
            increCnt++;
            if (increCnt < attus_arr_len1 || AttractionOptions % 5 == 0) {
                newarrayTmp = "(" + spatt_new1[loopvar + 4] + " OR " + spatt_new1[loopvar + 3] + " OR " + spatt_new1[loopvar + 2] + " OR " + spatt_new1[loopvar + 1] + " OR " + spatt_new1[loopvar] + ")";
                Attractionfunctioncall1(newarrayTmp);
            }
        }
        user_rest = "Nooption";
    }
}

function Attractionfunctioncall(att_category) {
    //debugger
    var pyrmont = "";
    if (onretrieve_addday_identify == 0) {
        if (identify_city == 0) {
            pyrmont = new google.maps.LatLng($('#latarray').val(), $('#lngarray').val());
            current_city = $('#firstCityname1').val();
        }
        else if (identify_city == 1) {
            pyrmont = new google.maps.LatLng(nextcity_lat, nextcity_lng);
        }
    }
    else {
        pyrmont = new google.maps.LatLng(add_day_citylatitude, add_day_citylongitude);
    }
    map = new google.maps.Map(document.getElementById('map-canvas'), {
        center: pyrmont,
        zoom: 15
    });
    var request = {
        location: pyrmont,
        radius: 20000,
        rankby: ['prominence'],
        types: [att_category]
    };
    var service = new google.maps.places.PlacesService(map);
    service.nearbySearch(request, function (response, status) { callbackattraction(response, status,att_category)});
}

function callbackattraction(results, status, att_category) {
    //debugger
    if (status == google.maps.places.PlacesServiceStatus.OK) {
        var needsExclusions = [];
        ListAttractions.push({
            attrType: att_category,
            attractions: results,
            cityname: current_city
        });
        for (var loopvar = 0, ResultsLength = results.length; loopvar < ResultsLength; loopvar++) {
            if (results[loopvar].types) {
                needsExclusions = results[loopvar].types;
                someFlag = true;
                for (var index1 = 0; index1 < excludeMe.length; index1++) {
                    if (needsExclusions.indexOf(excludeMe[index1]) !== -1) {
                        someFlag = false;
                        break;
                    }
                }
                if (someFlag) {
                    if ((results[loopvar].name).toLowerCase().indexOf("college") >= 0) {
                        someFlag = false;
                    }
                }
            }
            else
            {
                someFlag = false;
            }
            if (someFlag && results[loopvar].rating) {
                if (results[loopvar].rating >= 1) {
                    AllAttrCollect.push(results[loopvar]);
                    attraction_array.push(results[loopvar]);
                    att_actarray.push(results[loopvar]);
                    attrDetails.push({
                        plc_id: results[loopvar].place_id,
                        attrType: att_category,
                        details: results[loopvar]
                    });
                }
            }
        }
    }
    attus_arr_len--;
    if (attus_arr_len == 0 && attus_arr_len1 == 0) {
        attrStatus = 1;
        TotalAttrs = AllAttrCollect.length;
        UserpreferenceRestaurant();
        //GetAllAttrDet();
    }
}

function Attractionfunctioncall1(att_category1) {
    var pyrmont = "";
    if (onretrieve_addday_identify == 0) {
        if (identify_city == 0) {
            pyrmont = new google.maps.LatLng($('#latarray').val(), $('#lngarray').val());
        }
        else if (identify_city == 1) {
            pyrmont = new google.maps.LatLng(nextcity_lat, nextcity_lng);
        }
    }
    else {
        pyrmont = new google.maps.LatLng(add_day_citylatitude, add_day_citylongitude);
    }
    map = new google.maps.Map(document.getElementById('map-canvas'), {
        center: pyrmont,
        zoom: 15
    });
    var request = {
        location: pyrmont,
        radius: 20000,
        rankby: ['prominence'],
        keyword: [att_category1]
    };
    var service = new google.maps.places.PlacesService(map);
    service.nearbySearch(request, function (response, status) { callbackattraction1(response, status, att_category1) });
}

function callbackattraction1(results, status, att_category1) {
    if (status == google.maps.places.PlacesServiceStatus.OK) {
        ListAttractions.push({
            attrType: att_category1,
            attractions: results,
            cityname: current_city
        });
        var needsExclusions = [];
        for (var loopvar = 0, ResultsLength = results.length; loopvar < ResultsLength; loopvar++) {
            if (results[loopvar].types) {
                needsExclusions = results[loopvar].types;
                someFlag = true;
                for (var index1 = 0; index1 < excludeMe.length; index1++) {
                    if (needsExclusions.indexOf(excludeMe[index1]) !== -1) {
                        someFlag = false;
                        break;
                    }
                }
                if (someFlag) {
                    if ((results[loopvar].name).toLowerCase().indexOf("college") >= 0) {
                        someFlag = false;
                    }
                }
            }
            else {
                someFlag = false;
            }
            if (someFlag && results[loopvar].rating) {
                if (results[loopvar].rating >= 1) {
                    AllAttrCollect.push(results[loopvar]);
                    attraction_array.push(results[loopvar]);
                    att_actarray.push(results[loopvar]);
                    attrDetails.push({
                        plc_id: results[loopvar].place_id,
                        attrType: att_category1,
                        details: results[loopvar]
                    });
                }
            }
        }
    }
    attus_arr_len1--;
    if (attus_arr_len1 == 0 && attus_arr_len == 0) {
        attrStatus = 1;
        TotalAttrs = AllAttrCollect.length;
        UserpreferenceRestaurant();
        //GetAllAttrDet();        
    }
}

function GetAllAttrDet() {
    if (TotalAttrs == incrAttrs) {
        for (var arrIndex = 0, arrLen = oldatt_actarray.length; arrIndex < arrLen; arrIndex++) {
            if (oldatt_actarray[arrIndex].user_ratings_total) {
                if (oldatt_actarray[arrIndex].user_ratings_total > 700) {
                    att_actarray.push(oldatt_actarray[arrIndex]);
                }
                else if (oldatt_actarray[arrIndex].user_ratings_total > 200) {
                    secondClassAttr.push(oldatt_actarray[arrIndex]);
                }
                else if (oldatt_actarray[arrIndex].user_ratings_total > 50) {
                    thirdClassAttr.push(oldatt_actarray[arrIndex]);
                }
                else if (oldatt_actarray[arrIndex].user_ratings_total > 10) {
                    otherattraction.push(oldatt_actarray[arrIndex]);
                }
            }
        }
        setTimeout("UserpreferenceRestaurant()", 2000);        
    }
    else {
        var gotFlag = true;
        var places = AllAttrCollect[incrAttrs];
        for (var loopVal = 0, attrLen = oldatt_actarray.length; loopVal < attrLen; loopVal++) {
            if (oldatt_actarray[loopVal].idPlc == places.place_id) {
                gotFlag = false;
            }
        }
        if (gotFlag) {
            oldatt_actarray.push({
                plc_id: "attr||" + places.place_id,
                idPlc: places.place_id,
                plc_name: places.name,
                plc_lat: places.geometry.location.lat(),
                plc_lng: places.geometry.location.lng()
            });
        }
        var placeID = AllAttrCollect[incrAttrs].place_id;
        var request = {
            placeId: placeID
        };
        var service = new google.maps.places.PlacesService(map);
        service.getDetails(request, function (place, status) {
            if (status == google.maps.places.PlacesServiceStatus.OK) {
                if (place.user_ratings_total) {
                    allattractions_toleft.push(place);
                    for (var indxVal = 0, attrDetVal = attrDetails.length; indxVal < attrDetVal; indxVal++) {
                        if (attrDetails[indxVal].plc_id == place.place_id) {
                            attrDetails[indxVal].UserReviews = place.user_ratings_total;
                            attrDetails[indxVal].details = place;
                            break;
                        }
                    }
                    for (var arrIndex = 0, arrLen = oldatt_actarray.length; arrIndex < arrLen; arrIndex++) {
                        if (oldatt_actarray[arrIndex].idPlc == place.place_id) {
                            oldatt_actarray[arrIndex].user_ratings_total = place.user_ratings_total;
                            if (place.opening_hours) {
                                oldatt_actarray[arrIndex].plc_time = place.opening_hours;
                            }
                            else {
                                oldatt_actarray[arrIndex].plc_time = "No time";
                            }
                            break;
                        }
                    }
                }
                incrAttrs++;
                GetAllAttrDet();
            }
            else {
                if (status == google.maps.GeocoderStatus.OVER_QUERY_LIMIT) {
                    sleep(1000);
                    GetAllAttrDet();
                }
                else
                {
                    incrAttrs++;
                    GetAllAttrDet();
                }
            }
        });
    }
}

function sleep(milliseconds) {
    var start = new Date().getTime();
    for (var indexVal = 0; indexVal < 1e7; indexVal++) {
        if ((new Date().getTime() - start) > milliseconds) {
            break;
        }
    }
}

function UserpreferenceRestaurant() {
    var sprest = user_rest.split(',');
    if (user_rest == "Nooption") {       
        var arr_noopt_res = [];
        arr_noopt_res.push("mediterranean", "european", "mexican", "american", "french", "spanish", "italian", "indian", "japanese", "chinese", "thai", "asian", "breakfast", "pizza", "dessert", "bakery");
        var RestaurantsOptions = arr_noopt_res.length;
        resus_arr_len = Math.ceil(RestaurantsOptions / 4);
        var newTmpRest = "";
        var increCnt = 0;
        for (var loopvar = 0; loopvar < RestaurantsOptions; loopvar+=4) {
            increCnt++;
            if (increCnt < resus_arr_len || RestaurantsOptions % 4 == 0) {
                newTmpRest = "(" + arr_noopt_res[loopvar + 3] + " OR " + arr_noopt_res[loopvar + 2] + " OR " + arr_noopt_res[loopvar + 1] + " OR " + arr_noopt_res[loopvar] + ")";
                    Restaurantfunctioncall(newTmpRest); 
            }
        }
    }
    else {
        callRestaurantsIncr = 0;
        var RestaurantsOptions = sprest.length;
        resus_arr_len = Math.ceil(RestaurantsOptions / 2);
        var newTmpRest ="";
        for (var loopvar = 0; loopvar < RestaurantsOptions; loopvar++) {
            callRestaurantsIncr++;
            var extraTypes = RestaurantsOptions - loopvar;
            if (callRestaurantsIncr == 2 || extraTypes < 2) {
                if (callRestaurantsIncr == 2) {
                    newTmpRest = "(" + sprest[loopvar - 1] + " OR " + sprest[loopvar] + ")";
                    Restaurantfunctioncall(newTmpRest);
                }
                else
                {
                    Restaurantfunctioncall(sprest[loopvar]);
                }
                callRestaurantsIncr = 0;
            }
        }
    }
}

function Restaurantfunctioncall(rest_category) {
    var pyrmont = "";
    if (onretrieve_addday_identify == 0) {
        if (identify_city == 0) {
            pyrmont = new google.maps.LatLng($('#latarray').val(), $('#lngarray').val());
        }
        else if (identify_city == 1) {
            pyrmont = new google.maps.LatLng(nextcity_lat, nextcity_lng);
        }
    }
    else {
        pyrmont = new google.maps.LatLng(add_day_citylatitude, add_day_citylongitude);
    }
    map = new google.maps.Map(document.getElementById('map-canvas'), {
        center: pyrmont,
        zoom: 15
    });
    var request = {
        location: pyrmont,
        radius: 5000,
        rankby: ['prominence'],
        types: ['restaurant|food|bakery'],
        keyword: [rest_category]
    };
    var service = new google.maps.places.PlacesService(map);
    service.nearbySearch(request,function (results, status) { callback_restaurant(results, status, rest_category)});
}

function callback_restaurant(results, status,rest_category) {
    if (status == google.maps.places.PlacesServiceStatus.OK) {
        var needsExclusions = [];
        ListRestaurants.push({
            resType: rest_category,
            restaurants: results,
            cityname: current_city
        });
        for (var loopvar = 0, ResultsLength = results.length; loopvar < ResultsLength; loopvar++) {
            if (results[loopvar].rating) {
                if (results[loopvar].types) {
                    needsExclusions = results[loopvar].types;
                    someFlag = false;
                    //for (var index1 = 0; index1 < Rest_excludeMe.length; index1++) {
                    //    if (needsExclusions.indexOf(Rest_excludeMe[index1]) !== -1) {
                    //        someFlag = false;
                    //        break;
                    //    }
                    //}
                    for (var index2 = 0; index2 < Rest_includeMe.length; index2++) {
                        if (needsExclusions.indexOf(Rest_includeMe[index2]) !== -1) {
                            someFlag = true;
                            break;
                        }
                    }
                    
                    if (someFlag) {
                        AllRestCollect.push(results[loopvar]);
                        restlist_array.push(results[loopvar]);
                        rest_array.push(results[loopvar]);
                        restDetails.push({
                            plc_id: results[loopvar].place_id,
                            resType: rest_category,
                            details: results[loopvar]
                        });
                    }
                }
            }
        }
    }
    resus_arr_len--;
    if (resus_arr_len == 0) {
        TotalRests = AllRestCollect.length;
        //GetAllRestDet();
        UserpreferenceHotel();
        restStatus = 1;
    }
}

function GetAllRestDet() {
    if (TotalRests == incrRests) {
        for (var arrIndex = 0, arrLen = oldrest_array.length; arrIndex < arrLen; arrIndex++) {
            if (oldrest_array[arrIndex].user_ratings_total) {                
                if (oldrest_array[arrIndex].user_ratings_total > 50) {
                    rest_array.push(oldrest_array[arrIndex]);
                }
                else {
                    otherrestaurant.push(oldrest_array[arrIndex]);
                }
            }
        }
        UserpreferenceHotel();
    }
    else {
        var gotFlag = true;
        var places = AllRestCollect[incrRests];
        for (var loopVal = 0, restLen = oldrest_array.length; loopVal < restLen; loopVal++) {
            if (oldrest_array[loopVal].plc_id == places.place_id) {
                gotFlag = false;
            }
        }
        if (gotFlag) {
            oldrest_array.push({
                plc_id: places.place_id,
                plc_name: places.name,
                plc_lat: places.geometry.location.lat(),
                plc_lng: places.geometry.location.lng()
            });
        }
        var placeID = AllRestCollect[incrRests].place_id;
        var request = {
            placeId: placeID
        };
        var service = new google.maps.places.PlacesService(map);
        service.getDetails(request, function (place, status) {
            if (status == google.maps.places.PlacesServiceStatus.OK) {
                if (place.user_ratings_total) {
                    for (var indxVal = 0, restDetVal = restDetails.length; indxVal < restDetVal; indxVal++) {
                        if (restDetails[indxVal].plc_id == place.place_id) {
                            restDetails[indxVal].UserReviews = place.user_ratings_total;
                            restDetails[indxVal].details = place;
                            break;
                        }
                    }
                    for (var arrIndex = 0, arrLen = oldrest_array.length; arrIndex < arrLen; arrIndex++) {
                        if (oldrest_array[arrIndex].plc_id == place.place_id) {
                            oldrest_array[arrIndex].user_ratings_total = place.user_ratings_total;
                            if (place.opening_hours) {
                                oldrest_array[arrIndex].plc_time = place.opening_hours;
                            }
                            else {
                                oldrest_array[arrIndex].plc_time = "No time";
                            }
                            break;
                        }
                    }
                }
                incrRests++;
                GetAllRestDet();
            }
            else {
                if (status == google.maps.GeocoderStatus.OVER_QUERY_LIMIT) {
                    sleep(1000);
                    GetAllRestDet();
                }
                else {
                    incrRests++;
                    GetAllRestDet();
                }
            }
        });
    }
}

function addrestaurant_to_array(places) {
    if (places.rating) {
        if (places.rating >= 3) {
            rest_array.push({
                plc_id: places.place_id,
                plc_name: places.name,
                plc_lat: places.geometry.location.lat(),
                plc_lng: places.geometry.location.lng()
            });
        }
        else {
            otherrestaurant.push({
                plc_id: places.place_id,
                plc_name: places.name,
                plc_lat: places.geometry.location.lat(),
                plc_lng: places.geometry.location.lng()
            });
        }
    }
    else {
        otherrestaurant.push({
            plc_id: places.place_id,
            plc_name: places.name,
            plc_lat: places.geometry.location.lat(),
            plc_lng: places.geometry.location.lng()
        });
    }
}

function UserpreferenceHotel() {    
        var hotelArrayForFilter = [];
        var currentLat = "";
        var currentLng = "";
        if (identify_city == 0) {
            currentLat = $('#latarray').val();
            currentLng = $('#lngarray').val();
        }
        else if (identify_city == 1) {
            currentLat = nextcity_lat;
            currentLng = nextcity_lng;
        }

        var currentCity = "";

        $("#citypref li a").each(function () {
            if ($(this).data('value') == currentLat + "||" + currentLng) {
                currentCity = $(this).html();
            }
        });
        got_hotelResult = 0;
        for (var loopvarHotels = 0, CitiesHotelsLength = hotelsForCities.length; loopvarHotels < CitiesHotelsLength; loopvarHotels++) {
            if (hotelsForCities[loopvarHotels].cityName.trim() == currentCity.trim()) {
                got_hotelResult = 1;
                break;
            }
        }
        if (got_hotelResult == 1) {
        for (var loopvarHotels = 0, CitiesHotelsLength = hotelsForCities.length; loopvarHotels < CitiesHotelsLength; loopvarHotels++) {
            if (hotelsForCities[loopvarHotels].cityName.trim() == currentCity.trim()) {
                if (hotelsForCities[loopvarHotels].Hotels.HotelListResponse) {
                    if (hotelsForCities[loopvarHotels].Hotels.HotelListResponse.HotelList) {
                        if (hotelsForCities[loopvarHotels].Hotels.HotelListResponse.HotelList["@size"] == 1) {
                            hotelArrayForFilter = hotelsForCities[loopvarHotels].Hotels.HotelListResponse.HotelList['HotelSummary'];
                        }
                        else {
                            hotelArrayForFilter = hotelsForCities[loopvarHotels].Hotels.HotelListResponse.HotelList['HotelSummary'].slice();
                        }
                    }
                }
            }
        }

        var sphotelrating = user_hotelrating.split(',');
        var sphotelclass = user_hotelcls.split(',');
        var filthr = [], filthc = [];
        var filthrstr = "";
        for (var loopvar = 0, HotelRatingCount = sphotelrating.length; loopvar < HotelRatingCount ; loopvar++) {
            if (sphotelrating[loopvar] == 1) {
                filthr.push("el.tripAdvisorRating < 2 && el.tripAdvisorRating >= 1");
            }
            else if (sphotelrating[loopvar] == 2) {
                filthr.push("el.tripAdvisorRating < 3 && el.tripAdvisorRating >= 2");
            }
            else if (sphotelrating[loopvar] == 3) {
                filthr.push("el.tripAdvisorRating < 4 && el.tripAdvisorRating >= 3");
            }
            else if (sphotelrating[loopvar] == 4) {
                filthr.push("el.tripAdvisorRating < 5 && el.tripAdvisorRating >= 4");
            }
            else if (sphotelrating[loopvar] == 5) {
                filthr.push("el.tripAdvisorRating == 5");
            }
        }
        for (var loopvari = 0, HotelClassCount = sphotelclass.length; loopvari < HotelClassCount; loopvari++) {
            if (sphotelclass[loopvari] == 1) {
                filthc.push("el.hotelRating < 2 && el.hotelRating >= 1");
            }
            else if (sphotelclass[loopvari] == 2) {
                filthc.push("el.hotelRating < 3 && el.hotelRating >= 2");
            }
            else if (sphotelclass[loopvari] == 3) {
                filthc.push("el.hotelRating < 4 && el.hotelRating >= 3");
            }
            else if (sphotelclass[loopvari] == 4) {
                filthc.push("el.hotelRating < 5 && el.hotelRating >= 4");
            }
            else if (sphotelclass[loopvari] == 5) {
                filthc.push("el.hotelRating == 5");
            }
        }

        if (filthr.length > 0) {
            var filthrstrTemp = filthr.join(" || ");
            filthrstr += "(" + filthrstrTemp + ") && ";
        }
        if (filthc.length >= 1) {
            var filthrstrTemp = filthc.join(" || ");
            filthrstr += "(" + filthrstrTemp + ") && ";
        }
        filthrstr += "(el.highRate>=" + user_hStartprice + " && el.highRate<=" + user_hEndprice + ")";

        if (hotelArrayForFilter.length) {
            if (filthrstr == "\"(el.highRate >= 10 && el.highRate <= 2000)\"") {
                finalfilterArrayfull = hotelArrayForFilter.slice();
            }
            else {
                finalfilterArrayfull = hotelArrayForFilter.filter(function (el) {
                    return eval(filthrstr);
                });
            }
        }
        else {
            finalfilterArrayfull = jQuery.extend({}, hotelArrayForFilter);
        }
        $.each(finalfilterArrayfull, function (keys, values) {
            hotel_array.push({
                plc_id: values.hotelId,
                plc_name: values.name,
                plc_lat: values.latitude,
                plc_lng: values.longitude
            });
            hotel_arrayfull.push(finalfilterArrayfull[keys]);
        });

        if (onretrieve_addday_identify == 0) {
            callItineraryCreator();
        }
    }
    else
    {
        setTimeout("UserpreferenceHotel()", 2000);
    }
}

function UserpreferenceActInitialise() {
    var userpref_act = "";
    var filtactcat = "", filtcntact = 0;
    var closest = 0, next_clslat = "", next_dist = "";
    var next_clslng = "", next_place = "";
    var src_lat = "", src_lng = "";
    var distanceis = "";
    var mindist = 99999;
    $.getJSON('../Trip/Getuserpreference', function (data) {
        $.each(data, function (key, values) {
            var spact = user_act.split(',');
            for (var loopvar = 0, ActivitiesLength = spact.length; loopvar < ActivitiesLength; loopvar++) {
                filtcntact++;
            }
            for (var loopvar = 0; loopvar < spact.length; loopvar++) {
                if (spact[loopvar] === "Tours") {
                    filtactcat += "1,172";
                }
                else if (spact[loopvar] === "Concerts_Operas") {
                    filtactcat += "137";
                }
                else if (spact[loopvar] === "Shows_Musicals") {
                    filtactcat += "124,127,128";
                }
                else if (spact[loopvar] === "Meet_the_Locals") {
                    filtactcat += "34";
                }
                else if (spact[loopvar] === "Desert_Safaris") {
                    filtactcat += "37";
                }
                else if (spact[loopvar] === "Ski_Snowboard") {
                    filtactcat += "146";
                }
                else if (spact[loopvar] === "Sport_Events") {
                    filtactcat += "138";
                }
                else if (spact[loopvar] === "Shop") {
                    filtactcat += "20";
                }
                else if (spact[loopvar] === "Spa") {
                    filtactcat += "92,93";
                }
                else if (spact[loopvar] === "Movie") {
                    filtactcat += "126,22";
                }
                else if (spact[loopvar] === "Night") {
                    filtactcat += "111,112";
                }
                else if (spact[loopvar] === "Park") {
                    filtactcat += "36,131,136,159,192";
                }
                else if (spact[loopvar] === "Nooption") {
                    filtactcat += "1,172,137,124,127,128,34,37,146,138,20,92,93,126,22,111,112,36,131,136,159,192";
                }
                if (filtcntact != 0) {
                    filtactcat += "1";
                }
            }
        });
        var centrelat = "";
        var centrelng = "";
        var centrename = "";
        var centreid = "";
        $.ajax({
            type: "GET",
            dataType: "json",
            data: { filterdata: filtactcat },
            url: '../Trip/FilterActivitiesInit',
            success: function (response) {
                var itss = "";
                var tableData = JSON.stringify(response);
                var resdata = $.parseJSON(tableData);
                $.each(resdata, function (keys, values) {
                    centrelat = values.start_Latitude;
                    centrelng = values.start_Longitude;
                    centrename = values.title;
                    centreid = values.id;
                    activitiesfull_array.push(resdata[keys]);
                });
                if (onretrieve_addday_identify == 0) {

                    ;
                }
                else {
                    insert_into_foradd_day();
                }
            },
            error: function (response) {
                if (onretrieve_addday_identify == 0) {
                    startcreatingItinerary();
                }
                else {
                    insert_into_foradd_day();
                }
            }
        });
    });
}

function callItineraryCreator() {
    
    GetAttractionForFirst();
    startcreatingItinerary();
}

function startcreatingItinerary() {
   // debugger
    //;
    //alert('neftstartcreatingItinerary');
    var closest = 0, next_clslat = "", next_dist = 0, next_placeid = "";
    var next_clslng = "", next_place = "";
    var prevUsrRevTot = 0;
    var src_lat = "", src_lng = "";
    var distanceis = "";
    var strtDateIti = $("#TripStrtDate").val();
    var totFromItiStrt = 0;
    var gotPlcToFill = false;
    var gotActicity = true;
    clearTimeout(itineraryTimer);
    var cntcityhotel = 0;
    //var allattr_length = 0;
    //var allrest_length = rest_array.length + otherrestaurant.length;
    var allattr_length = att_actarray.length;
    att_actarray.sort(function (obj1, obj2) {
        return obj2.rating - obj1.rating
    });
    var allrest_length = rest_array.length;
    if (onretrieve_addday_identify == 0) {
        if (identify_city == 0) {
            src_lat = $('#latarray').val();
            src_lng = $('#lngarray').val();
            totFromItiStrt = 0;
        }
        else if (identify_city == 1) {
            src_lat = nextcity_lat;
            src_lng = nextcity_lng;
            totFromItiStrt = user_numofdays + totNumOfItiDays;
            totNumOfItiDays++;
        }
    }
    else {
        src_lat = add_day_citylatitude;
        src_lng = add_day_citylongitude;
    }
    var checkforattact = 0, checkforrest = 0, checkforhotel = 0;

    //if (allattr_length < 4) { ///////////////////Commented by aes93 for Franconia itineraries not display since <3
    //    checkforattact = 1;
    //}

    if (allattr_length < 1) {
        checkforattact = 1;
    }

    else {
        checkforattact = 0;
    }
    if (allrest_length < 2) {
        checkforrest = 1;
    }
    else {
        checkforrest = 0;
    }
    if (hotel_array.length < 1) {
        checkforhotel = 1;
        ishotel_avail = 1;
    }
    else {
        checkforhotel = 0;
        ishotel_avail = 0;
    }
    var islastfirstatt_array = 0;
    var atactarr_length = allattr_length + activitiesCopy.length;
    var restarr_length = rest_array.length;
    var hotelarr_length = hotel_array.length;

    var atact_nods = atactarr_length / 4;
    var res_nods = restarr_length / 2;
    var hotel_nods = hotelarr_length;

    var atact_tofix = atact_nods.toFixed(0);
    var res_tofix = res_nods.toFixed(0);
    var hotel_tofix = hotel_nods.toFixed(0);
    var cntdaysarr = [];
    cntdaysarr.push({
        numofdays: atact_tofix
    });
    cntdaysarr.push({
        numofdays: res_tofix
    });
    if (parseInt(user_numofdays) > parseInt(cntdaysarr[0].numofdays)) {
        minnumdays = cntdaysarr[0].numofdays;
    }
    else {
        if (identify_city == 0) {
            minnumdays = user_numofdays;
        }
        else if (identify_city == 1) {
            minnumdays = 1;
        }
    }
    var cnt = 0, ccnt = 0;
    if (checkforattact == 0) {
        for (var nod_loopvar = 0; nod_loopvar < minnumdays; nod_loopvar++) {
            var getNumdays1 = parseInt(totFromItiStrt) + parseInt(nod_loopvar);
            var getdate1 = new Date(strtDateIti);
            var newdate = new Date(getdate1);
            newdate.setDate(newdate.getDate() + getNumdays1);
            var getday1 = newdate.getDay();
            gotActicity = true;
            for (var plc_loopvar = 0; plc_loopvar < 8; plc_loopvar++) {
                gotPlcToFill = true;
                if (plc_loopvar == 3 || plc_loopvar == 6) {
                    mindist = 99999;
                    var plcStrtTime = "";
                    var plcEndtime = "";
                    if (plc_loopvar == 3) {
                        plcStrtTime = "13";
                        plcEndtime = "15";
                    }
                    else if (plc_loopvar == 6) {
                        plcStrtTime = "18";
                        plcEndtime = "22";
                    }
                    if (rest_array.length > 0) {
                        for (var loopvar = 0, RestaurntsLength = rest_array.length; loopvar < RestaurntsLength; loopvar++) {

                            var dist = Haversine(rest_array[loopvar].geometry.location.lat(), rest_array[loopvar].geometry.location.lng(), src_lat, src_lng);
                            if (dist < mindist) {
                                next_clslat = rest_array[loopvar].geometry.location.lat();
                                next_clslng = rest_array[loopvar].geometry.location.lng();
                                next_place = rest_array[loopvar].name;
                                next_placeid = rest_array[loopvar].place_id;
                                mindist = dist;
                                gotPlcToFill = false;
                                break;
                            }
                            //}
                        }
                        rest_array = rest_array.filter(function (ele) {
                            return ele.place_id !== next_placeid;
                        });
                    }

                    if (!gotPlcToFill) {
                        src_lat = next_clslat;
                        src_lng = next_clslng;
                        sortedarrayall.push({
                            plc_id: next_placeid,
                            plc_name: next_place,
                            plc_lat: next_clslat,
                            plc_lng: next_clslng,
                            plc_dist: next_dist
                        });
                    }
                    else {
                        sortedarrayall.push({
                            plc_id: "Empty",
                            plc_name: "Empty",
                            plc_lat: "Empty",
                            plc_lng: "Empty",
                            plc_dist: "Empty"
                        });
                    }
                }
                else if (plc_loopvar == 0 || plc_loopvar == 7) {
                    if (checkforhotel == 0) {
                        if (cntcityhotel == 0) {
                            mindist = 99999;
                            for (var loopvar = 0, HotelsLength = hotel_array.length; loopvar < HotelsLength ; loopvar++) {
                                var dist = Haversine(hotel_array[loopvar].plc_lat, hotel_array[loopvar].plc_lng, src_lat, src_lng);
                                if (dist < mindist) {
                                    next_clslat = hotel_array[loopvar].plc_lat;
                                    next_clslng = hotel_array[loopvar].plc_lng;
                                    next_place = hotel_array[loopvar].plc_name;
                                    next_placeid = hotel_array[loopvar].plc_id;
                                    next_dist = dist;
                                    mindist = dist;
                                }
                            }
                            src_lat = next_clslat;
                            src_lng = next_clslng;
                            hotelforadday = hotel_array.filter(function (ele) {
                                return ele.plc_id === next_placeid;
                            });
                            cityhotel.push({
                                plc_id: next_placeid,
                                plc_name: next_place,
                                plc_lat: next_clslat,
                                plc_lng: next_clslng,
                                plc_dist: next_dist
                            });
                            inthetrip_hotel.push(next_placeid);

                            cntcityhotel = 1;
                        }
                        else if (cntcityhotel == 1) {
                            next_clslat = cityhotel[0].plc_lat;
                            next_clslng = cityhotel[0].plc_lng;
                            next_place = cityhotel[0].plc_name;
                            next_placeid = cityhotel[0].plc_id;
                            next_dist = dist;
                            mindist = "";
                        }
                        sortedarrayall.push({
                            plc_id: next_placeid,
                            plc_name: next_place,
                            plc_lat: next_clslat,
                            plc_lng: next_clslng,
                            plc_dist: next_dist
                        });
                    }
                    else {
                        sortedarrayall.push({
                            plc_id: "Empty",
                            plc_name: "Empty",
                            plc_lat: "Empty",
                            plc_lng: "Empty",
                            plc_dist: "Empty"
                        });
                    }
                }
                else {
                    prevUsrRevTot = 0;
                    mindist = 99999;
                    //alert('AttractionActivities');
                    //debugger
                    if (att_actarray.length > 0) {
                        for (var loopvar = 0, AttractionActivities = att_actarray.length; loopvar < AttractionActivities; loopvar++) {
                            if (att_actarray[loopvar].rating > 4.5) {
                                var dist = Haversine(att_actarray[loopvar].geometry.location.lat(), att_actarray[loopvar].geometry.location.lng(), src_lat, src_lng);
                                if (dist < mindist) {
                                    next_clslat = att_actarray[loopvar].geometry.location.lat();
                                    next_clslng = att_actarray[loopvar].geometry.location.lng();
                                    next_place = att_actarray[loopvar].name;
                                    next_placeid = att_actarray[loopvar].place_id;
                                    //prevUsrRevTot = att_actarray[loopvar].user_ratings_total;
                                    next_dist = dist;
                                    mindist = dist;
                                    gotPlcToFill = false;
                                    break;
                                }
                            }
                            //        }
                        }
                        //}
                        att_actarray = att_actarray.filter(function (ele) {
                            return ele.place_id !== next_placeid;
                        });
                        next_placeid = "attr||" + next_placeid;
                    }
                    if (activitiesCopy.length > 0 && gotPlcToFill == false && gotActicity) {
                        for (var loopvar = 0, ActivLength = activitiesCopy.length; loopvar < ActivLength; loopvar++) {
                            if (activitiesCopy[loopvar].durationInMillis <= 7200000 && activitiesCopy[loopvar].recommendationScore > 75) {
                                var actlat = activitiesCopy[loopvar].latLng.split(",");
                                var dist = Haversine(actlat[0], actlat[1], src_lat, src_lng);

                                if (dist < mindist) {
                                    next_clslat = actlat[0];
                                    next_clslng = actlat[1];
                                    next_place = activitiesCopy[loopvar].title;
                                    next_placeid = "acti||" + activitiesCopy[loopvar].id;
                                    next_dist = dist;
                                    mindist = dist;
                                    gotPlcToFill = false;
                                    gotActicity = false;
                                }
                                else {
                                    if (cnt < minnumdays) {
                                        next_clslat = actlat[0];
                                        next_clslng = actlat[1];
                                        next_place = activitiesCopy[loopvar].title;
                                        next_placeid = "acti||" + activitiesCopy[loopvar].id;
                                        next_dist = dist;
                                        mindist = dist;
                                        gotPlcToFill = false;
                                        gotActicity = false;
                                        cnt += 1;
                                    }
                                }
                            }
                        }
                        if (next_placeid.toString().indexOf("acti||") != -1) {
                            var spltPlcId = next_placeid.split("||");
                            activitiesCopy = activitiesCopy.filter(function (ele) {
                                return ele.id !== spltPlcId[1];
                            });
                        }
                    }
                    if (activitiesCopy.length > 0 && gotPlcToFill && gotActicity) {
                        for (var loopvar = 0, ActivLength = activitiesCopy.length; loopvar < ActivLength; loopvar++) {
                            if (activitiesCopy[loopvar].durationInMillis <= 7200000 && activitiesCopy[loopvar].recommendationScore > 75) {
                                var actlat = activitiesCopy[loopvar].latLng.split(",");
                                var dist = Haversine(actlat[0], actlat[1], src_lat, src_lng);
                                if (dist < mindist) {
                                    next_clslat = actlat[0];
                                    next_clslng = actlat[1];
                                    next_place = activitiesCopy[loopvar].title;
                                    next_placeid = "acti||" + activitiesCopy[loopvar].id;
                                    next_dist = dist;
                                    mindist = dist;
                                    gotPlcToFill = false;
                                    gotActicity = false;
                                }
                                else {
                                    if (ccnt < minnumdays) {
                                        next_clslat = actlat[0];
                                        next_clslng = actlat[1];
                                        next_place = activitiesCopy[loopvar].title;
                                        next_placeid = "acti||" + activitiesCopy[loopvar].id;
                                        next_dist = dist;
                                        mindist = dist;
                                        gotPlcToFill = false;
                                        gotActicity = false;
                                        ccnt += 1;
                                    }
                                }
                            }
                        }
                        if (next_placeid.toString().indexOf("acti||") != -1) {
                            var spltPlcId = next_placeid.split("||");
                            activitiesCopy = activitiesCopy.filter(function (ele) {
                                return ele.id !== spltPlcId[1];
                            });
                        }
                    }

                    if (att_actarray.length > 0 && gotPlcToFill) {
                        for (var loopvar = 0, AttractionActivities = att_actarray.length; loopvar < AttractionActivities; loopvar++) {
                            if (att_actarray[loopvar].rating > 3.5) {
                                var dist = Haversine(att_actarray[loopvar].geometry.location.lat(), att_actarray[loopvar].geometry.location.lng(), src_lat, src_lng);
                                if (dist < mindist) {
                                    next_clslat = att_actarray[loopvar].geometry.location.lat();
                                    next_clslng = att_actarray[loopvar].geometry.location.lng();
                                    next_place = att_actarray[loopvar].name;
                                    next_placeid = att_actarray[loopvar].place_id;
                                    //prevUsrRevTot = att_actarray[loopvar].user_ratings_total;
                                    next_dist = dist;
                                    mindist = dist;
                                    gotPlcToFill = false;
                                    break;
                                }
                            }
                            //        }
                        }
                        //}
                        att_actarray = att_actarray.filter(function (ele) {
                            return ele.place_id !== next_placeid;
                        });
                        next_placeid = "attr||" + next_placeid;
                    }

                    if (activitiesCopy.length > 0 && gotPlcToFill && gotActicity) {
                        for (var loopvar = 0, ActivLength = activitiesCopy.length; loopvar < ActivLength; loopvar++) {
                            if (activitiesCopy[loopvar].durationInMillis <= 7200000 && activitiesCopy[loopvar].recommendationScore > 50) {
                                var actlat = activitiesCopy[loopvar].latLng.split(",");
                                var dist = Haversine(actlat[0], actlat[1], src_lat, src_lng);
                                if (dist < mindist) {
                                    next_clslat = actlat[0];
                                    next_clslng = actlat[1];
                                    next_place = activitiesCopy[loopvar].title;
                                    next_placeid = "acti||" + activitiesCopy[loopvar].id;
                                    next_dist = dist;
                                    mindist = dist;
                                    gotPlcToFill = false;
                                    gotActicity = false;
                                }
                            }
                        }
                        if (next_placeid.toString().indexOf("acti||") != -1) {
                            var spltPlcId = next_placeid.split("||");
                            activitiesCopy = activitiesCopy.filter(function (ele) {
                                return ele.id !== spltPlcId[1];
                            });
                        }
                    }
                    if (att_actarray.length > 0 && gotPlcToFill) {
                        for (var loopvar = 0, AttractionActivities = att_actarray.length; loopvar < AttractionActivities; loopvar++) {
                            if (att_actarray[loopvar].rating > 2.5) {
                                var dist = Haversine(att_actarray[loopvar].geometry.location.lat(), att_actarray[loopvar].geometry.location.lng(), src_lat, src_lng);
                                if (dist < mindist) {
                                    next_clslat = att_actarray[loopvar].geometry.location.lat();
                                    next_clslng = att_actarray[loopvar].geometry.location.lng();
                                    next_place = att_actarray[loopvar].name;
                                    next_placeid = att_actarray[loopvar].place_id;
                                    //prevUsrRevTot = att_actarray[loopvar].user_ratings_total;
                                    next_dist = dist;
                                    mindist = dist;
                                    gotPlcToFill = false;
                                    break;
                                }
                            }
                            //        }
                        }
                        //}
                        att_actarray = att_actarray.filter(function (ele) {
                            return ele.place_id !== next_placeid;
                        });
                        next_placeid = "attr||" + next_placeid;
                    }
                    if (activitiesCopy.length > 0 && gotPlcToFill && gotActicity) {
                        for (var loopvar = 0, ActivLength = activitiesCopy.length; loopvar < ActivLength; loopvar++) {
                            if (activitiesCopy[loopvar].durationInMillis <= 7200000) {
                                var actlat = activitiesCopy[loopvar].latLng.split(",");
                                var dist = Haversine(actlat[0], actlat[1], src_lat, src_lng);
                                if (dist < mindist) {
                                    next_clslat = actlat[0];
                                    next_clslng = actlat[1];
                                    next_place = activitiesCopy[loopvar].title;
                                    next_placeid = "acti||" + activitiesCopy[loopvar].id;
                                    next_dist = dist;
                                    mindist = dist;
                                    gotPlcToFill = false;
                                    gotActicity = false;
                                }
                            }
                        }
                        if (next_placeid.toString().indexOf("acti||") != -1) {
                            var spltPlcId = next_placeid.split("||");
                            activitiesCopy = activitiesCopy.filter(function (ele) {
                                return ele.id !== spltPlcId[1];
                            });
                        }
                    }
                    if (att_actarray.length > 0 && gotPlcToFill) {
                        for (var loopvar = 0, AttractionActivities = att_actarray.length; loopvar < AttractionActivities; loopvar++) {
                            var dist = Haversine(att_actarray[loopvar].geometry.location.lat(), att_actarray[loopvar].geometry.location.lng(), src_lat, src_lng);
                            if (dist < mindist) {
                                next_clslat = att_actarray[loopvar].geometry.location.lat();
                                next_clslng = att_actarray[loopvar].geometry.location.lng();
                                next_place = att_actarray[loopvar].name;
                                next_placeid = att_actarray[loopvar].place_id;
                                //prevUsrRevTot = att_actarray[loopvar].user_ratings_total;
                                next_dist = dist;
                                mindist = dist;
                                gotPlcToFill = false;
                                break;
                            }
                        }
                        //}
                        att_actarray = att_actarray.filter(function (ele) {
                            return ele.place_id !== next_placeid;
                        });
                        next_placeid = "attr||" + next_placeid;
                    }


                    if (!gotPlcToFill) {
                        src_lat = next_clslat;
                        src_lng = next_clslng;
                        var findattoract = next_placeid.split('||');
                        if (findattoract[0] === "acti") {
                            inthetrip_acti.push(findattoract[1]);
                        }
                        sortedarrayall.push({
                            plc_id: next_placeid,
                            plc_name: next_place,
                            plc_lat: next_clslat,
                            plc_lng: next_clslng,
                            plc_dist: next_dist
                        });
                    }
                    else {
                        sortedarrayall.push({
                            plc_id: "Empty",
                            plc_name: "Empty",
                            plc_lat: "Empty",
                            plc_lng: "Empty",
                            plc_dist: "Empty"
                        });
                    }
                }
            }
        }
        concatenate_array = att_actarray.concat(secondClassAttr);
        concatenate_array = concatenate_array.concat(thirdClassAttr);
        concatenate_array = concatenate_array.concat(otherattraction);

        foraddday_array.push({
            city_name: current_city,
            hotels: hotelforadday.slice(),
            hotelsfull: hotel_arrayfull.slice(),
            attractionsfull: attraction_array.slice(),
            FCattractions: att_actarray.slice(),
            SCattractions: secondClassAttr.slice(),
            TCattractions: thirdClassAttr.slice(),
            otherattractions: otherattraction.slice(),
            attractions: concatenate_array.slice(),
            activitiesfull: activitiesfull_array.slice(),
            restaurantsfull: restlist_array.slice(),
            restaurants: rest_array.slice(),
            otherrestaurants: otherrestaurant.slice(),
            activitiesAddDay: activitiesCopy.slice()
        });

        got_hotelResult = 0;
        showtripfordays();
    }
    else {
        callapiforothercities();
        if (isaddnewcity > 0 && isaddnewcity_decr < isaddnewcity) {
            CreatePackageForAddcity();
        }
        else {
            ready_for_add_day = 1;
        }
    }
}
//#region showtripfordays 
function showtripfordays() {
    // ;
    var src_City = "";
    if (onretrieve_addday_identify == 0) {
        if (identify_city == 0) {
            src_City = $('#selectedcity').html();
        }
        else if (identify_city == 1) {
            src_City = nextcity_name;
        }
    }
    else {
        src_City = cityname_foradd_day;

    }

    var incre_daynum = 1;
    $('.city-row').each(function () {
        var day_number = 0;
        if (incre_daynum >= 10) {
            day_number = incre_daynum;
        }
        else {
            day_number = "0" + incre_daynum;
        }
        $(this).find('.trip-date').find('.btn').text(day_number);
        incre_daynum++;
    });

    var plcbox_ids = [];

    $(".place-box-right").each(function () {
        var thisid = "";
        if ($(this).find(".place-box:first").attr('id') != "" || $(this).find(".place-box:first").attr('id') != null || $(this).find(".place-box:first").attr('id') != "undefined") {
            thisid = $(this).find(".place-box:first").attr('id');
        }
        plcbox_ids.push({
            plc_box_id: $(this).attr('id'),
            plc_name: "",
            plcbox_inside: thisid
        });
    });

    loopvariable = 0;
    $(".city-row").each(function () {
        plcbox_ids[loopvariable].plc_name = $(this).find(".trip-name").text();
        loopvariable++;
    });

    var sortarraylength = sortedarrayall.length;
    var no_of_packages = sortarraylength / 7;
    var int_noofpack = no_of_packages.toFixed(0);

    var setnumdays = 0;

    if (parseInt(user_numofdays) > parseInt(minnumdays)) {
        setnumdays = parseInt(minnumdays);
    }
    else if (identify_city == 1) {
        setnumdays = 1;
    }
    else {
        setnumdays = parseInt(user_numofdays);
    }

    var loopvarvalue = 0;
    var chkActarr = [];
    for (var loopvar1 = 0; loopvar1 < setnumdays; loopvar1++) {
        var appendid = "";
        var cntAct = 0;
        if (identify_city == 0) {
            appendid = plcbox_ids[loopvar1].plcbox_inside;
            appendplc_box = appendid;
        }
        else {
            if (isaddnewcity == 0) {
                for (var outsideloop = 0, PlaceBoxLength = plcbox_ids.length; outsideloop < PlaceBoxLength; outsideloop++) {
                    if ((plcbox_ids[outsideloop].plc_name).trim() === nextcity_name.trim()) {
                        appendid = plcbox_ids[outsideloop].plcbox_inside;
                        appendplc_box = appendid;
                        nextcity_plcbxid = appendid;
                        var cntvar = 0;
                        $("#" + appendid + " .place-box").each(function () {
                            if (cntvar > 9) {
                                if ($(this).attr('id') === null || $(this).attr('id') === "undefined" || $(this).attr('id') === '') {
                                    $(this).remove();
                                }
                            }
                            cntvar++;
                        });
                    }
                }
            }
            else {
                if (increment_for_append == 0) {
                    increment_for_append = isaddnewcity;
                }
                appendid = city_notsaved[isaddnewcity - increment_for_append].plcbxid;
                appendplc_box = appendid;
                nextcity_plcbxid = appendid;
                increment_for_append--;
            }
        }
        for (var loopvar_sort = 0; loopvar_sort < 8; loopvar_sort++) {
            if (identify_city == 0) {
                loopvarvalue = loopvar1 * 8 + loopvar_sort;
            }
            else {
                loopvarvalue = loopvar_sort;
            }
            if (loopvar_sort == 3 || loopvar_sort == 6) {
                var place = restlist_array.filter(function (obj) {
                    return obj.place_id == sortedarrayall[loopvarvalue].plc_id;
                });
                var att_or_res = 1;
                var placediv1 = "";
                if (place.length) {
                    leftsidearray.push(place[0].place_id);
                    placediv1 = "<div class='place' id='place" + place[0].place_id + "'><div class='place-details' id=" + place[0].place_id + " style ='height:100px;'><input type='hidden' value=" + place[0].geometry.location.lat() + " class='placeres_lat'/><input type='hidden' value=" + place[0].geometry.location.lng() + " class='placeres_lng'/>";
                    placediv1 += "<input type='hidden' value=" + place[0].name + " class='placeres_name'/><input type='hidden' value='1' class='frmApi'/><figure class='place-img'>";
                    if (!place[0].photos) {
                        placediv1 += "<img src='../content/images/AppImages/restaurant.png' alt='' /></figure><div class='place-cont'>";
                    }
                    else {//***Modified by Yuvapriya on 04 Oct 2016 to load default image instead of broken image
                        if (place[0].photos[0].getUrl({ 'maxWidth': 50, 'maxHeight': 50 }).indexOf('wUKdXbzXBPw3YpWO9BIF1pj9EgywJZeYQCLIB') != -1) {
                            placediv1 += "<img src='../content/images/AppImages/restaurant.png' alt='' /></figure><div class='place-cont'>";
                        }
                        else {
                            placediv1 += "<img src='" + place[0].photos[0].getUrl({ 'maxWidth': 50, 'maxHeight': 50 }) + "' /></figure><div class='place-cont'>";
                        }   
                    }//***End
                    placediv1 += "<div class='p-left'><h4>" + place[0].name + "</h4><p style='height:26px; overflow-y:hidden;'>" + place[0].vicinity + "</p><input type='hidden' class='hidlat' value='" + place[0].geometry.location.lat() + "'/><input type='hidden' class='hidlng' value='" + place[0].geometry.location.lng() + "'/><span>Restaurant.</span><br><span id='sp_" + place[0].place_id + "' class='sphrs'></span>";
                    placediv1 += "</div><div class='p-right'>";
                    placediv1 += "<ul><li><a href='#'><img src='../Content/images/del-icon.png' alt='' class='deleteplace' /></a></li><li><a href='#infowindow' class='fancybox' onclick='return infoattraction(\"" + place[0].place_id + "\",\"" + place[0].geometry.location.lat() + "\",\"" + place[0].geometry.location.lng() + "\",\"" + att_or_res + "\");' ><img src='../Content/images/info-icon.png' alt='' /></a></li> </ul>";
                    placediv1 += "</div> </div> </div>";
                    placediv1 += "</div>";
                    $("#" + appendid).append(placediv1);
                }
            }
            else if (loopvar_sort == 0 || loopvar_sort == 7) {
                if (ishotel_avail == 0) {
                    var placdiv = "";
                    //LoadImageEnd();
                    var place = hotel_arrayfull.filter(function (obj) {
                        return obj.hotelId == sortedarrayall[loopvarvalue].plc_id;
                    });
                    if (loopvar_sort == 7) {
                        if (place.length) {
                            var placediv1 = "<div class='place' id='place" + place[0].hotelId + "'><div class='place-details' id=" + place[0].hotelId + " style ='height:86px;'><input type='hidden' value='1' class='frmApi'/><figure class='place-img'>";
                           
                            var iurl = '"http://media3.expedia.com"' + place[0].thumbNailUrl + '"';

                            var obj = new Image();
                            obj.src = iurl;

                            if (obj.complete) {
                                placediv1 += "<img src='http://media3.expedia.com" + place[0].thumbNailUrl + "' alt='' /></figure><div class='place-cont'>";
                                //alert('worked');
                            } else {
                                placediv1 += "<img src='../Content/images/HotelImages/af5e-hotel.jpg' alt='' /></figure><div class='place-cont'>";                               
                                //alert('doesnt work');
                            }

                            placediv1 += "<div class='p-left'><h4>" + place[0].name + "</h4><p style='height:26px; overflow-y:hidden;'>" + place[0].address1 + "</p><input type='hidden' class='hidlat' value='" + place[0].latitude + "'/><input type='hidden' class='hidlng' value='" + place[0].longitude + "'/><span>Hotel.</span>";
                            //if (loopvarvalue == 7) {
                            //    placediv1 += "<span id='Plcbook" + place[0].hotelId + "' style='display: block;margin-left: 20px;'><a href='#' class='Plcbook'>Book</a></span>";
                            //}
                            placediv1 += "</div><div class='p-right'>";
                            placediv1 += "<ul><li><a href='#'><img src='../Content/images/del-icon.png' class='deleteplace' alt='' /></a></li><li><a href='#infowindow_hotels_activities' class='fancybox' onclick='return info_for_hotels(\"" + place[0].hotelId + "\",\"" + appendid + "\")'><img src='../Content/images/info-icon.png' alt='' /></a></li> </ul>";
                            placediv1 += "</div></div></div>";
                            placediv1 += "</div>";
                            $("#" + appendid).append(placediv1);
                        }
                    }
                }
            }
            else if (loopvar_sort == 1) {
                // ;
                // alert('loopid :1 ');
                var placeid = sortedarrayall[loopvarvalue].plc_id;
                var findid = placeid.split('||');
                var place = "";

                if (findid[0] == "acti") {
                    place = activitiesfull_array.filter(function (obj) {
                        return obj.id == findid[1];
                    });
                    // alert('place duration: ' + place[0].duration);
                    var dur = place[0].duration;
                    if (dur.indexOf('d') == -1) {
                        // alert('inside d1');
                        if (place.length) {
                            //  alert('d1 length : ' + place.length);
                            var ActivityDur = place[0].durationInMillis;
                            var minutes = ActivityDur / (1000 * 60);
                            minutes = ((minutes / 30) * 100) / 2;
                            var ltlng = place[0].latLng.split(',');

                            var placediv1 = "<div class='place' id='place" + place[0].id + "'><div class='place-details' id=" + place[0].id + " style ='height:" + minutes + "px;'><input type='hidden' value=" + ltlng[0] + " class='placeatt_lat'/><input type='hidden' value=" + ltlng[1] + " class='placeatt_lng'/>";
                            placediv1 += "<input type='hidden' value='1' class='frmApi'/><input type='hidden' value=" + place[0].title + " class='placeatt_name'/><figure class='place-img'>";
                            placediv1 += "<img src=" + place[0].imageUrl + " alt='' /></figure><div class='place-cont'>";
                            placediv1 += "<div class='p-left'><h4>" + place[0].title + "</h4><p style='height:26px; overflow-y:hidden;'>" + place[0].duration + "</p><input type='hidden' class='hidlat' value='" + ltlng[0] + "'/><input type='hidden' class='hidlng' value='" + ltlng[1] + "'/><span>Activity.</span><br><span id='sp_" + place[0].id + "' class='sphrs'></span>";
                            placediv1 += "</div><div class='p-right'>";
                            placediv1 += "<ul><li><a href='#'><img src='../Content/images/del-icon.png' class='deleteplace' alt=''/></a></li><li><a href='#infowindow_hotels_activities' class='fancybox' onclick='getActivityInfo(\"" + place[0].id + "\",\"" + src_City + "\");' ><img src='../Content/images/info-icon.png' alt='' /></a></li> </ul>";
                            placediv1 += "</div> </div> </div>";
                            placediv1 += "</div>";

                            $("#" + appendid).append(placediv1);
                        }

                    }

                }
                else {
                    //modified by yuvapriya on 20 Mar 2017 .. sometimes application remains as 'please wait your iteneariry is being created..'
                    //if that hapens, then page will be reloaded from server 
                    if (typeof (activitiesfull_array[loopvarvalue]) === "undefined") {// .indexOf(id) == -1) {
                        //alert('');
                        location.reload(true);
                    }
                    //end
                    placeid = activitiesfull_array[loopvarvalue].id;
                    place = activitiesfull_array.filter(function (obj) {
                        return obj.id == placeid;
                    });
                    // alert('place[0].duration: ' + place[0].duration)
                    var dura = place[0].duration;
                    if (dura.indexOf('d') == -1) {
                        // alert('inside d');
                        if (place.length) {

                            //  alert('d length : ' + place.length);
                            var ActivityDur = place[0].durationInMillis;
                            var minutes = ActivityDur / (1000 * 60);
                            minutes = ((minutes / 30) * 100) / 2;
                            var ltlng = place[0].latLng.split(',');

                            var placediv1 = "<div class='place' id='place" + place[0].id + "'><div class='place-details' id=" + place[0].id + " style ='height:" + minutes + "px;'><input type='hidden' value=" + ltlng[0] + " class='placeatt_lat'/><input type='hidden' value=" + ltlng[1] + " class='placeatt_lng'/>";
                            placediv1 += "<input type='hidden' value='1' class='frmApi'/><input type='hidden' value=" + place[0].title + " class='placeatt_name'/><figure class='place-img'>";
                            placediv1 += "<img src=" + place[0].imageUrl + " alt='' /></figure><div class='place-cont'>";
                            placediv1 += "<div class='p-left'><h4>" + place[0].title + "</h4><p style='height:26px; overflow-y:hidden;'>" + place[0].duration + "</p><input type='hidden' class='hidlat' value='" + ltlng[0] + "'/><input type='hidden' class='hidlng' value='" + ltlng[1] + "'/><span>Activity.</span><br><span id='sp_" + place[0].id + "' class='sphrs'></span>";
                            placediv1 += "</div><div class='p-right'>";
                            placediv1 += "<ul><li><a href='#'><img src='../Content/images/del-icon.png' class='deleteplace' alt=''/></a></li><li><a href='#infowindow_hotels_activities' class='fancybox' onclick='getActivityInfo(\"" + place[0].id + "\",\"" + src_City + "\");' ><img src='../Content/images/info-icon.png' alt='' /></a></li> </ul>";
                            placediv1 += "</div> </div> </div>";
                            placediv1 += "</div>";

                            $("#" + appendid).append(placediv1);
                        }

                    }
                    else {
                        //  alert('exec loop');
                        var dura = 0;
                        for (var i = 1; i < 20; i++) {

                            placeid = activitiesfull_array[loopvarvalue + i].id;
                            place = activitiesfull_array.filter(function (obj) {
                                return obj.id == placeid;
                            });
                            dura = place[0].duration;
                            if (dura.indexOf('d') == -1 && place.length) {
                                break;
                            }
                        }
                        // alert('dura:brk : ' + dura + ' - ' + 'place.length: ' + place.length);

                        if (place.length) {

                            //  alert('dura:brk  length : ' + place.length);
                            var ActivityDur = place[0].durationInMillis;
                            var minutes = ActivityDur / (1000 * 60);
                            minutes = ((minutes / 30) * 100) / 2;
                            var ltlng = place[0].latLng.split(',');

                            var placediv1 = "<div class='place' id='place" + place[0].id + "'><div class='place-details' id=" + place[0].id + " style ='height:" + minutes + "px;'><input type='hidden' value=" + ltlng[0] + " class='placeatt_lat'/><input type='hidden' value=" + ltlng[1] + " class='placeatt_lng'/>";
                            placediv1 += "<input type='hidden' value='1' class='frmApi'/><input type='hidden' value=" + place[0].title + " class='placeatt_name'/><figure class='place-img'>";
                            placediv1 += "<img src=" + place[0].imageUrl + " alt='' /></figure><div class='place-cont'>";
                            placediv1 += "<div class='p-left'><h4>" + place[0].title + "</h4><p style='height:26px; overflow-y:hidden;'>" + place[0].duration + "</p><input type='hidden' class='hidlat' value='" + ltlng[0] + "'/><input type='hidden' class='hidlng' value='" + ltlng[1] + "'/><span>Activity.</span><br><span id='sp_" + place[0].id + "' class='sphrs'></span>";
                            placediv1 += "</div><div class='p-right'>";
                            placediv1 += "<ul><li><a href='#'><img src='../Content/images/del-icon.png' class='deleteplace' alt=''/></a></li><li><a href='#infowindow_hotels_activities' class='fancybox' onclick='getActivityInfo(\"" + place[0].id + "\",\"" + src_City + "\");' ><img src='../Content/images/info-icon.png' alt='' /></a></li> </ul>";
                            placediv1 += "</div> </div> </div>";
                            placediv1 += "</div>";

                            $("#" + appendid).append(placediv1);
                        }

                    }
                }
            }
            else {
                var placeid = sortedarrayall[loopvarvalue].plc_id;
                var findid = placeid.split('||');
                var place = "";
                if (findid[0] == "attr") {
                    var att_or_res = 2;
                    place = attraction_array.filter(function (obj) {
                        return obj.place_id == findid[1];
                    });
                    var placediv1 = "";
                    
                    if (place.length) {
                        leftsidearray.push(place[0].place_id);
                        placediv1 = "<div class='place' id='place" + place[0].place_id + "'><div class='place-details' id=" + place[0].place_id + " style ='height:200px;'><input type='hidden' value=" + place[0].geometry.location.lat() + " class='placeatt_lat'/><input type='hidden' value=" + place[0].geometry.location.lng() + " class='placeatt_lng'/>";
                        placediv1 += "<input type='hidden' value='1' class='frmApi'/><input type='hidden' value=" + place[0].name + " class='placeatt_name'/><figure class='place-img'>";
                        
                        if (!place[0].photos) {
                            placediv1 += "<img src='../content/images/AppImages/attractions.jpg' alt='' /></figure><div class='place-cont'>";
                        }
                        else {//***Modified by Yuvapriya on 04 Oct 2016 to load default image instead of broken image
                            
                            if (place[0].photos[0].getUrl({ 'maxWidth': 50, 'maxHeight': 50 }).indexOf('wUKdXbzXBPw3YpWO9BIF1pj9EgywJZeYQCLIB') != -1) {
                               
                                placediv1 += "<img src='../content/images/AppImages/attractions.jpg' alt='' /></figure><div class='place-cont'>";
                            }
                            else {
                              
                                placediv1 += "<img src='" + place[0].photos[0].getUrl({ 'maxWidth': 50, 'maxHeight': 50 }) + "'/></figure><div class='place-cont'>";
                            }
                           //***End
                        }
                        placediv1 += "<div class='p-left'><h4>" + place[0].name + "</h4><p style='height:26px; overflow-y:hidden;'>" + place[0].vicinity + "</p><input type='hidden' class='hidlat' value='" + place[0].geometry.location.lat() + "'/><input type='hidden' class='hidlng' value='" + place[0].geometry.location.lng() + "'/><span>Attraction.</span><br><span id='sp_" + place[0].place_id + "' class='sphrs'></span>";
                        placediv1 += "</div><div class='p-right'>";
                        placediv1 += "<ul><li><a href='#'><img src='../Content/images/del-icon.png' class='deleteplace' alt=''/></a></li><li><a href='#infowindow' class='fancybox' onclick='return infoattraction(\"" + place[0].place_id + "\",\"" + place[0].geometry.location.lat() + "\",\"" + place[0].geometry.location.lng() + "\",\"" + att_or_res + "\");' ><img src='../Content/images/info-icon.png' alt='' /></a></li> </ul>";
                        placediv1 += "</div> </div> </div>";
                        placediv1 += "</div>";
                        $("#" + appendid).append(placediv1);
                    }
                }


            }
        }
    }
    //LoadImageEnd();
    onresizingplc();
    callapiforothercities();
    if (isaddnewcity > 0 && isaddnewcity_decr < isaddnewcity) {
        CreatePackageForAddcity();
    }
    else {
        ready_for_add_day = 1;
    }
}
//#endregion
function showtripfordays222() {
    //;
    var src_City = "";
    if (onretrieve_addday_identify == 0) {
        if (identify_city == 0) {
            src_City = $('#selectedcity').html();
        }
        else if (identify_city == 1) {
            src_City = nextcity_name;
        }
    }
    else {
        src_City = cityname_foradd_day;

    }

    var incre_daynum = 1;
    $('.city-row').each(function () {
        var day_number = 0;
        if (incre_daynum >= 10) {
            day_number = incre_daynum;
        }
        else {
            day_number = "0" + incre_daynum;
        }
        $(this).find('.trip-date').find('.btn').text(day_number);
        incre_daynum++;
    });

    var plcbox_ids = [];

    $(".place-box-right").each(function () {
        var thisid = "";
        if ($(this).find(".place-box:first").attr('id') != "" || $(this).find(".place-box:first").attr('id') != null || $(this).find(".place-box:first").attr('id') != "undefined") {
            thisid = $(this).find(".place-box:first").attr('id');
        }
        plcbox_ids.push({
            plc_box_id: $(this).attr('id'),
            plc_name: "",
            plcbox_inside: thisid
        });
    });

    loopvariable = 0;
    $(".city-row").each(function () {
        plcbox_ids[loopvariable].plc_name = $(this).find(".trip-name").text();
        loopvariable++;
    });

    var sortarraylength = sortedarrayall.length;
    var no_of_packages = sortarraylength / 7;
    var int_noofpack = no_of_packages.toFixed(0);

    var setnumdays = 0;

    if (parseInt(user_numofdays) > parseInt(minnumdays)) {
        setnumdays = parseInt(minnumdays);
    }
    else if (identify_city == 1) {
        setnumdays = 1;
    }
    else {
        setnumdays = parseInt(user_numofdays);
    }

    var loopvarvalue = 0;
    var chkActarr = [];
    for (var loopvar1 = 0; loopvar1 < setnumdays; loopvar1++) {
        var appendid = "";
        var cntAct = 0;
        if (identify_city == 0) {
            appendid = plcbox_ids[loopvar1].plcbox_inside;
            appendplc_box = appendid;
        }
        else {
            if (isaddnewcity == 0) {
                for (var outsideloop = 0, PlaceBoxLength = plcbox_ids.length; outsideloop < PlaceBoxLength; outsideloop++) {
                    if ((plcbox_ids[outsideloop].plc_name).trim() === nextcity_name.trim()) {
                        appendid = plcbox_ids[outsideloop].plcbox_inside;
                        appendplc_box = appendid;
                        nextcity_plcbxid = appendid;
                        var cntvar = 0;
                        $("#" + appendid + " .place-box").each(function () {
                            if (cntvar > 9) {
                                if ($(this).attr('id') === null || $(this).attr('id') === "undefined" || $(this).attr('id') === '') {
                                    $(this).remove();
                                }
                            }
                            cntvar++;
                        });
                    }
                }
            }
            else {
                if (increment_for_append == 0) {
                    increment_for_append = isaddnewcity;
                }
                appendid = city_notsaved[isaddnewcity - increment_for_append].plcbxid;
                appendplc_box = appendid;
                nextcity_plcbxid = appendid;
                increment_for_append--;
            }
        }
        for (var loopvar_sort = 0; loopvar_sort < 8; loopvar_sort++) {
            if (identify_city == 0) {
                loopvarvalue = loopvar1 * 8 + loopvar_sort;
            }
            else {
                loopvarvalue = loopvar_sort;
            }
            if (loopvar_sort == 3 || loopvar_sort == 6) {
                var place = restlist_array.filter(function (obj) {
                    return obj.place_id == sortedarrayall[loopvarvalue].plc_id;
                });
                var att_or_res = 1;
                var placediv1 = "";
                if (place.length) {
                   
                    leftsidearray.push(place[0].place_id);
                    placediv1 = "<div class='place' id='place" + place[0].place_id + "'><div class='place-details' id=" + place[0].place_id + " style ='height:100px;'><input type='hidden' value=" + place[0].geometry.location.lat() + " class='placeres_lat'/><input type='hidden' value=" + place[0].geometry.location.lng() + " class='placeres_lng'/>";
                    placediv1 += "<input type='hidden' value=" + place[0].name + " class='placeres_name'/><input type='hidden' value='1' class='frmApi'/><figure class='place-img'>";
                    if (!place[0].photos) {
                        placediv1 += "<img src='../content/images/AppImages/restaurant.png' alt='' /></figure><div class='place-cont'>";
                    }
                    else {
                        placediv1 += "<img src='" + place[0].photos[0].getUrl({ 'maxWidth': 50, 'maxHeight': 50 }) + "' /></figure><div class='place-cont'>";
                    }
                    placediv1 += "<div class='p-left'><h4>" + place[0].name + "</h4><p style='height:26px; overflow-y:hidden;'>" + place[0].vicinity + "</p><input type='hidden' class='hidlat' value='" + place[0].geometry.location.lat() + "'/><input type='hidden' class='hidlng' value='" + place[0].geometry.location.lng() + "'/><span>Restaurant.</span><br><span id='sp_" + place[0].place_id + "' class='sphrs'></span>";
                    placediv1 += "</div><div class='p-right'>";
                    placediv1 += "<ul><li><a href='#'><img src='../Content/images/del-icon.png' alt='' class='deleteplace' /></a></li><li><a href='#infowindow' class='fancybox' onclick='return infoattraction(\"" + place[0].place_id + "\",\"" + place[0].geometry.location.lat() + "\",\"" + place[0].geometry.location.lng() + "\",\"" + att_or_res + "\");' ><img src='../Content/images/info-icon.png' alt='' /></a></li> </ul>";
                    placediv1 += "</div> </div> </div>";
                    placediv1 += "</div>";
                    $("#" + appendid).append(placediv1);
                }
            }
            else if (loopvar_sort == 0 || loopvar_sort == 7) {
                if (ishotel_avail == 0) {
                    var placdiv = "";
                    //LoadImageEnd();
                    var place = hotel_arrayfull.filter(function (obj) {
                        return obj.hotelId == sortedarrayall[loopvarvalue].plc_id;
                    });
                    if (loopvar_sort == 7) {
                        if (place.length) {
                           
                            var placediv1 = "<div class='place' id='place" + place[0].hotelId + "'><div class='place-details' id=" + place[0].hotelId + " style ='height:86px;'><input type='hidden' value='1' class='frmApi'/><figure class='place-img'>";
                            placediv1 += "<img src='http://media3.expedia.com" + place[0].thumbNailUrl + "' alt='' /></figure><div class='place-cont'>";
                            placediv1 += "<div class='p-left'><h4>" + place[0].name + "</h4><p style='height:26px; overflow-y:hidden;'>" + place[0].address1 + "</p><input type='hidden' class='hidlat' value='" + place[0].latitude + "'/><input type='hidden' class='hidlng' value='" + place[0].longitude + "'/><span>Hotel.</span>";
                            //***Modified by Yuvapriya on 20Sep16 To remove Hotel Book Btn remove -- To add Hotel book uncomment below condition ***
                            //if (loopvarvalue == 7) {
                            //    placediv1 += "<span id='Plcbook" + place[0].hotelId + "' style='display: block;margin-left: 20px;'><a href='#' class='Plcbook'>Book</a></span>";
                            //}
                            //***end ***
                            placediv1 += "</div><div class='p-right'>";
                            placediv1 += "<ul><li><a href='#'><img src='../Content/images/del-icon.png' class='deleteplace' alt='' /></a></li><li><a href='#infowindow_hotels_activities' class='fancybox' onclick='return info_for_hotels(\"" + place[0].hotelId + "\",\"" + appendid + "\")'><img src='../Content/images/info-icon.png' alt='' /></a></li> </ul>";
                            placediv1 += "</div></div></div>";
                            placediv1 += "</div>";
                            $("#" + appendid).append(placediv1);
                        }
                    }
                }
            }
            else if (loopvar_sort == 1) {
                
                var placeid = sortedarrayall[loopvarvalue].plc_id;
                var findid = placeid.split('||');
                var place = "";
                var chkActLen = 0;
                chkActLen = chkActarr.length;
                var dup = 0;

                if (findid[0] == "acti") {
                    if (chkActLen > 0) {
                        for (var k = 0; k < chkActLen; k++) {
                            if (findid[1] == chkActarr[k]) {
                                dup += 1;
                            }
                        }
                    }
                    if (dup == 0) {
                        chkActarr.push(findid[1]);
                        place = activitiesfull_array.filter(function (obj) {
                            return obj.id == findid[1];
                        });
                        
                        var dur = place[0].duration;
                        if (dur.indexOf('d') == -1) {
                            // alert('inside d1');
                            if (place.length) {
                                //  alert('d1 length : ' + place.length);
                                var ActivityDur = place[0].durationInMillis;
                                var minutes = ActivityDur / (1000 * 60);
                                minutes = ((minutes / 30) * 100) / 2;
                                var ltlng = place[0].latLng.split(',');
                                
                                var placediv1 = "<div class='place' id='place" + place[0].id + "'><div class='place-details' id=" + place[0].id + " style ='height:" + minutes + "px;'><input type='hidden' value=" + ltlng[0] + " class='placeatt_lat'/><input type='hidden' value=" + ltlng[1] + " class='placeatt_lng'/>";
                                placediv1 += "<input type='hidden' value='1' class='frmApi'/><input type='hidden' value=" + place[0].title + " class='placeatt_name'/><figure class='place-img'>";
                                placediv1 += "<img src=" + place[0].imageUrl + " alt='' /></figure><div class='place-cont'>";
                                placediv1 += "<div class='p-left'><h4>" + place[0].title + "</h4><p style='height:26px; overflow-y:hidden;'>" + place[0].duration + "</p><input type='hidden' class='hidlat' value='" + ltlng[0] + "'/><input type='hidden' class='hidlng' value='" + ltlng[1] + "'/><span>Activity.</span><br><span id='sp_" + place[0].id + "' class='sphrs'></span>";
                                placediv1 += "</div><div class='p-right'>";
                                placediv1 += "<ul><li><a href='#'><img src='../Content/images/del-icon.png' class='deleteplace' alt=''/></a></li><li><a href='#infowindow_hotels_activities' class='fancybox' onclick='getActivityInfo(\"" + place[0].id + "\",\"" + src_City + "\");' ><img src='../Content/images/info-icon.png' alt='' /></a></li> </ul>";
                                placediv1 += "</div> </div> </div>";
                                placediv1 += "</div>";

                                //var placediv1 = "<div class='place' id='place" + place[0].id + "'><div class='place-details' id=" + place[0].id + " style ='height:200px;'><input type='hidden' value='1' class='frmApi'/><figure class='place-img'>";
                                //placediv1 += "<img src='" + place[0].imageUrl + "'/></figure><div class='place-cont'>";
                                //placediv1 += "<div class='p-left'><h4>" + place[0].title + "</h4><p style='height:26px; overflow-y:hidden;'>" + place[0].locations_text + "</p><input type='hidden' class='hidlat' value='" + place[0].start_Latitude + "'/><input type='hidden' class='hidlng' value='" + place[0].start_Longitude + "'/><span>Activity.</span><br><span id='sp_" + place[0].id + "' class='sphrs'></span>";
                                //placediv1 += "</div><div class='p-right'>";
                                //placediv1 += "<ul><li><a href='#'><img src='../Content/images/del-icon.png' class='deleteplace' alt=''/></a></li><li><a href='#infowindow_hotels_activities' class='fancybox' onclick='return info_for_hotels(\"" + place[0].id + "\",\"" + appendid + "\")'><img src='../Content/images/info-icon.png' alt='' /></a></li> </ul>";
                                //placediv1 += "</div> </div> </div>";
                                //placediv1 += "</div>";
                                $("#" + appendid).append(placediv1);
                            }

                        }
                    }
                    else {
                        chkActarr = AppendActivityForDay(chkActarr);
                    }
                }
                else {
                    placeid = activitiesfull_array[loopvarvalue].id;
                    place = activitiesfull_array.filter(function (obj) {
                        return obj.id == placeid;
                    });
                    // alert('place[0].duration: ' + place[0].duration)
                    var dura = place[0].duration;
                    var dup1 = 0;
                    if (dura.indexOf('d') == -1) {
                        if (chkActLen > 0) {
                            for (var k = 0; k < chkActLen; k++) {
                                if (placeid == chkActarr[k]) {
                                    dup1 += 1;
                                }
                            }
                        }
                        if (dup1 == 0) {
                            //   alert('inside d');
                            if (place.length) {
                                chkActarr.push(placeid);
                                //  alert('d length : ' + place.length);
                                var ActivityDur = place[0].durationInMillis;
                                var minutes = ActivityDur / (1000 * 60);
                                minutes = ((minutes / 30) * 100) / 2;
                                var ltlng = place[0].latLng.split(',');

                                var placediv1 = "<div class='place' id='place" + place[0].id + "'><div class='place-details' id=" + place[0].id + " style ='height:" + minutes + "px;'><input type='hidden' value=" + ltlng[0] + " class='placeatt_lat'/><input type='hidden' value=" + ltlng[1] + " class='placeatt_lng'/>";
                                placediv1 += "<input type='hidden' value='1' class='frmApi'/><input type='hidden' value=" + place[0].title + " class='placeatt_name'/><figure class='place-img'>";
                                placediv1 += "<img src=" + place[0].imageUrl + " alt='' /></figure><div class='place-cont'>";
                                placediv1 += "<div class='p-left'><h4>" + place[0].title + "</h4><p style='height:26px; overflow-y:hidden;'>" + place[0].duration + "</p><input type='hidden' class='hidlat' value='" + ltlng[0] + "'/><input type='hidden' class='hidlng' value='" + ltlng[1] + "'/><span>Activity.</span><br><span id='sp_" + place[0].id + "' class='sphrs'></span>";
                                placediv1 += "</div><div class='p-right'>";
                                placediv1 += "<ul><li><a href='#'><img src='../Content/images/del-icon.png' class='deleteplace' alt=''/></a></li><li><a href='#infowindow_hotels_activities' class='fancybox' onclick='getActivityInfo(\"" + place[0].id + "\",\"" + src_City + "\");' ><img src='../Content/images/info-icon.png' alt='' /></a></li> </ul>";
                                placediv1 += "</div> </div> </div>";
                                placediv1 += "</div>";

                                $("#" + appendid).append(placediv1);
                            }
                        }
                        else {
                            chkActarr = AppendActivityForDay(chkActarr);
                        }
                    }
                    else {
                        //  alert('exec loop');
                        var dura = 0;
                        for (var i = 1; i < 20; i++) {

                            placeid = activitiesfull_array[loopvarvalue + i].id;
                            place = activitiesfull_array.filter(function (obj) {
                                return obj.id == placeid;
                            });
                            dura = place[0].duration;
                            if (dura.indexOf('d') == -1 && place.length) {
                                break;
                            }
                        }
                        //alert('dura:brk : ' + dura + ' - ' + 'place.length: ' + place.length);
                        var dup2 = 0;
                        if (chkActLen > 0) {
                            for (var k = 0; k < chkActLen; k++) {
                                if (placeid == chkActarr[k]) {
                                    dup2 += 1;
                                }
                            }
                        }
                        if (dup2 == 0) {
                            if (place.length) {
                                chkActarr.push(placeid);
                                //  alert('dura:brk  length : ' + place.length);
                                var ActivityDur = place[0].durationInMillis;
                                var minutes = ActivityDur / (1000 * 60);
                                minutes = ((minutes / 30) * 100) / 2;
                                var ltlng = place[0].latLng.split(',');
                               
                                var placediv1 = "<div class='place' id='place" + place[0].id + "'><div class='place-details' id=" + place[0].id + " style ='height:" + minutes + "px;'><input type='hidden' value=" + ltlng[0] + " class='placeatt_lat'/><input type='hidden' value=" + ltlng[1] + " class='placeatt_lng'/>";
                                placediv1 += "<input type='hidden' value='1' class='frmApi'/><input type='hidden' value=" + place[0].title + " class='placeatt_name'/><figure class='place-img'>";
                                placediv1 += "<img src=" + place[0].imageUrl + " alt='' /></figure><div class='place-cont'>";
                                placediv1 += "<div class='p-left'><h4>" + place[0].title + "</h4><p style='height:26px; overflow-y:hidden;'>" + place[0].duration + "</p><input type='hidden' class='hidlat' value='" + ltlng[0] + "'/><input type='hidden' class='hidlng' value='" + ltlng[1] + "'/><span>Activity.</span><br><span id='sp_" + place[0].id + "' class='sphrs'></span>";
                                placediv1 += "</div><div class='p-right'>";
                                placediv1 += "<ul><li><a href='#'><img src='../Content/images/del-icon.png' class='deleteplace' alt=''/></a></li><li><a href='#infowindow_hotels_activities' class='fancybox' onclick='getActivityInfo(\"" + place[0].id + "\",\"" + src_City + "\");' ><img src='../Content/images/info-icon.png' alt='' /></a></li> </ul>";
                                placediv1 += "</div> </div> </div>";
                                placediv1 += "</div>";

                                $("#" + appendid).append(placediv1);
                            }
                        }
                        else {
                            chkActarr = AppendActivityForDay(chkActarr);
                        }
                    }
                }
            }
            else {
                var placeid = sortedarrayall[loopvarvalue].plc_id;
                var findid = placeid.split('||');
                var place = "";
                if (findid[0] == "attr") {
                    var att_or_res = 2;
                    place = attraction_array.filter(function (obj) {
                        return obj.place_id == findid[1];
                    });
                    var placediv1 = "";
                    if (place.length) {
                   
                        leftsidearray.push(place[0].place_id);
                        placediv1 = "<div class='place' id='place" + place[0].place_id + "'><div class='place-details' id=" + place[0].place_id + " style ='height:200px;'><input type='hidden' value=" + place[0].geometry.location.lat() + " class='placeatt_lat'/><input type='hidden' value=" + place[0].geometry.location.lng() + " class='placeatt_lng'/>";
                        placediv1 += "<input type='hidden' value='1' class='frmApi'/><input type='hidden' value=" + place[0].name + " class='placeatt_name'/><figure class='place-img'>";
                        if (!place[0].photos) {
                            placediv1 += "<img src='../content/images/AppImages/attractions.jpg' alt='' /></figure><div class='place-cont'>";
                        }
                        else {
                            placediv1 += "<img src='" + place[0].photos[0].getUrl({ 'maxWidth': 50, 'maxHeight': 50 }) + "'/></figure><div class='place-cont'>";
                        }
                        placediv1 += "<div class='p-left'><h4>" + place[0].name + "</h4><p style='height:26px; overflow-y:hidden;'>" + place[0].vicinity + "</p><input type='hidden' class='hidlat' value='" + place[0].geometry.location.lat() + "'/><input type='hidden' class='hidlng' value='" + place[0].geometry.location.lng() + "'/><span>Attraction.</span><br><span id='sp_" + place[0].place_id + "' class='sphrs'></span>";
                        placediv1 += "</div><div class='p-right'>";
                        placediv1 += "<ul><li><a href='#'><img src='../Content/images/del-icon.png' class='deleteplace' alt=''/></a></li><li><a href='#infowindow' class='fancybox' onclick='return infoattraction(\"" + place[0].place_id + "\",\"" + place[0].geometry.location.lat() + "\",\"" + place[0].geometry.location.lng() + "\",\"" + att_or_res + "\");' ><img src='../Content/images/info-icon.png' alt='' /></a></li> </ul>";
                        placediv1 += "</div> </div> </div>";
                        placediv1 += "</div>";
                        $("#" + appendid).append(placediv1);
                    }
                }


            }
        }
    }
    //LoadImageEnd();
    onresizingplc();
    callapiforothercities();
    if (isaddnewcity > 0 && isaddnewcity_decr < isaddnewcity) {
        CreatePackageForAddcity();
    }
    else {
        ready_for_add_day = 1;
    }
}

function AppendActivityForDay(chkActarr) {
    var arr = [];
    arr = chkActarr;
    var dura = 0;
    var dup = 0;
    var ii = 1;
    for (var i = 1; i < 20; i++) {
        ii = i;
        placeid = activitiesfull_array[loopvarvalue + i].id;
        place = activitiesfull_array.filter(function (obj) {
            return obj.id == placeid;
        });
        dura = place[0].duration;
        if (arr.length > 0) {
            for (var k = 0; k < arr.length; k++) {
                if (placeid == arr[k]) {
                    dup += 1;
                }
            }
        }
        if (dup == 0) {
            if (dura.indexOf('d') == -1 && place.length) {
                break;
            }
        }
    }

    if (dup == 0) {
        if (place.length) {
            chkActarr.push(placeid);

            var ActivityDur = place[0].durationInMillis;
            var minutes = ActivityDur / (1000 * 60);
            minutes = ((minutes / 30) * 100) / 2;
            var ltlng = place[0].latLng.split(',');
            
            var placediv1 = "<div class='place' id='place" + place[0].id + "'><div class='place-details' id=" + place[0].id + " style ='height:" + minutes + "px;'><input type='hidden' value=" + ltlng[0] + " class='placeatt_lat'/><input type='hidden' value=" + ltlng[1] + " class='placeatt_lng'/>";
            placediv1 += "<input type='hidden' value='1' class='frmApi'/><input type='hidden' value=" + place[0].title + " class='placeatt_name'/><figure class='place-img'>";
            placediv1 += "<img src=" + place[0].imageUrl + " alt='' /></figure><div class='place-cont'>";
            placediv1 += "<div class='p-left'><h4>" + place[0].title + "</h4><p style='height:26px; overflow-y:hidden;'>" + place[0].duration + "</p><input type='hidden' class='hidlat' value='" + ltlng[0] + "'/><input type='hidden' class='hidlng' value='" + ltlng[1] + "'/><span>Activity.</span><br><span id='sp_" + place[0].id + "' class='sphrs'></span>";
            placediv1 += "</div><div class='p-right'>";
            placediv1 += "<ul><li><a href='#'><img src='../Content/images/del-icon.png' class='deleteplace' alt=''/></a></li><li><a href='#infowindow_hotels_activities' class='fancybox' onclick='getActivityInfo(\"" + place[0].id + "\",\"" + src_City + "\");' ><img src='../Content/images/info-icon.png' alt='' /></a></li> </ul>";
            placediv1 += "</div> </div> </div>";
            placediv1 += "</div>";

            $("#" + appendid).append(placediv1);
        }
    }
    return chkActarr;
}


function callapiforothercities() {
    if (incre_for_cities >= 1) {
        var citynameForAct = citieslist_array[incre_for_oc].city_name;
        $.getJSON('../Trip/EanHotelRequest', { lat: citieslist_array[incre_for_oc].city_latitude, lng: citieslist_array[incre_for_oc].city_longitude }, function (response) {
            var citynameact = citieslist_array[incre_for_oc].city_name;

            var CityForHotelsReq = citynameact;
            var chkForHoteldetails = 0;
            for (var loopvarHotels = 0, CityHotelsLength = hotelsForCities.length; loopvarHotels < CityHotelsLength; loopvarHotels++) {
                if (hotelsForCities[loopvarHotels].cityName.trim() == CityForHotelsReq.trim()) {
                    chkForHoteldetails = 1;
                }
            }
            if (chkForHoteldetails == 0) {
                hotelsForCities.push({
                    cityName: CityForHotelsReq,
                    Hotels: response
                });
            }

            current_city = citynameact;
            var sm = citynameact.split(',');
            attraction_array.length = 0;
            otherattraction.length = 0;
            hotel_arrayfull.length = 0;
            sortedarrayall.length = 0;
            rest_array.length = 0;
            otherrestaurant.length
            restlist_array.length = 0;
            rest_arraynew.length = 0;
            att_actarray.length = 0;
            hotel_array.length = 0;
            cityhotel.length = 0;
            var murl = "&q=product_list&where=" + sm[0];
            identify_city = 1;
            nextcity_lat = citieslist_array[incre_for_oc].city_latitude;
            nextcity_lng = citieslist_array[incre_for_oc].city_longitude;
            nextcity_name = citieslist_array[incre_for_oc].city_name;
            CreatePackage();
            incre_for_cities--;
            incre_for_oc++;
        });
        var CityForActsReq = citynameForAct;
        $.ajax({
            type: 'GET',
            url: '../Trip/LocalExpertAPI',
            dataType: 'json',
            data: { 'cityname': CityForActsReq },
            success: function (response) {
                var CityForHotelsReq = citynameForAct;
                var chkForHoteldetails = 0;
                for (var loopvarHotels = 0, CitiesHotelsLength = ActsForCities.length; loopvarHotels < CitiesHotelsLength; loopvarHotels++) {
                    if (ActsForCities[loopvarHotels].cityName.trim() == CityForHotelsReq.trim()) {
                        chkForHoteldetails = 1;
                    }
                }
                if (chkForHoteldetails == 0) {
                    ActsForCities.push({
                        cityName: CityForActsReq,
                        Acts: response
                    });
                }
                activitiesfull_array = response.activities.slice(0);
                activitiesCopy = response.activities.slice(0);

                if (user_act != "Nooption") {
                    var tmpactivities_forsort = [];
                    var spAct = user_act.split(',');

                    for (var actIndx = 0, actLeng = activitiesCopy.length; actIndx < actLeng; actIndx++) {
                        for (var filtIndx = 0, filtLeng = spAct.length; filtIndx < filtLeng; filtIndx++) {
                            var matchAct = false;
                            var categories = activitiesCopy[actIndx].categories;
                            for (var cateIndx = 0, cateLen = categories.length; cateIndx < cateLen; cateIndx++) {
                                if (categories[cateIndx] == spAct[filtIndx]) {
                                    matchAct = true;
                                }
                            }
                            if (matchAct) {
                                tmpactivities_forsort.push(activitiesCopy[actIndx]);
                            }
                        }
                    }
                    if (spAct.length > 0) {
                        activitiesCopy = tmpactivities_forsort.slice(0);
                    }
                }
            },
            error: function (response) {
                var errResponse = response;
            }
        });
    }
    else {
        $('#wholepart').unblock();
        initialisetravel();
        localStorage.setItem('IsSavedinDB5', 'YES');
        setTimeout(function () { Getplaces_details_forsave(); }, 9000);
    }
}

function CreatePackageForAddcity() {
    if (isaddnewcity == 0) {
        var Allcitiesarray = [];
        city_notsaved.length = 0;
        $("#citypref li").each(function () {
            var getcityname = $(this).find("a").text();
            var citylat_lng = $(this).find('a').data('value').split('||');
            var citylatit = citylat_lng[0];
            var citylongi = citylat_lng[1];
            Allcitiesarray.push({
                cityname: getcityname,
                citylat: citylatit,
                citylng: citylongi
            });
        });
        var checkprevcity = "";
        $('.place-box-right').each(function () {
            var nameofcity = $(this).data('value');
            if (!($(this).find('.place').length) && nameofcity != checkprevcity) {
                checkprevcity = nameofcity;
                var idofcity = "";
                if ($(this).find('.place-box').attr('id') != null || $(this).find('.place-box').attr('id') != "" || $(this).find('.place-box').attr('id') != undefined) {
                    idofcity = $(this).find('.place-box').attr('id');
                }
                for (var loopv = 0, AllCitiesLength = Allcitiesarray.length; loopv < AllCitiesLength; loopv++) {
                    if (nameofcity.trim() == Allcitiesarray[loopv].cityname.trim()) {
                        city_notsaved.push({
                            cityname: nameofcity,
                            plcbxid: idofcity,
                            citylat: Allcitiesarray[loopv].citylat,
                            citylng: Allcitiesarray[loopv].citylng
                        });
                        isaddnewcity++;
                    }
                }
            }
            else
            {
                checkprevcity = nameofcity;
            }
        });
    }

    if (isaddnewcity_decr < isaddnewcity) {
        var citynameact = city_notsaved[isaddnewcity_decr].cityname;
        current_city = citynameact;
        var sm = citynameact.split(',');
        sortedarrayall.length = 0;
        rest_array.length = 0;
        otherrestaurant.length = 0;
        restlist_array.length = 0;
        rest_arraynew.length = 0;
        att_actarray.length = 0;
        hotel_array.length = 0;
        cityhotel.length = 0;
        var murl = "&q=product_list&where=" + sm[0];
        identify_city = 1;
        nextcity_lat = city_notsaved[isaddnewcity_decr].citylat;
        nextcity_lng = city_notsaved[isaddnewcity_decr].citylng;
        nextcity_name = city_notsaved[isaddnewcity_decr].cityname;
        $('#RightSidePart').block({
            message: 'Please wait while data is loading...',
            css: {
                border: 'none',
                padding: '15px',
                float: 'right',
                'margin-right': '0%',
                backgroundColor: '#033363',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });

        $.getJSON('../Trip/EanHotelRequest', { lat: city_notsaved[isaddnewcity_decr].citylat, lng: city_notsaved[isaddnewcity_decr].citylng }, function (response) {
            var CityForHotelsReq = nextcity_name;
            var chkForHoteldetails = 0;
            for (var loopvarHotels = 0, CitiesHotelsLength = hotelsForCities.length; loopvarHotels < CitiesHotelsLength; loopvarHotels++) {
                if (hotelsForCities[loopvarHotels].cityName.trim() == CityForHotelsReq.trim()) {
                    chkForHoteldetails = 1;
                }
            }
            if (chkForHoteldetails == 0) {
                hotelsForCities.push({
                    cityName: CityForHotelsReq,
                    Hotels: response
                });
            }
            isaddnewcity_decr++;
            CreatePackage();
        });
        var CityForActsReq = nextcity_name;
        $.ajax({
            type: 'GET',
            url: '../Trip/LocalExpertAPI',
            dataType: 'json',
            data: { 'cityname': CityForActsReq },
            success: function (response) {
                var CityForHotelsReq = nextcity_name;
                var chkForHoteldetails = 0;
                for (var loopvarHotels = 0, CitiesHotelsLength = ActsForCities.length; loopvarHotels < CitiesHotelsLength; loopvarHotels++) {
                    if (ActsForCities[loopvarHotels].cityName.trim() == CityForHotelsReq.trim()) {
                        chkForHoteldetails = 1;
                    }
                }
                if (chkForHoteldetails == 0) {
                    ActsForCities.push({
                        cityName: CityForActsReq,
                        Acts: response
                    });
                }
                activitiesfull_array = response.activities.slice(0);
                activitiesCopy = response.activities.slice(0);

                if (user_act != "Nooption") {
                    var tmpactivities_forsort = [];
                    var spAct = user_act.split(',');

                    for (var actIndx = 0, actLeng = activitiesCopy.length; actIndx < actLeng; actIndx++) {
                        for (var filtIndx = 0, filtLeng = spAct.length; filtIndx < filtLeng; filtIndx++) {
                            var matchAct = false;
                            var categories = activitiesCopy[actIndx].categories;
                            for (var cateIndx = 0, cateLen = categories.length; cateIndx < cateLen; cateIndx++) {
                                if (categories[cateIndx] == spAct[filtIndx]) {
                                    matchAct = true;
                                }
                            }
                            if (matchAct) {
                                tmpactivities_forsort.push(activitiesCopy[actIndx]);
                            }
                        }
                    }
                    if (spAct.length > 0) {
                        activitiesCopy = tmpactivities_forsort.slice(0);
                    }
                }

            },
            error: function (response) {
                var errResponse = response;
            }
        });
    }
    else {
        $('#RightSidePart').block({
            message: 'Please wait while data is loading...',
            css: {
                border: 'none',
                padding: '15px',
                float: 'right',
                'margin-right': '0%',
                backgroundColor: '#033363',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });
        setTimeout(function () { GetApiForRetrieved_cities(); }, 2000);
    }
}

function initialisetravel() {
    $('#wholepart').unblock();
    $('#LeftSidePart').unblock();
    var travelModediv = "<div class='travel findnewtravel'><a href='#' style='float:right;'><img src='../Content/images/del_travel.png' class='travedelimg'/></a><p>Time taken by car is -- <img src='../Content/images/edit.png' alt='' class='editimg' style='height: 15px;width: 15px;'></p>";
    travelModediv += "<ul><li><input type='hidden' class='hidcar' value=''/><a href='#' data-value='car' class='active'><img src='../Content/images/car-icon.png' alt=''/></a></li>";
    travelModediv += "<li><input type='hidden' class='hidtrain' value=''/><a href='#' data-value='train'><img src='../Content/images/train-icon.png' alt='' /></a></li>";
    travelModediv += "<li><input type='hidden' class='hidplane' value=''/><a href='#' data-value='plane'><img src='../Content/images/plane-icon.png' alt='' /></a></li>";
    travelModediv += "<li><input type='hidden' class='hidwalk' value=''/><a href='#' data-value='walk'><img src='../Content/images/walk-icon.png' alt='' /></a></li>";
    travelModediv += "<li class='last'><input type='hidden' class='hidcycle' value=''/><a href='#' data-value='bicycle'><img src='../Content/images/scooter-icon.png' alt='' /></a></li>";
    travelModediv += "<input type='hidden' class='hiddriving' value=''/></ul></div>";
    $('.place + .place').before($(travelModediv));

    setid_for_alltravelmodes();
    calculatetravelduration();
    inthetripfn();
    //var setnumday = 0;

    //if (parseInt(user_numofdays) > parseInt(minnumdays)) {
    //    setnumday = parseInt(minnumdays);
    //}
    //else if (identify_city == 1) {
    //    setnumday = 1;
    //}
    //else {
    //    setnumday = parseInt(user_numofdays);
    //}
  
    //if (setnumday == 1) {
    //    $('.hotel-details').css("margin-left", "2px").css("width", "93.6%");
    //    $('.trip-right').css("margin-left", "-52px").css("width", "51%");
    //    $('.place-box-right').css("width", "550px");
    //    $('.city-row').css("width", "172%");
    //    $('.place-details').css("width", "115%").css("margin-left", "13%");
    //    $('.travel').css("width", "124%").css("margin-left", "13%").css("text-align", "center");
    //    $('.travel ul').css("text-align", "center").css("margin-left", "11%");
    //    $('.mCSB_draggerRail').css("width", "0%");
    //    $('.mCSB_scrollTools').css("width", "0%");
    //    $('.mCSB_draggerContainer').css("display", "none");
    //    $('.ui-widget-content').css("background", "none");
    //    $('.mCSB_dragger_bar').css("width", "0% !important");
    //    //$('.hotel-details').css("margin-left", "2px").css("width", "93.6%");
    //    //$('.trip-right').css("margin-left", "-52px").css("width", "51%");
    //    //$('.place-box-right').css("width", "550px");
    //    //$('.city-row').css("width", "625px");
    //    //$('.place-details').css("width", "177%").css("margin-left", "13%");
    //    //$('.travel').css("width", "187%").css("margin-left", "13%");
    //    //$('.travel ul').css(" text-align", "center").css("margin-left", "30%");
    //}
}

function setid_for_alltravelmodes() {
    $('.place-box .travel').each(function (i) {
        $(this).attr("id", "ltra_" + i);
    });
}

function inthetripfn() {
    var rhaaeleid = "";
    $('#rhaa1 .resort-row').each(function () {
        var curresid = $(this).attr('id');
        var sp_curresid = curresid.split("_");
        for (var loopvar = 0, HotelsInTrip = inthetrip_hotel.length; loopvar < HotelsInTrip; loopvar++) {
            if (inthetrip_hotel[loopvar] == sp_curresid[1]) {
                $(this).find('.add-trip').html("In the trip");
                $(this).find('.book').parent().css('display', 'block');
                $(this).switchClass("resort-row", "resort-row2");
            }
        }
    });
}

function calculatetravelduration() {
    var plc_origin_addr = "", plc_destin_addr = "", plc_origin_addr1 = "", plc_destin_addr1 = "";
    var ori_lat = "", ori_lng = "", des_lat = "", des_lng = "";
    incr_td = 0, incre_printtravelduration = 0;

    $(".findnewtravel").each(function () {
        plc_origin_addr1 = $(this).prev('.place').find(".p-left h4").text() + "," + $(this).prev('.place').find(".p-left p").text();
        plc_destin_addr1 = $(this).next('.place').find(".p-left h4").text() + "," + $(this).next('.place').find(".p-left p").text();
        plc_origin_addr = $(this).prev('.place').find(".p-left p").text();
        plc_destin_addr = $(this).next('.place').find(".p-left p").text();
        ori_lat = $(this).prev('.place').find('.hidlat').val();
        ori_lng = $(this).prev('.place').find('.hidlng').val();
        des_lat = $(this).next('.place').find('.hidlat').val();
        des_lng = $(this).next('.place').find('.hidlng').val();

        forfirsttimetravelduration.push({
            ori_lt: ori_lat,
            ori_ln: ori_lng,
            des_lt: des_lat,
            des_ln: des_lng,
            orig: plc_origin_addr1,
            desti: plc_destin_addr1,
            origh: plc_origin_addr,
            destih: plc_destin_addr
        });
    });
    array_lengthfrtraveldura = forfirsttimetravelduration.length;
    if (array_lengthfrtraveldura > 0) {
        loopthrougharraytravelmode();
    }
}

function CalcTravelDuration()
{
    for (var indxVal = 0, trvlDurLen = forfirsttimetravelduration.length; indxVal < trvlDurLen; indxVal++) {
        var orlat = forfirsttimetravelduration[indxVal].ori_lt;
        var orlng = forfirsttimetravelduration[indxVal].ori_ln;
        var delat = forfirsttimetravelduration[indxVal].des_lt;
        var delng = forfirsttimetravelduration[indxVal].des_ln;
        var dista = Haversine(orlat, orlng, delat, delng);
        var duration = dista / 40;
        var kmtomin2 = duration * 60;
        var kmtomin3 = kmtomin2.toFixed();
        var kmtomints = 0;
        if (kmtomin3 > 60) {
            var kmtohrs = Math.floor(kmtomin3 / 60);
            var kmtomints1 = kmtomin3 % 60;
            if (kmtohrs <= 3) {
                if (kmtohrs == 1) {
                    kmtomints = Math.ceil(kmtomints / 5) * 5;
                    final_time = kmtohrs + " hour " + kmtomints + " mins ";
                }
                else {
                    kmtomints = Math.ceil(kmtomints / 5) * 5;
                    final_time = kmtohrs + " hours " + kmtomints + " mins ";
                }
            }
            else {
                final_time = "20 mins ";
            }
        }
        else {
            if (kmtomin3 <= 1) {
                kmtomin3 = kmtomin3 + 4;
            }
            kmtomin3 = Math.ceil(kmtomin3 / 5) * 5;
            final_time = parseInt(kmtomin3) + " mins ";
        }
        traveldurationarray.push({
            distance: dista,
            duration: final_time
        });
    }
}

function loopthrougharraytravelmode() {
    var distance_service = new google.maps.DistanceMatrixService();
    var orlat = forfirsttimetravelduration[incr_td].ori_lt;
    var orlng = forfirsttimetravelduration[incr_td].ori_ln;
    var delat = forfirsttimetravelduration[incr_td].des_lt;
    var delng = forfirsttimetravelduration[incr_td].des_ln;
    var orilat_lng = new google.maps.LatLng(orlat, orlng);
    var deslat_lng = new google.maps.LatLng(delat, delng);    

    distance_service.getDistanceMatrix(
         {
             origins: [orilat_lng],
             destinations: [deslat_lng],
             travelMode: google.maps.TravelMode.DRIVING
         }, callbackcar);
}

function callbackcar(response, status) {
    if (status === "OK") {
        var dist = "", dura = "";
        if (response.rows[0].elements[0].distance) {
            dist = response.rows[0].elements[0].distance.text;
            dura = response.rows[0].elements[0].duration.text;
            if (dura.indexOf("day") != -1 || dura.indexOf("days") != -1) {
                dura = "20 mins";
            }
            if (dura.indexOf("hours") != -1) {
                dura = "1 hour";
            }
            traveldurationarray.push({
                distance: dist,
                duration: dura
            });
        }
        else {
            traveldurationarray.push({
                distance: "--",
                duration: "No dura.."
            });
        }
        incr_td++;
        if (incr_td < array_lengthfrtraveldura) {
            loopthrougharraytravelmode();
        }
        else {
            printdurationfortravelmode();
        }
    }
    else {
        if (status == "OVER_QUERY_LIMIT") {
            if (incr_td < array_lengthfrtraveldura) {
                loopthrougharraytravelmode();
            }
            else {
                printdurationfortravelmode();
            }
        }
        else {
            traveldurationarray.push({
                distance: "--",
                duration: "No dura.."
            });
            incr_td++;
            if (incr_td < array_lengthfrtraveldura) {
                loopthrougharraytravelmode();
            }
            else {
                printdurationfortravelmode();
            }
        }
    }
}

function printdurationfortravelmode() {
    $(".findnewtravel").each(function () {
        var travel_duris = traveldurationarray[incre_printtravelduration].duration;
        var splitTraveltime = traveldurationarray[incre_printtravelduration].duration.split(' ');
        for (var loopthr = 0, TravelTimeFind = splitTraveltime.length; loopthr < TravelTimeFind; loopthr++) {
            if (splitTraveltime[loopthr] == "min" || splitTraveltime[loopthr] == "mins") {
                splitTraveltime[loopthr - 1] = Math.ceil(splitTraveltime[loopthr - 1] / 5) * 5;
            }
        }
        traveldurationarray[incre_printtravelduration].duration = splitTraveltime.join(" ");
        $(this).find("p").text('Time taken by car is ' + traveldurationarray[incre_printtravelduration].duration);
        $(this).find("p").append(' <img src="../Content/images/edit.png" alt="" class="editimg" style="height: 15px;width: 15px;">');
        $(this).find(".hidcar").val(traveldurationarray[incre_printtravelduration].duration);
        incre_printtravelduration++;
        var day = "day";
        var days = "days";
        var hours = "hours";
        var hour = "hour";
        var mins = "mins";
        if (travel_duris.indexOf(day) != -1 || travel_duris.indexOf(days) != -1)
        {
            travel_duris = "20 mins";
        }
        if (travel_duris.indexOf(hours) != -1) {
            travel_duris = "1 hour";           
        }
        if (travel_duris.indexOf(hour) != -1) {
            var array = travel_duris.split(' ');
            if (parseInt(array[0]) > 2)
            {
                array[0] = "1";
            }
            var amount = array[0] / .50;
            if (array[0] < 50) {
                amount = array[0] / .50;
            }
            else {
                amount = 50 / .50;
            }
            var timespent = amount * 58;
            if (travel_duris.indexOf(mins) != -1) {
                if (array[2] > 20) {
                    timespent = array[2] / 30 + timespent;
                    $(this).css("height", timespent);
                }
                else {
                    $(this).css("height", timespent);
                }
            }
            else {
                $(this).css("height", timespent);
            }
            travel_duris = array.join(' ');
            //traveldurationarray[incre_printtravelduration-1].duration = travel_duris;
            //$(this).find("p").text('Time taken by car is ' + traveldurationarray[incre_printtravelduration-1].duration);
            //$(this).find("p").append(' <img src="../Content/images/edit.png" alt="" class="editimg" style="height: 15px;width: 15px;">');
            //$(this).find(".hidcar").val(traveldurationarray[incre_printtravelduration-1].duration);
        }
        else if (travel_duris.indexOf(mins) != -1) {
            var array = travel_duris.split(' ');
            if (array[0] > 20) {
                var amount = array[0] / 30;
                var timespent = amount * 58;
                $(this).css("height", timespent);
            }
        }        
    });
    finalcallfortravelmode();
}

function finalcallfortravelmode() {
    for (var index_loopvariable = 0, TravelmodesLength = traveldurationarray.length; index_loopvariable < TravelmodesLength; index_loopvariable++) {
        if (traveldurationarray[index_loopvariable].duration == "No dura..") {
            var dista = Haversine(forfirsttimetravelduration[index_loopvariable].ori_lt, forfirsttimetravelduration[index_loopvariable].ori_ln, forfirsttimetravelduration[index_loopvariable].des_lt, forfirsttimetravelduration[index_loopvariable].des_ln);
            var kmtomints = 0;
            var kmtomin1 = dista / 70;
            var kmtomin2 = kmtomin1 * 60;
            var kmtomin3 = kmtomin2.toFixed();
            var final_time = "";
            if (isNaN(kmtomin3)) {
                kmtomin3 = 15;
            }
            if (kmtomin3 > 60) {
                var kmtohrs = Math.floor(kmtomin3 / 60);
                var kmtomints1 = kmtomin3 % 60;
                if (kmtohrs <= 3) {
                    if (kmtohrs == 1) {
                        kmtomints = Math.ceil(kmtomints / 5) * 5;
                        final_time = kmtohrs + " hour " + kmtomints + " mins ";
                    }
                    else {
                        kmtomints = Math.ceil(kmtomints / 5) * 5;
                        final_time = kmtohrs + " hours " + kmtomints + " mins ";
                    }
                }
                else {
                    final_time = "20 mins ";
                }
            }
            else {
                if (kmtomin3 <= 1) {
                    kmtomin3 = kmtomin3 + 4;
                }
                kmtomin3 = Math.ceil(kmtomin3 / 5) * 5;
                final_time = parseInt(kmtomin3) + " mins ";
            }
            traveldurationarray[index_loopvariable].duration = final_time;
            $(".findnewtravel:eq(" + index_loopvariable + ")").css("height", "");
            var hours = "hours";
            var hour = "hour";
            var mins = "mins";

            $(".findnewtravel:eq(" + index_loopvariable + ")").find("p").text('Time taken by car is ' + traveldurationarray[index_loopvariable].duration);
            $(".findnewtravel:eq(" + index_loopvariable + ")").find("p").append(' <img src="../Content/images/edit.png" alt="" class="editimg" style="height: 15px;width: 15px;">');
            $(".findnewtravel:eq(" + index_loopvariable + ")").find(".hidcar").val(traveldurationarray[index_loopvariable].duration);

            if (final_time.indexOf(hour) != -1) {
                var array = final_time.split(' ');
                var amount = array[0] / .25;
                if (array[0] < 50) {
                    amount = array[0] / .25;
                }
                else {
                    amount = 50 / .25;
                }
                var timespent = amount * 58;
                if (final_time.indexOf(mins) != -1) {
                    if (array[2] > 20) {
                        timespent = array[2] / 30 + timespent;
                        $(".findnewtravel:eq(" + index_loopvariable + ")").css("height", timespent);
                    }
                    else {
                        $(".findnewtravel:eq(" + index_loopvariable + ")").css("height", timespent);
                    }
                }
                else {
                    $(".findnewtravel:eq(" + index_loopvariable + ")").css("height", timespent);
                }
            }
            else if (final_time.indexOf(mins) != -1) {
                var array = final_time.split(' ');
                if (array[0] > 20) {
                    var amount = array[0] / 30;
                    var timespent = amount * 58;
                    $(".findnewtravel:eq(" + index_loopvariable + ")").css("height", timespent);
                }
            }
        }
    }
    $('.travel').each(function () {
        $(this).removeClass('findnewtravel');
    });

    chck_ItnraryExceedsDay();
}

function Haversine(lat1, lon1, lat2, lon2) {
    var earth_rad = 6372.8; 
    var dLat = Deg2Rad(lat2 - lat1);
    var dLon = Deg2Rad(lon2 - lon1);
    var area = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(Deg2Rad(lat1)) * Math.cos(Deg2Rad(lat2)) * Math.sin(dLon / 2) * Math.sin(dLon / 2);
    var circum = 2 * Math.atan2(Math.sqrt(area), Math.sqrt(1 - area));
    var distance_ll = earth_rad * circum;
    return distance_ll; 
}

function Deg2Rad(deg) {
    return deg * Math.PI / 180;
}