﻿var i = true;
var IncreasePoint = 1;
var Interval;
var ItemDragged = false;
var IncreasePointLeft = 1;
var strtPlcHeit = "", strtTmHeit = "";
var last_position = {};
//var strtNoty = "";
var prevOffset = 0, prevTopOffset = 0, prevLeftOffset = 0;
var scrollVal = 0;

function dragDropWithItinerary() {
    //alert('dragDropWithItinerary');
    //debugger
    $('#LogTripId').val('');
    $(".place-box").sortable({
        connectWith: ".place-box",
        cursor: "move",
        handle1: ".place-details",
        cancel: ".portlet-toggle,.custom_travel,.travel",
        containment: "#mCSB_2_container",
        helper: "clone",
        delay: 200,
        tolerance: "intersect",
        //distance: 40,
        opacity: 0.8,
        tolerance: "pointer",
        //revert: true,
        placeholder: "portlet-placeholder ui-corner-all",
        start: function (event, ui) {

            scrollVal = 0;
            prevOffset = 0, prevTopOffset = 0, prevLeftOffset = 0;
            //strtNoty = noty({
            //    layout: 'topRight',
            //   text: 'Are you sure..',
            //   type: 'warning',               
            //    dismissQueue: true,
            //    maxVisible: 1,
            //    animation: {
            //        open: { height: 'toggle' },
            //        close: { height: 'toggle' },
            //        easing: 'swing',
            //        speed: 500
            //    }
            //});
            keepTrackForsave++;
            orpre = 0, despre = 0;
            origi = "", origi_id = "", desti = "", desti_id = "";
            $(ui.helper).find('.place-details').css('height', '100px');
            var strtplcbxrit = $(ui.item).parents(".place-box-right").attr('id');
            sortStartId = strtplcbxrit;
            var spstrtplcbxrit = strtplcbxrit.split("_");
            var strtid = parseInt(spstrtplcbxrit[1]) + 1;
            strtcityname1 = $("#cit_" + strtid).find(".trip-name").html();


            if (!($(ui.item).prev(".travel").length || $(ui.item).prev(".place").length || $(ui.item).prev(".place-box").length)) {
                strtPlcHeit = $(ui.item).height();
                if ($(ui.item).next(".travel").length) {
                    strtTmHeit = $(ui.item).next(".travel").height();
                }
            }
            else {
                strtPlcHeit = 0;
                strtTmHeit = 0;
            }
            if ($(ui.item).find('.p-left').find('span:first').html() != "Custom place.") {
                if ($(ui.item).nextAll(".travel:first").length && $(ui.item).nextAll(".travel:first").prevAll(".place").length <= 1) {
                    desti = $(ui.item).nextAll(".travel:first").next(".place").find(".place-details").find(".p-left").find("h4").text();
                    desti_id = $(ui.item).nextAll(".travel:first").attr('id');
                    despre = 1;
                }
                if ($(ui.item).prev(".travel").length) {
                    origi = $(ui.item).prev(".travel").prev(".place").find(".place-details").find(".p-left").find("h4").text();
                    origi_id = $(ui.item).prev(".travel").attr('id');
                    orpre = 1;
                }
            }
            else {
                if ($(ui.item).prev(".custom_travel").length) {
                    origi = $(ui.item).prev(".custom_travel").prev(".place").find(".place-details").find(".p-left").find("h4").text();
                    customPlc_sort1 = $(ui.item).prev(".custom_travel").attr("id");
                    customSortOriId = $(ui.item).prev(".custom_travel").prev(".place").find(".place-details").attr('id');
                    prev_lat_sort2 = $(ui.item).prev(".custom_travel").prev(".place").find(".hidlat").val();
                    prev_lng_sort2 = $(ui.item).prev(".custom_travel").prev(".place").find(".hidlng").val();
                    orpre = 2;
                }
                else if ($(ui.item).prev(".place").length) {
                    customSortOriId = $(ui.item).prev(".place").find(".place-details").attr('id');
                    origi = $(ui.item).prev(".place").find(".place-details").find(".p-left").find("h4").text();
                    prev_lat_sort2 = $(ui.item).prev(".place").find(".hidlat").val();
                    prev_lng_sort2 = $(ui.item).prev(".place").find(".hidlng").val();
                }
                if ($(ui.item).nextAll(".custom_travel:first").length && $(ui.item).nextAll(".travel:first").prevAll(".place").length >= 1) {
                    desti = $(ui.item).nextAll(".custom_travel:first").next(".place").find(".place-details").find(".p-left").find("h4").text();
                    customSortOriId = $(ui.item).nextAll(".custom_travel:first").next(".place").find(".place-details").attr('id');
                    next_lat_sort2 = $(ui.item).nextAll(".custom_travel:first").next(".place").find(".hidlat").val();
                    next_lng_sort2 = $(ui.item).nextAll(".custom_travel:first").next(".place").find(".hidlng").val();
                    despre = 2;
                }
                else if ($(ui.item).nextAll(".place:first").length) {
                    customSortDesId = $(ui.item).nextAll(".place:first").find(".place-details").attr('id');
                    desti = $(ui.item).nextAll(".place:first").find(".place-details").find(".p-left").find("h4").text();
                    next_lat_sort2 = $(ui.item).nextAll(".place:first").find(".hidlat").val();
                    next_lng_sort2 = $(ui.item).nextAll(".place:first").find(".hidlng").val();
                }
            }
        },
        sort: function (event, ui) {

            //if (scrollVal > 4) {
            var offsetPos = $('#mCSB_2_container').offset();
            $(ui.helper).css("left", event.pageX - offsetPos.left);
            $(ui.helper).css("top", event.pageY - offsetPos.top);
            var width1 = $("#mCSB_1").width();
            var ulWidth = $('#mCSB_1_container').width() - width1;
            var Mscb_1Height = $('#mCSB_1_container').height();
            var divLeft = $("#mCSB_1").offset().left;
            var left1 = event.pageX - divLeft;
            var percent1 = left1 / width1;
            var totalcal = percent1 * ulWidth;
            var draggableDivLeft = ui.position.left;
            var draggableDivRight = (ui.position.left);
            var MouseOnContainer = $("#mCSB_1").offset();
            var DivPositionLeft = MouseOnContainer.left - draggableDivLeft;
            IncreasePointLeft = IncreasePointLeft * 1.02;
            if ((percent1 * ulWidth) < ulWidth) {
                if (IncreasePointLeft > 1.35) {
                    IncreasePointLeft = 1.35;
                }
                $("#mCSB_1_container").css('left', -(percent1 * ulWidth));
            }
            else if (-(percent1 * ulWidth) > ulWidth) {
                $("#mCSB_1_container").css('left', -(ulWidth));
            }
            var cityRowHeight = $(".city-row").height();
            var height1 = $("#mCSB_2").height();
            var mCSBHeight = $('#mCSB_2_container').height();
            var ulheight = mCSBHeight - height1;
            var draggableDivTop = ui.position.top;
            var divTop = $("#mCSB_2").offset().top;
            var top1 = event.pageY - divTop;
            var percent2 = (top1 / ulheight);
            var TopPosition = (percent2 * ulheight);
            var divTopPos = -(percent2 * ulheight) - 100;
            var placeboxheight = ($(".place-box").height());
            divTopPos = divTopPos + 150;
            var HalfHeight = height1 / 4;
            var remainingheight = height1 - HalfHeight;
            IncreasePoint = IncreasePoint * 1.02;
            if ((-(divTopPos) <= HalfHeight) && (-(divTopPos) <= 0)) {
                $("#mCSB_2_container").css("top", "0px");
            }
            else if ((-(divTopPos) >= height1 - 250)) {
                $("#mCSB_2_container").css("top", -(ulheight));
            }
            else {
                if (IncreasePoint > 5) {
                    IncreasePoint = 5;
                }
                if (scrollVal > 8) {
                    $("#mCSB_2_container").css("top", ((divTopPos) * IncreasePoint));
                }
                else {
                    $("#mCSB_2_container").css("top", ((divTopPos) * scrollVal));
                }
            }
            //}
            scrollVal = scrollVal + 2;

            var timeoutId;
            $(".travel>ul,.travel>ul>li").hover(function () {
                if (!timeoutId) {
                    timeoutId = window.setTimeout(function () {
                        timeoutId = null;
                        $(".travel").removeClass("expandTmTop");
                        $(".travel").not(this).removeClass("expandTmBtm");
                        $(this).parents('.travel').addClass('expandTmBtm');
                        // $(this).parents('.travel').css('margin-bottom', '50px');
                    }, 1000);
                }
            },
    function () {
        if (timeoutId) {
            window.clearTimeout(timeoutId);
            timeoutId = null;
        }
        else {
            $(".travel").removeClass("expandTmTop");
            $(".travel").not(this).removeClass("expandTmBtm");
            $(this).parents('.travel').addClass('expandTmBtm');
        }
    });
            $(".travel>p").hover(function () {
                if (!timeoutId) {
                    timeoutId = window.setTimeout(function () {
                        timeoutId = null;
                        $(".travel").not(this).removeClass("expandTmTop");
                        $(".travel").removeClass("expandTmBtm");
                        $(this).parents('.travel').addClass('expandTmTop');
                    }, 500);
                }
            },
       function () {
           if (timeoutId) {
               window.clearTimeout(timeoutId);
               timeoutId = null;
           }
           else {
               $(".travel").not(this).removeClass("expandTmTop");
               $(".travel").removeClass("expandTmBtm");
               $(this).parents('.travel').addClass('expandTmTop');
           }
       });
            $(".custom_travel>ul,.custom_travel>ul>li").hover(function () {
                if (!timeoutId) {
                    timeoutId = window.setTimeout(function () {
                        timeoutId = null;
                        $(".custom_travel").removeClass("expandTmTop");
                        $(".custom_travel").not(this).removeClass("expandTmBtm");
                        $(this).parents('.custom_travel').addClass('expandTmBtm');
                        // $(this).parents('.travel').css('margin-bottom', '50px');
                    }, 1000);
                }
            },
        function () {
            if (timeoutId) {
                window.clearTimeout(timeoutId);
                timeoutId = null;
            }
            else {
                $(".custom_travel").removeClass("expandTmTop");
                $(".custom_travel").not(this).removeClass("expandTmBtm");
                $(this).parents('.custom_travel').addClass('expandTmBtm');
            }
        });
            $(".custom_travel>p").hover(function () {
                if (!timeoutId) {
                    timeoutId = window.setTimeout(function () {
                        timeoutId = null;
                        $(".custom_travel").not(this).removeClass("expandTmTop");
                        $(".custom_travel").removeClass("expandTmBtm");
                        $(this).parents('.custom_travel').addClass('expandTmTop');
                    }, 500);
                }
            },
       function () {
           if (timeoutId) {
               window.clearTimeout(timeoutId);
               timeoutId = null;
           }
           else {
               $(".custom_travel").not(this).removeClass("expandTmTop");
               $(".custom_travel").removeClass("expandTmBtm");
               $(this).parents('.custom_travel').addClass('expandTmTop');
           }
       });

        },
        change: function (event, ui) {
            prevOffset = $('#mCSB_2_container').offset();
            prevTopOffset = event.pageY - prevOffset.top;
            prevLeftOffset = event.pageX - prevOffset.left;
        },
        stop: function (event, ui) {
            //alert('dragDropWithItinerary');
            //debugger
            $(".travel,.custom_travel").removeClass("expandTmTop");
            $(".travel,.custom_travel").removeClass("expandTmBtm");
            $('.travel>ul>li,.travel>ul,.travel>p,.custom_travel>ul,.custom_travel>ul>li,.custom_travel>p').unbind('mouseenter mouseleave');

            //strtNoty.close();

            //var offsetPos = $('#mCSB_2_container').offset();
            //var dropOnelem = document.elementFromPoint(event.pageX - offsetPos.left, (event.pageY - offsetPos.top)-2);
            //if ($(dropOnelem).hasClass('place-details')) {
            //    var drpnew = $(dropOnelem).parents('.place').attr('id');
            //    var elemhit = $('#' + drpnew).height()/2;
            //    //var btmPos = offsetPos.top + elemhit;
            //    //var differ = btmPos - offsetPos.top;
            //    var midvalue=offsetPos.top + elemhit;
            //    if (offsetPos.top < midvalue) {
            //        $('#' + drpnew).before($(ui.item));
            //    }
            //    else
            //    {
            //        $('#' + drpnew).after($(ui.item));
            //    }
            //}

            //var noty_alert = noty({
            //    layout: 'topRight',
            //    text: 'The other elements opening and closing hours might have changed due to this drag and drop operation',
            //    type: 'information',
            //    timeout: 2000,
            //    maxVisible: 1,
            //    animation: {
            //        open: { height: 'toggle' },
            //        close: { height: 'toggle' },
            //        easing: 'swing',
            //        speed: 500
            //    }
            //});            

            var totalHeight = $("#mCSB_2_container").height();
            var uiPosition = ui.position.top;
            var halfHeight = totalHeight / 2;
            var QuarterHeight = totalHeight / 4;
            var haHeight = ((totalHeight * 3) / 4);
            if (uiPosition < totalHeight / QuarterHeight) {
                IncreasePoint = 1;
            } else if ((uiPosition > totalHeight / QuarterHeight) && ((uiPosition < halfHeight))) {
                IncreasePoint = 2;
            }
            else if ((uiPosition > halfHeight) && (uiPosition < haHeight)) {
                IncreasePoint = 3;

            } else if ((uiPosition > haHeight)) {
                IncreasePoint = 4;
            }
            IncreasePointLeft = 1;
            DivNextValue = 0;
            $("#mCSB_1_scrollbar_horizontal").removeClass("mCSB_scrollTools_onDrag");
            $("#mCSB_1_dragger_horizontal").removeClass("mCSB_scrollTools_onDrag");
            var stopplcbxrit = $(ui.item).parents(".place-box-right").attr('id');
            sortStopId = stopplcbxrit;
            var spstopplcbxrit = stopplcbxrit.split("_");
            var stopid = parseInt(spstopplcbxrit[1]) + 1;
            var stopcityname = $("#cit_" + stopid).find(".trip-name").html();
            var stopHolderId = parseInt(spstopplcbxrit[1]);
            if (strtcityname1.trim() != stopcityname.trim()) {
                $(this).sortable('cancel');
            }
            else {
                var FitOrNot = 1;
                if (sortStopId != sortStartId) {
                    //FitOrNot = findCanFitPlace(stopplcbxrit);
                    var incr_num_plcbx = 0, PlcsTm_height = 0;
                    $("#" + stopplcbxrit).find(".place-box").each(function () {
                        incr_num_plcbx++;
                    });
                    $("#" + stopplcbxrit).find(".place").each(function () {
                        PlcsTm_height = PlcsTm_height + $(this).height();
                    });
                    $("#" + stopplcbxrit).find(".travel").each(function () {
                        PlcsTm_height = PlcsTm_height + $(this).height();
                    });
                    $("#" + stopplcbxrit).find(".custom_travel").each(function () {
                        PlcsTm_height = PlcsTm_height + $(this).height();
                    });
                    PlcsTm_height = PlcsTm_height + ((incr_num_plcbx - 1) * 50);
                    if (PlcsTm_height > 2060) {
                        FitOrNot = 0;
                    }
                    else {
                        FitOrNot = 1;
                    }
                }
                if (FitOrNot) {
                    var plcTyp = $(ui.item).find('.p-left').find('span:first').html();
                    if (plcTyp != "Custom place.") {
                        var tripStrtDt = $("#TripStrtDate").val();
                        var daynum1 = 0, daynum2 = 0;
                        $(".place-box-right").each(function () {
                            if ($(this).attr('id') == stopplcbxrit) {
                                daynum2 = daynum1;
                            }
                            daynum1++;
                        });
                        var getNumdays1 = daynum2;
                        var getdate1 = new Date(tripStrtDt);
                        var newdate = new Date(getdate1);
                        newdate.setDate(newdate.getDate() + getNumdays1);
                        var getday1 = newdate.getDay();
                        var curInvObj = this;
                        var uiplcID = $(ui.item).find(".place-details").attr('id');
                        var uiItemPos = $(ui.item).position().top;
                        var findtime = Math.ceil((uiItemPos / 55) / 5) * 5;
                        var actStTime = parseInt(timingArr[findtime]) - 2;
                        var actEnTime = timingArr[findtime];
                        var DayExists1 = false;
                        var DayExists2 = false;
                        var strtTime = "";
                        var clsTime = "";
                        var timeDet = [];
                        var acceptState = 0;

                        if (plcTyp == "Attraction.") {
                            for (var loopval = 0, looplen = attrDetails.length; loopval < looplen; loopval++) {
                                if (attrDetails[loopval].plc_id == uiplcID) {
                                    if (attrDetails[loopval].details.opening_hours) {
                                        timeDet = "No time";
                                        //timeDet = attrDetails[loopval].details.opening_hours.periods;  //commented by aes93 since periods is not defined                                       
                                    }
                                    else {
                                        timeDet = "No time";
                                    }
                                    break;
                                }
                            }
                        }
                        else if (plcTyp == "Restaurant.") {
                            for (var loopval = 0, looplen = restDetails.length; loopval < looplen; loopval++) {
                                if (restDetails[loopval].plc_id == uiplcID) {
                                    if (restDetails[loopval].details.opening_hours) {

                                        timeDet = "No time";
                                        //timeDet = restDetails[loopval].details.opening_hours.periods; //commented by aes93 since periods is not defined             

                                    }
                                    else {
                                        timeDet = "No time";
                                    }
                                    break;
                                }
                            }
                        }
                        //else if (plcTyp == "Hotel." || plcTyp == "Activity.") {
                        //    timeDet = "No time";
                        //}

                        if (timeDet != "No time" && timeDet !== undefined) {
                            for (var dayIndx = 0, dayArrLen = timeDet.length; dayIndx < dayArrLen; dayIndx++) {
                                if (timeDet[dayIndx].open) {
                                    if (timeDet[dayIndx].open.day == getday1) {
                                        strtTime = timeDet[dayIndx].open.hours;
                                        DayExists1 = true;
                                    }
                                }
                                if (timeDet[dayIndx].close) {
                                    if (timeDet[dayIndx].close.day == getday1) {
                                        clsTime = timeDet[dayIndx].close.hours;
                                        DayExists2 = true;
                                    }
                                }
                                if (!DayExists1) {
                                    strtTime = "25";
                                }
                                if (!DayExists2) {
                                    clsTime = "0";
                                }
                            }
                        }
                        else {
                            strtTime = "6";
                            clsTime = "24";
                        }
                        if ((parseInt(strtTime) > parseInt(actStTime) || parseInt(clsTime) < parseInt(actEnTime)) && plcTyp != "Hotel.") {
                            var answer = confirm("The spot is closed at this time. Do you still want to place it in this time?");
                            if (!answer) {
                                acceptState = 1;
                            }
                            if (acceptState == 0) {
                                if (orpre == 1 && despre == 1) {
                                    $("#" + origi_id).remove();
                                    travelmodeid2 = desti_id;
                                    LoadssdTravels(origi, desti);
                                }
                                else if (orpre == 1 && despre == 0) {
                                    $("#" + origi_id).remove();
                                }
                                else if (orpre == 0 && despre == 1) {
                                    $("#" + desti_id).remove();
                                }
                                var uiplacename = "", uiplaceid = "", ui_plc_lat = "", ui_plc_lng = "";
                                var ui_origin = "", ui_destin = "";
                                var ui_origin_id = "", ui_destin_id = "";
                                if ($(ui.item).parent().attr('id') == "" || $(ui.item).parent().attr('id') == null || $(ui.item).parent().attr('id') == "undefined") {
                                    $(ui.item).parent().replaceWith($(ui.item));
                                }
                                if ($(ui.item).hasClass("place")) {
                                    uiplacename = $(ui.item).find(".place-details").find(".p-left").find("h4").text();
                                    uiplaceid = $(ui.item).find(".place-details").attr('id');
                                    ui_plc_lat = $(ui.item).find(".hidlat").val();
                                    ui_plc_lng = $(ui.item).find(".hidlng").val();
                                }
                                if ($(ui.item).prev(".travel").length) {
                                    ui_origin = $('#place' + uiplaceid).prev(".travel").prevAll(".place:first").find(".place-details").find(".p-left").find("h4").text();
                                    ui_origin_id = $('#place' + uiplaceid).prev(".travel").prevAll(".place:first").find(".place-details").attr('id');
                                    prev_lat_sort1 = $('#place' + uiplaceid).prev(".travel").prevAll(".place:first").find(".hidlat").val();
                                    prev_lng_sort1 = $('#place' + uiplaceid).prev(".travel").prevAll(".place:first").find(".hidlng").val();
                                    next_lat_sort1 = ui_plc_lat;
                                    next_lng_sort1 = ui_plc_lng;
                                    var tm_id = "tm" + ui_origin_id.substring(0, 6) + "_" + uiplaceid.substring(0, 6);
                                    $(ui.item).prev(".travel").attr('id', tm_id);
                                    travelmodeid1 = tm_id;
                                    callhaversineforsort(1);
                                    //LoadsdTravels(ui_origin, uiplacename);
                                }
                                else if ($(ui.item).prev(".place").length) {
                                    if ($(ui.item).prev(".place").find('.p-left').find('span:first').html() != "Custom place.") {
                                        ui_origin = $('#place' + uiplaceid).prev(".place").find(".place-details").find(".p-left").find("h4").text();
                                        ui_origin_id = $('#place' + uiplaceid).prev(".place").find(".place-details").attr('id');
                                        prev_lat_sort1 = $('#place' + uiplaceid).prev(".place").find(".hidlat").val();
                                        prev_lng_sort1 = $('#place' + uiplaceid).prev(".place").find(".hidlng").val();
                                        next_lat_sort1 = ui_plc_lat;
                                        next_lng_sort1 = ui_plc_lng;
                                        var tm_id = "tm" + ui_origin_id.substring(0, 6) + "_" + uiplaceid.substring(0, 6);
                                        var travelModediv = "<div class='travel' id='" + tm_id + "'><a href='#' style='float:right;'><img src='../Content/images/del_travel.png' class='travedelimg'/></a><p></p><ul>";
                                        travelModediv += "<li><input type='hidden' class='hidcar' value=''/><a href='#' data-value='car' class='active'><img src='../Content/images/car-icon.png' alt=''/></a></li>";
                                        travelModediv += "<li><input type='hidden' class='hidtrain' value=''/><a href='#' data-value='train'><img src='../Content/images/train-icon.png' alt='' /></a></li>";
                                        travelModediv += "<li><input type='hidden' class='hidplane' value=''/><a href='#' data-value='plane'><img src='../Content/images/plane-icon.png' alt='' /></a></li>";
                                        travelModediv += "<li><input type='hidden' class='hidwalk' value=''/><a href='#' data-value='walk'><img src='../Content/images/walk-icon.png' alt='' /></a></li>";
                                        travelModediv += "<li class='last'><input type='hidden' class='hidcycle' value=''/><a href='#' data-value='bicycle'><img src='../Content/images/scooter-icon.png' alt='' /></a></li>";
                                        travelModediv += "<input type='hidden' class='hiddriving' value=''/></ul></div>";
                                        $(ui.item).before($(travelModediv));
                                        travelmodeid1 = tm_id;
                                        callhaversineforsort(1);
                                        //LoadsdTravels(ui_origin, uiplacename);
                                    }
                                }
                                if ($(ui.item).next(".travel").length) {
                                    ui_destin = $(ui.item).next(".travel").nextAll(".place:first").find(".place-details").find(".p-left").find("h4").text();
                                    ui_destin_id = $(ui.item).next(".travel").nextAll(".place:first").find(".place-details").attr('id');
                                    next_lat_sort2 = $(ui.item).next(".travel").nextAll(".place:first").find(".hidlat").val();
                                    next_lng_sort2 = $(ui.item).next(".travel").nextAll(".place:first").find(".hidlng").val();
                                    prev_lat_sort2 = ui_plc_lat;
                                    prev_lng_sort2 = ui_plc_lng;
                                    //next_lat_sort2 = "";
                                    //next_lng_sort2 = "";
                                    var tm_id = "tm" + uiplaceid.substring(0, 6) + "_" + ui_destin_id.substring(0, 6);
                                    $(ui.item).next(".travel").attr('id', tm_id);
                                    travelmodeid2 = tm_id;
                                    setTimeout(function () { LoadssdTravels(uiplacename, ui_destin) }, 2000);
                                }
                                else if ($(ui.item).next(".place").length) {
                                    if ($(ui.item).next(".place").find('.p-left').find('span:first').html() != "Custom place.") {
                                        ui_destin = $(ui.item).next(".place").find(".place-details").find(".p-left").find("h4").text();
                                        ui_destin_id = $(ui.item).next(".place").find(".place-details").attr('id');
                                        prev_lat_sort2 = ui_plc_lat;
                                        prev_lng_sort2 = ui_plc_lng;
                                        next_lat_sort2 = $(ui.item).next(".place").find(".hidlat").val();
                                        next_lng_sort2 = $(ui.item).next(".place").find(".hidlng").val();
                                        var tm_id = "tm" + uiplaceid.substring(0, 6) + "_" + ui_destin_id.substring(0, 6);
                                        var travelModediv = "<div class='travel' id='" + tm_id + "'><a href='#' style='float:right;'><img src='../Content/images/del_travel.png' class='travedelimg'/></a><p></p><ul>";
                                        travelModediv += "<li><input type='hidden' class='hidcar' value=''/><a href='#' data-value='car' class='active'><img src='../Content/images/car-icon.png' alt=''/></a></li>";
                                        travelModediv += "<li><input type='hidden' class='hidtrain' value=''/><a href='#' data-value='train'><img src='../Content/images/train-icon.png' alt='' /></a></li>";
                                        travelModediv += "<li><input type='hidden' class='hidplane' value=''/><a href='#' data-value='plane'><img src='../Content/images/plane-icon.png' alt='' /></a></li>";
                                        travelModediv += "<li><input type='hidden' class='hidwalk' value=''/><a href='#' data-value='walk'><img src='../Content/images/walk-icon.png' alt='' /></a></li>";
                                        travelModediv += "<li class='last'><input type='hidden' class='hidcycle' value=''/><a href='#' data-value='bicycle'><img src='../Content/images/scooter-icon.png' alt='' /></a></li>";
                                        travelModediv += "<input type='hidden' class='hiddriving' value=''/></ul></div>";
                                        $(ui.item).after($(travelModediv));
                                        travelmodeid2 = tm_id;
                                        setTimeout(function () { LoadssdTravels(uiplacename, ui_destin) }, 2000);
                                    }
                                }
                                else if ($(ui.item).next(".place-box").length) {
                                    $(ui.item).nextUntil(".place", ".place-box").each(function () {
                                        $(this).remove();
                                    });
                                    if ($(ui.item).next(".place").find('.p-left').find('span:first').html() != "Custom place.") {
                                        ui_destin = $(ui.item).next(".place").find(".place-details").find(".p-left").find("h4").text();
                                        ui_destin_id = $(ui.item).next(".place").find(".place-details").attr('id');
                                        prev_lat_sort2 = ui_plc_lat;
                                        prev_lng_sort2 = ui_plc_lng;
                                        next_lat_sort2 = $(ui.item).next(".place").find(".hidlat").val();
                                        next_lng_sort2 = $(ui.item).next(".place").find(".hidlng").val();
                                        var tm_id = "tm" + uiplaceid.substring(0, 6) + "_" + ui_destin_id.substring(0, 6);
                                        var travelModediv = "<div class='travel' id='" + tm_id + "'><a href='#' style='float:right;'><img src='../Content/images/del_travel.png' class='travedelimg'/></a><p></p><ul>";
                                        travelModediv += "<li><input type='hidden' class='hidcar' value=''/><a href='#' data-value='car' class='active'><img src='../Content/images/car-icon.png' alt=''/></a></li>";
                                        travelModediv += "<li><input type='hidden' class='hidtrain' value=''/><a href='#' data-value='train'><img src='../Content/images/train-icon.png' alt='' /></a></li>";
                                        travelModediv += "<li><input type='hidden' class='hidplane' value=''/><a href='#' data-value='plane'><img src='../Content/images/plane-icon.png' alt='' /></a></li>";
                                        travelModediv += "<li><input type='hidden' class='hidwalk' value=''/><a href='#' data-value='walk'><img src='../Content/images/walk-icon.png' alt='' /></a></li>";
                                        travelModediv += "<li class='last'><input type='hidden' class='hidcycle' value=''/><a href='#' data-value='bicycle'><img src='../Content/images/scooter-icon.png' alt='' /></a></li>";
                                        travelModediv += "<input type='hidden' class='hiddriving' value=''/></ul></div>";
                                        $(ui.item).after($(travelModediv));
                                        travelmodeid2 = tm_id;
                                        setTimeout(function () { LoadssdTravels(uiplacename, ui_destin) }, 2000);
                                    }
                                }
                            }
                            else {
                                $(this).sortable('cancel');
                            }
                        }
                        else {
                            if (orpre == 1 && despre == 1) {
                                $("#" + origi_id).remove();
                                travelmodeid2 = desti_id;
                                callhaversineforsort(2);
                                //LoadssdTravels(origi, desti);
                            }
                            else if (orpre == 1 && despre == 0) {
                                $("#" + origi_id).remove();
                            }
                            else if (orpre == 0 && despre == 1) {
                                $("#" + desti_id).remove();
                            }
                            var uiplacename = "", uiplaceid = "", ui_plc_lat = "", ui_plc_lng = "";
                            var ui_origin = "", ui_destin = "";
                            var ui_origin_id = "", ui_destin_id = "";
                            if ($(ui.item).parent().attr('id') == "" || $(ui.item).parent().attr('id') == null || $(ui.item).parent().attr('id') == "undefined") {
                                $(ui.item).parent().replaceWith($(ui.item));
                            }
                            if ($(ui.item).hasClass("place")) {
                                uiplacename = $(ui.item).find(".place-details").find(".p-left").find("h4").text();
                                uiplaceid = $(ui.item).find(".place-details").attr('id');
                                ui_plc_lat = $(ui.item).find(".hidlat").val();
                                ui_plc_lng = $(ui.item).find(".hidlng").val();
                            }
                            if ($(ui.item).prev(".travel").length) {
                                ui_origin = $('#' + sortStopId).find('#place' + uiplaceid).prev(".travel").prevAll(".place:first").find(".place-details").find(".p-left").find("h4").text();
                                ui_origin_id = $('#' + sortStopId).find('#place' + uiplaceid).prev(".travel").prevAll(".place:first").find(".place-details").attr('id');
                                prev_lat_sort1 = $('#' + sortStopId).find('#place' + uiplaceid).prev(".travel").prevAll(".place:first").find(".hidlat").val();
                                prev_lng_sort1 = $('#' + sortStopId).find('#place' + uiplaceid).prev(".travel").prevAll(".place:first").find(".hidlng").val();
                                next_lat_sort1 = ui_plc_lat;
                                next_lng_sort1 = ui_plc_lng;
                                var tm_id = "tm" + ui_origin_id.substring(0, 6) + "_" + uiplaceid.substring(0, 6);
                                $(ui.item).prev(".travel").attr('id', tm_id);
                                travelmodeid1 = tm_id;
                                callhaversineforsort(1);
                                //LoadsdTravels(ui_origin, uiplacename);
                            }
                            else if ($(ui.item).prev(".place").length) {
                                if ($(ui.item).prev(".place").find('.p-left').find('span:first').html() != "Custom place.") {
                                    ui_origin = $('#' + sortStopId).find('#place' + uiplaceid).prev(".place").find(".place-details").find(".p-left").find("h4").text();
                                    ui_origin_id = $('#' + sortStopId).find('#place' + uiplaceid).prev(".place").find(".place-details").attr('id');
                                    prev_lat_sort1 = $('#' + sortStopId).find('#place' + uiplaceid).prev(".place").find(".hidlat").val();
                                    prev_lng_sort1 = $('#' + sortStopId).find('#place' + uiplaceid).prev(".place").find(".hidlng").val();
                                    next_lat_sort1 = ui_plc_lat;
                                    next_lng_sort1 = ui_plc_lng;
                                    var tm_id = "tm" + ui_origin_id.substring(0, 6) + "_" + uiplaceid.substring(0, 6);
                                    var travelModediv = "<div class='travel' id='" + tm_id + "'><a href='#' style='float:right;'><img src='../Content/images/del_travel.png' class='travedelimg'/></a><p></p><ul>";
                                    travelModediv += "<li><input type='hidden' class='hidcar' value=''/><a href='#' data-value='car' class='active'><img src='../Content/images/car-icon.png' alt=''/></a></li>";
                                    travelModediv += "<li><input type='hidden' class='hidtrain' value=''/><a href='#' data-value='train'><img src='../Content/images/train-icon.png' alt='' /></a></li>";
                                    travelModediv += "<li><input type='hidden' class='hidplane' value=''/><a href='#' data-value='plane'><img src='../Content/images/plane-icon.png' alt='' /></a></li>";
                                    travelModediv += "<li><input type='hidden' class='hidwalk' value=''/><a href='#' data-value='walk'><img src='../Content/images/walk-icon.png' alt='' /></a></li>";
                                    travelModediv += "<li class='last'><input type='hidden' class='hidcycle' value=''/><a href='#' data-value='bicycle'><img src='../Content/images/scooter-icon.png' alt='' /></a></li>";
                                    travelModediv += "<input type='hidden' class='hiddriving' value=''/></ul></div>";
                                    $(ui.item).before($(travelModediv));
                                    travelmodeid1 = tm_id;
                                    callhaversineforsort(1);
                                    //LoadsdTravels(ui_origin, uiplacename);
                                }
                            }
                            if ($(ui.item).next(".travel").length) {
                                ui_destin = $(ui.item).next(".travel").nextAll(".place:first").find(".place-details").find(".p-left").find("h4").text();
                                ui_destin_id = $(ui.item).next(".travel").nextAll(".place:first").find(".place-details").attr('id');
                                next_lat_sort2 = $(ui.item).next(".travel").nextAll(".place:first").find(".hidlat").val();
                                next_lng_sort2 = $(ui.item).next(".travel").nextAll(".place:first").find(".hidlng").val();
                                prev_lat_sort2 = ui_plc_lat;
                                prev_lng_sort2 = ui_plc_lng;
                                //next_lat_sort2 = "";
                                //next_lng_sort2 = "";
                                var tm_id = "tm" + uiplaceid.substring(0, 6) + "_" + ui_destin_id.substring(0, 6);
                                $(ui.item).next(".travel").attr('id', tm_id);
                                travelmodeid2 = tm_id;
                                callhaversineforsort(2);
                                //setTimeout(function () { LoadssdTravels(uiplacename, ui_destin) }, 2000);
                            }
                            else if ($(ui.item).next(".place").length) {
                                if ($(ui.item).next(".place").find('.p-left').find('span:first').html() != "Custom place.") {
                                    ui_destin = $(ui.item).next(".place").find(".place-details").find(".p-left").find("h4").text();
                                    ui_destin_id = $(ui.item).next(".place").find(".place-details").attr('id');
                                    prev_lat_sort2 = ui_plc_lat;
                                    prev_lng_sort2 = ui_plc_lng;
                                    next_lat_sort2 = $(ui.item).next(".place").find(".hidlat").val();
                                    next_lng_sort2 = $(ui.item).next(".place").find(".hidlng").val();
                                    var tm_id = "tm" + uiplaceid.substring(0, 6) + "_" + ui_destin_id.substring(0, 6);
                                    var travelModediv = "<div class='travel' id='" + tm_id + "'><a href='#' style='float:right;'><img src='../Content/images/del_travel.png' class='travedelimg'/></a><p></p><ul>";
                                    travelModediv += "<li><input type='hidden' class='hidcar' value=''/><a href='#' data-value='car' class='active'><img src='../Content/images/car-icon.png' alt=''/></a></li>";
                                    travelModediv += "<li><input type='hidden' class='hidtrain' value=''/><a href='#' data-value='train'><img src='../Content/images/train-icon.png' alt='' /></a></li>";
                                    travelModediv += "<li><input type='hidden' class='hidplane' value=''/><a href='#' data-value='plane'><img src='../Content/images/plane-icon.png' alt='' /></a></li>";
                                    travelModediv += "<li><input type='hidden' class='hidwalk' value=''/><a href='#' data-value='walk'><img src='../Content/images/walk-icon.png' alt='' /></a></li>";
                                    travelModediv += "<li class='last'><input type='hidden' class='hidcycle' value=''/><a href='#' data-value='bicycle'><img src='../Content/images/scooter-icon.png' alt='' /></a></li>";
                                    travelModediv += "<input type='hidden' class='hiddriving' value=''/></ul></div>";
                                    $(ui.item).after($(travelModediv));
                                    travelmodeid2 = tm_id;
                                    callhaversineforsort(2);
                                    //setTimeout(function () { LoadssdTravels(uiplacename, ui_destin) }, 2000);
                                }
                            }
                            else if ($(ui.item).next(".place-box").length) {
                                $(ui.item).nextUntil(".place", ".place-box").each(function () {
                                    $(this).remove();
                                });
                                if ($(ui.item).next(".place").find('.p-left').find('span:first').html() != "Custom place.") {
                                    ui_destin = $(ui.item).next(".place").find(".place-details").find(".p-left").find("h4").text();
                                    ui_destin_id = $(ui.item).next(".place").find(".place-details").attr('id');
                                    prev_lat_sort2 = ui_plc_lat;
                                    prev_lng_sort2 = ui_plc_lng;
                                    next_lat_sort2 = $(ui.item).next(".place").find(".hidlat").val();
                                    next_lng_sort2 = $(ui.item).next(".place").find(".hidlng").val();
                                    var tm_id = "tm" + uiplaceid.substring(0, 6) + "_" + ui_destin_id.substring(0, 6);
                                    var travelModediv = "<div class='travel' id='" + tm_id + "'><a href='#' style='float:right;'><img src='../Content/images/del_travel.png' class='travedelimg'/></a><p></p><ul>";
                                    travelModediv += "<li><input type='hidden' class='hidcar' value=''/><a href='#' data-value='car' class='active'><img src='../Content/images/car-icon.png' alt=''/></a></li>";
                                    travelModediv += "<li><input type='hidden' class='hidtrain' value=''/><a href='#' data-value='train'><img src='../Content/images/train-icon.png' alt='' /></a></li>";
                                    travelModediv += "<li><input type='hidden' class='hidplane' value=''/><a href='#' data-value='plane'><img src='../Content/images/plane-icon.png' alt='' /></a></li>";
                                    travelModediv += "<li><input type='hidden' class='hidwalk' value=''/><a href='#' data-value='walk'><img src='../Content/images/walk-icon.png' alt='' /></a></li>";
                                    travelModediv += "<li class='last'><input type='hidden' class='hidcycle' value=''/><a href='#' data-value='bicycle'><img src='../Content/images/scooter-icon.png' alt='' /></a></li>";
                                    travelModediv += "<input type='hidden' class='hiddriving' value=''/></ul></div>";
                                    $(ui.item).after($(travelModediv));
                                    travelmodeid2 = tm_id;
                                    callhaversineforsort(2);
                                    //setTimeout(function () { LoadssdTravels(uiplacename, ui_destin) }, 2000);
                                }
                            }
                        }
                    }
                    else {
                        if ($(ui.item).next(".place-box").length) {
                            $(ui.item).nextUntil(".place", ".place-box").each(function () {
                                $(this).remove();
                            });
                        }
                        if ($(ui.item).prev(".travel").length) {
                            $(ui.item).prev(".travel").remove();
                        }
                        if ($(ui.item).next(".travel").length) {
                            $(ui.item).next(".travel").remove();
                        }
                        if (orpre == 2 && despre == 2) {
                            var tm_id = "tm" + customSortOriId.substring(0, 6) + "_" + customSortDesId.substring(0, 6);
                            var travelModediv = "<div class='travel' id='" + tm_id + "'><a href='#' style='float:right;'><img src='../Content/images/del_travel.png' class='travedelimg'/></a><p></p><ul>";
                            travelModediv += "<li><input type='hidden' class='hidcar' value=''/><a href='#' data-value='car' class='active'><img src='../Content/images/car-icon.png' alt=''/></a></li>";
                            travelModediv += "<li><input type='hidden' class='hidtrain' value=''/><a href='#' data-value='train'><img src='../Content/images/train-icon.png' alt='' /></a></li>";
                            travelModediv += "<li><input type='hidden' class='hidplane' value=''/><a href='#' data-value='plane'><img src='../Content/images/plane-icon.png' alt='' /></a></li>";
                            travelModediv += "<li><input type='hidden' class='hidwalk' value=''/><a href='#' data-value='walk'><img src='../Content/images/walk-icon.png' alt='' /></a></li>";
                            travelModediv += "<li class='last'><input type='hidden' class='hidcycle' value=''/><a href='#' data-value='bicycle'><img src='../Content/images/scooter-icon.png' alt='' /></a></li>";
                            travelModediv += "<input type='hidden' class='hiddriving' value=''/></ul></div>";
                            $("#" + customPlc_sort1).before($(travelModediv));
                            travelmodeid2 = tm_id;
                            //LoadssdTravels(origi, desti);
                            callhaversineforsort(2);
                            $("#" + customPlc_sort1).remove();
                            $("#" + customPlc_sort2).remove();
                        }
                        else if (orpre == 2 && despre == 0) {
                            $("#" + customPlc_sort1).remove();
                        }
                        else if (orpre == 0 && despre == 2) {
                            $("#" + customPlc_sort2).remove();
                        }
                        else {
                            if (origi != "" && desti != "") {
                                var tm_id = "tm" + customSortOriId.substring(0, 6) + "_" + customSortDesId.substring(0, 6);
                                var travelModediv = "<div class='travel' id='" + tm_id + "'><a href='#' style='float:right;'><img src='../Content/images/del_travel.png' class='travedelimg'/></a><p></p>";
                                travelModediv += "<ul><li><input type='hidden' class='hidcar' value=''/><a href='#' data-value='car' class='active'><img src='../Content/images/car-icon.png' alt=''/></a></li>";
                                travelModediv += "<li><input type='hidden' class='hidtrain' value=''/><a href='#' data-value='train'><img src='../Content/images/train-icon.png' alt='' /></a></li>";
                                travelModediv += "<li><input type='hidden' class='hidplane' value=''/><a href='#' data-value='plane'><img src='../Content/images/plane-icon.png' alt='' /></a></li>";
                                travelModediv += "<li><input type='hidden' class='hidwalk' value=''/><a href='#' data-value='walk'><img src='../Content/images/walk-icon.png' alt='' /></a></li>";
                                travelModediv += "<li class='last'><input type='hidden' class='hidcycle' value=''/><a href='#' data-value='bicycle'><img src='../Content/images/scooter-icon.png' alt='' /></a></li>";
                                travelModediv += "<input type='hidden' class='hiddriving' value=''/></ul></div>";
                                $("#" + customSortDesId).parent(".place").before($(travelModediv));
                                travelmodeid2 = tm_id;
                                callhaversineforsort(2);
                                //LoadssdTravels(origi, desti);
                            }
                        }
                    }
                    if (strtPlcHeit != 0) {
                        var numOfPlcBx = (strtPlcHeit + strtTmHeit) / 50;
                        numOfPlcBx = parseInt(numOfPlcBx);
                        var PlcBox = "";
                        for (var indx = 0; indx < numOfPlcBx; indx++) {
                            PlcBox += "<div class='place-box timing-details'></div>";
                        }
                        $("#" + sortStartId).find(".place:first").before(PlcBox);
                    }
                }
                else {
                    $(this).sortable('cancel');
                    var notify_alert = noty({
                        layout: 'topRight',
                        text: 'This operation is cancelled. Cannot accommodate into this day..',
                        type: 'warning',
                        timeout: 2000,
                        maxVisible: 1,
                        animation: {
                            open: { height: 'toggle' },
                            close: { height: 'toggle' },
                            easing: 'swing',
                            speed: 500
                        }
                    });
                }
            }
        }
    });

    //var setnumofday = 0;

    //$('.city-row:visible').each(function () {
    //    setnumofday++;
    //});

    //if (setnumofday == 1) {
    //    $('.hotel-details').css("margin-left", "2px").css("width", "93.6%");
    //    $('.trip-right').css("margin-left", "-52px").css("width", "51%");
    //    $('.place-box-right').css("width", "550px");
    //    $('.city-row').css("width", "172%");
    //    $('.place-details').css("width", "115%").css("margin-left", "13%");
    //    $('.travel').css("width", "124%").css("margin-left", "13%").css("text-align", "center");
    //    $('.travel ul').css("text-align", "center").css("margin-left", "11%");
    //    $('.mCSB_draggerRail').css("width", "0% !important");
    //    $('.mCSB_scrollTools').css("width", "0% !important");
    //    $('.mCSB_draggerContainer').css("display", "none !important");
    //    $('.ui-widget-content').css("background", "none");
    //    $('.mCSB_dragger_bar').css("width", "0% !important");

    //}
}

$("#rhaa1").sortable({
    connectWith: ".place-box",
    cursor: "move",
    handle: ".resort-img , .rigtravel",
    cancel: ".portlet-toggle,.resort-row2",
    appendTo: "#mCSB_2_container",
    placeholder: "portlet-placeholder ui-corner-all",
    helper: "clone",
    //revert: "invalid",

    start: function (event, ui) {
        keepTrackForsave++;
        prevResortId = ui.item.prev("aside").attr("id");
        de = ui.item.attr('id');
        arr = de.split('_');
        e = $('#' + de).html();
        $(ui.helper).css({ 'width': '260px', 'height': '75px' });
        $(ui.helper).find('.resort-row1').find('.resort-img').find('img').css({ 'width': '50px', 'height': '50px' });
        $(ui.helper).find('.resort-row1').find('.resort-img').find('span').css('display', 'none');
        $(ui.helper).find('.resort-row1').find('.resort-details').find('ul').css('display', 'none');
        $(ui.helper).find('.resort-row1').find('.resort-details').find('.r-left').find('.book-add').css('display', 'none');
        $(ui.helper).find('.resort-row1').find('.r-right').find('ul').css('display', 'block');
        ui.item.show();
        forappendid_onsort = arr[1];
        if ($("#prefer").html() === "Activity Preferences") {
            var sortacthidval = $('#sorthelpact').val();
            if (sortacthidval == 0) {
                $('#sorthelpact').val(arr[1]);
            }
            else {
                $('#sorthelpact').val(sortacthidval + "_" + arr[1]);
            }
        }
        else if ($("#prefer").html() === "Hotel Preferences") {
            var dusort = $('#sorthelp').val();
            if (dusort == 0) {
                $('#sorthelp').val(arr[1]);
            }
            else {
                $('#sorthelp').val(dusort + "_" + arr[1]);
            }
        }
        else if ($("#prefer").html() === "Attraction Preferences") {
            if (arr[2]) {
                forappendid_onsort = arr[1] + "_" + arr[2];
            }
            else {
                forappendid_onsort = arr[1];
            }

        }
        else if ($("#prefer").html() === "Restaurant Preferences") {
            if (arr[2]) {
                forappendid_onsort = arr[1] + "_" + arr[2];
            }
            else {
                forappendid_onsort = arr[1];
            }
        }
    },

    sort: function (event, ui) {
        var id = ui.item[0].id;
        var offsetPos = $('#mCSB_2_container').offset();
        $(ui.helper).css("left", event.pageX - offsetPos.left);
        $(ui.helper).css("top", event.pageY - offsetPos.top);

        var width1 = $("#mCSB_1").width();
        var ulWidth = $('#mCSB_1_container').width() - width1;
        var Mscb_1Height = $('#mCSB_1_container').height();
        var divLeft = $("#mCSB_1").offset().left;
        var left1 = event.pageX - divLeft;
        var percent1 = left1 / width1;
        var totalcal = percent1 * ulWidth;
        var draggableDivLeft = ui.position.left;
        var draggableDivRight = (ui.position.left);
        var MouseOnContainer = $("#mCSB_1").offset();
        var DivPositionLeft = MouseOnContainer.left - draggableDivLeft;
        IncreasePointLeft = IncreasePointLeft * 1.02;
        if ((percent1 * ulWidth) < ulWidth) {
            if (IncreasePointLeft > 1.35) {
                IncreasePointLeft = 1.35;
            }
            $("#mCSB_1_container").css('left', -(percent1 * ulWidth));
        }
        else if (-(percent1 * ulWidth) > ulWidth) {
            $("#mCSB_1_container").css('left', -(ulWidth));
        }
        var cityRowHeight = $(".city-row").height();
        var height1 = $("#mCSB_2").height();
        var mCSBHeight = $('#mCSB_2_container').height();
        var ulheight = mCSBHeight - height1;
        var draggableDivTop = ui.position.top;
        var divTop = $("#mCSB_2").offset().top;
        var top1 = event.pageY - divTop;
        var percent2 = (top1 / ulheight);
        var TopPosition = (percent2 * ulheight);
        var divTopPos = -(percent2 * ulheight) - 100;
        var placeboxheight = ($(".place-box").height());
        divTopPos = divTopPos + 150;
        var HalfHeight = height1 / 4;
        var remainingheight = height1 - HalfHeight;
        IncreasePoint = IncreasePoint * 1.02;
        if ((-(divTopPos) <= HalfHeight) && (-(divTopPos) <= 0)) {
            $("#mCSB_2_container").css("top", "0px");
        }
        else if ((-(divTopPos) >= height1 - 250)) {
            $("#mCSB_2_container").css("top", -(ulheight));
        }
        else {
            if (IncreasePoint > 5) {
                IncreasePoint = 5;
            }
            $("#mCSB_2_container").css("top", ((divTopPos) * IncreasePoint));
        }

        var timeoutId;
        $(".travel>ul,.travel>ul").hover(function () {
            if (!timeoutId) {
                timeoutId = window.setTimeout(function () {
                    timeoutId = null;
                    $(".travel").removeClass("expandTmTop");
                    $(".travel").not(this).removeClass("expandTmBtm");
                    $(this).parents('.travel').addClass('expandTmBtm');
                    // $(this).parents('.travel').css('margin-bottom', '50px');
                }, 1000);
            }
        },
function () {
    if (timeoutId) {
        window.clearTimeout(timeoutId);
        timeoutId = null;
    }
    else {
        $(".travel").removeClass("expandTmTop");
        $(".travel").not(this).removeClass("expandTmBtm");
        $(this).parents('.travel').addClass('expandTmBtm');
    }
});

        $(".travel>p").hover(function () {
            if (!timeoutId) {
                timeoutId = window.setTimeout(function () {
                    timeoutId = null;
                    $(".travel").not(this).removeClass("expandTmTop");
                    $(".travel").removeClass("expandTmBtm");
                    $(this).parents('.travel').addClass('expandTmTop');
                }, 500);
            }
        },
   function () {
       if (timeoutId) {
           window.clearTimeout(timeoutId);
           timeoutId = null;
       }
       else {
           $(".travel").not(this).removeClass("expandTmTop");
           $(".travel").removeClass("expandTmBtm");
           $(this).parents('.travel').addClass('expandTmTop');
       }
   });

        $(".custom_travel>ul,.custom_travel>ul>li").hover(function () {
            if (!timeoutId) {
                timeoutId = window.setTimeout(function () {
                    timeoutId = null;
                    $(".custom_travel").removeClass("expandTmTop");
                    $(".custom_travel").not(this).removeClass("expandTmBtm");
                    $(this).parents('.custom_travel').addClass('expandTmBtm');
                    // $(this).parents('.travel').css('margin-bottom', '50px');
                }, 1000);
            }
        },
            function () {
                if (timeoutId) {
                    window.clearTimeout(timeoutId);
                    timeoutId = null;
                }
                else {
                    $(".custom_travel").removeClass("expandTmTop");
                    $(".custom_travel").not(this).removeClass("expandTmBtm");
                    $(this).parents('.custom_travel').addClass('expandTmBtm');
                }
            });
        $(".custom_travel>p").hover(function () {
            if (!timeoutId) {
                timeoutId = window.setTimeout(function () {
                    timeoutId = null;
                    $(".custom_travel").not(this).removeClass("expandTmTop");
                    $(".custom_travel").removeClass("expandTmBtm");
                    $(this).parents('.custom_travel').addClass('expandTmTop');
                }, 500);
            }
        },
   function () {
       if (timeoutId) {
           window.clearTimeout(timeoutId);
           timeoutId = null;
       }
       else {
           $(".custom_travel").not(this).removeClass("expandTmTop");
           $(".custom_travel").removeClass("expandTmBtm");
           $(this).parents('.custom_travel').addClass('expandTmTop');
       }
   });
    },
    stop: function (event, ui) {

        //alert('dragDropWithItinerary');
        //debugger

        $(".travel,.custom_travel").removeClass("expandTmTop");
        $(".travel,.custom_travel").removeClass("expandTmBtm");
        $('.travel>ul>li,.travel>ul,.travel>p,.custom_travel>ul,.custom_travel>ul>li,.custom_travel>p').unbind('mouseenter mouseleave');

        var totalHeight = $("#mCSB_2_container").height();
        var uiPosition = ui.position.top;
        var halfHeight = totalHeight / 2;
        var QuarterHeight = totalHeight / 4;
        var haHeight = ((totalHeight * 3) / 4);
        if (uiPosition < totalHeight / QuarterHeight) {
            IncreasePoint = 1;
        } else if ((uiPosition > totalHeight / QuarterHeight) && ((uiPosition < halfHeight))) {
            IncreasePoint = 2;
        }
        else if ((uiPosition > halfHeight) && (uiPosition < haHeight)) {
            IncreasePoint = 3;

        } else if ((uiPosition > haHeight)) {
            IncreasePoint = 4;
        }
        IncreasePointLeft = 1;
        DivNextValue = 0;
        $("#mCSB_1_scrollbar_horizontal").removeClass("mCSB_scrollTools_onDrag");
        $("#mCSB_1_dragger_horizontal").removeClass("mCSB_scrollTools_onDrag");
        var incre_plcs_in_plcbx = 0, chck_num_of_hotels = 0;
        var change_to_plc = "";
        var stopplcbxrit = $(ui.item).parents(".place-box-right").attr('id');
        var spstopplcbxrit = stopplcbxrit.split("_");
        var stopid = parseInt(spstopplcbxrit[1]) + 1;
        var stopcityname = $("#cit_" + stopid).find(".trip-name").html();
        var strtcityname = $("#selectedcity").html();

        if ($(ui.item).parent(".place-box").length) {
            $('.place-box').find("#res_" + forappendid_onsort).css('width', '260px');
            var plcbxrit_id = $(ui.item).parents(".place-box-right").attr('id');
            $("#" + plcbxrit_id + " .place").each(function () {
                incre_plcs_in_plcbx++;
                if ($(this).find(".p-left").find("span").html().trim() == "Hotel.") {
                    chck_num_of_hotels++;
                }
            });
            $("#" + plcbxrit_id + " .resort-row").each(function () {
                incre_plcs_in_plcbx++;
                if ($(this).find(".r-left").find(".hotel_identify").length) {
                    chck_num_of_hotels++;
                }
            });
            if (strtcityname.trim() != stopcityname.trim()) {
                $(ui.item).css("width", "");
                $(this).sortable('cancel');
            }
            else {
                var FitOrNot = 1;
                var incr_num_plcbx = 0, PlcsTm_height = 0;
                $("#" + stopplcbxrit).find(".place-box").each(function () {
                    incr_num_plcbx++;
                });
                $("#" + stopplcbxrit).find(".place").each(function () {
                    PlcsTm_height = PlcsTm_height + $(this).height();
                });
                $("#" + stopplcbxrit).find(".travel").each(function () {
                    PlcsTm_height = PlcsTm_height + $(this).height();
                });
                $("#" + stopplcbxrit).find(".custom_travel").each(function () {
                    PlcsTm_height = PlcsTm_height + $(this).height();
                });
                PlcsTm_height = PlcsTm_height + ((incr_num_plcbx - 1) * 50);
                if ($("#prefer").html() == "Attraction Preferences") {
                    PlcsTm_height = PlcsTm_height + 200;
                }
                else if ($("#prefer").html() == "Activity Preferences") {
                    var activPlc = activities_forsort.filter(function (obj) {
                        return obj.id == forappendid_onsort;
                    });
                    var ActivityDur = activPlc[0].durationInMillis;
                    var minutes = ActivityDur / (1000 * 60);
                    minutes = ((minutes / 30) * 100) / 2;
                    if (minutes > 2000) {
                        minutes = 1700;
                    }
                    PlcsTm_height = PlcsTm_height + parseInt(minutes);
                }
                else {
                    PlcsTm_height = PlcsTm_height + 100;
                }
                if (PlcsTm_height > 2010) {
                    FitOrNot = 0;
                }
                else {
                    FitOrNot = 1;
                }

                // FitOrNot = findCanFitPlace(stopplcbxrit);                              
                if (!FitOrNot) {
                    $(ui.item).css('width', '96.8%');
                    $(ui.item).find('.resort-img>img').css({ 'width': '220px', 'height': '140px' });
                    $(ui.item).find('.resort-row1').find('.resort-details').find('.r-left').find('ul').show();
                    $(ui.item).find('.resort-row1').find('.resort-details').find('.r-left').find('.book-add').css('display', 'block');
                    $("#rhaa1").sortable('cancel');
                    var notify_alert = noty({
                        layout: 'topRight',
                        text: 'Exceeds day time limit,cannot be added..',
                        type: 'warning',
                        timeout: 3000,
                        maxVisible: 1,
                        animation: {
                            open: { height: 'toggle' },
                            close: { height: 'toggle' },
                            easing: 'swing',
                            speed: 500
                        }
                    });
                }
                else if (FitOrNot) {
                    if ($("#prefer").html() == "Hotel Preferences") {
                        if (chck_num_of_hotels > 1) {
                            noty({
                                layout: 'topRight',
                                type: 'confirm',
                                maxVisible: 1,
                                modal: true,
                                text: 'Already a hotel is available for this day, do you still want to add? (If \'yes\' please remove the unwanted hotel manually..)',
                                buttons: [
                                    {
                                        addClass: 'btn btn-primary', text: 'Yes', onClick: function ($noty) {
                                            //change_to_plc = "Yes";
                                            $noty.close();
                                            //change_all_to_place(de);
                                            if ($(ui.item).parent(".place-box").length) {
                                                var ifslsrev = 0;
                                                if ($(ui.item).parent().attr('id') == "" || $(ui.item).parent().attr('id') == null || $(ui.item).parent().attr('id') == "undefined") {
                                                    $(ui.item).parent().replaceWith($(ui.item));
                                                }
                                                travelmode = "";

                                                // on drop if the element is placed below a travel mode stripe..
                                                if ($(ui.item).prev(".travel").length) {
                                                    var orgina = $(ui.item).prev(".travel").prev(".place:first").find(".place-details").find(".p-left").find("h4").text();
                                                    prev_lat_sort1 = $(ui.item).prev(".travel").prev(".place:first").find(".hidlat").val();
                                                    prev_lng_sort1 = $(ui.item).prev(".travel").prev(".place:first").find(".hidlng").val();
                                                    next_lat_sort1 = $(ui.item).find(".reslat").val();
                                                    next_lng_sort1 = $(ui.item).find(".reslng").val();
                                                    var destin = $(ui.item).find(".resort-row1").find(".r-left").find("h3").text();
                                                    var org_id = $(ui.item).prev(".travel").prev(".place:first").find(".place-details").attr('id');
                                                    var destval = $(ui.item).find(".resort-row1").attr('id');
                                                    var destsp = destval.split('_');
                                                    var tm_id = "tm" + org_id.substring(0, 6) + "_" + destsp[1].substring(0, 6);
                                                    $(ui.item).prev(".travel").attr('id', tm_id);
                                                    travelmodeid1 = tm_id;
                                                    Loadtravelduration_sort1(orgina, destin);
                                                }
                                                    // on drop if the element is placed below a place item..
                                                else if ($(ui.item).prev(".place").length) {
                                                    if ($(ui.item).prev(".place").find('.p-left').find('span:first').html() != "Custom place.") {
                                                        var orgina = $(ui.item).prev(".place:first").find(".place-details").find(".p-left").find("h4").text();
                                                        prev_lat_sort1 = $(ui.item).prev(".place:first").find(".hidlat").val();
                                                        prev_lng_sort1 = $(ui.item).prev(".place:first").find(".hidlng").val();
                                                        next_lat_sort1 = $(ui.item).find(".reslat").val();
                                                        next_lng_sort1 = $(ui.item).find(".reslng").val();
                                                        var destin = $(ui.item).find(".resort-row1").find(".r-left").find("h3").text();
                                                        var org_id = $(ui.item).prev(".place:first").find(".place-details").attr('id');
                                                        var destval = $(ui.item).find(".resort-row1").attr('id');
                                                        var destsp = destval.split('_');
                                                        var tm_id = "tm" + org_id.substring(0, 6) + "_" + destsp[1].substring(0, 6);

                                                        var travelModediv = "<div class='travel' id='" + tm_id + "'><a href='#' style='float:right;'><img src='../Content/images/del_travel.png' class='travedelimg'/></a><p></p>";
                                                        travelModediv += "<ul><li><input type='hidden' class='hidcar' value=''/><a href='#' data-value='car' class='active'><img src='../Content/images/car-icon.png' alt=''/></a></li>";
                                                        travelModediv += "<li><input type='hidden' class='hidtrain' value=''/><a href='#' data-value='train'><img src='../Content/images/train-icon.png' alt='' /></a></li>";
                                                        travelModediv += "<li><input type='hidden' class='hidplane' value=''/><a href='#' data-value='plane'><img src='../Content/images/plane-icon.png' alt='' /></a></li>";
                                                        travelModediv += "<li><input type='hidden' class='hidwalk' value=''/><a href='#' data-value='walk'><img src='../Content/images/walk-icon.png' alt='' /></a></li>";
                                                        travelModediv += "<li class='last'><input type='hidden' class='hidcycle' value=''/><a href='#' data-value='bicycle'><img src='../Content/images/scooter-icon.png' alt='' /></a></li>";
                                                        travelModediv += "<input type='hidden' class='hiddriving' value=''/></ul></div>";

                                                        $(ui.item).before($(travelModediv));

                                                        //$(ui.item).before($('<div class="travel" id="' + tm_id + '" style="border-radius:8px;"><a href="#" style="float:right;"><img src="../Content/images/del_travel.png" class="travedelimg"/></a><p></p><ul><li><a href="#" data-value="car" class="active"><img src="../Content/images/car-icon.png" alt=""/></a></li><li><a href="#" data-value="train"><img src="../Content/images/train-icon.png" alt="" /></a></li><li><a href="#" data-value="plane"><img src="../Content/images/plane-icon.png" alt="" /></a></li><li><a href="#" data-value="walk"><img src="../Content/images/walk-icon.png" alt="" /></a></li><li class="last"><a href="#" data-value="bicycle"><img src="../Content/images/scooter-icon.png" alt="" /></a></li><input type="hidden" class="hiddriving" value=""/></ul></div>'));
                                                        travelmodeid1 = tm_id;
                                                        Loadtravelduration_sort1(orgina, destin);
                                                    }
                                                }

                                                if ($(ui.item).next(".travel").length) {
                                                    var destin = $(ui.item).next(".travel").next(".place:first").find(".place-details").find(".p-left").find("h4").text();
                                                    var orgina = $(ui.item).find(".resort-row1").find(".r-left").find("h3").text();
                                                    prev_lat_sort2 = $(ui.item).next(".travel").next(".place:first").find(".hidlat").val();
                                                    prev_lng_sort2 = $(ui.item).next(".travel").next(".place:first").find(".hidlng").val();
                                                    next_lat_sort2 = $(ui.item).find(".reslat").val();
                                                    next_lng_sort2 = $(ui.item).find(".reslng").val();
                                                    var dest_id = $(ui.item).next(".travel").next(".place:first").find(".place-details").attr('id');
                                                    var orival = $(ui.item).find(".resort-row1").attr('id');
                                                    var orisp = orival.split('_');
                                                    var tm_id = "tm" + orisp[1].substring(0, 6) + "_" + dest_id.substring(0, 6);
                                                    $(ui.item).next(".travel").attr('id', tm_id);
                                                    travelmodeid2 = tm_id;
                                                    setTimeout(function () { Loadtravelduration_sort2(orgina, destin) }, 2000);
                                                }
                                                    // on drop if the element is placed below a place item..
                                                else if ($(ui.item).next(".place").length) {
                                                    if ($(ui.item).next(".place").find('.p-left').find('span:first').html() != "Custom place.") {
                                                        var destin = $(ui.item).next(".place:first").find(".place-details").find(".p-left").find("h4").text();
                                                        var orgina = $(ui.item).find(".resort-row1").find(".r-left").find("h3").text();
                                                        prev_lat_sort2 = $(ui.item).next(".place:first").find(".hidlat").val();
                                                        prev_lng_sort2 = $(ui.item).next(".place:first").find(".hidlng").val();
                                                        next_lat_sort2 = $(ui.item).find(".reslat").val();
                                                        next_lng_sort2 = $(ui.item).find(".reslng").val();
                                                        var dest_id = $(ui.item).next(".place:first").find(".place-details").attr('id');
                                                        var orival = $(ui.item).find(".resort-row1").attr('id');
                                                        var orisp = orival.split('_');
                                                        var tm_id = "tm" + orisp[1].substring(0, 6) + "_" + dest_id.substring(0, 6);

                                                        var travelModediv = "<div class='travel' id='" + tm_id + "'><a href='#' style='float:right;'><img src='../Content/images/del_travel.png' class='travedelimg'/></a><p></p>";
                                                        travelModediv += "<ul><li><input type='hidden' class='hidcar' value=''/><a href='#' data-value='car' class='active'><img src='../Content/images/car-icon.png' alt=''/></a></li>";
                                                        travelModediv += "<li><input type='hidden' class='hidtrain' value=''/><a href='#' data-value='train'><img src='../Content/images/train-icon.png' alt='' /></a></li>";
                                                        travelModediv += "<li><input type='hidden' class='hidplane' value=''/><a href='#' data-value='plane'><img src='../Content/images/plane-icon.png' alt='' /></a></li>";
                                                        travelModediv += "<li><input type='hidden' class='hidwalk' value=''/><a href='#' data-value='walk'><img src='../Content/images/walk-icon.png' alt='' /></a></li>";
                                                        travelModediv += "<li class='last'><input type='hidden' class='hidcycle' value=''/><a href='#' data-value='bicycle'><img src='../Content/images/scooter-icon.png' alt='' /></a></li>";
                                                        travelModediv += "<input type='hidden' class='hiddriving' value=''/></ul></div>";
                                                        $(ui.item).after($(travelModediv));

                                                        //$(ui.item).after($('<div class="travel" id="' + tm_id + '" style="border-radius:8px;"><a href="#" style="float:right;"><img src="../Content/images/del_travel.png" class="travedelimg"/></a><p></p><ul><li><a href="#" data-value="car" class="active"><img src="../Content/images/car-icon.png" alt=""/></a></li><li><a href="#" data-value="train"><img src="../Content/images/train-icon.png" alt="" /></a></li><li><a href="#" data-value="plane"><img src="../Content/images/plane-icon.png" alt="" /></a></li><li><a href="#" data-value="walk"><img src="../Content/images/walk-icon.png" alt="" /></a></li><li class="last"><a href="#" data-value="bicycle"><img src="../Content/images/scooter-icon.png" alt="" /></a></li><input type="hidden" class="hiddriving" value=""/></ul></div>'));
                                                        travelmodeid2 = tm_id;
                                                        setTimeout(function () { Loadtravelduration_sort2(orgina, destin) }, 2000);
                                                    }
                                                }
                                                else if ($(ui.item).next(".place-box").length) {
                                                    $(ui.item).nextUntil(".place", ".place-box").each(function () {
                                                        $(this).remove();
                                                    });
                                                    if ($(ui.item).next(".place").find('.p-left').find('span:first').html() != "Custom place.") {
                                                        var destin = $(ui.item).next(".place:first").find(".place-details").find(".p-left").find("h4").text();
                                                        var orgina = $(ui.item).find(".resort-row1").find(".r-left").find("h3").text();
                                                        prev_lat_sort2 = $(ui.item).next(".place:first").find(".hidlat").val();
                                                        prev_lng_sort2 = $(ui.item).next(".place:first").find(".hidlng").val();
                                                        next_lat_sort2 = $(ui.item).find(".reslat").val();
                                                        next_lng_sort2 = $(ui.item).find(".reslng").val();
                                                        var dest_id = $(ui.item).next(".place:first").find(".place-details").attr('id');
                                                        var orival = $(ui.item).find(".resort-row1").attr('id');
                                                        var orisp = orival.split('_');
                                                        var tm_id = "tm" + orisp[1].substring(0, 6) + "_" + dest_id.substring(0, 6);

                                                        var travelModediv = "<div class='travel' id='" + tm_id + "'><a href='#' style='float:right;'><img src='../Content/images/del_travel.png' class='travedelimg'/></a><p></p>";
                                                        travelModediv += "<ul><li><input type='hidden' class='hidcar' value=''/><a href='#' data-value='car' class='active'><img src='../Content/images/car-icon.png' alt=''/></a></li>";
                                                        travelModediv += "<li><input type='hidden' class='hidtrain' value=''/><a href='#' data-value='train'><img src='../Content/images/train-icon.png' alt='' /></a></li>";
                                                        travelModediv += "<li><input type='hidden' class='hidplane' value=''/><a href='#' data-value='plane'><img src='../Content/images/plane-icon.png' alt='' /></a></li>";
                                                        travelModediv += "<li><input type='hidden' class='hidwalk' value=''/><a href='#' data-value='walk'><img src='../Content/images/walk-icon.png' alt='' /></a></li>";
                                                        travelModediv += "<li class='last'><input type='hidden' class='hidcycle' value=''/><a href='#' data-value='bicycle'><img src='../Content/images/scooter-icon.png' alt='' /></a></li>";
                                                        travelModediv += "<input type='hidden' class='hiddriving' value=''/></ul></div>";

                                                        $(ui.item).after($(travelModediv));

                                                        //$(ui.item).after($('<div class="travel" id="' + tm_id + '" style="border-radius:8px;"><a href="#" style="float:right;"><img src="../Content/images/del_travel.png" class="travedelimg"/></a><p></p><ul><li><a href="#" data-value="car" class="active"><img src="../Content/images/car-icon.png" alt=""/></a></li><li><a href="#" data-value="train"><img src="../Content/images/train-icon.png" alt="" /></a></li><li><a href="#" data-value="plane"><img src="../Content/images/plane-icon.png" alt="" /></a></li><li><a href="#" data-value="walk"><img src="../Content/images/walk-icon.png" alt="" /></a></li><li class="last"><a href="#" data-value="bicycle"><img src="../Content/images/scooter-icon.png" alt="" /></a></li><input type="hidden" class="hiddriving" value=""/></ul></div>'));
                                                        travelmodeid2 = tm_id;
                                                        setTimeout(function () { Loadtravelduration_sort2(orgina, destin) }, 2000);
                                                    }
                                                }
                                                if (prevResortId != null && prevResortId != undefined && prevResortId != "") {
                                                    $('#rhaa1').find("#" + prevResortId).after(e);
                                                }
                                                else {
                                                    $('#rhaa1').find("aside:first").before(e);
                                                }
                                                //$('#rhaa1').find(".resort-row:first").after(e);
                                                $('#rhaa1').find("#divres_" + forappendid_onsort).wrap("<aside class='resort-row' id='res_" + forappendid_onsort + "'></aside>")
                                                $('#rhaa1').find("#res_" + forappendid_onsort).find(".resort-details").find(".add-trip").html("In the trip");
                                                $('#rhaa1').find("#res_" + forappendid_onsort).find(".resort-details").find(".book").parent().css('display', 'block');
                                                $('#rhaa1').find("#res_" + forappendid_onsort).addClass("resort-row2").removeClass("resort-row");

                                                $('.del-show').on('click', function () {
                                                    var placeid1 = "";
                                                    $(this).closest('.resort-row').fadeOut('slow', function () {
                                                        placeid1 = $(this).attr('id');
                                                        $(this).hide();
                                                        if (($(this).prev('.travel').length) && ($(this).next('.travel').length)) {
                                                            travelmodeid = $(this).next('.travel').attr('id');
                                                            var orgina = "", destin = "";
                                                            if ($(this).prev('.travel').prev('.place').length) {
                                                                orgina = $(this).prev(".travel").prev(".place:first").find(".place-details").find(".p-left").find("h4").text();
                                                            }
                                                            else if ($(this).prev('.travel').prev(".resort-row").length) {
                                                                orgina = $(this).prev('.travel').prev(".resort-row:first").find(".resort-details").find(".r-left").find("h3").text();
                                                            }
                                                            if ($(this).next('.travel').next('.place').length) {
                                                                destin = $(this).next('.travel').next(".place:first").find(".place-details").find(".p-left").find("h4").text();
                                                            }
                                                            else if ($(this).next('.travel').next(".resort-row").length) {
                                                                orgina = $(this).next('.travel').next(".resort-row:first").find(".resort-details").find(".r-left").find("h3").text();
                                                            }
                                                            LoadsdTravels(orgina, destin);
                                                            $(this).prev('.travel').remove();
                                                        }
                                                        else if (($(this).prev('.travel').length)) {
                                                            $(this).prev('.travel').remove();
                                                        }
                                                        else if (($(this).next('.travel').length)) {
                                                            $(this).next('.travel').remove();
                                                        }
                                                        $(this).remove();
                                                        var splitplaceid1 = placeid1.split('_');
                                                        var sorthelphsp = $("#sorthelp").val().split('_');
                                                        var sorthelpfval = "", sorthelpfaval = "";
                                                        var hacheck = 0;
                                                        for (var loopval = 0; loopval < sorthelphsp.length; loopval++) {
                                                            if (sorthelphsp[loopval] != splitplaceid1[1]) {
                                                                sorthelpfval += sorthelphsp[loopval] + "_";
                                                            }
                                                        }
                                                        $("#sorthelp").val(sorthelpfval);

                                                        $('#rhaa1 .resort-row2').each(function () {
                                                            var thisid = $(this).attr('id');
                                                            var thisidsp = $(this).attr('id').split('_');
                                                            if (splitplaceid1[1] === thisidsp[1]) {
                                                                $(this).find('.resort-details').find('.book-add').find('a').text("Add trip");
                                                                $(this).addClass("resort-row").removeClass("resort-row2");
                                                                hacheck = 1;
                                                            }
                                                        });

                                                        if (hacheck == 0) {
                                                            var sorthelpasp = $("#sorthelpact").val().split('_');
                                                            for (var loopval1 = 0; loopval1 < sorthelphsp.length; loopval1++) {
                                                                if (sorthelpasp[loopval1] != splitplaceid1[1]) {
                                                                    sorthelpfaval += sorthelpasp[loopval1] + "_";
                                                                }
                                                            }
                                                            $("#sorthelpact").val(sorthelpfaval);

                                                            $('#rhaa1 .resort-row2').each(function () {
                                                                var thisid = $(this).attr('id');
                                                                var thisidsp = $(this).attr('id').split('_');
                                                                if (splitplaceid1[1] === thisidsp[1]) {
                                                                    $(this).find('.resort-row1').find('.resort-details').find('.book-add').find('a').html("Add trip");
                                                                    $(this).addClass("resort-row").removeClass("resort-row2");
                                                                    hacheck = 1;
                                                                }
                                                            });
                                                        }
                                                    });
                                                });
                                                change_all_to_place(de);
                                            }
                                        }
                                    },
                                    {
                                        addClass: 'btn btn-danger', text: 'No', onClick: function ($noty) {
                                            $noty.close();

                                            $(ui.item).css("width", "");
                                            $("#rhaa1").sortable('cancel');
                                            $("#" + plcbxrit_id + " .travel").each(function () {
                                                if ($(this).prev().hasClass('travel'))
                                                    $(this).remove();
                                            });

                                            $("#rhaa1 .resort-row2").each(function () {
                                                if ($(this).attr('id') == ui_itemid) {
                                                    $(this).remove();
                                                }
                                            });
                                        }
                                    }
                                ]
                            });
                        }
                    }
                    else if ($(ui.item).parent(".place-box").length) {
                        var acceptState = 0;
                        var plcTyp = $("#prefer").html();
                        if (plcTyp == "Attraction Preferences" || plcTyp == "Restaurant Preferences") {
                            var tripStrtDt = $("#TripStrtDate").val();
                            var daynum1 = 0, daynum2 = 0;
                            $(".place-box-right").each(function () {
                                if ($(this).attr('id') == stopplcbxrit) {
                                    daynum2 = daynum1;
                                }
                                daynum1++;
                            });
                            var getNumdays1 = daynum2;
                            var getdate1 = new Date(tripStrtDt);
                            var newdate = new Date(getdate1);
                            newdate.setDate(newdate.getDate() + getNumdays1);
                            var getday1 = newdate.getDay();
                            var uiplcID = forappendid_onsort;
                            var uiItemPos = $(ui.item).position().top;
                            var findtime = Math.ceil((uiItemPos / 55) / 5) * 5;
                            var actStTime = parseInt(timingArr[findtime]) - 2;
                            var actEnTime = timingArr[findtime];
                            var DayExists1 = false;
                            var DayExists2 = false;
                            var strtTime = "";
                            var clsTime = "";
                            var timeDet = [];

                            //debugger
                            //alert(plcTyp);
                            if (plcTyp == "Attraction Preferences") {
                                for (var loopval = 0, looplen = attrDetails.length; loopval < looplen; loopval++) {
                                    if (attrDetails[loopval].plc_id == uiplcID) {
                                        if (attrDetails[loopval].details.opening_hours) {
                                            timeDet = "No time";
                                            //timeDet = attrDetails[loopval].details.opening_hours.periods;     //commented by aes93 since periods is not defined                                  
                                        }
                                        else {
                                            timeDet = "No time";
                                        }
                                        break;
                                    }
                                }
                            }
                            else if (plcTyp == "Restaurant Preferences") {
                                for (var loopval = 0, looplen = restDetails.length; loopval < looplen; loopval++) {
                                    if (restDetails[loopval].plc_id == uiplcID) {
                                        if (restDetails[loopval].details.opening_hours) {

                                            timeDet = "No time";                          
                                            //timeDet = restDetails[loopval].details.opening_hours.periods;  //commented by aes93 since periods is not defined            
                                        }
                                        else {
                                            timeDet = "No time";
                                        }
                                        break;
                                    }
                                }
                            }
                            else if (plcTyp == "Activity Preferences") {
                                timeDet = "No time";
                            }

                            else if (plcTyp == "Hotel") {
                                timeDet = "No time";
                            }


                            if (timeDet != "No time") {
                                for (var dayIndx = 0, dayArrLen = timeDet.length; dayIndx < dayArrLen; dayIndx++) {
                                    if (timeDet[dayIndx].open) {
                                        if (timeDet[dayIndx].open.day == getday1) {
                                            strtTime = timeDet[dayIndx].open.hours;
                                            DayExists1 = true;
                                        }
                                    }
                                    if (timeDet[dayIndx].close) {
                                        if (timeDet[dayIndx].close.day == getday1) {
                                            clsTime = timeDet[dayIndx].close.hours;
                                            DayExists2 = true;
                                        }
                                    }
                                    if (!DayExists1) {
                                        strtTime = "25";
                                    }
                                    if (!DayExists2) {
                                        clsTime = "0";
                                    }
                                }
                            }
                            else {
                                strtTime = "6";
                                clsTime = "23";
                            }
                            if (parseInt(strtTime) > parseInt(actStTime) || parseInt(clsTime) < parseInt(actEnTime)) {
                                var answer = confirm("The spot is closed at this time. Do you still want to place it in this time?");
                                if (!answer) {
                                    acceptState = 1;
                                }
                            }
                        }
                        var ifslsrev = 0;
                        travelmode = "";
                        if (acceptState == 0) {
                            if ($(ui.item).parent().attr('id') == "" || $(ui.item).parent().attr('id') == null || $(ui.item).parent().attr('id') == "undefined") {
                                $(ui.item).parent().replaceWith($(ui.item));
                            }
                            if ($(ui.item).prev(".travel").length) {
                                var orgina = $(ui.item).prev(".travel").prev(".place:first").find(".place-details").find(".p-left").find("h4").text();
                                prev_lat_sort1 = $(ui.item).prev(".travel").prev(".place:first").find(".hidlat").val();
                                prev_lng_sort1 = $(ui.item).prev(".travel").prev(".place:first").find(".hidlng").val();
                                next_lat_sort1 = $(ui.item).find(".reslat").val();
                                next_lng_sort1 = $(ui.item).find(".reslng").val();
                                var destin = $(ui.item).find(".resort-row1").find(".r-left").find("h3").text();
                                var org_id = $(ui.item).prev(".travel").prev(".place:first").find(".place-details").attr('id');
                                var destval = $(ui.item).find(".resort-row1").attr('id');
                                var destsp = destval.split('_');
                                var tm_id = "tm" + org_id.substring(0, 6) + "_" + destsp[1].substring(0, 6);
                                $(ui.item).prev(".travel").attr('id', tm_id);
                                travelmodeid1 = tm_id;
                                Loadtravelduration_sort1(orgina, destin);
                            }
                            else if ($(ui.item).prev(".place").length) {
                                if ($(ui.item).prev(".place").find('.p-left').find('span:first').html() != "Custom place.") {
                                    var orgina = $(ui.item).prev(".place:first").find(".place-details").find(".p-left").find("h4").text();
                                    prev_lat_sort1 = $(ui.item).prev(".place:first").find(".hidlat").val();
                                    prev_lng_sort1 = $(ui.item).prev(".place:first").find(".hidlng").val();
                                    next_lat_sort1 = $(ui.item).find(".reslat").val();
                                    next_lng_sort1 = $(ui.item).find(".reslng").val();
                                    var destin = $(ui.item).find(".resort-row1").find(".r-left").find("h3").text();
                                    var org_id = $(ui.item).prev(".place:first").find(".place-details").attr('id');
                                    var destval = $(ui.item).find(".resort-row1").attr('id');
                                    var destsp = destval.split('_');
                                    var tm_id = "tm" + org_id.substring(0, 6) + "_" + destsp[1].substring(0, 6);
                                    var travelModediv = "<div class='travel' id='" + tm_id + "'><a href='#' style='float:right;'><img src='../Content/images/del_travel.png' class='travedelimg'/></a><p></p>";
                                    travelModediv += "<ul><li><input type='hidden' class='hidcar' value=''/><a href='#' data-value='car' class='active'><img src='../Content/images/car-icon.png' alt=''/></a></li>";
                                    travelModediv += "<li><input type='hidden' class='hidtrain' value=''/><a href='#' data-value='train'><img src='../Content/images/train-icon.png' alt='' /></a></li>";
                                    travelModediv += "<li><input type='hidden' class='hidplane' value=''/><a href='#' data-value='plane'><img src='../Content/images/plane-icon.png' alt='' /></a></li>";
                                    travelModediv += "<li><input type='hidden' class='hidwalk' value=''/><a href='#' data-value='walk'><img src='../Content/images/walk-icon.png' alt='' /></a></li>";
                                    travelModediv += "<li class='last'><input type='hidden' class='hidcycle' value=''/><a href='#' data-value='bicycle'><img src='../Content/images/scooter-icon.png' alt='' /></a></li>";
                                    travelModediv += "<input type='hidden' class='hiddriving' value=''/></ul></div>";
                                    $(ui.item).before($(travelModediv));
                                    travelmodeid1 = tm_id;
                                    Loadtravelduration_sort1(orgina, destin);
                                }
                            }
                            if ($(ui.item).next(".travel").length) {
                                var destin = $(ui.item).next(".travel").next(".place:first").find(".place-details").find(".p-left").find("h4").text();
                                var orgina = $(ui.item).find(".resort-row1").find(".r-left").find("h3").text();
                                prev_lat_sort2 = $(ui.item).next(".travel").next(".place:first").find(".hidlat").val();
                                prev_lng_sort2 = $(ui.item).next(".travel").next(".place:first").find(".hidlng").val();
                                next_lat_sort2 = $(ui.item).find(".reslat").val();
                                next_lng_sort2 = $(ui.item).find(".reslng").val();
                                var dest_id = $(ui.item).next(".travel").next(".place:first").find(".place-details").attr('id');
                                var orival = $(ui.item).find(".resort-row1").attr('id');
                                var orisp = orival.split('_');
                                var tm_id = "tm" + orisp[1].substring(0, 6) + "_" + dest_id.substring(0, 6);
                                $(ui.item).next(".travel").attr('id', tm_id);
                                travelmodeid2 = tm_id;
                                setTimeout(function () { Loadtravelduration_sort2(orgina, destin) }, 2000);
                            }
                            else if ($(ui.item).next(".place").length) {
                                if ($(ui.item).next(".place").find('.p-left').find('span:first').html() != "Custom place.") {
                                    var destin = $(ui.item).next(".place:first").find(".place-details").find(".p-left").find("h4").text();
                                    var orgina = $(ui.item).find(".resort-row1").find(".r-left").find("h3").text();
                                    prev_lat_sort2 = $(ui.item).next(".place:first").find(".hidlat").val();
                                    prev_lng_sort2 = $(ui.item).next(".place:first").find(".hidlng").val();
                                    next_lat_sort2 = $(ui.item).find(".reslat").val();
                                    next_lng_sort2 = $(ui.item).find(".reslng").val();
                                    var dest_id = $(ui.item).next(".place:first").find(".place-details").attr('id');
                                    var orival = $(ui.item).find(".resort-row1").attr('id');
                                    var orisp = orival.split('_');
                                    var tm_id = "tm" + orisp[1].substring(0, 6) + "_" + dest_id.substring(0, 6);
                                    var travelModediv = "<div class='travel' id='" + tm_id + "'><a href='#' style='float:right;'><img src='../Content/images/del_travel.png' class='travedelimg'/></a><p></p>";
                                    travelModediv += "<ul><li><input type='hidden' class='hidcar' value=''/><a href='#' data-value='car' class='active'><img src='../Content/images/car-icon.png' alt=''/></a></li>";
                                    travelModediv += "<li><input type='hidden' class='hidtrain' value=''/><a href='#' data-value='train'><img src='../Content/images/train-icon.png' alt='' /></a></li>";
                                    travelModediv += "<li><input type='hidden' class='hidplane' value=''/><a href='#' data-value='plane'><img src='../Content/images/plane-icon.png' alt='' /></a></li>";
                                    travelModediv += "<li><input type='hidden' class='hidwalk' value=''/><a href='#' data-value='walk'><img src='../Content/images/walk-icon.png' alt='' /></a></li>";
                                    travelModediv += "<li class='last'><input type='hidden' class='hidcycle' value=''/><a href='#' data-value='bicycle'><img src='../Content/images/scooter-icon.png' alt='' /></a></li>";
                                    travelModediv += "<input type='hidden' class='hiddriving' value=''/></ul></div>";
                                    $(ui.item).after($(travelModediv));
                                    travelmodeid2 = tm_id;
                                    setTimeout(function () { Loadtravelduration_sort2(orgina, destin) }, 2000);
                                }
                            }
                            else if ($(ui.item).next(".place-box").length) {
                                $(ui.item).nextUntil(".place", ".place-box").each(function () {
                                    $(this).remove();
                                });
                                if ($(ui.item).next(".place").find('.p-left').find('span:first').html() != "Custom place.") {
                                    var destin = $(ui.item).next(".place:first").find(".place-details").find(".p-left").find("h4").text();
                                    var orgina = $(ui.item).find(".resort-row1").find(".r-left").find("h3").text();
                                    prev_lat_sort2 = $(ui.item).next(".place:first").find(".hidlat").val();
                                    prev_lng_sort2 = $(ui.item).next(".place:first").find(".hidlng").val();
                                    next_lat_sort2 = $(ui.item).find(".reslat").val();
                                    next_lng_sort2 = $(ui.item).find(".reslng").val();
                                    var dest_id = $(ui.item).next(".place:first").find(".place-details").attr('id');
                                    var orival = $(ui.item).find(".resort-row1").attr('id');
                                    var orisp = orival.split('_');
                                    var tm_id = "tm" + orisp[1].substring(0, 6) + "_" + dest_id.substring(0, 6);
                                    var travelModediv = "<div class='travel' id='" + tm_id + "'><a href='#' style='float:right;'><img src='../Content/images/del_travel.png' class='travedelimg'/></a><p></p>";
                                    travelModediv += "<ul><li><input type='hidden' class='hidcar' value=''/><a href='#' data-value='car' class='active'><img src='../Content/images/car-icon.png' alt=''/></a></li>";
                                    travelModediv += "<li><input type='hidden' class='hidtrain' value=''/><a href='#' data-value='train'><img src='../Content/images/train-icon.png' alt='' /></a></li>";
                                    travelModediv += "<li><input type='hidden' class='hidplane' value=''/><a href='#' data-value='plane'><img src='../Content/images/plane-icon.png' alt='' /></a></li>";
                                    travelModediv += "<li><input type='hidden' class='hidwalk' value=''/><a href='#' data-value='walk'><img src='../Content/images/walk-icon.png' alt='' /></a></li>";
                                    travelModediv += "<li class='last'><input type='hidden' class='hidcycle' value=''/><a href='#' data-value='bicycle'><img src='../Content/images/scooter-icon.png' alt='' /></a></li>";
                                    travelModediv += "<input type='hidden' class='hiddriving' value=''/></ul></div>";
                                    $(ui.item).after($(travelModediv));
                                    travelmodeid2 = tm_id;
                                    setTimeout(function () { Loadtravelduration_sort2(orgina, destin) }, 2000);
                                }
                            }
                            if (prevResortId != null && prevResortId != undefined && prevResortId != "") {
                                $('#rhaa1').find("#" + prevResortId).after(e);
                            }
                            else {
                                $('#rhaa1').find("aside:first").before(e);
                            }
                            $('#rhaa1').find("#divres_" + forappendid_onsort).wrap("<aside class='resort-row' id='res_" + forappendid_onsort + "'></aside>")
                            $('#rhaa1').find("#res_" + forappendid_onsort).find(".resort-details").find(".add-trip").html("In the trip");
                            $('#rhaa1').find("#res_" + forappendid_onsort).addClass("resort-row2").removeClass("resort-row");
                            change_all_to_place(de);
                        }
                        else {
                            $(ui.item).css({ 'width': '622px' });
                            $(this).sortable('cancel');
                        }
                    }
                }
            }
        }
    }
});

function LoadssdTravels(ori, des) {
    var origin = ori,
   destination = des,
   dservice = new google.maps.DistanceMatrixService();
    dservice.getDistanceMatrix(
         {
             origins: [origin],
             destinations: [destination],
             travelMode: google.maps.TravelMode.DRIVING,
             avoidHighways: false,
             avoidTolls: false
         }, callbackd1tm);
}

function callbackd1tm(response, status) {
    if (status === "OK") {
        var dest = response.destinationAddresses[0];
        var orig = response.originAddresses[0];
        var dist = "", dura = "";
        if (response.rows[0].elements[0].distance) {
            dist = response.rows[0].elements[0].distance.text;
            dura = response.rows[0].elements[0].duration.text;
            var splitTraveltime = dura.split(' ');
            for (var loopthr = 0, TimeTravelFind = splitTraveltime.length; loopthr < TimeTravelFind; loopthr++) {
                if (splitTraveltime[loopthr] == "min" || splitTraveltime[loopthr] == "mins") {
                    splitTraveltime[loopthr - 1] = Math.ceil(splitTraveltime[loopthr - 1] / 5) * 5;
                }
            }
            dura = splitTraveltime.join(" ");
            var strdur = "Travel time by " + travemode + " is " + dura;
            $('#' + travelmodeid2).find('p').html(strdur);
            $('#' + travelmodeid2).find(".hidcar").val(dura);
            $('#' + travelmodeid2).find("p").append("<img src='../Content/images/edit.png' alt='' class='editimg' style='height: 15px;width: 15px;'>");
        }
        else {
            callhaversineforsort(2);
        }
    }
    else {
        callhaversineforsort(2);
    }
    var size = $('#compl').find('.travel').length;
    TravelDivResize(size);
}

function LoadsdTravels(ori, des) {
    var origin = ori,
   destination = des,
   dservice = new google.maps.DistanceMatrixService();
    dservice.getDistanceMatrix(
         {
             origins: [origin],
             destinations: [destination],
             travelMode: google.maps.TravelMode.DRIVING,
             avoidHighways: false,
             avoidTolls: false
         }, callbacksdtm);
}

function callbacksdtm(response, status) {
    var dura = "";
    if (status === "OK") {
        var ulitem = "<li><a href='#' data-value='car' class='active'><img src='../Content/images/car-icon.png' alt='' /></a></li>";
        ulitem += "<li><a href='#' data-value='train'><img src='../Content/images/train-icon.png' alt='' /></a></li>";
        ulitem += "<li><a href='#' data-value='plane'><img src='../Content/images/plane-icon.png' alt='' /></a></li>";
        ulitem += "<li><a href='#' data-value='walk'><img src='../Content/images/walk-icon.png' alt='' /></a></li>";
        ulitem += "<li class='last'><a href='#' data-value='bicycle'><img src='../Content/images/scooter-icon.png' alt='' /></a></li>";
        ulitem += "<input type='hidden' class='hiddriving'/>";
        $('#' + travelmodeid1).find("ul").html(ulitem);
        if (response.rows[0].elements[0].duration) {
            dura = response.rows[0].elements[0].duration.text;
            var splitTraveltime = dura.split(' ');
            for (var loopthr = 0, TimeTravelFind = splitTraveltime.length; loopthr < TimeTravelFind; loopthr++) {
                if (splitTraveltime[loopthr] == "min" || splitTraveltime[loopthr] == "mins") {
                    splitTraveltime[loopthr - 1] = Math.ceil(splitTraveltime[loopthr - 1] / 5) * 5;
                }
            }
            dura = splitTraveltime.join(" ");
            $('#' + travelmodeid1).find(".hidcar").val(dura);
            $('#' + travelmodeid1).find("p").text("Time taken by car is " + dura);
            $('#' + travelmodeid1).find("p").append("<img src='../Content/images/edit.png' alt='' class='editimg' style='height: 15px;width: 15px;'>");
        }
        else {
            callhaversineforsort(1);
        }
        incre = incre + 1;
    }
    else {
        callhaversineforsort(1);
    }
    var size = $('#compl').find('.travel').length;
    TravelDivResize(size);
    var customTravelsize = $('#compl').find('.custom_travel').length;
    CustomTravelDivResize(customTravelsize);
}

function Loadtravelduration_sort1(ori, des) {
    var origin = ori,
   destination = des,
   dservice = new google.maps.DistanceMatrixService();
    dservice.getDistanceMatrix(
         {
             origins: [origin],
             destinations: [destination],
             travelMode: google.maps.TravelMode.DRIVING,
             avoidHighways: false,
             avoidTolls: false
         }, callback_sort1);
}

function callback_sort1(response, status) {
    var dest = "", orig = "", dist = "", dura = "";
    if (status === "OK") {
        if (response.rows[0].elements[0].distance) {
            dist = response.rows[0].elements[0].distance.text;
            dura = response.rows[0].elements[0].duration.text;
            var splitTraveltime = dura.split(' ');
            for (var loopthr = 0, TravelTimeFind = splitTraveltime.length; loopthr < TravelTimeFind; loopthr++) {
                if (splitTraveltime[loopthr] == "min" || splitTraveltime[loopthr] == "mins") {
                    splitTraveltime[loopthr - 1] = Math.ceil(splitTraveltime[loopthr - 1] / 5) * 5;
                }
            }
            dura = splitTraveltime.join(" ");
            $('#' + travelmodeid1).find("p").text("Time taken by car is " + dura);
            $('#' + travelmodeid2).find(".hidcar").val(dura);
            $('#' + travelmodeid1).find("p").append("<img src='../Content/images/edit.png' alt='' class='editimg' style='height: 15px;width: 15px;'>");
        }
        else {
            callhaversineforsort(1);
        }
        $('#' + travelmodeid1 + ' li a').on('click', function () {
            previous_travelmode = $(this).parent().parent().parent().find('.active').data('value');
            previous_travelduration = $(this).parent().parent().parent().find('p').html();
            previous_height = $(this).parent().parent().parent().height();
            var mode = $(this).data('value');
            var travel_id = $(this).parent().parent().parent().attr('id');
            $('#' + travel_id + ' li a').removeClass('active');
            $(this).addClass('active');
            $('#' + travel_id).find('p').html("<img src='../Content/images/edit.png' alt='' class='editimg' style='height: 15px;width: 15px;'>");
            travelmodeid = travel_id;
            travemode = mode;
            traveldur();
        });
    }
    else {
        callhaversineforsort(1);
    }
    var size = $('#compl').find('.travel').length;
    TravelDivResize(size);
    var customTravelsize = $('#compl').find('.custom_travel').length;
    CustomTravelDivResize(customTravelsize);
}

function Loadtravelduration_sort2(ori, des) {
    var origin = ori,
   destination = des,
   dservice = new google.maps.DistanceMatrixService();
    dservice.getDistanceMatrix(
         {
             origins: [origin],
             destinations: [destination],
             travelMode: google.maps.TravelMode.DRIVING,
             avoidHighways: false,
             avoidTolls: false
         }, callback_sort2);
}

function callback_sort2(response, status) {
    var dest = "", orig = "", dist = "", dura = "";
    if (status === "OK") {
        if (response.rows[0].elements[0].distance) {
            dist = response.rows[0].elements[0].distance.text;
            dura = response.rows[0].elements[0].duration.text;
            var splitTraveltime = dura.split(' ');
            for (var loopthr = 0, TimeTravelFind = splitTraveltime.length; loopthr < TimeTravelFind; loopthr++) {
                if (splitTraveltime[loopthr] == "min" || splitTraveltime[loopthr] == "mins") {
                    splitTraveltime[loopthr - 1] = Math.ceil(splitTraveltime[loopthr - 1] / 5) * 5;
                }
            }
            dura = splitTraveltime.join(" ");
            $('#' + travelmodeid2).find("p").text("Time taken by car is " + dura);
            $('#' + travelmodeid2).find(".hidcar").val(dura);
            $('#' + travelmodeid2).find("p").append("<img src='../Content/images/edit.png' alt='' class='editimg' style='height: 15px;width: 15px;'>");
        }
        else {
            callhaversineforsort(2);
        }
        $('#' + travelmodeid2 + ' li a').on('click', function () {
            previous_travelmode = $(this).parent().parent().parent().find('.active').data('value');
            previous_travelduration = $(this).parent().parent().parent().find('p').html();
            previous_height = $(this).parent().parent().parent().height();
            var mode = $(this).data('value');
            var travel_id = $(this).parent().parent().parent().attr('id');
            $('#' + travel_id + ' li a').removeClass('active');
            $(this).addClass('active');
            $('#' + travel_id).find('p').html("<img src='../Content/images/edit.png' alt='' class='editimg' style='height: 15px;width: 15px;'>");
            travelmodeid = travel_id;
            travemode = mode;
            traveldur();
        });
    }
    else {
        callhaversineforsort(2);
    }
    var size = $('#compl').find('.travel').length;
    TravelDivResize(size);
    var customTravelsize = $('#compl').find('.custom_travel').length;
    CustomTravelDivResize(customTravelsize);
}

function callhaversineforsort(TravelmodeBefOrAft) {
    var dista = "";
    if (TravelmodeBefOrAft == 1) {
        dista = Haversine(prev_lat_sort1, prev_lng_sort1, next_lat_sort1, next_lng_sort1);
    }
    else if (TravelmodeBefOrAft == 2) {
        dista = Haversine(prev_lat_sort2, prev_lng_sort2, next_lat_sort2, next_lng_sort2);
    }
    var kmtomin1 = dista / 70;
    var kmtomin2 = kmtomin1 * 60;
    var kmtomin3 = kmtomin2.toFixed();
    var final_time = "";
    if (isNaN(kmtomin3)) {
        kmtomin3 = 15;
    }
    if (kmtomin3 > 60) {
        var kmtohrs = Math.floor(kmtomin3 / 60);
        var kmtomints = kmtomin3 % 60;
        if (kmtohrs > 1 && kmtohrs <= 2) {
            if (kmtohrs == 1) {
                kmtomints = Math.ceil(kmtomints / 5) * 5;
                if (kmtomints == 60) {
                    kmtohrs = kmtohrs + 1;
                    final_time = kmtohrs + " hours";
                }
                else {
                    final_time = kmtohrs + " hour " + kmtomints + " mins ";
                }
            }
            else if (kmtohrs == 2) {
                kmtomints = Math.ceil(kmtomints / 5) * 5;
                if (kmtomints == 60) {
                    kmtohrs = kmtohrs + 1;
                    final_time = kmtohrs + " hours";
                }
                else {
                    final_time = kmtohrs + " hours " + kmtomints + " mins ";
                }
            }
        }
            //else if (kmtohrs > 2) {
            //    final_time = "1 hour";
            //}
        else {
            kmtomints = Math.ceil(kmtomints / 5) * 5;
            final_time = kmtohrs + " hours " + kmtomints + " mins ";
        }
    }
    else {
        if (kmtomin3 <= 1) {
            kmtomin3 = kmtomin3 + 4;
        }
        kmtomin3 = Math.ceil(kmtomin3 / 5) * 5;
        final_time = parseInt(kmtomin3) + " mins ";
    }
    var setTravelModeId = "";
    if (TravelmodeBefOrAft == 1) {
        setTravelModeId = travelmodeid1;
    }
    else if (TravelmodeBefOrAft == 2) {
        setTravelModeId = travelmodeid2;
    }
    $("#" + setTravelModeId + ' li a').removeClass('active');
    $("#" + setTravelModeId).find('li a[data-value=car]').addClass('active');
    $("#" + setTravelModeId).find("p").html('Time taken by car is ' + final_time);
    $("#" + setTravelModeId).find("p").append(' <img src="../Content/images/edit.png" alt="" class="editimg" style="height: 15px;width: 15px;">');
    $("#" + setTravelModeId).find(".hidcar").val(final_time);
}