﻿function GetUsrpreferenceAttr() {
    var filtattcat = "", filtcntatt = 0;
    oldatt_actarray.length = 0, oldotherattraction.length = 0;
    att_actarray.length = 0, otherattraction.length = 0, secondClassAttr.length = 0;
    attus_arr_len = 0, attus_arr_len1 = 0;
    $.getJSON('../Trip/Getuserpreference', function (data) {
        $.each(data, function (key, values) {
            var useratt = (values.attraction).toString();
            user_attr_pref = (values.attraction).toString();
            user_act = (values.activity).toString();
            user_rest = (values.cuisine).toString();
            user_numofdays = (values.No_of_days).toString();
            if (values.Hotel_class != null && values.Hotel_class != "") {
                user_hotelcls = (values.Hotel_class).toString();
            }
            else {
                user_hotelcls = "";
            }
            if (values.star != null && values.star != "") {
                user_hotelrating = (values.star).toString();
            }
            else {
                user_hotelrating = "";
            }
            user_hStartprice = (values.startprice).toString();
            user_hEndprice = (values.endprice).toString();
            if (useratt != "Nooption") {
                var spatt_new = useratt.split(',');
                var attractionLength = spatt_new.length;
                attus_arr_len = 1;
                var increCnt = 1;
                var newarrayTmp = "";
                var pushToArray = [];
                for (var loopvar = 0; loopvar < attractionLength; loopvar++) {
                    pushToArray.push(spatt_new[loopvar]);
                }
                newarrayTmp = pushToArray.join("|");
                Attractionfunctioncall(newarrayTmp);
            }
            else {
                var spatt_new = [];
                var increCnt = 0;
                spatt_new.push("aquarium", "place_of_worship", "museum", "art_gallery", "amusement_park", "park");
                var attractionLength = spatt_new.length;
                attus_arr_len = Math.ceil(attractionLength / 6);
                var newarrayTmp = "";
                for (var loopvar = 0; loopvar < attractionLength; loopvar += 6) {
                    increCnt++;
                    if (increCnt < attus_arr_len || attractionLength % 6 == 0) {
                        newarrayTmp = spatt_new[loopvar + 5] + "|" + spatt_new[loopvar + 4] + "|" + spatt_new[loopvar + 3] + "|" + spatt_new[loopvar + 2] + "|" + spatt_new[loopvar + 1] + "|" + spatt_new[loopvar];
                        Attractionfunctioncall(newarrayTmp);
                    }
                }
                var spatt_new1 = [];
                increCnt = 0;
                spatt_new1.push("palace", "botanical garden", "stadium", "bridge", "cathedral", "museum", "beach", "island", "zoo", "square", "statue", "harbour", "pier", "tower", "historical building");
                attractionLength = spatt_new1.length;
                attus_arr_len1 = Math.ceil(attractionLength / 5);
                for (var loopvar = 0; loopvar < attractionLength; loopvar += 5) {
                    increCnt++;
                    if (increCnt < attus_arr_len1 || attractionLength % 5 == 0) {
                        newarrayTmp = "(" + spatt_new1[loopvar + 4] + " OR " + spatt_new1[loopvar + 3] + " OR " + spatt_new1[loopvar + 2] + " OR " + spatt_new1[loopvar + 1] + " OR " + spatt_new1[loopvar] + ")";
                        Attractionfunctioncall1(newarrayTmp);
                    }
                }
            }
        });
    });
}

function Attractionfunctioncall(att_category) {
    var pyrmont = new google.maps.LatLng($('#tn_1lat').val(), $('#tn_1lng').val());
    map = new google.maps.Map(document.getElementById('map-canvas'), {
        center: pyrmont,
        zoom: 15
    });
    var request = {
        location: pyrmont,
        radius: 20000,
        rankby: ['prominence'],
        types: [att_category]
    };
    var service = new google.maps.places.PlacesService(map);
    service.nearbySearch(request, function (response, status) { callbackattraction(response, status, att_category) });
}

function callbackattraction(results, status, att_category) {
    if (status == google.maps.places.PlacesServiceStatus.OK) {
        var needsExclusions = [];
        for (var loopvar = 0, ResultsLength = results.length; loopvar < ResultsLength ; loopvar++) {
            if (results[loopvar].types) {
                needsExclusions = results[loopvar].types;
                someFlag = true;
                for (var index1 = 0; index1 < excludeMe.length; index1++) {
                    if (needsExclusions.indexOf(excludeMe[index1]) !== -1) {
                        someFlag = false;
                        break;
                    }
                }
                if (someFlag) {
                    if ((results[loopvar].name).toLowerCase().indexOf("college") >= 0) {
                        someFlag = false;
                    }
                }
                if (someFlag && results[loopvar].rating) {
                    if (results[loopvar].rating >= 1) {
                        AllAttrCollect.push(results[loopvar]);
                        attraction_array.push(results[loopvar]);
                    }
                }
            }
        }
    }
    attus_arr_len--;
    if (attus_arr_len == 0 && attus_arr_len1 == 0) {
        TotalAttrs = AllAttrCollect.length;
        GetAllAttrDet();
        attrStatus = 1;
    }
}

function Attractionfunctioncall1(att_category1) {
    var pyrmont = new google.maps.LatLng($('#tn_1lat').val(), $('#tn_1lng').val());
    map = new google.maps.Map(document.getElementById('map-canvas'), {
        center: pyrmont,
        zoom: 15
    });
    var request = {
        location: pyrmont,
        radius: 20000,
        rankby: ['prominence'],
        keyword: [att_category1]
    };
    var service = new google.maps.places.PlacesService(map);
    service.nearbySearch(request, function (response, status) { callbackattraction1(response, status, att_category1) });
}

function callbackattraction1(results, status, att_category1) {
    if (status == google.maps.places.PlacesServiceStatus.OK) {
        var needsExclusions = [];
        for (var loopvar = 0, ResultsLength = results.length; loopvar < ResultsLength; loopvar++) {
            if (results[loopvar].types) {
                needsExclusions = results[loopvar].types;
                someFlag = true;
                for (var index1 = 0; index1 < excludeMe.length; index1++) {
                    if (needsExclusions.indexOf(excludeMe[index1]) !== -1) {
                        someFlag = false;
                        break;
                    }
                }
                if (someFlag) {
                    if ((results[loopvar].name).toLowerCase().indexOf("college") >= 0) {
                        someFlag = false;
                    }
                }
                if (someFlag) {
                    if (results[loopvar].rating) {
                        if (results[loopvar].rating >= 1) {
                            AllAttrCollect.push(results[loopvar]);
                            attraction_array.push(results[loopvar]);
                        }
                    }
                }
            }
        }
    }
    attus_arr_len1--;
    if (attus_arr_len1 == 0 && attus_arr_len == 0) {
        attrStatus = 1;
        TotalAttrs = AllAttrCollect.length;
        GetAllAttrDet();
    }
}

