﻿var dayslen = 0;
var sortedarrayall = [], att_actarray = [], activitiesfull_array = [],activitiesCopy=[];
var rest_array = [], restlist_array = [], attraction_array = [], hotel_arrayfull = [];
var hotel_array = [], otherattraction = [], latcheck = [], lngcheck = [], otherrestaurant = [];
var user_numofdays = 0, incr_td = 0, array_lengthfrtraveldura = 0, ishotel_avail = 0;
var user_hotelrating = "", user_hStartprice = "", user_hEndprice = "", user_attr_pref = "", user_act = "", user_rest = "";
var attus_arr_len = 0, attus_arr_len1 = 0, resus_arr_len = 0;
var forfirsttimetravelduration = [], traveldurationarray = [];
var got_hotelResult = 0;
var hotelsForCities = [], hotelArrayForFilter = [], finalfilterArrayfull = [], CopyOfHotels = [];
var Rest_excludeMe = ["clothing_store", "store", "library", "bank", "finance", "embassy"];
var Rest_includeMe = ["food", "restaurant"];
var excludeMe = ["lodging", "night_club", "bar", "food", "general_contractor", "home_goods_store", "dentist", "finance", "store", "restaurant", "parking", "local_government_office", "health", "lawyer", "travel_agency", "beauty_salon", "bakery", "clothing_store", "spa", "gym", "pet_store", "university","school","locality","political","airport"];
var someFlag = true;
var oldatt_actarray = [], oldotherattraction = [];
var secondClassAttr = [],thirdClassAttr=[];
var attrStatus = 0, restStatus = 0;
var ListAttrArr = [];
var TripStartDate = "", TripStartDay = "";
var TotalAttrs = 0, incrAttrs = 0;
var AllAttrCollect = [];
var TotalRests = 0, AllRestCollect = [], incrRests = 0, oldrest_array = [];
var prev_tm_id_del = "", next_tm_id_del = "", current_plc_id_del = "";
var gotActivities = 0;

jQuery(document).ready(function ($) {
    var fancybx1 = $('.fancybox').fancybox({
        keys: {
            close: [null] 
        }
    });
    
    $(".accordian-popup").resp_Accordion();
    $("#datepicker").datepicker({
        minDate: '1d',
        maxDate: "+6M",
        dateFormat: 'mm/dd/yy',
        changeYear: true
    });
    $("#slider-range1").slider({
        range: true,
        min: 10,
        max: 3000,
        values: [10, 3000],
        stop: function (event, ui) {
            var price1 = null;
            var price2 = null;
            price1 = "$" + ui.values[0];
            price2 = "$" + ui.values[1];
            $("#txtprice").val("$" + $("#slider-range1").slider("values", 0) + " - $" + $("#slider-range1").slider("values", 1));
        }
    });
    $("#txtprice").val("$" + $("#slider-range1").slider("values", 0) + " - $" + $("#slider-range1").slider("values", 1));
    $(".place").addClass("ui-widget ui-widget-content ui-helper-clearfix ui-corner-all").find(".place-details").addClass("ui-widget-header ui-corner-all");
    $(".portlet-toggle").click(function () {
        var icon = $(this);
        icon.toggleClass("ui-icon-minusthick ui-icon-plusthick");
        icon.closest(".place").find(".travel").toggle();
    });        
    LoadRestaurantPref();
    $("#welcome").trigger("click");
    $("#datepicker").val("");
    $("#noofdays").val("");
    if ($("#IsfrmTopItin").val() == '1') {
        var PlanId = $("#TopItinryId").val();
        var CityName = "";
        var CityLatitude = "";
        var CityLongitude = "";
        var CityImage = "";
        var PlanName = "";
        var PlanDesc = "";
        var NumOfDays = 0;
        $.getJSON('../CMSPage/ItineraryEdit', { ItinID: PlanId }, function (response) {
            if (response != "No Result") {
                $.each(response, function (keys, values) {
                    CityName = values.CityName;
                    CityLatitude = values.CityLatitude;
                    CityLongitude = values.CityLongitude;
                    CityImage = values.CityImage;
                    PlanName = values.PlanName;
                    PlanDesc = values.PlanDescription;
                    NumOfDays = values.No_of_days;
                });
                $("#noofdays").val(NumOfDays);
                $('#noofdays').attr('readonly', true);
            }
        });
    }
    $("#wel-mem").hide();
    $("#liMyAccount").show();
    $('#Filteredbyid').append($('<option></option>').val("0").html("Select"));
    $('#Sortbyid').append($('<option></option>').val("0").html("Select"));
    if ($.trim($('[id$=lblusername]').text()) == "") {
        $('#LoggedUserLi').hide();
        $('#LogdUsrMyTrip').hide();
        return false;
    }
    else {
        $("#wel-mem").show();
        $('#LoggedUserLi').show();
        $('#LogdUsrMyTrip').show();
        $("#hidnavigate").val("1");
        $("#hidcityid").val("1");
        $("#hidcustomizeid").val("1");
        $("#liMyAccount").hide();
        notificationCount();
        return false;
    }
    $("#hotel-dropdown-img").click(function () {
        $(".dropdown-pad").slideToggle();
    });
    $("#overlay").click(function () {
        $(".overlay").hide();
    });
    var citynameact = $("#selectedcity").html();
    var sm = citynameact.split(',');
    $('#rhaa1').html('loading..');
    //$('#datepicker').focus();
    $.getJSON('../Trip/GetDestinationPlace', {}, function (data) {
        $.each(data, function (keys, datas) {
            var HtmlData = '';
            HtmlData += "<li style='background-image:none'><a href='#' style='text-overflow: ellipsis;white-space: nowrap;overflow: hidden;width:87px;' title='" + datas.cityname + "' class='ignore_click' onclick=\'AddedinCityAlign(\"" + datas.lat + "\",\"" + datas.lng + "\",\"" + datas.cityname + "\");\'><img src='../Content/images/CityImages/" + datas.Photo + "' alt='' style='width: 83px;height:83px;' class='ignore_click' />" + datas.cityname + " </a></li>";
            var Id = datas.customplace;
            Id = Id.replace(/\s/g, '');
            $('#' + Id).append(HtmlData);
        });
    });
});

$(document).ready("popup.html #register", function () {
    var notify_alert = noty({
        layout: 'topRight',
        text: 'Load was performed.',
        type: 'success',
        timeout: 3000,
        maxVisible: 1,
        animation: {
            open: { height: 'toggle' },
            close: { height: 'toggle' },
            easing: 'swing',
            speed: 500
        }
    });
});

(function ($) {
    $(window).load(function () {
        $("#content-x").mCustomScrollbar({
            axis: "x",
            theme: "3d",
            scrollInertia: 550,
            scrollbarPosition: "outside"
        });
        $("#content-y").mCustomScrollbar({
            axis: "y",
            theme: "3d",
            scrollInertia: 550,
            scrollbarPosition: "outside"
        });
        var widthToset = 0;
        $('.city-row').each(function (increDays) {
            widthToset = increDays;
        });
        widthToset = widthToset + 1;
        if (widthToset > 2) {
            var numberOfDays = widthToset * 370;
            $(".mCSB_container").css('width', + 'px');
        }
    });
})(jQuery);

$(document).keypress(function (event) {
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if (keycode == '13') {
        var a = $('#inputField').val();
        var listItems = $("#citypref").children();
        var count = listItems.length;
        test = $('#preftext').attr('value');
        var htmlData = '<li><a id=' + count + ' href="" onclick="return getcity(' + count + ')" ></a></li>';
        $('#citypref').append(htmlData);
        $('#' + count).html(a);
        $('#inputField').val("");
        $('#box').hide();
    }
});

$("#noofdays").keypress(function (e) {
   
    var maxlen = 2;
    var text = $(this).val();
    var textLength = text.length;
    if (textLength == 0) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            var noty_alert = noty({
                layout: 'topRight',
                text: 'Field should not be empty',
                type: 'warning',
                timeout: 2000,
                maxVisible: 1,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                }
            });
            $("#noofdays").val('');
            dayslen = 0;
            return false;
        }
    }
    else if (textLength < maxlen) {
        if (e.which != 8 && e.which != 0 && e.which != 13 && (e.which < 48 || e.which > 57)) {
            var noty_alert = noty({
                layout: 'topRight',
                text: 'Numbers Only',
                type: 'warning',
                timeout: 2000,
                maxVisible: 1,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                }
            });
            $("#noofdays").val('');
            dayslen = 0;
            return false;
        }
    }
    else {
        var noty_alert = noty({
            layout: 'topRight',
            text: 'Number of Days should be less than 15',
            type: 'warning',
            timeout: 2000,
            maxVisible: 1,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            }
        });
        $("#noofdays").val('');
        dayslen = 0;
        return false;
    }
});

$('#txtregfirstname').keyup(function (e) {
    var maxlen = 15;
    var text = $(this).val();
    var textLength = text.length;
    if (textLength < maxlen) {
        if (!((e.which == 8) || (e.which == 32) || (e.which == 46) || (e.which >= 35 && e.which <= 40) || (e.which >= 65 && e.which <= 90))) {
            $('#txtregfirstname').val('');
            $("#errms").html("Alphabets Only").show().fadeOut(2500);
            return false;
        }
    }
    else {
        $('#txtregfirstname').val('');
        $("#errms").html("Exceeds maximum length").show().fadeOut(2500);
        return false;
    }
});

$('#txtreglastname').keyup(function (e) {
    var maxlen = 15;
    var text = $(this).val();
    var textLength = text.length;
    if (textLength < maxlen) {
        if (!((e.which == 8) || (e.which == 32) || (e.which == 46) || (e.which >= 35 && e.which <= 40) || (e.which >= 65 && e.which <= 90))) {
            $('#txtreglastname').val('');
            $("#errms1").html("Alphabets Only").show().fadeOut(2500);
            return false;
        }
    }
    else {
        $('#txtreglastname').val('');
        $("#errms1").html("Exceeds maximum length").show().fadeOut(2500);
        return false;
    }
});

$("#txtreglastname").change(function () {
    var LastName = $("#txtreglastname").val().length;
    if (LastName <= 1) {
        var noty_alert = noty({
            layout: 'topRight',
            text: 'Last name should contain more than one character!',
            type: 'warning',
            timeout: 3000,
            maxVisible: 1,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            }
        });
        $('[id$=txtreglastname]').focus();
        $('[id$=txtreglastname]').val("");
        return false;
    }
});

$("#txtregfirstname").change(function () {
    var LastName = $("#txtregfirstname").val().length;
    if (LastName <= 1) {
        var noty_alert = noty({
            layout: 'topRight',
            text: 'First name should contain more than one character!',
            type: 'warning',
            timeout: 3000,
            maxVisible: 1,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            }
        });
        $('[id$=txtregfirstname]').focus();
        $('[id$=txtregfirstname]').val("");
        return false;
    }
});

$("#txtregpwd").change(function () {
    var regPassword = $("#txtregpwd").val();
    if (!MatchPassword(regPassword)) {
        var noty_alert = noty({
            layout: 'topRight',
            text: 'Password should contain alphabets and numbers and should have minimum 7 characters!',
            type: 'warning',
            timeout: 3000,
            maxVisible: 1,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            }
        });
        $('[id$=txtregpwd]').focus();
        $('[id$=txtregpwd]').val("");
        return false;
    }
});

$("#btnregclear").on('click', function () {
    $("#txtregfirstname").val('');
    $("#txtreglastname").val('');
    $("#txtregmail").val('');
    $("#txtregpwd").val('');
    return false;
});

$("#txtfbregmail").change(function () {
    var EmailTxt = $('#txtfbregmail').val().toLowerCase();
    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    if (filter.test(EmailTxt)) {
        $.getJSON('../Trip/CheckEmailStatus', { Email: EmailTxt }, function (data) {
            if (data == "1") {
                var noty_alert = noty({
                    layout: 'topRight',
                    text: 'Email Id Already Exist',
                    type: 'warning',
                    timeout: 3000,
                    maxVisible: 1,
                    animation: {
                        open: { height: 'toggle' },
                        close: { height: 'toggle' },
                        easing: 'swing',
                        speed: 500
                    }
                });
                $('#txtfbregmail').focus();
                $('#txtfbregmail').val("");
                return false;
            }
            else if (data == "2") {
                var noty_alert = noty({
                    layout: 'topRight',
                    text: 'Your account is deactivated. Please contact 1eztrip support team..',
                    type: 'warning',
                    timeout: 3000,
                    maxVisible: 1,
                    animation: {
                        open: { height: 'toggle' },
                        close: { height: 'toggle' },
                        easing: 'swing',
                        speed: 500
                    }
                });
                $('#txtfbregmail').focus();
                $('#txtfbregmail').val("");
                return false;
            }
        });
    }
    else {
        var noty_alert = noty({
            layout: 'topRight',
            text: 'In-Valid Email address..',
            type: 'warning',
            timeout: 3000,
            maxVisible: 1,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            }
        });
        $('#txtfbregmail').focus();
        $('#txtfbregmail').val("");
        return false;
    }
});

$("#btnfbregclear").on('click', function () {
    $("#txtfbregmail").val('');
    $("#txtfbregpwd").val('');
    $("#txtfbregfirstname").val('');
    $("#txtfbreglastname").val('');
    $('[id$=txtfbregconpwd]').val("");
    return false;
});

$("#btnfbregsubmit").click(function () {
    if (($.trim($('[id$=txtfbregmail]').val() == "")) && ($.trim($('[id$=txtfbregpwd]').val() == "")) && ($.trim($('[id$=txtfbregfirstname]').val() == "")) && ($.trim($('[id$=txtfbreglastname]').val() == ""))) {
        if ($.trim($('[id$=txtfbregfirstname]').val()) == "") {
            var noty_alert = noty({
                layout: 'topRight',
                text: 'Please enter first name!',
                type: 'warning',
                timeout: 3000,
                maxVisible: 1,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                }
            });
            $('[id$=txtfbregfirstname]').focus();
            $('[id$=txtfbregfirstname]').val("");
            return false;
        }
        if ($.trim($('[id$=txtfbreglastname]').val()) == "") {
            var noty_alert = noty({
                layout: 'topRight',
                text: 'Please enter last name!',
                type: 'warning',
                timeout: 3000,
                maxVisible: 1,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                }
            });
            $('[id$=txtfbreglastname]').focus();
            $('[id$=txtfbreglastname]').val("");
            return false;
        }
        if ($.trim($('[id$=txtfbregmail]').val()) == "") {
            var noty_alert = noty({
                layout: 'topRight',
                text: 'Please enter Email Id!',
                type: 'warning',
                timeout: 3000,
                maxVisible: 1,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                }
            });
            $('[id$=txtfbregmail]').focus();
            $('[id$=txtfbregmail]').val("");
            return false;
        }
        if ($.trim($('[id$=txtfbregpwd]').val()) == "") {
            var noty_alert = noty({
                layout: 'topRight',
                text: 'Please enter password',
                type: 'warning',
                timeout: 3000,
                maxVisible: 1,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                }
            });
            $('[id$=txtfbregpwd]').focus();
            $('[id$=txtfbregpwd]').val("");
            return false;
        }
        if ($.trim($('[id$=txtfbregconpwd]').val()) == "") {
            var noty_alert = noty({
                layout: 'topRight',
                text: 'Please enter conform password',
                type: 'warning',
                timeout: 3000,
                maxVisible: 1,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                }
            });
            $('[id$=txtfbregconpwd]').focus();
            $('[id$=txtfbregconpwd]').val("");
            return false;
        }
        if ($.trim($('[id$=txtfbregpwd]').val()) != $.trim($('[id$=txtfbregconpwd]').val())) {
            var noty_alert = noty({
                layout: 'topRight',
                text: 'Password Not Matched..',
                type: 'warning',
                timeout: 3000,
                maxVisible: 1,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                }
            });
            $('[id$=txtfbregpwd]').focus();
            $('[id$=txtfbregconpwd]').val("");
            return false;
        }
    }
    var dataObject = {
        mail: $("#txtfbregmail").val().toLowerCase(),
        password: $("#txtfbregpwd").val(),
        firstname: $("#txtfbregfirstname").val(),
        lastname: $("#txtfbreglastname").val(),
        FacebookId: $('#FacebookId').val()
    };
    $.ajax({
        url: '../Trip/Save',
        type: "POST",
        data: dataObject,
        dataType: "json",
        success: function (result) {
            var url = '../Trip/IntimateConfirmEmail';
            window.location.href = url;
        },
        error: function () {
            var noty_alert = noty({
                layout: 'topRight',
                text: 'Not registered, please try once again..',
                type: 'warning',
                timeout: 3000,
                maxVisible: 1,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                }
            });
        }
    });
    $("#txtfbregmail").val('');
    $("#txtfbregpwd").val('');
    $("#txtfbregfirstname").val('');
    $("#txtfbreglastname").val('');
    $('[id$=txtfbregconpwd]').val("");
    return false;
});

$('#txtloginpwd').keypress(function (e) {
    var key = e.which;
    if (key == 13) {
        $('input[id = btnlogin]').click();
        return false;
    }
});

$("#btnpwdclear").on('click', function () {
    $("#txtloginmail").val('');
    $("#txtloginpwd").val('');
});

$('#daycr').on('click', function () {
    //alert("neftdaycr");
    //
    TotalAttrs = 0;
    incrAttrs = 0;
    var favorite_cuisine = [];
    var cuisine = "";
    $.each($("input[name='cuisine']:checked"), function () {
        favorite_cuisine.push($(this).val());
    });
    cuisine = favorite_cuisine.join(",");
    $("#htnrestaurant").val(cuisine);
    var favorite_attraction = [];
    var attraction = "";
    $.each($("input[name='Attraction']:checked"), function () {
        favorite_attraction.push($(this).val());
    });
    attraction = favorite_attraction.join(",");
    $("#htnattraction").val(attraction);
    var favorite_hc = [];
    var hcstar = "";
    $.each($("input[name='hclass']:checked"), function () {
        favorite_hc.push($(this).val());
    });
    hcstar = favorite_hc.join(",");
    $("#htnhc").val(hcstar);
    var favorite_rating = [];
    var star = "";
    $.each($("input[name='star']:checked"), function () {
        favorite_rating.push($(this).val());
    });
    star = favorite_rating.join(",");
    $("#htnstar").val(star);
    var favorite_activiy = [];
    var activiy = "";
    $.each($("input[name='Activity']:checked"), function () {
        favorite_activiy.push($(this).val());
    });
    activiy = favorite_activiy.join(",");
    $("#hidactivity").val(activiy);
    var slival1 = $("#slider-range1").slider("values", 0);
    var slival2 = $("#slider-range1").slider("values", 1);
    $("div.place").remove();
    $("div.travel").remove();
    sortedarrayall.length = 0;
    var popstar = $("#htnstar").val();
    var rest_cuisine = $("#htnrestaurant").val();
    var attr_opt = $("#htnattraction").val();
    var acti_opt = $("#hidactivity").val();
    var hcls_star = $("#htnhc").val();
    if (rest_cuisine == "") {
        $("#htnrestaurant").val("Nooption");
    }
    if (attr_opt == "") {
        $("#htnattraction").val("Nooption");
    }
    if (acti_opt == "") {
        $("#hidactivity").val("Nooption");
    }
    var noof = $("#noofdays").val();
    var dtVal = $('#datepicker').val();
    var travel_dates = new String(dtVal).split("/", 3);
    var travel_date = new Date(travel_dates[2], travel_dates[0]-1, travel_dates[1]);
    var Current_Date = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
    var travelEnd_date = new Date(Current_Date);
    var travelEndAfter_6 = new Date(new Date(travelEnd_date).setMonth(travelEnd_date.getMonth() + 6));
    var idate = document.getElementById("datepicker");
    if ($('#datepicker').val() == '') {
        var notify_alert = noty({
            layout: 'topRight',
            text: 'Enter Date of Travel',
            type: 'warning',
            timeout: 2000,
            maxVisible: 1,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            }
        });
        return false;
    }
    else if (travel_date < Current_Date) {
        var notify_alert = noty({
            layout: 'topRight',
            text: 'Invalid date. Please enter future date',
            type: 'warning',
            timeout: 2000,
            maxVisible: 1,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            }
        });
        $('#datepicker').focus();
        return false;        
    }
    else if (travel_date > travelEndAfter_6) {
        var notify_alert = noty({
            layout: 'topRight',
            text: 'Invalid date. Please enter date within 6 months from today..',
            type: 'warning',
            timeout: 2000,
            maxVisible: 1,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            }
        });
        $('#datepicker').focus();
        return false;
    }
    else if (!(ValidateDate(dtVal))) {
        var notify_alert = noty({
            layout: 'topRight',
            text: 'Invalid date(mm/dd/yyyy)',
            type: 'warning',
            timeout: 2000,
            maxVisible: 1,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            }
        });
        return false;
    }
    else if (!(isFutureDate(idate.value))) {
        var notify_alert = noty({
            layout: 'topRight',
            text: 'Invalid date. Please enter future date',
            type: 'warning',
            timeout: 2000,
            maxVisible: 1,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            }
        });
        return false;
    }
    else if (noof == "") {
        var notify_alert = noty({
            layout: 'topRight',
            text: 'Enter Number of Days for your travel',
            type: 'warning',
            timeout: 2000,
            maxVisible: 1,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            }
        });
        $("#noofdays").focus();
        return false;
    }
    else if (noof > 15) {
        var notify_alert = noty({
            layout: 'topRight',
            text: 'Please enter number of days within 15 days',
            type: 'warning',
            timeout: 2000,
            maxVisible: 1,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            }
        });
        $("#noofdays").val('');
        $("#noofdays").focus();
        return false;
    }
    else if (noof < 1) {
        var notify_alert = noty({
            layout: 'topRight',
            text: 'Please select number of days greater than 0',
            type: 'warning',
            timeout: 2000,
            maxVisible: 1,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            }
        });
        $("#noofdays").val('');
        $("#noofdays").focus();
        return false;
    }
    else {
        $.fancybox.close();
        $('#LeftSidePart').block({
            message: 'Please wait while your itinerary is being created...',
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#033363',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });
        if (noof == 0) {
            noof = 1;
        }
        TripStartDate = dtVal;
        var dateParts = TripStartDate.split("/");
        var datenew = new Date(dateParts[2], dateParts[0] - 1, dateParts[1]);
        var dayFound = datenew.getDay();
        TripStartDay = dayFound;
        $.getJSON('../Trip/UpdateDate', { noday: noof, TripStartDate: dtVal }, function (data) {
            if ($("#priceValue").val() > 0) {
                GetHotels();
                UserPreferenceActivities();
            }
        });
        var NewData = {
            noday: noof,
            StartPrice: $("#slider-range1").slider("values", 0),
            EndPrice: $("#slider-range1").slider("values", 1),
            Star: $("#htnstar").val(),
            Hotel_class: $("#htnhc").val(),
            Cuisine: $("#htnrestaurant").val(),
            Attraction: $("#htnattraction").val(),
            Activity: $("#hidactivity").val()
        };
        $.ajax({
            url: '../Trip/Preference',
            type: 'POST',
            data: {
                d: NewData
            },
            success: function (data) {
                $("#htnstar").val("");
                $("#htnrestaurant").val("");
                $("#htnattraction").val("");
                //$("#hidactivity").val("");
                $("#htnhc").val("");
                if ($("#priceValue").val() > 0) {
                    if ($("#IsfrmTopItin").val() == '0') {
                        UserpreferenceAttraction();
                    }
                    else if ($("#IsfrmTopItin").val() == '1') {
                        var PlanId = $("#TopItinryId").val();
                        if (PlanId.indexOf("1EzTrip") < 0) {
                            var days_num = "", plcs_ids = "", plcs_names = "", plcs_lat = "", plcs_lons = "", plcs_addr = "", plcs_rat = "", plcs_types = "", plcs_imgs = "", tm_type = "", tm_dur = "";
                            $.getJSON('../CMSPage/GetPackage', { PlanID: PlanId }, function (response) {
                                if (response.data != "No Result") {
                                    packagedata = response;
                                    $.each(packagedata, function (keys, values) {
                                        plcs_ids = values.Spot_Id.split('||');
                                        plcs_names = values.Spot_Name.split('||');
                                        plcs_lat = values.Spot_Latitude.split('||');
                                        plcs_lons = values.Spot_Longitude.split('||');
                                        plcs_addr = values.Spot_Address.split('||');
                                        plcs_rat = values.Spot_Rating.split('||');
                                        plcs_types = values.Spot_Category.split('||');
                                        plcs_imgs = values.thumbnail_URL.split('|image|');
                                        tm_type = values.Travel_Mode.split('||');
                                        tm_dur = values.Travel_time.split('||');
                                        days_num = values.Day_Number;
                                        var itineraryData = "";
                                        for (var loopvar = 0, PlacesLength = plcs_ids.length; loopvar < PlacesLength; loopvar++) {
                                            if (plcs_ids[loopvar] != "") {
                                                itineraryData += "<div class='place'><div class='place-details' id=" + plcs_ids[loopvar] + " style ='height:100px;'><figure class='place-img'>";
                                                itineraryData += "<img src='" + plcs_imgs[loopvar] + "' /></figure><div class='place-cont'>";
                                                itineraryData += "<div class='p-left'><h4>" + plcs_names[loopvar] + "</h4><p style='height:26px; overflow-y:hidden;'>" + plcs_addr[loopvar] + "</p><input type='hidden' class='hidlat' value='" + plcs_lat[loopvar] + "'/><input type='hidden' class='hidlng' value='" + plcs_lons[loopvar] + "'/>";
                                                itineraryData += "<span>" + plcs_types[loopvar] + "</span>";
                                                itineraryData += "</div><div class='p-right'>";
                                                itineraryData += "<ul><li><a href='#'><img src='../Content/images/del-icon.png' alt='' onclick='fun1('uniid')' /></a></li><li><a href='#infowindow' class='fancybox' onclick='return infoattraction(\"" + plcs_ids[loopvar] + "\",\"" + plcs_lat[loopvar] + "\",\"" + plcs_lons[loopvar] + "\",\"" + plcs_types[loopvar] + "\");' ><img src='../Content/images/info-icon.png' alt='' /></a></li> </ul>";
                                                itineraryData += "</div></div></div>";
                                                itineraryData += "</div>";
                                                if (tm_type[loopvar].trim() !== "No travelmode") {
                                                    itineraryData += "<div class='travel'><a href='#' style='float:right;'><img src='../Content/images/del_travel.png' class='travedelimg'/></a>";
                                                    itineraryData += "<p>Travel time by " + tm_type[loopvar] + " is " + tm_dur[loopvar] + "<img src='../Content/images/edit.png' alt='' class='editimg' style='height: 15px;width: 15px;'></p>";
                                                    itineraryData += "<ul>";
                                                    if (tm_type[loopvar] == "car") {
                                                        itineraryData += "<li><a href='#' data-value='car' class='active'><img src='../Content/images/car-icon.png' alt=''/></a></li>";
                                                    }
                                                    else {
                                                        itineraryData += "<li><a href='#' data-value='car'><img src='../Content/images/car-icon.png' alt=''/></a></li>";
                                                    }
                                                    if (tm_type[loopvar] == "train") {
                                                        itineraryData += "<li><a href='#' data-value='train' class='active'><img src='../Content/images/train-icon.png' alt='' /></a></li>";
                                                    }
                                                    else {
                                                        itineraryData += "<li><a href='#' data-value='train'><img src='../Content/images/train-icon.png' alt='' /></a></li>";
                                                    }
                                                    if (tm_type[loopvar] == "plane") {
                                                        itineraryData += "<li><a href='#' data-value='plane' class='active'><img src='../Content/images/plane-icon.png' alt='' /></a></li>";
                                                    }
                                                    else {
                                                        itineraryData += "<li><a href='#' data-value='plane'><img src='../Content/images/plane-icon.png' alt='' /></a></li>";
                                                    }
                                                    if (tm_type[loopvar] == "walk") {
                                                        itineraryData += "<li><a href='#' data-value='walk' class='active'><img src='../Content/images/walk-icon.png' alt='' /></a></li>";
                                                    }
                                                    else {
                                                        itineraryData += "<li><a href='#' data-value='walk'><img src='../Content/images/walk-icon.png' alt='' /></a></li>";
                                                    }
                                                    if (tm_type[loopvar] == "bicycle") {
                                                        itineraryData += "<li class='last'><a href='#' data-value='bicycle' class='active'><img src='../Content/images/scooter-icon.png' alt='' /></a></li>";
                                                    }
                                                    else {
                                                        itineraryData += "<li class='last'><a href='#' data-value='bicycle'><img src='../Content/images/scooter-icon.png' alt='' /></a></li>";
                                                    }
                                                    itineraryData += "<input type='hidden' class='hiddriving' value=''/></ul></div>";
                                                }
                                            }
                                        }
                                        $(".place-box-right").eq(values.Day_Number - 1).find('.place-box:first').append(itineraryData);
                                    });
                                    var size = $('#compl').find('.travel').length;
                                    TravelDivResize(size);
                                    $('#LeftSidePart').unblock();
                                    //LoadImageEnd();
                                    UserpreferenceAttraction();
                                }                                
                            });
                        }
                        else
                        {
                            var spltPlanId = PlanId.split("1EzTrip");
                            var TopPlanId = spltPlanId[1];
                            $.getJSON('../trip/RetrievePackage', { TripId: TopPlanId }, function (data) {
                                var itinerary = "";
                                var increTravelMode = 0;
                                var plcs_id_split = [], plcs_names_split = [], plcs_spendtime_split = [], plcs_addr_split = [], plcs_lat_split = [];
                                var plcs_lng_split = [], travelmode_split = [], traveldur_split = [], plc_start_split = [], plc_cate_split = [], plc_thumbnailurl_split = [];
                                $.each(data, function (keys, values) {
                                    plcs_id_split = values.Spot_Id.split('||');
                                    plcs_names_split = values.Spot_Name.split('||');
                                    plcs_spendtime_split = values.Spot_SpendingTime.split('||');
                                    plcs_addr_split = values.Spot_Address.split('||');
                                    plcs_lat_split = values.Spot_Latitude.split('||');
                                    plcs_lng_split = values.Spot_Longitude.split('||');
                                    travelmode_split = values.Travel_Mode.split('||');
                                    traveldur_split = values.Travel_time.split('||');
                                    plc_start_split = values.Visit_StartTime.split('||');/**stored in database as top,left styles**/
                                    plc_cate_split = values.spot_category.split('||');
                                    plc_thumbnailurl_split = values.thumbnail_Url.split('|image_split|')
                                    var att_or_res = 0;                                    
                                    var daynum = values.Day_Number;
                                    var plc_bx_to_append = "";
                                        if ($('.place-box-right').eq(daynum - 1).find('.place-box').attr('id') != null || $('.place-box-right').eq(daynum - 1).find('.place-box').attr('id') != "undefined") {
                                            plc_bx_to_append = $('.place-box-right').eq(daynum - 1).find('.place-box').attr('id');
                                            $("#" + plc_bx_to_append).empty();
                                            for (var loopvar = 0, PlacesLength = plcs_id_split.length; loopvar < PlacesLength; loopvar++) {
                                                if (plcs_id_split[loopvar] == "pl_bx") {
                                                    itinerary = "<div class='place-box timing-details'></div>";
                                                }
                                                else {
                                                    var startingtime_parse = $.parseJSON(plc_start_split[loopvar]);
                                                    itinerary = "<div class='place' id='place" + plcs_id_split[loopvar] + "'><div class='place-details' id=" + plcs_id_split[loopvar] + " style ='height:" + plcs_spendtime_split[loopvar] + "px;'><input type='hidden' value=" + plcs_lat_split[loopvar] + " class='placeatt_lat'/><input type='hidden' value=" + plcs_lng_split[loopvar] + " class='placeatt_lng'/><input type='hidden' value=" + plcs_names_split[loopvar] + " class='placeatt_name'/><figure class='place-img'>";
                                                    itinerary += "<img src='" + plc_thumbnailurl_split[loopvar] + "' alt=''/></figure><div class='place-cont'>";
                                                    itinerary += "<div class='p-left'><h4>" + plcs_names_split[loopvar] + "</h4><p style='height:26px; overflow-y:hidden;'>" + plcs_addr_split[loopvar] + "</p><input type='hidden' class='hidlat' value='" + plcs_lat_split[loopvar] + "'/><input type='hidden' class='hidlng' value='" + plcs_lng_split[loopvar] + "'/><span>" + plc_cate_split[loopvar] + "</span>";
                                                    if (plc_cate_split[loopvar] != "Hotel.") {
                                                        itinerary += "<br><span id='sp_" + plcs_id_split[loopvar] + "' class='sphrs'>";
                                                    }
                                                    if (plc_cate_split[loopvar] == "Hotel.") {
                                                        //var found = jQuery.inArray(plcs_id_split[loopvar], inthetrip_array_hotels);
                                                        //if (found == -1) {
                                                        //    itinerary += "<span id='Plcbook" + plcs_id_split[loopvar] + "' style='display: block;margin-left: 20px;'><a href='#' class='Plcbook'>Book</a></span>";
                                                        //}
                                                    }
                                                    itinerary += "</div><div class='p-right'>";
                                                    itinerary += "<ul><li><a href='#'><img src='../Content/images/del-icon.png' class='deleteplace' alt=''/></a></li>";

                                                    if (plc_cate_split[loopvar] == "Restaurant.") {
                                                        //att_or_res = 1;
                                                        //itinerary += "<li><a href='#infowindow' class='fancybox' onclick='return infoattraction(\"" + plcs_id_split[loopvar] + "\",\"" + plcs_lat_split[loopvar] + "\",\"" + plcs_lng_split[loopvar] + "\",\"" + att_or_res + "\");' ><img src='../Content/images/info-icon.png' alt='' /></a></li> </ul>";
                                                        //var found = jQuery.inArray(plcs_id_split[loopvar], inthetrip_array_restaurants);
                                                        //if (found == -1) {
                                                        //    inthetrip_array_restaurants.push(plcs_id_split[loopvar]);
                                                        //}
                                                    }
                                                    else if (plc_cate_split[loopvar] == "Attraction.") {
                                                        //att_or_res = 2;
                                                        //itinerary += "<li><a href='#infowindow' class='fancybox' onclick='return infoattraction(\"" + plcs_id_split[loopvar] + "\",\"" + plcs_lat_split[loopvar] + "\",\"" + plcs_lng_split[loopvar] + "\",\"" + att_or_res + "\");' ><img src='../Content/images/info-icon.png' alt='' /></a></li> </ul>";
                                                        //var found = jQuery.inArray(plcs_id_split[loopvar], inthetrip_array_attractions);
                                                        //if (found == -1) {
                                                        //    inthetrip_array_attractions.push(plcs_id_split[loopvar]);
                                                        //}
                                                    }
                                                    else if (plc_cate_split[loopvar] == "Hotel.") {
                                                        //itinerary += "<li><a href='#infowindow' class='fancybox' onclick='return infohotel(\"" + plcs_id_split[loopvar] + "\");' ><img src='../Content/images/info-icon.png' alt='' /></a></li> </ul>";
                                                        //var found = jQuery.inArray(plcs_id_split[loopvar], inthetrip_array_hotels);
                                                        //if (found == -1) {
                                                        //    inthetrip_array_hotels.push(plcs_id_split[loopvar]);
                                                        //}
                                                        var CitiesCnt = 0;
                                                        for (var loopvariable = 0, RetrievedHotels = hotels_retrievedCities.length; loopvariable < RetrievedHotels; loopvariable++) {
                                                            if (hotels_retrievedCities[loopvariable].cityname.trim() == values.City_Name.trim()) {
                                                                CitiesCnt++;
                                                            }
                                                        }
                                                        if (CitiesCnt == 0) {
                                                            hotels_retrievedCities.push({
                                                                cityname: values.City_Name,
                                                                hotelId: plcs_id_split[loopvar]
                                                            })
                                                        }
                                                    }
                                                    itinerary += "</div> </div> </div>";
                                                    itinerary += "</div>";
                                                    if (travelmode_split[loopvar].trim() !== "No travelmode") {
                                                        increTravelMode++;
                                                        var getTravelmode = travelmode_split[loopvar];
                                                        var TravelmodeSelected = "";
                                                        if (getTravelmode.indexOf("Cus~") != -1) {
                                                            var GetCustomTravelMode = travelmode_split[loopvar].split('~');
                                                            TravelmodeSelected = GetCustomTravelMode[1];
                                                            itinerary += "<div class='custom_travel' id='ltra_" + increTravelMode + "'>";
                                                        }
                                                        else {
                                                            TravelmodeSelected = travelmode_split[loopvar];
                                                            itinerary += "<div class='travel' id='ltra_" + increTravelMode + "'>";
                                                        }
                                                        itinerary += "<a href='#' style='float:right;'><img src='../Content/images/del_travel.png' class='travedelimg'/></a>";
                                                        itinerary += "<p>Time taken by " + TravelmodeSelected + " is " + traveldur_split[loopvar] + "</p>";
                                                        itinerary += "<ul>";
                                                        if (TravelmodeSelected == "car") {
                                                            itinerary += "<li><input type='hidden' class='hidcar' value='" + traveldur_split[loopvar] + "'/><a href='#' data-value='car' class='active'><img src='../Content/images/car-icon.png' alt=''/></a></li>";
                                                        }
                                                        else {
                                                            itinerary += "<li><input type='hidden' class='hidcar' value=''/><a href='#' data-value='car'><img src='../Content/images/car-icon.png' alt=''/></a></li>";
                                                        }
                                                        if (TravelmodeSelected == "train") {
                                                            itinerary += "<li><input type='hidden' class='hidtrain'  value='" + traveldur_split[loopvar] + "'/><a href='#' data-value='train' class='active'><img src='../Content/images/train-icon.png' alt='' /></a></li>";
                                                        }
                                                        else {
                                                            itinerary += "<li><input type='hidden' class='hidtrain' value=''/><a href='#' data-value='train'><img src='../Content/images/train-icon.png' alt='' /></a></li>";
                                                        }
                                                        if (TravelmodeSelected == "plane") {
                                                            itinerary += "<li><input type='hidden' class='hidplane'  value='" + traveldur_split[loopvar] + "'/><a href='#' data-value='plane' class='active'><img src='../Content/images/plane-icon.png' alt='' /></a></li>";
                                                        }
                                                        else {
                                                            itinerary += "<li><input type='hidden' class='hidplane' value=''/><a href='#' data-value='plane'><img src='../Content/images/plane-icon.png' alt='' /></a></li>";
                                                        }
                                                        if (TravelmodeSelected == "walk") {
                                                            itinerary += "<li><input type='hidden' class='hidwalk'  value='" + traveldur_split[loopvar] + "'/><a href='#' data-value='walk' class='active'><img src='../Content/images/walk-icon.png' alt='' /></a></li>";
                                                        }
                                                        else {
                                                            itinerary += "<li><input type='hidden' class='hidwalk' value=''/><a href='#' data-value='walk'><img src='../Content/images/walk-icon.png' alt='' /></a></li>";
                                                        }
                                                        if (TravelmodeSelected == "bicycle") {
                                                            itinerary += "<li class='last'><input type='hidden' class='hidcycle' value='" + traveldur_split[loopvar] + "'/><a href='#' data-value='bicycle' class='active'><img src='../Content/images/scooter-icon.png' alt='' /></a></li>";
                                                        }
                                                        else {
                                                            itinerary += "<li class='last'><input type='hidden' class='hidcycle' value=''/><a href='#' data-value='bicycle'><img src='../Content/images/scooter-icon.png' alt='' /></a></li>";
                                                        }
                                                        itinerary += "<input type='hidden' class='hiddriving' value=''/>";
                                                        itinerary += "</ul></div>";
                                                    }
                                                }
                                                $("#" + plc_bx_to_append).append(itinerary);
                                            }
                                        }
                                });
                                var size = $('#compl').find('.travel').length;
                                TravelDivResize(size);
                                $('#LeftSidePart').unblock();
                                //LoadImageEnd();
                                UserpreferenceAttraction();
                            });
                        }
                    }
                }
                else {
                    $('#overlay').hide();
                    getcustomize();
                }
            }
        });
        //LoadImageStart();
        var nodays = $('#noofdays').val();
        var widthTodiv = 0;
        $('.city-row').each(function (increDays) {
            widthTodiv = increDays;
        });
        widthTodiv = widthTodiv + 1;
        if (nodays > 2 || widthTodiv > 2) {
            var daysnum = parseInt(widthTodiv) + parseInt(nodays);
            $(".mCSB_container").css('width', (daysnum * 370) + 'px');
        }
        var sel = $('#selectedcity').html();
        for (var h = parseInt(widthTodiv) ; h < nodays; h++) {
            var adfrpop = "";
            adfrpop = "<div class='city-row'><div class='trip-day'> <a href='#'><span class='btn'><img src=' ../Content/images/add-icon.png' alt='' /></span>";
            adfrpop += "<span class='txt'>Add Day</span></a></div><div class='trip-date'><span class='btn'>" + h + "</span><span class='txt'>Day</span></div>";
            adfrpop += "<div class='trip-name'>" + sel + "</div>";
            adfrpop += "<figure class='city-row-img'><a href='#'><img alt='' src='../Content/images/del-icon.png'></a></figure></div>";
            var aditi = "";
            aditi += "<div class='city-row-details'><div class='time' style='border-radius: 2px;'><ul><li>06.00 AM</li><li>06.30 AM</li><li>07.00 AM</li>";
            aditi += "<li>07.30 AM</li><li>08.00 AM</li><li>08.30 AM</li><li>09.00 AM</li>";
            aditi += "<li>09.30 AM</li><li>10.00 AM</li><li>10.30 AM</li><li>11.00 AM</li>";
            aditi += "<li>11.30 AM</li><li>12.00 PM</li><li>12.30 PM</li><li>01.00 PM</li>";
            aditi += "<li>01.30 PM</li><li>02.00 PM</li><li>02.30 PM</li><li>03.00 PM</li><li>03.30 PM</li>";
            aditi += "<li>04.00 PM</li><li>04.30 PM</li><li>05.00 PM</li><li>05.30 PM</li>";
            aditi += "<li>06.00 PM</li><li>06.30 PM</li><li>07.00 PM</li><li>07.30 PM</li>";
            aditi += "<li>08.00 PM</li><li>08.30 PM</li><li>09.00 PM</li><li>09.30 PM</li>";
            aditi += "<li>10.00 PM</li><li>10.30 PM</li><li>11.00 PM</li><li>11.30 PM</li><li>12.00 AM</li><li>12.30 AM</li><li>01.00 AM</li>";
            aditi += "</ul></div><div class='place-box-right'>";
            aditi += "<div class='place-box timing-details' style='height:2100px;'><div class='place-box timing-details'></div><div class='place-box timing-details'></div><div class='place-box timing-details'></div>";
            aditi += "<div class='place-box timing-details'></div><div class='place-box timing-details'></div>";            
            aditi += "</div></div></div>";
            $('.fr').append(adfrpop);
            $('#mCSB_4_container').append(aditi);            
        }
        $('.city-row').each(function (DayNumber) {
            var dayNumCnt = DayNumber + 1;
            if (dayNumCnt < 10) {
                $(this).find('.trip-date').find('.btn').html('0' + '' + dayNumCnt);
            }
            else {
                $(this).find('.trip-date').find('.btn').html(dayNumCnt);
            }
        });
    }
});

$("#btnregsubmit").on('click', function () {

    //alert("test");
    //

    if (($.trim($('[id$=regmail]').val() == "")) && ($.trim($('[id$=txtregpwd]').val() == "")) && ($.trim($('[id$=txtreglastname]').val() == "")) && ($.trim($('[id$=txtregfirstname]').val() == ""))) {
        if ($.trim($('[id$=txtregfirstname]').val()) == "") {
            var notify_alert = noty({
                layout: 'topRight',
                text: 'Please enter first name',
                type: 'warning',
                timeout: 2000,
                maxVisible: 1,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                }
            });
            $('[id$=txtregfirstname]').focus();
            $('[id$=txtregfirstname]').val("");
            return false;
        }
        if ($.trim($('[id$=txtreglastname]').val()) == "") {
            var notify_alert = noty({
                layout: 'topRight',
                text: 'Please enter last name',
                type: 'warning',
                timeout: 2000,
                maxVisible: 1,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                }
            });
            $('[id$=txtreglastname]').focus();
            $('[id$=txtreglastname]').val("");
            return false;
        }
        if ($.trim($('[id$=txtregmail]').val()) == "") {
            var notify_alert = noty({
                layout: 'topRight',
                text: 'Please enter Email Id',
                type: 'warning',
                timeout: 2000,
                maxVisible: 1,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                }
            });
            $('[id$=txtregmail]').focus();
            $('[id$=txtregmail]').val("");
            return false;
        }
        if ($.trim($('[id$=txtregpwd]').val()) == "") {
            var notify_alert = noty({
                layout: 'topRight',
                text: 'Please enter password',
                type: 'warning',
                timeout: 2000,
                maxVisible: 1,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                }
            });
            $('[id$=txtregpwd]').focus();
            $('[id$=txtregpwd]').val("");
            return false;
        }
        if ($.trim($('[id$=txtregconpwd]').val()) == "") {
            var notify_alert = noty({
                layout: 'topRight',
                text: 'Please enter conform password',
                type: 'warning',
                timeout: 2000,
                maxVisible: 1,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                }
            });
            $('[id$=txtregconpwd]').focus();
            $('[id$=txtregconpwd]').val("");
            return false;
        }
        if ($.trim($('[id$=txtregpwd]').val()) != $.trim($('[id$=txtregconpwd]').val())) {
            var notify_alert = noty({
                layout: 'topRight',
                text: 'Password doesn\'t match',
                type: 'warning',
                timeout: 2000,
                maxVisible: 1,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                }
            });
            $('[id$=txtregpwd]').focus();
            $('[id$=txtregconpwd]').val("");
            return false;
        }
    }
    var dataObject = {
        mail: $("#txtregmail").val(),
        password: $("#txtregpwd").val(),
        firstname: $("#txtregfirstname").val(),
        lastname: $("#txtreglastname").val()
    };
    $.ajax({
        url: "../Trip/Save",
        type: "POST",
        data: dataObject,
        dataType: "json",
        success: function (result) {
            $("#txtregmail").val('');
            $("#txtregpwd").val('');
            $("#txtregfirstname").val('');
            $("#txtreglastname").val('');
            $("#register").hide();
            $(".fancybox-overlay").hide();
            $("#wel-mem").show();
            $("#liMyAccount").hide();
            $("#hidcustomizeid").val("1");
            $("#hidcityid").val("1");
            $("#lblusername").text(result);
        },
        error: function () {
            var notify_alert = noty({
                layout: 'topRight',
                text: 'Not registered, please try once again..',
                type: 'warning',
                timeout: 2000,
                maxVisible: 1,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                }
            });
        }
    });
    $("#txtregmail").val('');
    $("#txtregpwd").val('');
    $("#txtregfirstname").val('');
    $("#txtreglastname").val('');
    $('[id$=txtregconpwd]').val("");




  
        //alert("neftdaycr");
        //
        TotalAttrs = 0;
        incrAttrs = 0;
        var favorite_cuisine = [];
        var cuisine = "";
        $.each($("input[name='cuisine']:checked"), function () {
            favorite_cuisine.push($(this).val());
        });
        cuisine = favorite_cuisine.join(",");
        $("#htnrestaurant").val(cuisine);
        var favorite_attraction = [];
        var attraction = "";
        $.each($("input[name='Attraction']:checked"), function () {
            favorite_attraction.push($(this).val());
        });
        attraction = favorite_attraction.join(",");
        $("#htnattraction").val(attraction);
        var favorite_hc = [];
        var hcstar = "";
        $.each($("input[name='hclass']:checked"), function () {
            favorite_hc.push($(this).val());
        });
        hcstar = favorite_hc.join(",");
        $("#htnhc").val(hcstar);
        var favorite_rating = [];
        var star = "";
        $.each($("input[name='star']:checked"), function () {
            favorite_rating.push($(this).val());
        });
        star = favorite_rating.join(",");
        $("#htnstar").val(star);
        var favorite_activiy = [];
        var activiy = "";
        $.each($("input[name='Activity']:checked"), function () {
            favorite_activiy.push($(this).val());
        });
        activiy = favorite_activiy.join(",");
        $("#hidactivity").val(activiy);
        var slival1 = $("#slider-range1").slider("values", 0);
        var slival2 = $("#slider-range1").slider("values", 1);
        $("div.place").remove();
        $("div.travel").remove();
        sortedarrayall.length = 0;
        var popstar = $("#htnstar").val();
        var rest_cuisine = $("#htnrestaurant").val();
        var attr_opt = $("#htnattraction").val();
        var acti_opt = $("#hidactivity").val();
        var hcls_star = $("#htnhc").val();
        if (rest_cuisine == "") {
            $("#htnrestaurant").val("Nooption");
        }
        if (attr_opt == "") {
            $("#htnattraction").val("Nooption");
        }
        if (acti_opt == "") {
            $("#hidactivity").val("Nooption");
        }
        var noof = $("#noofdays").val();
        var dtVal = $('#datepicker').val();
        var travel_dates = new String(dtVal).split("/", 3);
        var travel_date = new Date(travel_dates[2], travel_dates[0] - 1, travel_dates[1]);
        var Current_Date = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
        var travelEnd_date = new Date(Current_Date);
        var travelEndAfter_6 = new Date(new Date(travelEnd_date).setMonth(travelEnd_date.getMonth() + 6));
        var idate = document.getElementById("datepicker");
        if ($('#datepicker').val() == '') {
            var notify_alert = noty({
                layout: 'topRight',
                text: 'Enter Date of Travel',
                type: 'warning',
                timeout: 2000,
                maxVisible: 1,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                }
            });
            return false;
        }
        else if (travel_date < Current_Date) {
            var notify_alert = noty({
                layout: 'topRight',
                text: 'Invalid date. Please enter future date',
                type: 'warning',
                timeout: 2000,
                maxVisible: 1,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                }
            });
            $('#datepicker').focus();
            return false;
        }
        else if (travel_date > travelEndAfter_6) {
            var notify_alert = noty({
                layout: 'topRight',
                text: 'Invalid date. Please enter date within 6 months from today..',
                type: 'warning',
                timeout: 2000,
                maxVisible: 1,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                }
            });
            $('#datepicker').focus();
            return false;
        }
        else if (!(ValidateDate(dtVal))) {
            var notify_alert = noty({
                layout: 'topRight',
                text: 'Invalid date(mm/dd/yyyy)',
                type: 'warning',
                timeout: 2000,
                maxVisible: 1,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                }
            });
            return false;
        }
        else if (!(isFutureDate(idate.value))) {
            var notify_alert = noty({
                layout: 'topRight',
                text: 'Invalid date. Please enter future date',
                type: 'warning',
                timeout: 2000,
                maxVisible: 1,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                }
            });
            return false;
        }
        else if (noof == "") {
            var notify_alert = noty({
                layout: 'topRight',
                text: 'Enter Number of Days for your travel',
                type: 'warning',
                timeout: 2000,
                maxVisible: 1,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                }
            });
            $("#noofdays").focus();
            return false;
        }
        else if (noof > 15) {
            var notify_alert = noty({
                layout: 'topRight',
                text: 'Please enter number of days within 15 days',
                type: 'warning',
                timeout: 2000,
                maxVisible: 1,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                }
            });
            $("#noofdays").val('');
            $("#noofdays").focus();
            return false;
        }
        else if (noof < 1) {
            var notify_alert = noty({
                layout: 'topRight',
                text: 'Please select number of days greater than 0',
                type: 'warning',
                timeout: 2000,
                maxVisible: 1,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                }
            });
            $("#noofdays").val('');
            $("#noofdays").focus();
            return false;
        }
        else {
            $.fancybox.close();
            $('#LeftSidePart').block({
                message: 'Please wait while your itinerary is being created...',
                css: {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#033363',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
            });
            if (noof == 0) {
                noof = 1;
            }
            TripStartDate = dtVal;
            var dateParts = TripStartDate.split("/");
            var datenew = new Date(dateParts[2], dateParts[0] - 1, dateParts[1]);
            var dayFound = datenew.getDay();
            TripStartDay = dayFound;
            $.getJSON('../Trip/UpdateDate', { noday: noof, TripStartDate: dtVal }, function (data) {
                if ($("#priceValue").val() > 0) {
                    GetHotels();
                    UserPreferenceActivities();
                }
            });
            var NewData = {
                noday: noof,
                StartPrice: $("#slider-range1").slider("values", 0),
                EndPrice: $("#slider-range1").slider("values", 1),
                Star: $("#htnstar").val(),
                Hotel_class: $("#htnhc").val(),
                Cuisine: $("#htnrestaurant").val(),
                Attraction: $("#htnattraction").val(),
                Activity: $("#hidactivity").val()
            };
            $.ajax({
                url: '../Trip/Preference',
                type: 'POST',
                data: {
                    d: NewData
                },
                success: function (data) {
                    $("#htnstar").val("");
                    $("#htnrestaurant").val("");
                    $("#htnattraction").val("");
                    //$("#hidactivity").val("");
                    $("#htnhc").val("");
                    if ($("#priceValue").val() > 0) {
                        if ($("#IsfrmTopItin").val() == '0') {
                            UserpreferenceAttraction();
                        }


                      

                        else if ($("#IsfrmTopItin").val() == '1') {

                            //alert("neftdaycr1");

                            var PlanId = $("#TopItinryId").val();
                            if (PlanId.indexOf("1EzTrip") < 0) {
                                var days_num = "", plcs_ids = "", plcs_names = "", plcs_lat = "", plcs_lons = "", plcs_addr = "", plcs_rat = "", plcs_types = "", plcs_imgs = "", tm_type = "", tm_dur = "";
                                $.getJSON('../CMSPage/GetPackage', { PlanID: PlanId }, function (response) {
                                    if (response.data != "No Result") {
                                        packagedata = response;
                                        $.each(packagedata, function (keys, values) {
                                            plcs_ids = values.Spot_Id.split('||');
                                            plcs_names = values.Spot_Name.split('||');
                                            plcs_lat = values.Spot_Latitude.split('||');
                                            plcs_lons = values.Spot_Longitude.split('||');
                                            plcs_addr = values.Spot_Address.split('||');
                                            plcs_rat = values.Spot_Rating.split('||');
                                            plcs_types = values.Spot_Category.split('||');
                                            plcs_imgs = values.thumbnail_URL.split('|image|');
                                            tm_type = values.Travel_Mode.split('||');
                                            tm_dur = values.Travel_time.split('||');
                                            days_num = values.Day_Number;
                                            var itineraryData = "";
                                            for (var loopvar = 0, PlacesLength = plcs_ids.length; loopvar < PlacesLength; loopvar++) {
                                                if (plcs_ids[loopvar] != "") {
                                                    itineraryData += "<div class='place'><div class='place-details' id=" + plcs_ids[loopvar] + " style ='height:100px;'><figure class='place-img'>";
                                                    itineraryData += "<img src='" + plcs_imgs[loopvar] + "' /></figure><div class='place-cont'>";
                                                    itineraryData += "<div class='p-left'><h4>" + plcs_names[loopvar] + "</h4><p style='height:26px; overflow-y:hidden;'>" + plcs_addr[loopvar] + "</p><input type='hidden' class='hidlat' value='" + plcs_lat[loopvar] + "'/><input type='hidden' class='hidlng' value='" + plcs_lons[loopvar] + "'/>";
                                                    itineraryData += "<span>" + plcs_types[loopvar] + "</span>";
                                                    itineraryData += "</div><div class='p-right'>";
                                                    itineraryData += "<ul><li><a href='#'><img src='../Content/images/del-icon.png' alt='' onclick='fun1('uniid')' /></a></li><li><a href='#infowindow' class='fancybox' onclick='return infoattraction(\"" + plcs_ids[loopvar] + "\",\"" + plcs_lat[loopvar] + "\",\"" + plcs_lons[loopvar] + "\",\"" + plcs_types[loopvar] + "\");' ><img src='../Content/images/info-icon.png' alt='' /></a></li> </ul>";
                                                    itineraryData += "</div></div></div>";
                                                    itineraryData += "</div>";
                                                    if (tm_type[loopvar].trim() !== "No travelmode") {
                                                        itineraryData += "<div class='travel'><a href='#' style='float:right;'><img src='../Content/images/del_travel.png' class='travedelimg'/></a>";
                                                        itineraryData += "<p>Travel time by " + tm_type[loopvar] + " is " + tm_dur[loopvar] + "<img src='../Content/images/edit.png' alt='' class='editimg' style='height: 15px;width: 15px;'></p>";
                                                        itineraryData += "<ul>";
                                                        if (tm_type[loopvar] == "car") {
                                                            itineraryData += "<li><a href='#' data-value='car' class='active'><img src='../Content/images/car-icon.png' alt=''/></a></li>";
                                                        }
                                                        else {
                                                            itineraryData += "<li><a href='#' data-value='car'><img src='../Content/images/car-icon.png' alt=''/></a></li>";
                                                        }
                                                        if (tm_type[loopvar] == "train") {
                                                            itineraryData += "<li><a href='#' data-value='train' class='active'><img src='../Content/images/train-icon.png' alt='' /></a></li>";
                                                        }
                                                        else {
                                                            itineraryData += "<li><a href='#' data-value='train'><img src='../Content/images/train-icon.png' alt='' /></a></li>";
                                                        }
                                                        if (tm_type[loopvar] == "plane") {
                                                            itineraryData += "<li><a href='#' data-value='plane' class='active'><img src='../Content/images/plane-icon.png' alt='' /></a></li>";
                                                        }
                                                        else {
                                                            itineraryData += "<li><a href='#' data-value='plane'><img src='../Content/images/plane-icon.png' alt='' /></a></li>";
                                                        }
                                                        if (tm_type[loopvar] == "walk") {
                                                            itineraryData += "<li><a href='#' data-value='walk' class='active'><img src='../Content/images/walk-icon.png' alt='' /></a></li>";
                                                        }
                                                        else {
                                                            itineraryData += "<li><a href='#' data-value='walk'><img src='../Content/images/walk-icon.png' alt='' /></a></li>";
                                                        }
                                                        if (tm_type[loopvar] == "bicycle") {
                                                            itineraryData += "<li class='last'><a href='#' data-value='bicycle' class='active'><img src='../Content/images/scooter-icon.png' alt='' /></a></li>";
                                                        }
                                                        else {
                                                            itineraryData += "<li class='last'><a href='#' data-value='bicycle'><img src='../Content/images/scooter-icon.png' alt='' /></a></li>";
                                                        }
                                                        itineraryData += "<input type='hidden' class='hiddriving' value=''/></ul></div>";
                                                    }
                                                }
                                            }
                                            $(".place-box-right").eq(values.Day_Number - 1).find('.place-box:first').append(itineraryData);
                                        });
                                        var size = $('#compl').find('.travel').length;
                                        TravelDivResize(size);
                                        $('#LeftSidePart').unblock();
                                        //LoadImageEnd();
                                        UserpreferenceAttraction();
                                    }
                                });
                            }
                            else {
                                var spltPlanId = PlanId.split("1EzTrip");
                                var TopPlanId = spltPlanId[1];
                                $.getJSON('../trip/RetrievePackage', { TripId: TopPlanId }, function (data) {
                                    var itinerary = "";
                                    var increTravelMode = 0;
                                    var plcs_id_split = [], plcs_names_split = [], plcs_spendtime_split = [], plcs_addr_split = [], plcs_lat_split = [];
                                    var plcs_lng_split = [], travelmode_split = [], traveldur_split = [], plc_start_split = [], plc_cate_split = [], plc_thumbnailurl_split = [];
                                    $.each(data, function (keys, values) {
                                        plcs_id_split = values.Spot_Id.split('||');
                                        plcs_names_split = values.Spot_Name.split('||');
                                        plcs_spendtime_split = values.Spot_SpendingTime.split('||');
                                        plcs_addr_split = values.Spot_Address.split('||');
                                        plcs_lat_split = values.Spot_Latitude.split('||');
                                        plcs_lng_split = values.Spot_Longitude.split('||');
                                        travelmode_split = values.Travel_Mode.split('||');
                                        traveldur_split = values.Travel_time.split('||');
                                        plc_start_split = values.Visit_StartTime.split('||');/**stored in database as top,left styles**/
                                        plc_cate_split = values.spot_category.split('||');
                                        plc_thumbnailurl_split = values.thumbnail_Url.split('|image_split|')
                                        var att_or_res = 0;
                                        var daynum = values.Day_Number;
                                        var plc_bx_to_append = "";
                                        if ($('.place-box-right').eq(daynum - 1).find('.place-box').attr('id') != null || $('.place-box-right').eq(daynum - 1).find('.place-box').attr('id') != "undefined") {
                                            plc_bx_to_append = $('.place-box-right').eq(daynum - 1).find('.place-box').attr('id');
                                            $("#" + plc_bx_to_append).empty();
                                            for (var loopvar = 0, PlacesLength = plcs_id_split.length; loopvar < PlacesLength; loopvar++) {
                                                if (plcs_id_split[loopvar] == "pl_bx") {
                                                    itinerary = "<div class='place-box timing-details'></div>";
                                                }
                                                else {
                                                    var startingtime_parse = $.parseJSON(plc_start_split[loopvar]);
                                                    itinerary = "<div class='place' id='place" + plcs_id_split[loopvar] + "'><div class='place-details' id=" + plcs_id_split[loopvar] + " style ='height:" + plcs_spendtime_split[loopvar] + "px;'><input type='hidden' value=" + plcs_lat_split[loopvar] + " class='placeatt_lat'/><input type='hidden' value=" + plcs_lng_split[loopvar] + " class='placeatt_lng'/><input type='hidden' value=" + plcs_names_split[loopvar] + " class='placeatt_name'/><figure class='place-img'>";
                                                    itinerary += "<img src='" + plc_thumbnailurl_split[loopvar] + "' alt=''/></figure><div class='place-cont'>";
                                                    itinerary += "<div class='p-left'><h4>" + plcs_names_split[loopvar] + "</h4><p style='height:26px; overflow-y:hidden;'>" + plcs_addr_split[loopvar] + "</p><input type='hidden' class='hidlat' value='" + plcs_lat_split[loopvar] + "'/><input type='hidden' class='hidlng' value='" + plcs_lng_split[loopvar] + "'/><span>" + plc_cate_split[loopvar] + "</span>";
                                                    if (plc_cate_split[loopvar] != "Hotel.") {
                                                        itinerary += "<br><span id='sp_" + plcs_id_split[loopvar] + "' class='sphrs'>";
                                                    }
                                                    if (plc_cate_split[loopvar] == "Hotel.") {
                                                        //var found = jQuery.inArray(plcs_id_split[loopvar], inthetrip_array_hotels);
                                                        //if (found == -1) {
                                                        //    itinerary += "<span id='Plcbook" + plcs_id_split[loopvar] + "' style='display: block;margin-left: 20px;'><a href='#' class='Plcbook'>Book</a></span>";
                                                        //}
                                                    }
                                                    itinerary += "</div><div class='p-right'>";
                                                    itinerary += "<ul><li><a href='#'><img src='../Content/images/del-icon.png' class='deleteplace' alt=''/></a></li>";

                                                    if (plc_cate_split[loopvar] == "Restaurant.") {
                                                        //att_or_res = 1;
                                                        //itinerary += "<li><a href='#infowindow' class='fancybox' onclick='return infoattraction(\"" + plcs_id_split[loopvar] + "\",\"" + plcs_lat_split[loopvar] + "\",\"" + plcs_lng_split[loopvar] + "\",\"" + att_or_res + "\");' ><img src='../Content/images/info-icon.png' alt='' /></a></li> </ul>";
                                                        //var found = jQuery.inArray(plcs_id_split[loopvar], inthetrip_array_restaurants);
                                                        //if (found == -1) {
                                                        //    inthetrip_array_restaurants.push(plcs_id_split[loopvar]);
                                                        //}
                                                    }
                                                    else if (plc_cate_split[loopvar] == "Attraction.") {
                                                        //att_or_res = 2;
                                                        //itinerary += "<li><a href='#infowindow' class='fancybox' onclick='return infoattraction(\"" + plcs_id_split[loopvar] + "\",\"" + plcs_lat_split[loopvar] + "\",\"" + plcs_lng_split[loopvar] + "\",\"" + att_or_res + "\");' ><img src='../Content/images/info-icon.png' alt='' /></a></li> </ul>";
                                                        //var found = jQuery.inArray(plcs_id_split[loopvar], inthetrip_array_attractions);
                                                        //if (found == -1) {
                                                        //    inthetrip_array_attractions.push(plcs_id_split[loopvar]);
                                                        //}
                                                    }
                                                    else if (plc_cate_split[loopvar] == "Hotel.") {
                                                        //itinerary += "<li><a href='#infowindow' class='fancybox' onclick='return infohotel(\"" + plcs_id_split[loopvar] + "\");' ><img src='../Content/images/info-icon.png' alt='' /></a></li> </ul>";
                                                        //var found = jQuery.inArray(plcs_id_split[loopvar], inthetrip_array_hotels);
                                                        //if (found == -1) {
                                                        //    inthetrip_array_hotels.push(plcs_id_split[loopvar]);
                                                        //}
                                                        var CitiesCnt = 0;
                                                        for (var loopvariable = 0, RetrievedHotels = hotels_retrievedCities.length; loopvariable < RetrievedHotels; loopvariable++) {
                                                            if (hotels_retrievedCities[loopvariable].cityname.trim() == values.City_Name.trim()) {
                                                                CitiesCnt++;
                                                            }
                                                        }
                                                        if (CitiesCnt == 0) {
                                                            hotels_retrievedCities.push({
                                                                cityname: values.City_Name,
                                                                hotelId: plcs_id_split[loopvar]
                                                            })
                                                        }
                                                    }
                                                    itinerary += "</div> </div> </div>";
                                                    itinerary += "</div>";
                                                    if (travelmode_split[loopvar].trim() !== "No travelmode") {
                                                        increTravelMode++;
                                                        var getTravelmode = travelmode_split[loopvar];
                                                        var TravelmodeSelected = "";
                                                        if (getTravelmode.indexOf("Cus~") != -1) {
                                                            var GetCustomTravelMode = travelmode_split[loopvar].split('~');
                                                            TravelmodeSelected = GetCustomTravelMode[1];
                                                            itinerary += "<div class='custom_travel' id='ltra_" + increTravelMode + "'>";
                                                        }
                                                        else {
                                                            TravelmodeSelected = travelmode_split[loopvar];
                                                            itinerary += "<div class='travel' id='ltra_" + increTravelMode + "'>";
                                                        }
                                                        itinerary += "<a href='#' style='float:right;'><img src='../Content/images/del_travel.png' class='travedelimg'/></a>";
                                                        itinerary += "<p>Time taken by " + TravelmodeSelected + " is " + traveldur_split[loopvar] + "</p>";
                                                        itinerary += "<ul>";
                                                        if (TravelmodeSelected == "car") {
                                                            itinerary += "<li><input type='hidden' class='hidcar' value='" + traveldur_split[loopvar] + "'/><a href='#' data-value='car' class='active'><img src='../Content/images/car-icon.png' alt=''/></a></li>";
                                                        }
                                                        else {
                                                            itinerary += "<li><input type='hidden' class='hidcar' value=''/><a href='#' data-value='car'><img src='../Content/images/car-icon.png' alt=''/></a></li>";
                                                        }
                                                        if (TravelmodeSelected == "train") {
                                                            itinerary += "<li><input type='hidden' class='hidtrain'  value='" + traveldur_split[loopvar] + "'/><a href='#' data-value='train' class='active'><img src='../Content/images/train-icon.png' alt='' /></a></li>";
                                                        }
                                                        else {
                                                            itinerary += "<li><input type='hidden' class='hidtrain' value=''/><a href='#' data-value='train'><img src='../Content/images/train-icon.png' alt='' /></a></li>";
                                                        }
                                                        if (TravelmodeSelected == "plane") {
                                                            itinerary += "<li><input type='hidden' class='hidplane'  value='" + traveldur_split[loopvar] + "'/><a href='#' data-value='plane' class='active'><img src='../Content/images/plane-icon.png' alt='' /></a></li>";
                                                        }
                                                        else {
                                                            itinerary += "<li><input type='hidden' class='hidplane' value=''/><a href='#' data-value='plane'><img src='../Content/images/plane-icon.png' alt='' /></a></li>";
                                                        }
                                                        if (TravelmodeSelected == "walk") {
                                                            itinerary += "<li><input type='hidden' class='hidwalk'  value='" + traveldur_split[loopvar] + "'/><a href='#' data-value='walk' class='active'><img src='../Content/images/walk-icon.png' alt='' /></a></li>";
                                                        }
                                                        else {
                                                            itinerary += "<li><input type='hidden' class='hidwalk' value=''/><a href='#' data-value='walk'><img src='../Content/images/walk-icon.png' alt='' /></a></li>";
                                                        }
                                                        if (TravelmodeSelected == "bicycle") {
                                                            itinerary += "<li class='last'><input type='hidden' class='hidcycle' value='" + traveldur_split[loopvar] + "'/><a href='#' data-value='bicycle' class='active'><img src='../Content/images/scooter-icon.png' alt='' /></a></li>";
                                                        }
                                                        else {
                                                            itinerary += "<li class='last'><input type='hidden' class='hidcycle' value=''/><a href='#' data-value='bicycle'><img src='../Content/images/scooter-icon.png' alt='' /></a></li>";
                                                        }
                                                        itinerary += "<input type='hidden' class='hiddriving' value=''/>";
                                                        itinerary += "</ul></div>";
                                                    }
                                                }
                                                $("#" + plc_bx_to_append).append(itinerary);
                                            }
                                        }
                                    });
                                    var size = $('#compl').find('.travel').length;
                                    TravelDivResize(size);
                                    $('#LeftSidePart').unblock();
                                    //LoadImageEnd();
                                    UserpreferenceAttraction();
                                });
                            }
                        }
                    }
                    else {
                        $('#overlay').hide();
                        getcustomize();
                    }
                }
            });
            //LoadImageStart();
            var nodays = $('#noofdays').val();
            var widthTodiv = 0;
            $('.city-row').each(function (increDays) {
                widthTodiv = increDays;
            });
            widthTodiv = widthTodiv + 1;
            if (nodays > 2 || widthTodiv > 2) {
                var daysnum = parseInt(widthTodiv) + parseInt(nodays);
                $(".mCSB_container").css('width', (daysnum * 370) + 'px');
            }
            var sel = $('#selectedcity').html();
            for (var h = parseInt(widthTodiv) ; h < nodays; h++) {
                var adfrpop = "";
                adfrpop = "<div class='city-row'><div class='trip-day'> <a href='#'><span class='btn'><img src=' ../Content/images/add-icon.png' alt='' /></span>";
                adfrpop += "<span class='txt'>Add Day</span></a></div><div class='trip-date'><span class='btn'>" + h + "</span><span class='txt'>Day</span></div>";
                adfrpop += "<div class='trip-name'>" + sel + "</div>";
                adfrpop += "<figure class='city-row-img'><a href='#'><img alt='' src='../Content/images/del-icon.png'></a></figure></div>";
                var aditi = "";
                aditi += "<div class='city-row-details'><div class='time' style='border-radius: 2px;'><ul><li>06.00 AM</li><li>06.30 AM</li><li>07.00 AM</li>";
                aditi += "<li>07.30 AM</li><li>08.00 AM</li><li>08.30 AM</li><li>09.00 AM</li>";
                aditi += "<li>09.30 AM</li><li>10.00 AM</li><li>10.30 AM</li><li>11.00 AM</li>";
                aditi += "<li>11.30 AM</li><li>12.00 PM</li><li>12.30 PM</li><li>01.00 PM</li>";
                aditi += "<li>01.30 PM</li><li>02.00 PM</li><li>02.30 PM</li><li>03.00 PM</li><li>03.30 PM</li>";
                aditi += "<li>04.00 PM</li><li>04.30 PM</li><li>05.00 PM</li><li>05.30 PM</li>";
                aditi += "<li>06.00 PM</li><li>06.30 PM</li><li>07.00 PM</li><li>07.30 PM</li>";
                aditi += "<li>08.00 PM</li><li>08.30 PM</li><li>09.00 PM</li><li>09.30 PM</li>";
                aditi += "<li>10.00 PM</li><li>10.30 PM</li><li>11.00 PM</li><li>11.30 PM</li><li>12.00 AM</li><li>12.30 AM</li><li>01.00 AM</li>";
                aditi += "</ul></div><div class='place-box-right'>";
                aditi += "<div class='place-box timing-details' style='height:2100px;'><div class='place-box timing-details'></div><div class='place-box timing-details'></div><div class='place-box timing-details'></div>";
                aditi += "<div class='place-box timing-details'></div><div class='place-box timing-details'></div>";
                aditi += "</div></div></div>";
                $('.fr').append(adfrpop);
                $('#mCSB_4_container').append(aditi);
            }
            $('.city-row').each(function (DayNumber) {
                var dayNumCnt = DayNumber + 1;
                if (dayNumCnt < 10) {
                    $(this).find('.trip-date').find('.btn').html('0' + '' + dayNumCnt);
                }
                else {
                    $(this).find('.trip-date').find('.btn').html(dayNumCnt);
                }
            });
        }

    //alert("neftend");

       
    return false;
});

$("#txtloginmail").change(function () {
    var EmailTxt = $('#txtloginmail').val();
    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    if (filter.test(EmailTxt)) {
        $.getJSON('../Trip/CheckEmailStatus', { Email: EmailTxt }, function (data) {
            if (data == "1") {
                $.getJSON('../Trip/GetRememberedPassword', { Email: EmailTxt }, function (data) {
                    if (data.Success != "false") {
                        $("#txtloginpwd").val(data.Success);
                    }
                });
                return false;
            }
            else if(data=="2")
            {
                $('#txtloginmail').val("");
                $('#txtloginmail').focus();
                var noty_alert = noty({
                    layout: 'topRight',
                    text: 'Your account is deactivated. Please contact 1eztrip support team..',
                    type: 'warning',
                    timeout: 3000,
                    maxVisible: 1,
                    animation: {
                        open: { height: 'toggle' },
                        close: { height: 'toggle' },
                        easing: 'swing',
                        speed: 500
                    }
                });
            }
            //else {
            //    var notify_alert = noty({
            //        layout: 'topRight',
            //        text: 'Email-Id is not exist!',
            //        type: 'warning',
            //        timeout: 2000,
            //        maxVisible: 1,
            //        animation: {
            //            open: { height: 'toggle' },
            //            close: { height: 'toggle' },
            //            easing: 'swing',
            //            speed: 500
            //        }
            //    });
            //    $('#txtloginmail').focus();
            //    $('#txtloginmail').val("");
            //    return false;
            //}
        });
    }
    else {
        var notify_alert = noty({
            layout: 'topRight',
            text: 'In-Valid Email address',
            type: 'warning',
            timeout: 2000,
            maxVisible: 1,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            }
        });
        $('#txtloginmail').focus();
        $('#txtloginmail').val("");
        return false;
    }
});

$("#txtregmail").change(function () {
    var EmailTxt = $('#txtregmail').val();
    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    if (filter.test(EmailTxt)) {
        $.getJSON('../Trip/CheckEmailStatus', { Email: EmailTxt }, function (data) {
            if (data == "1") {
                var notify_alert = noty({
                    layout: 'topRight',
                    text: 'User name already exist',
                    type: 'warning',
                    timeout: 2000,
                    maxVisible: 1,
                    animation: {
                        open: { height: 'toggle' },
                        close: { height: 'toggle' },
                        easing: 'swing',
                        speed: 500
                    }
                });
                $('#txtregmail').focus();
                $('#txtregmail').val("");
                return false;
            }
            else if (data == "2") {
                var notify_alert = noty({
                    layout: 'topRight',
                    text: 'Your account is deactivated. Please contact 1eztrip support team..',
                    type: 'warning',
                    timeout: 2000,
                    maxVisible: 1,
                    animation: {
                        open: { height: 'toggle' },
                        close: { height: 'toggle' },
                        easing: 'swing',
                        speed: 500
                    }
                });
                $('#txtregmail').focus();
                $('#txtregmail').val("");
                return false;
            }
        });
    }
    else {
        var notify_alert = noty({
            layout: 'topRight',
            text: 'In-Valid Email address',
            type: 'warning',
            timeout: 2000,
            maxVisible: 1,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            }
        });
        $('#txtregmail').focus();
        $('#txtregmail').val("");
        return false;
    }
});

$("#btnlogin").on('click', function () {
    //alert("neftbtnlogin");
    //
    var value;
    if ($('#chkremember').prop("checked") == true) {
        value = 1;
    }
    else {
        value = 0;
    }
    if ($.trim($('[id$=txtloginmail]').val()) == "") {
        var notify_alert = noty({
            layout: 'topRight',
            text: 'Please enter Email Id',
            type: 'warning',
            timeout: 2000,
            maxVisible: 1,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            }
        });
        $('[id$=txtloginmail]').focus();
        $('[id$=txtloginmail]').val("");
        return false;
    }
    if ($.trim($('[id$=txtloginpwd]').val()) == "") {
        var notify_alert = noty({
            layout: 'topRight',
            text: 'Please enter password',
            type: 'warning',
            timeout: 2000,
            maxVisible: 1,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            }
        });
        $('[id$=txtloginpwd]').focus();
        $('[id$=txtloginpwd]').val("");
        return false;
    }
    var dataObject = {
        username: $("#txtloginmail").val(),
        pwd: $("#txtloginpwd").val(),
        rememberOpt: value
    };
    if ($("#hidmyacc").val() == 1) {

        //alert("neftlogin");
        $.ajax({
            url: "../Trip/login",
            type: "POST",
            data: dataObject,
            dataType: "json",
            success: function (result) {
                if (result == "Not confirm")
                {
                    var noty_alert = noty({
                        layout: 'topRight',
                        text: 'Login Failed! Your Email-Id is not confirmed!!',
                        type: 'warning',
                        timeout: 3000,
                        maxVisible: 1,
                        animation: {
                            open: { height: 'toggle' },
                            close: { height: 'toggle' },
                            easing: 'swing',
                            speed: 500
                        }
                    });
                    $("#hidcustomizeid").val("0");
                }
                else if (result == null) {
                    var noty_alert = noty({
                        layout: 'topRight',
                        text: 'Login Failed! Your Email-Id and Password doesn\'t match!!',
                        type: 'warning',
                        timeout: 3000,
                        maxVisible: 1,
                        animation: {
                            open: { height: 'toggle' },
                            close: { height: 'toggle' },
                            easing: 'swing',
                            speed: 500
                        }
                    });
                    $("#hidcustomizeid").val("0");
                }
                else {
                    notificationCount();
                    $('#LoggedUserLi').show();
                    $('#LogdUsrMyTrip').show();
                    $("#wel-mem").show();
                    $("#txtloginmail").val('');
                    $("#txtloginpwd").val('');
                    $("#liMyAccount").hide();
                    $("#register").hide();
                    $(".fancybox-overlay").hide();
                    $("#hidcustomizeid").val("1");
                    $("#hidcityid").val("1");
                    var page = $("#hidnavigate").val();
                    if (page == 1) {
                        if ($("#priceValue").val() > 0) {
                            $("#firstday").trigger("click");
                        }
                        else {
                            //alert("neftloginSaveMovement");
                            SaveMovement();
                        }
                    }
                    else {
                        $("#wel-mem").show();
                        $("#liMyAccount").hide();
                        $("#lblusername").text(result[0].username);
                        $('#LoggedUserId').val(result[0].Id);
                    }
                }
            },
            error: function (error) {
                var notify_alert = noty({
                    layout: 'topRight',
                    text: 'Login failed',
                    type: 'warning',
                    timeout: 2000,
                    maxVisible: 1,
                    animation: {
                        open: { height: 'toggle' },
                        close: { height: 'toggle' },
                        easing: 'swing',
                        speed: 500
                    }
                });
                $("#hidcustomizeid").val("0");
            }
        });
        return false;
    }
    else {

        //alert("neftloginSaveMovement1");
        $.ajax({
            url: "../Trip/login",
            type: "POST",
            data: dataObject,
            dataType: "json",
            success: function (result) {
                if (result == "Not confirm")
                {
                    var noty_alert = noty({
                        layout: 'topRight',
                        text: 'Login Failed! Your Email-Id is not confirmed!!',
                        type: 'warning',
                        timeout: 3000,
                        maxVisible: 1,
                        animation: {
                            open: { height: 'toggle' },
                            close: { height: 'toggle' },
                            easing: 'swing',
                            speed: 500
                        }
                    });
                    $("#hidcustomizeid").val("0");
                    $("#txtloginpwd").val('');
                }
                else if (result == null) {
                    var noty_alert = noty({
                        layout: 'topRight',
                        text: 'Login Failed! Your Email-Id and Password doesn\'t match!!',
                        type: 'warning',
                        timeout: 3000,
                        maxVisible: 1,
                        animation: {
                            open: { height: 'toggle' },
                            close: { height: 'toggle' },
                            easing: 'swing',
                            speed: 500
                        }
                    });
                    $("#hidcustomizeid").val("0");
                    $("#txtloginpwd").val('');
                }
                else {
                    notificationCount();
                    $('#LoggedUserLi').show();
                    $('#LogdUsrMyTrip').show();
                    $("#txtloginmail").val('');
                    $("#txtloginpwd").val('');
                    $("#liMyAccount").hide();
                    $("#register").hide();
                    $(".fancybox-overlay").hide();
                    $("#hidcustomizeid").val("1");
                    $("#hidcityid").val("1");
                    var page = $("#hidnavigate").val();
                    $("#wel-mem").show();
                    if (page == 1) {
                        if ($("#priceValue").val() > 0) {
                            $("#firstday").trigger("click");
                            $("#iternary").slideToggle("slow");
                            $("#lblusername").text(result[0].username);
                            $('#LoggedUserId').val(result[0].Id);
                        }
                        else {
                            //alert("neftloginSaveMovement2");
                            SaveMovement();
                        }
                    }
                    else {

                        $("#hlcity").attr("href", "#dest-popup");
                        $("#dest-popup").slideToggle("slow");
                        $("#wel-mem").show();
                        $("#liMyAccount").hide();
                        $("#lblusername").text(result[0].username);
                        $('#LoggedUserId').val(result[0].Id);
                    }
                }
            },
            error: function (error) {
                var notify_alert = noty({
                    layout: 'topRight',
                    text: 'Login Failed! Please enter a valid username or password!!',
                    type: 'warning',
                    timeout: 2000,
                    maxVisible: 1,
                    animation: {
                        open: { height: 'toggle' },
                        close: { height: 'toggle' },
                        easing: 'swing',
                        speed: 500
                    }
                });
                $("#hidcustomizeid").val("0");
                //$("#txtloginmail").val('');
                $("#txtloginpwd").val('');
            }
        });
        return false;
    }
});

$("#btnsend").on('click', function () {
    var dataObject = {
        username: $("#txtloginmail").val()
    };
    $.ajax({
        url: "../Trip/forgot",
        type: "POST",
        data: dataObject,
        dataType: "json",
        success: function (result) {
            var notify_alert = noty({
                layout: 'topRight',
                text: 'Mail delivered successfully',
                type: 'success',
                timeout: 2000,
                maxVisible: 1,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                }
            });
            $("#txtloginmail").val('');
        },
        error: function (result) {
            var notify_alert = noty({
                layout: 'topRight',
                text: 'Mail sent failed',
                type: 'warning',
                timeout: 2000,
                maxVisible: 1,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                }
            });
        }
    });
});

$("#finalize").on('click', function () {
    window.location.href = "../Trip/Index";
});

$("#payment").on('click', function () {
    $("#lightbox").hide();
    $("#overlay").hide();
    document.getElementById("preferrence").disabled = false;
    var allChildNodes = document.getElementById("preferrence").getElementsByTagName('*');
    for (var indexVal = 0, AllChildNodesLength = allChildNodes.length; indexVal < AllChildNodesLength; indexVal++) {
        allChildNodes[indexVal].disabled = false;
    }
    $('#slider-range').slider('enable');
    $('#hlcustomize').css("pointer-events", "none");
    $('#hlcustomize').css("cursor ", "defaults");
    $('#hlcustomize1').css("background-color", "black");
    $("#wel-mem").show();
    var noof = $("#noofdays").val();
    if (noof == 0) {
        noof = 1;
    }
    $.ajax({
        url: "../Trip/settheval",
        type: 'POST',
        async: false,
        data: {
            nodays: noof
        },
        success: function (data) { }
    });
});

$('input[type=text]').on('keyup', function (e) {
    if (e.which == 13) {
        $('#typedestination').show();
        var cityname = $('#txttypedest').val();
        var data = "<li><a href='#'><img src='../Content/images/country/Albania.jpg' alt='' /> <input type='checkbox' value=" + cityname + " /> " + cityname + "</a></li>";
        $('#cityalign').append(data);
        $('#typedestination').addClass("accordion_in acc_active");
        $('#typedestination').find('.acc_content').show();
        $('#txttypedest').val('');
    }
});

$('#citylist').on('change', function () {
    $('#typedestination').show();
    var cityname = $('#citylist').val();
    var mycit = $('#selcit').val();
    var selcityid = $('#citylist option:selected').val();
    if (mycit == '0') {
        var cit = $('#citylist option:selected').text();
        var data = "<li><a href='#' style=''><img src='../Content/images/country/Albania.jpg' alt='' /> <input checked='checked' type='checkbox' value=" + cityname + " /> " + cit + "</a></li>";
        $('#cityalign').append(data);
        $('#typedestination').addClass("accordion_in acc_active");
        $('#typedestination').find('.acc_content').show();
        $('#txttypedest').val('');
        $('#selcit').val(selcityid + '|');
    }
    else {
        var ccit = 0;
        var mycitsp = mycit.split('|');
        for (var ci = 0, citiesLen = mycitsp.length; ci < citiesLen ; ci++) {
            if (mycitsp[ci] == selcityid) {
                ccit = 1;
                break;
            }
        }
        if (ccit == 0) {
            var cit = $('#citylist option:selected').text();
            var data = "<li><a href='#' style=''><img src='../Content/images/country/Albania.jpg' alt='' /> <input checked='checked' type='checkbox' value=" + cityname + " /> " + cit + "</a></li>";
            $('#cityalign').append(data);
            $('#typedestination').addClass("accordion_in acc_active");
            $('#typedestination').find('.acc_content').show();
            $('#txttypedest').val('');
            $('#selcit').val(mycit + selcityid + '|');
        }
        else {
            var notify_alert = noty({
                layout: 'topRight',
                text: 'The city is already selected',
                type: 'warning',
                timeout: 2000,
                maxVisible: 1,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                }
            });
            return false;
        }
    }
});

function gotocustoradd() {
    $('#loadgif').show();
    $('#btnsubmit').hide();
    var count = $('#cityalign').find('.cityfindlist').length;
    var i = 1;
    var name = "";
    var latitude = "";
    var longitude = "", noofdays = "", save = "";
    noofdays += $('#dayhead').find('.city-row').length;
    name += $('#tn_1').html();
    latitude += $('#tn_1lat').val();
    longitude += $('#tn_1lng').val();
    save += "0";
    for (var indexValue = 0; indexValue < count; indexValue++) {
        name += "|" + $('#cityalign').find('.cityfindlist').eq(indexValue).html();
        latitude += "|" + $('#cityalign').find('.lat').eq(indexValue).html();
        longitude += "|" + $('#cityalign').find('.lng').eq(indexValue).html();
        noofdays += "|1";
        save += "|0";
    }
    $.getJSON('../Paypal/getPlanDetails', { TripName: "-", CityDaysCnt: noofdays, StartDate: $('#datepicker').val(), TofDays: noofdays, UserId: $('#LoggedUserId').val(), IsGroup: '0', GroupId: $('#LoggedUserId').val(), UpdatedBy: $("#lblusername").text(), Amount: $('#FirstCityPrice').html(), CityList: name, lat: latitude, lng: longitude, SaveFlag: save, ReturnPageName: $('#viewName').val(), IsAddCity: 'Yes' }, function (data) {
        $.each(data, function (keys, values) {
            window.location.href = "CustomiseorAdd";
        });
    });
    return false;
}

function getcity(id) {
    //alert("neftgetcity");
    var test = $('#' + id).html();
    $('#selectedcity').html(test);    
    return false;
}

function getcustomize() {
    //alert("custom");

    
    $("#hidmyacc").val("0");
    $("#hidnavigate").val("1");
    var Options = $("#hidcustomizeid").val();
    if (Options == 0) {
        $("#hlcustomize").attr("href", "#register");
        $("#register").slideToggle("slow");
        return false;
    }
    else {
        if ($("#priceValue").val() > 0) {
            $("#hlcustomize").attr("href", "#iternary");
            $("#iternary").slideToggle("slow");
            return false;
        }
        else {
            SaveMovement();
        }
    }
}

function logout() {
    var confirm_noty = noty({
        layout: 'topRight',
        type: 'confirm',
        text: 'Do you want to logout?',
        buttons: [
            {
                addClass: 'btn btn-primary', text: 'Ok', onClick: function ($noty) {
                    $noty.close();
                    $.ajax({
                        type: "POST",
                        url: '../Trip/ClearLoginSession',
                        success: function () {
                            window.location.href = "../Trip/Index";
                        }
                    });
                    FB.logout();
                }
            },
            {
                addClass: 'btn btn-danger', text: 'Cancel', onClick: function ($noty) {
                    $noty.close();
                    return false;
                }
            }
        ]
    });
}

function test() {
    $("#hlcustomize").trigger("click");
}

function getcity() {
    $("#hidmyacc").val("0");
    var Options = $("#hidcityid").val();
    if (Options == 0) {
        $("#hlcity").attr("href", "#register");
        $("#register").slideToggle("slow");
        return false;
    }
    else {
        $("#hlcity").attr("href", "#dest-popup");
        $("#dest-popup").slideToggle("slow");
        return false;
    }
}

function GetHotels() {
    var CityForHotelsReq = $('#selectedcity').html();
    var hotelsortv = $('#hotel_sortid_select').val();
    $.ajax({
        type: 'GET',
        url: '../Trip/EanHotelRequest',
        dataType: 'json',
        data: { 'lat': $('#tn_1lat').val(), 'lng': $('#tn_1lng').val() },
        success: function (response) {
            got_hotelResult = 1;
            hotelsForCities.push({
                cityName: CityForHotelsReq,
                Hotels: response
            });
            var hotelsAvailable = 0;
            for (var loopvarHotels = 0, citiesHotelsLength = hotelsForCities.length; loopvarHotels < citiesHotelsLength ; loopvarHotels++) {
                if (hotelsForCities[loopvarHotels].cityName.trim() == CityForHotelsReq.trim()) {
                    if (hotelsForCities[loopvarHotels].Hotels.HotelListResponse.HotelList) {
                        if (hotelsForCities[loopvarHotels].Hotels.HotelListResponse.HotelList["@size"] == 1) {
                            CopyOfHotels = hotelsForCities[loopvarHotels].Hotels.HotelListResponse.HotelList.HotelSummary;
                            hotelsAvailable = 1;
                        }
                        else {
                            CopyOfHotels = (hotelsForCities[loopvarHotels].Hotels.HotelListResponse.HotelList.HotelSummary).slice();
                            hotelsAvailable = 1;
                        }
                    }
                }
            }
            if ($("#IsfrmTopItin").val() == '1') {
                $('#classchangeid li').removeClass('last');
                $('#classchangeid #Hotel').removeClass('first');
                $('#classchangeid #Hotel').addClass('last');
                $('#prefer').html("Hotel Preferences");
                $.each(CopyOfHotels, function (keys, values) {
                    if (keys <= 10) {
                        var items = '';
                        items += "<aside class='resort-row' id='res_" + values.hotelId + "'><div class='resort-row1' id='divres_" + values.hotelId + "'>";
                        items += "<input type='hidden' class='hide'  id=" + values.hotelId + "><input type='hidden' id='latarray" + values.hotelId + "' value='" + values.latitude + "' class='reslat'><input type='hidden' id='lngarray" + values.hotelId + "' value='" + values.longitude + "' class='reslng'>";
                        items += "<figure class='resort-img'>";
                        var imgurl_split = values.thumbNailUrl.split('_');
                        var imgUrlLength = imgurl_split.length - 1;
                        imgurl_split[imgUrlLength] = imgurl_split[imgUrlLength].replace('t.jpg', 'b.jpg');
                        var imgurl = imgurl_split.join('_');
                        items += "<img src='http://media3.expedia.com" + imgurl + "' alt='' />";
                        items += "<span>Night from $<strong id='s" + values.hotelId + "'>" + values.highRate + "</strong></span></figure>";
                        items += "<div class='resort-details'><div class='r-left'><span class='hotel_identify' style='display:none'></span>";
                        items += "<h3 id='h" + values.hotelId + "'>" + values.name + "  </h3><ul>";
                        for (var loopvar_rate = 1; loopvar_rate <= 5; loopvar_rate++) {
                            if (values.tripAdvisorRating >= loopvar_rate) {
                                items += "<li><img src='../Content/images/hotel-color.png' /></li>";
                            }
                            else if (values.tripAdvisorRating > (loopvar_rate - 1) && values.tripAdvisorRating < loopvar_rate) {
                                items += "<li><img src='../Content/images/hotel-half.png' /></li>";
                            }
                            else {
                                items += "<li><img src='../Content/images/hotel-gray.png' /></li>";
                            }
                        }
                        items += "<li>(" + values.tripAdvisorRating + "/5)</li></ul>";
                        var desc = (values.shortDescription).replace('&lt;p&gt;&lt;b&gt;Property Location&lt;/b&gt; &lt;br /&gt;', '') + "...";
                        items += "<p id='pho_" + values.hotelId + "' style='height:40px; overflow-y:hidden;'>" + desc + "</p>";
                        items += "<div class='book-add'><span><a href='#' id='inner" + values.hotelId + "' class='add-trip'  onclick='return addtoleft(" + values.hotelId + ")'>Add Trip</a></span></div></div> ";
                        items += "<div class='r-right'>";
                        items += " <ul><li><a href='#'><img src='../Content/images/del-icon.png' alt='' class='del-show' onclick=''/></a></li>";
                        items += "<li><a  class='fancybox new1' href='#info_" + values.hotelId + "'><img src='../Content/images/info-icon.png' alt='' class='example1demo' /></a></li></ul>";
                        items += "</div></div></div>";
                        items += "</aside>";
                        items += "<div id='info_" + values.hotelId + "' style='display: none;'></div>";
                        $('#rhaa1').append(items);
                    }
                });
            }
        }
    });
}

function ValidateDate(dtValue) {
    var dtRegex = new RegExp("^(1[0-2]|0?[1-9])/(3[01]|[12][0-9]|0?[1-9])/(?:[0-9]{2})?[0-9]{2}$");
    return dtRegex.test(dtValue);
}

function isFutureDate(idate) {
    var today = new Date().getTime(),
        idate = idate.split("/");
    idate = new Date(idate[1] - 1, idate[2], idate[0]).getTime();
    return (today - idate) < 0 ? true : false;
}

function initialisetravel() {
    $('#LeftSidePart').unblock();
    $('.place + .place').before($('<div class="travel"><a href="#" style="float:right;"><img src="../Content/images/del_travel.png" class="travedelimg"/></a><p>Travel time by car is --</p><ul><li><a href="#" data-value="car" class="active"><img src="../Content/images/car-icon.png" alt=""/></a></li><li><a href="#" data-value="train"><img src="../Content/images/train-icon.png" alt="" /></a></li><li><a href="#" data-value="plane"><img src="../Content/images/plane-icon.png" alt="" /></a></li><li><a href="#" data-value="walk"><img src="../Content/images/walk-icon.png" alt="" /></a></li><li class="last"><a href="#" data-value="bicycle"><img src="../Content/images/scooter-icon.png" alt="" /></a></li></ul></div>'));
    $('.place-box .travel').each(function (indexVal) {
        $(this).attr("id", "ltra_" + indexVal);
    });
    calculatetravelduration();
}

function calculatetravelduration() {
    var plc_origin_addr = "", plc_destin_addr = "", plc_origin_addr1 = "", plc_destin_addr1 = "";
    var ori_lat = "", ori_lng = "", des_lat = "", des_lng = "";
    forfirsttimetravelduration.length = 0;
    traveldurationarray.length = 0;
    var timetaken = 0, roundoftime = 0;
    var increvariable = 0;
    $(".travel").each(function () {
        plc_origin_addr1 = $(this).prev('.place').find(".p-left h4").text() + "," + $(this).prev('.place').find(".p-left p").text();
        plc_destin_addr1 = $(this).next('.place').find(".p-left h4").text() + "," + $(this).next('.place').find(".p-left p").text();
        plc_origin_addr = $(this).prev('.place').find(".p-left p").text();
        plc_destin_addr = $(this).next('.place').find(".p-left p").text();
        ori_lat = $(this).prev('.place').find('.hidlat').val();
        ori_lng = $(this).prev('.place').find('.hidlng').val();
        des_lat = $(this).next('.place').find('.hidlat').val();
        des_lng = $(this).next('.place').find('.hidlng').val();
        forfirsttimetravelduration.push({
            ori_lt: ori_lat,
            ori_ln: ori_lng,
            des_lt: des_lat,
            des_ln: des_lng,
            orig: plc_origin_addr1,
            desti: plc_destin_addr1,
            origh: plc_origin_addr,
            destih: plc_destin_addr
        });
    });
    array_lengthfrtraveldura = forfirsttimetravelduration.length;
    if (array_lengthfrtraveldura > 0) {
        loopthrougharraytravelmode();
    }
}

function CalcTravelDuration() {
    for (var indxVal = 0, trvlDurLen = forfirsttimetravelduration.length; indxVal < trvlDurLen; indxVal++) {
        var orlat = forfirsttimetravelduration[indxVal].ori_lt;
        var orlng = forfirsttimetravelduration[indxVal].ori_ln;
        var delat = forfirsttimetravelduration[indxVal].des_lt;
        var delng = forfirsttimetravelduration[indxVal].des_ln;
        var dista = Haversine(orlat, orlng, delat, delng);
        var kmtomin1 = dista / 40;
        var kmtomin2 = kmtomin1 * 60;
        var kmtomin3 = kmtomin2.toFixed();
        var final_time = "";
        if (kmtomin3 > 60) {
            var kmtohrs = Math.floor(kmtomin3 / 60);
            var kmtomints1 = kmtomin3 % 60;
            if (kmtohrs <= 3) {
                if (kmtohrs == 1) {
                    var kmtomints = Math.ceil(kmtomints / 5) * 5;
                    final_time = kmtohrs + " hour " + kmtomints + " mins ";
                }
                else {
                    var kmtomints = Math.ceil(kmtomints / 5) * 5;
                    final_time = kmtohrs + " hours " + kmtomints + " mins ";
                }
            }
            else {
                final_time = "20 mins ";
            }
        }
        else {
            if (kmtomin3 <= 1) {
                kmtomin3 = kmtomin3 + 4;
            }
            kmtomin3 = Math.ceil(kmtomin3 / 5) * 5;
            final_time = parseInt(kmtomin3) + " mins ";
        }
        traveldurationarray.push({
            distance: dista,
            duration: final_time
        });
    }
}

function loopthrougharraytravelmode() {
    var distance_service = new google.maps.DistanceMatrixService();
    var orlat = forfirsttimetravelduration[incr_td].ori_lt;
    var orlng = forfirsttimetravelduration[incr_td].ori_ln;
    var delat = forfirsttimetravelduration[incr_td].des_lt;
    var delng = forfirsttimetravelduration[incr_td].des_ln;
    var orilat_lng = new google.maps.LatLng(orlat, orlng);
    var deslat_lng = new google.maps.LatLng(delat, delng);
    distance_service.getDistanceMatrix(
         {
             origins: [orilat_lng],
             destinations: [deslat_lng],
             travelMode: google.maps.TravelMode.DRIVING
         }, callbackcar);
}

function callbackcar(response, status) {
    if (status === "OK") {
        var dist = "", dura = "";
        if (response.rows[0].elements[0].duration) {
            dist = response.rows[0].elements[0].distance.text;
            dura = response.rows[0].elements[0].duration.text;
            traveldurationarray.push({
                distance: dist,
                duration: dura
            });
        }
        else {
            traveldurationarray.push({
                distance: "--",
                duration: "No dura.."
            });
        }
        incr_td++;
        if (incr_td < array_lengthfrtraveldura) {
            loopthrougharraytravelmode();
        }
        else {
            printdurationfortravelmode();
        }
    }
    else {
        if (status === "OVER_QUERY_LIMIT") {
            if (incr_td < array_lengthfrtraveldura) {
                loopthrougharraytravelmode();
            }
            else {
                printdurationfortravelmode();
            }
        }
        else {
            traveldurationarray.push({
                distance: "--",
                duration: "No dura.."
            });
            incr_td++;
            if (incr_td < array_lengthfrtraveldura) {
                loopthrougharraytravelmode();
            }
            else {
                printdurationfortravelmode();
            }
        }
    }
}

function printdurationfortravelmode() {
    var incre_printtravelduration = 0;
    $(".travel").each(function () {
        var travel_duris = traveldurationarray[incre_printtravelduration].duration;
        var splitTraveltime = traveldurationarray[incre_printtravelduration].duration.split(' ');
        for (var loopthr = 0, travelTimeLength = splitTraveltime.length; loopthr < travelTimeLength; loopthr++) {
            if (splitTraveltime[loopthr] == "min" || splitTraveltime[loopthr] == "mins") {
                splitTraveltime[loopthr - 1] = Math.ceil(splitTraveltime[loopthr - 1] / 5) * 5;
            }
        }
        traveldurationarray[incre_printtravelduration].duration = splitTraveltime.join(" ");
        $(this).find("p").text('Time taken by car is ' + traveldurationarray[incre_printtravelduration].duration);
        incre_printtravelduration++;
        var day = "day";
        var days = "days";
        var hours = "hours";
        var hour = "hour";
        var mins = "mins";
        if (travel_duris.indexOf(day) != -1 || travel_duris.indexOf(days) != -1)
        {
            travel_duris = "20 mins";
            $(this).find("p").text('Time taken by car is ' + travel_duris);
        }
        if (travel_duris.indexOf(hour) != -1) {
            var array = travel_duris.split(' ');
            var amount = array[0] / .50;
            if (array[0] < 50) {
                amount = array[0] / .50;
            }
            else {
                amount = 50 / .50;
            }
            var timespent = amount * 58;
            if (travel_duris.indexOf(mins) != -1) {
                timespent = array[2] / .50 + timespent;
                $(this).css("height", timespent);
            }
            else {
                $(this).css("height", timespent);
            }
        }
        else if (travel_duris.indexOf(mins) != -1) {
            var array = travel_duris.split(' ');
            if (array[0] > 20) {
                var amount = array[0] / 30;
                var timespent = amount * 58;

                $(this).css("height", timespent);
            }
        }
    });
    finalcallfortravelmode();
}

function finalcallfortravelmode() {
    for (var loopvariable = 0, travelDurationLength = traveldurationarray.length; loopvariable < travelDurationLength; loopvariable++) {
        if (traveldurationarray[loopvariable].duration == "No dura..") {
            var dista = Haversine(forfirsttimetravelduration[loopvariable].ori_lt, forfirsttimetravelduration[loopvariable].ori_ln, forfirsttimetravelduration[loopvariable].des_lt, forfirsttimetravelduration[loopvariable].des_ln);
            var kmtomin1 = dista / 50;
            var kmtomin2 = kmtomin1 * 60;
            var kmtomin3 = kmtomin2.toFixed();
            var final_time = "";
            if (isNaN(kmtomin3)) {
                kmtomin3 = 15;
            }
            if (kmtomin3 > 60) {
                var kmtohrs = Math.floor(kmtomin3 / 60);
                var kmtomints = kmtomin3 % 60;
                if (kmtohrs <= 3) {
                    if (kmtohrs == 1) {
                        kmtomints = Math.ceil(kmtomints / 5) * 5;
                        final_time = kmtohrs + " hour " + kmtomints + " mins ";
                    }
                    else {
                        kmtomints = Math.ceil(kmtomints / 5) * 5;
                        final_time = kmtohrs + " hours " + kmtomints + " mins ";
                    }
                }
                else {
                    final_time = "2 hours 20 mins ";
                }
            }
            else {
                if (kmtomin3 <= 1) {
                    kmtomin3 = kmtomin3 + 4;
                }
                kmtomin3 = Math.ceil(kmtomin3 / 5) * 5;
                final_time = parseInt(kmtomin3) + " mins ";
            }
            traveldurationarray[loopvariable].duration = final_time;
            $(".travel:eq(" + loopvariable + ")").find("p").text('Time taken by car is ' + traveldurationarray[loopvariable].duration);
            var hours = "hours";
            var hour = "hour";
            var mins = "mins";
            if (final_time.indexOf(hour) != -1) {
                var array = final_time.split(' ');
                var amount = array[0] / .50;
                if (array[0] < 50) {
                    amount = array[0] / .50;
                }
                else {
                    amount = 50 / .50;
                }
                var timespent = amount * 58;
                if (final_time.indexOf(mins) != -1) {
                    timespent = array[2] / .50 + timespent;
                    $(".travel:eq(" + loopvariable + ")").css("height", timespent);
                }
                else {
                    $(".travel:eq(" + loopvariable + ")").css("height", timespent);
                }
            }
            else if (final_time.indexOf(mins) != -1) {
                var array = final_time.split(' ');
                if (array[0] > 20) {
                    var amount = array[0] / 30;
                    var timespent = amount * 58;
                    $(".travel:eq(" + loopvariable + ")").css("height", timespent);
                }
            }
        }
    }
    chck_ItnraryExceedsDay();
}

function UserpreferenceAttraction() {
    var filtattcat = "", filtcntatt = 0;
    oldatt_actarray.length = 0, oldotherattraction.length = 0;
    att_actarray.length = 0, otherattraction.length = 0,secondClassAttr.length=0;    
    attus_arr_len=0,attus_arr_len1=0;
    $.getJSON('../Trip/Getuserpreference', function (data) {
        $.each(data, function (key, values) {
            var useratt = (values.attraction).toString();
            user_attr_pref = (values.attraction).toString();
            user_act = (values.activity).toString();
            user_rest = (values.cuisine).toString();
            user_numofdays = (values.No_of_days).toString();
            if (values.Hotel_class != null && values.Hotel_class != "") {
                user_hotelcls = (values.Hotel_class).toString();
            }
            else {
                user_hotelcls = "";
            }
            if (values.star != null && values.star != "") {
                user_hotelrating = (values.star).toString();
            }
            else {
                user_hotelrating = "";
            }
            user_hStartprice = (values.startprice).toString();
            user_hEndprice = (values.endprice).toString();
            if (useratt != "Nooption") {
                var spatt_new = useratt.split(',');
                var attractionLength = spatt_new.length;
                attus_arr_len = 1;
                var increCnt = 1;
                var newarrayTmp = "";
                var pushToArray = [];
                for (var loopvar = 0; loopvar < attractionLength; loopvar ++) {                    
                    pushToArray.push(spatt_new[loopvar]);
                }
                newarrayTmp = pushToArray.join("|");
                Attractionfunctioncall(newarrayTmp);                
            }
            else {
                var spatt_new = [];
                var increCnt = 0;
                spatt_new.push("aquarium", "place_of_worship", "museum", "art_gallery", "amusement_park","park");
                var attractionLength = spatt_new.length;
                attus_arr_len = Math.ceil(attractionLength / 6);
                var newarrayTmp = "";
                for (var loopvar = 0; loopvar < attractionLength; loopvar += 6) {
                    increCnt++;
                    if (increCnt < attus_arr_len || attractionLength % 6==0) {
                        newarrayTmp = spatt_new[loopvar + 5] + "|" + spatt_new[loopvar + 4] + "|" + spatt_new[loopvar + 3] + "|" + spatt_new[loopvar + 2] + "|" + spatt_new[loopvar + 1] + "|" + spatt_new[loopvar];
                        Attractionfunctioncall(newarrayTmp);
                    }
                }
                var spatt_new1 = [];
                increCnt = 0;                
                spatt_new1.push("palace", "botanical garden", "stadium", "bridge", "cathedral", "museum", "beach", "island", "zoo", "square", "statue", "harbour", "pier", "tower", "historical building");
                attractionLength = spatt_new1.length;
                attus_arr_len1 = Math.ceil(attractionLength / 5);
                for (var loopvar = 0; loopvar < attractionLength; loopvar += 5) {
                    increCnt++;
                    if (increCnt < attus_arr_len1 || attractionLength % 5 == 0) {
                        newarrayTmp = "(" + spatt_new1[loopvar + 4] + " OR " + spatt_new1[loopvar + 3] + " OR " + spatt_new1[loopvar + 2] + " OR " + spatt_new1[loopvar + 1] + " OR " + spatt_new1[loopvar] + ")";
                        Attractionfunctioncall1(newarrayTmp);
                    }
                }

                //spatt_new1.push("attractions");
                //attractionLength = spatt_new1.length;
                //attus_arr_len1 = 1;
                 
                //    increCnt++;
 
                //        newarrayTmp = "(" + spatt_new1[0] +  ")";
                //        Attractionfunctioncall1(newarrayTmp);

            }
        });
    });
}

function Attractionfunctioncall(att_category) {
    var pyrmont = new google.maps.LatLng($('#tn_1lat').val(), $('#tn_1lng').val());
    map = new google.maps.Map(document.getElementById('map-canvas'), {
        center: pyrmont,
        zoom: 15
    });
    var request = {
        location: pyrmont,
        radius: 20000,
        rankby: ['prominence'],
        types: [att_category]
    };
    var service = new google.maps.places.PlacesService(map);
    service.nearbySearch(request, function (response, status) { callbackattraction(response, status,att_category) });
}

function callbackattraction(results, status, att_category) {
    if (status == google.maps.places.PlacesServiceStatus.OK) {
        var needsExclusions = [];
        for (var loopvar = 0, ResultsLength = results.length; loopvar < ResultsLength ; loopvar++) {
            if (results[loopvar].types) {
                needsExclusions = results[loopvar].types;
                someFlag = true;
                for (var index1 = 0; index1 < excludeMe.length; index1++) {
                    if (needsExclusions.indexOf(excludeMe[index1]) !== -1) {
                        someFlag = false;
                        break;
                    }
                }
                if (someFlag) {
                    if ((results[loopvar].name).toLowerCase().indexOf("college") >= 0) {
                        someFlag = false;
                    }
                }
                if (someFlag && results[loopvar].rating) {
                    if (results[loopvar].rating >= 1) {
                        AllAttrCollect.push(results[loopvar]);
                        attraction_array.push(results[loopvar]);
                        att_actarray.push(results[loopvar]);
                        ListAttrArr.push(results[loopvar]);
                        //addattraction_to_array(results[loopvar]);
                    }
                }
            }
        }
    }
    attus_arr_len--;
    if (attus_arr_len == 0 && attus_arr_len1 == 0) {
        TotalAttrs = AllAttrCollect.length;
        //GetAllAttrDet();
        UserpreferenceRestaurant();
        attrStatus = 1;
    }
}

function Attractionfunctioncall1(att_category1) {
    var pyrmont = new google.maps.LatLng($('#tn_1lat').val(), $('#tn_1lng').val());
    map = new google.maps.Map(document.getElementById('map-canvas'), {
        center: pyrmont,
        zoom: 15
    });
    var request = {
        location: pyrmont,
        radius: 20000,
        //rankby: ['prominence'],
        keyword: [att_category1]
    };
    var service = new google.maps.places.PlacesService(map);
    service.nearbySearch(request, function (response, status) { callbackattraction1(response, status, att_category1) });
}

function callbackattraction1(results, status, att_category1) {
    if (status == google.maps.places.PlacesServiceStatus.OK) {
        var needsExclusions = [];
        for (var loopvar = 0, ResultsLength = results.length; loopvar < ResultsLength; loopvar++) {
            if (results[loopvar].types) {
                needsExclusions = results[loopvar].types;
                someFlag = true;
                for (var index1 = 0; index1 < excludeMe.length; index1++) {
                    if (needsExclusions.indexOf(excludeMe[index1]) !== -1) {
                        someFlag = false;
                        break;
                    }
                }
                if (someFlag) {
                    if ((results[loopvar].name).toLowerCase().indexOf("college") >= 0) {
                        someFlag = false;
                    }
                }
                if (someFlag) {
                    if (results[loopvar].rating) {
                        if (results[loopvar].rating >= 1) {
                            AllAttrCollect.push(results[loopvar]);
                            attraction_array.push(results[loopvar]);
                            att_actarray.push(results[loopvar]);
                            ListAttrArr.push(results[loopvar]);
                        }
                    }
                }
            }
        }
    }
    attus_arr_len1--;
    if (attus_arr_len1 == 0 && attus_arr_len == 0) {
        attrStatus = 1;
        TotalAttrs = AllAttrCollect.length;
        UserpreferenceRestaurant();
        //GetAllAttrDet();
    }
}

function sleep(milliseconds) {
    var start = new Date().getTime();
    for (var indexVal = 0; indexVal < 1e7; indexVal++) {
        if ((new Date().getTime() - start) > milliseconds) {
            break;
        }
    }
}

function UserpreferenceRestaurant() {
    var sprest = user_rest.split(',');
    if (user_rest == "Nooption") {
        var arr_noopt_res = [];
        arr_noopt_res.push("mediterranean", "european", "mexican", "american", "french", "spanish", "italian", "indian", "japanese", "chinese", "thai", "asian", "breakfast", "pizza", "dessert", "bakery");
        var restauarntTypesLength = arr_noopt_res.length;
        resus_arr_len = Math.ceil(restauarntTypesLength / 4);
        var increCnt = 0;
        var newTmpRest = "";
        for (var loopvar = 0; loopvar < restauarntTypesLength; loopvar+=4) {
            increCnt++;            
            if (increCnt < resus_arr_len || restauarntTypesLength%4 == 0) {                                 
                newTmpRest = "(" + arr_noopt_res[loopvar + 3] + " OR " + arr_noopt_res[loopvar + 2] + " OR " + arr_noopt_res[loopvar + 1] + " OR " + arr_noopt_res[loopvar] + ")";
                    Restaurantfunctioncall(newTmpRest);
            }
        }
    }
    else {
        callRestaurantsIncr = 0;
        var restauarntTypesLength = sprest.length;
        resus_arr_len = Math.ceil(restauarntTypesLength / 2);
        for (var loopvar = 0; loopvar < restauarntTypesLength; loopvar++) {
            callRestaurantsIncr++;
            var extraTypes = restauarntTypesLength - loopvar;
            var newTmpRest = "";
            if (callRestaurantsIncr == 2 || extraTypes < 2) {
                if (callRestaurantsIncr == 2) {
                    newTmpRest = "(" + sprest[loopvar - 1] + " OR " + sprest[loopvar] + ")";
                    Restaurantfunctioncall(newTmpRest);
                }
                else {
                    Restaurantfunctioncall(sprest[loopvar]);
                }
                callRestaurantsIncr = 0;
            }
        }
    }
}

function Restaurantfunctioncall(rest_category) {
    var pyrmont = new google.maps.LatLng($('#tn_1lat').val(), $('#tn_1lng').val());
    map = new google.maps.Map(document.getElementById('map-canvas'), {
        center: pyrmont,
        zoom: 15
    });
    var request = {
        location: pyrmont,
        radius: 5000,
        rankby: ['prominence'],
        types: ['restaurant|food|bakery'],
        keyword: [rest_category]
    };
    var service = new google.maps.places.PlacesService(map);
    service.nearbySearch(request, callback_restaurant);
}

function callback_restaurant(results, status) {
    if (status == google.maps.places.PlacesServiceStatus.OK) {
        var needsExclusions = [];
        for (var loopvar = 0, resultsLength = results.length; loopvar < resultsLength; loopvar++) {
            if (results[loopvar].types) {
                needsExclusions = results[loopvar].types;
                someFlag = false;
                //for (var index1 = 0; index1 < Rest_excludeMe.length; index1++) {
                //    if (needsExclusions.indexOf(Rest_excludeMe[index1]) !== -1) {
                //        someFlag = false;
                //        break;
                //    }
                //}
                for (var index2 = 0; index2 < Rest_includeMe.length; index2++) {
                    if (needsExclusions.indexOf(Rest_includeMe[index2]) !== -1) {
                        someFlag = true;
                        break;
                    }
                }
                if (someFlag) {
                    if (results[loopvar].rating) {
                        AllRestCollect.push(results[loopvar]);
                        restlist_array.push(results[loopvar]);
                        rest_array.push(results[loopvar]);
                    }
                }
            }
        }
    }
    resus_arr_len--;
    if (resus_arr_len == 0) {
        TotalRests = AllRestCollect.length;
        //GetAllRestDet();
        UserpreferenceHotel();
        //UserPreferenceActivities();
        restStatus = 1;
    }
}

function UserPreferenceActivities() {
    var CityForActsReq = $('#selectedcity').html();
    $.ajax({
        type: 'GET',
        url: '../Trip/LocalExpertAPI',
        dataType: 'json',
        data: { 'cityname': CityForActsReq },
        success: function (response) {
            //ActsForCities.push({
            //    cityName: CityForActsReq,
            //    Acts: response
            //});
            var tmpFiltAct = [];
            activitiesCopy= activitiesfull_array = response.activities.slice(0);
            
            var filtActi = $("#hidactivity").val().split(',');

            for (var actIndx = 0, actLeng = activitiesCopy.length; actIndx < actLeng; actIndx++)  
            {
                for (var filtIndx = 0, filtLeng = filtActi.length; filtIndx < filtLeng; filtIndx++)
                {
                    var matchAct = false;
                    var categories = activitiesCopy[actIndx].categories;
                    for(var cateIndx=0,cateLen=categories.length;cateIndx<cateLen;cateIndx++)
                    {
                        if(categories[cateIndx]==filtActi[filtIndx])
                        {
                            matchAct = true;
                        }
                    }
                    if(matchAct)
                    {
                        tmpFiltAct.push(activitiesCopy[actIndx]);
                    }
                }
            }
            activitiesCopy = tmpFiltAct.slice(0);
            gotActivities = 1;
        },
        error: function (response) {
            gotActivities = 1;
            var errResponse = response;
        }
    });
   // UserpreferenceHotel();
}

function UserpreferenceHotel() {
    if (got_hotelResult == 1) {
        var incre_daynum = 1;
        if (hotelsForCities[0].Hotels.HotelListResponse.HotelList) {
            if (hotelsForCities[0].Hotels.HotelListResponse.HotelList["@size"] == 1) {
                hotelArrayForFilter = hotelsForCities[0].Hotels.HotelListResponse.HotelList['HotelSummary'];
            }
            else {
                hotelArrayForFilter = hotelsForCities[0].Hotels.HotelListResponse.HotelList['HotelSummary'].slice();
            }
            var sphotelrating = user_hotelrating.split(',');
            var sphotelclass = user_hotelcls.split(',');
            var filthr = [], filthc = [];
            var filthrstr = "";
            for (var loopvar = 0, HotelsRatingLength = sphotelrating.length; loopvar < HotelsRatingLength; loopvar++) {
                if (sphotelrating[loopvar] == 1) {
                    filthr.push("el.tripAdvisorRating < 2 && el.tripAdvisorRating >= 0");
                }
                else if (sphotelrating[loopvar] == 2) {
                    filthr.push("el.tripAdvisorRating < 3 && el.tripAdvisorRating >= 2");
                }
                else if (sphotelrating[loopvar] == 3) {
                    filthr.push("el.tripAdvisorRating < 4 && el.tripAdvisorRating >= 3");
                }
                else if (sphotelrating[loopvar] == 4) {
                    filthr.push("el.tripAdvisorRating < 5 && el.tripAdvisorRating >= 4");
                }
                else if (sphotelrating[loopvar] == 5) {
                    filthr.push("el.tripAdvisorRating == 5");
                }
            }
            for (var loopvari = 0, HotelsClassLength = sphotelclass.length; loopvari < HotelsClassLength; loopvari++) {
                if (sphotelclass[loopvari] == 1) {
                    filthc.push("el.hotelRating < 2 && el.hotelRating >= 0");
                }
                else if (sphotelclass[loopvari] == 2) {
                    filthc.push("el.hotelRating < 3 && el.hotelRating >= 2");
                }
                else if (sphotelclass[loopvari] == 3) {
                    filthc.push("el.hotelRating < 4 && el.hotelRating >= 3");
                }
                else if (sphotelclass[loopvari] == 4) {
                    filthc.push("el.hotelRating < 5 && el.hotelRating >= 4");
                }
                else if (sphotelclass[loopvari] == 5) {
                    filthc.push("el.hotelRating == 5");
                }
            }
            if (filthr.length > 0) {
                var filthrstrTemp = filthr.join(" || ");
                filthrstr += "(" + filthrstrTemp + ") && ";
            }
            if (filthc.length >= 1) {
                var filthrstrTemp = filthc.join(" || ");
                filthrstr += "(" + filthrstrTemp + ") && ";
            }
            filthrstr += "(el.highRate>=" + user_hStartprice + " && el.highRate<=" + user_hEndprice + ")";
            if (hotelArrayForFilter.length) {
                if (filthrstr == "\"(el.highRate >= 10 && el.highRate <= 2000)\"") {
                    finalfilterArrayfull = hotelArrayForFilter.slice();
                }
                else {
                    finalfilterArrayfull = hotelArrayForFilter.filter(function (el) {
                        return eval(filthrstr);
                    });
                }
            }
            else {
                finalfilterArrayfull = jQuery.extend({}, hotelArrayForFilter);
            }
            $.each(finalfilterArrayfull, function (keys, values) {
                hotel_array.push({
                    plc_id: values.hotelId,
                    plc_name: values.name,
                    plc_lat: values.latitude,
                    plc_lng: values.longitude
                });
                hotel_arrayfull.push(finalfilterArrayfull[keys]);
            });
        }
        //if (gotActivities == 1) {
            callItineraryCreator();
        //}
        //else
        //{
        //    setTimeout("UserpreferenceHotel()", 2000);
        //}
    }
    else {
        setTimeout("UserpreferenceHotel()", 2000);
    }
}

function callItineraryCreator() {
    GetAttractionForFirst();
    startItinerarycreation();
}

function startItinerarycreation() {

    var closest = 0, next_clslat = "";
    var next_placeid = "";
    var next_clslng = "", next_place = "";
    var prevUsrRevTot = 0;
    var src_lat = "", src_lng = "";
    var distanceis = "";
    var gotPlcToFill = false,gotActicity=true;
    var mindist = 99999;
    var allattr_length = att_actarray.length;
    var allrest_length = rest_array.length;
    src_lat = $('#tn_1lat').val();
    src_lng = $('#tn_1lng').val();
    var checkforattact = 0, checkforrest = 0, checkforhotel = 0;
    if (allattr_length < 4) {
        checkforattact = 1;
    }
    else {
        checkforattact = 0;
    }
    if (allrest_length < 2) {
        checkforrest = 1;
    }
    else {
        checkforrest = 0;
    }
    if (hotel_array.length < 1) {
        checkforhotel = 1;
        ishotel_avail = 1;
    }
    var atactarr_length = allattr_length;
    if (checkforattact == 0) {
        for (var nod_loopvar = 0; nod_loopvar < 1; nod_loopvar++) {
            gotActicity = true;
            for (var plc_loopvar = 0; plc_loopvar < 7; plc_loopvar++) {
                gotPlcToFill = true;                
                if (plc_loopvar == 3 || plc_loopvar == 6) {
                    mindist = 99999;
                    var plcStrtTime = "";
                    var plcEndtime = "";
                    if (plc_loopvar == 3) {
                        plcStrtTime = "13";
                        plcEndtime = "15";
                    }
                    else if (plc_loopvar == 6) {
                        plcStrtTime = "18";
                        plcEndtime = "22";
                    }
                    if (rest_array.length > 0)
                    {
                        for (var loopvar = 0, RestaurntsLength = rest_array.length; loopvar < RestaurntsLength; loopvar++) {
                            var strtTime = "";
                            var clsTime = "";
                            var DayExists1 = false;
                            var DayExists2 = false;
                            //if (rest_array[loopvar].plc_time != "No time") {
                            //    var tmpTimeArr = rest_array[loopvar].plc_time.periods.slice(0);
                            //    for (var dayIndx = 0, dayArrLen = tmpTimeArr.length; dayIndx < dayArrLen; dayIndx++) {
                            //        if (tmpTimeArr[dayIndx].open) {
                            //            if (tmpTimeArr[dayIndx].open.day == TripStartDay) {
                            //                strtTime = tmpTimeArr[dayIndx].open.hours;
                            //                DayExists1 = true;
                            //            }
                            //        }
                            //        if (tmpTimeArr[dayIndx].close) {
                            //            if (tmpTimeArr[dayIndx].close.day == TripStartDay) {
                            //                clsTime = tmpTimeArr[dayIndx].close.hours;
                            //                DayExists2 = true;
                            //            }
                            //        }
                            //    }
                            //    if (!DayExists1) {
                            //        strtTime = "25";
                            //    }
                            //    if (!DayExists2) {
                            //        clsTime = "0";
                            //    }
                            //}
                            //else {
                            //    strtTime = "6";
                            //    clsTime = "23";
                            //}
                            //if (parseInt(strtTime) <= parseInt(plcStrtTime) && parseInt(clsTime) >= parseInt(plcEndtime)) {
                            var dist = Haversine(rest_array[loopvar].geometry.location.lat(), rest_array[loopvar].geometry.location.lng(), src_lat, src_lng);
                                if (dist < mindist) {
                                    next_clslat = rest_array[loopvar].geometry.location.lat();
                                    next_clslng = rest_array[loopvar].geometry.location.lng();
                                    next_place = rest_array[loopvar].name;
                                    next_placeid = rest_array[loopvar].place_id;
                                    mindist = dist;
                                    gotPlcToFill = false;
                                    break;
                                }
                            //}
                        }
                        rest_array = rest_array.filter(function (ele) {
                            return ele.place_id !== next_placeid;
                        });
                    }                    
                    src_lat = next_clslat;
                    src_lng = next_clslng;
                    sortedarrayall.push({
                        plc_id: next_placeid,
                        plc_name: next_place,
                        plc_lat: next_clslat,
                        plc_lng: next_clslng,
                        plc_dist: mindist
                    });
                }
                else if (plc_loopvar == 0) {
                    mindist = 99999;
                    if (checkforhotel == 0) {
                        for (var loopvar = 0, HotelsLength = hotel_array.length; loopvar < HotelsLength; loopvar++) {
                            var dist = Haversine(hotel_array[loopvar].plc_lat, hotel_array[loopvar].plc_lng, src_lat, src_lng);
                            if (dist < mindist) {
                                next_clslat = hotel_array[loopvar].plc_lat;
                                next_clslng = hotel_array[loopvar].plc_lng;
                                next_place = hotel_array[loopvar].plc_name;
                                next_placeid = hotel_array[loopvar].plc_id;
                                mindist = dist;
                            }
                        }
                        src_lat = next_clslat;
                        src_lng = next_clslng;
                        hotel_array = hotel_array.filter(function (ele) {
                            return ele.plc_id !== next_placeid;
                        });
                        sortedarrayall.push({
                            plc_id: next_placeid,
                            plc_name: next_place,
                            plc_lat: next_clslat,
                            plc_lng: next_clslng,
                            plc_dist: mindist
                        });
                    }
                    else {
                        sortedarrayall.push({
                            plc_id: "Empty",
                            plc_name: "Empty",
                            plc_lat: "Empty",
                            plc_lng: "Empty",
                            plc_dist: "Empty"
                        });
                    }
                }
                else {
                    prevUsrRevTot = 0;
                    mindist = 99999;
                    var plcStrtTime = "";
                    var plcEndtime="";
                    if (plc_loopvar == 1)
                    {
                        plcStrtTime = "8";
                        plcEndtime = "10";
                    }
                    else if (plc_loopvar == 2) {
                        plcStrtTime = "11";
                        plcEndtime = "13";
                    }
                    else if (plc_loopvar == 4) {
                        plcStrtTime = "15";
                        plcEndtime = "17";
                    }
                    else if (plc_loopvar == 5) {
                        plcStrtTime = "17";
                        plcEndtime = "20";
                    }
                    if (att_actarray.length>0) {
                        for (var loopvar = 0, AttractionActivity = att_actarray.length; loopvar < AttractionActivity; loopvar++) {                            
                            var strtTime = "";
                            var clsTime = "";
                            var DayExists1 = false;
                            var DayExists2 = false;
                            //if (att_actarray[loopvar].plc_time != "No time") {
                            //    var tmpTimeArr = att_actarray[loopvar].plc_time.periods.slice(0);
                            //    for (var dayIndx = 0, dayArrLen = tmpTimeArr.length; dayIndx < dayArrLen; dayIndx++) {
                            //        if (tmpTimeArr.length == 1 && tmpTimeArr[dayIndx].open && !(tmpTimeArr[dayIndx].close)) {
                            //            strtTime = "6";
                            //            DayExists1 = true;
                            //            clsTime = "23";
                            //            DayExists2 = true;
                            //        }
                            //        else {
                            //            if (tmpTimeArr[dayIndx].open) {
                            //                if (tmpTimeArr[dayIndx].open.day == TripStartDay) {
                            //                    strtTime = tmpTimeArr[dayIndx].open.hours;
                            //                    DayExists1 = true;
                            //                }
                            //            }
                            //            if (tmpTimeArr[dayIndx].close) {
                            //                if (tmpTimeArr[dayIndx].close.day == TripStartDay) {
                            //                    clsTime = tmpTimeArr[dayIndx].close.hours;
                            //                    DayExists2 = true;
                            //                }
                            //            }
                            //        }
                            //    }
                            //    if (!DayExists1) {
                            //        strtTime = "25";
                            //    }
                            //    if (!DayExists2) {
                            //        clsTime = "0";
                            //    }
                            //}
                            //else {
                            //    strtTime = "6";
                            //    clsTime = "23";
                            //}
                            //if (parseInt(strtTime) <= parseInt(plcStrtTime) && parseInt(clsTime) >= parseInt(plcEndtime)) {
                            //if (prevUsrRevTot < att_actarray[loopvar].user_ratings_total) {
                            if (att_actarray[loopvar].rating > 4.5) {
                                var dist = Haversine(att_actarray[loopvar].geometry.location.lat(), att_actarray[loopvar].geometry.location.lng(), src_lat, src_lng);
                                if (dist < mindist) {
                                    next_clslat = att_actarray[loopvar].geometry.location.lat();
                                    next_clslng = att_actarray[loopvar].geometry.location.lng();
                                    next_place = att_actarray[loopvar].name;
                                    //prevUsrRevTot = att_actarray[loopvar].user_ratings_total;
                                    next_placeid = att_actarray[loopvar].place_id;
                                    next_dist = dist;
                                    mindist = dist;
                                    gotPlcToFill = false;
                                }
                            }
                                //}
                            //}
                        }
                        att_actarray = att_actarray.filter(function (ele) {
                            return ele.place_id !== next_placeid;
                        });
                        next_placeid = "attr||" + next_placeid;
                    }
                    if (activitiesCopy.length > 0 && gotPlcToFill && gotActicity)
                    {
                        for (var loopvar = 0, ActivLength = activitiesCopy.length; loopvar < ActivLength; loopvar++) {
                            if (activitiesCopy[loopvar].durationInMillis <= 7200000 && activitiesCopy[loopvar].recommendationScore>75)
                            {
                                var actlat = activitiesCopy[loopvar].latLng.split(",");
                                var dist = Haversine(actlat[0], actlat[1], src_lat, src_lng);
                                if (dist < mindist) {
                                    next_clslat = actlat[0];
                                    next_clslng = actlat[1];                                    
                                    next_place = activitiesCopy[loopvar].title;
                                    next_placeid = "acti||" + activitiesCopy[loopvar].id;
                                    next_dist = dist;
                                    mindist = dist;
                                    gotPlcToFill = false;
                                    gotActicity = false;
                                }
                            }
                        }
                        if (next_placeid.toString().indexOf("acti||") != -1) {
                            var spltPlcId = next_placeid.split("||");
                            activitiesCopy = activitiesCopy.filter(function (ele) {
                                return ele.id !== spltPlcId[1];
                            });
                        }
                    }
                    if (att_actarray.length > 0 && gotPlcToFill) {
                        for (var loopvar = 0, AttractionActivity = att_actarray.length; loopvar < AttractionActivity; loopvar++) {
                            var strtTime = "";
                            var clsTime = "";
                            var DayExists1 = false;
                            var DayExists2 = false;                           
                            if (att_actarray[loopvar].rating > 3.5) {
                                var dist = Haversine(att_actarray[loopvar].geometry.location.lat(), att_actarray[loopvar].geometry.location.lng(), src_lat, src_lng);
                                if (dist < mindist) {
                                    next_clslat = att_actarray[loopvar].geometry.location.lat();
                                    next_clslng = att_actarray[loopvar].geometry.location.lng();
                                    next_place = att_actarray[loopvar].name;
                                    next_placeid = att_actarray[loopvar].place_id;
                                    next_dist = dist;
                                    mindist = dist;
                                    gotPlcToFill = false;
                                }
                            }
                        }
                        att_actarray = att_actarray.filter(function (ele) {
                            return ele.place_id !== next_placeid;
                        });
                        next_placeid = "attr||" + next_placeid;
                    }

                    if (activitiesCopy.length > 0 && gotPlcToFill && gotActicity) {
                        for (var loopvar = 0, ActivLength = activitiesCopy.length; loopvar < ActivLength; loopvar++) {
                            if (activitiesCopy[loopvar].durationInMillis <= 7200000 && activitiesCopy[loopvar].recommendationScore > 50) {
                                var actlat = activitiesCopy[loopvar].latLng.split(",");
                                var dist = Haversine(actlat[0], actlat[1], src_lat, src_lng);
                                if (dist < mindist) {
                                    next_clslat = actlat[0];
                                    next_clslng = actlat[1];
                                    next_place = activitiesCopy[loopvar].title;
                                    next_placeid = "acti||" + activitiesCopy[loopvar].id;
                                    next_dist = dist;
                                    mindist = dist;
                                    gotPlcToFill = false;
                                    gotActicity = false;
                                }
                            }
                        }
                        if (next_placeid.toString().indexOf("acti||") != -1) {
                            var spltPlcId = next_placeid.split("||");
                            activitiesCopy = activitiesCopy.filter(function (ele) {
                                return ele.id !== spltPlcId[1];
                            });
                        }
                    }
                    if (att_actarray.length > 0 && gotPlcToFill) {
                        for (var loopvar = 0, AttractionActivity = att_actarray.length; loopvar < AttractionActivity; loopvar++) {
                            var strtTime = "";
                            var clsTime = "";
                            var DayExists1 = false;
                            var DayExists2 = false;
                            if (att_actarray[loopvar].rating > 2.5) {
                                var dist = Haversine(att_actarray[loopvar].geometry.location.lat(), att_actarray[loopvar].geometry.location.lng(), src_lat, src_lng);
                                if (dist < mindist) {
                                    next_clslat = att_actarray[loopvar].geometry.location.lat();
                                    next_clslng = att_actarray[loopvar].geometry.location.lng();
                                    next_place = att_actarray[loopvar].name;
                                    next_placeid = att_actarray[loopvar].place_id;
                                    next_dist = dist;
                                    mindist = dist;
                                    gotPlcToFill = false;
                                }
                            }
                        }
                        att_actarray = att_actarray.filter(function (ele) {
                            return ele.place_id !== next_placeid;
                        });
                        next_placeid = "attr||" + next_placeid;
                    }
                    if (activitiesCopy.length > 0 && gotPlcToFill && gotActicity) {
                        for (var loopvar = 0, ActivLength = activitiesCopy.length; loopvar < ActivLength; loopvar++) {
                            if (activitiesCopy[loopvar].durationInMillis <= 7200000) {
                                var actlat = activitiesCopy[loopvar].latLng.split(",");
                                var dist = Haversine(actlat[0], actlat[1], src_lat, src_lng);
                                if (dist < mindist) {
                                    next_clslat = actlat[0];
                                    next_clslng = actlat[1];
                                    next_place = activitiesCopy[loopvar].title;
                                    next_placeid = "acti||" + activitiesCopy[loopvar].id;
                                    next_dist = dist;
                                    mindist = dist;
                                    gotPlcToFill = false;
                                    gotActicity = false;
                                }
                            }
                        }
                        if (next_placeid.toString().indexOf("acti||") != -1) {
                            var spltPlcId = next_placeid.split("||");
                            activitiesCopy = activitiesCopy.filter(function (ele) {
                                return ele.id !== spltPlcId[1];
                            });
                        }
                    }
                    if (att_actarray.length > 0 && gotPlcToFill) {
                        for (var loopvar = 0, AttractionActivity = att_actarray.length; loopvar < AttractionActivity; loopvar++) {
                            var strtTime = "";
                            var clsTime = "";
                            var DayExists1 = false;
                            var DayExists2 = false;
                                var dist = Haversine(att_actarray[loopvar].geometry.location.lat(), att_actarray[loopvar].geometry.location.lng(), src_lat, src_lng);
                                if (dist < mindist) {
                                    next_clslat = att_actarray[loopvar].geometry.location.lat();
                                    next_clslng = att_actarray[loopvar].geometry.location.lng();
                                    next_place = att_actarray[loopvar].name;
                                    next_placeid = att_actarray[loopvar].place_id;
                                    next_dist = dist;
                                    mindist = dist;
                                    gotPlcToFill = false;
                                }
                        }
                        att_actarray = att_actarray.filter(function (ele) {
                            return ele.place_id !== next_placeid;
                        });
                        next_placeid = "attr||" + next_placeid;
                    }
                                                                                
                    if (!gotPlcToFill) {
                        src_lat = next_clslat;
                        src_lng = next_clslng;
                        sortedarrayall.push({
                            plc_id: next_placeid,
                            plc_name: next_place,
                            plc_lat: next_clslat,
                            plc_lng: next_clslng,
                            plc_dist: mindist
                        });
                    }
                    else
                    {
                        sortedarrayall.push({
                            plc_id: "Empty",
                            plc_name: "Empty",
                            plc_lat: "Empty",
                            plc_lng: "Empty",
                            plc_dist: "Empty"
                        });
                    }
                }
            }
        }
        showtripforfirstday();
    }
    else {
        var htmlText = "Oops!! We have no itinerary for this city.. Please change your preferences..";
        var notify_alert = noty({
            layout: 'topRight',
            text: htmlText,
            type: 'warning',
            timeout: 2000,
            maxVisible: 1,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            }
        });
        $("#welcome").trigger("click");
    }
}

function showtripforfirstday() {
    for (var loopvar_sort = 0; loopvar_sort < 8; loopvar_sort++) {
        if (loopvar_sort == 3 || loopvar_sort == 6) {
            var place = restlist_array.filter(function (obj) {
                return obj.place_id == sortedarrayall[loopvar_sort].plc_id;
            });
            var placediv1 = "";
            if (place.length) {
                var att_or_res = 1;
                placediv1 = "<div class='place'><div class='place-details' id=" + place[0].id + " style ='height:100px;'><figure class='place-img'>";
                if (!place[0].photos) {
                    placediv1 += "<img src='../content/images/AppImages/restaurant.png' alt='' />";
                }
                else {
                    placediv1 += "<img src='" + place[0].photos[0].getUrl({ 'maxWidth': 35, 'maxHeight': 35 }) + "' />";
                }
                placediv1 += "</figure><div class='place-cont'>";
                placediv1 += "<div class='p-left'><h4>" + place[0].name + "</h4><p style='height:26px; overflow-y:hidden;'>" + place[0].vicinity + "</p><input type='hidden' class='hidlat' value='" + place[0].geometry.location.lat() + "'/><input type='hidden' class='hidlng' value='" + place[0].geometry.location.lng() + "'/><span>Restaurant.</span>";
                placediv1 += "</div><div class='p-right'>";
                placediv1 += "<ul><li><a href='#'><img src='../Content/images/del-icon.png' alt='' onclick='fun1('uniid')' /></a></li><li><a href='#infowindow' class='fancybox' onclick='return infoattraction(\"" + place[0].place_id + "\",\"" + place[0].geometry.location.D + "\",\"" + place[0].geometry.location.k + "\",\"" + att_or_res + "\");' ><img src='../Content/images/info-icon.png' alt='' /></a></li> </ul>";
                placediv1 += "</div> </div> </div>";
                placediv1 += "</div>";
                $("#col_0").append(placediv1);
            }
        }
        else if (loopvar_sort == 0 || loopvar_sort == 7) {
            if (ishotel_avail == 0) {
                var placdiv = "";

                var place = hotel_arrayfull.filter(function (obj) {
                    return obj.hotelId == sortedarrayall[0].plc_id;
                });
                if (loopvar_sort == 7) {
                    if (place.length) {
                        var placediv1 = "<div class='place'>";
                        placediv1 += "<div class='place-details' id=" + place[0].hotelId + " style ='height:86px;'><figure class='place-img'>";
                        placediv1 += "<img src='http://media3.expedia.com" + place[0].thumbNailUrl + "' alt='' /></figure><div class='place-cont'>";
                        placediv1 += "<div class='p-left'><h4>" + place[0].name + "</h4><p style='height:26px; overflow-y:hidden;'>" + place[0].address1 + "</p><input type='hidden' class='hidlat' value='" + place[0].latitude + "'/><input type='hidden' class='hidlng' value='" + place[0].longitude + "'/><span>Hotel.</span>";
                        placediv1 += "</div><div class='p-right'>";
                        placediv1 += "<ul><li><a href='#'><img src='../Content/images/del-icon.png' alt='' /></a></li><li><a href='#infohotel_" + place[0].hotelId + "' class='fancybox'><img src='../Content/images/info-icon.png' alt='' /></a></li> </ul>";
                        var descri = "";
                        var amenitiesare = "";
                        var multiam = 0;
                        if (place[0].amenityMask & 1) {
                            amenitiesare += "<li>Business Center</li>";
                        }
                        if (place[0].amenityMask & 2) {
                            amenitiesare += "<li>Fitness Center</li>";
                        }
                        if (place[0].amenityMask & 8) {
                            amenitiesare += "<li>Internet Access Available</li>";
                        }
                        if (place[0].amenityMask & 16) {
                            amenitiesare += "<li>Kids' Activities</li>";
                        }
                        if (place[0].amenityMask & 32) {
                            amenitiesare += "<li>Kitchen or Kitchenette</li>";
                        }
                        if (place[0].amenityMask & 64) {
                            amenitiesare += "<li>Pets Allowed</li>";
                        }
                        if (place[0].amenityMask & 128) {
                            amenitiesare += "<li>Pool</li>";
                        }
                        if (place[0].amenityMask & 256) {
                            amenitiesare += "<li>Restaurant On-site</li>";
                        }
                        if (place[0].amenityMask & 512) {
                            amenitiesare += "<li>Spa On-site</li>";
                        }
                        if (place[0].amenityMask & 2048) {
                            amenitiesare += "<li>Breakfast</li>";
                        }
                        if (place[0].amenityMask & 4096) {
                            amenitiesare += "<li>Babysitting</li>";
                        }
                        if (place[0].amenityMask & 16384) {
                            amenitiesare += "<li>Parking</li>";
                        }
                        if (place[0].amenityMask & 32768) {
                            amenitiesare += "<li>Room Service </li>";
                        }
                        if (place[0].amenityMask & 262144) {
                            amenitiesare += "<li>Roll-in Shower</li>";
                        }
                        if (place[0].amenityMask & 524288) {
                            amenitiesare += "<li>Handicapped Parking</li>";
                        }
                        if (place[0].amenityMask & 2097152) {
                            amenitiesare += "<li>Accessibility Equipment for the Deaf</li>";
                        }
                        if (place[0].amenityMask & 4194304) {
                            amenitiesare += "<li>Braille or Raised Signage</li>";
                        }
                        if (place[0].amenityMask & 8388608) {
                            amenitiesare += "<li>Free Airport Shuttle</li>";
                        }
                        if (place[0].amenityMask & 16777216) {
                            amenitiesare += "<li>Indoor Pool</li>";
                        }
                        if (place[0].amenityMask & 33554432) {
                            amenitiesare += "<li>Outdoor Pool</li>";
                        }
                        placediv1 += "<div id='infohotel_" + place[0].hotelId + "' class='info-left infoPopUp popup-bg' style='display:none;width:94%;'>";
                        placediv1 += "<a class='close-fancyboxButton' onclick='$.fancybox.close()' title='Close'><img src='../content/images/del_travel-bkp-7-5-15.png' style='padding-left: 98%;'/></a>";
                        placediv1 += "<figure class='resort-img'>";
                        var imgurl_split = place[0].thumbNailUrl.split('_');
                        var imgUrl_length = imgurl_split.length - 1;
                        imgurl_split[imgUrl_length] = imgurl_split[imgUrl_length].replace('t.jpg', 'b.jpg');
                        var imgurl = imgurl_split.join('_');
                        placediv1 += "<img src='http://media3.expedia.com" + imgurl + "' alt='' />";
                        placediv1 += "<span>Night from <strong>$" + place[0].highRate + "</strong></span></figure>";
                        placediv1 += "<h2>" + place[0].name + "</h2>";
                        placediv1 += "<ul><b>Guest Rating : </b>";
                        for (var loopvar2 = 0; loopvar2 < 5; loopvar2++) {
                            if (place[0].tripAdvisorRating == loopvar2) {
                                placediv1 += "<li><img src='../Content/images/hotel-gray.png' /></li>";
                            }
                            else if (place[0].tripAdvisorRating > loopvar2 && place[0].tripAdvisorRating < (loopvar2 + 1)) {
                                placediv1 += "<li><img src='../Content/images/hotel-half.png' /></li>";
                            }
                            else {
                                placediv1 += "<li><img src='../Content/images/hotel-color.png' /></li>";
                            }
                        }
                        placediv1 += "<li>(" + place[0].tripAdvisorRating + "/5)</li></ul>";
                        placediv1 += "<p><ul><b>Hotel Class: </b>";
                        for (var loopvar_hc = 1; loopvar_hc <= 5; loopvar_hc++) {
                            if (place[0].hotelRating >= loopvar_hc) {
                                placediv1 += "<li><img src='../Content/images/yellow-star1.png' /></li>";
                            }
                            else if (place[0].hotelRating > (loopvar_hc - 1) && place[0].hotelRating < loopvar_hc) {
                                placediv1 += "<li><img src='../Content/images/yellow-gray-starNew.png' /></li>";
                            }
                            else {
                                placediv1 += "<li><img src='../Content/images/grey-starNew.png' /></li>";
                            }
                        }
                        placediv1 += "(" + place[0].hotelRating + ")</ul></p>";
                        descri = "<p><b>Short Description : </b></p>" + (place[0].shortDescription).replace('&lt;p&gt;&lt;b&gt;Property Location&lt;/b&gt; &lt;br /&gt;', '') + "...";
                        placediv1 += "<p><b>Address : </b>" + place[0].address1;
                        if (place[0].address2 != null && place[0].address2 != "") {
                            placediv1 += "," + place[0].address2;
                        }
                        if (place[0].city != null && place[0].city != "") {
                            placediv1 += "," + place[0].city;
                        }
                        if (place[0].postalCode != null && place[0].postalCode != "") {
                            placediv1 += "-" + place[0].postalCode;
                        }
                        placediv1 += "</p><p><b>Landmark : </b>" + place[0].locationDescription + "</p><br/><p>" + descri + " </p>";
                        var propertycat = "";
                        if (place[0].propertyCategory == 1) {
                            propertycat = "hotel";
                        }
                        else if (place[0].propertyCategory == 2) {
                            propertycat = "suite";
                        }
                        else if (place[0].propertyCategory == 3) {
                            propertycat = "resort";
                        }
                        else if (place[0].propertyCategory == 4) {
                            propertycat = "vacation rental/condo";
                        }
                        else if (place[0].propertyCategory == 5) {
                            propertycat = "bed & breakfast";
                        }
                        else if (place[0].propertyCategory == 6) {
                            propertycat = "all-inclusive ";
                        }
                        if (propertycat != "hotel") {
                            placediv1 += "</p><p><b>Property Category : </b>" + propertycat + "</p>";
                        }
                        placediv1 += "<p><b>Rate starts from: </b>$" + place[0].highRate + "</p>";
                        placediv1 += "<p><b>For booking : </b><a href='" + place[0].deepLink + "'>Click here</a></p>";
                        if (amenitiesare != "") {
                            placediv1 += "<br/><p><b>Hotel Preferences : </b>" + amenitiesare + "</p>";
                        }
                        placediv1 += "</div>";
                        placediv1 += "</div></div></div>";
                        placediv1 += "</div>";
                        $("#col_0").append(placediv1);
                    }
                }
            }
        }
        else {
            var placeid = sortedarrayall[loopvar_sort].plc_id;
            if (placeid != "Empty") {
                var findid = placeid.split('||');
                var place = "";
                if (findid[0] == "attr") {
                    place = attraction_array.filter(function (obj) {
                        return obj.place_id == findid[1];
                    });
                    var placediv1 = "";
                    if (place.length) {
                        var att_or_res = 2;
                        placediv1 = "<div class='place'><div class='place-details' id=" + place[0].id + " style ='height:200px;'><figure class='place-img'>";
                        if (!place[0].photos) {
                            placediv1 += "<img src='../content/images/AppImages/attractions.jpg' alt='' />";
                        }
                        else {
                            placediv1 += "<img src='" + place[0].photos[0].getUrl({ 'maxWidth': 35, 'maxHeight': 35 }) + "' />";
                        }
                        placediv1 += "</figure><div class='place-cont'>";
                        placediv1 += "<div class='p-left'><h4>" + place[0].name + "</h4><p style='height:26px; overflow-y:hidden;'>" + place[0].vicinity + "</p><input type='hidden' class='hidlat' value='" + place[0].geometry.location.lat() + "'/><input type='hidden' class='hidlng' value='" + place[0].geometry.location.lng() + "'/><span>Attraction.</span>";
                        placediv1 += "</div><div class='p-right'>";
                        placediv1 += "<ul><li><a href='#'><img src='../Content/images/del-icon.png' alt='' onclick='fun1('uniid')' /></a></li><li><a href='#infowindow' class='fancybox' onclick='return infoattraction(\"" + place[0].place_id + "\",\"" + place[0].geometry.location.D + "\",\"" + place[0].geometry.location.k + "\",\"" + att_or_res + "\");' ><img src='../Content/images/info-icon.png' alt='' /></a></li> </ul>";
                        placediv1 += "</div></div></div>";
                        placediv1 += "</div>";
                        $("#col_0").append(placediv1);
                    }
                }
                else if (findid[0] == "acti") {
                    place = activitiesfull_array.filter(function (obj) {
                        return obj.id == findid[1];
                    });
                    var ActivityDur = place[0].durationInMillis;
                    var minutes = ActivityDur / (1000 * 60);
                    minutes = ((minutes / 30) * 100) / 2;
                    if (place.length) {
                        var ltlng = place[0].latLng.split(',');
                        var placediv1 = "<div  class='place' id='place" + place[0].id + "'>";
                        placediv1 += "<div class='place-details' id='" + place[0].id + "'  style='height:" + minutes + "px;'>";
                        placediv1 += "<input type='hidden' value='" + ltlng[0] + "' class='placeatt_lat'/><input type='hidden' value='" + ltlng[1] + "' class='placeatt_lng'/><input type='hidden' value='" + place[0].title + "' class='placeatt_name'/>";
                        placediv1 += "<input type='hidden' value='1' class='frmApi'/><figure class='place-img'><img src='" + place[0].imageUrl + "' alt='' /></figure>";
                        placediv1 += "<div class='place-cont'><div class='p-left'><h4>" + place[0].title + "</h4><p style='height:26px; overflow-y:hidden;'>" + place[0].duration + " .</p>";
                        placediv1 += "<input type='hidden' value='" + ltlng[0] + "' class='hidlat'/><input type='hidden' value='" + ltlng[1] + "' class='hidlng'/>"
                        placediv1 += "<span>Activity.</span><br><span id='sp_" + place[0].id + "' class='sphrs'></span></div>";
                        placediv1 += "<div class='p-right'><ul>";
                        placediv1 += "<li><a href='#'><img src='../content/images/del-icon.png' alt='' class='deleteplace' /></a></li>";
                        placediv1 += "<li><a href='#infoActi_" + place[0].id + "' class='fancybox'><img src='../Content/images/info-icon.png' alt='' /></a></li></ul>";
                        placediv1 += "<div id='infoActi_" + place[0].id + "' class='info-left infoPopUp popup-bg' style='display: none;width: 94%;'>";
                        placediv1 += "<a class='close-fancyboxButton' onclick='$.fancybox.close()' title='Close'><img src='../content/images/del_travel-bkp-7-5-15.png' style='padding-left: 98%;'/></a>";
                        placediv1 += "<figure class='resort-img'><img src='" + place[0].imageUrl + "' alt='' /><span>From <strong>" + place[0].fromPrice + "</strong></span></figure>";
                        placediv1 += "<h3>" + place[0].title + "</h3>";
                        placediv1 += "<ul><b>Ratings : </b>";
                        var rating = (place[0].recommendationScore / 20).toFixed(1);
                        for (var loopvar_rate = 1; loopvar_rate <= 5; loopvar_rate++) {
                            if (rating >= loopvar_rate) {
                                placediv1 += "<li><img src='../Content/images/tennisball_full.png' /></li>";
                            }
                            else if (rating > (loopvar_rate - 1) && rating < loopvar_rate) {
                                placediv1 += "<li><img src='../Content/images/tennisball_half.png' /></li>";
                            }
                            else {
                                placediv1 += "<li><img src='../Content/images/tennisball_grey_full.png' /></li>";
                            }
                        }
                        placediv1 += "<li>(" + rating + "/5)</li></ul>";
                        placediv1 += "<p><b>Duration : </b>" + place[0].duration + "</p>";
                        placediv1 += "<p><b>Recommended Score : </b>" + place[0].recommendationScore + "</p>";                        
                        placediv1 += "</div>";
                        placediv1 += "</div></div></div>";
                        placediv1 += "</div>";
                        
                        $("#col_0").append(placediv1);
                    }
                }
            }
        }
    }
    //LoadImageEnd();
    initialisetravel();
}

function UserpreferenceActInitialise() {
    var userpref_act = "";
    var filtactcat = "", filtcntact = 0;
    $.getJSON('../Trip/Getuserpreference', function (data) {
        $.each(data, function (key, values) {
            var spact = user_act.split(',');
            var ActivitiesLength = spact.length;
            for (var loopvar = 0; loopvar < ActivitiesLength; loopvar++) {
                filtcntact++;
            }
            for (var loopvar = 0; loopvar < ActivitiesLength; loopvar++) {
                if (spact[loopvar] === "Tours") {
                    filtactcat += "1,172";
                }
                else if (spact[loopvar] === "Concerts_Operas") {
                    filtactcat += "137";
                }
                else if (spact[loopvar] === "Shows_Musicals") {
                    filtactcat += "124,127,128";
                }
                else if (spact[loopvar] === "Meet_the_Locals") {
                    filtactcat += "34";
                }
                else if (spact[loopvar] === "Desert_Safaris") {
                    filtactcat += "37";
                }
                else if (spact[loopvar] === "Ski_Snowboard") {
                    filtactcat += "146";
                }
                else if (spact[loopvar] === "Sport_Events") {
                    filtactcat += "138";
                }
                else if (spact[loopvar] === "Shop") {
                    filtactcat += "20";
                }
                else if (spact[loopvar] === "Spa") {
                    filtactcat += "92,93";
                }
                else if (spact[loopvar] === "Movie") {
                    filtactcat += "126,22";
                }
                else if (spact[loopvar] === "Night") {
                    filtactcat += "111,112";
                }
                else if (spact[loopvar] === "Park") {
                    filtactcat += "36,131,136,159,192";
                }
                else if (spact[loopvar] === "Nooption") {
                    filtactcat += "1,172,137,124,127,128,34,37,146,138,20,92,93,126,22,111,112,36,131,136,159,192";
                }
                if (filtcntact != 0) {
                    filtactcat += ",";
                }
            }
        });
        var centrelat = "";
        var centrelng = "";
        var centrename = "";
        var centreid = "";
        $.ajax({
            type: "GET",
            dataType: "json",
            data: { filterdata: filtactcat },
            dataType: "json",
            url: '../Trip/FilterActivitiesInit',
            success: function (response) {
                var itss = "";
                var tableData = JSON.stringify(response);
                var resdata = $.parseJSON(tableData);
                $.each(resdata, function (keys, values) {
                    centrelat = values.start_Latitude;
                    centrelng = values.start_Longitude;
                    centrename = values.title;
                    centreid = values.id;
                    if (values.rating > 4.9) {
                        att_actarray.push({
                            plc_id: "acti||" + centreid,
                            plc_name: centrename,
                            plc_lat: centrelat,
                            plc_lng: centrelng
                        });
                    }
                    else {
                        otherattraction.push({
                            plc_id: "acti||" + centreid,
                            plc_name: centrename,
                            plc_lat: centrelat,
                            plc_lng: centrelng
                        });
                    }
                    activitiesfull_array.push(resdata[keys]);
                });
                startItinerarycreation();
            }
        });
    });
}

function Haversine(lat1, lon1, lat2, lon2) {
    var earth_rad = 6372.8;
    var dLat = Deg2Rad(lat2 - lat1);
    var dLon = Deg2Rad(lon2 - lon1);
    var area = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(Deg2Rad(lat1)) * Math.cos(Deg2Rad(lat2)) * Math.sin(dLon / 2) * Math.sin(dLon / 2);
    var circum = 2 * Math.atan2(Math.sqrt(area), Math.sqrt(1 - area));
    var distance_ll = earth_rad * circum;
    return distance_ll;
}

function Deg2Rad(deg) {
    return deg * Math.PI / 180;
}

google.maps.event.addDomListener(window, 'load', function () {
    var places = new google.maps.places.Autocomplete(document.getElementById('desttext'));
    google.maps.event.addListener(places, 'place_changed', function () {        
        $('#dest-popup').block({
            message: 'Adding the city to list..',
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#033363',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });
        var place = places.getPlace();
        var address = place.formatted_address;
        var latitude = place.geometry.location.lat();
        var longitude = place.geometry.location.lng();
        $('#selcit').val("0");
        var count = $('#dayhead').find('.city-listlat').length;
        for (k = 0; k < count; k++) {
            var lat = $('#dayhead').find('.city-listlat').eq(k).val();
            var lng = $('#dayhead').find('.city-listlng').eq(k).val();
            if (lng == longitude || lat == latitude) {
                $('#selcit').val("1");
                var notify_alert = noty({
                    layout: 'topRight',
                    text: 'Already in the plan',
                    type: 'warning',
                    timeout: 2000,
                    maxVisible: 1,
                    animation: {
                        open: { height: 'toggle' },
                        close: { height: 'toggle' },
                        easing: 'swing',
                        speed: 500
                    }
                });
                $('#desttext').val("");
            }
        }
        var check = $('#selcit').val();
        if (check != 1) {
            var i = $.inArray(latitude, latcheck);
            var j = $.inArray(longitude, lngcheck);
            if (i == -1 && j == -1) {
                $('#typedestination').show();
                var cityname = $('#desttext').val();
                $('#desttext').val("");
                CommonFunctionAddCity(latitude, longitude, cityname);
            }
            else {
                var notify_alert = noty({
                    layout: 'topRight',
                    text: 'Already added',
                    type: 'warning',
                    timeout: 2000,
                    maxVisible: 1,
                    animation: {
                        open: { height: 'toggle' },
                        close: { height: 'toggle' },
                        easing: 'swing',
                        speed: 500
                    }
                });
                $('#desttext').val("");
            }
        }
    });
});

function CommonFunctionAddCity(latitude, longitude, cityname) {
    var IsLatExists = $.inArray(JSON.parse(latitude), latcheck);
    var IsLngExists = $.inArray(JSON.parse(longitude), lngcheck);
    if (IsLatExists == -1 && IsLngExists == -1) {
        $('#typedestination').show();
        latcheck.push(JSON.parse(latitude));
        lngcheck.push(JSON.parse(longitude));
        var data = "<li style='background-image:none'><a href='#'  style='text-overflow: ellipsis;white-space: nowrap;overflow: hidden;width:87px;' title='" + cityname + "'><span class='cityfindlist' style='display:none;'>" + cityname + "</span><span class='lat' style='display:none;'>" + latitude + "</span><span class='lng' style='display:none;'>" + longitude + "</span><img src='https://maps.googleapis.com/maps/api/staticmap?center=" + latitude + "," + longitude + "&zoom=13&size=90x75&maptype=roadmap&markers=color:blue%7Clabel:%7C" + latitude + "," + longitude + "' alt='' style='border-radius:8px;'/> <input type='checkbox' value=\"" + cityname + "\" checked /> " + cityname + "</a></li>";
        $('#dest-popup').unblock();
        $('#cityalign').append(data);
        $('#typedestination').addClass("accordion_in acc_active");
        $('#typedestination').find('.acc_content').show();
    }
    else {
        var notify_alert = noty({
            layout: 'topRight',
            text: 'Already added',
            type: 'warning',
            timeout: 2000,
            maxVisible: 1,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            }
        });
        $('#desttext').val("");
    }
}

function AddedinCityAlign(Lat, Lng, cityname) {
    var latitude = Lat;
    var longitude = Lng;
    $('#selcit').val("0");
    var count = $('#dayhead').find('.city-listlat').length;
    for (k = 0; k < count; k++) {
        var vartestlat = $('#dayhead').find('.city-listlat').eq(k).val();
        var vartestlng = $('#dayhead').find('.city-listlng').eq(k).val();
        if (vartestlng == longitude || vartestlat == latitude) {
            $('#selcit').val("1");
            var notify_alert = noty({
                layout: 'topRight',
                text: 'Already in the plan',
                type: 'warning',
                timeout: 2000,
                maxVisible: 1,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                }
            });
        }
    }
    var check = $('#selcit').val();
    if (check != 1) {
        CommonFunctionAddCity(latitude, longitude, cityname);
    }
}

function SaveMovement() {
    //alert("neftSaveMovement");
    if ($("#priceValue").val() > 0) {
        $.getJSON('../Paypal/getPlanDetails', { TripName: "-", CityDaysCnt: $('#dayhead').find('.city-row').length, StartDate: $('#datepicker').val(), TofDays: $('#dayhead').find('.city-row').length, UserId: $('#LoggedUserId').val(), IsGroup: '0', GroupId: $('#LoggedUserId').val(), UpdatedBy: $("#lblusername").text(), Amount: $('#PriceForSingleCity').val(), CityList: $('#tn_1').html(), lat: $('#tn_1lat').val(), lng: $('#tn_1lng').val(), ReturnPageName: $('#viewName').val(), IsAddCity: 'No', SaveFlag: '0' }, function (data) {
            var url = "../Paypal/ValidateCommand?citiesName=" + $('#tn_1').html() + "&totalPrice=" + $('#PriceForSingleCity').val();
            window.location.href = url;
        });
    }
    else {

        //alert("Firstdaytripredirect");
        $.getJSON('../trip/SaveMovement', { TripName: "-", CityDaysCnt: $('#dayhead').find('.city-row').length, StartDate: $('#datepicker').val(), TofDays: $('#dayhead').find('.city-row').length, UserId: $('#LoggedUserId').val(), IsGroup: '0', GroupId: $('#LoggedUserId').val(), UpdatedBy: $("#lblusername").text(), Amount: $('#PriceForSingleCity').val(), CityList: $('#tn_1').html(), lat: $('#tn_1lat').val(), lng: $('#tn_1lng').val() }, function (data) {
            if (!data.Success) {
                $.each(data, function (keys, vals) {
                    $('#LoggedTripId').val(vals.Id);
                    $.fancybox.close();
                    window.location.href = "Firstdaytrip";
                });
            }
            else {
                window.location.href = "Index";
            }
        });
    }
}

function GetAttractionForFirst() {
    //alert('GetAttractionForFirst');
    var items = '';
    for (var indx = 0, maxRange = ListAttrArr.length; indx < maxRange; indx++) {
        if (indx <= 10) {
            var place = ListAttrArr[indx];
            var rating = Math.round(place.rating);
            if (place.rating) {
                rating = place.rating;
                var ratefilter = Math.round(place.rating);
                items += "<div id='rh" + place.place_id + "'></div><input type='hidden' class='hide'  id=" + place.place_id + "><input type='hidden' id='rates_" + place.place_id + "' value='" + rating + "'><input type='hidden' id='ratefilter_" + place.place_id + "' value='" + ratefilter + "'><input type='hidden' id='latarray" + place.place_id + "' value='" + place.geometry.location.lat() + "'><input type='hidden' id='lngarray" + place.place_id + "' value='" + place.geometry.location.lng() + "'><aside class='resort-row2' id='res_" + place.place_id + "'><div class='resort-row1' id='divres_" + place.place_id + "'>";
                if (place.user_ratings_total) {
                    items += "<input type='hidden' class='reviews' value='" + place.user_ratings_total + "'/>";
                }
                items += "<input type='hidden' id='latarray" + place.place_id + "' value='" + place.geometry.location.lat() + "' class='reslat'><input type='hidden' id='lngarray" + place.place_id + "' value='" + place.geometry.location.lng() + "' class='reslng'>";
                if (place.photos) {
                    items += "<figure class='resort-img'><img src='" + place.photos[0].getUrl({ 'maxWidth': 182, 'maxHeight': 120 }) + "' alt='' id='img_" + place.place_id + "' />";
                }
                else {
                    items += "<figure class='resort-img'><img src='../Content/images/AppImages/attractions.jpg' alt='' id='img_" + place.place_id + "' />";
                }
            }
            else {
                rating = 1;
                var ratefilter = rating;
                items += "<div id='rh" + place.place_id + "'></div><input type='hidden' class='hide'  id=" + place.place_id + "><input type='hidden' id='rates_" + place.place_id + "' value='1'><input type='hidden' id='ratefilter_" + place.place_id + "' value='" + ratefilter + "'><input type='hidden' id='latarray" + place.place_id + "' value='" + place.geometry.location.lat() + "'><input type='hidden' id='lngarray" + place.place_id + "' value='" + place.geometry.location.lng() + "'><aside class='resort-row2' id='res_" + place.place_id + "'><div class='resort-row1' id='divres_" + place.place_id + "'>";
                if (place.user_ratings_total) {
                    items += "<input type='hidden' class='reviews' value='" + place.user_ratings_total + "'/>";
                }
                items += "<input type='hidden' id='latarray" + place.place_id + "' value='" + place.geometry.location.lat() + "' class='reslat'><input type='hidden' id='lngarray" + place.place_id + "' value='" + place.geometry.location.lng() + "' class='reslng'>";
                if (place.photos) {
                    items += "<figure class='resort-img'><img src='" + place.photos[0].getUrl({ 'maxWidth': 182, 'maxHeight': 120 }) + "' alt='' id='img_" + place.place_id + "' />";
                }
                else {
                    items += "<figure class='resort-img'><img src='../Content/images/AppImages/attractions.jpg' alt='' id='img_" + place.place_id + "' />";
                }
            }
            items += "</figure>";
            items += "<div class='resort-details'><div class='r-left'>";
            items += "<h3 id='h" + place.place_id + "'>" + place.name + "  </h3><br/>";
            items += "<p style='height:36px; overflow-y:hidden;' id='p" + place.place_id + "'>" + place.vicinity + "</p>";
            if (place.website) {
                items += "<p style='height:18px; overflow-y:hidden;width:100%;text-overflow: ellipsis;white-space: nowrap;overflow: hidden;' id='web" + place.place_id + "'><b>Website : </b><a target='_blank' href='" + place.website + "' style='text-decoration: underline;'>" + place.website + "</a></p>";
            }
            items += "<ul id='rat" + place.place_id + "'><b>Ratings : </b>";
            for (var loopvar_rate = 1; loopvar_rate <= 5; loopvar_rate++) {
                if (rating >= loopvar_rate) {
                    items += "<li><img src='../Content/images/tower_full.png' /></li>";
                }
                else if (rating > (loopvar_rate - 1) && rating < loopvar_rate) {
                    items += "<li><img src='../Content/images/tower_half.png' /></li>";
                }
                else {
                    items += "<li><img src='../Content/images/tower_grey_full.png' /></li>";
                }
            }
            items += "</ul>";
            if (place.price_level) {
                if (place.price_level == 0) {
                    items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b> $</p> ";
                }
                else if (place.price_level == 1) {
                    items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b> $</p> ";
                }
                else if (place.price_level == 2) {
                    items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b>  $$</p> ";
                }
                else if (place.price_level == 3) {
                    items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b>  $$$</p> ";
                }
                else if (place.price_level == 4) {
                    items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b>  $$$$</p> ";
                }
            }
            else
            {
                items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b> $</p> ";
            }
            items += "<div class='book-add'><span><a href='#' id='inner" + place.place_id + "' class='add-trip' onclick='return AttractionToleft(\"" + place.place_id + "\");'>Add trip</a></span></div></div> ";
            items += "<div class='r-right'>";
            items += " <ul><li><a href='#'><img src='../Content/images/del-icon.png' alt='' class='del-show' onclick='fun2'/></a></li>";
            items += "<li><a  class='fancybox' href='#infowindow' onclick='return info(\"" + place.place_id + "\",\"" + place.geometry.location.lat() + "\",\"" + place.geometry.location.lng() + "\",2);'><img src='../Content/images/info-icon.png' alt='' class='example1demo' /></a></li></ul>";
            items += "</div></div></div>";
            items += "</aside>";
        }
    }
    $('#rhaa1').append(items);
}

function notificationCount() {
    var totalNotificationCount;
    $.ajax({
        url: '../Trip/GetNotificationCount',
        type: "POST",
        async: false,
        dataType: "json",
        success: function (result) {
            totalNotificationCount = result;
            if (totalNotificationCount > 0) {
                $("#notificationCount").show();
                $("#notificationCount").text(totalNotificationCount);
            } else {
                $("#notificationCount").hide();

            }
        }
    });
}

function chck_ItnraryExceedsDay() {
    var incr_num_plcbx = 0, height_of_plcbx = 0, PlcsTm_height = 0;
    var alert_height = "";
    PlcsTm_height = 0;
    $(".place-box-right:first").find(".place-box").each(function () {
        incr_num_plcbx++;
    });
    $(".place-box-right:first").find(".place").each(function () {
        PlcsTm_height = PlcsTm_height + $(this).height();
    });
    $(".place-box-right:first").find(".travel").each(function () {
        PlcsTm_height = PlcsTm_height + $(this).height();
    });
    PlcsTm_height = PlcsTm_height + ((incr_num_plcbx - 1) * 50);
    if (PlcsTm_height > 2150) {
        var id_todelete = "", height_todelete = 0;
        $(".place-box-right:first").find(".travel").each(function () {
            if (height_todelete == 0) {
                height_todelete = $(this).height();
                id_todelete = $(this).attr('id');
            }
            else {
                if (height_todelete < $(this).height()) {
                    height_todelete = $(this).height();
                    id_todelete = $(this).attr('id');
                }
            }
        });
        alert_height += id_todelete + "\n";
        var attr_of_plc = $("#" + id_todelete).next(".place").attr("id");

        next_tm_id_del = "";
        prev_tm_id_del = "";
        current_plc_id_del = $("#" + id_todelete).next(".place").find(".place-details").attr("id");
        if ($('#' + current_plc_id_del).parent(".place").prev(".travel").length) {
            prev_tm_id_del = $('#' + current_plc_id_del).parent(".place").prev(".travel").attr("id");
        }
        if ($('#' + current_plc_id_del).parent(".place").next(".travel").length) {
            next_tm_id_del = $('#' + current_plc_id_del).parent(".place").next(".travel").attr("id");
        }
        if (prev_tm_id_del != "" && next_tm_id_del != "") {
            prev_plc_name_del = $("#" + prev_tm_id_del).prev(".place").find(".p-left h4").text() + "," + $("#" + prev_tm_id_del).prev('.place').find(".p-left p").text();
            next_plc_name_del = $("#" + next_tm_id_del).next(".place").find(".p-left h4").text() + "," + $("#" + next_tm_id_del).next('.place').find(".p-left p").text();
            prev_plc_lat_del = $("#" + prev_tm_id_del).prev('.place').find('.hidlat').val();
            prev_plc_lng_del = $("#" + prev_tm_id_del).prev('.place').find('.hidlng').val();
            next_plc_lat_del = $("#" + next_tm_id_del).next('.place').find('.hidlat').val();
            next_plc_lng_del = $("#" + next_tm_id_del).next('.place').find('.hidlng').val();

            var distance_service = new google.maps.DistanceMatrixService();
            distance_service.getDistanceMatrix(
                             {
                                 origins: [prev_plc_name_del],
                                 destinations: [next_plc_name_del],
                                 travelMode: google.maps.TravelMode.DRIVING
                             }, callbackcar_deleteday);
        }
        else if (prev_tm_id_del == "" && next_tm_id_del != "") {
            $('#' + current_plc_id_del).parent(".place").next(".travel").remove();
            $('#' + current_plc_id_del).parent(".place").remove();
        }
        else if (prev_tm_id_del != "" && next_tm_id_del == "") {
            $('#' + current_plc_id_del).parent(".place").prev(".travel").remove();
            $('#' + current_plc_id_del).parent(".place").remove();
        }
        else if (prev_tm_id_del == "" && next_tm_id_del == "") {
            $('#' + current_plc_id_del).parent(".place").remove();
        }
        //$("#" + id_todelete).next(".place").remove();
        //$("#" + id_todelete).remove();
        alert_height += attr_of_plc + "\n";
    }
    else {
        alert_height += "Fits\n";
    }
}

function callbackcar_deleteday(response, status) {
    var dura = "", dist = "";
    if (status === "OK") {
        if (response.rows[0].elements[0].distance) {
            dist = response.rows[0].elements[0].distance.text;
            dura = response.rows[0].elements[0].duration.text;

            var splitTraveltime = dura.split(" ");
            for (var loopthr = 0, TravelTimeFind = splitTraveltime.length; loopthr < TravelTimeFind; loopthr++) {
                if (splitTraveltime[loopthr] == "min" || splitTraveltime[loopthr] == "mins") {
                    splitTraveltime[loopthr - 1] = Math.ceil(splitTraveltime[loopthr - 1] / 5) * 5;
                }
            }
            dura = splitTraveltime.join(" ");

            $("#" + current_plc_id_del).parent(".place").prev(".travel").find("p").text('Time taken by car is ' + dura);
            $("#" + current_plc_id_del).parent(".place").prev(".travel").find("p").append(' <img src="../Content/images/edit.png" alt="" class="editimg" style="height: 15px;width: 15px;">');
            $("#" + current_plc_id_del).parent(".place").prev(".travel").find(".hiddriving").val(dura);
            $("#" + current_plc_id_del).parent(".place").prev(".travel").css("height", "");
            var hours = "hours";
            var hour = "hour";
            var mins = "mins";
            if (dura.indexOf(hours) != -1 || dura.indexOf(hour) != -1) {
                var array = dura.split(' ');
                var amount = array[0] / .50;
                var timespent = amount * 58;
                if (dura.indexOf(mins) != -1) {
                    timespent = array[2] / .50 + timespent;
                    $("#" + current_plc_id_del).parent(".place").prev(".travel").css("height", timespent);
                }
                else {
                    $("#" + current_plc_id_del).parent(".place").prev(".travel").css("height", timespent);
                }
            }
            else if (dura.indexOf(mins) != -1) {
                var array = dura.split(' ');
                if (array[0] > 20) {
                    var amount = array[0] / 30;
                    var timespent = amount * 58;
                    $("#" + current_plc_id_del).parent(".place").prev(".travel").css("height", timespent);
                }
            }
        }
        else {
            dura = "No dura..";
        }
    }
    else {
        dura = "No dura..";
    }
    if (dura == "No dura..") {
        var dista = Haversine(prev_plc_lat_del, prev_plc_lng_del, next_plc_lat_del, next_plc_lng_del);
        var kmtomin1 = dista / 50;
        var kmtomin2 = kmtomin1 * 60;
        var kmtomin3 = kmtomin2.toFixed();
        var final_time = "";
        if (kmtomin3 > 60) {
            var kmtohrs = Math.floor(kmtomin3 / 60);
            var kmtomints = kmtomin3 % 60;
            kmtomints = Math.ceil(kmtomints / 5) * 5;
            final_time = kmtohrs + " hour " + kmtomints + " mins ";
        }
        else {
            if (kmtomin3 <= 1) {
                kmtomin3 = kmtomin3 + 4;
            }
            kmtomin3 = Math.ceil(kmtomin3 / 5) * 5;
            final_time = parseInt(kmtomin3) + " mins ";
        }
        $("#" + current_plc_id_del).parent(".place").prev(".travel").find("p").text('Time taken by car is ' + final_time);       
        $("#" + current_plc_id_del).parent(".place").prev(".travel").find(".hiddriving").val(final_time);

        $("#" + current_plc_id_del).parent(".place").prev(".travel").css("height", "");
        var hours = "hours";
        var hour = "hour";
        var mins = "mins";
        if (final_time.indexOf(hours) != -1 || final_time.indexOf(hour) != -1) {
            var array = final_time.split(' ');
            var amount = array[0] / .50;
            var timespent = amount * 58;
            if (final_time.indexOf(mins) != -1) {
                timespent = array[2] / .50 + timespent;
                $("#" + current_plc_id_del).parent(".place").prev(".travel").css("height", timespent);
            }
            else {
                $("#" + current_plc_id_del).parent(".place").prev(".travel").css("height", timespent);
            }
        }
        else if (final_time.indexOf(mins) != -1) {
            var array = final_time.split(' ');
            if (array[0] > 20) {
                var amount = array[0] / 30;
                var timespent = amount * 58;
                $("#" + current_plc_id_del).parent(".place").prev(".travel").css("height", timespent);
            }
        }
    }
    $("#" + current_plc_id_del).parent(".place").next(".travel").remove();
    $("#" + current_plc_id_del).parent(".place").remove();
}