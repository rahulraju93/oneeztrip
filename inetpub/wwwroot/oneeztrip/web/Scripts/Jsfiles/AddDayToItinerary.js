﻿$('#dayhead').on('click', '.trip-day', function () {
    keepTrackForsave++;
    var widthTodiv = 0;
    $('.city-row').each(function (increnumDays) {
        widthTodiv = increnumDays;
    });
    widthTodiv = widthTodiv + 2;
    if (widthTodiv > 1) {
        $(".mCSB_container").css('width', (widthTodiv * 370) + 'px');
    }
    var RearrangeChangeValueId = $(this).attr("id");
    var RearrangeChangeValue = $('#DateForRearrange_' + RearrangeChangeValueId).html();
    RearrangeChangeValue = ++RearrangeChangeValue;
    $('#DateForRearrange_' + RearrangeChangeValueId).html(RearrangeChangeValue);
    var cityrowid = $(this).parent().attr('id');
    sp_cityrowid = cityrowid.split('_');
    sp_spcityrowid = sp_cityrowid[1].split("");
    var SeparateValue = $('#dayhead').find('.city-listdet').eq(sp_spcityrowid[0] - 1).val();
    ++SeparateValue;
    var defineinside = sp_spcityrowid[0] - 1;
    $('#dayhead').find('.city-listdet').eq(sp_spcityrowid[0] - 1).val(SeparateValue);
    var incrbyone = parseInt(sp_spcityrowid[1]) + 1;
    var newid = sp_spcityrowid[0] + "" + incrbyone;
    var getcitynameid = $('#' + cityrowid).find('.trip-name').attr('id');
    var getcityname = $('#' + getcitynameid).text();
    addday_cityname = getcityname;
    var addday_citylat = ""; addday_citylng = "";
    $("#citypref li a").each(function () {
        if ($(this).html().trim() == getcityname.trim()) {
            var addday_citylatlng = $(this).data('value').split('||');
            addday_citylat = addday_citylatlng[0];
            addday_citylng = addday_citylatlng[1];
        }
    });
    newidnext = newid + 1;
    oldid = newid - 2;
    var id = $(this).attr('id');
    var ite = "<div class='city-row' id='cit_" + newid + "'>";
    ite += "<div class='trip-day' id='" + id + "'><a href='#'><span class='btn'><img src='../Content/images/add-icon.png' alt='' /></span>";
    ite += "<span class='txt'>Add Day</span></a> </div>";
    ite += "<div class='trip-date'> <span class='btn'>00</span><span class='txt'>Day</span> </div>";
    ite += "<div class='trip-name' id='tn_" + newid + "'>" + getcityname + "</div>";
    ite += "<figure class='city-row-img'><a href='#'><img alt='' src='../Content/images/del-icon.png' /></a></figure></div>";
    $('#' + cityrowid).after(ite);
    --newid;
    var nite = "<div class='city-row-details' id='ccol_" + newid + "' data-value='" + getcityname + "'>";
    nite += "<div class='time'>";
    nite += "<ul><li>06.00 AM</li><li>06.30 AM</li><li>07.00 AM</li><li>07.30 AM</li><li>08.00 AM</li><li>08.30 AM</li>";
    nite += "<li>09.00 AM</li><li>09.30 AM</li><li>10.00 AM</li><li>10.30 AM</li><li>11.00 AM</li><li>11.30 AM</li>";
    nite += "<li>12.00 PM</li><li>12.30 PM</li><li>01.00 PM</li><li>01.30 PM</li><li>02.00 PM</li><li>02.30 PM</li>";
    nite += "<li>03.00 PM</li><li>03.30 PM</li><li>04.00 PM</li><li>04.30 PM</li><li>05.00 PM</li><li>05.30 PM</li>";
    nite += "<li>06.00 PM</li><li>06.30 PM</li><li>07.00 PM</li><li>07.30 PM</li><li>08.00 PM</li><li>08.30 PM</li>";
    nite += "<li>09.00 PM</li><li>09.30 PM</li><li>10.00 PM</li><li>10.30 PM</li><li>11.00 PM</li><li>11.30 PM</li>";
    nite += "<li>12.00 PM</li><li>12.30 PM</li><li>01.00 AM</li><li>01.30 AM</li></ul>";
    nite += "</div>";
    nite += "<div class='place-box-right newPlcBoxRit' data-value='" + getcityname + "' id='col_" + newid + "'><input type='hidden' class='plcbx_hidlatlng' value='" + addday_citylat + "||" + addday_citylng + "'/><div id='plc_col_" + newid + "' class='place-box timing-details' style='height:2160px;'>";
    nite += "<div class='place-box timing-details'></div><div class='place-box timing-details'></div>";
    nite += "<div class='place-box timing-details'></div><div class='place-box timing-details'></div><div class='place-box timing-details'></div>";
    nite += "</div></div></div>";
    $('#ccol_' + oldid).after(nite);
    var ind = 1;
    $(".city-row").each(function () {
        var indstr = "";
        if (ind < 10) {
            indstr = '0';
        }
        $(this).find('.trip-date .btn').html(indstr + ind);
        ind = ind + 1;
    });
    var incre_for_cr = 0, incre_for_others = 0;
    $(".city-row").each(function () {
        var current_cn = $(this).find('.trip-name').text();
        if (current_cn == getcityname) {
            var increcr1 = incre_for_cr + 1;
            $(this).attr("id", "cit_" + sp_spcityrowid[0] + "" + increcr1);
            $(this).find(".trip-name").attr("id", "tn_" + sp_spcityrowid[0] + "" + increcr1);
            $('.city-row-details').eq(incre_for_others).attr("id", "ccol_" + sp_spcityrowid[0] + "" + incre_for_cr);
            $('.city-row-details').eq(incre_for_others).find('.place-box-right').eq(incre_for_others).attr("id", "col_" + sp_spcityrowid[0] + "" + incre_for_cr);
            $('.city-row-details').eq(incre_for_others).find('.place-box-right .place-box:first').eq(incre_for_others).attr("id", "plc_col_" + sp_spcityrowid[0] + "" + incre_for_cr);
            if ($(this).attr('id') == "cit_" + newid) {
                append_to_id = "plc_col_" + sp_spcityrowid[0] + "" + increcr1;
            }
            incre_for_cr++;
        }
        incre_for_others++;
    });
    var incrNoOfDays=0;
    $(".place-box-right").each(function () {        
        if($(this).hasClass("newPlcBoxRit"))
        {
            NumOfPrevDays = incrNoOfDays;
            $(this).removeClass("newPlcBoxRit");
        }
        incrNoOfDays++;
    });
    dragDropWithItinerary();
    var count = $('#dayhead').find('.city-row').length;
    var idd = id - 1;
    var count = $('#dayhead').find('.city-row').length;
    var NofDaysvar = "", CityListvar = "", latvar = "", lngvar = "";
    var SeparateCount = $('#dayhead').find('.city-listlat').length;
    for (var i = 0; i < SeparateCount; i++) {
        if (i == 0) {
            CityListvar += $('#citypref').find('li').eq(i).find('a').html();
            latvar += $('#dayhead').find('.city-listlat').eq(i).val();
            NofDaysvar += $('#dayhead').find('.city-listdet').eq(i).val();
            lngvar += $('#dayhead').find('.city-listlng').eq(i).val();
        }
        else {
            CityListvar += "|" + $('#citypref').find('li').eq(i).find('a').html();
            latvar += "|" + $('#dayhead').find('.city-listlat').eq(i).val();
            NofDaysvar += "|" + $('#dayhead').find('.city-listdet').eq(i).val();
            lngvar += "|" + $('#dayhead').find('.city-listlng').eq(i).val();
        }
    }
    $.getJSON('../Trip/UpdateCustomise', { UserId: $('#LoggedTripId').val(), TofDays: NofDaysvar, CityList: CityListvar, lat: latvar, lng: lngvar, NofDays: '1' }, function (data) {
    });
    fillnewday();
});

function fillnewday() {
    var strtDateIti = $("#TripStrtDate").val();
    var gotPlcToFill = false,gotActicity=true;
    var prevUsrRevTot = 0;
    var closest = 0, next_clslat = "", next_dist = "", next_placeid = "";
    var next_clslng = "", next_place = "";
    sortedarrayall.length = 0;
    var chckhotel = 0, chckrest = 0, chckatac = 0, attr_act_length = 0;
    var cntcityhotel = 0;
    cityhotel.length = 0;
    for (var loopnewday = 0, AddDayArrayLength = foraddday_array.length; loopnewday < AddDayArrayLength; loopnewday++) {
        if ((foraddday_array[loopnewday].city_name).trim() == addday_cityname.trim()) {
            for (var findcityll_loop = 0, CitiesLength = citieslist_array.length; findcityll_loop < CitiesLength; findcityll_loop++) {
                if (citieslist_array[findcityll_loop].city_name.trim() == addday_cityname.trim()) {
                    thiscitylat = citieslist_array[findcityll_loop].city_latitude;
                    thiscitylng = citieslist_array[findcityll_loop].city_longitude;
                }
            }
            attr_act_length = foraddday_array[loopnewday].FCattractions.length + foraddday_array[loopnewday].SCattractions.length + foraddday_array[loopnewday].TCattractions.length + foraddday_array[loopnewday].otherattractions.length;
            if (foraddday_array[loopnewday].hotels.length == 0) {
                chckhotel = 1;
                ishotel_avail = 1;
            }
            else {
                chckhotel = 0;
                ishotel_avail = 0;
            }
            if (foraddday_array[loopnewday].restaurants.length == 0) {
                chckrest = 1;
            }
            else {
                chckrest = 0;
            }
            
            if (attr_act_length == 0 || (attr_act_length / 4) < 1) {
                chckatac = 1;
            }
            else {
                chckatac = 0;
            }
            if (chckatac == 0) {
                hotelsdetails_nd = foraddday_array[loopnewday].hotelsfull.slice(0);
                restsdetails_nd = foraddday_array[loopnewday].restaurantsfull.slice(0);
                attsdetails_nd = foraddday_array[loopnewday].attractionsfull.slice(0);
                actsdetails_nd = foraddday_array[loopnewday].activitiesfull.slice(0)
                restforaddday_array = foraddday_array[loopnewday].restaurants.slice(0);
                FCattactforaddday_array = foraddday_array[loopnewday].FCattractions.slice(0);
                SCattactforaddday_array = foraddday_array[loopnewday].SCattractions.slice(0);
                TCattactforaddday_array = foraddday_array[loopnewday].TCattractions.slice(0);
                otherattactforaddday_array = foraddday_array[loopnewday].otherattractions.slice(0);
                attactforaddday_array = foraddday_array[loopnewday].attractions.slice(0);
                hotelforaddday_array = foraddday_array[loopnewday].hotels.slice(0);
                otherrestaurant_nd = foraddday_array[loopnewday].otherrestaurants.slice(0);
                acts_nd = foraddday_array[loopnewday].activitiesAddDay.slice(0);
 
                var getNumdays1 = NumOfPrevDays;
                var getdate1 = new Date(strtDateIti);
                var newdate = new Date(getdate1);
                newdate.setDate(newdate.getDate() + getNumdays1);
                var getday1 = newdate.getDay();

                FCattactforaddday_array.sort(function (obj1, obj2) {
                    return obj2.rating - obj1.rating
                });
                gotActicity = true;
                for (var plc_loopvar = 0; plc_loopvar < 8; plc_loopvar++) {
                    gotPlcToFill = true;
                    if (plc_loopvar == 3 || plc_loopvar == 6) {
                        mindist = 99999;
                        var plcStrtTime = "";
                        var plcEndtime = "";
                        if (plc_loopvar == 3) {
                            plcStrtTime = "13";
                            plcEndtime = "15";
                        }
                        else if (plc_loopvar == 6) {
                            plcStrtTime = "18";
                            plcEndtime = "22";
                        }
                        if (restforaddday_array.length > 0) {
                            for (var loopvar = 0, RestaurantsLength = restforaddday_array.length; loopvar < RestaurantsLength; loopvar++) {
                                 
                                var dist = Haversine(restforaddday_array[loopvar].geometry.location.lat(), restforaddday_array[loopvar].geometry.location.lng(), thiscitylat, thiscitylng);
                                    if (dist < mindist) {
                                        next_clslat = restforaddday_array[loopvar].geometry.location.lat();
                                        next_clslng = restforaddday_array[loopvar].geometry.location.lng();
                                        next_place = restforaddday_array[loopvar].name;
                                        next_placeid = restforaddday_array[loopvar].place_id;
                                        next_dist = dist;
                                        mindist = dist;
                                        gotPlcToFill = false;
                                        break;
                                    } 
                            }
                            restforaddday_array = restforaddday_array.filter(function (ele) {
                                return ele.place_id !== next_placeid;
                            });
                            foraddday_array[loopnewday].restaurants = restforaddday_array;
                        }
 
                        if (!gotPlcToFill) {
                            thiscitylat = next_clslat;
                            thiscitylng = next_clslng;
                            sortedarrayall.push({
                                plc_id: next_placeid,
                                plc_name: next_place,
                                plc_lat: next_clslat,
                                plc_lng: next_clslng,
                                plc_dist: next_dist
                            });
                        }
                        else
                        {
                            sortedarrayall.push({
                                plc_id: "Empty",
                                plc_name: "Empty",
                                plc_lat: "Empty",
                                plc_lng: "Empty",
                                plc_dist: "Empty"
                            });
                        }
                    }
                    else if (plc_loopvar == 0 || plc_loopvar == 7) {
                        if (chckhotel == 0) {
                            if (cntcityhotel == 0) {
                                mindist = 99999;
                                for (var loopvar = 0, HotelsLength = hotelforaddday_array.length; loopvar < HotelsLength; loopvar++) {
                                    var dist = Haversine(hotelforaddday_array[loopvar].plc_lat, hotelforaddday_array[loopvar].plc_lng, thiscitylat, thiscitylng);
                                    if (dist < mindist) {
                                        next_clslat = hotelforaddday_array[loopvar].plc_lat;
                                        next_clslng = hotelforaddday_array[loopvar].plc_lng;
                                        next_place = hotelforaddday_array[loopvar].plc_name;
                                        next_placeid = hotelforaddday_array[loopvar].plc_id;
                                        next_dist = dist;
                                        mindist = dist;
                                    }
                                }
                                cityhotel.push({
                                    plc_id: next_placeid,
                                    plc_name: next_place,
                                    plc_lat: next_clslat,
                                    plc_lng: next_clslng,
                                    plc_dist: next_dist
                                });

                                thiscitylat = next_clslat;
                                thiscitylng = next_clslng;
                                cntcityhotel = 1;
                            }
                            else if (cntcityhotel == 1) {
                                next_clslat = cityhotel[0].plc_lat;
                                next_clslng = cityhotel[0].plc_lng;
                                next_place = cityhotel[0].plc_name;
                                next_placeid = cityhotel[0].plc_id;
                                next_dist = dist;
                                mindist = "";
                            }
                            sortedarrayall.push({
                                plc_id: next_placeid,
                                plc_name: next_place,
                                plc_lat: next_clslat,
                                plc_lng: next_clslng,
                                plc_dist: next_dist
                            });
                        }
                        else {
                            sortedarrayall.push({
                                plc_id: "Empty",
                                plc_name: "Empty",
                                plc_lat: "Empty",
                                plc_lng: "Empty",
                                plc_dist: "Empty"
                            });
                        }
                    }
                    else {
                        prevUsrRevTot = 0;
                        mindist = 99999;
                        
                        if (FCattactforaddday_array.length>0) {
                            for (var loopvar = 0, AttractionsActivities = FCattactforaddday_array.length; loopvar < AttractionsActivities; loopvar++) {
                                if (FCattactforaddday_array[loopvar].rating > 4.5) {
                                    var dist = Haversine(FCattactforaddday_array[loopvar].geometry.location.lat(), FCattactforaddday_array[loopvar].geometry.location.lng(), thiscitylat, thiscitylng);
                                    if (dist < mindist) {
                                        next_clslat = FCattactforaddday_array[loopvar].geometry.location.lat();
                                        next_clslng = FCattactforaddday_array[loopvar].geometry.location.lng();
                                        next_place = FCattactforaddday_array[loopvar].name;
                                        next_placeid = FCattactforaddday_array[loopvar].place_id;
                                        next_dist = dist;
                                        mindist = dist;
                                        gotPlcToFill = false;
                                        break;
                                    }
                                }
                            }
                            FCattactforaddday_array = FCattactforaddday_array.filter(function (ele) {
                                return ele.place_id !== next_placeid;
                            });
                            next_placeid = "attr||" + next_placeid;
                            foraddday_array[loopnewday].FCattractions = FCattactforaddday_array;
                        }
                        if (acts_nd.length > 0 && gotPlcToFill && gotActicity) {
                            for (var loopvar = 0, ActivLength = acts_nd.length; loopvar < ActivLength; loopvar++) {
                                if (acts_nd[loopvar].durationInMillis <= 7200000 && acts_nd[loopvar].recommendationScore > 75) {
                                    var actlat = acts_nd[loopvar].latLng.split(",");
                                    var dist = Haversine(actlat[0], actlat[1], thiscitylat, thiscitylng);
                                    if (dist < mindist) {
                                        next_clslat = actlat[0];
                                        next_clslng = actlat[1];
                                        next_place = acts_nd[loopvar].title;
                                        next_placeid = "acti||"+acts_nd[loopvar].id;
                                        next_dist = dist;
                                        mindist = dist;
                                        gotPlcToFill = false;
                                        gotActicity = false;
                                        break;
                                    }
                                }
                            }
                            if (next_placeid.toString().indexOf("acti||") != -1) {
                                var spltPlcId = next_placeid.split("||");
                                acts_nd = acts_nd.filter(function (ele) {
                                    return ele.id !== spltPlcId[1];
                                });
                                foraddday_array[loopnewday].activitiesAddDay = acts_nd;
                            }
                        }
                        if (FCattactforaddday_array.length > 0 && gotPlcToFill) {
                            for (var loopvar = 0, AttractionsActivities = FCattactforaddday_array.length; loopvar < AttractionsActivities; loopvar++) {
                                if (FCattactforaddday_array[loopvar].rating > 3.5) {
                                    var dist = Haversine(FCattactforaddday_array[loopvar].geometry.location.lat(), FCattactforaddday_array[loopvar].geometry.location.lng(), thiscitylat, thiscitylng);
                                    if (dist < mindist) {
                                        next_clslat = FCattactforaddday_array[loopvar].geometry.location.lat();
                                        next_clslng = FCattactforaddday_array[loopvar].geometry.location.lng();
                                        next_place = FCattactforaddday_array[loopvar].name;
                                        next_placeid = FCattactforaddday_array[loopvar].place_id;
                                        next_dist = dist;
                                        mindist = dist;
                                        gotPlcToFill = false;
                                        break;
                                    }
                                }
                            }
                            FCattactforaddday_array = FCattactforaddday_array.filter(function (ele) {
                                return ele.place_id !== next_placeid;
                            });
                            next_placeid = "attr||" + next_placeid;
                            foraddday_array[loopnewday].FCattractions = FCattactforaddday_array;
                        }
                        if (acts_nd.length > 0 && gotPlcToFill && gotActicity) {
                            for (var loopvar = 0, ActivLength = acts_nd.length; loopvar < ActivLength; loopvar++) {
                                if (acts_nd[loopvar].durationInMillis <= 7200000 && acts_nd[loopvar].recommendationScore > 50) {
                                    var actlat = acts_nd[loopvar].latLng.split(",");
                                    var dist = Haversine(actlat[0], actlat[1], thiscitylat, thiscitylng);
                                    if (dist < mindist) {
                                        next_clslat = actlat[0];
                                        next_clslng = actlat[1];
                                        next_place = acts_nd[loopvar].title;
                                        next_placeid = "acti||"+acts_nd[loopvar].id;
                                        next_dist = dist;
                                        mindist = dist;
                                        gotPlcToFill = false;
                                        gotActicity = false;
                                        break;
                                    }
                                }
                            }
                            if (next_placeid.toString().indexOf("acti||") != -1) {
                                var spltPlcId = next_placeid.split("||");
                                acts_nd = acts_nd.filter(function (ele) {
                                    return ele.id !== spltPlcId[1];
                                });
                                foraddday_array[loopnewday].activitiesAddDay = acts_nd;
                            }
                        }
                        if (FCattactforaddday_array.length > 0 && gotPlcToFill) {
                            for (var loopvar = 0, AttractionsActivities = FCattactforaddday_array.length; loopvar < AttractionsActivities; loopvar++) {
                                if (FCattactforaddday_array[loopvar].rating > 2.5) {
                                    var dist = Haversine(FCattactforaddday_array[loopvar].geometry.location.lat(), FCattactforaddday_array[loopvar].geometry.location.lng(), thiscitylat, thiscitylng);
                                    if (dist < mindist) {
                                        next_clslat = FCattactforaddday_array[loopvar].geometry.location.lat();
                                        next_clslng = FCattactforaddday_array[loopvar].geometry.location.lng();
                                        next_place = FCattactforaddday_array[loopvar].name;
                                        next_placeid = FCattactforaddday_array[loopvar].place_id;
                                        next_dist = dist;
                                        mindist = dist;
                                        gotPlcToFill = false;
                                        break;
                                    }
                                }
                            }
                            FCattactforaddday_array = FCattactforaddday_array.filter(function (ele) {
                                return ele.place_id !== next_placeid;
                            });
                            next_placeid = "attr||" + next_placeid;
                            foraddday_array[loopnewday].FCattractions = FCattactforaddday_array;
                        }
                        if (acts_nd.length > 0 && gotPlcToFill && gotActicity) {
                            for (var loopvar = 0, ActivLength = acts_nd.length; loopvar < ActivLength; loopvar++) {
                                if (acts_nd[loopvar].durationInMillis <= 7200000) {
                                    var actlat = acts_nd[loopvar].latLng.split(",");
                                    var dist = Haversine(actlat[0], actlat[1], thiscitylat, thiscitylng);
                                    if (dist < mindist) {
                                        next_clslat = actlat[0];
                                        next_clslng = actlat[1];
                                        next_place = acts_nd[loopvar].title;
                                        next_placeid = "acti||"+acts_nd[loopvar].id;
                                        next_dist = dist;
                                        mindist = dist;
                                        gotPlcToFill = false;
                                        gotActicity = false;
                                        break;
                                    }
                                }
                            }
                            if (next_placeid.toString().indexOf("acti||") != -1) {
                                var spltPlcId = next_placeid.split("||");
                                acts_nd = acts_nd.filter(function (ele) {
                                    return ele.id !== spltPlcId[1];
                                });
                                foraddday_array[loopnewday].activitiesAddDay = acts_nd;
                            }
                        }
                        if (FCattactforaddday_array.length > 0 && gotPlcToFill) {
                            for (var loopvar = 0, AttractionsActivities = FCattactforaddday_array.length; loopvar < AttractionsActivities; loopvar++) {
                                var dist = Haversine(FCattactforaddday_array[loopvar].geometry.location.lat(), FCattactforaddday_array[loopvar].geometry.location.lng(), thiscitylat, thiscitylng);
                                if (dist < mindist) {
                                    next_clslat = FCattactforaddday_array[loopvar].geometry.location.lat();
                                    next_clslng = FCattactforaddday_array[loopvar].geometry.location.lng();
                                    next_place = FCattactforaddday_array[loopvar].name;
                                    next_placeid = FCattactforaddday_array[loopvar].place_id;
                                    next_dist = dist;
                                    mindist = dist;
                                    gotPlcToFill = false;
                                    break;
                                }
                            }
                            FCattactforaddday_array = FCattactforaddday_array.filter(function (ele) {
                                return ele.place_id !== next_placeid;
                            });
                            next_placeid = "attr||" + next_placeid;
                            foraddday_array[loopnewday].FCattractions = FCattactforaddday_array;
                        }
                                                
                        if (!gotPlcToFill) {

                            thiscitylat = next_clslat;
                            thiscitylng = next_clslng;

                            sortedarrayall.push({
                                plc_id: next_placeid,
                                plc_name: next_place,
                                plc_lat: next_clslat,
                                plc_lng: next_clslng,
                                plc_dist: next_dist
                            });
                        }
                        else
                        {
                            sortedarrayall.push({
                                plc_id: "Empty",
                                plc_name: "Empty",
                                plc_lat: "Empty",
                                plc_lng: "Empty",
                                plc_dist: "Empty"
                            });
                        }
                    }
                }
                displaytripfornewday();
            }
        }
        break;
    }
}

function displaytripfornewday() {
    for (var loopvar_sort = 0; loopvar_sort < 8; loopvar_sort++) {
        loopvarvalue = loopvar_sort;
        if (loopvar_sort == 3 || loopvar_sort == 6) {
            var place = restsdetails_nd.filter(function (obj) {
                return obj.place_id == sortedarrayall[loopvarvalue].plc_id;
            });
            if (place.length) {
                var placediv1 = "";
                    if (!place[0].photos) {
                        placediv1 = "<div class='place'><div class='place-details' id=" + place[0].place_id + " style ='height:100px;'><input type='hidden' value='1' class='frmApi'/><figure class='place-img'>";
                        placediv1 += "<img src='../content/images/AppImages/small-img4.jpg' alt='' /></figure><div class='place-cont'>";
                        placediv1 += "<div class='p-left'><h4>" + place[0].name + "</h4><p style='height:26px; overflow-y:hidden;'>" + place[0].vicinity + "</p><input type='hidden' class='hidlat' value='" + place[0].geometry.location.lat() + "'/><input type='hidden' class='hidlng' value='" + place[0].geometry.location.lng() + "'/><span>Restaurant.</span><br><span id='sp_" + place[0].place_id + "' class='sphrs'>";
                        placediv1 += "</div><div class='p-right'>";
                        placediv1 += "<ul><li><a href='#'><img src='../Content/images/del-icon.png' alt='' class='deleteplace' /></a></li><li><a href='#infowindow' class='fancybox' onclick='return infoattraction(\"" + place[0].place_id + "\",\"" + place[0].geometry.location.lat() + "\",\"" + place[0].geometry.location.lng() + "\");' ><img src='../Content/images/info-icon.png' alt='' /></a></li> </ul>";
                        placediv1 += "</div> </div> </div>";
                        placediv1 += "</div>";
                    }
                    else {
                        placediv1 = "<div class='place'><div class='place-details' id=" + place[0].place_id + " style ='height:100px;'><input type='hidden' value='1' class='frmApi'/><figure class='place-img'>";

                        placediv1 += "<img src='" + place[0].photos[0].getUrl({ 'maxWidth': 50, 'maxHeight': 50 }) + "' /></figure><div class='place-cont'>";
                        placediv1 += "<div class='p-left'><h4>" + place[0].name + "</h4><p style='height:26px; overflow-y:hidden;'>" + place[0].vicinity + "</p><input type='hidden' class='hidlat' value='" + place[0].geometry.location.lat() + "'/><input type='hidden' class='hidlng' value='" + place[0].geometry.location.lng() + "'/><span>Restaurant.</span><br><span id='sp_" + place[0].place_id + "' class='sphrs'>";
                        placediv1 += "</div><div class='p-right'>";
                        placediv1 += "<ul><li><a href='#'><img src='../Content/images/del-icon.png' alt='' class='deleteplace'/></a></li><li><a href='#infowindow' class='fancybox' onclick='return infoattraction(\"" + place[0].place_id + "\",\"" + place[0].geometry.location.lat() + "\",\"" + place[0].geometry.location.lng() + "\");' ><img src='../Content/images/info-icon.png' alt='' /></a></li> </ul>";
                        placediv1 += "</div> </div> </div>";
                        placediv1 += "</div>";
                    }
                    $("#" + append_to_id).append(placediv1);
            }
        }
        else if (loopvar_sort == 0 || loopvar_sort == 7) {
            if (ishotel_avail == 0) {
                var placdiv = "";

                var place = hotelsdetails_nd.filter(function (obj) {
                    return obj.hotelId == sortedarrayall[loopvarvalue].plc_id;
                });
                if (loopvar_sort == 7) {
                    if (place.length) {
                        var placediv1 = "<div class='place'><div class='place-details' id=" + place[0].hotelId + " style ='height:86px;'><input type='hidden' value='1' class='frmApi'/><figure class='place-img'>";
                        placediv1 += "<img src='http://media3.expedia.com" + place[0].thumbNailUrl + "' alt='' /></figure><div class='place-cont'>";
                        placediv1 += "<div class='p-left'><h4>" + place[0].name + "</h4><p style='height:26px; overflow-y:hidden;'>" + place[0].address1 + "</p><input type='hidden' class='hidlat' value='" + place[0].latitude + "'/><input type='hidden' class='hidlng' value='" + place[0].longitude + "'/><span>Hotel.</span>";
                        
                        placediv1 += "</div><div class='p-right'>";
                        placediv1 += "<ul><li><a href='#'><img src='../Content/images/del-icon.png' alt='' class='deleteplace'/></a></li>";
                        placediv1 += "<li><a href='#infowindow_hotels_activities' class='fancybox' onclick='return infohotel(\"" + place[0].hotelId + "\")'><img src='../Content/images/info-icon.png' alt='' /></a></li> </ul>";
                        placediv1 += "</div> </div> </div>";
                        placediv1 += "</div>";
                        $("#" + append_to_id).append(placediv1);
                    }
                }
            }
        }
        else {
            var placeid = sortedarrayall[loopvarvalue].plc_id;
            var findid = placeid.split('||');
            var place = "";
            if (findid[0] == "attr") {
                place = attsdetails_nd.filter(function (obj) {
                    return obj.place_id == findid[1];
                });
                var placediv1 = "";
                if (place.length) {
                    if (!place[0].photos) {
                        placediv1 = "<div class='place'><div class='place-details' id=" + place[0].place_id + " style ='height:200px;'><input type='hidden' value='1' class='frmApi'/><figure class='place-img'>";
                        placediv1 += "<img src='../content/images/AppImages/small-img4.jpg' alt='' /></figure><div class='place-cont'>";
                        placediv1 += "<div class='p-left'><h4>" + place[0].name + "</h4><p style='height:26px; overflow-y:hidden;'>" + place[0].vicinity + "</p><input type='hidden' class='hidlat' value='" + place[0].geometry.location.lat() + "'/><input type='hidden' class='hidlng' value='" + place[0].geometry.location.lng() + "'/><span>Attraction.</span><br><span id='sp_" + place[0].place_id + "' class='sphrs'>";
                        placediv1 += "</div><div class='p-right'>";
                        placediv1 += "<ul><li><a href='#'><img src='../Content/images/del-icon.png' alt='' class='deleteplace' /></a></li><li><a href='#infowindow' class='fancybox' onclick='return infoattraction(\"" + place[0].place_id + "\",\"" + place[0].geometry.location.lat() + "\",\"" + place[0].geometry.location.lng() + "\");' ><img src='../Content/images/info-icon.png' alt='' /></a></li> </ul>";
                        placediv1 += "</div> </div> </div>";
                        placediv1 += "</div>";
                    }
                    else {
                        placediv1 = "<div class='place'><div class='place-details' id=" + place[0].place_id + " style ='height:200px;'><input type='hidden' value='1' class='frmApi'/><figure class='place-img'>";
                        placediv1 += "<img src='" + place[0].photos[0].getUrl({ 'maxWidth': 50, 'maxHeight': 50 }) + "' /></figure><div class='place-cont'>";
                        placediv1 += "<div class='p-left'><h4>" + place[0].name + "</h4><p style='height:26px; overflow-y:hidden;'>" + place[0].vicinity + "</p><input type='hidden' class='hidlat' value='" + place[0].geometry.location.lat() + "'/><input type='hidden' class='hidlng' value='" + place[0].geometry.location.lng() + "'/><span>Attraction.</span><br><span id='sp_" + place[0].place_id + "' class='sphrs'>";
                        placediv1 += "</div><div class='p-right'>";
                        placediv1 += "<ul><li><a href='#'><img src='../Content/images/del-icon.png' alt='' class='deleteplace' /></a></li><li><a href='#infowindow' class='fancybox' onclick='return infoattraction(\"" + place[0].place_id + "\",\"" + place[0].geometry.location.lat() + "\",\"" + place[0].geometry.location.lng() + "\");' ><img src='../Content/images/info-icon.png' alt='' /></a></li> </ul>";
                        placediv1 += "</div> </div> </div>";
                        placediv1 += "</div>";
                    }
                    $("#" + append_to_id).append(placediv1);
                }
            }
            else if (findid[0] == "acti") {
                place = actsdetails_nd.filter(function (obj) {
                    return obj.id == findid[1];
                });
                if (place.length) {
                    var ActivityDur = place[0].durationInMillis;
                    var minutes = ActivityDur / (1000 * 60);
                    minutes = ((minutes / 30) * 100) / 2;
                    var ltlng = place[0].latLng.split(',');

                    var placediv1 = "<div class='place' id='place" + place[0].id + "'><div class='place-details' id=" + place[0].id + " style ='height:" + minutes + "px;'><input type='hidden' value=" + ltlng[0] + " class='placeatt_lat'/><input type='hidden' value=" + ltlng[1] + " class='placeatt_lng'/>";
                    placediv1 += "<input type='hidden' value='1' class='frmApi'/><input type='hidden' value=" + place[0].title + " class='placeatt_name'/><figure class='place-img'>";
                    placediv1 += "<img src=" + place[0].imageUrl + " alt='' /></figure><div class='place-cont'>";
                    placediv1 += "<div class='p-left'><h4>" + place[0].title + "</h4><p style='height:26px; overflow-y:hidden;'>" + place[0].duration + "</p><input type='hidden' class='hidlat' value='" + ltlng[0] + "'/><input type='hidden' class='hidlng' value='" + ltlng[1] + "'/><span>Activity.</span><br><span id='sp_" + place[0].id + "' class='sphrs'></span>";
                    placediv1 += "</div><div class='p-right'>";
                    placediv1 += "<ul><li><a href='#'><img src='../Content/images/del-icon.png' class='deleteplace' alt=''/></a></li><li><a href='#infowindow_hotels_activities' class='fancybox' onclick='getActivityInfo(\"" + place[0].id + "\",\"" + addday_cityname + "\");' ><img src='../Content/images/info-icon.png' alt='' /></a></li> </ul>";
                    placediv1 += "</div> </div> </div>";
                    placediv1 += "</div>";

                    //var placediv1 = "<div class='place'><div class='place-details' id=" + place[0].id + " style ='height:200px;'><input type='hidden' value='1' class='frmApi'/><figure class='place-img'>";
                    //placediv1 += "<img src='" + place[0].Images + "' /></figure><div class='place-cont'>";
                    //placediv1 += "<div class='p-left'><h4>" + place[0].title + "</h4><p style='height:26px; overflow-y:hidden;'>" + place[0].locations_text + "</p><input type='hidden' class='hidlat' value='" + place[0].start_Latitude + "'/><input type='hidden' class='hidlng' value='" + place[0].start_Longitude + "'/><span>Activity.</span><br><span id='sp_" + place[0].id + "' class='sphrs'>";
                    //placediv1 += "</div><div class='p-right'>";
                    //placediv1 += "<ul><li><a href='#'><img src='../Content/images/del-icon.png' alt='' class='deleteplace' /></a></li><li><a href='#infowindow_hotels_activities' class='fancybox' onclick='return info_for_hotels(\"" + place[0].id + "\",\"" + append_to_id + "\")'><img src='../Content/images/info-icon.png' alt='' /></a></li> </ul>";
                    //placediv1 += "</div> </div> </div>";
                    //placediv1 += "</div>";
                    $("#" + append_to_id).append(placediv1);
                }
            }
        }
    }
    $("#" + append_to_id).append('<div class="place-box timing-details"></div><div class="place-box timing-details"></div><div class="place-box timing-details"></div><div class="place-box timing-details"></div><div class="place-box timing-details"></div>');
    onresizingplc();
    showtravelmodeforaddday();
}

function showtravelmodeforaddday() {
    if ($('#' + append_to_id + ' .place').length > 0) {
        $("#" + append_to_id).find(".place + .place").before($('<div class="travel"><a href="#" style="float:right;"><img src="../Content/images/del_travel.png" class="travedelimg"/></a><p>Time taken by car is -- <img src="../Content/images/edit.png" alt="" class="editimg" style="height: 15px;width: 15px;"></p><ul><li><a href="#" data-value="car" class="active"><img src="../Content/images/car-icon.png" alt=""/></a></li><li><a href="#" data-value="train"><img src="../Content/images/train-icon.png" alt="" /></a></li><li><a href="#" data-value="plane"><img src="../Content/images/plane-icon.png" alt="" /></a></li><li><a href="#" data-value="walk"><img src="../Content/images/walk-icon.png" alt="" /></a></li><li class="last"><a href="#" data-value="bicycle"><img src="../Content/images/scooter-icon.png" alt="" /></a></li><input type="hidden" class="hiddriving" value=""/></ul></div>'));
        setid_for_alltravelmodes();
        showdurationforaddday();
    }
    else {
        setTimeout(function () { showtravelmodeforaddday(); }, 1000);
    }
}

function showdurationforaddday() {
    var plc_origin = "", plc_destination = "";
    var plc_origin_addr = "", plc_destin_addr = "", plc_origin_addr1 = "", plc_destin_addr1 = "";
    var ori_lat = "", ori_lng = "", des_lat = "", des_lng = "";
    incr_td = 0, incre_printtravelduration = 0;
    forfirsttimetravelduration.length = 0;
    traveldurationarray.length = 0;

    $("#" + append_to_id + " .travel").each(function () {
        plc_destination = $(this).next('.place').find(".p-left h4").text();
        plc_origin_addr1 = $(this).prev('.place').find(".p-left h4").text() + "," + $(this).prev('.place').find(".p-left p").text();
        plc_destin_addr1 = $(this).next('.place').find(".p-left h4").text() + "," + $(this).next('.place').find(".p-left p").text();
        plc_origin_addr = $(this).prev('.place').find(".p-left p").text();
        plc_destin_addr = $(this).next('.place').find(".p-left p").text();
        ori_lat = $(this).prev('.place').find('.hidlat').val();
        ori_lng = $(this).prev('.place').find('.hidlng').val();
        des_lat = $(this).next('.place').find('.hidlat').val();
        des_lng = $(this).next('.place').find('.hidlng').val();

        forfirsttimetravelduration.push({
            ori_lt: ori_lat,
            ori_ln: ori_lng,
            des_lt: des_lat,
            des_ln: des_lng,
            orig: plc_origin_addr1,
            desti: plc_destin_addr1,
            origh: plc_origin_addr,
            destih: plc_destin_addr
        });
    });

    array_lengthfrtraveldura = forfirsttimetravelduration.length;
    loopthrougharraytravelmode_newday();
}

function loopthrougharraytravelmode_newday() {
    var distance_service = new google.maps.DistanceMatrixService();
    var orlat = forfirsttimetravelduration[incr_td].ori_lt;
    var orlng = forfirsttimetravelduration[incr_td].ori_ln;
    var delat = forfirsttimetravelduration[incr_td].des_lt;
    var delng = forfirsttimetravelduration[incr_td].des_ln;
    var orilat_lng = new google.maps.LatLng(orlat, orlng);
    var deslat_lng = new google.maps.LatLng(delat, delng);

    distance_service.getDistanceMatrix(
         {
             origins: [orilat_lng],
             destinations: [deslat_lng],
             travelMode: google.maps.TravelMode.DRIVING
         }, callbackcar_newday);
}

function callbackcar_newday(response, status) {
    if (status === "OK") {
        var dist = "", dura = "";
        if (response.rows[0].elements[0].distance) {
            dist = response.rows[0].elements[0].distance.text;
            dura = response.rows[0].elements[0].duration.text;
            traveldurationarray.push({
                distance: dist,
                duration: dura
            });
        }
        else {
            traveldurationarray.push({
                distance: dist,
                duration: "No dura.."
            });
        }
        incr_td++;
        if (incr_td < array_lengthfrtraveldura) {
            loopthrougharraytravelmode_newday();
        }
        else {
            printdurationfortravelmode_newday();
        }
    }
    else {
        traveldurationarray.push({
            distance: "--",
            duration: "No dura.."
        });
        incr_td++;
        if (incr_td < array_lengthfrtraveldura) {
            loopthrougharraytravelmode_newday();
        }
        else {
            printdurationfortravelmode_newday();
        }
    }
}

function finalcallfortravelmode_newday() {
    var eachtravel_id_newday = [];
    for (var index_loopvariable = 0, TravelModesLength = traveldurationarray.length; index_loopvariable < TravelModesLength; index_loopvariable++) {
        $("#" + append_to_id + " .travel").each(function () {
            eachtravel_id_newday.push($(this).attr('id'));
        });

        if (traveldurationarray[index_loopvariable].duration == "No dura..") {
            var dista = Haversine(forfirsttimetravelduration[index_loopvariable].ori_lt, forfirsttimetravelduration[index_loopvariable].ori_ln, forfirsttimetravelduration[index_loopvariable].des_lt, forfirsttimetravelduration[index_loopvariable].des_ln);
            var kmtomin1 = dista / 40;
            var kmtomin2 = kmtomin1 * 60;
            var kmtomin3 = kmtomin2.toFixed();
            var final_time = "";
            if (kmtomin3 > 60) {
                var kmtohrs = Math.floor(kmtomin3 / 60);
                var kmtomints = kmtomin3 % 60;
                kmtomints = Math.ceil(kmtomints / 5) * 5;
                final_time = kmtohrs + " hour " + kmtomints + " mins ";
            }
            else {
                if (kmtomin3 <= 1) {
                    kmtomin3 = kmtomin3 + 4;
                }
                kmtomin3 = Math.ceil(kmtomin3 / 5) * 5;
                final_time = parseInt(kmtomin3) + " mins ";
            }
            traveldurationarray[index_loopvariable].duration = final_time;
            var thistravelid_nd = eachtravel_id_newday[index_loopvariable];
            $("#" + thistravelid_nd).find("p").text('Time taken by car is ' + traveldurationarray[index_loopvariable].duration);
            $("#" + thistravelid_nd).find("p").append(' <img src="../Content/images/edit.png" alt="" class="editimg" style="height: 15px;width: 15px;">');
            $("#" + thistravelid_nd).find(".hiddriving").val(traveldurationarray[index_loopvariable].duration);
            $("#" + thistravelid_nd).css("height", "");
            var hours = "hours";
            var hour = "hour";
            var mins = "mins";
            if (final_time.indexOf(hour) != -1) {
                var array = final_time.split(' ');
                var amount = array[0] / .50;
                if (array[0] < 50) {
                    amount = array[0] / .50;
                }
                else {
                    amount = 50 / .50;
                }
                var timespent = amount * 58;
                if (final_time.indexOf(mins) != -1) {
                        timespent = array[2] / .50 + timespent;
                        $("#" + thistravelid_nd).css("height", timespent);
                }
                else {
                    $("#" + thistravelid_nd).css("height", timespent);
                }
            }
            else if (final_time.indexOf(mins) != -1) {
                var array = final_time.split(' ');
                if (array[0] > 20) {
                    var amount = array[0] / 30;
                    var timespent = amount * 58;
                    $("#" + thistravelid_nd).css("height", timespent);
                }
            }
        }
    }
    ChangeId_after_AddDay();
}

function printdurationfortravelmode_newday() {
    $("#" + append_to_id + " .travel").each(function () {
        var travel_duris = traveldurationarray[incre_printtravelduration].duration;
        var splitTraveltime = traveldurationarray[incre_printtravelduration].duration.split(' ');
        for (var loopthr = 0, TravelTimeFind = splitTraveltime.length; loopthr < TravelTimeFind; loopthr++) {
            if (splitTraveltime[loopthr] == "min" || splitTraveltime[loopthr] == "mins") {
                splitTraveltime[loopthr - 1] = Math.ceil(splitTraveltime[loopthr - 1] / 5) * 5;
            }
        }
        traveldurationarray[incre_printtravelduration].duration = splitTraveltime.join(" ");
        $(this).find("p").text('Time taken by car is ' + traveldurationarray[incre_printtravelduration].duration);
        $(this).find("p").append(' <img src="../Content/images/edit.png" alt="" class="editimg" style="height: 15px;width: 15px;">');
        $(this).find(".hiddriving").val(traveldurationarray[incre_printtravelduration].duration);
        incre_printtravelduration++;
        var hours = "hours";
        var hour = "hour";
        var mins = "mins";
        if (travel_duris.indexOf(hour) != -1) {
            var array = travel_duris.split(' ');
            var amount = array[0] / .50;
            if (array[0] < 50) {
                amount = array[0] / .50;
            }
            else {
                amount = 50 / .50;
            }
            var timespent = amount * 58;
            if (travel_duris.indexOf(mins) != -1) {
                    timespent = array[2] / .50 + timespent;
                    $(this).css("height", timespent);
            }
            else {
                $(this).css("height", timespent);
            }
        }
        else if (travel_duris.indexOf(mins) != -1) {
            var array = travel_duris.split(' ');
            if (array[0] > 20) {
                var amount = array[0] / 30;
                var timespent = amount * 58;
                $(this).css("height", timespent);
            }
        }
    });
    finalcallfortravelmode_newday();
}

function apicallforaddday_onretrieve() {
    if (isaddnewcity > 0) {
        sortedarrayall.length = 0;
        rest_array.length = 0;
        otherrestaurant.length
        restlist_array.length = 0;
        attraction_array.length = 0;
        otherattraction.length = 0;
        rest_arraynew.length = 0;
        att_actarray.length = 0;
        hotel_array.length = 0;
        cityhotel.length = 0;
        hotel_arrayfull.length = 0;
        onretrieve_addday_identify = 1;
        for (var loopv = 0, AddDayLength = foraddday_array.length; loopv < AddDayLength; loopv++) {
            if (foraddday_array[loopv].hotelsfull == "" && foraddday_array[loopv].attractionsfull == "" && foraddday_array[loopv].restaurantsfull == "") {
                cityname_foradd_day = foraddday_array[loopv].city_name;
                for (var inloopvar = 0, CitiesLength = citieslist_array.length; inloopvar < CitiesLength; inloopvar++) {
                    if (citieslist_array[inloopvar].city_name.trim() == cityname_foradd_day.trim()) {
                        add_day_citylatitude = citieslist_array[inloopvar].city_latitude;
                        add_day_citylongitude = citieslist_array[inloopvar].city_longitude;
                    }
                }
                $.getJSON('../Trip/EanHotelRequest', { lat: add_day_citylatitude, lng: add_day_citylongitude }, function (response) {
                    var citynameact = citieslist_array[incre_for_oc].city_name;
                    var CityForHotelsReq = citynameact;
                    var chkForHoteldetails = 0;
                    for (var loopvarHotels = 0, CityHotelsLength = hotelsForCities.length; loopvarHotels < CityHotelsLength; loopvarHotels++) {
                        if (hotelsForCities[loopvarHotels].cityName.trim() == CityForHotelsReq.trim()) {
                            chkForHoteldetails = 1;
                        }
                    }
                    if (chkForHoteldetails == 0) {
                        hotelsForCities.push({
                            cityName: CityForHotelsReq,
                            Hotels: response
                        });
                    }
                    UserpreferenceAttraction();
                });
            }
        }
    }
}

function ChangeId_after_AddDay() {
    var nxt_id_plc = 0;
    var split_plcbx_id = append_to_id.split('_');
    var calc_nxt_id = split_plcbx_id[2].split('');
    $(".place-box-right").each(function () {
        if ($(this).data('value').trim() == addday_cityname.trim()) {
            $(this).attr('id', 'col_' + calc_nxt_id[0] + '' + nxt_id_plc);
            $(this).parents('.city-row-details').attr('id', "ccol_" + calc_nxt_id[0] + '' + nxt_id_plc);
            if ($(this).find('.place-box').attr('id') != null || $(this).find('.place-box').attr('id') != "" || $(this).find('.place-box').attr('id') != "undefined") {
                $(this).find('.place-box').attr('id', "plc_col_" + calc_nxt_id[0] + "" + nxt_id_plc);
            }
            nxt_id_plc++;
        }
    })
    var prefOpt = $('#classchangeid li.last').attr('id');
    intheTripRHAA(prefOpt);
    Getplaces_details_forsave();
}