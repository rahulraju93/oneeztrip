﻿$(document).ready(function () {
    var CurrentTripId = $("#CurrentTripId").val();
    var CurrentTripDate = $("#CurrentTripDate").val();
    if (CurrentTripId != null && CurrentTripId != undefined && CurrentTripId != "") {
        $("#CurrentIternary").parent().show();
    } else {
        $("#CurrentIternary").parent().hide();
    }
    $("#CurrentIternary").click(function () {

        //alert("neftCurrentIternary");
        var TripIdEdit = CurrentTripId;
        var TripDateEdit = CurrentTripDate;

        if (CurrentTripId != null && CurrentTripId != undefined && CurrentTripId != "") {

            $.ajax({
                type: "POST",
                url: "../Trip/EditSelectedTrip",
                data: { 'TripidToEdit': TripIdEdit, 'TripDateToEdit': TripDateEdit },
                success: function (result) {

                    localStorage.setItem('IsSavedinDB5', 'YES');
                    window.location.href = "../Trip/Firstdaytrip";

                }
            });
        } else {
            $("#CurrentIternary").parent().hide();
        }
    });
});