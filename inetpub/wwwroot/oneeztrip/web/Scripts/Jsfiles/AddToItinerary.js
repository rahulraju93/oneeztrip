﻿function AttractionToleft(id) {
    keepTrackForsave++;
    var trip = $('#inner' + id).html();
    var ArrayCheck = $.inArray(id, leftsidearray);
    if (trip == "In the trip") {
        var notify_alert = noty({
            layout: 'topRight',
            text: 'Already in the trip!',
            type: 'warning',
            timeout: 3000,
            maxVisible: 1,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            }
        });
    }
    else {
        var SelectedCity = $('#selectedcity').html();
        var PlaceArray = [];
        var ToponeId = [];
        $('.city-row-details').each(function (data) {
            if ($(this).data('value').trim() == SelectedCity.trim()) {
                ToponeId.push({
                    id: $(this).find('.place-box-right').attr('id')
                });
            }
        });
        var PreviousValue = -1;
        var PlcaeDivId=-1
        var incr_num_plcbx = 0, PlcsTm_height = 0;
        for (var PlacesIndex = 0, PlacesLength = ToponeId.length; PlacesIndex < PlacesLength; PlacesIndex++) {
            incr_num_plcbx = 0;
            PlcsTm_height = 0;
            $("#plc_" + ToponeId[PlacesIndex].id).find(".place-box").each(function () {
                incr_num_plcbx++;
            });
            $("#plc_" + ToponeId[PlacesIndex].id).find(".place").each(function () {
                PlcsTm_height = PlcsTm_height + $(this).height();
            });
            $("#plc_" + ToponeId[PlacesIndex].id).find(".travel").each(function () {
                PlcsTm_height = PlcsTm_height + $(this).height();
            });
            $("#plc_" + ToponeId[PlacesIndex].id).find(".custom_travel").each(function () {
                PlcsTm_height = PlcsTm_height + $(this).height();
            });
            PlcsTm_height = PlcsTm_height + ((incr_num_plcbx - 1) * 50);

            if (PlcsTm_height < 1790) {
                var CurrentValue = PlcsTm_height;
                if (PreviousValue == -1) {
                    PreviousValue = CurrentValue;
                    PlcaeDivId = PlacesIndex;
                }
                else {
                    if (PreviousValue > CurrentValue) {
                        PreviousValue = CurrentValue;
                        PlcaeDivId = PlacesIndex;
                    }
                }
            }
        }
        if (PlcaeDivId != -1)
        {
            $('#plc_' + ToponeId[PlcaeDivId].id).find('.place').each(function () {
                var attrid = $(this).children().attr("id");
                var lat = $('#' + attrid).find('.placeatt_lat').val();
                var lng = $('#' + attrid).find('.placeatt_lng').val();
                var sorcename = $('#' + attrid).find('.placeatt_name').val();
                if (lat != null) {
                    PlaceArray.push({
                        plc_attrid: attrid,
                        plc_lat: lat,
                        plc_lng: lng,
                        plc_name: sorcename
                    });
                }
            });
        }        
        if (PlaceArray.length != 0) {
            var cityid = '"' + id + '"';
            var Description = $('#p' + id).html();
            var Name = $('#h' + id).html();
            var rating = $('#s' + id).html();
            var count = $('#orderarray').val();
            var lat = $('#latarray').val();
            var image = $('#img_' + id).attr("src");
            var ismanual = $('#isME' + id).val();
            var lng = $('#lngarray').val(), mindist = 99999, next_dist = "", next_placeid = "", plc_name = "";
            var next_placeid2 = "";
            for (var loopvar = 0, PlacesLength = PlaceArray.length; loopvar < PlacesLength; loopvar++) {
                var dist = Haversine(PlaceArray[loopvar].plc_lat, PlaceArray[loopvar].plc_lng, lat, lng);
                if (dist < mindist) {
                    next_placeid = PlaceArray[loopvar].plc_attrid;
                    next_dist = dist;
                    mindist = dist;
                    plc_name = PlaceArray[loopvar].plc_name;
                }
            }
            var htmldata = "";
            if ($('#plc_' + ToponeId[PlcaeDivId].id).find('#' + next_placeid).parent().next().attr('class') != "travel") {
                htmldata += "<div class='travel' id='ltra_" + id + "'><a href='#' style='float:right;'><img src='../Content/images/del_travel.png' class='travedelimg'></a><p>Time taken by car is 15 mins <img src='../Content/images/edit.png' alt='' class='editimg' style='height: 15px;width: 15px;'></p><ul><li><a href='#' data-value='car' class='active'><img src='../Content/images/car-icon.png' alt=''></a></li><li><a href='#' data-value='train'><img src='../Content/images/train-icon.png' alt=''></a></li><li><a href='#' data-value='plane'><img src='../Content/images/plane-icon.png' alt=''></a></li><li><a href='#' data-value='walk'><img src='../Content/images/walk-icon.png' alt=''></a></li><li class='last'><a href='#' data-value='bicycle'><img src='../Content/images/scooter-icon.png' alt=''></a></li><input type='hidden' class='hiddriving' value=''/></ul><input type='hidden' class='currenttravelmode' value='car'><input type='hidden' class='prevtime'></div>";
                next_placeid2 = $('#plc_' + ToponeId[PlcaeDivId].id).find('#' + next_placeid).parent().attr("id");
                ToleftFirstId = "";
                ToleftFirstIdnextname = $('#plc_' + ToponeId[PlcaeDivId].id).find('#' + next_placeid2).find('.place-cont').find('.p-left').find('h4').html();
                ToleftFirstIdnext = "ltra_" + id;
            }
            htmldata += "<div  class='place' id='place" + id + "'>";
            htmldata += "<div class='place-details' id='" + id + "'  style='height:200px;'>";
            htmldata += "<input type='hidden' value='" + lat + "' class='placeatt_lat'/><input type='hidden' value='" + lng + "' class='placeatt_lng'/><input type='hidden' value='" + Name + "' class='placeatt_name'/>";
            if (ismanual == 1)
            {
                htmldata += "<input type='hidden' value='0' class='frmApi'/>";
            }
            else
            {
                htmldata += "<input type='hidden' value='1' class='frmApi'/>";
            }
            
            htmldata += "<figure class='place-img'><img src='" + image + "' alt='' /></figure>";
            htmldata += "<div class='place-cont'><div class='p-left'><h4>" + Name + "</h4><p style='height:26px; overflow-y:hidden;'>" + Description + " .</p>";
            htmldata += "<input type='hidden' value='" + lat + "' class='hidlat'/><input type='hidden' value='" + lng + "' class='hidlng'/>";
            htmldata += "<span>Attraction.</span><br><span id='sp_" + id + "' class='sphrs'></span></div>";
            htmldata += "<div class='p-right'><ul>";
            htmldata += "<li><a href='#'><img src='../content/images/del-icon.png' alt='' class='deleteplace' /></a></li>";
            if (ismanual == 1) {
                htmldata += "<li><a href='#infowindow' class='fancybox' onclick='return infomanual(\"2\",\""+id+"\");'><img src='../content/images/info-icon.png' alt='' /></a></li>";
            }
            else {
                htmldata += "<li><a href='#infowindow' class='fancybox' onclick='return infoattraction(\"" + id + "\",\"" + lat + "\",\"" + lng + "\",\"2\");'><img src='../content/images/info-icon.png' alt='' /></a></li>";
            }
            htmldata += "</ul></div></div></div>";
            htmldata += "</div>";
            if ($('#plc_' + ToponeId[PlcaeDivId].id).find('#' + next_placeid).parent().next().attr('class') == "travel") {
                htmldata += "<div class='travel' id='ltra_" + id + "'><a href='#' style='float:right;'><img src='../Content/images/del_travel.png' class='travedelimg'/></a><p>Time taken by car is 15 mins <img src='../Content/images/edit.png' alt='' class='editimg' style='height: 15px;width: 15px;'></p>";
                htmldata += "<ul><li><input type='hidden' class='hidcar' value=''/><a href='#' data-value='car' class='active'><img src='../Content/images/car-icon.png' alt=''/></a></li>";
                htmldata += "<li><input type='hidden' class='hidtrain' value=''/><a href='#' data-value='train'><img src='../Content/images/train-icon.png' alt='' /></a></li>";
                htmldata += "<li><input type='hidden' class='hidplane' value=''/><a href='#' data-value='plane'><img src='../Content/images/plane-icon.png' alt='' /></a></li>";
                htmldata += "<li><input type='hidden' class='hidwalk' value=''/><a href='#' data-value='walk'><img src='../Content/images/walk-icon.png' alt='' /></a></li>";
                htmldata += "<li class='last'><input type='hidden' class='hidcycle' value=''/><a href='#' data-value='bicycle'><img src='../Content/images/scooter-icon.png' alt='' /></a></li>";
                htmldata += "<input type='hidden' class='hiddriving' value=''/></ul></div>";
                //htmldata += "<div class='travel' id='ltra_" + id + "'><a href='#' style='float:right;'><img src='../Content/images/del_travel.png' class='travedelimg'></a><p>Time taken by car is 13 mins <img src='../Content/images/edit.png' alt='' class='editimg' style='height: 15px;width: 15px;'></p><ul><li><a href='#' data-value='car' class='active'><img src='../Content/images/car-icon.png' alt=''></a></li><li><a href='#' data-value='train'><img src='../Content/images/train-icon.png' alt=''></a></li><li><a href='#' data-value='plane'><img src='../Content/images/plane-icon.png' alt=''></a></li><li><a href='#' data-value='walk'><img src='../Content/images/walk-icon.png' alt=''></a></li><li class='last'><a href='#' data-value='bicycle'><img src='../Content/images/scooter-icon.png' alt=''></a></li><input type='hidden' class='hiddriving' value=''/></ul><input type='hidden' class='currenttravelmode' value='car'><input type='hidden' class='prevtime'></div>";
                next_placeid2 = $('#plc_' + ToponeId[PlcaeDivId].id).find('#' + next_placeid).parent().next().attr("id");
                ToleftFirstId = next_placeid2;
                ToleftFirstIdnextname = $('#plc_' + ToponeId[PlcaeDivId].id).find('#' + next_placeid2).after().next().find('.place-cont').find('.p-left').find('h4').html();
                ToleftFirstIdnext = "ltra_" + id;
            }
            next_placeid = next_placeid2;
            $('#plc_' + ToponeId[PlcaeDivId].id).find('#' + next_placeid).after(htmldata);
            $('#res_' + id).removeClass("resort-row");
            $('#res_' + id).addClass("resort-row2");
            onresizingplc();
            $('#inner' + id).html("In the trip");
            var spId = (ToponeId[PlcaeDivId].id).split('_');
            var sp_Id = spId[1].split('');
            var parId = parseInt(sp_Id[1]) + 1;
            var getDayNum = $('#cit_' + sp_Id[0] + parId).find('.trip-date').find('.btn').html();
            var notify_alert = noty({
                layout: 'topRight',
                text: 'Added to Day' + getDayNum + '',
                type: 'success',
                timeout: 3000,
                maxVisible: 1,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                }
            });
            //var numoddays = 0;

            //$('.city-row:visible').each(function () {
            //    numoddays++;
            //});
           
            //if (numoddays == 1) {
            //    $('.hotel-details').css("margin-left", "2px").css("width", "93.6%");
            //    $('.trip-right').css("margin-left", "-52px").css("width", "51%");
            //    $('.place-box-right').css("width", "550px");
            //    $('.city-row').css("width", "172%");
            //    $('.place-details').css("width", "115%").css("margin-left", "13%");
            //    $('.travel').css("width", "124%").css("margin-left", "13%").css("text-align", "center");
            //    $('.travel ul').css("text-align", "center").css("margin-left", "11%");
            //    $('.mCSB_draggerRail').css("width", "0% !important");
            //    $('.mCSB_dragger_bar').css("width", "0% !important");
            //    $('.mCSB_scrollTools').css("width", "0% !important");
            //    $('.mCSB_draggerContainer').css("display", "none !important");
            //    $('.ui-widget-content').css("background", "none");
            //}
            leftsidearray.push(id);
            if (ToleftFirstId != "") {
                ToleftFirstIdprev = Name;
                var origin = Name,
               destination = plc_name,
              dservice = new google.maps.DistanceMatrixService();
                dservice.getDistanceMatrix(
                     {
                         origins: [origin],
                         destinations: [destination],
                         travelMode: google.maps.TravelMode.DRIVING,
                         avoidHighways: false,
                         avoidTolls: false
                     }, callbacktoleftattraction);
            }
            else {
                travelinsidecallback();
            }
            return false;
        }
        else if(PlaceArray.length == 0 && PlcaeDivId != -1)
        {
            var cityid = '"' + id + '"';
            var Description = $('#p' + id).html();
            var Name = $('#h' + id).html();
            var rating = $('#s' + id).html();
            var count = $('#orderarray').val();
            var lat = $('#latarray').val();
            var image = $('#img_' + id).attr("src");
            var lng = $('#lngarray').val();
            var ismanual = $('#isME' + id).val();
            var htmldata = "<div  class='place' id='place" + id + "'>";
            htmldata += "<div class='place-details' id='" + id + "'  style='height:200px;'>";
            if (ismanual == 1)
            {
                htmldata += "<input type='hidden' value='0' class='frmApi'/>";
            }
            else {
                htmldata += "<input type='hidden' value='1' class='frmApi'/>";
            }
           
            htmldata += "<input type='hidden' value='" + lat + "' class='placeatt_lat'/><input type='hidden' value='" + lng + "' class='placeatt_lng'/><input type='hidden' value='" + Name + "' class='placeatt_name'/>";
            htmldata += "<figure class='place-img'><img src='" + image + "' alt='' /></figure>";
            htmldata += "<div class='place-cont'><div class='p-left'><h4>" + Name + "</h4><p style='height:26px; overflow-y:hidden;'>" + Description + " .</p>";
            htmldata += "<input type='hidden' value='" + lat + "' class='hidlat'/><input type='hidden' value='" + lng + "' class='hidlng'/>";
            htmldata += "<span>Attraction.</span><br><span id='sp_" + id + "' class='sphrs'></span></div>";
            htmldata += "<div class='p-right'><ul>";
            htmldata += "<li><a href='#'><img src='../content/images/del-icon.png' alt='' class='deleteplace' /></a></li>";
            if (ismanual == 1) {
                htmldata += "<li><a href='#infowindow' class='fancybox' onclick='return infomanual(\"2\",\"" + id + "\");'><img src='../content/images/info-icon.png' alt='' /></a></li>";
            }
            else {
                htmldata += "<li><a href='#infowindow' class='fancybox' onclick='return infoattraction(\"" + id + "\",\"" + lat + "\",\"" + lng + "\",\"2\");'><img src='../content/images/info-icon.png' alt='' /></a></li>";
            }
            htmldata += "</ul></div></div></div>";
            htmldata += "</div>";
            $("#plc_" + ToponeId[PlcaeDivId].id).append(htmldata);
            $('#res_' + id).removeClass("resort-row");
            $('#res_' + id).addClass("resort-row2");
            //var numodday = 0;

            //$('.city-row:visible').each(function () {
            //    numodday++;
            //});
            
            //if (numodday == 1) {
            //    $('.hotel-details').css("margin-left", "2px").css("width", "93.6%");
            //    $('.trip-right').css("margin-left", "-52px").css("width", "51%");
            //    $('.place-box-right').css("width", "550px");
            //    $('.city-row').css("width", "172%");
            //    $('.place-details').css("width", "115%").css("margin-left", "13%");
            //    $('.travel').css("width", "124%").css("margin-left", "13%").css("text-align", "center");
            //    $('.travel ul').css("text-align", "center").css("margin-left", "11%");
            //    $('.mCSB_draggerRail').css("width", "0% !important");
            //    $('.mCSB_dragger_bar').css("width", "0% !important");
            //    $('.mCSB_scrollTools').css("width", "0% !important");
            //    $('.mCSB_draggerContainer').css("display", "none !important");
            //    $('.ui-widget-content').css("background", "none");
            //}
            onresizingplc();
            $('#inner' + id).html("In the trip");
            return false;
        }
        else {
            var notify_alert = noty({
                layout: 'topRight',
                text: 'Can\'t accomodate in the city \''+SelectedCity+'\',try after either deleting a place or creating a new day..',
                type: 'warning',
                timeout: 4000,
                maxVisible: 1,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                }
            });
            return false;
        }
        
    }

   
}

function RestaurantToleft(id) {
    keepTrackForsave++;
    var trip = $('#inner' + id).html();
    var i = $.inArray(id, leftsidearray);
    if (trip == "In the trip") {
        var notify_alert = noty({
            layout: 'topRight',
            text: 'Already in the trip! You can drag and drop at your prefered place.',
            type: 'warning',
            timeout: 3000,
            maxVisible: 1,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            }
        });
    }
    else {
        var SelectedCity = $('#selectedcity').html();
        var PlaceArray = [];
        var ToponeId = [];
        $('.city-row-details').each(function (data) {
            if ($(this).data('value') == SelectedCity) {
                ToponeId.push({
                    id: $(this).find('.place-box-right').attr('id')
                });
            }
        });
        var PreviousValue = -1;
        var PlcaeDivId = -1;
        var incr_num_plcbx = 0, PlcsTm_height = 0;
        for (var PlacesIndex = 0, PlacesLength = ToponeId.length; PlacesIndex < PlacesLength ; PlacesIndex++) {
            incr_num_plcbx = 0;
            PlcsTm_height = 0;
            $("#plc_" + ToponeId[PlacesIndex].id).find(".place-box").each(function () {
                incr_num_plcbx++;
            });
            $("#plc_" + ToponeId[PlacesIndex].id).find(".place").each(function () {
                PlcsTm_height = PlcsTm_height + $(this).height();
            });
            $("#plc_" + ToponeId[PlacesIndex].id).find(".travel").each(function () {
                PlcsTm_height = PlcsTm_height + $(this).height();
            });
            $("#plc_" + ToponeId[PlacesIndex].id).find(".custom_travel").each(function () {
                PlcsTm_height = PlcsTm_height + $(this).height();
            });
            PlcsTm_height = PlcsTm_height + ((incr_num_plcbx - 1) * 50);
            if (PlcsTm_height < 1850) {
                var CurrentValue = PlcsTm_height;
                if (PreviousValue == -1) {
                    PreviousValue = CurrentValue;
                    PlcaeDivId = PlacesIndex;
                }
                else {
                    if (PreviousValue > CurrentValue) {
                        PreviousValue = CurrentValue;
                        PlcaeDivId = PlacesIndex;
                    }
                }
            }
        }
        if (PlcaeDivId != -1)
        {
            $('#plc_' + ToponeId[PlcaeDivId].id).find('.place').each(function () {
                var attrid = $(this).children().attr("id");                
                var lat = $('#' + attrid).find('.placeatt_lat').val();
                var lng = $('#' + attrid).find('.placeatt_lng').val();
                var sorcename = $('#' + attrid).find('.placeatt_name').val();
                if (lat != null) {
                    PlaceArray.push({
                        plc_attrid: attrid,
                        plc_lat: lat,
                        plc_lng: lng,
                        plc_name: sorcename
                    });
                }
            });
        }
        if (PlaceArray.length != 0) {
            var cityid = '"' + id + '"';
            var description = $('#p' + id).html();
            var name = $('#h' + id).html();
            var rating = $('#s' + id).html();
            var count = $('#orderarray').val();
            var lat = $('#latarray').val();
            var image = $('#img_' + id).attr("src");
            var ismanual = $('#isME' + id).val();
            var lng = $('#lngarray').val(), mindist = 99999, next_dist = "", next_placeid = "", plc_name = "", next_placeid2="";
            for (var loopvar = 0, PlacesLength = PlaceArray.length; loopvar < PlacesLength; loopvar++) {
                var dist = Haversine(PlaceArray[loopvar].plc_lat, PlaceArray[loopvar].plc_lng, lat, lng);
                if (dist < mindist) {
                    next_placeid = PlaceArray[loopvar].plc_attrid;
                    next_dist = dist;
                    mindist = dist;
                    plc_name = PlaceArray[loopvar].plc_name;
                }
            }
            var htmldata = "";
            if ($('#plc_' + ToponeId[PlcaeDivId].id).find('#' + next_placeid).parent().next().attr('class') != "travel") {
                htmldata = "<div class='travel' id='ltra_" + id + "'><a href='#' style='float:right;'><img src='../Content/images/del_travel.png' class='travedelimg'/></a><p>Time taken by car is 15 mins <img src='../Content/images/edit.png' alt='' class='editimg' style='height: 15px;width: 15px;'></p>";
                htmldata += "<ul><li><input type='hidden' class='hidcar' value=''/><a href='#' data-value='car' class='active'><img src='../Content/images/car-icon.png' alt=''/></a></li>";
                htmldata += "<li><input type='hidden' class='hidtrain' value=''/><a href='#' data-value='train'><img src='../Content/images/train-icon.png' alt='' /></a></li>";
                htmldata += "<li><input type='hidden' class='hidplane' value=''/><a href='#' data-value='plane'><img src='../Content/images/plane-icon.png' alt='' /></a></li>";
                htmldata += "<li><input type='hidden' class='hidwalk' value=''/><a href='#' data-value='walk'><img src='../Content/images/walk-icon.png' alt='' /></a></li>";
                htmldata += "<li class='last'><input type='hidden' class='hidcycle' value=''/><a href='#' data-value='bicycle'><img src='../Content/images/scooter-icon.png' alt='' /></a></li>";
                htmldata += "<input type='hidden' class='hiddriving' value=''/></ul></div>";
                //htmldata += "<div class='travel' id='ltra_" + id + "'><a href='#' style='float:right;'><img src='../Content/images/del_travel.png' class='travedelimg'></a><p>Time taken by car is 13 mins <img src='../Content/images/edit.png' alt='' class='editimg' style='height: 15px;width: 15px;'></p><ul><li><a href='#' data-value='car' class='active'><img src='../Content/images/car-icon.png' alt=''></a></li><li><a href='#' data-value='train'><img src='../Content/images/train-icon.png' alt=''></a></li><li><a href='#' data-value='plane'><img src='../Content/images/plane-icon.png' alt=''></a></li><li><a href='#' data-value='walk'><img src='../Content/images/walk-icon.png' alt=''></a></li><li class='last'><a href='#' data-value='bicycle'><img src='../Content/images/scooter-icon.png' alt=''></a></li><input type='hidden' class='hiddriving' value=''/></ul><input type='hidden' class='currenttravelmode' value='car'><input type='hidden' class='prevtime'></div>";
                next_placeid2 = $('#plc_' + ToponeId[PlcaeDivId].id).find('#' + next_placeid).parent().attr("id");
                toleftfirstid = "";
                toleftfirstidnextname = $('#plc_' + ToponeId[PlcaeDivId].id).find('#' + next_placeid2).find('.place-cont').find('.p-left').find('h4').html();
                toleftfirstidnext = "ltra_" + id;
            }
            htmldata += "<div  class='place' id='place" + id + "'>";
            if (ismanual == 1)
            {
                htmldata += "<input type='hidden' value='0' class='frmApi'/>";
            }
            else
            {
                htmldata += "<input type='hidden' value='1' class='frmApi'/>";
            }
           
            htmldata += "<div class='place-details' id='" + id + "'  style='height:100px;'>";
            htmldata += "<input type='hidden' value='" + lat + "' class='placeres_lat'/><input type='hidden' value='" + lng + "' class='placeres_lng'/><input type='hidden' value='" + name + "' class='placeres_name'/>";
            htmldata += "<figure class='place-img'><img src='" + image + "' alt='' /></figure>";
            htmldata += "<div class='place-cont'><div class='p-left'><h4>" + name + "</h4><p style='height:26px; overflow-y:hidden;'>" + description + " .</p>";
            htmldata += "<input type='hidden' value='" + lat + "' class='hidlat'/><input type='hidden' value='" + lng + "' class='hidlng'/>"
            htmldata += "<span>Restaurant.</span><br><span id='sp_" + id + "' class='sphrs'></span></div>";
            htmldata += "<div class='p-right'><ul>";
            htmldata += "<li><a href='#'><img src='../content/images/del-icon.png' alt='' class='deleteplace' /></a></li>";
            if (ismanual == 1) {
                htmldata += "<li><a href='#infowindow' class='fancybox' onclick='return infomanual(\"3\",\"" + id + "\");'><img src='../content/images/info-icon.png' alt='' /></a></li>";
            }
            else {
                htmldata += "<li><a href='#infowindow' class='fancybox' onclick='return infoattraction(\"" + id + "\",\"" + lat + "\",\"" + lng + "\",\"2\");'><img src='../content/images/info-icon.png' alt='' /></a></li>";
            }
            htmldata += "</ul></div></div></div>";
            htmldata += "</div>";            
            if ($('#plc_' + ToponeId[PlcaeDivId].id).find('#' + next_placeid).parent().next().attr('class') == "travel") {
                htmldata += "<div class='travel' id='ltra_" + id + "'><a href='#' style='float:right;'><img src='../content/images/del_travel.png' class='travedelimg'></a><p>Time taken by car is 15 mins <img src='../content/images/edit.png' alt='' class='editimg' style='height: 15px;width: 15px;'></p><ul><li><a href='#' data-value='car' class='active'><img src='../content/images/car-icon.png' alt=''></a></li><li><a href='#' data-value='train'><img src='../content/images/train-icon.png' alt=''></a></li><li><a href='#' data-value='plane'><img src='../content/images/plane-icon.png' alt=''></a></li><li><a href='#' data-value='walk'><img src='../content/images/walk-icon.png' alt=''></a></li><li class='last'><a href='#' data-value='bicycle'><img src='../content/images/scooter-icon.png' alt=''></a></li><input type='hidden' class='hiddriving' value=''/></ul><input type='hidden' class='currenttravelmode' value='car'><input type='hidden' class='prevtime'></div>";
                next_placeid2 = $('#plc_' + ToponeId[PlcaeDivId].id).find('#' + next_placeid).parent().next().attr("id");
                toleftfirstid = next_placeid2;
                toleftfirstidnextname = $('#plc_' + ToponeId[PlcaeDivId].id).find('#' + next_placeid2).after().next().find('.place-cont').find('.p-left').find('h4').html();
                toleftfirstidnext = "ltra_" + id;
            }
            next_placeid = next_placeid2;
            $('#plc_' + ToponeId[PlcaeDivId].id).find('#' + next_placeid).after(htmldata);

            //***Modified by Yuvapriya on 27Sep16
            //var varnoofday = 0;

            //$('.city-row:visible').each(function () {
            //    varnoofday++;
            //});

            //if (varnoofday == 1) {
            //    $('.hotel-details').css("margin-left", "2px").css("width", "93.6%");
            //    $('.trip-right').css("margin-left", "-52px").css("width", "51%");
            //    $('.place-box-right').css("width", "550px");
            //    $('.city-row').css("width", "172%");
            //    $('.place-details').css("width", "115%").css("margin-left", "13%");
            //    $('.travel').css("width", "124%").css("margin-left", "13%").css("text-align", "center");
            //    $('.travel ul').css("text-align", "center").css("margin-left", "11%");
            //    $('.mCSB_draggerRail').css("width", "0% !important");
            //    $('.mCSB_scrollTools').css("width", "0% !important");
            //    $('.mCSB_draggerContainer').css("display", "none !important");
            //    $('.ui-widget-content').css("background", "none");
            //    $('.mCSB_dragger_bar').css("width", "0% !important");

            //}
            //***End

            onresizingplc();
            $('#inner' + id).html("In the trip");
            var spId = (ToponeId[PlcaeDivId].id).split('_');
            var sp_Id = spId[1].split('');
            var parId = parseInt(sp_Id[1]) + 1;
            var getDayNum = $('#cit_' + sp_Id[0] + parId).find('.trip-date').find('.btn').html();
            var notify_alert = noty({
                layout: 'topRight',
                text: 'Added to Day' + getDayNum + '',
                type: 'success',
                timeout: 3000,
                maxVisible: 1,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                }
            });
            $('#res_' + id).removeClass("resort-row");
            $('#res_' + id).addClass("resort-row2");
            leftsidearray.push(id);
            if (toleftfirstid != "") {
                toleftfirstidprev = name;
                var origin = name,
              destination = plc_name,
              dservice = new google.maps.DistanceMatrixService();
                dservice.getDistanceMatrix(
                       {
                           origins: [origin],
                           destinations: [destination],
                           travelMode: google.maps.TravelMode.DRIVING,
                       }, callbacktoleftattraction);
            }
            else
            {
                travelinsidecallback();
            }
        }
        else if (PlaceArray.length == 0 && PlcaeDivId != -1) {
            var cityid = '"' + id + '"';
            var description = $('#p' + id).html();
            var name = $('#h' + id).html();
            var rating = $('#s' + id).html();
            var count = $('#orderarray').val();
            var lat = $('#latarray').val();
            var image = $('#img_' + id).attr("src");
            var lng = $('#lngarray').val();
            var ismanual = $('#isME' + id).val();
            var htmldata = "<div  class='place' id='place" + id + "'>";
            if (ismaual == 1)
            {
                htmldata += "<input type='hidden' value='0' class='frmApi'/>";
            }
            else
            {
                htmldata += "<input type='hidden' value='1' class='frmApi'/>";
            }
           
            htmldata += "<div class='place-details' id='" + id + "'  style='height:100px;'>";
            htmldata += "<input type='hidden' value='" + lat + "' class='placeatt_lat'/><input type='hidden' value='" + lng + "' class='placeatt_lng'/><input type='hidden' value='" + Name + "' class='placeatt_name'/>";
            htmldata += "<figure class='place-img'><img src='" + image + "' alt='' /></figure>";
            htmldata += "<div class='place-cont'><div class='p-left'><h4>" + Name + "</h4><p style='height:26px; overflow-y:hidden;'>" + Description + " .</p>";
            htmldata += "<input type='hidden' value='" + lat + "' class='hidlat'/><input type='hidden' value='" + lng + "' class='hidlng'/>";
            htmldata += "<span>Restaurant.</span><br><span id='sp_" + id + "' class='sphrs'></span></div>";
            htmldata += "<div class='p-right'><ul>";
            htmldata += "<li><a href='#'><img src='../content/images/del-icon.png' alt='' class='deleteplace' /></a></li>";
            if (ismanual == 1) {
                htmldata += "<li><a href='#infowindow' class='fancybox' onclick='return infomanual(\"3\",\"" + id + "\");'><img src='../content/images/info-icon.png' alt='' /></a></li>";
            }
            else {
                htmldata += "<li><a href='#infowindow' class='fancybox' onclick='return infoattraction(\"" + id + "\",\"" + lat + "\",\"" + lng + "\",\"2\");'><img src='../content/images/info-icon.png' alt='' /></a></li>";
            }   
            htmldata += "</ul></div></div></div>";
            htmldata += "</div>";
            $("#plc_" + ToponeId[PlcaeDivId].id).append(htmldata);

            //***Modified by Yuvapriya on 27Sep16
            //var varnoofdays = 0;

            //$('.city-row:visible').each(function () {
            //    varnoofdays++;
            //});

            //if (varnoofdays == 1) {
            //    $('.hotel-details').css("margin-left", "2px").css("width", "93.6%");
            //    $('.trip-right').css("margin-left", "-52px").css("width", "51%");
            //    $('.place-box-right').css("width", "550px");
            //    $('.city-row').css("width", "172%");
            //    $('.place-details').css("width", "115%").css("margin-left", "13%");
            //    $('.travel').css("width", "124%").css("margin-left", "13%").css("text-align", "center");
            //    $('.travel ul').css("text-align", "center").css("margin-left", "11%");
            //    $('.mCSB_draggerRail').css("width", "0% !important");
            //    $('.mCSB_scrollTools').css("width", "0% !important");
            //    $('.mCSB_draggerContainer').css("display", "none !important");
            //    $('.ui-widget-content').css("background", "none");
            //    $('.mCSB_dragger_bar').css("width", "0% !important");

            //}
            //***End


            onresizingplc();
            $('#res_' + id).removeClass("resort-row");
            $('#res_' + id).addClass("resort-row2");
            $('#inner' + id).html("In the trip");
            return false;
        }
        else {
            var notify_alert = noty({
                layout: 'topRight',
                text: 'Can\'t accomodate in the city \'' + SelectedCity + '\',try after either deleting a place or creating a new day..',
                type: 'warning',
                timeout: 4000,
                maxVisible: 1,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                }
            });
            return false;
        }
    }
    return false;
}

function addtoleft(id) {
    keepTrackForsave++;
    var hotels_array_toappend = [];
    var plc_box_to_append = "";
    var idvalue = $('#inner' + id + '.add-trip').html();
    var incre_hoteladd = 0, incre_no_of_hotels = 0;
    var cityrow_id = 0, hotels_cnt_in_day = 0;
    var selected_city_name = $("#selectedcity").html();
    if ($("#prefer").html() == "Hotel Preferences") {
        hotels_array_toappend = addtoleft_hotelsfull.slice();
        var place = [];
        if ($("#isME" + id).val() == 0) {
            place = hotels_array_toappend.filter(function (obj) {
                return obj.hotelId == id;
            });
        }
        else
        {
            var tmpdata = [];
            for (var indx = 0, lmt = manualdata.length; indx < lmt; indx++) {
                if (manualdata[indx].ctyName == $('#selectedcity').html()) {
                    tmpdata = manualdata[indx].hotldata;
                    break;
                }
            }
            place = tmpdata.filter(function (obj) {
                return obj.Id == id;
            });
        }
        var find_ith_day = 0;
        var arr_samecityid = [], arr_daynum = [], chchbox_chckd = [];
        var samecity_plcbox_id = "", ith_day_samecity = "";
        $(".place-box-right").each(function () {
            var candisplay = 0;
            $(this).find(".place").each(function () {
                var ishotel = $(this).find(".p-left").find("span:first").html();
                if(ishotel=="Hotel.")
                {
                    candisplay = 1;
                }
            });            
            var PlcsTm_height = 0;
            var incr_num_plcbx = 0;
            if (candisplay == 0) {
                $(this).find(".place-box").each(function () {
                    incr_num_plcbx++;
                });
                $(this).find(".place").each(function () {
                    PlcsTm_height = PlcsTm_height + $(this).height();
                });
                $(this).find(".travel").each(function () {
                    PlcsTm_height = PlcsTm_height + $(this).height();
                });
                $(this).find(".custom_travel").each(function () {
                    PlcsTm_height = PlcsTm_height + $(this).height();
                });
                PlcsTm_height = PlcsTm_height + ((incr_num_plcbx - 1) * 50);
                if (PlcsTm_height < 1850) {
                    candisplay = 1;
                }
            }
            if (candisplay) {
                find_ith_day++;
                if ($(this).data('value').trim() == selected_city_name.trim()) {
                    if (find_ith_day > 1) {
                        samecity_plcbox_id += ",";
                        ith_day_samecity += ","
                    }
                    samecity_plcbox_id += $(this).attr('id');
                    ith_day_samecity += find_ith_day;
                }
            }
        });
        arr_samecityid = samecity_plcbox_id.split(',');
        arr_daynum = ith_day_samecity.split(',');
        $("#addtoleft_checkbox").html("");
        var create_chckbox = "";
        create_chckbox = "<h2>Select the days to replace the hotels..</h2><br/>";
        create_chckbox += "<input type='checkbox' class='chckbx_alldays' style='margin-left: 4%;margin-bottom: 2%;' name='chckall_days' id='hotelsleft_replaceall' /><label for='hotelsleft_replaceall'>Replace hotel for all the days<label> <br/>";
        for (var loop_val = 0; loop_val < arr_samecityid.length; loop_val++) {
            create_chckbox += "<input type='checkbox' class='chckbx_addtoleft' style='margin-left: 4%;margin-bottom: 2%;' name='hotelstoleft' id='hotelsleft" + arr_daynum[loop_val] + "' value='" + arr_samecityid[loop_val] + "' ><label for='hotelsleft" + arr_daynum[loop_val] + "'>Day" + arr_daynum[loop_val] + "<label> <br/>";
        }
        create_chckbox += "<div><input type='button' value='Replace' id='btnsubmit_addlefthotel' style='background-color: rgb(167, 42, 107);margin-left: 10%;width: 25%;border-color: rgb(167, 42, 107);border-radius: 4px;height: 10%;color: white;'><input type='button' value='Cancel' id='btncancel_addlefthotel' style='background-color: rgb(167, 42, 107);margin-left: 3%;width: 25%;border-color: rgb(167, 42, 107);border-radius: 4px;height: 10%;color: white;'></div>";
        $("#addtoleft_checkbox").append(create_chckbox);
        $("#click_addtolefthotel").trigger("click");
        $('#hotelsleft_replaceall').click(function (event) {
            if (this.checked) {
                $('.chckbx_addtoleft').each(function () {
                    this.checked = true;
                });
            }
            else {
                $('.chckbx_addtoleft').each(function () {
                    this.checked = false;
                });
            }
        });
        $('#btnsubmit_addlefthotel').on('click', function () {
            $("input:checkbox[name=hotelstoleft]:checked").each(function () {
                hotels_cnt_in_day = 0;
                var chckbx_val_hotelstolft = $(this).val();
                $("#" + chckbx_val_hotelstolft + " .place").each(function () {
                    if ($(this).find(".p-left").find("span").html().trim() == "Hotel.") {
                        hotels_cnt_in_day++;
                        $(this).prev(".travel").remove();
                        $(this).remove();
                    }
                });
                var dusort = $('#sorthelp').val();
                if (dusort == 0) {
                    $('#sorthelp').val(id);
                }
                else {
                    $('#sorthelp').val(dusort + "_" + id);
                }                        
                $('#rhaa1 .resort-row2').each(function () {
                    $(this).find('.add-trip').html("Add trip");
                    $(this).switchClass("resort-row2", "resort-row");
                    $(this).find('.book').parent().css('display', 'none');
                });
                plc_box_to_append = chckbx_val_hotelstolft;
                if ($("#isME" + id).val() == 0) {
                   
                    var placediv1 = "<div class='place' id='place" + place[0].hotelId + "'><div class='place-details' id=" + place[0].hotelId + " style ='height:86px;'><input type='hidden' value='1' class='frmApi'/><figure class='place-img'>";
                    placediv1 += "<img src='http://media3.expedia.com" + place[0].thumbNailUrl + "' alt='' /></figure><div class='place-cont'>";
                    placediv1 += "<div class='p-left'><h4>" + place[0].name + "</h4><p style='height:26px; overflow-y:hidden;'>" + place[0].address1 + "</p><input type='hidden' class='hidlat' value='" + place[0].latitude + "'/><input type='hidden' class='hidlng' value='" + place[0].longitude + "'/><span>Hotel.</span>";
                    placediv1 += "</div><div class='p-right'>";
                    placediv1 += "<ul><li><a href='#'><img src='../Content/images/del-icon.png' class='deleteplace' alt='' /></a></li><li><a href='#infowindow_hotels_activities' onclick='return info_for_hotels(\"" + place[0].hotelId + "\",\"plc_" + plc_box_to_append + "\")' class='fancybox'><img src='../Content/images/info-icon.png' alt='' /></a></li> </ul>";
                    placediv1 += "</div> </div> </div>";
                    placediv1 += "</div>";
                    $('#' + plc_box_to_append + ' .place:last').after(placediv1);
                }
                else
                {
                    
                    var placediv1 = "<div class='place' id='place" + place[0].Id + "'><div class='place-details' id=" + place[0].Id + " style ='height:86px;'><input type='hidden' value='0' class='frmApi'/><figure class='place-img'>";
                    placediv1 += "<img src='../Content/images/HotelImages/" + place[0].Photo + "' alt='' /></figure><div class='place-cont'>";
                    placediv1 += "<div class='p-left'><h4>" + place[0].Name + "</h4><p style='height:26px; overflow-y:hidden;'>" + place[0].Street + "</p><input type='hidden' class='hidlat' value='" + place[0].HotlLat + "'/><input type='hidden' class='hidlng' value='" + place[0].HotlLng + "'/><span>Hotel.</span>";
                    placediv1 += "</div><div class='p-right'>";
                    placediv1 += "<ul><li><a href='#'><img src='../Content/images/del-icon.png' class='deleteplace' alt='' /></a></li><li><a href='#infowindow_hotels_activities' onclick='return GetMnlHtlinfo(\"" + place[0].Id + "\")' class='fancybox'><img src='../Content/images/info-icon.png' alt='' /></a></li> </ul>";
                    placediv1 += "</div> </div> </div>";
                    placediv1 += "</div>";
                    $('#' + plc_box_to_append + ' .place:last').after(placediv1);
                }                
                onresizingplc();
                $('.place + .place').before($('<div class="travel"><a href="#" style="float:right;"><img src="../Content/images/del_travel.png" class="travedelimg"/></a><p></p><ul><li><input type="hidden" class="hidcar" value=""/><a href="#" data-value="car" class="active"><img src="../Content/images/car-icon.png" alt=""/></a></li><li><input type="hidden" class="hidtrain" value=""/><a href="#" data-value="train"><img src="../Content/images/train-icon.png" alt="" /></a></li><li><input type="hidden" class="hidplane" value=""/><a href="#" data-value="plane"><img src="../Content/images/plane-icon.png" alt="" /></a></li><li><input type="hidden" class="hidwalk" value=""/><a href="#" data-value="walk"><img src="../Content/images/walk-icon.png" alt="" /></a></li><li class="last"><input type="hidden" class="hidcycle" value=""/><a href="#" data-value="bicycle"><img src="../Content/images/scooter-icon.png" alt="" /></a></li><input type="hidden" class="hiddriving" value=""/></ul></div>'));
                $(".travel").each(function () {
                    if ($(this).attr('id') == null || $(this).attr('id') == "" || $(this).attr('id') == "undefined") {
                        incre_travel++;
                        incre_forfinal_travel++;
                        $(this).attr('id', 'newtravel_' + incre_travel);
                    }
                });
                if (incre_travel > 0) {
                    calcduration_addtoleft();
                }

                //***Modified by Yuvapriya on 27Sep16
                //var noofdayy = 0;

                //$('.city-row:visible').each(function () {
                //    noofdayy++;
                //});

                //if (noofdayy == 1) {
                //    $('.hotel-details').css("margin-left", "2px").css("width", "93.6%");
                //    $('.trip-right').css("margin-left", "-52px").css("width", "51%");
                //    $('.place-box-right').css("width", "550px");
                //    $('.city-row').css("width", "172%");
                //    $('.place-details').css("width", "115%").css("margin-left", "13%");
                //    $('.travel').css("width", "124%").css("margin-left", "13%").css("text-align", "center");
                //    $('.travel ul').css("text-align", "center").css("margin-left", "11%");
                //    $('.mCSB_draggerRail').css("width", "0% !important");
                //    $('.mCSB_scrollTools').css("width", "0% !important");
                //    $('.mCSB_draggerContainer').css("display", "none !important");
                //    $('.ui-widget-content').css("background", "none");
                //    $('.mCSB_dragger_bar').css("width", "0% !important");

                //}
                //***End

            });

            //***Modeified by Yuvapriya on 20 Sep 16 To remove Hotel Book Option*** 
            // FirstHotelBookBtn();
            //***End***
            setTimeout(function () { FindHotelsInItinerary(); }, 500);
            $("#click_addtolefthotel").trigger("click");
        });
        $('#btncancel_addlefthotel').on('click', function () {
            $("#click_addtolefthotel").trigger("click");
        });
    }
    else if ($("#prefer").html() == "Activity Preferences") {        
        if ($('#inner' + id + '.add-trip').html() === "In the trip") {
            var notify_alert = noty({
                layout: 'topRight',
                text: 'It is already in the trip,cannot be added one more time..',
                type: 'warning',
                timeout: 3000,
                maxVisible: 1,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                }
            });
        }
        else {
            var place = [];
            place = activities_forsort.filter(function (obj) {
                    return obj.id == id;
            });
            var ActivityDur = place[0].durationInMillis;
            var minutes = ActivityDur / (1000 * 60);
            minutes = ((minutes / 30) * 100) / 2;
            if (minutes > 2000)
            {
                minutes = 1700;
            }
            var ToponeId = [];
            var PlaceArray = [];
            $('.city-row-details').each(function (data) {
                if ($(this).data('value') == selected_city_name) {
                    ToponeId.push({
                        id: $(this).find('.place-box-right').attr('id')
                    });
                }
            });
            var PreviousValue = -1;
            var PlcaeDivId = -1;
            var incr_num_plcbx = 0, PlcsTm_height = 0;
            var activityHight = "";
            for (var PlacesIndex = 0, PlacesLength = ToponeId.length; PlacesIndex < PlacesLength ; PlacesIndex++) {
                incr_num_plcbx = 0;
                PlcsTm_height = 0;
                $("#plc_" + ToponeId[PlacesIndex].id).find(".place-box").each(function () {
                    incr_num_plcbx++;
                });
                $("#plc_" + ToponeId[PlacesIndex].id).find(".place").each(function () {
                    PlcsTm_height = PlcsTm_height + $(this).height();
                });
                $("#plc_" + ToponeId[PlacesIndex].id).find(".travel").each(function () {
                    PlcsTm_height = PlcsTm_height + $(this).height();
                });
                $("#plc_" + ToponeId[PlacesIndex].id).find(".custom_travel").each(function () {
                    PlcsTm_height = PlcsTm_height + $(this).height();
                });
                PlcsTm_height = PlcsTm_height + ((incr_num_plcbx - 1) * 50);
                if ((parseInt(PlcsTm_height) + parseInt(minutes)) < 1910) {
                    var CurrentValue = PlcsTm_height;
                    if (PreviousValue == -1) {
                        PreviousValue = CurrentValue;
                        PlcaeDivId = PlacesIndex;
                    }
                    else {
                        if (PreviousValue > CurrentValue) {
                            PreviousValue = CurrentValue;
                            PlcaeDivId = PlacesIndex;
                        }
                    }
                }
            }
            if (PlcaeDivId != -1) {
                $('#plc_' + ToponeId[PlcaeDivId].id).find('.place').each(function () {
                    var attrid = $(this).children().attr("id");
                    var lat = $('#' + attrid).find('.placeatt_lat').val();
                    var lng = $('#' + attrid).find('.placeatt_lng').val();
                    var sorcename = $('#' + attrid).find('.placeatt_name').val();
                    if (lat != null) {
                        PlaceArray.push({
                            plc_attrid: attrid,
                            plc_lat: lat,
                            plc_lng: lng,
                            plc_name: sorcename
                        });
                    }
                });
            }
            if (PlaceArray.length != 0) {
                var cityid = '"' + id + '"';
                var description = $('#s' + id).html();
                var name = $('#h' + id).html();
                //var rating = $('#s' + id).html();
                var count = $('#orderarray').val();
                var lat = $('#latarray').val();
                var image = $('#img_' + id).attr("src");
                var lng = $('#lngarray').val(), mindist = 99999, next_dist = "", next_placeid = "", plc_name = "", next_placeid2 = "";
                for (var loopvar = 0, PlacesLength = PlaceArray.length; loopvar < PlacesLength; loopvar++) {
                    var dist = Haversine(PlaceArray[loopvar].plc_lat, PlaceArray[loopvar].plc_lng, lat, lng);
                    if (dist < mindist) {
                        next_placeid = PlaceArray[loopvar].plc_attrid;
                        next_dist = dist;
                        mindist = dist;
                        plc_name = PlaceArray[loopvar].plc_name;
                    }
                }
                var htmldata = "";
                if ($('#plc_' + ToponeId[PlcaeDivId].id).find('#' + next_placeid).parent().next().attr('class') != "travel") {
                    htmldata = "<div class='travel' id='ltra_" + id + "'><a href='#' style='float:right;'><img src='../Content/images/del_travel.png' class='travedelimg'/></a><p>Time taken by car is 15 mins <img src='../Content/images/edit.png' alt='' class='editimg' style='height: 15px;width: 15px;'></p>";
                    htmldata += "<ul><li><input type='hidden' class='hidcar' value=''/><a href='#' data-value='car' class='active'><img src='../Content/images/car-icon.png' alt=''/></a></li>";
                    htmldata += "<li><input type='hidden' class='hidtrain' value=''/><a href='#' data-value='train'><img src='../Content/images/train-icon.png' alt='' /></a></li>";
                    htmldata += "<li><input type='hidden' class='hidplane' value=''/><a href='#' data-value='plane'><img src='../Content/images/plane-icon.png' alt='' /></a></li>";
                    htmldata += "<li><input type='hidden' class='hidwalk' value=''/><a href='#' data-value='walk'><img src='../Content/images/walk-icon.png' alt='' /></a></li>";
                    htmldata += "<li class='last'><input type='hidden' class='hidcycle' value=''/><a href='#' data-value='bicycle'><img src='../Content/images/scooter-icon.png' alt='' /></a></li>";
                    htmldata += "<input type='hidden' class='hiddriving' value=''/></ul></div>";
                    //htmldata += "<div class='travel' id='ltra_" + id + "'><a href='#' style='float:right;'><img src='../Content/images/del_travel.png' class='travedelimg'></a><p>Time taken by car is 13 mins <img src='../Content/images/edit.png' alt='' class='editimg' style='height: 15px;width: 15px;'></p><ul><li><a href='#' data-value='car' class='active'><img src='../Content/images/car-icon.png' alt=''></a></li><li><a href='#' data-value='train'><img src='../Content/images/train-icon.png' alt=''></a></li><li><a href='#' data-value='plane'><img src='../Content/images/plane-icon.png' alt=''></a></li><li><a href='#' data-value='walk'><img src='../Content/images/walk-icon.png' alt=''></a></li><li class='last'><a href='#' data-value='bicycle'><img src='../Content/images/scooter-icon.png' alt=''></a></li><input type='hidden' class='hiddriving' value=''/></ul><input type='hidden' class='currenttravelmode' value='car'><input type='hidden' class='prevtime'></div>";
                    next_placeid2 = $('#' + next_placeid).parent().attr("id");
                    toleftfirstid = "";
                    toleftfirstidnextname = $('#' + next_placeid2).find('.place-cont').find('.p-left').find('h4').html();
                    toleftfirstidnext = "ltra_" + id;
                }
                var ltlng=place[0].latLng.split(',');
                htmldata += "<div  class='place' id='place" + place[0].id + "'>";
                htmldata += "<div class='place-details' id='" + place[0].id + "'  style='height:" + minutes + "px;'>";
                htmldata += "<input type='hidden' value='" + ltlng[0] + "' class='placeatt_lat'/><input type='hidden' value='" + ltlng[1] + "' class='placeatt_lng'/><input type='hidden' value='" + place[0].title + "' class='placeatt_name'/>";
               
                htmldata += "<input type='hidden' value='1' class='frmApi'/><figure class='place-img'><img src='" + place[0].imageUrl + "' alt='' /></figure>";
                htmldata += "<div class='place-cont'><div class='p-left'><h4>" + place[0].title + "</h4><p style='height:26px; overflow-y:hidden;'>" + place[0].duration + " .</p>";
                htmldata += "<input type='hidden' value='" + ltlng[0] + "' class='hidlat'/><input type='hidden' value='" + ltlng[1] + "' class='hidlng'/>"
                htmldata += "<span>Activity.</span><br><span id='sp_" + place[0].id + "' class='sphrs'></span></div>";
                htmldata += "<div class='p-right'><ul>";
                htmldata += "<li><a href='#'><img src='../content/images/del-icon.png' alt='' class='deleteplace' /></a></li>";
                htmldata += "<li><a  class='fancybox' href='#infowindow_hotels_activities' onclick='getActivityInfo(\"" + place[0].id + "\",\"" + $("#selectedcity").html() + "\");'><img src='../Content/images/info-icon.png' alt='' class='example1demo' /></a></li>";
                htmldata += "</ul></div></div></div>";
                htmldata += "</div>";
                if ($('#plc_' + ToponeId[PlcaeDivId].id).find('#' + next_placeid).parent().next().attr('class') == "travel") {
                    htmldata += "<div class='travel' id='ltra_" + id + "'><a href='#' style='float:right;'><img src='../content/images/del_travel.png' class='travedelimg'></a><p>Time taken by car is 15 mins <img src='../content/images/edit.png' alt='' class='editimg' style='height: 15px;width: 15px;'></p><ul><li><a href='#' data-value='car' class='active'><img src='../content/images/car-icon.png' alt=''></a></li><li><a href='#' data-value='train'><img src='../content/images/train-icon.png' alt=''></a></li><li><a href='#' data-value='plane'><img src='../content/images/plane-icon.png' alt=''></a></li><li><a href='#' data-value='walk'><img src='../content/images/walk-icon.png' alt=''></a></li><li class='last'><a href='#' data-value='bicycle'><img src='../content/images/scooter-icon.png' alt=''></a></li><input type='hidden' class='hiddriving' value=''/></ul><input type='hidden' class='currenttravelmode' value='car'><input type='hidden' class='prevtime'></div>";
                    next_placeid2 = $('#' + next_placeid).parent().next().attr("id");
                    toleftfirstid = next_placeid2;
                    toleftfirstidnextname = $('#' + next_placeid2).after().next().find('.place-cont').find('.p-left').find('h4').html();
                    toleftfirstidnext = "ltra_" + id;
                }
                next_placeid = next_placeid2;
                $('#plc_' + ToponeId[PlcaeDivId].id).find('#' + next_placeid).after(htmldata);

                //***Modified by Yuvapriya on 27Sep16
                //var noofdayss = 0;

                //$('.city-row:visible').each(function () {
                //    noofdayss++;
                //});

                //if (noofdayss == 1) {
                //    $('.hotel-details').css("margin-left", "2px").css("width", "93.6%");
                //    $('.trip-right').css("margin-left", "-52px").css("width", "51%");
                //    $('.place-box-right').css("width", "550px");
                //    $('.city-row').css("width", "172%");
                //    $('.place-details').css("width", "115%").css("margin-left", "13%");
                //    $('.travel').css("width", "124%").css("margin-left", "13%").css("text-align", "center");
                //    $('.travel ul').css("text-align", "center").css("margin-left", "11%");
                //    $('.mCSB_draggerRail').css("width", "0% !important");
                //    $('.mCSB_scrollTools').css("width", "0% !important");
                //    $('.mCSB_draggerContainer').css("display", "none !important");
                //    $('.ui-widget-content').css("background", "none");
                //    $('.mCSB_dragger_bar').css("width", "0% !important");

                //}
                //***End

                onresizingplc();
                $('#inner' + id).html("In the trip");
                var spId = (ToponeId[PlcaeDivId].id).split('_');
                var sp_Id = spId[1].split('');
                var parId = parseInt(sp_Id[1]) + 1;
                var getDayNum = $('#cit_' + sp_Id[0] + parId).find('.trip-date').find('.btn').html();
                var notify_alert = noty({
                    layout: 'topRight',
                    text: 'Added to Day' + getDayNum + '',
                    type: 'success',
                    timeout: 3000,
                    maxVisible: 1,
                    animation: {
                        open: { height: 'toggle' },
                        close: { height: 'toggle' },
                        easing: 'swing',
                        speed: 500
                    }
                });
                $('#res_' + id).removeClass("resort-row");
                $('#res_' + id).addClass("resort-row2");
                leftsidearray.push(id);
                if (toleftfirstid != "") {
                    toleftfirstidprev = name;
                    var origin = name,
                  destination = plc_name,
                  dservice = new google.maps.DistanceMatrixService();
                    dservice.getDistanceMatrix(
                           {
                               origins: [origin],
                               destinations: [destination],
                               travelMode: google.maps.TravelMode.DRIVING,
                           }, callbacktoleftattraction);
                }
                else {
                    travelinsidecallback();
                }
            }
            else if (PlaceArray.length == 0 && PlcaeDivId != -1) {
                var cityid = '"' + id + '"';
                var description = $('#s' + id).html();
                var name = $('#h' + id).html();
                //var rating = $('#s' + id).html();
                var count = $('#orderarray').val();
                var lat = $('#latarray').val();
                var image = $('#img_' + id).attr("src");
                var lng = $('#lngarray').val();
                var ltlng = place[0].latLng.split(',');
                
                var htmldata = "<div  class='place' id='place" + place[0].id + "'>";
                htmldata += "<div class='place-details' id='" + place[0].id + "'  style='height:" + minutes + "px;'>";
                htmldata += "<input type='hidden' value='" + ltlng[0] + "' class='placeatt_lat'/><input type='hidden' value='" + ltlng[1] + "' class='placeatt_lng'/><input type='hidden' value='" + place[0].title + "' class='placeatt_name'/>";
                htmldata += "<input type='hidden' value='1' class='frmApi'/><figure class='place-img'><img src='" + place[0].imageUrl + "' alt='' /></figure>";
                htmldata += "<div class='place-cont'><div class='p-left'><h4>" + place[0].title + "</h4><p style='height:26px; overflow-y:hidden;'>" + place[0].duration + " .</p>";
                htmldata += "<input type='hidden' value='" + ltlng[0] + "' class='hidlat'/><input type='hidden' value='" + ltlng[1] + "' class='hidlng'/>"
                htmldata += "<span>Activity.</span><br><span id='sp_" + place[0].id + "' class='sphrs'></span></div>";
                htmldata += "<div class='p-right'><ul>";
                htmldata += "<li><a href='#'><img src='../content/images/del-icon.png' alt='' class='deleteplace' /></a></li>";
                htmldata += "<li><a  class='fancybox' href='#infowindow_hotels_activities' onclick='getActivityInfo(\"" + place[0].id + "\",\"" + $("#selectedcity").html() + "\");'><img src='../Content/images/info-icon.png' alt='' class='example1demo' /></a></li>";
                htmldata += "</ul></div></div></div>";
                htmldata += "</div>";
                $("#plc_" + ToponeId[PlcaeDivId].id).append(htmldata);

                //***Modified by Yuvapriya on 27Sep16
                //var noofdas = 0;

                //$('.city-row:visible').each(function () {
                //    noofdas++;
                //});

                //if (noofdas == 1) {
                //    $('.hotel-details').css("margin-left", "2px").css("width", "93.6%");
                //    $('.trip-right').css("margin-left", "-52px").css("width", "51%");
                //    $('.place-box-right').css("width", "550px");
                //    $('.city-row').css("width", "172%");
                //    $('.place-details').css("width", "115%").css("margin-left", "13%");
                //    $('.travel').css("width", "124%").css("margin-left", "13%").css("text-align", "center");
                //    $('.travel ul').css("text-align", "center").css("margin-left", "11%");
                //    $('.mCSB_draggerRail').css("width", "0% !important");
                //    $('.mCSB_scrollTools').css("width", "0% !important");
                //    $('.mCSB_draggerContainer').css("display", "none !important");
                //    $('.ui-widget-content').css("background", "none");
                //    $('.mCSB_dragger_bar').css("width", "0% !important");

                //}
                //***End

                onresizingplc();
                $('#res_' + id).removeClass("resort-row");
                $('#res_' + id).addClass("resort-row2");
                $('#inner' + id).html("In the trip");
                return false;
            }
            else {
                var notify_alert = noty({
                    layout: 'topRight',
                    text: 'Can\'t accomodate in the city \'' + selected_city_name + '\',try after either deleting a place or creating a new day..',
                    type: 'warning',
                    timeout: 4000,
                    maxVisible: 1,
                    animation: {
                        open: { height: 'toggle' },
                        close: { height: 'toggle' },
                        easing: 'swing',
                        speed: 500
                    }
                });
                return false;
            }
            
            $('#rhaa1 #inner' + id).html("In the trip");
            //var actsInCity = [];
            //for (var loopvarHotels = 0, CitiesHotelsLength = ActsForCities.length; loopvarHotels < CitiesHotelsLength; loopvarHotels++) {
            //    if (ActsForCities[loopvarHotels].cityName.trim() == selected_city_name.trim()) {
            //        if (ActsForCities[loopvarHotels].Acts.activities) {
            //            actsInCity = ActsForCities[loopvarHotels].Acts.activities.slice();
            //            var foundAct = [];
            //            for (var inloopAct = 0, actsLength = actsInCity.length; inloopAct < actsLength; inloopAct++) {
            //                if (actsInCity[inloopAct].id == id) {
            //                    foundAct = actsInCity[inloopAct];

            //                }
            //            }
            //        }
            //    }
            //}

            var dusort = $('#sorthelp').val();
            if (dusort == 0) {
                $('#sorthelp').val(id);
            }
            else {
                $('#sorthelp').val(dusort + "_" + id);
            }


            //var place = activities_foradd.filter(function (obj) {
            //    return obj.id == id;
            //});
            //var toleft = "<div class='place' id='place" + place[0].id + "'><div class='place-details' id=" + place[0].id + " style ='height:135px;'><figure class='place-img'>";
            //toleft += "<img src='" + place[0].Images + "'/></figure><div class='place-cont'>";
            //toleft += "<div class='p-left'><h4>" + place[0].title + "</h4><p style='height:26px; overflow-y:hidden;'>" + place[0].locations_text + "</p><input type='hidden' class='hidlat' value='" + place[0].start_Latitude + "'/><input type='hidden' class='hidlng' value='" + place[0].start_Longitude + "'/><span>Activity.</span>";
            //toleft += "<br><span id='sp_" + place[0].id + "' class='sphrs'></span>";
            //toleft += "</div><div class='p-right'>";
            //toleft += "<ul><li><a href='#'><img src='../Content/images/del-icon.png' class='deleteplace' alt=''/></a></li><li><a href='#infoActi_" + place[0].id + "' class='fancybox'><img src='../Content/images/info-icon.png' alt='' /></a></li> </ul>";
            //toleft += "<div id='infoActi_" + place[0].id + "' class='info-left popup-bg' style='display: none;'>";
            //toleft += "<figure class='resort-img'><img src='" + place[0].Images + "' alt='' /><span>From <strong>$" + place[0].Cost + "</strong><br/>(" + place[0].Price_Description + ")</span></figure>";
            //toleft += "<h3>" + place[0].title + "</h3>";
            //if (place[0].rating != "") {
            //    var actrating = parseFloat(place[0].rating).toFixed(1);
            //    toleft += "<ul><b>Ratings : </b>";
            //    for (var loopvaria1 = 0; loopvaria1 < 5; loopvaria1++) {
            //        if (place[0].rating < (loopvaria1 + 0.5)) {
            //            toleft += "<li><img src='../Content/images/grey-star.png' alt='' /></li>";
            //        }
            //        else {
            //            toleft += "<li><img src='../Content/images/yellow-star.png' alt='' /></li>";
            //        }
            //    }
            //    toleft += "<li>(" + actrating + "/5)</li></ul>";
            //}
            //toleft += "<p><b>Starting location : </b>" + place[0].starting_location + "</p>";
            //toleft += "<p><b>Locations : </b>" + place[0].locations_text + "</p>";
            //toleft += "<p><b>Url : </b><a href='" + place[0].url + "'>" + place[0].url + "</a></p>";
            //toleft += "<p><b>Description : </b>" + place[0].abstract + "</p>";
            //toleft += "</div>"
            //toleft += "</div> </div> </div>";
            //toleft += "</div>";

           // var appended_or_not = 0;
            //$('.place-box-right').each(function () {
            //    var incre_plc_no = 0;
            //    if ($(this).data('value').trim() == selected_city_name.trim() && appended_or_not != 1) {
            //        var cur_plcbx_attr = $(this).attr('id');
            //        $("#" + cur_plcbx_attr + " .place").each(function () {
            //            incre_plc_no++;
            //        });
            //        $("#" + cur_plcbx_attr + " .resort-row").each(function () {
            //            incre_plc_no++;
            //        });

            //        if (incre_plc_no < 9) {
            //            $("#rhaa1").find("#res_" + id).switchClass("resort-row", "resort-row2");
            //            $('#' + cur_plcbx_attr).find('.travel:last').after(toleft);
            //            if ($("#" + cur_plcbx_attr).find(".place + .place").length) {
            //                $("#" + cur_plcbx_attr).find(".place + .place").before($('<div class="travel" id="travel_new_12345"><a href="#" style="float:right;"><img src="../Content/images/del_travel.png" class="travedelimg"/></a><p>Time taken by car is -- <img src="../Content/images/edit.png" alt="" class="editimg" style="height: 15px;width: 15px;"></p><ul><li><a href="#" data-value="car" class="active"><img src="../Content/images/car-icon.png" alt=""/></a></li><li><a href="#" data-value="train"><img src="../Content/images/train-icon.png" alt="" /></a></li><li><a href="#" data-value="plane"><img src="../Content/images/plane-icon.png" alt="" /></a></li><li><a href="#" data-value="walk"><img src="../Content/images/walk-icon.png" alt="" /></a></li><li class="last"><a href="#" data-value="bicycle"><img src="../Content/images/scooter-icon.png" alt="" /></a></li><input type="hidden" class="hiddriving" value=""/></ul></div>'));
            //            }
            //            else if ($("#" + cur_plcbx_attr).find(".resort-row + .place").length) {
            //                $("#" + cur_plcbx_attr).find(".resort-row + .place").before($('<div class="travel" id="travel_new_12345"><a href="#" style="float:right;"><img src="../Content/images/del_travel.png" class="travedelimg"/></a><p>Time taken by car is -- <img src="../Content/images/edit.png" alt="" class="editimg" style="height: 15px;width: 15px;"></p><ul><li><a href="#" data-value="car" class="active"><img src="../Content/images/car-icon.png" alt=""/></a></li><li><a href="#" data-value="train"><img src="../Content/images/train-icon.png" alt="" /></a></li><li><a href="#" data-value="plane"><img src="../Content/images/plane-icon.png" alt="" /></a></li><li><a href="#" data-value="walk"><img src="../Content/images/walk-icon.png" alt="" /></a></li><li class="last"><a href="#" data-value="bicycle"><img src="../Content/images/scooter-icon.png" alt="" /></a></li><input type="hidden" class="hiddriving" value=""/></ul></div>'));
            //            }
            //            else if ($("#" + cur_plcbx_attr).find(".place + .resort-row").length) {
            //                $("#" + cur_plcbx_attr).find(".place + .resort-row").before($('<div class="travel" id="travel_new_12345"><a href="#" style="float:right;"><img src="../Content/images/del_travel.png" class="travedelimg"/></a><p>Time taken by car is -- <img src="../Content/images/edit.png" alt="" class="editimg" style="height: 15px;width: 15px;"></p><ul><li><a href="#" data-value="car" class="active"><img src="../Content/images/car-icon.png" alt=""/></a></li><li><a href="#" data-value="train"><img src="../Content/images/train-icon.png" alt="" /></a></li><li><a href="#" data-value="plane"><img src="../Content/images/plane-icon.png" alt="" /></a></li><li><a href="#" data-value="walk"><img src="../Content/images/walk-icon.png" alt="" /></a></li><li class="last"><a href="#" data-value="bicycle"><img src="../Content/images/scooter-icon.png" alt="" /></a></li><input type="hidden" class="hiddriving" value=""/></ul></div>'));
            //            }
            //            else if ($("#" + cur_plcbx_attr).find(".resort-row + .resort-row").length) {
            //                $("#" + cur_plcbx_attr).find(".resort-row + .resort-row").before($('<div class="travel" id="travel_new_12345"><a href="#" style="float:right;"><img src="../Content/images/del_travel.png" class="travedelimg"/></a><p>Time taken by car is -- <img src="../Content/images/edit.png" alt="" class="editimg" style="height: 15px;width: 15px;"></p><ul><li><a href="#" data-value="car" class="active"><img src="../Content/images/car-icon.png" alt=""/></a></li><li><a href="#" data-value="train"><img src="../Content/images/train-icon.png" alt="" /></a></li><li><a href="#" data-value="plane"><img src="../Content/images/plane-icon.png" alt="" /></a></li><li><a href="#" data-value="walk"><img src="../Content/images/walk-icon.png" alt="" /></a></li><li class="last"><a href="#" data-value="bicycle"><img src="../Content/images/scooter-icon.png" alt="" /></a></li><input type="hidden" class="hiddriving" value=""/></ul></div>'));
            //            }
            //            prev_lat_sort2 = $("#" + cur_plcbx_attr).find('#place' + id).find('.hidlat').val();
            //            prev_lng_sort2 = $("#" + cur_plcbx_attr).find('#place' + id).find('.hidlng').val();
            //            if ($("#travel_new_12345").next('.place').length) {
            //                next_lat_sort2 = $("#travel_new_12345").next('.place').find('.hidlat').val();
            //                next_lng_sort2 = $("#travel_new_12345").next('.place').find('.hidlng').val();
            //            }
            //            else if ($("#travel_new_12345").next('.resort-row').length) {
            //                next_lat_sort2 = $("#travel_new_12345").next('.resort-row').find('.hidlat').val();
            //                next_lng_sort2 = $("#travel_new_12345").next('.resort-row').find('.hidlng').val();
            //            }
            //            travelmodeid2 = "travel_new_12345";
            //            callhaversineforaddtoleft();
            //            appended_or_not = 1;
            //        }
            //    }
            //});
            //if (appended_or_not == 0) {
            //    var notify_alert = noty({
            //        layout: 'topRight',
            //        text: 'Exceeds day time limit,cannot be added..',
            //        type: 'warning',
            //        timeout: 3000,
            //        maxVisible: 1,
            //        animation: {
            //            open: { height: 'toggle' },
            //            close: { height: 'toggle' },
            //            easing: 'swing',
            //            speed: 500
            //        }
            //    });
            //}
        }
    }
    return false;
}

function FindHotelsInItinerary() {
    var SelectedCity = $('#selectedcity').html();
    var hotelIdsList = [], temphotelIdsList = [], presentDayId = "", HotelPlaceId = "";
    $('.place-box-right').each(function () {
        if ($(this).data('value').trim() == SelectedCity.trim()) {
            presentDayId = $(this).attr('id');
            $('#' + presentDayId + ' .place').each(function () {
                HotelPlaceId = $(this).find('.place-details').attr('id');
                if ($(this).find(".p-left").find("span").html().trim() == "Hotel.") {
                    temphotelIdsList.push(HotelPlaceId);
                }
            });
        }
    });
    $.each(temphotelIdsList, function (keys, values) {
        if ($.inArray(values, hotelIdsList) == -1) {
            hotelIdsList.push(values);
        }
    });    
    $('#rhaa1 .resort-row').each(function () {
        for (var loopvar = 0 ; loopvar < hotelIdsList.length; loopvar++) {
            var curresid = $(this).attr('id');
            var sp_curresid = curresid.split("_");
            if (hotelIdsList[loopvar] == sp_curresid[1]) {
                $(this).find('.add-trip').html("In the trip");
                $(this).find('.book').parent().css('display', 'block');
                $(this).switchClass("resort-row", "resort-row2");                
            }
        }
    });
}

function callhaversineforaddtoleft() {
    var dista = Haversine(prev_lat_sort2, prev_lng_sort2, next_lat_sort2, next_lng_sort2);
    var kmtomin1 = dista / 40;
    var kmtomin2 = kmtomin1 * 60;
    var kmtomin3 = kmtomin2.toFixed();
    var final_time = "";
    if (isNaN(kmtomin3)) {
        kmtomin3 = 15;
    }
    if (kmtomin3 > 60) {
        var kmtohrs = Math.floor(kmtomin3 / 60);
        var kmtomints = kmtomin3 % 60;
        if (kmtohrs > 1 && kmtohrs <= 2) {
            if (kmtohrs == 1) {
                final_time = kmtohrs + " hour " + kmtomints + " mins ";
            }
            else if (kmtohrs == 2) {
                final_time = kmtohrs + " hours " + kmtomints + " mins ";
            }
        }
        else if (kmtohrs > 2) {
            final_time = "15 mins";
        }
        else {
            final_time = kmtohrs + " hours " + kmtomints + " mins ";
        }
    }
    else {
        if (kmtomin3 <= 1) {
            kmtomin3 = kmtomin3 + 4;
        }
        final_time = parseInt(kmtomin3) + " mins ";
    }
    $("#" + travelmodeid2).find("p").html('Time taken by car is ' + final_time);
    $("#" + travelmodeid2).find("p").append(' <img src="../Content/images/edit.png" alt="" class="editimg" style="height: 15px;width: 15px;">');
    $("#" + travelmodeid2).find(".hidcar").val(final_time);
}

function callbacktoleftattraction(response, status) {
    if (status === "OK") {
        var array = [];
        if (response.rows[0].elements[0].duration) {
            var splitTraveltime = (response.rows[0].elements[0].duration.text).split(' ');
            for (var loopthr = 0, TravelTimeFind = splitTraveltime.length; loopthr < TravelTimeFind; loopthr++) {
                if (splitTraveltime[loopthr] == "min" || splitTraveltime[loopthr] == "mins") {
                    splitTraveltime[loopthr - 1] = Math.ceil(splitTraveltime[loopthr - 1] / 5) * 5;
                }
            }
            var dura = splitTraveltime.join(" ");
            $('#' + ToleftFirstId).find('p').html("Time taken by car is " + dura + " <img src='../Content/images/edit.png' alt='' class='editimg' style='height: 15px;width: 15px;'>");
            $('#' + ToleftFirstId).find(".hidcar").val(dura);
            array = response.rows[0].elements[0].duration.text.split('');
            travelinsidecallback();
        }
        else {
            var durat = "15 mins";
            $('#' + ToleftFirstId).find('p').html("Time taken by car is 15 mins <img src='../Content/images/edit.png' alt='' class='editimg' style='height: 15px;width: 15px;'>");
            $('#' + ToleftFirstId).find(".hidcar").val(durat);
            array = durat.split('');
            travelinsidecallback();
        }
        var size = $('#compl').find('.travel').length;
        TravelDivResize(size);
    }
    else {
        var durat = "15 mins";
        $('#' + ToleftFirstId).find('p').html("Time taken by car is 15 mins <img src='../Content/images/edit.png' alt='' class='editimg' style='height: 15px;width: 15px;'>");
        $('#' + ToleftFirstId).find(".hidcar").val(durat);
        array = durat.split('');
        var size = $('#compl').find('.travel').length;
        TravelDivResize(size);
        travelinsidecallback();
    }
}

function travelinsidecallback() {
    var dservice = new google.maps.DistanceMatrixService();
    dservice.getDistanceMatrix(
         {
             origins: [ToleftFirstIdprev],
             destinations: [ToleftFirstIdnextname],
             travelMode: google.maps.TravelMode.DRIVING,
             avoidHighways: false,
             avoidTolls: false
         }, callbacktoleftattractionnext);
}

function callbacktoleftattractionnext(response, status) {
    if (status === "OK") {
        var array = [];
        if (response.rows[0].elements[0].duration) {
            var splitTraveltime = (response.rows[0].elements[0].duration.text).split(' ');
            for (var loopthr = 0, TravelTimeFind = splitTraveltime.length; loopthr < TravelTimeFind; loopthr++) {
                if (splitTraveltime[loopthr] == "min" || splitTraveltime[loopthr] == "mins") {
                    splitTraveltime[loopthr - 1] = Math.ceil(splitTraveltime[loopthr - 1] / 5) * 5;
                }
            }
            var dura = splitTraveltime.join(" ");
            $('#' + ToleftFirstIdnext).find('p').html("Time taken by car is " + dura + " <img src='../Content/images/edit.png' alt='' class='editimg' style='height: 15px;width: 15px;'>");
            $('#' + ToleftFirstIdnext).find(".hidcar").val(dura);
            array = response.rows[0].elements[0].duration.text.split('');

        }
        else {
            var durat = "15 mins";
            $('#' + ToleftFirstIdnext).find('p').html("Time taken by car is 15 mins <img src='../Content/images/edit.png' alt='' class='editimg' style='height: 15px;width: 15px;'>");
            $('#' + ToleftFirstIdnext).find(".hidcar").val(durat);
            array = durat.split('');
        }
        var size = $('#compl').find('.travel').length;
        TravelDivResize(size);
    }
    else {
        var durat = "15 mins";
        $('#' + ToleftFirstIdnext).find('p').html("Time taken by car is 15 mins <img src='../Content/images/edit.png' alt='' class='editimg' style='height: 15px;width: 15px;'>");
        $('#' + ToleftFirstIdnext).find(".hidcar").val(durat);
        array = durat.split('');
        var size = $('#compl').find('.travel').length;
        TravelDivResize(size);
    }
}

function calcduration_addtoleft() {
    if (incre_travel > 0) {
        addleft_ori = $('#newtravel_' + incre_travel).prev('.place').find(".p-left h4").text() + "," + $('#newtravel_' + incre_travel).prev('.place').find(".p-left p").text();
        addleft_des = $('#newtravel_' + incre_travel).next('.place').find(".p-left h4").text() + "," + $('#newtravel_' + incre_travel).next('.place').find(".p-left p").text();
        var distance_service = new google.maps.DistanceMatrixService();
        distance_service.getDistanceMatrix(
             {
                 origins: [addleft_ori],
                 destinations: [addleft_des],
                 travelMode: google.maps.TravelMode.DRIVING
             }, callbackcar_addtoleft);
    }
}

function callbackcar_addtoleft(response, status) {
    if (status === "OK") {
        var dist = "", dura = "";
        if (response.rows[0].elements[0].distance) {
            dist = response.rows[0].elements[0].distance.text;
            dura = response.rows[0].elements[0].duration.text;
            if (dura.indexOf("hours") != -1) {
                dura = "15 mins";
            }
            $('#newtravel_' + incre_travel).find("p").text('Time taken by car is ' + dura);
            $('#newtravel_' + incre_travel).find("p").append(' <img src="../Content/images/edit.png" alt="" class="editimg" style="height: 15px;width: 15px;">');
            $('#newtravel_' + incre_travel).find(".hiddriving").val(dura);
            $('#newtravel_' + incre_travel).find(".hidcar").val(dura);
            var hours = "hours";
            var hour = "hour";
            var mins = "mins";
            if (dura.indexOf(hour) != -1) {
                var array = dura.split(' ');
                var amount = array[0] / .25;
                if (array[0] < 50) {
                    amount = array[0] / .25;
                }
                else {
                    amount = 50 / .25;
                }
                var timespent = amount * 58;
                if (dura.indexOf(mins) != -1) {
                    if (array[2] > 20) {
                        timespent = array[2] / 30 + timespent;
                        $('#newtravel_' + incre_travel).css("height", timespent);
                    }
                    else {
                        $('#newtravel_' + incre_travel).css("height", timespent);
                    }
                }
                else {
                    $('#newtravel_' + incre_travel).css("height", timespent);
                }
            }
            else if (dura.indexOf(mins) != -1) {
                var array = dura.split(' ');
                if (array[0] > 20) {
                    var amount = array[0] / 30;
                    var timespent = amount * 58;
                    $('#newtravel_' + incre_travel).css("height", timespent);
                }
            }
        }
        else {
            $('#newtravel_' + incre_travel).find("p").text('Time taken by car is 15 mins');
            $('#newtravel_' + incre_travel).find("p").append(' <img src="../Content/images/edit.png" alt="" class="editimg" style="height: 15px;width: 15px;">');
        }
        incre_travel--;
        if (incre_travel > 0) {
            calcduration_addtoleft();
        }
        else {
            final_calc_travelmode_addleft();
        }
    }
    else {
        $('#newtravel_' + incre_travel).find("p").text('Time taken by car is 15 mins');
        $('#newtravel_' + incre_travel).find("p").append(' <img src="../Content/images/edit.png" alt="" class="editimg" style="height: 15px;width: 15px;">');
        incre_travel--;
        if (incre_travel > 0) {
            calcduration_addtoleft();
        }
        else {
            final_calc_travelmode_addleft();
        }
    }
}

function final_calc_travelmode_addleft() {
    var timecalc = "", tmpvar = 2;
    if (incre_forfinal_travel > 0) {
        if ($('#newtravel_' + incre_forfinal_travel).find("p").text() == "No dura..") {
            addleft_ori_lat = $('#newtravel_' + incre_forfinal_travel).prev('.place').find('.hidlat').val();
            addleft_ori_lng = $('#newtravel_' + incre_forfinal_travel).prev('.place').find('.hidlng').val();
            addleft_des_lat = $('#newtravel_' + incre_forfinal_travel).next('.place').find('.hidlat').val();
            addleft_des_lng = $('#newtravel_' + incre_forfinal_travel).next('.place').find('.hidlng').val();
            var dista = Haversine(addleft_ori_lat, addleft_ori_lng, addleft_des_lat, addleft_des_lng);            
            var kmtomin1 = dista / 40;
            var kmtomin2 = kmtomin1 * 60;
            var kmtomin3 = kmtomin2.toFixed();
            var final_time = "";
            if (kmtomin3 > 60) {
                var kmtohrs = Math.floor(kmtomin3 / 60);
                var kmtomints = kmtomin3 % 60;
                if (kmtohrs > 1) {
                    kmtohrs = tmpvar / 2;
                    timecalc = kmtohrs + " hour " + kmtomints + " mins ";
                }
                else {
                    timecalc = kmtohrs + " hour " + kmtomints + " mins ";
                }
            }
            else {
                if (kmtomin3 <= 1) {
                    kmtomin3 = kmtomin3 + 4;
                }
                timecalc = parseInt(kmtomin3) + " mins ";
            }
            var splitTraveltime = timecalc.split(' ');
            for (var loopthr = 0, TravelTimeFind = splitTraveltime.length; loopthr < TravelTimeFind; loopthr++) {
                if (splitTraveltime[loopthr] == "min" || splitTraveltime[loopthr] == "mins") {
                    splitTraveltime[loopthr - 1] = Math.ceil(splitTraveltime[loopthr - 1] / 5) * 5;
                }
            }
            timecalc = splitTraveltime.join(" ");
            $('#newtravel_' + incre_forfinal_travel).find("p").text('Time taken by car is ' + timecalc);
            $('#newtravel_' + incre_forfinal_travel).find("p").append(' <img src="../Content/images/edit.png" alt="" class="editimg" style="height: 15px;width: 15px;">');
            $('#newtravel_' + incre_forfinal_travel).find(".hiddriving").val(timecalc);
            $('#newtravel_' + incre_forfinal_travel).find(".hidcar").val(timecalc);
            $('#newtravel_' + incre_forfinal_travel).css("height", "");
            var hours = "hours";
            var hour = "hour";
            var mins = "mins";
            if (timecalc.indexOf(hour) != -1) {
                var array = timecalc.split(' ');
                var amount = array[0] / .50;
                if (array[0] < 50) {
                    amount = array[0] / .50;
                }
                else {
                    amount = 50 / .50;
                }
                var timespent = amount * 58;
                if (timecalc.indexOf(mins) != -1) {
                        timespent = array[2] / .50 + timespent;
                        $('#newtravel_' + incre_forfinal_travel).css("height", timespent);
                }
                else {
                    $('#newtravel_' + incre_forfinal_travel).css("height", timespent);
                }
            }
            else if (timecalc.indexOf(mins) != -1) {
                var array = timecalc.split(' ');
                if (array[0] > 20) {
                    var amount = array[0] / 30;
                    var timespent = amount * 58;
                    $('#newtravel_' + incre_forfinal_travel).css("height", timespent);
                }
            }
        }
        incre_forfinal_travel--;
    }
    setid_for_alltravelmodes();
}

function FirstHotelBookBtn()
{    
    var SelectedCity = $('#selectedcity').html();
    var hotelIdsList = [], temphotelIdsList = [], presentDayId = "", HotelPlaceId = "";
    var prevHotelId = '';
    $('.place-box-right').each(function () {
        if ($(this).data('value').trim() == SelectedCity.trim()) {
            presentDayId = $(this).attr('id');
            $('#' + presentDayId + ' .place').each(function () {
                HotelPlaceId = $(this).find('.place-details').attr('id');
                if ($(this).find(".p-left").find("span").html().trim() == "Hotel.") {
                    //if (prevHotelId != HotelPlaceId && $(this).find('.Plcbook').length<=0)
                    //{
                    //    var bookBtn = "<span id='Plcbook" + HotelPlaceId + "' style='display: block;margin-left: 20px;'><a href='#' class='Plcbook'>Book</a></span>";
                    //    $(this).find(".p-left").append(bookBtn);
                    //}
                    //else if ($(this).find('.Plcbook').length)
                    //{
                    //    $(this).find('.Plcbook').remove();
                    //}
                    temphotelIdsList.push(HotelPlaceId);
                }
            });
        }
    });
}