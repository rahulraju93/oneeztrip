﻿var all_places_names = "", all_places_id = "", all_places_spendingtime = "", eachcityname = "", allplace_des = "", allplace_rating;
var allplaces_addr = "", allplc_url = "", city_lat = "", city_lng = "", allplace_lat = "", allplace_lng = "";
var alltravel_mode = "", alltravel_time = "", allplace_starttime = "", traveltimeinmins = "20", is_apidata = "", manualplc_id = "", plan_createdbyuser = "", allplace_category = "";
var daynumber_incre = 0, dayorder = 0;
var get_itinerary_plc_box = "";
var manualdata = [];

function GetManualEntry(modenum, lat, lng) {
    var existsVal = 0;
    var tmpdata = [], typOfPlc = "";
    var tmpActFlg = 0;
    var cty = $('#selectedcity').html();
    for (var indx = 0, lmt = manualdata.length; indx < lmt; indx++) {
        if (manualdata[indx].ctyName == cty) {
            if (modenum == 1) {
                typOfPlc = "Hotel";
                if (manualdata[indx].hotldata) {
                    existsVal = 1;
                    tmpdata = manualdata[indx].hotldata;
                    break;
                }
            }
            else if (modenum == 2) {
                typOfPlc = "Attractions";
                if (manualdata[indx].attrdata) {
                    existsVal = 1;
                    tmpdata = manualdata[indx].attrdata;
                    break;
                }
            }
            else if (modenum == 3) {
                typOfPlc = "Restaurant";
                if (manualdata[indx].restdata) {
                    existsVal = 1;
                    tmpdata = manualdata[indx].restdata;
                    break;
                }
            }
            else if (modenum == 4) {
                typOfPlc = "Activities";
                if (manualdata[indx].actidata) {
                    existsVal = 1;
                    tmpdata = manualdata[indx].actidata;
                    break;
                }
            }
        }
    }
    if (existsVal == 1)
    {
        if (tmpdata != "No Result") {
            $.each(tmpdata, function (keys, values) {
                if (modenum == 2) {
                    var ratefilter = parseInt(values.Rating);
                    var items = '';
                    items += "<div id='rh" + values.Id + "'></div><input type='hidden' class='hide'  id=" + values.Id + " value='" + values.Type + "'><input type='hidden' id='rates_" + values.Id + "' value='" + values.Rating + "'/>";
                    items += "<input type='hidden' id='ratefilter_" + values.Id + "' value='" + ratefilter + "'/><input type='hidden' id='latarray" + values.Id + "' value='" + values.AttrLat + "'/><input type='hidden' id='lngarray" + values.Id + "' value='" + values.AttrLng + "'/>";
                    items += "<aside class='resort-row' id='res_" + values.Id + "'><div class='resort-row1' id='divres_" + values.Id + "'>";
                    items += "<input type='hidden' id='latarray" + values.Id + "' value='" + values.AttrLat + "' class='reslat'><input type='hidden' id='lngarray" + values.Id + "' value='" + values.AttrLng + "' class='reslng'>";
                    items += "<input type='hidden' id='isME" + values.Id + "' value='1' class='resME'>";
                    //Modified by yuvapriya on 28Feb2017 Reviews
                    ////if (place.user_ratings_total) {
                    ////    items += "<input type='hidden' class='reviews' value='" + place.user_ratings_total + "'/>"
                    ////}
                    ////else {
                    //items += "<input type='hidden' class='reviews' value='0'/>"
                    ////}

                    if (place.user_ratings_total) {
                        items += "<input type='hidden' class='reviews' value='" + place.user_ratings_total + "'/>"
                    }
                    else {
                    items += "<input type='hidden' class='reviews' value='0'/>"
                    }

                    //End
                    if (values.Price != null) {
                        items += "<input type='hidden' class='priceLev' value='" + values.Price + "'/>"
                    }
                    else {
                        items += "<input type='hidden' class='priceLev' value='0'/>"
                    }
                    if (values.Website != null) {
                        items += "<input type='hidden' class='webhidden' value='" + values.Website + "'>";
                    }
                    else {
                        items += "<input type='hidden' class='webhidden' value='No site'>";
                    }

                    items += "<figure class='resort-img'><div class='tooltip'>Drag to add</div><img src='../Content/images/AttractionImage/" + values.Photo + "' alt='' id='img_" + values.Id + "'  />";
                    items += "<span>Rating <strong id='s" + values.Id + "'>" + values.Rating + "</strong></span></figure>";
                    items += "<div class='resort-details'><div class='r-left'>";
                    items += "<h3 id='h" + values.Id + "'>" + values.Name + "  </h3><br/>";
                    items += "<p style='height:40px; overflow-y:hidden;' id='p" + values.Id + "'>" + values.Street + "</p>";
                    if (values.Website != null) {
                        items += "<p style='height:18px; overflow-y:hidden;width:100%;text-overflow: ellipsis;white-space: nowrap;overflow: hidden;' id='web" + values.Id + "'><b>Website : </b><a target='_blank' href='" + values.Website + "' style='text-decoration: underline;'>" + values.Website + "</a></p>";
                    }
                    items += "<ul id='rat" + values.Id + "'><b>Ratings : </b>";
                    for (var loopvar_rate = 1; loopvar_rate <= 5; loopvar_rate++) {
                        if (values.Rating >= loopvar_rate) {
                            items += "<li><img src='../Content/images/tower_full.png' /></li>";
                        }
                        else if (values.Rating > (loopvar_rate - 1) && values.Rating < loopvar_rate) {
                            items += "<li><img src='../Content/images/tower_half.png' /></li>";
                        }
                        else {
                            items += "<li><img src='../Content/images/tower_grey_full.png' /></li>";
                        }
                    }
                    //Modified by yuvapriya on 28Feb2017
                    ////if (values.user_ratings_total) {
                    ////    items += "<li>(" + values.user_ratings_total + " reviews)</li>";
                    ////}
                    if (values.user_ratings_total) {
                        //items += "<li>(" + values.user_ratings_total + " reviews)</li>";
                    }
                    //end
                    items += "</ul>";
                    if (values.Price) {
                        if (values.Price == 0) {
                            items += "<p style='height: 18px;' id='price" + values.Id + "'><b> Price Level:</b> $</p> ";
                        }
                        else if (values.Price == 1) {
                            items += "<p style='height: 18px;' id='price" + values.Id + "'><b> Price Level:</b> $</p> ";
                        }
                        else if (values.Price == 2) {
                            items += "<p style='height: 18px;' id='price" + values.Id + "'><b> Price Level:</b>  $$</p> ";
                        }
                        else if (values.Price == 3) {
                            items += "<p style='height: 18px;' id='price" + values.Id + "'><b> Price Level:</b>  $$$</p> ";
                        }
                        else if (values.Price == 4) {
                            items += "<p style='height: 18px;' id='price" + values.Id + "'><b> Price Level:</b>  $$$$</p> ";
                        }
                    }
                    else {
                        items += "<p style='height: 18px;' id='price" + values.Id + "'><b> Price Level:</b> $</p> ";
                    }
                    items += "<div class='book-add'><span><a href='#' id='inner" + values.Id + "' class='add-trip' onclick='return AttractionToleft(\"" + values.Id + "\");'>Add Trip</a></span></div></div> ";
                    items += "<div class='r-right'>";
                    //alert('neftn');
                    items += " <ul><li><a href='#'><img src='../Content/images/del-icon.png' alt='' class='del-show' /></a></li>";
                    items += "<li><a  class='fancybox' href='#infowindow' onclick='infomanual(\"2\",\"" + values.Id + "\");'><img src='../Content/images/info-icon.png' alt='' class='example1demo' /></a></li></ul>";
                    items += "</div></div></div>";
                    items += "</aside>";
                    if (items == "") {
                        getmanualEntryVal = 1;
                    }
                    if (apiStatus == 1 && getmanualEntryVal == 1) {
                        $('#rhaa1').html("No results found..");
                    }
                    $('#rhaa1').append(items);
                }
                else if (modenum == 1) {
                    var items = '';
                    items += "<aside class='resort-row' id='res_" + values.hotelId + "'><div class='resort-row1' id='divres_" + values.hotelId + "'>";
                    items += "<input type='hidden' id='" + values.hotelId + "' value='" + values.hotelId + "' class='hide'><input type='hidden' id='latarray" + values.hotelId + "' value='" + values.latitude + "' class='reslat'/><input type='hidden' id='lngarray" + values.hotelId + "' value='" + values.longitude + "' class='reslng'/>";
                    items += "<input type='hidden' id='isME" + values.hotelId + "' value='1' class='resME'>";
                    items += "<input type='hidden' id='htlRt" + values.hotelId + "' value='" + values.Rating + "' class='htlrt'><input type='hidden' id='htlCls" + values.hotelId + "' value='" + values.hotelRating + "' class='htlCls'>";
                    items += "<input type='hidden' id='htlPrc" + values.hotelId + "' value='" + values.highRate + "' class='htlPrc'>";
                    items += "<figure class='resort-img'>";
                    items += "<div class='tooltip'>Drag to add</div><img src='../Content/images/HotelImages/" + values.thumbNailUrl + "' alt=''/>";
                    items += "<span>Night from $<strong id='s" + values.hotelId + "'>" + values.highRate + "</strong></span></figure>";
                    items += "<div class='resort-details'><div class='r-left'><span class='hotel_identify' style='display:none'></span>";
                    items += "<h3 id='h" + values.hotelId + "'>" + values.name + "  </h3><ul><b>Hotel class :</b>";
                    for (var loopvar_hc = 1; loopvar_hc <= 5; loopvar_hc++) {
                        if (values.hotelRating >= loopvar_hc) {
                            items += "<li><img src='../Content/images/yellow-star1.png' /></li>";
                        }
                        else if (values.hotelRating > (loopvar_hc - 1) && values.hotelRating < loopvar_hc) {
                            items += "<li><img src='../Content/images/yellow-gray-starNew.png' /></li>";
                        }
                        else {
                            items += "<li><img src='../Content/images/grey-starNew.png' /></li>";
                        }
                    }
                    items += "(" + (parseFloat(values.hotelRating)).toFixed(1) + ")</ul>";
                    items += "<ul><b>Rating :</b>";
                    for (var loopvar_rate = 1; loopvar_rate <= 5; loopvar_rate++) {
                        if (values.Rating >= loopvar_rate) {
                            items += "<li><img src='../Content/images/hotel-color.png' /></li>";
                        }
                        else if (values.Rating > (loopvar_rate - 1) && values.Rating < loopvar_rate) {
                            items += "<li><img src='../Content/images/hotel-half.png' /></li>";
                        }
                        else {
                            items += "<li><img src='../Content/images/hotel-gray.png' /></li>";
                        }
                    }
                    items += "<li>(" + (parseFloat(values.Rating)).toFixed(1) + "/5)</li></ul>";
                    items += "<p id='pho_" + values.hotelId + "' style='height:40px; overflow-y:hidden;'>" + values.shortDescription + "</p>";
                    //items += "<p style='height:40px; overflow-y:hidden;' id='p" + values.hotelId + "'>" + values.address1 + "</p>";
                    //***Modified by Yuvapriya 
                    //items += "<div class='book-add'><span><a href='#' id='inner" + values.hotelId + "' class='add-trip' onclick='return hotelsToleft(\"" + values.hotelId + "\");'>Add Trip</a></span><span id='book" + values.hotelId + "' style='display:none;'><a href='#' class='book'>Book</a></span></div></div> ";
                    items += "<div class='book-add'><span><a href='#' id='inner" + values.hotelId + "' class='add-trip' onclick='return hotelsToleft(\"" + values.hotelId + "\");'>Add Trip</a></span></div></div> ";
                    //***End***
                    items += "<div class='r-right'>";
                    //alert('neft0');
                    items += " <ul><li><a href='#'><img src='../Content/images/del-icon.png' alt='' class='del-show' onclick=''/></a></li>";
                    items += "<li><a  class='fancybox' href='#infohomanu_" + values.hotelId + "'><img src='../Content/images/info-icon.png' alt='' class='example1demo' /></a></li></ul>";
                    items += "</div></div></div>";
                    items += "</aside>";
                    items += "<div id='infohomanu_" + values.hotelId + "' style='display:none'><div class='info-left popup-bg' style='width: 112%;'><figure class='resort-img'><img src='../Content/images/HotelImages/" + values.thumbNailUrl + "' alt='' /><span>Night from <strong>$" + values.highRate + "</strong></span></figure>";
                    items += "<h3>" + values.name + "</h3>";
                    items += "<ul><b>Hotel class : </b>";
                    for (var loopvar_hc = 1; loopvar_hc <= 5; loopvar_hc++) {
                        if (values.hotelRating >= loopvar_hc) {
                            items += "<li><img src='../Content/images/yellow-star1.png' /></li>";
                        }
                        else if (values.hotelRating > (loopvar_hc - 1) && values.hotelRating < loopvar_hc) {
                            items += "<li><img src='../Content/images/yellow-gray-starNew.png' /></li>";
                        }
                        else {
                            items += "<li><img src='../Content/images/grey-starNew.png' /></li>";
                        }
                    }
                    items += "(" + (parseFloat(values.hotelRating)).toFixed(1) + ")</ul>";
                    items += "<ul><b>Rating :</b>"
                    for (var loopvar_rate = 1; loopvar_rate <= 5; loopvar_rate++) {
                        if (values.Rating >= loopvar_rate) {
                            items += "<li><img src='../Content/images/hotel-color.png' /></li>";
                        }
                        else if (values.Rating > (loopvar_rate - 1) && values.Rating < loopvar_rate) {
                            items += "<li><img src='../Content/images/hotel-half.png' /></li>";
                        }
                        else {
                            items += "<li><img src='../Content/images/hotel-gray.png' /></li>";
                        }
                    }
                    items += "<li>(" + (parseFloat(values.Rating)).toFixed(1) + "/5)</li></ul>";
                    items += "<p><b>Short Description : </b>" + values.shortDescription + "</p>";
                    items += "<p><b>Address : </b>" + values.address1 + "</p>";
                    items += "<p><b>Rate starts from : </b>$" + values.highRate + "</p>";
                    if (values.deepLink != null && values.deepLink != "null") {
                        items += "<p><b>For booking :</b>" + values.deepLink + "</p>";
                    }
                    if (values.Amenities != null) {
                        items += "<p><b>Hotel Amenities : </b>" + values.Amenities + "</p>"
                    }
                    items += "</div>";
                    items += "</div>";
                    if (items == "") {
                        getmanualEntryVal = 1;
                    }
                    if (apiStatus == 1 && getmanualEntryVal == 1) {
                        $('#rhaa1').html("No results found..");
                    }
                    $('#rhaa1').append(items);
                }
                else if (modenum == 3) {
                    var items = '';
                    items += "<div id='rh" + values.Id + "'></div><input type='hidden' class='hide'  id=" + values.Id + " value='" + values.Type + "'><input type='hidden' id='rates_" + values.Id + "' value='" + values.Rating + "'/>";
                    var ratefilter = parseInt(values.Rating);
                    items += "<input type='hidden' id='ratefilter_" + values.Id + "' value='" + ratefilter + "'/><input type='hidden' id='latarray" + values.Id + "' value='" + values.RestLat + "'/><input type='hidden' id='lngarray" + values.Id + "' value='" + values.RestLng + "'/>";
                    items += "<aside class='resort-row' id='res_" + values.Id + "'><div class='resort-row1' id='divres_" + values.Id + "'>";
                    //Modifed by yuvapriya on 28Feb2017
                    ////if (values.user_ratings_total) {
                    ////    items += "<input type='hidden' class='reviews' value='" + place.user_ratings_total + "'/>"
                    ////}
                    ////else {
                    //items += "<input type='hidden' class='reviews' value='0'/>"
                    ////}
                    if (values.user_ratings_total) {
                        items += "<input type='hidden' class='reviews' value='" + place.user_ratings_total + "'/>"
                    }
                    else {
                    items += "<input type='hidden' class='reviews' value='0'/>"
                    }
                    //end
                    if (values.Price != null) {
                        items += "<input type='hidden' class='priceLev' value='" + values.Price + "'/>"
                    }
                    else {
                        items += "<input type='hidden' class='priceLev' value='0'/>"
                    }
                    if (values.Website != null) {
                        items += "<input type='hidden' class='webhidden' value='" + values.Website + "'>";
                    }
                    else {
                        items += "<input type='hidden' class='webhidden' value='No site'>";
                    }
                    items += "<input type='hidden' id='latarray" + values.Id + "' value='" + values.RestLat + "' class='reslat'><input type='hidden' id='lngarray" + values.Id + "' value='" + values.RestLng + "' class='reslng'>";
                    items += "<input type='hidden' id='isME" + values.Id + "' value='1' class='resME'>";
                    items += "<figure class='resort-img'><div class='tooltip'>Drag to add</div><img src='../Content/images/RestaurantImage/" + values.Photo + "' alt='' id='img_" + values.Id + "'  />";
                    //items += "<span>Rating <strong id='s" + values.Id + "'>" + values.Rating + "</strong></span>";
                    items += "</figure>";
                    items += "<div class='resort-details'><div class='r-left'>";
                    items += "<h3 id='h" + values.Id + "'>" + values.Name + "  </h3><br/>";
                    items += "<p style='height:40px; overflow-y:hidden;' id='p" + values.Id + "'>" + values.Street + "</p>";

                    if (values.Website != null) {
                        items += "<p style='height:18px; overflow-y:hidden;width:100%;text-overflow: ellipsis;white-space: nowrap;overflow: hidden;' id='web" + values.Id + "'><b>Website : </b><a target='_blank' href='" + values.Website + "' style='text-decoration: underline;'>" + values.Website + "</a></p>";
                    }
                    items += "<ul id='rat" + values.Id + "'><b>Ratings : </b>";
                    for (var loopvar_rate = 1; loopvar_rate <= 5; loopvar_rate++) {
                        if (values.Rating >= loopvar_rate) {
                            items += "<li><img src='../Content/images/coffee_full.png' /></li>";
                        }
                        else if (values.Rating > (loopvar_rate - 1) && values.Rating < loopvar_rate) {
                            items += "<li><img src='../Content/images/coffee_half.png' /></li>";
                        }
                        else {
                            items += "<li><img src='../Content/images/coffee_grey_full.png' /></li>";
                        }
                    }
                    //modified by yuvapriya on 28Feb2017
                    ////items += "<li>(" + values.user_ratings_total + " reviews)</li>";

                    //items += "<li>(" + values.user_ratings_total + " reviews)</li>";
                    //end
                    items += "</ul>";
                    if (values.Price != null) {
                        if (values.Price == 0) {
                            items += "<p style='height: 18px;' id='price" + values.Id + "'><b> Price Level:</b> $</p>";
                        }
                        else if (values.Price == 1) {
                            items += "<p style='height: 18px;' id='price" + values.Id + "'><b> Price Level:</b> $</p>";
                        }
                        else if (values.Price == 2) {
                            items += "<p style='height: 18px;' id='price" + values.Id + "'><b> Price Level:</b>  $$</p>";
                        }
                        else if (values.Price == 3) {
                            items += "<p style='height: 18px;' id='price" + values.Id + "'><b> Price Level:</b>  $$$</p>";
                        }
                        else if (values.Price == 4) {
                            items += "<p style='height: 18px;' id='price" + values.Id + "'><b> Price Level:</b>  $$$$</p>";
                        }
                    }
                    else {
                        items += "<p style='height: 18px;' id='price" + values.Id + "'><b> Price Level:</b> $</p>";
                    }

                    items += "<div class='book-add'><span><a href='#' id='inner" + values.Id + "' class='add-trip' onclick='return RestaurantToleft(\"" + values.Id + "\");'>Add Trip</a></span></div></div> ";
                    items += "<div class='r-right'>";
                    //alert('neftq');
                    items += " <ul><li><a href='#'><img src='../Content/images/del-icon.png' alt='' class='del-show' /></a></li>";
                    items += "<li><a  class='fancybox' href='#infowindow' onclick='infomanual(\"3\",\"" + values.Id + "\");'><img src='../Content/images/info-icon.png' alt='' class='example1demo' /></a></li></ul>";
                    items += "</div></div></div>";
                    items += "</aside>";
                    if (items == "") {
                        getmanualEntryVal = 1;
                    }
                    if (apiStatus == 1 && getmanualEntryVal == 1) {
                        $('#rhaa1').html("No results found..");
                    }
                    $('#rhaa1').append(items);
                }
                else if (modenum == 4) {
                    var items = '';
                    items += "<div id='rh" + values.id + "'></div><input type='hidden' class='hide'  id=" + values.id + " value=''><input type='hidden' id='rates_" + values.id + "' value='" + values.rating + "'><aside class='resort-row' id='res_" + values.id + "'><div class='resort-row1' id='divres_" + values.id + "'>";
                    items += "<figure class='resort-img'><img src=" + values.imageUrl + " alt='' id='img_" + values.id + "'  />";
                    items += "</figure>";
                    items += "<div class='resort-details'><div class='r-left'>";
                    items += "<h3 id='h" + values.id + "'>" + values.title + "  </h3><br/>";
                    items += "<p style='height:40px; overflow-y:hidden;' id='p" + values.id + "'>" + values.Street + "</p>";
                    items += "<div class='book-add'><span><a href='#' id='inner" + values.id + "' class='add-trip' onclick='return RestaurantToleft(\"" + values.id + "\");'>Add Trip</a></span></div></div> ";
                    items += "<div class='r-right'>";
                    //alert('neftr');
                    items += " <ul><li><a href='#'><img src='../Content/images/del-icon.png' alt='' class='del-show'/></a></li>";
                    items += "<li><a  class='fancybox' href='#infoactmanu_" + values.id + "'><img src='../Content/images/info-icon.png' alt='' class='example1demo' /></a></li></ul>";
                    items += "<div id='infoactmanu_" + values.id + "' style='display:none'><div class='info-left popup-bg' style='width: 112%;'><figure class='resort-img'><img src=" + values.imageUrl + " alt='' /><span>From <strong>" + values.fromPrice + "</strong></span></figure>";
                    items += "<h3>" + values.title + "</h3>";
                    items += "<ul><b>Ratings : </b>";
                    for (var loopvalue = 0; loopvalue < 5; loopvalue++) {
                        if (values.rating < (loopvalue + 0.5)) {
                            items += "<li><img src='../Content/images/grey-star.png' alt='' /></li>";
                        } else {
                            items += "<li><img src='../Content/images/yellow-star.png' alt='' /></li>";
                        }
                    }
                    items += "<li>(" + values.rating + "/5)</li></ul>";
                    items += "<p><b>Short Description : </b></p>" + values.shortDescription;
                    items += "<p><b>Address : </b>" + values.Street;
                    items += "</div>";
                    items += "</div>";
                    items += "</div></div></div>";
                    items += "</aside>";
                    $('#rhaa1').append(items);
                    if (items == "") {
                        getmanualEntryVal = 1;
                    }
                    if (apiStatus == 1 && getmanualEntryVal == 1) {
                        $('#rhaa1').html("No results found..");
                    }
                    $('#RightSidePart').unblock();
                }
            });
        }
        else {
            getmanualEntryVal = 1;
            if (apiStatus == 1 && getmanualEntryVal == 1) {
                $('#rhaa1').html("No results found..");
            }
        }
        intheTripRHAA(typOfPlc);
    }
    else if (existsVal == 0) {
        $.getJSON('../trip/GetManualEntry', { Mode: modenum, cityname: lat, placename: lng }, function (data) {
            getmanualEntryVal = 0;
            if (data == "No Result") {
                getmanualEntryVal = 1;
            }
            var notexst = 0;
            for (var indx = 0, lmt = manualdata.length; indx < lmt; indx++) {
                if (manualdata[indx].ctyName == $('#selectedcity').html()) {
                    if (modenum == 1) {
                        typOfPlc = "Hotel";
                        manualdata[indx].hotldata = data;
                        notexst = 1;
                        break;
                    }
                    else if (modenum == 2) {
                        typOfPlc = "Attractions";
                        manualdata[indx].attrdata = data;
                        notexst = 1;
                        break;
                    }
                    else if (modenum == 3) {
                        typOfPlc = "Restaurant";
                        manualdata[indx].restdata = data;
                        notexst = 1;
                        break;
                    }
                    else if (modenum == 4) {
                        typOfPlc = "Activities";
                        manualdata[indx].actidata = data;
                        notexst = 1;
                        break;
                    }
                }
            }
            if (notexst == 0)
            {
                if (modenum == 1) {
                    typOfPlc = "Hotel";
                    manualdata.push({
                        ctyName:$('#selectedcity').html(),
                        hotldata : data
                    });
                }
                else if (modenum == 2) {
                    typOfPlc = "Attractions";
                    manualdata.push({
                        ctyName:$('#selectedcity').html(),
                        attrdata: data
                    });
                }
                else if (modenum == 3) {
                    typOfPlc = "Restaurant";
                    manualdata.push({
                        ctyName:$('#selectedcity').html(),
                        restdata: data
                    });
                }
                else if (modenum == 4) {
                    typOfPlc = "Activities";                    
                    manualdata.push({
                        ctyName:$('#selectedcity').html(),
                        actidata : data
                    });
                }
            }
            if (modenum == 4)
            {
                
                    if (ActsForCities.length) {
                        for (var loopvarHotels = 0, CitiesHotelsLength = ActsForCities.length; loopvarHotels < CitiesHotelsLength; loopvarHotels++) {
                            if (ActsForCities[loopvarHotels].cityName.trim() == cty.trim()) {
                                if (ActsForCities[loopvarHotels].Acts.activities) {
                                    if (!(ActsForCities[loopvarHotels].Acts.Manual)) {
                                        if (data != "No Result" && data != "Failed") {
                                            $.each(data, function (keys, values) {
                                                ActsForCities[loopvarHotels].Acts.activities.push(data[keys]);
                                            });
                                            tmpActFlg = 1;
                                        }
                                        ActsForCities[loopvarHotels].Acts.Manual = "1";
                                    }
                                }
                            }
                        }                        
                    }
                
            }
            if (apiStatus == 1) {
                $('#rhaa1').html("");
            }
            if (apiStatus == 1 && getmanualEntryVal == 1) {
                $('#rhaa1').html("No results found..");
            }
            $.each(data, function (keys, values) {
                //alert("nefta");
                //debugger

                if (modenum == 2) {
                    var ratefilter = parseInt(values.Rating);
                    var items = '';
                    items += "<div id='rh" + values.Id + "'></div><input type='hidden' class='hide'  id=" + values.Id + " value='" + values.Type + "'><input type='hidden' id='rates_" + values.Id + "' value='" + values.Rating + "'/>";
                    items += "<input type='hidden' id='ratefilter_" + values.Id + "' value='" + ratefilter + "'/><input type='hidden' id='latarray" + values.Id + "' value='" + values.AttrLat + "'/><input type='hidden' id='lngarray" + values.Id + "' value='" + values.AttrLng + "'/>";
                    items += "<aside class='resort-row' id='res_" + values.Id + "'><div class='resort-row1' id='divres_" + values.Id + "'>";
                    items += "<input type='hidden' id='latarray" + values.Id + "' value='" + values.AttrLat + "' class='reslat'><input type='hidden' id='lngarray" + values.Id + "' value='" + values.AttrLng + "' class='reslng'>";
                    items += "<input type='hidden' id='isME" + values.Id + "' value='1' class='resME'>";
                    //modified by yuvapriya on 28Feb2017
                    ////if (place.user_ratings_total) {
                    ////    items += "<input type='hidden' class='reviews' value='" + place.user_ratings_total + "'/>"
                    ////}
                    ////else {
                    //items += "<input type='hidden' class='reviews' value='0'/>"
                    ////}
                    if (typeof (values.user_ratings_total) !== "undefined") {
                        if (values.user_ratings_total) {
                            items += "<input type='hidden' class='reviews' value='" + values.user_ratings_total + "'/>"
                        }
                    }
                    else {
                        items += "<input type='hidden' class='reviews' value='0'/>"
                    }
                    //end
                    if (values.Price != null) {
                        items += "<input type='hidden' class='priceLev' value='" + values.Price + "'/>"
                    }
                    else {
                        items += "<input type='hidden' class='priceLev' value='0'/>"
                    }
                    if (values.Website != null) {
                        items += "<input type='hidden' class='webhidden' value='" + values.Website + "'>";
                    }
                    else {
                        items += "<input type='hidden' class='webhidden' value='No site'>";
                    }

                    items += "<figure class='resort-img'><div class='tooltip'>Drag to add</div><img src='../Content/images/AttractionImage/" + values.Photo + "' alt='' id='img_" + values.Id + "'  />";
                    items += "<span>Rating <strong id='s" + values.Id + "'>" + values.Rating + "</strong></span></figure>";
                    items += "<div class='resort-details'><div class='r-left'>";
                    items += "<h3 id='h" + values.Id + "'>" + values.Name + "  </h3><br/>";
                    items += "<p style='height:40px; overflow-y:hidden;' id='p" + values.Id + "'>" + values.Street + "</p>";
                    if (values.Website != null) {
                        items += "<p style='height:18px; overflow-y:hidden;width:100%;text-overflow: ellipsis;white-space: nowrap;overflow: hidden;' id='web" + values.Id + "'><b>Website : </b><a target='_blank' href='" + values.Website + "' style='text-decoration: underline;'>" + values.Website + "</a></p>";
                    }
                    items += "<ul id='rat" + values.Id + "'><b>Ratings : </b>";
                    for (var loopvar_rate = 1; loopvar_rate <= 5; loopvar_rate++) {
                        if (values.Rating >= loopvar_rate) {
                            items += "<li><img src='../Content/images/tower_full.png' /></li>";
                        }
                        else if (values.Rating > (loopvar_rate - 1) && values.Rating < loopvar_rate) {
                            items += "<li><img src='../Content/images/tower_half.png' /></li>";
                        }
                        else {
                            items += "<li><img src='../Content/images/tower_grey_full.png' /></li>";
                        }
                    }
                    //modified by yuvapriya on 28Feb2017
                    ////if (values.user_ratings_total) {
                    ////    items += "<li>(" + values.user_ratings_total + " reviews)</li>";
                    ////}
                    if (values.user_ratings_total) {
                        //items += "<li>(" + values.user_ratings_total + " reviews)</li>";
                    }
                    //end
                    items += "</ul>";
                    if (values.Price) {
                        if (values.Price == 0) {
                            items += "<p style='height: 18px;' id='price" + values.Id + "'><b> Price Level:</b>$</p> ";
                        }
                        else if (values.Price == 1) {
                            items += "<p style='height: 18px;' id='price" + values.Id + "'><b> Price Level:</b>$</p> ";
                        }
                        else if (values.Price == 2) {
                            items += "<p style='height: 18px;' id='price" + values.Id + "'><b> Price Level:</b>$$</p> ";
                        }
                        else if (values.Price == 3) {
                            items += "<p style='height: 18px;' id='price" + values.Id + "'><b> Price Level:</b>$$$</p> ";
                        }
                        else if (values.Price == 4) {
                            items += "<p style='height: 18px;' id='price" + values.Id + "'><b> Price Level:</b>$$$$</p> ";
                        }
                    }
                    else {
                        items += "<p style='height: 18px;' id='price" + values.Id + "'><b> Price Level:</b>$</p> ";
                    }
                    items += "<div class='book-add'><span><a href='#' id='inner" + values.Id + "' class='add-trip' onclick='return RestaurantToleft(\"" + values.Id + "\");'>Add Trip</a></span></div></div> ";
                    items += "<div class='r-right'>";
                    //alert('neftt');
                    items += " <ul><li><a href='#'><img src='../Content/images/del-icon.png' alt='' class='del-show' /></a></li>";
                    items += "<li><a  class='fancybox' href='#infowindow' onclick='infomanual(\"2\",\"" + values.Id + "\");'><img src='../Content/images/info-icon.png' alt='' class='example1demo' /></a></li></ul>";
                    items += "</div></div></div>";
                    items += "</aside>";
                    if (items == "") {
                        getmanualEntryVal = 1;
                    }
                    if (apiStatus == 1 && getmanualEntryVal == 1) {
                        $('#rhaa1').html("No results found..");
                    }
                    $('#rhaa1').append(items);
                }
                else if (modenum == 1) {
                    //alert('neftm');
                    var items = '';
                    items += "<aside class='resort-row' id='res_" + values.hotelId + "'><div class='resort-row1' id='divres_" + values.hotelId + "'>";
                    items += "<input type='hidden' id='" + values.hotelId + "' value='" + values.hotelId + "' class='hide'><input type='hidden' id='latarray" + values.hotelId + "' value='" + values.latitude + "' class='reslat'/><input type='hidden' id='lngarray" + values.hotelId + "' value='" + values.longitude + "' class='reslng'/>";
                    items += "<input type='hidden' id='isME" + values.hotelId + "' value='1' class='resME'>";
                    items += "<input type='hidden' id='htlRt" + values.hotelId + "' value='" + values.Rating + "' class='htlrt'><input type='hidden' id='htlCls" + values.hotelId + "' value='" + values.hotelRating + "' class='htlCls'>";
                    items += "<input type='hidden' id='htlPrc" + values.hotelId + "' value='" + values.highRate + "' class='htlPrc'>";
                    items += "<figure class='resort-img'>";
                    items += "<div class='tooltip'>Drag to add</div><img src=" + values.thumbNailUrl + " alt=''/>";
                    items += "<span>Night from $<strong id='s" + values.hotelId + "'>" + values.highRate + "</strong></span></figure>";
                    items += "<div class='resort-details'><div class='r-left'><span class='hotel_identify' style='display:none'></span>";
                    items += "<h3 id='h" + values.hotelId + "'>" + values.name + "  </h3><ul><b>Hotel class :</b>";
                    for (var loopvar_hc = 1; loopvar_hc <= 5; loopvar_hc++) {
                        if (values.hotelRating >= loopvar_hc) {
                            items += "<li><img src='../Content/images/yellow-star1.png' /></li>";
                        }
                        else if (values.hotelRating > (loopvar_hc - 1) && values.hotelRating < loopvar_hc) {
                            items += "<li><img src='../Content/images/yellow-gray-starNew.png' /></li>";
                        }
                        else {
                            items += "<li><img src='../Content/images/grey-starNew.png' /></li>";
                        }
                    }
                    items += "(" + (parseFloat(values.hotelRating)).toFixed(1) + ")</ul>";
                    items += "<ul><b>Rating :</b>";
                    for (var loopvar_rate = 1; loopvar_rate <= 5; loopvar_rate++) {
                        if (values.Rating >= loopvar_rate) {
                            items += "<li><img src='../Content/images/hotel-color.png' /></li>";
                        }
                        else if (values.Rating > (loopvar_rate - 1) && values.Rating < loopvar_rate) {
                            items += "<li><img src='../Content/images/hotel-half.png' /></li>";
                        }
                        else {
                            items += "<li><img src='../Content/images/hotel-gray.png' /></li>";
                        }
                    }
                    items += "<li>(" + (parseFloat(values.Rating)).toFixed(1) + "/5)</li></ul>";
                    items += "<p id='pho_" + values.hotelId + "' style='height:40px; overflow-y:hidden;'>" + values.shortDescription + "</p>";
                    //items += "<p style='height:40px; overflow-y:hidden;' id='p" + values.hotelId + "'>" + values.address1 + "</p>";
                    //***Modified by Yuvapriya
                    //items += "<div class='book-add'><span><a href='#' id='inner" + values.hotelId + "' class='add-trip' onclick='return hotelsToleft(\"" + values.hotelId + "\");'>Add Trip</a></span><span id='book" + values.hotelId + "' style='display:none;'><a href='#' class='book'>Book</a></span></div></div> ";
                    items += "<div class='book-add'><span><a href='#' id='inner" + values.hotelId + "' class='add-trip' onclick='return hotelsToleft(\"" + values.hotelId + "\");'>Add Trip</a></span></div></div> ";
                    //***End***
                    items += "<div class='r-right'>";
                    //alert('neftu');
                    items += " <ul><li><a href='#'><img src='../Content/images/del-icon.png' alt='' class='del-show' onclick=''/></a></li>";
                    items += "<li><a  class='fancybox' href='#infohomanu_" + values.hotelId + "'><img src='../Content/images/info-icon.png' alt='' class='example1demo' /></a></li></ul>";
                    items += "</div></div></div>";
                    items += "</aside>";
                    items += "<div id='infohomanu_" + values.hotelId + "' style='display:none'><div class='info-left popup-bg' style='width: 112%;'><figure class='resort-img'><img src='../Content/images/HotelImages/" + values.thumbNailUrl + "' alt='' /><span>Night from <strong>$" + values.highRate + "</strong></span></figure>";
                    items += "<h3>" + values.name + "</h3>";
                    items += "<ul><b>Hotel class : </b>";
                    for (var loopvar_hc = 1; loopvar_hc <= 5; loopvar_hc++) {
                        if (values.hotelRating >= loopvar_hc) {
                            items += "<li><img src='../Content/images/yellow-star1.png' /></li>";
                        }
                        else if (values.hotelRating > (loopvar_hc - 1) && values.hotelRating < loopvar_hc) {
                            items += "<li><img src='../Content/images/yellow-gray-starNew.png' /></li>";
                        }
                        else {
                            items += "<li><img src='../Content/images/grey-starNew.png' /></li>";
                        }
                    }
                    items += "(" + (parseFloat(values.hotelRating)).toFixed(1) + ")</ul>";
                    items += "<ul><b>Rating :</b>"
                    for (var loopvar_rate = 1; loopvar_rate <= 5; loopvar_rate++) {
                        if (values.Rating >= loopvar_rate) {
                            items += "<li><img src='../Content/images/hotel-color.png' /></li>";
                        }
                        else if (values.Rating > (loopvar_rate - 1) && values.Rating < loopvar_rate) {
                            items += "<li><img src='../Content/images/hotel-half.png' /></li>";
                        }
                        else {
                            items += "<li><img src='../Content/images/hotel-gray.png' /></li>";
                        }
                    }
                    items += "<li>(" + (parseFloat(values.Rating)).toFixed(1) + "/5)</li></ul>";
                    items += "<p><b>Short Description : </b>" + values.shortDescription + "</p>";
                    items += "<p><b>Address : </b>" + values.address1 + "</p>";
                    items += "<p><b>Rate starts from : </b>$" + values.highRate + "</p>";
                    if (values.deepLink != null && values.deepLink != "null") {
                        items += "<p><b>For booking :</b>" + values.deepLink + "</p>";
                    }
                    if (values.Amenities != null) {
                        items += "<p><b>Hotel Amenities : </b>" + values.Amenities + "</p>"
                    }
                    items += "</div>";
                    items += "</div>";
                    if (items == "") {
                        getmanualEntryVal = 1;
                    }
                    if (apiStatus == 1 && getmanualEntryVal == 1) {
                        $('#rhaa1').html("No results found..");
                    }
                    $('#rhaa1').append(items);
                }
                else if (modenum == 3) {
                    var items = '';
                    items += "<div id='rh" + values.Id + "'></div><input type='hidden' class='hide'  id=" + values.Id + " value='" + values.Type + "'><input type='hidden' id='rates_" + values.Id + "' value='" + values.Rating + "'/>";
                    var ratefilter = parseInt(values.Rating);
                    items += "<input type='hidden' id='ratefilter_" + values.Id + "' value='" + ratefilter + "'/><input type='hidden' id='latarray" + values.Id + "' value='" + values.RestLat + "'/><input type='hidden' id='lngarray" + values.Id + "' value='" + values.RestLng + "'/>";
                    items += "<aside class='resort-row' id='res_" + values.Id + "'><div class='resort-row1' id='divres_" + values.Id + "'>";
                    //modified by yuvapriya on 28Feb2017 
                    //if (values.user_ratings_total) {
                    //    items += "<input type='hidden' class='reviews' value='" + place.user_ratings_total + "'/>"
                    //}
                    //else {
                    items += "<input type='hidden' class='reviews' value='0'/>"
                    //}

                    if (values.user_ratings_total) {
                        items += "<input type='hidden' class='reviews' value='" + place.user_ratings_total + "'/>"
                    }
                    else {
                    items += "<input type='hidden' class='reviews' value='0'/>"
                    }
                    //end

                    if (values.Price != null) {
                        items += "<input type='hidden' class='priceLev' value='" + values.Price + "'/>"
                    }
                    else {
                        items += "<input type='hidden' class='priceLev' value='0'/>"
                    }
                    if (values.Website != null) {
                        items += "<input type='hidden' class='webhidden' value='" + values.Website + "'>";
                    }
                    else {
                        items += "<input type='hidden' class='webhidden' value='No site'>";
                    }
                    items += "<input type='hidden' id='latarray" + values.Id + "' value='" + values.RestLat + "' class='reslat'><input type='hidden' id='lngarray" + values.Id + "' value='" + values.RestLng + "' class='reslng'>";
                    items += "<input type='hidden' id='isME" + values.Id + "' value='1' class='resME'>";
                    items += "<figure class='resort-img'><div class='tooltip'>Drag to add</div><img src='../Content/images/RestaurantImage/" + values.Photo + "' alt='' id='img_" + values.Id + "'  />";
                    //items += "<span>Rating <strong id='s" + values.Id + "'>" + values.Rating + "</strong></span>";
                    items += "</figure>";
                    items += "<div class='resort-details'><div class='r-left'>";
                    items += "<h3 id='h" + values.Id + "'>" + values.Name + "  </h3><br/>";
                    items += "<p style='height:40px; overflow-y:hidden;' id='p" + values.Id + "'>" + values.Street + "</p>";

                    if (values.Website != null) {
                        items += "<p style='height:18px; overflow-y:hidden;width:100%;text-overflow: ellipsis;white-space: nowrap;overflow: hidden;' id='web" + values.Id + "'><b>Website : </b><a target='_blank' href='" + values.Website + "' style='text-decoration: underline;'>" + values.Website + "</a></p>";
                    }
                    items += "<ul id='rat" + values.Id + "'><b>Ratings : </b>";
                    for (var loopvar_rate = 1; loopvar_rate <= 5; loopvar_rate++) {
                        if (values.Rating >= loopvar_rate) {
                            items += "<li><img src='../Content/images/coffee_full.png' /></li>";
                        }
                        else if (values.Rating > (loopvar_rate - 1) && values.Rating < loopvar_rate) {
                            items += "<li><img src='../Content/images/coffee_half.png' /></li>";
                        }
                        else {
                            items += "<li><img src='../Content/images/coffee_grey_full.png' /></li>";
                        }
                    }
                    //modified by yuvapriya on 28feb2017
                    //items += "<li>(" + values.user_ratings_total + " reviews)</li>";

                    //items += "<li>(" + values.user_ratings_total + " reviews)</li>";
                    //end
                    items += "</ul>";
                    if (values.Price != null) {
                        if (values.Price == 0) {
                            items += "<p style='height: 18px;' id='price" + values.Id + "'><b> Price Level:</b>$</p>";
                        }
                        else if (values.Price == 1) {
                            items += "<p style='height: 18px;' id='price" + values.Id + "'><b> Price Level:</b>$</p>";
                        }
                        else if (values.Price == 2) {
                            items += "<p style='height: 18px;' id='price" + values.Id + "'><b> Price Level:</b>$$</p>";
                        }
                        else if (values.Price == 3) {
                            items += "<p style='height: 18px;' id='price" + values.Id + "'><b> Price Level:</b>$$$</p>";
                        }
                        else if (values.Price == 4) {
                            items += "<p style='height: 18px;' id='price" + values.Id + "'><b> Price Level:</b>$$$$</p>";
                        }
                    }
                    else {
                        items += "<p style='height: 18px;' id='price" + values.Id + "'><b> Price Level:</b>$</p>";
                    }

                    items += "<div class='book-add'><span><a href='#' id='inner" + values.Id + "' class='add-trip' onclick='return RestaurantToleft(\"" + values.Id + "\");'>Add Trip</a></span></div></div> ";
                    items += "<div class='r-right'>";
                    //alert('neftw');
                    items += " <ul><li><a href='#'><img src='../Content/images/del-icon.png' alt='' class='del-show'/></a></li>";
                    items += "<li><a  class='fancybox' href='#infowindow' onclick='infomanual(\"3\",\"" + values.Id + "\");'><img src='../Content/images/info-icon.png' alt='' class='example1demo' /></a></li></ul>";
                    items += "</div></div></div>";
                    items += "</aside>";
                    if (items == "") {
                        getmanualEntryVal = 1;
                    }
                    if (apiStatus == 1 && getmanualEntryVal == 1) {
                        $('#rhaa1').html("No results found..");
                    }
                    $('#rhaa1').append(items);
                }
                else if (modenum == 4) {
                    if (tmpActFlg == 0) {
                        var items = '';
                        items += "<div id='rh" + values.id + "'></div><input type='hidden' class='hide'  id=" + values.id + " value=''><input type='hidden' id='rates_" + values.id + "' value='" + values.Rating + "'><aside class='resort-row' id='res_" + values.id + "'><div class='resort-row1' id='divres_" + values.id + "'>";
                        items += "<figure class='resort-img'><img src=" + values.imageUrl + " alt='' id='img_" + values.id + "'  />";
                        items += "</figure>";
                        items += "<div class='resort-details'><div class='r-left'>";
                        items += "<h3 id='h" + values.id + "'>" + values.title + "  </h3><br/>";
                        items += "<p style='height:40px; overflow-y:hidden;' id='p" + values.id + "'>" + values.Street + "</p>";
                        items += "<div class='book-add'><span><a href='#' id='inner" + values.id + "' class='add-trip' onclick='return RestaurantToleft(\"" + values.id + "\");'>Add Trip</a></span></div></div> ";
                        items += "<div class='r-right'>";
                        //alert('neftv');
                        items += " <ul><li><a href='#'><img src='../Content/images/del-icon.png' alt='' class='del-show'/></a></li>";
                        items += "<li><a  class='fancybox' href='#infowindow_hotels_activities' onclick='getActivityInfo('" + activities_forsort[loopvar].id + "','" + $("#selectedcity").html() + "');'><img src='../Content/images/info-icon.png' alt='' class='example1demo' /></a></li></ul>";
                        items += "<div id='infoactmanu_" + values.id + "' style='display:none'><div class='info-left popup-bg' style='width: 112%;'><figure class='resort-img'><img src='" + values.imageUrl + "' alt='' /><span>From <strong>" + values.fromPrice + "</strong></span></figure>";
                        items += "<h3>" + values.title + "</h3>";
                        items += "<ul><b>Ratings : </b>";
                        for (var loopvalue = 0; loopvalue < 5; loopvalue++) {
                            if (values.rating < (loopvalue + 0.5)) {
                                items += "<li><img src='../Content/images/grey-star.png' alt='' /></li>";
                            } else {
                                items += "<li><img src='../Content/images/yellow-star.png' alt='' /></li>";
                            }
                        }
                        items += "<li>(" + values.rating + "/5)</li></ul>";
                        items += "<p><b>Short Description : </b></p>" + values.shortDescription;
                        items += "<p><b>Address : </b>" + values.Street;
                        items += "</div>";
                        items += "</div>";
                        items += "</div></div></div>";
                        items += "</aside>";
                        $('#rhaa1').append(items);
                        if (items == "") {
                            getmanualEntryVal = 1;
                        }
                        if (apiStatus == 1 && getmanualEntryVal == 1) {
                            $('#rhaa1').html("No results found..");
                        }
                        $('#RightSidePart').unblock();
                    }
                    else
                    {
                        GetActivities("list");
                    }
                }
            });
            intheTripRHAA(typOfPlc);
            if(modenum == 1)
            {
                sortHotels();
            }
        });
    }
}

function GetMnlHtlinfo(Htlid) {
    var tmpdata = [],gotflg=0;
    for (var indx = 0, lmt = manualdata.length; indx < lmt; indx++) {
        if (manualdata[indx].ctyName == $('#selectedcity').html()) {
            if (manualdata[indx].hotldata) {
                tmpdata = manualdata[indx].hotldata;
                gotflg = 1;                
            }
            break;
        }
    }
    if (gotflg == 1) {
        $.each(tmpdata, function (keys, values) {
            if (values.Id == Htlid) {
                $('#infowindow_hotels_activities').html("");
                //var items = "<div id='infohomanu_" + values.Id + "'><div class='info-left popup-bg' style='width: 112%;'>";
                var items = "<figure class='resort-img'><img src='../Content/images/HotelImages/" + values.Photo + "' alt='' /><span>Night from <strong>$" + values.Price + "</strong></span></figure>";
                items += "<h3>" + values.Name + "</h3>";
                items += "<ul><b>Hotel class : </b>";
                for (var loopvar_hc = 1; loopvar_hc <= 5; loopvar_hc++) {
                    if (values.Hclass >= loopvar_hc) {
                        items += "<li><img src='../Content/images/yellow-star1.png' /></li>";
                    }
                    else if (values.Hclass > (loopvar_hc - 1) && values.Hclass < loopvar_hc) {
                        items += "<li><img src='../Content/images/yellow-gray-starNew.png' /></li>";
                    }
                    else {
                        items += "<li><img src='../Content/images/grey-starNew.png' /></li>";
                    }
                }
                items += "(" + (parseFloat(values.Hclass)).toFixed(1) + ")</ul>";
                items += "<ul><b>Rating :</b>"
                for (var loopvar_rate = 1; loopvar_rate <= 5; loopvar_rate++) {
                    if (values.Rating >= loopvar_rate) {
                        items += "<li><img src='../Content/images/hotel-color.png' /></li>";
                    }
                    else if (values.Rating > (loopvar_rate - 1) && values.Rating < loopvar_rate) {
                        items += "<li><img src='../Content/images/hotel-half.png' /></li>";
                    }
                    else {
                        items += "<li><img src='../Content/images/hotel-gray.png' /></li>";
                    }
                }
                items += "<li>(" + (parseFloat(values.Rating)).toFixed(1) + "/5)</li></ul>";
                items += "<p><b>Short Description : </b>" + values.desc + "</p>";
                items += "<p><b>Address : </b>" + values.Street + "</p>";
                items += "<p><b>Rate starts from : </b>$" + values.Price + "</p>";
                if (values.BookURL != null && values.BookURL != "null") {
                    items += "<p><b>For booking :</b>" + values.BookURL + "</p>";
                }
                if (values.Amenities != null) {
                    items += "<p><b>Hotel Amenities : </b>" + values.Amenities + "</p>"
                }
                //items += "</div>";
                //items += "</div>";
                $('#infowindow_hotels_activities').append(items);
                return false;
            }
        });
    }
    else
    {
        $.getJSON('../trip/GetManualEntry', { Mode: '1', cityname: $('#latarray').val(), placename: $('#lngarray').val() }, function (data) {
            var notexst = 0;
            for (var indx = 0, lmt = manualdata.length; indx < lmt; indx++) {
                if (manualdata[indx].ctyName == $('#selectedcity').html()) {
                    manualdata[indx].hotldata = data;
                    notexst = 1;
                    break;
                }
            }
            if (notexst == 0) {
                manualdata.push({
                    ctyName: $('#selectedcity').html(),
                    hotldata: data
                });
            }
            GetMnlHtlinfo(Htlid);
        });
    }
}

function GetApiHtlinfo(Htlid) {
    $('#infowindow_hotels_activities').html("");
    var array_of_hotels = [], place = [], placediv1 = "";

    for (var loopvar = 0, AddDayLength = hotelsForCities.length; loopvar < AddDayLength; loopvar++) {
        if ((hotelsForCities[loopvar].cityName).trim() == $('#selectedcity').html().trim()) {
            array_of_hotels = hotelsForCities[loopvar].Hotels.HotelListResponse.HotelList['HotelSummary'].slice(0);
            place = array_of_hotels.filter(function (obj) {
                return obj.hotelId == Htlid;
            });
            var descri = "", amenitiesare = "";
            var amenityvalues = place[0].amenityMask;
            if (place[0].amenityMask & 1) {
                amenitiesare += "<li>Business Center</li>"
            }
            if (place[0].amenityMask & 2) {
                amenitiesare += "<li>Fitness Center</li>";
            }
            if (place[0].amenityMask & 8) {
                amenitiesare += "<li>Internet Access Available</li>";
            }
            if (place[0].amenityMask & 16) {
                amenitiesare += "<li>Kids' Activities</li>";
            }
            if (place[0].amenityMask & 32) {
                amenitiesare += "<li>Kitchen or Kitchenette</li>";
            }
            if (place[0].amenityMask & 64) {
                amenitiesare += "<li>Pets Allowed</li>";
            }
            if (place[0].amenityMask & 128) {
                amenitiesare += "<li>Pool</li>";
            }
            if (place[0].amenityMask & 256) {
                amenitiesare += "<li>Restaurant On-site</li>";
            }
            if (place[0].amenityMask & 512) {
                amenitiesare += "<li>Spa On-site</li>";
            }
            if (place[0].amenityMask & 2048) {
                amenitiesare += "<li>Breakfast</li>";
            }
            if (place[0].amenityMask & 4096) {
                amenitiesare += "<li>Babysitting</li>";
            }
            if (place[0].amenityMask & 16384) {
                amenitiesare += "<li>Parking</li>";
            }
            if (place[0].amenityMask & 32768) {
                amenitiesare += "<li>Room Service </li>";
            }
            if (place[0].amenityMask & 262144) {
                amenitiesare += "<li>Roll-in Shower</li>";
            }
            if (place[0].amenityMask & 524288) {
                amenitiesare += "<li>Handicapped Parking</li>";
            }
            if (place[0].amenityMask & 2097152) {
                amenitiesare += "<li>Accessibility Equipment for the Deaf</li>";
            }
            if (place[0].amenityMask & 4194304) {
                amenitiesare += "<li>Braille or Raised Signage</li>";
            }
            if (place[0].amenityMask & 8388608) {
                amenitiesare += "<li>Free Airport Shuttle</li>";
            }
            if (place[0].amenityMask & 16777216) {
                amenitiesare += "<li>Indoor Pool</li>";
            }
            if (place[0].amenityMask & 33554432) {
                amenitiesare += "<li>Outdoor Pool</li>";
            }
            placediv1 += "<figure class='resort-img'>";
            var imgurl_split = place[0].thumbNailUrl.split('_');
            imgurl_split[imgurl_split.length - 1] = imgurl_split[imgurl_split.length - 1].replace('t.jpg', 'b.jpg');
            var imgurl = imgurl_split.join('_');
            placediv1 += "<img src='http://media3.expedia.com" + imgurl + "' alt='' />";
            placediv1 += "<span>Night from <strong>$" + place[0].highRate + "</strong></span></figure>";
            placediv1 += "<h2>" + place[0].name + "</h2>";
            placediv1 += "<ul><b>Hotel class :</b>";
            for (var loopvar_hc = 1; loopvar_hc <= 5; loopvar_hc++) {
                if (place[0].hotelRating >= loopvar_hc) {
                    placediv1 += "<li><img src='../Content/images/yellow-star1.png' /></li>";
                }
                else if (place[0].hotelRating > (loopvar_hc - 1) && place[0].hotelRating < loopvar_hc) {
                    placediv1 += "<li><img src='../Content/images/yellow-gray-starNew.png' /></li>";
                }
                else {
                    placediv1 += "<li><img src='../Content/images/grey-starNew.png' /></li>";
                }
            }
            placediv1 += "(" + place[0].hotelRating + ")</ul>";
            descri = "<p><b>Short Description : </b></p>" + (place[0].shortDescription).replace('&lt;p&gt;&lt;b&gt;Property Location&lt;/b&gt; &lt;br /&gt;', '') + "...";
            placediv1 += "<p><b>Address : </b>" + place[0].address1;
            if (place[0].address2 != null && place[0].address2 != "") {
                placediv1 += "," + place[0].address2;
            }
            if (place[0].city != null && place[0].city != "") {
                placediv1 += "," + place[0].city;
            }
            if (place[0].postalCode != null && place[0].postalCode != "") {
                placediv1 += "-" + place[0].postalCode;
            }
            placediv1 += "</p><p><b>Landmark : </b>" + place[0].locationDescription + "</p><br/><p>" + descri + " </p>";
            var propertycat = "";
            if (place[0].propertyCategory == 1) {
                propertycat = "hotel";
            }
            else if (place[0].propertyCategory == 2) {
                propertycat = "suite";
            }
            else if (place[0].propertyCategory == 3) {
                propertycat = "resort";
            }
            else if (place[0].propertyCategory == 4) {
                propertycat = "vacation rental/condo";
            }
            else if (place[0].propertyCategory == 5) {
                propertycat = "bed & breakfast";
            }
            else if (place[0].propertyCategory == 6) {
                propertycat = "all-inclusive ";
            }
            if (propertycat != "hotel") {
                placediv1 += "</p><p><b>Property Category : </b>" + propertycat + "</p>";
            }
            placediv1 += "<p><b>Rate starts from : </b>$" + place[0].highRate + "</p>";
            placediv1 += "<p><b>For booking : </b><a href='" + place[0].deepLink + "'>Click here</a></p>";
            if (amenitiesare != "") {
                placediv1 += "<br/><p><b>Hotel Amenities : </b>" + amenitiesare + "</p>";
            }
            placediv1 += "</div> </div> </div>";
            $('#infowindow_hotels_activities').append(placediv1);
        }
    }
}

/************Travel Div Resize***************/

function TravelDivResize(size)
{
    for (var TravelModeIndex = 0; TravelModeIndex < size; TravelModeIndex++)
    {
        var time = $('#compl').find('.travel').eq(TravelModeIndex).find('p').html();
        var day = "day";
        var days = "days";
        var hours = "hours";
        if (time.indexOf(day) != -1) {
            $('#compl').find('.travel').eq(TravelModeIndex).find('p').html("Time taken by car is " + traveltimeinmins + " mins");
            $('#compl').find('.travel').eq(TravelModeIndex).find('p').append("     <img src='../Content/images/edit.png' alt='' class='editimg' style='height: 15px;width: 15px;'/>");
            $('#compl').find('.travel').eq(TravelModeIndex).find(".hiddriving").val(traveltimeinmins + " mins");
        }
        else if (time.indexOf(days) != -1) {
            $('#compl').find('.travel').eq(TravelModeIndex).find('p').html("Time taken by car is " + traveltimeinmins + " mins");
            $('#compl').find('.travel').eq(TravelModeIndex).find('p').append("     <img src='../Content/images/edit.png' alt='' class='editimg' style='height: 15px;width: 15px;'/>");
            $('#compl').find('.travel').eq(TravelModeIndex).find(".hiddriving").val(traveltimeinmins + " mins");
        }
        else if (time.indexOf(hours) != -1) {
            $('#compl').find('.travel').eq(TravelModeIndex).find('p').html("Time taken by car is " + traveltimeinmins + " mins");
            $('#compl').find('.travel').eq(TravelModeIndex).find('p').append("     <img src='../Content/images/edit.png' alt='' class='editimg' style='height: 15px;width: 15px;'/>");
            $('#compl').find('.travel').eq(TravelModeIndex).find(".hiddriving").val(traveltimeinmins + " mins");
        }
    }
    for (var TravelModeIndex = 0; TravelModeIndex < size; TravelModeIndex++) {
        var time = $('#compl').find('.travel').eq(TravelModeIndex).find('p').html();
        var hours = "hours";
        var hour = "hour";
        var mins = "mins";
        if (time.indexOf(hour) != -1) {
            var array = time.split(' ');            
            var amount = array[5] / .50;
            var timespent = amount * 58;
            if (time.indexOf(mins) != -1) {
                    timespent = array[7] / .50 + timespent;
                    $('#compl').find('.travel').eq(TravelModeIndex).css("height", timespent);
            }
            else {
                $('#compl').find('.travel').eq(TravelModeIndex).css("height", timespent);
            }
        }
        else if (time.indexOf(hours) != -1) {
            var array = time.split(' ');
            var amount = array[5] / .50;
            var timespent = amount * 58;
            if (time.indexOf(mins) != -1) {
                    timespent = array[7] / .50 + timespent;
                    $('#compl').find('.travel').eq(TravelModeIndex).css("height", timespent);
            }
            else {
                $('#compl').find('.travel').eq(TravelModeIndex).css("height", timespent);
            }
        }
        else if (time.indexOf(mins) != -1) {
            var array = time.split(' ');
            if (array[5] > 20) {
                var amount = array[5] / 30;
                var timespent = amount * 58;
                $('#compl').find('.travel').eq(TravelModeIndex).css("height", timespent);
            }
        }
    }
}

function CustomTravelDivResize(customTravelsize) {
    for (var TravelModeIndex = 0; TravelModeIndex < customTravelsize; TravelModeIndex++) {
        var time = $('#compl').find('.custom_travel').eq(TravelModeIndex).find('p').html();
        var hours = "hours";
        var hour = "hour";
        var mins = "mins";
        if (time.indexOf(hour) != -1) {
            var array = time.split(' ');
            var amount = array[5] / .50;
            var timespent = amount * 58;
            if (time.indexOf(mins) != -1) {
                    timespent = array[7] / .50 + timespent;
                    $('#compl').find('.custom_travel').eq(TravelModeIndex).css("height", timespent);
            }
            else {
                $('#compl').find('.custom_travel').eq(TravelModeIndex).css("height", timespent);
            }
        }
        else if (time.indexOf(hours) != -1) {
            var array = time.split(' ');
            var amount = array[5] / .50;
            var timespent = amount * 58;
            if (time.indexOf(mins) != -1) {
                    timespent = array[7] / .50 + timespent;
                    $('#compl').find('.custom_travel').eq(TravelModeIndex).css("height", timespent);
            }
            else {
                $('#compl').find('.custom_travel').eq(TravelModeIndex).css("height", timespent);
            }
        }
        else if (time.indexOf(mins) != -1) {
            var array = time.split(' ');
            if (array[5] > 20) {
                var amount = array[5] / 30;
                var timespent = amount * 58;
                $('#compl').find('.custom_travel').eq(TravelModeIndex).css("height", timespent);
            }
        }
    }  

}

function TravelDivResizeToLeft(size)
{
    for (var TravelModeIndex = 0; TravelModeIndex < size; TravelModeIndex++) {
        var time = $('#compl').find('.travel').eq(TravelModeIndex).find('p').html();
        var day = "day";
        var days = "days";
        var hours = "hours";
        if (time.indexOf(day) != -1)
        {
            $('#compl').find('.travel').eq(TravelModeIndex).find('p').html("Time taken by car is " + traveltimeinmins + " mins");
            $('#compl').find('.travel').eq(TravelModeIndex).find('p').append("     <img src='../Content/images/edit.png' alt='' class='editimg' style='height: 15px;width: 15px;'/>");
            $('#compl').find('.travel').eq(TravelModeIndex).find(".hiddriving").val(traveltimeinmins + " mins");
        }
        else if (time.indexOf(days) != -1) {
            $('#compl').find('.travel').eq(TravelModeIndex).find('p').html("Time taken by car is " + traveltimeinmins + " mins");
            $('#compl').find('.travel').eq(TravelModeIndex).find('p').append("     <img src='../Content/images/edit.png' alt='' class='editimg' style='height: 15px;width: 15px;'/>");
            $('#compl').find('.travel').eq(TravelModeIndex).find(".hiddriving").val(traveltimeinmins + " mins");
        }
        else if (time.indexOf(hours) != -1) {
            $('#compl').find('.travel').eq(TravelModeIndex).find('p').html("Time taken by car is " + traveltimeinmins + " mins");
            $('#compl').find('.travel').eq(TravelModeIndex).find('p').append("     <img src='../Content/images/edit.png' alt='' class='editimg' style='height: 15px;width: 15px;'/>");
            $('#compl').find('.travel').eq(TravelModeIndex).find(".hiddriving").val(traveltimeinmins + " mins");
        }
    }
    for (var TravelModeIndex = 0; TravelModeIndex < size; TravelModeIndex++) {
        var time = $('#compl').find('.travel').eq(TravelModeIndex).find('p').html();
        var hours = "hours";
        var hour = "hour";
        var mins = "mins";
        if (time.indexOf(hour) != -1) {
            var array = time.split(' ');
            var amount = array[5] / 30;
            var timespent = amount * 58;
            if (time.indexOf(mins) != -1) {
                    timespent = array[7] / .50 + timespent;
                    $('#compl').find('.travel').eq(TravelModeIndex).css("height", timespent);
            }
            else {
                $('#compl').find('.travel').eq(TravelModeIndex).css("height", timespent);
            }
        }
        else if (time.indexOf(hours) != -1) {
            var array = time.split(' ');
            var amount = array[5] / 30;
            var timespent = amount * 58;
            if (time.indexOf(mins) != -1) {
                    timespent = array[7] / .50 + timespent;
                    $('#compl').find('.travel').eq(TravelModeIndex).css("height", timespent);
            }
            else {
                $('#compl').find('.travel').eq(TravelModeIndex).css("height", timespent);
            }
        }
        else if (time.indexOf(mins) != -1) {
            var array = time.split(' ');
            if (array[5] > 20) {
                var amount = array[5] / 30;
                var timespent = amount * 58;
                $('#compl').find('.travel').eq(TravelModeIndex).css("height", timespent);
            }
        }
    }
}

/****************Save place details**********************************/

function Getplaces_details_forsave()
{
    var plcbx_hidvalue = "",prev_city_order="";
    var plcbx_hidvalue_split = [],cities_in_order=[];
    daynumber_incre = 0;
    var incr_val = 1;
    $("#CityOrderTable>tbody>tr").each(function () {
        var cityname_tr = $(this).find(".cus:first").html();
        cities_in_order.push({
            city_no: incr_val,
            city: cityname_tr
        });
        incr_val++;
    });    

    $(".place-box-right").each(function () {        
        plcbx_hidvalue = $(this).find('.plcbx_hidlatlng').val();
        plcbx_hidvalue_split = plcbx_hidvalue.split('||');
        eachcityname = $(this).data('value');
        daynumber_incre++;
        for (var city_incr = 0, CitiesLength = cities_in_order.length; city_incr < CitiesLength; city_incr++)
        {
            if(cities_in_order[city_incr].city.trim()==eachcityname.trim())
            {                
                dayorder = cities_in_order[city_incr].city_no;                
            }
        }
        city_lat = plcbx_hidvalue_split[0];
        city_lng = plcbx_hidvalue_split[1];
        allplace_des = "";
        allplace_rating = "";
        all_places_names = "", all_places_id = "", all_places_spendingtime = "";
        allplaces_addr = "";
        allplace_lat = "", allplace_lng = "", alltravel_mode = "", alltravel_time = "";
        allplace_starttime = "", is_apidata = "", manualplc_id = "", allplace_category = "", allplc_url = "";       

        var thisplc_bx_rit_id = $(this).attr('id');
        get_itinerary_plc_box = "plc_" + thisplc_bx_rit_id;
        
        $('#'+ get_itinerary_plc_box +'> div').each(function () {
                var classname_plc = $(this).attr('class').split(' ');
                if (classname_plc[0] == "place-box") {                    
                        if (all_places_names != "") {
                            all_places_names += "||";
                            all_places_id += "||";
                            all_places_spendingtime += "||";
                            allplaces_addr += "||";
                            allplc_url += "|image_split|";
                            allplace_lat += "||";
                            allplace_lng += "||";
                            alltravel_mode += "||";
                            alltravel_time += "||";
                            allplace_starttime += "||";
                            is_apidata += "||";
                            manualplc_id += "||";
                            allplace_category += "||";
                            allplace_des += "||";
                            allplace_rating += "|web_split|";
                        }
                    all_places_names += "pl_bx";
                    all_places_id += "pl_bx";
                    all_places_spendingtime += "pl_bx";
                    allplaces_addr += "pl_bx";
                    allplc_url += "pl_bx";
                    allplace_lat += "pl_bx";
                    allplace_lng += "pl_bx";
                    allplace_category += "pl_bx";
                    allplace_des += "pl_bx";
                    allplace_rating += "pl_bx";
                    if ($(this).next(".travel").length) {
                        alltravel_mode += $(this).next(".travel").find(".active").data('value');
                        var retrieve_traveltime = $(this).next(".travel").find("p").text().split("is");
                        if (retrieve_traveltime.length) {
                            alltravel_time += retrieve_traveltime[1];
                        }
                        else {
                            alltravel_time += "No traveltime";
                        }
                    }
                    else {
                        alltravel_mode += "No travelmode";
                        alltravel_time += "No traveltime";
                    }
                    var findpos = $(this).position();
                    allplace_starttime += findpos.top;
                    is_apidata += "Yes";
                    manualplc_id += "Not manual";
                }
                else if (classname_plc[0] == "place") {
                    if (all_places_names != "") {
                        all_places_names += "||";
                        all_places_id += "||";
                        all_places_spendingtime += "||";
                        allplaces_addr += "||";
                        allplc_url += "|image_split|";
                        allplace_lat += "||";
                        allplace_lng += "||";
                        alltravel_mode += "||";
                        alltravel_time += "||";
                        allplace_starttime += "||";
                        is_apidata += "||";
                        manualplc_id += "||";
                        allplace_category += "||";
                        allplace_des += "||";
                        allplace_rating += "|web_split|";
                    }
                    all_places_names += $(this).find(".place-details").find(".p-left").find("h4").text();
                    all_places_id += $(this).find(".place-details").attr('id');
                    var tmpPlcId = $(this).find(".place-details").attr('id');
                    all_places_spendingtime += $(this).find(".place-details").height();
                    allplaces_addr += $(this).find(".place-details").find(".p-left").find("p").text();
                    allplc_url += $(this).find(".place-img img").attr('src');
                    allplace_lat += $(this).find(".hidlat").val();
                    allplace_lng += $(this).find(".hidlng").val();
                    allplace_category += $(this).find(".place-details").find(".p-left").find("span:first").text();
                    var tmpPlcCat = $(this).find(".place-details").find(".p-left").find("span:first").text();
                    if ($(this).find(".frmApi").val() == "0")
                    {
                        is_apidata += "No";
                    }
                    else
                    {
                        is_apidata += "Yes";
                    }
                    if (tmpPlcCat == "Attraction.") {
                        for (var indxVal = 0, attrDetVal = attrDetails.length; indxVal < attrDetVal; indxVal++) {
                            if (attrDetails[indxVal].plc_id == tmpPlcId) {
                                //if (attrDetails[indxVal].details.formatted_phone_number) {
                                //    allplace_des += attrDetails[indxVal].details.formatted_phone_number;
                                //}
                                //else {
                                    allplace_des += "No Number";
                                //}
                                //if (attrDetails[indxVal].details.website) {
                                //    allplace_rating += attrDetails[indxVal].details.website;
                                //}
                                //else {
                                    allplace_rating += "No site";
                                //}
                                break;
                            }
                        }
                    }
                    else if (tmpPlcCat == "Restaurant.")
                    {
                        for (var indxVal1 = 0, restDetVal = restDetails.length; indxVal1 < restDetVal; indxVal1++) {
                            if (restDetails[indxVal1].plc_id == tmpPlcId) {
                                //if (restDetails[indxVal1].details.formatted_phone_number) {
                                //    allplace_des += restDetails[indxVal1].details.formatted_phone_number;
                                //}
                                //else {
                                    allplace_des += "No Number";
                                //}
                                //if (restDetails[indxVal1].details.website) {
                                //    allplace_rating += restDetails[indxVal1].details.website;
                                //}
                                //else {
                                    allplace_rating += "No site";
                                //}
                                break;
                            }
                        }
                    }
                    if ($(this).next(".travel").length) {
                        alltravel_mode += $(this).next(".travel").find(".active").data('value');
                        var retrieve_traveltime = $(this).next(".travel").find("p").text().split("is");
                        if (retrieve_traveltime.length) {
                            alltravel_time += retrieve_traveltime[1].trim();
                        }
                        else {
                            alltravel_time += " No traveltime";
                        }
                    }
                    else if ($(this).next(".custom_travel").length) {
                        alltravel_mode += "Cus~"+$(this).next(".custom_travel").find(".active").data('value');
                        var retrieve_traveltime = $(this).next(".custom_travel").find("p").text().split("is");
                        if (retrieve_traveltime.length) {
                            alltravel_time += retrieve_traveltime[1].trim();
                        }
                        else {
                            alltravel_time += "No traveltime";
                        }
                    }
                    else {
                        alltravel_mode += "No travelmode";
                        alltravel_time += "No traveltime";
                    }
                    var findpos = $(this).position();
                    allplace_starttime += findpos.top;
                    //is_apidata += "Yes";
                    manualplc_id += "Not manual";
                }
            });
        plan_createdbyuser = $("#lblusername").text();
        $.ajax({
            url: '../trip/AddColumn',
            type: "POST",
            async:false,
            data: { spot_id: all_places_id, spot_name: all_places_names, spotspending_time: all_places_spendingtime, eachcityname: eachcityname, day_number: daynumber_incre, spot_desc: allplace_des, spot_rating: allplace_rating, spot_address: allplaces_addr, spot_url: allplc_url, currentcity_latitude: city_lat, currentcity_longitude: city_lng, spot_lat: allplace_lat, spot_lng: allplace_lng, travel_mode: alltravel_mode, travel_duration: alltravel_time, day_order: dayorder, spot_starttime: allplace_starttime, isapidata: is_apidata, manualspot_id: manualplc_id, plan_createdby: plan_createdbyuser, spot_category: allplace_category },
            success: function (data) {

            }
        });
    });
    var updatedUser = $("#lblusername").text();
    $.getJSON('../trip/SavePlanDetails',{'UpdatedBy':updatedUser}, function (data) {
        if (ready_for_add_day == 1) {
            var newforaddday_array = foraddday_array.slice();
            setTimeout(function () { GetApiForRetrieved_cities(); }, 5000);
            setTimeout(function () { updatecityflags(); }, 4000);
            ready_for_add_day = 0;
        }
    });
}

function updatecityflags()
{
    $.getJSON('../trip/SavecitiesDetails', function (response) {        
        if(response.citieslist)
        {
            
        }              
    });
}

function swapcities_itinerary()
{
    $.getJSON('../trip/Update_DayOrder', {  }, function (data) {       
    });
}

function intheTripRHAA(onchangeCategory)
{
    //alert("neftintheTripRHAAs");
    //debugger
    inthetrip_array_hotels.length = 0;
    inthetrip_array_attractions.length = 0;
    inthetrip_array_activities.length = 0;
    inthetrip_array_restaurants.length = 0;
    inthetrip_array_custom.length = 0;
    inthetrip_acti.length = 0;
    inthetrip_hotel.length = 0;
    leftsidearray.length = 0;
    inthetrip_custom.length = 0;
    $(".place").each(function () {
        var plcIdis = $(this).find(".place-details").attr('id');
        if($(this).find(".place-details").find(".p-left").find("span:first").text().trim()=="Hotel.")
        {
            inthetrip_array_hotels.push(plcIdis);
            inthetrip_hotel.push(plcIdis);
        }
        else if ($(this).find(".place-details").find(".p-left").find("span:first").text().trim() == "Attraction.") {
            inthetrip_array_attractions.push(plcIdis);
            leftsidearray.push(plcIdis);
        }
        else if ($(this).find(".place-details").find(".p-left").find("span:first").text().trim() == "Activity.") {
            inthetrip_array_activities.push(plcIdis);
            inthetrip_acti.push(plcIdis);
        }
        else if ($(this).find(".place-details").find(".p-left").find("span:first").text().trim() == "Restaurant.") {
            inthetrip_array_restaurants.push(plcIdis);
            leftsidearray.push(plcIdis);
        }
        else if ($(this).find(".place-details").find(".p-left").find("span:first").text().trim() == "Custom place.") {
            inthetrip_array_custom.push(plcIdis);
            inthetrip_custom.push(plcIdis);
        }
    });
    
    if (onchangeCategory == "Hotel") {
        for (var loopArray = 0, HotelsInTrip = inthetrip_array_hotels.length; loopArray < HotelsInTrip; loopArray++)
        {
            $("#rhaa1 .resort-row").each(function () {
                if ("res_" + inthetrip_array_hotels[loopArray] == $(this).attr('id')) {
                    $(this).find('.add-trip').html("In the trip");
                    $(this).find('.book').parent().css('display', 'block');
                    $(this).switchClass("resort-row", "resort-row2");
                }
                else
                {
                    $(this).find('.add-trip').html("Add Trip");
                    $(this).find('.book').parent().css('display', 'none');
                    $(this).switchClass("resort-row2", "resort-row");
                }
            });
        }
    }
    if (onchangeCategory == "Restaurant") {
        for (var loopArray = 0, RestaurantsInTrip = inthetrip_array_restaurants.length; loopArray < RestaurantsInTrip; loopArray++) {
            $("#rhaa1 .resort-row").each(function () {
                if ("res_" + inthetrip_array_restaurants[loopArray] == $(this).attr('id')) {
                    $(this).find('.add-trip').html("In the trip");
                    $(this).switchClass("resort-row", "resort-row2");
                }
            });
        }
    }
    if (onchangeCategory == "Attractions") {
        for (var loopArray = 0, AttractionsInTrip = inthetrip_array_attractions.length; loopArray < AttractionsInTrip ; loopArray++) {           
            $("#rhaa1 .resort-row").each(function () {
                if ("res_" + inthetrip_array_attractions[loopArray] == $(this).attr('id')) {                    
                    $(this).find('.add-trip').html("In the trip");
                    $(this).switchClass("resort-row", "resort-row2");
                }
            });
        }
    }
    if (onchangeCategory == "Activities") {
        for (var loopArray = 0, ActivitiesInTrip = inthetrip_array_activities.length; loopArray < ActivitiesInTrip ; loopArray++) {
            $("#rhaa1 .resort-row").each(function () {
                if ("res_" + inthetrip_array_activities[loopArray] == $(this).attr('id')) {
                    $(this).find('.add-trip').html("In the trip");
                    $(this).switchClass("resort-row", "resort-row2");
                    $(this).find('.Actbook').parent().css('display', 'block');
                }
                else
                {
                    $(this).find('.add-trip').html("Add Trip");
                    $(this).switchClass("resort-row2", "resort-row");
                    $(this).find('.Actbook').parent().css('display', 'none');
                }
            });
        }
    }
    if (onchangeCategory == "Others") {
        for (var loopArray = 0, CustomPlcsInTrip = inthetrip_array_custom.length; loopArray < CustomPlcsInTrip ; loopArray++) {
            $("#divplace .resort-row").each(function () {
                if ("res_" + inthetrip_array_custom[loopArray] == $(this).attr('id')) {
                    $(this).find('.add-trip').html("In the trip");
                    $(this).switchClass("resort-row", "resort-row2");
                }
            });
        }
    }
    $('#RightSidePart').unblock();
}

/******************End of save place details***************************/