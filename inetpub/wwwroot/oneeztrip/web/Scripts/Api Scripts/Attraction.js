﻿function LoadRestaurantPref() {
    var HtmlData = '';
    $.getJSON('../trip/RetreivePreferrence', { Mode: 3 }, function (data) {
        $.each(data, function (indexValue, dataValue) {
            var id = dataValue.ID;
            var type = dataValue.Keyword;
            HtmlData += "<div class='rating-vote'>";
            HtmlData += "<input type='checkbox' class='cuisinecheck-box' id='chk" + dataValue.Name + "' name='cuisine' value='" + type + "' />";
            HtmlData += "<div><label for='chk" + dataValue.Name + "'>" + dataValue.Name + "</label></div>";
            HtmlData += "</div>";
        });
        $('#RestaurantPrefDiv').find('img').hide();
        $('#RestaurantPrefDiv').append(HtmlData);
        LoadAttractionPref();
    });
}

function LoadAttractionPref() {
    var HtmlData = '';
    $.getJSON('../trip/RetreivePreferrence', { Mode: 2 }, function (data) {
        $.each(data, function (indexValue, dataValue) {
            var id = dataValue.ID;
            var type = dataValue.Keyword;
            HtmlData += "<div class='rating-vote'>";
            HtmlData += "<input type='checkbox' class='attrcheck-box' id='chk" + dataValue.Name + "' name='Attraction' value='" + type + "' />";
            HtmlData += "<div><label for='chk" + dataValue.Name + "'>" + dataValue.Name + "</label></div>";
            HtmlData += "</div>";
        });
        $('#AttractionPrefDiv').find('img').hide();
        $('#AttractionPrefDiv').append(HtmlData);
    });
}

function info(id, lng, lat,types,att_or_res) {
    $('#infowindow').html("");
    //var flagVal = true;
    //for (var indxVal = 0, attrDetVal = attrDetails.length; indxVal < attrDetVal; indxVal++) {
    //    if (attrDetails[indxVal].plc_id == id) {
    //        showInfo(attrDetails[indxVal].details,types,att_or_res);
    //        flagVal = false;
    //        break;
    //    }
    //}
    //if (flagVal) {
        var map = new google.maps.Map(document.getElementById('map-canvas'), {
            center: new google.maps.LatLng(lat, lng),
            zoom: 15
        });
        var request = {
            placeId: id
        };
        var infowindow = new google.maps.InfoWindow();
        var service = new google.maps.places.PlacesService(map);
        service.getDetails(request, function (place, status) {
            if (status == google.maps.places.PlacesServiceStatus.OK) {
                showInfo(place, types,att_or_res);
            }
        });
    //}
}

function showInfo(place, types,att_or_res)
{
    if (place.photos) {
        var photo = place.photos[0].getUrl({ 'maxWidth': 180, 'maxHeight': 120 });
    }
    else {
        var photo = $('#divres_' + place.place_id).find('.resort-img').find('img').attr('src');
    }
    var Type = types;
    var HtmlData = "";   
    HtmlData = "<div class='info-left popup-bg' style='width: 94%;'>";
    HtmlData += "<figure class='PopUp-CloseBtn' onclick='$.fancybox.close();'><img src='../Content/images/del_travel-bkp-7-5-15.png'></figure>";
    HtmlData+="<figure class='resort-img'><img src='" + photo + "' alt='' /></figure>";
    HtmlData += "<h3>" + place.name + "</h3>";
    HtmlData += "<ul><b>Ratings : </b>";
    if (place.rating) {
        var rating = place.rating;
        if (att_or_res == '1') {
            for (var loopvar_rate = 1; loopvar_rate <= 5; loopvar_rate++) {
                if (rating >= loopvar_rate) {
                    HtmlData += "<li><img src='../Content/images/coffee_full.png' /></li>";
                }
                else if (rating > (loopvar_rate - 1) && rating < loopvar_rate) {
                    HtmlData += "<li><img src='../Content/images/coffee_half.png' /></li>";
                }
                else {
                    HtmlData += "<li><img src='../Content/images/coffee_grey_full.png' /></li>";
                }
            }
        }
        else if (att_or_res == '2') {
            for (var loopvar_rate = 1; loopvar_rate <= 5; loopvar_rate++) {
                if (rating >= loopvar_rate) {
                    HtmlData += "<li><img src='../Content/images/tower_full.png' /></li>";
                }
                else if (rating > (loopvar_rate - 1) && rating < loopvar_rate) {
                    HtmlData += "<li><img src='../Content/images/tower_half.png' /></li>";
                }
                else {
                    HtmlData += "<li><img src='../Content/images/tower_grey_full.png' /></li>";
                }
            }
        }
        HtmlData += "<li>(" + rating + "/5)</li></ul>";
    }
    else {
        var rating = 1;
        if (att_or_res == '1') {
            for (var loopvar_rate = 1; loopvar_rate <= 5; loopvar_rate++) {
                if (rating >= loopvar_rate) {
                    HtmlData += "<li><img src='../Content/images/coffee_full.png' /></li>";
                }
                else if (rating > (loopvar_rate - 1) && rating < loopvar_rate) {
                    HtmlData += "<li><img src='../Content/images/coffee_half.png' /></li>";
                }
                else {
                    HtmlData += "<li><img src='../Content/images/coffee_grey_full.png' /></li>";
                }
            }
        }
        else if (att_or_res == '2') {
            for (var loopvar_rate = 1; loopvar_rate <= 5; loopvar_rate++) {
                if (rating >= loopvar_rate) {
                    HtmlData += "<li><img src='../Content/images/tower_full.png' /></li>";
                }
                else if (rating > (loopvar_rate - 1) && rating < loopvar_rate) {
                    HtmlData += "<li><img src='../Content/images/tower_half.png' /></li>";
                }
                else {
                    HtmlData += "<li><img src='../Content/images/tower_grey_full.png' /></li>";
                }
            }
        }
        HtmlData += "<li>(" + rating + "/5)</li></ul>";
    }
    debugger;
    //alert(place.user_ratings_total);
    
    //added by Yuvapriya to re-enable number of reviews on 28Feb2017
    HtmlData += "<p style='line-height: 22px;'><b> Total No.of Reviews:</b> " + place.reviews.length + "</p> ";//place.user_ratings_total
    //Added by yuvapriya end 
    HtmlData += "<p style='line-height: 22px;'><b>Address : </b>" + place.vicinity + "</p>";
    if (place.website) {
        HtmlData += "<p style='line-height: 22px;'><b>Website : </b><a href='#' onclick='FunctionRedirect(\"" + place.website + "\");' style='text-decoration: underline;text-overflow: ellipsis;white-space: nowrap;overflow: hidden;'>" + place.website + "</a></p>";
    }
    if (place.international_phone_number)
    {
        HtmlData += "<p style='line-height: 22px;'><b>Phone Number : </b>" + place.international_phone_number + "</p>";
    }
    if (place.price_level) {
        if (place.price_level == 0) {
            HtmlData += "<p style='line-height: 22px;'><b> Price Level:</b> $</p>";
        }
        else if (place.price_level == 1) {
            HtmlData += "<p style='line-height: 22px;'><b> Price Level:</b> $</p>";
        }
        else if (place.price_level == 2) {
            HtmlData += "<p style='line-height: 22px;'><b> Price Level:</b>  $$</p>";
        }
        else if (place.price_level == 3) {
            HtmlData += "<p style='line-height: 22px;'><b> Price Level:</b>  $$$</p>";
        }
        else if (place.price_level == 4) {
            HtmlData += "<p style='line-height: 22px;'><b> Price Level:</b>  $$$$</p>";
        }
    }
    else {
        HtmlData += "<p style='line-height: 22px;'><b> Price Level:</b> Inexpensive</p>";
    }
    if (place.opening_hours) {
        HtmlData += "<p style='width:100%; clear:both;'><b> Opening Hour:</b></p>";
        HtmlData += "<li style='margin-left:2%;'>" + place.opening_hours.weekday_text[0] + "</li>";
        HtmlData += "<li style='margin-left:2%;'>" + place.opening_hours.weekday_text[1] + "</li>";
        HtmlData += "<li style='margin-left:2%;'>" + place.opening_hours.weekday_text[2] + "</li>";
        HtmlData += "<li style='margin-left:2%;'>" + place.opening_hours.weekday_text[3] + "</li>";
        HtmlData += "<li style='margin-left:2%;'>" + place.opening_hours.weekday_text[4] + "</li>";
        HtmlData += "<li style='margin-left:2%;'>" + place.opening_hours.weekday_text[5] + "</li>";
        if (place.opening_hours.weekday_text[6]) {
            HtmlData += "<li style='margin-left:2%;'>" + place.opening_hours.weekday_text[6] + "</li>";
        }
        else {
            HtmlData += "<li style='margin-left:2%;'>Sunday :Closed</li>";
        }
    }
    if (place.reviews) {
        HtmlData += "<div id='accordion'><p ><b>Guest Reviews:</b></p>";
        var count = place.reviews.length;
        for (var i = 0; i < count; i++) {
            var no = i + 1;
            HtmlData += "<p style='margin-left:2%;' class='accordion-toggle' id='accordion" + no + "' onclick='accordion(" + no + ")'><b> Reviews " + no + ":</b></p> ";
            if (i == 0) {
                HtmlData += "<div class='accordion-content default'>";

                HtmlData += "<li style='margin-left:4%;'><b>Rating </b>:" + place.reviews[i].rating + "</li>";
                if (place.reviews[i].text == "") {
                    HtmlData += "<li style='margin-left:4%;'><b>Comments</b> : No Comments</li> ";
                }
                else {
                    HtmlData += "<li style='margin-left:4%;'><b>Comments</b> :" + place.reviews[i].text + "</li>";
                }
                var Epoch = place.reviews[i].time * 1000;
                var myDate = new Date(Epoch);
                var InfoTimeSplit = myDate.toGMTString();
                InfoTimeSplit = InfoTimeSplit.split(' ');
                HtmlData += "<li style='margin-left:4%;'><b>Posted Date</b> :" + InfoTimeSplit[1] + " " + InfoTimeSplit[2] + " " + InfoTimeSplit[3] + "</li></div>";
            }
            else {
                HtmlData += "<div class='accordion-content'> ";
                HtmlData += "<li style='margin-left:4%;'><b>Rating </b>:" + place.reviews[i].rating + "</li> ";
                if (place.reviews[i].text == "") {
                    HtmlData += "<li style='margin-left:4%;'><b>Comments</b> : No Comments</li> ";
                }
                else {
                    HtmlData += "<li style='margin-left:4%;'><b>Comments</b> :" + place.reviews[i].text + "</li> ";
                }
                var Epoch = place.reviews[i].time * 1000;
                var myDate = new Date(Epoch);
                var InfoTimeSplit = myDate.toGMTString();
                InfoTimeSplit = InfoTimeSplit.split(' ');
                HtmlData += "<li style='margin-left:4%;'><b>Posted Date</b> :" + InfoTimeSplit[1] + " " + InfoTimeSplit[2] + " " + InfoTimeSplit[3] + "</li></div> ";
            }
        } HtmlData += "</div>";
    }
    HtmlData += "</div>";
    $('#infowindow').append(HtmlData);
}

function FunctionRedirect(Link) {
    window.open(Link);
}

function infomanual(Modenum, Id) {
    $('#infowindow').html("");
    $.getJSON('../trip/GetManualEntryDetails', { Mode: Modenum, sortby: Id }, function (data) {
        $.each(data, function (indexValue, dataValue) {
            if (Modenum == 3) {
                var HtmlData = "";               
                HtmlData += "<div class='info-left popup-bg'>";
                HtmlData += "<figure class='resort-img'><img src='../Content/images/RestaurantImage/" + dataValue.Photo + "' alt='' /></figure>";
                HtmlData += "<h3>" + dataValue.Name + "</h3>";
                HtmlData += "<ul><b>Ratings : </b>";
                var rating = dataValue.Rating;
                for (var loopvar_rate = 1; loopvar_rate <= 5; loopvar_rate++) {
                    if (rating >= loopvar_rate) {
                        HtmlData += "<li><img src='../Content/images/coffee_full.png' /></li>";
                    }
                    else if (rating > (loopvar_rate - 1) && rating < loopvar_rate) {
                        HtmlData += "<li><img src='../Content/images/coffee_half.png' /></li>";
                    }
                    else {
                        HtmlData += "<li><img src='../Content/images/coffee_grey_full.png' /></li>";
                    }
                }
                HtmlData += "<li>(" + rating + "/5)</li></ul>";
                HtmlData += "<p style='line-height: 22px;'><b>Address : </b>" + dataValue.Address + "</p>";
                if (dataValue.Price) {
                    if (dataValue.Price == 0) {
                        HtmlData += "<p style='line-height: 22px;'><b> Price Level:</b> $</p>";
                    }
                    else if (dataValue.Price == 1) {
                        HtmlData += "<p style='line-height: 22px;'><b> Price Level:</b> $</p>";
                    }
                    else if (dataValue.Price == 2) {
                        HtmlData += "<p style='line-height: 22px;'><b> Price Level:</b>  $$</p>";
                    }
                    else if (dataValue.Price == 3) {
                        HtmlData += "<p style='line-height: 22px;'><b> Price Level:</b>  $$$</p>";
                    }
                    else if (dataValue.Price == 4) {
                        HtmlData += "<p style='line-height: 22px;'><b> Price Level:</b>  $$$$</p>";
                    }
                }
                else {
                    HtmlData += "<p style='line-height: 22px;'><b> Price Level:</b> Inexpensive</p>";
                }
                //HtmlData += "<p style='color:cadetblue;'> Price Level:<span style='color:orange;'> " + dataValue.Price + "</span></p> ";
                //HtmlData += "<p style='color:cadetblue;'> Opening Time:<span style='color:orange;'> " + dataValue.Time + "</span></p> ";
                HtmlData += "</div>";
                $('#infowindow').append(HtmlData);
            }
            else if (Modenum == 2) {
                var HtmlData = "";
               
                HtmlData += "<div class='info-left popup-bg'>";
                HtmlData += "<figure class='resort-img'><img src='../Content/images/AttractionImage/" + dataValue.Photo + "' alt='' /></figure>";
                HtmlData += "<h3>" + dataValue.Name + "</h3>";
                HtmlData += "<ul><b>Ratings : </b>";
                var rating = dataValue.Rating;
                for (var loopvar_rate = 1; loopvar_rate <= 5; loopvar_rate++) {
                    if (rating >= loopvar_rate) {
                        HtmlData += "<li><img src='../Content/images/tower_full.png' /></li>";
                    }
                    else if (rating > (loopvar_rate - 1) && rating < loopvar_rate) {
                        HtmlData += "<li><img src='../Content/images/tower_half.png' /></li>";
                    }
                    else {
                        HtmlData += "<li><img src='../Content/images/tower_grey_full.png' /></li>";
                    }
                }
                HtmlData += "<li>(" + rating + "/5)</li></ul>";
                HtmlData += "<p style='line-height: 22px;'><b>Address : </b>" + dataValue.Street + "</p>";
                if (dataValue.Website) {
                    HtmlData += "<p style='line-height: 22px;'><b>Website : </b><a href='#' onclick='FunctionRedirect(\"" + dataValue.Website + "\");' style='text-decoration: underline;text-overflow: ellipsis;white-space: nowrap;overflow: hidden;'>" + dataValue.Website + "</a></p>";
                }
                if (dataValue.Price) {
                    if (dataValue.Price == 0) {
                        HtmlData += "<p style='line-height: 22px;'><b> Price Level:</b> $</p>";
                    }
                    else if (dataValue.Price == 1) {
                        HtmlData += "<p style='line-height: 22px;'><b> Price Level:</b> $</p>";
                    }
                    else if (dataValue.Price == 2) {
                        HtmlData += "<p style='line-height: 22px;'><b> Price Level:</b>  $$</p>";
                    }
                    else if (dataValue.Price == 3) {
                        HtmlData += "<p style='line-height: 22px;'><b> Price Level:</b>  $$$</p>";
                    }
                    else if (dataValue.Price == 4) {
                        HtmlData += "<p style='line-height: 22px;'><b> Price Level:</b>  $$$$</p>";
                    }
                }
                else {
                    HtmlData += "<p style='line-height: 22px;'><b> Price Level:</b> Inexpensive</p>";
                }
                //HtmlData += "<p style='color:cadetblue;'> Price Level:<span style='color:orange;'> " + dataValue.Price + "</span></p> ";
                HtmlData += "</div>";
                $('#infowindow').append(HtmlData);
            }
        });
    });
}

var restaurant = [];

function filterrestaurant(place, indexValue) {
    var count = place.rating;
    var value = Math.round(count);
    if (value == $('#dynamicresrating').val()) {
        if (jQuery.inArray(place.place_id, restaurant) !== -1) {
            var items = '';
            
            items += "<div id='rh" + place.place_id + "'></div><input type='hidden' class='hide'  id=" + place.place_id + "><aside class='resort-row' id='res_" + place.place_id + "'><div class='resort-row1' id='divres_" + place.place_id + "'>";
            items += "<figure class='resort-img'><img src='" + place.photos[0].getUrl({ 'maxWidth': 35, 'maxHeight': 35 }) + "'/>";
            items += "</figure>";
            items += "<div class='resort-details'><div class='r-left'>";
            items += "<h3 id='h" + place.place_id + "'>" + place.name + "  </h3><br/>";
            items += "<p style='height:40px; overflow-y:hidden;' id='p" + place.place_id + "'>" + place.vicinity + "</p>";
            items += "<div class='book-add'><span><a href='#' id='inner" + place.place_id + "' class='add-trip' onclick='return RestaurantToleft(\"" + place.place_id + "\");'>In The Trip</a></span></div></div> ";
            items += "<div class='r-right'>";
            items += " <ul><li><a href='#'><img src='../Content/images/del-icon.png' alt='' class='del-show' /></a></li>";
            items += "<li><a  class='fancybox' href='#infowindow' onclick='return info(\"" + place.place_id + "\",\"" + place.geometry.location.D + "\",\"" + place.geometry.location.k + "\");'><img src='../Content/images/info-icon.png' alt='' class='example1demo' /></a></li></ul>";
            items += "</div></div></div>";
            items += "</aside>";
            $('#rhaa1').append(items);
        }
        else {
          
            var items = '';
            items += "<div id='rh" + place.place_id + "'></div><input type='hidden' class='hide'  id=" + place.place_id + "><aside class='resort-row' id='res_" + place.place_id + "'><div class='resort-row1' id='divres_" + place.place_id + "'>";
            items += "<figure class='resort-img'><img src='../Content/images/AppImages/small-img4.jpg' alt='' />";
            items += "<span>Rating <strong id='s" + place.place_id + "'>" + place.rating + "</strong></span>";
            items += "</figure>";
            items += "<div class='resort-details'><div class='r-left'>";
            items += "<h3 id='h" + place.place_id + "'>" + place.name + "  </h3><br/>";
            items += "<p style='height:40px; overflow-y:hidden;' id='p" + place.place_id + "'>" + place.vicinity + "</p>";
            items += "<div class='book-add'><span><a href='#' id='inner" + place.place_id + "' class='add-trip' onclick='return RestaurantToleft(\"" + place.place_id + "\");'>Add Trip</a></span></div></div> ";
            items += "<div class='r-right'>";
            items += " <ul><li><a href='#'><img src='../Content/images/del-icon.png' alt='' class='del-show' /></a></li>";
            items += "<li><a  class='fancybox' href='#infowindow' onclick='return info(\"" + place.place_id + "\",\"" + place.geometry.location.D + "\",\"" + place.geometry.location.k + "\");'><img src='../Content/images/info-icon.png' alt='' class='example1demo' /></a></li></ul>";
            items += "</div></div></div>";
            items += "</aside>";
            $('#rhaa1').append(items);
        }
    }
}

function infoattraction(id, lng, lat,att_or_res) {
    $('#infowindow').html("");
    var map = new google.maps.Map(document.getElementById('map-canvas'), {
        center: new google.maps.LatLng(lat, lng),
        zoom: 15
    });
    var request = {
        placeId: id
    };
    var infowindow = new google.maps.InfoWindow();
    var service = new google.maps.places.PlacesService(map);
    service.getDetails(request, function (place, status) {
        if (status == google.maps.places.PlacesServiceStatus.OK) {
            if (place.photos) {
                var photo = place.photos[0].getUrl({ 'maxWidth': 182, 'maxHeight': 120 });
            }
            else {
                var photo = "../Content/images/AppImages/small-img4.jpg";
            }
            var HtmlData = "";
           
            HtmlData = "<div class='info-left infoPopUp popup-bg' style='width: 94%;'>";
            HtmlData += "<figure class='PopUp-CloseBtn' onclick='$.fancybox.close();'><img src='../Content/images/del_travel-bkp-7-5-15.png'></figure>";
            //HtmlData += "<a class='close-fancyboxButton' onclick='$.fancybox.close()' title='Close'><img src='../content/images/del_travel-bkp-7-5-15.png' style='padding-left: 98%;'/></a>";
            HtmlData +="<figure class='resort-img'><img src='" + photo + "' alt='' /></figure>";
            HtmlData += "<h3>" + place.name + "</h3>";
            HtmlData += "<ul><b>Ratings : </b>";
            if (place.rating) {
                var rating = place.rating;
                if (att_or_res == '1') {
                    for (var loopvar_rate = 1; loopvar_rate <= 5; loopvar_rate++) {
                        if (rating >= loopvar_rate) {
                            HtmlData += "<li><img src='../Content/images/coffee_full.png' /></li>";
                        }
                        else if (rating > (loopvar_rate - 1) && rating < loopvar_rate) {
                            HtmlData += "<li><img src='../Content/images/coffee_half.png' /></li>";
                        }
                        else {
                            HtmlData += "<li><img src='../Content/images/coffee_grey_full.png' /></li>";
                        }
                    }
                }
                else if (att_or_res == '2') {
                    for (var loopvar_rate = 1; loopvar_rate <= 5; loopvar_rate++) {
                        if (rating >= loopvar_rate) {
                            HtmlData += "<li><img src='../Content/images/tower_full.png' /></li>";
                        }
                        else if (rating > (loopvar_rate - 1) && rating < loopvar_rate) {
                            HtmlData += "<li><img src='../Content/images/tower_half.png' /></li>";
                        }
                        else {
                            HtmlData += "<li><img src='../Content/images/tower_grey_full.png' /></li>";
                        }
                    }
                }
                HtmlData += "<li>(" + rating + "/5)</li></ul>";
            }
            else {
                var rating = 1;
                if (att_or_res == '1') {
                    for (var loopvar_rate = 1; loopvar_rate <= 5; loopvar_rate++) {
                        if (rating >= loopvar_rate) {
                            HtmlData += "<li><img src='../Content/images/coffee_full.png' /></li>";
                        }
                        else if (rating > (loopvar_rate - 1) && rating < loopvar_rate) {
                            HtmlData += "<li><img src='../Content/images/coffee_half.png' /></li>";
                        }
                        else {
                            HtmlData += "<li><img src='../Content/images/coffee_grey_full.png' /></li>";
                        }
                    }
                }
                else if (att_or_res == '2') {
                    for (var loopvar_rate = 1; loopvar_rate <= 5; loopvar_rate++) {
                        if (rating >= loopvar_rate) {
                            HtmlData += "<li><img src='../Content/images/tower_full.png' /></li>";
                        }
                        else if (rating > (loopvar_rate - 1) && rating < loopvar_rate) {
                            HtmlData += "<li><img src='../Content/images/tower_half.png' /></li>";
                        }
                        else {
                            HtmlData += "<li><img src='../Content/images/tower_grey_full.png' /></li>";
                        }
                    }
                }
                HtmlData += "<li>(" + rating + "/5)</li></ul>";
            }
            //Added by Yuvapriya on 28Feb2017 to re-enable number of reviews p place.user_ratings_total
            HtmlData += "<p style='line-height: 22px;'><b> Total No.of Reviews:</b> " + place.reviews.length + "</p>";
            //End
            HtmlData += "<p style='line-height: 23px;'><b>Address : </b>" + place.vicinity + "</p>";
            HtmlData += "<p style='line-height: 23px;'><b>Types : </b></p>";
            if (place.types)
            {
                for(var indx=0;indx<place.types.length;indx++)
                {
                    HtmlData += "<p style='line-height: 23px;'><b></b>" + place.types[indx] + "</p>";
                }
            }
            if(place.website)
            {
                HtmlData += "<p style='line-height: 33px;'><b>Website : </b><a href='#' onclick='FunctionRedirect(\"" + place.website + "\");' style='text-decoration: underline;text-overflow: ellipsis;white-space: nowrap;overflow: hidden;'>" + place.website + "</a></p>";
            }
            if (place.international_phone_number)
            {
                HtmlData += "<p style='line-height: 27px;'><b>Phone Number : </b>" + place.international_phone_number + "</p>";
            }            
            if (place.price_level) {
                if (place.price_level == 0) {
                    HtmlData += "<p style='line-height: 22px;'><b> Price Level:</b> Free</p>";
                }
                else if (place.price_level == 1) {
                    HtmlData += "<p style='line-height: 22px;'><b> Price Level:</b> Inexpensive</p>";
                }
                else if (place.price_level == 2) {
                    HtmlData += "<p style='line-height: 22px;'><b> Price Level:</b>  Moderate</p>";
                }
                else if (place.price_level == 3) {
                    HtmlData += "<p style='line-height: 22px;'><b> Price Level:</b>  Expensive</p>";
                }
                else if (place.price_level == 4) {
                    HtmlData += "<p style='line-height: 22px;'><b> Price Level:</b>  Very Expensive</p>";
                }
            }
            else {
                HtmlData += "<p style='line-height: 22px;'><b> Price Level:</b> Inexpensive</p>";
            }
            if (place.opening_hours) {
                HtmlData += "<p><b> Opening Hour:</b></p>";
                HtmlData += "<li style='margin-left:2%;'>" + place.opening_hours.weekday_text[0] + "</li>";
                HtmlData += "<li style='margin-left:2%;'>" + place.opening_hours.weekday_text[1] + "</li>";
                HtmlData += "<li style='margin-left:2%;'>" + place.opening_hours.weekday_text[2] + "</li>";
                HtmlData += "<li style='margin-left:2%;'>" + place.opening_hours.weekday_text[3] + "</li>";
                HtmlData += "<li style='margin-left:2%;'>" + place.opening_hours.weekday_text[4] + "</li>";
                HtmlData += "<li style='margin-left:2%;'>" + place.opening_hours.weekday_text[5] + "</li>";
                if (place.opening_hours.weekday_text[6])
                {
                    HtmlData += "<li style='margin-left:2%;'>" + place.opening_hours.weekday_text[6] + "</li>";
                }
                else
                {
                    HtmlData += "<li style='margin-left:2%;'>Sunday :Closed</li>";
                }
            }
            if (place.reviews) {
                var count = place.reviews.length;
                HtmlData += "<div id='accordion'><p><b>Guest Reviews:</b></p>";
                for (var ReviewCount = 0; ReviewCount < count; ReviewCount++) {
                    var no = ReviewCount + 1;
                    HtmlData += "<p style='margin-left:2%;' class='accordion-toggle' id='accordion" + no + "' onclick='accordion(" + no + ")'><b> Reviews " + no + ":</b></p>";
                    if (ReviewCount == 0) {
                        HtmlData += "<div class='accordion-content default'>";
                        HtmlData += "<li style='margin-left:4%;'><b>Rating </b>:" + place.reviews[ReviewCount].rating + "</li>";
                        if (place.reviews[ReviewCount].text == "") {
                            HtmlData += "<li style='margin-left:4%;'><b>Comments</b> : No Comments</li>";
                        }
                        else {
                            HtmlData += "<li style='margin-left:4%;'><b>Comments</b> :" + place.reviews[ReviewCount].text + "</li>";
                        }
                        var Epoch = place.reviews[ReviewCount].time * 1000;
                        var myDate = new Date(Epoch);
                        var InfoTimeSplit = myDate.toGMTString();
                        InfoTimeSplit = InfoTimeSplit.split(' ');
                        HtmlData += "<li style='margin-left:4%;'><b>Posted Date</b> :" + InfoTimeSplit[1] + " " + InfoTimeSplit[2] + " " + InfoTimeSplit[3] + "</li></div> ";
                    }
                    else {
                        HtmlData += "<div class='accordion-content'>";
                        HtmlData += "<li style='margin-left:4%;'><b>Rating </b>:" + place.reviews[ReviewCount].rating + "</li>";
                        if (place.reviews[ReviewCount].text == "") {
                            HtmlData += "<li style='margin-left:4%;'><b>Comments</b> : No Comments</li> ";
                        }
                        else {
                            HtmlData += "<li style='margin-left:4%;'><b>Comments</b> :" + place.reviews[ReviewCount].text + "</li>";
                        }
                        var Epoch = place.reviews[ReviewCount].time * 1000;
                        var myDate = new Date(Epoch);
                        var InfoTimeSplit = myDate.toGMTString();
                        InfoTimeSplit = InfoTimeSplit.split(' ');
                        HtmlData += "<li style='margin-left:4%;'><b>Posted Date</b> :" + InfoTimeSplit[1] + " " + InfoTimeSplit[2] + " " + InfoTimeSplit[3] + "</li></div>";
                    }
                } HtmlData += "</div>";
            }
            HtmlData += "</div>";
            $('#infowindow').append(HtmlData);
        }
    });
}

function accordion(id) {
    $('#accordion' + id).next().slideToggle('fast');
    $(".accordion-content").not($('#accordion' + id)).prev().css("background-image", "url(../Content/images/up.png)");
    $(".accordion-content").not($('#accordion' + id).next()).slideUp('fast');    
    $('#accordion' + id).css("background-image", "url(../Content/images/down.png) !important");
}

function getcity(id, citylat, citylng, days) {
    var test = $('#city_' + id).html();
    $('#selectedcity').html();
    $('#selectedcity').html(test);
    $('#latarray').val(citylat);
    $('#lngarray').val(citylng);
    $('#orderarray').val(id);
    $('#orderofday').val(days);
    $('#rhaa1').html("");
    $('#rhaa1').text('loading...');
    $('#info').html("");
    var TriggerElement = $('#classchangeid').find('.last').attr('id');
    PreferenceType = $("#prefer").html();
    $('#' + TriggerElement).trigger('click');
    return false;
}

var attraction = [];
var travemode = "";

function deletediv(obj, uni, id) {
    $('#place' + obj).remove();
    if (id == 1) {
        var index = attraction.indexOf(obj);
        attraction.splice(index, 1);
        $('#inner' + uni).html("Add Trip");
    }
    else if(id==2) {
        var index = restaurant.indexOf(obj);
        restaurant.splice(index, 1);
        $('#inner' + uni).html("Add Trip");
    }
    else if(id==3)
    {
        var isprsnt=0;
        $('#compl').find('.place').each(function () {
            if ($(this).find('.p-left').find('span:first').html() == "Custom place." && $(this).id == 'place'+obj) {
                isprsnt++;
            }
        });
        if (isprsnt > 0) {
            $('#inner' + uni).html("Add Trip");
        }
    }
}

function LoadTravelsleft(ori, des) {
    var origin = ori,
   destination = des,
   dservice = new google.maps.DistanceMatrixService();
    dservice.getDistanceMatrix(
         {
             origins: [origin],
             destinations: [destination],
             travelMode: google.maps.TravelMode.DRIVING,
             avoidHighways: false,
             avoidTolls: false
         }, callbacktmleft);
}

function callbacktmleft(response, status) {
    var dest = "", orig = "", dist = "", dura = "";
    if (status === "OK") {
        dest = response.destinationAddresses[0];
        orig = response.originAddresses[0];
        dist = response.rows[0].elements[0].distance.text;
        dura = response.rows[0].elements[0].duration.text;
        $('#' + travelmodeid).find("p").text("Time taken by car is " + dura);
        $('#' + travelmodeid).find("p").append("<img src='../Content/images/edit.png' alt='' class='editimg' style='height: 15px;width: 15px;'>");
    }
    else {      
    }   
}

function hotelstoleft(id) {
    var cityid = '"' + id + '"';
    var Name = $('#h' + id).html();
    var Description = $('#pho_' + id).html();
    var rating = $('#s' + id).html();
    var count = $('#orderarray').val();
    var testl = $('#ccol_' + count + '0').find('.place-box-right').attr("id");
    var test2 = $('#' + testl).find('.place-box').attr('id');
    if ($(".place").length) {
        var originname = $(".place:last").find(".place-details").find(".p-left").find("h4").text();
        var orgid = $(".place:last").find(".place-details").attr('id');
        var trm_id = "tm_" + orgid;
        travelmodeid = trm_id;
        var colid = $('#col_' + testl).find('.place-box').attr('id');
        $('#' + test2).append($('<div class="travel" id="' + trm_id + '"><a href="#" style="float:right;"><img src="../Content/images/del_travel.png" class="travedelimg"/></a><p><img src="../Content/images/edit.png" alt="" class="editimg" style="height: 15px;width: 15px;"></p><ul><li><a href="#" data-value="car" class="active"><img src="../Content/images/car-icon.png" alt=""/></a></li><li><a href="#" data-value="train"><img src="../Content/images/train-icon.png" alt="" /></a></li><li><a href="#" data-value="plane"><img src="../Content/images/plane-icon.png" alt="" /></a></li><li><a href="#" data-value="walk"><img src="../Content/images/walk-icon.png" alt="" /></a></li><li class="last"><a href="#" data-value="bicycle"><img src="../Content/images/scooter-icon.png" alt="" /></a></li></ul></div>'));
        travemode = $('#' + trm_id).find('ul li .active').data('value');
    }
    LoadTravelsleft(originname, Name);
    var image = $('#divres_' + id).find('.resort-img').find('img').attr("src");
    var test = $('#ccol_' + count + '0').find('.place-box-right').attr("id");
    var htmldata = "<div id='place_" + id + "' class='place'><div id='place' class='place' style='margin-top:10px;'><input type='hidden'/>";

    
    htmldata += "<div class='place-details' id='" + id + "' style='height:86px;'>";
    var obj = new Image();
    obj.src = image;

    if (obj.complete) {
        htmldata += "<figure class='place-img'><img src='" + image + "' alt='' /></figure>";
    } else {
        htmldata += "<figure class='place-img'><img src='../Content/images/HotelImages/af5e-hotel.jpg' alt='' /></figure>";
        //alert('doesnt work');
    }

   
    htmldata += "<div class='place-cont'><div class='p-left'><h4>" + Name + "</h4><p style='height:26px;overflow-y:hidden;'>" + Description + " .</p>";
    htmldata += "<span>Price:$" + rating + " <span></span> <br /><span>Hotel</span></span></div>";
    htmldata += "<div class='p-right'><ul>";
    htmldata += "<li><a href='#'><img src='../Content/images/del-icon.png' alt='' onclick='deletediv(" + cityid + "," + cityid + ",3)' /></a></li>";
    htmldata += "<li><a href='#info_" + id + "' class='fancybox'><img src='../Content/images/info-icon.png' alt='' /></a></li>";
    htmldata += "</ul></div></div></div>";
    htmldata += "</div></div>";
    $('#' + test2).append(htmldata);
    var dusort = $('#sorthelp').val();
    if (dusort == 0) {
        $('#sorthelp').val(id);
    }
    else {
        $('#sorthelp').val(dusort + "_" + id);
    }
    onresizingplc();
    $("#res_" + id).switchClass("resort-row", "resort-row2");
    $('#inner' + id).html("In the trip");
    return false;
}

function LoadImageStart() {
    $("body").removeClass("unload");
    $("body").addClass("load");
}

function LoadImageEnd() {
    $("body").removeClass("load");
    $("body").addClass("unload");
}

function SortRestaurant(Expression, att_or_res) {
    $('#RightSidePart').unblock();
    var Resselectsort = Expression;
    var restaurant_forsort = [];
    var Count = $('#rhaa1').find('aside').length;
    var rhaatype = "",srtNew=0;
    if (att_or_res == '1')
    {
        //if (RstSrtArr.length == 0 && Expression != prevRstSrt && prevSugCity != $('#selectedcity').html())
        //{
        //    srtNew = 1;
        //}
        rhaatype = "Restaurant";
    }
    else if (att_or_res == '2')
    {
        //if (AttrSrtArr.length == 0 && Expression != prevAttrSrt && prevSugCity != $('#selectedcity').html()) {
        //    srtNew = 1;
        //}
        rhaatype="Attractions";
    }
    //if (srtNew == 1) {
        var tmpArr1 = [], tmpArr2 = [], tmpArr3 = [], tmpArr4 = [], tmpArr5 = [];
        for (var RestaurantsCount = 0; RestaurantsCount < Count; RestaurantsCount++) {
            var id = $('#rhaa1').find('.hide').eq(RestaurantsCount).attr('id');
            var Roundof = $('#ratefilter_' + id).val();
            var Rating = $('#rates_' + id).val();
            var lat = $('#latarray' + id).val();
            var lng = $('#lngarray' + id).val();
            var isManual = $('#isME' + id).val();
            var Name = $('#h' + id).html();
            var Address = $('#p' + id).html();
            var Photo = $('#divres_' + id).find('.resort-img').find('img').attr('src');
            var Trip = $('#inner' + id).html();
            var types = $('#' + id).val();
            var no_of_reviews = $('#divres_' + id).find('.reviews').val();
            var priceLev = $('#divres_' + id).find('.priceLev').val();
            var websiteLink = $('#divres_' + id).find('.webhidden').val();
            var chckxsts = true;
            for (var eachElem = 0; eachElem < restaurant_forsort.length; eachElem++) {
                if (id == restaurant_forsort[eachElem].plc_id) {
                    chckxsts = false;
                    break;
                }
            }
            if (chckxsts) {
                restaurant_forsort.push({
                    plc_name: Name,
                    plc_id: id,
                    plc_rating: Rating,
                    plc_Roundof: Roundof,
                    plc_lat: lat,
                    plc_lng: lng,
                    plc_address: Address,
                    plc_photo: Photo,
                    plc_type: types,
                    plc_trip: Trip,
                    plc_reviews: no_of_reviews,
                    plc_price: priceLev,
                    plc_web: websiteLink,
                    plc_manual: isManual
                });
                if (priceLev == 0) {
                    tmpArr1.push({
                        plc_name: Name,
                        plc_id: id,
                        plc_rating: Rating,
                        plc_Roundof: Roundof,
                        plc_lat: lat,
                        plc_lng: lng,
                        plc_address: Address,
                        plc_photo: Photo,
                        plc_type: types,
                        plc_trip: Trip,
                        plc_reviews: no_of_reviews,
                        plc_price: priceLev,
                        plc_web: websiteLink,
                        plc_manual: isManual
                    });
                }
                else if (priceLev == 1) {
                    tmpArr2.push({
                        plc_name: Name,
                        plc_id: id,
                        plc_rating: Rating,
                        plc_Roundof: Roundof,
                        plc_lat: lat,
                        plc_lng: lng,
                        plc_address: Address,
                        plc_photo: Photo,
                        plc_type: types,
                        plc_trip: Trip,
                        plc_reviews: no_of_reviews,
                        plc_price: priceLev,
                        plc_web: websiteLink,
                        plc_manual: isManual
                    });
                }
                else if (priceLev == 2) {
                    tmpArr3.push({
                        plc_name: Name,
                        plc_id: id,
                        plc_rating: Rating,
                        plc_Roundof: Roundof,
                        plc_lat: lat,
                        plc_lng: lng,
                        plc_address: Address,
                        plc_photo: Photo,
                        plc_type: types,
                        plc_trip: Trip,
                        plc_reviews: no_of_reviews,
                        plc_price: priceLev,
                        plc_web: websiteLink,
                        plc_manual: isManual
                    });
                }
                else if (priceLev == 3) {
                    tmpArr4.push({
                        plc_name: Name,
                        plc_id: id,
                        plc_rating: Rating,
                        plc_Roundof: Roundof,
                        plc_lat: lat,
                        plc_lng: lng,
                        plc_address: Address,
                        plc_photo: Photo,
                        plc_type: types,
                        plc_trip: Trip,
                        plc_reviews: no_of_reviews,
                        plc_price: priceLev,
                        plc_web: websiteLink,
                        plc_manual: isManual
                    });
                }
                else {
                    tmpArr5.push({
                        plc_name: Name,
                        plc_id: id,
                        plc_rating: Rating,
                        plc_Roundof: Roundof,
                        plc_lat: lat,
                        plc_lng: lng,
                        plc_address: Address,
                        plc_photo: Photo,
                        plc_type: types,
                        plc_trip: Trip,
                        plc_reviews: no_of_reviews,
                        plc_price: priceLev,
                        plc_web: websiteLink,
                        plc_manual: isManual
                    });
                }
            }
        }

        if (Resselectsort == 'asc' || Resselectsort == 'desc') {
            if (Resselectsort == 'asc') {
                $('#rhaa1').html(" ");
                restaurant_forsort.sort(function (obj1, obj2) {
                    var nameA = obj1.plc_name.toLowerCase(),
                    nameB = obj2.plc_name.toLowerCase();
                    if (nameA < nameB)
                        return -1
                    if (nameA > nameB)
                        return 1
                    return 0
                });
            }
            else {
                $('#rhaa1').html(" ");
                restaurant_forsort.sort(function (obj1, obj2) {
                    var nameA = obj1.plc_name.toLowerCase(),
                    nameB = obj2.plc_name.toLowerCase()
                    if (nameA < nameB)
                        return 1
                    if (nameA > nameB)
                        return -1
                    return 0
                });
            }
        }
        else if (Resselectsort == 'high' || Resselectsort == 'low') {
            if (Resselectsort == 'low') {
                $('#rhaa1').html(" ");
                restaurant_forsort.sort(function (obj1, obj2) {
                    return obj1.plc_rating - obj2.plc_rating
                });
            }
            else {
                $('#rhaa1').html(" ");
                restaurant_forsort.sort(function (obj1, obj2) {
                    return obj2.plc_rating - obj1.plc_rating
                });
            }
        }
        else if (Resselectsort == 'popular' || Resselectsort == 'popularLow') {
            $('#rhaa1').html(" ");
            if (Resselectsort == 'popular') {
                restaurant_forsort.sort(function (obj1, obj2) {
                    return obj2.plc_reviews - obj1.plc_reviews
                });
            }
            else {
                restaurant_forsort.sort(function (obj1, obj2) {
                    return obj1.plc_reviews - obj2.plc_reviews
                });
            }
        }
        else if (Resselectsort == 'priceLow') {
            $('#rhaa1').html(" ");
            restaurant_forsort.length = 0;
            restaurant_forsort = tmpArr1.concat(tmpArr2);
            restaurant_forsort = restaurant_forsort.concat(tmpArr3);
            restaurant_forsort = restaurant_forsort.concat(tmpArr4);
            restaurant_forsort = restaurant_forsort.concat(tmpArr5);
        }
        else if (Resselectsort == 'priceHigh') {
            $('#rhaa1').html(" ");
            restaurant_forsort.length = 0;
            restaurant_forsort = tmpArr5.concat(tmpArr4);
            restaurant_forsort = restaurant_forsort.concat(tmpArr3);
            restaurant_forsort = restaurant_forsort.concat(tmpArr2);
            restaurant_forsort = restaurant_forsort.concat(tmpArr1);
        }
        for (var position = 0, RestaurantsLength = restaurant_forsort.length; position < RestaurantsLength; position++) {
            var id = restaurant_forsort[position].plc_id;
            var Roundof = restaurant_forsort[position].plc_Roundof;
            var Rating = restaurant_forsort[position].plc_rating;
            var lat = restaurant_forsort[position].plc_lat;
            var lng = restaurant_forsort[position].plc_lng;
            var Name = restaurant_forsort[position].plc_name;
            var Address = restaurant_forsort[position].plc_address;
            var Photo = restaurant_forsort[position].plc_photo;
            var Trip = restaurant_forsort[position].plc_trip;
            var types = restaurant_forsort[position].plc_type;
            var reviews = restaurant_forsort[position].plc_reviews;
            var pricelevel = restaurant_forsort[position].plc_price;
            var weblink = restaurant_forsort[position].plc_web;
            var manentry = restaurant_forsort[position].plc_manual;
            var items = '';
           //***Modified by Yuvapriya on 04 Oct 2016 to load default image instead of broken image
            if (Photo.indexOf('wUKdXbzXBPw3YpWO9BIF1pj9EgywJZeYQCLIB') != -1) {
                if (att_or_res == '1') {
                    Photo = "../Content/images/RestaurantImage/DefaultRestaurantImage.png";
                }
                else {
                    Photo = "../Content/images/AttractionImage/DefaultAttractionImage.jpg";
                }                
            }
            //***End         
            items += "<div id='rh" + id + "'></div><input type='hidden' class='hide'  id=" + id + " value='" + types + "'><input type='hidden' id='rates_" + id + "' value='" + Rating + "'><input type='hidden' id='ratefilter_" + id + "' value='" + Roundof + "'><input type='hidden' id='latarray" + id + "' value='" + lat + "'><input type='hidden' id='lngarray" + id + "' value='" + lng + "'><aside class='resort-row' id='res_" + id + "'><div class='resort-row1' id='divres_" + id + "'>";
            items += "<input type='hidden' class='priceLev' value='" + pricelevel + "'/>";
            items += "<input type='hidden' class='webhidden' value='" + weblink + "'/>";
            items += "<input type='hidden' id='latarray" + id + "' value='" + lat + "' class='reslat'><input type='hidden' id='lngarray" + id + "' value='" + lng + "' class='reslng'>";
            items += "<input type='hidden' id='isME" + id + "' value='" + manentry + "' class='resME'>";
            items += "<figure class='resort-img'><div class='tooltip'>Drag to add</div><img src='" + Photo + "' alt='' id='img_" + id + "' />";
            items += "</figure>";
            items += "<div class='resort-details'><div class='r-left'>";
            items += "<h3 id='h" + id + "'>" + Name + "  </h3><br/>";
            items += "<p style='height:40px; overflow-y:hidden;' id='p" + id + "'>" + Address + "</p>";
            if (weblink != "No site") {
                items += "<p style='height:18px; overflow-y:hidden;width:100%;text-overflow: ellipsis;white-space: nowrap;overflow: hidden;' id='web" + id + "'><b>Website : </b><a target='_blank' href='" + weblink + "' style='text-decoration: underline;'>" + weblink + "</a></p>";
            }
            items += "<ul id='rat" + id + "'><b>Ratings : </b>";
            if (att_or_res == '1') {
                for (var loopvar_rate = 1; loopvar_rate <= 5; loopvar_rate++) {
                    if (Rating >= loopvar_rate) {
                        items += "<li><img src='../Content/images/coffee_full.png' /></li>";
                    }
                    else if (Rating > (loopvar_rate - 1) && Rating < loopvar_rate) {
                        items += "<li><img src='../Content/images/coffee_half.png' /></li>";
                    }
                    else {
                        items += "<li><img src='../Content/images/coffee_grey_full.png' /></li>";
                    }
                }
            }
            else {
                for (var loopvar_rate = 1; loopvar_rate <= 5; loopvar_rate++) {
                    if (Rating >= loopvar_rate) {
                        items += "<li><img src='../Content/images/tower_full.png' /></li>";
                    }
                    else if (Rating > (loopvar_rate - 1) && Rating < loopvar_rate) {
                        items += "<li><img src='../Content/images/tower_half.png' /></li>";
                    }
                    else {
                        items += "<li><img src='../Content/images/tower_grey_full.png' /></li>";
                    }
                }
            }
            if (reviews != "No rev") {
                //items += "<li>(" + reviews + " reviews)</li>";
            }
            items += "</ul>";
            if (pricelevel == 0) {
                items += "<p style='height: 18px;' id='price" + id + "'><b> Price Level:</b> $</p> ";
            }
            else if (pricelevel == 1) {
                items += "<p style='height: 18px;' id='price" + id + "'><b> Price Level:</b> $</p> ";
            }
            else if (pricelevel == 2) {
                items += "<p style='height: 18px;' id='price" + id + "'><b> Price Level:</b>  $$</p> ";
            }
            else if (pricelevel == 3) {
                items += "<p style='height: 18px;' id='price" + id + "'><b> Price Level:</b>  $$$</p> ";
            }
            else if (pricelevel == 4) {
                items += "<p style='height: 18px;' id='price" + id + "'><b> Price Level:</b>  $$$$</p> ";
            }
            if (att_or_res == '1') {
                items += "<div class='book-add'><span><a href='#' id='inner" + id + "' class='add-trip' onclick='return RestaurantToleft(\"" + id + "\");'>" + Trip + "</a></span></div>";
            }
            else if (att_or_res == '2') {
                items += "<div class='book-add'><span><a href='#' id='inner" + id + "' class='add-trip' onclick='return AttractionToleft(\"" + id + "\");'>" + Trip + "</a></span></div>";
            }
            items += "</div>";
            items += "<div class='r-right'>";
            items += " <ul><li><a href='#'><img src='../Content/images/del-icon.png' alt='' class='del-show' /></a></li>";
            if (manentry == '0') {
                items += "<li><a  class='fancybox' href='#infowindow' onclick='return info(\"" + id + "\",\"" + lat + "\",\"" + lng + "\",\"" + types + "\",\"" + att_or_res + "\");'><img src='../Content/images/info-icon.png' alt='' class='example1demo' /></a></li></ul>";
            }
            else if (manentry == '1') {
                if (rhaatype == "Attractions") {
                    items += "<li><a class='fancybox' href='#infowindow' onclick='return infomanual(\"2\",\"" + id + "\");'><img src='../Content/images/info-icon.png' alt='' class='example1demo' /></a></li></ul>";
                }
                else if (rhaatype == "Restaurant") {
                    items += "<li><a class='fancybox' href='#infowindow' onclick='return infomanual(\"3\",\"" + id + "\");'><img src='../Content/images/info-icon.png' alt='' class='example1demo' /></a></li></ul>";
                }
            }
            items += "</div></div></div>";
            items += "</aside>";
            $('#rhaa1').append(items);
        }
        //if (att_or_res == '1') {
        //    RstSrtArr=restaurant_forsort.slice();            
        //}
        //else if (att_or_res == '2') {
        //    AttrSrtArr=restaurant_forsort.slice();
        //}
    //}
    //else
    //{
    //    $('#rhaa1').html(" ");
    //    if (att_or_res == '1') {
    //        restaurant_forsort=RstSrtArr.slice();
    //    }
    //    else if (att_or_res == '2') {
    //        restaurant_forsort = AttrSrtArr.slice();
    //    }
    //    for (var position = 0, RestaurantsLength = restaurant_forsort.length; position < RestaurantsLength; position++) {
    //        var id = restaurant_forsort[position].plc_id;
    //        var Roundof = restaurant_forsort[position].plc_Roundof;
    //        var Rating = restaurant_forsort[position].plc_rating;
    //        var lat = restaurant_forsort[position].plc_lat;
    //        var lng = restaurant_forsort[position].plc_lng;
    //        var Name = restaurant_forsort[position].plc_name;
    //        var Address = restaurant_forsort[position].plc_address;
    //        var Photo = restaurant_forsort[position].plc_photo;
    //        var Trip = restaurant_forsort[position].plc_trip;
    //        var types = restaurant_forsort[position].plc_type;
    //        var reviews = restaurant_forsort[position].plc_reviews;
    //        var pricelevel = restaurant_forsort[position].plc_price;
    //        var weblink = restaurant_forsort[position].plc_web;
    //        var manentry = restaurant_forsort[position].plc_manual;
    //        var items = '';
    //        items += "<div id='rh" + id + "'></div><input type='hidden' class='hide'  id=" + id + " value='" + types + "'><input type='hidden' id='rates_" + id + "' value='" + Rating + "'><input type='hidden' id='ratefilter_" + id + "' value='" + Roundof + "'><input type='hidden' id='latarray" + id + "' value='" + lat + "'><input type='hidden' id='lngarray" + id + "' value='" + lng + "'><aside class='resort-row' id='res_" + id + "'><div class='resort-row1' id='divres_" + id + "'>";
    //        items += "<input type='hidden' class='reviews' value='" + reviews + "'/>";
    //        items += "<input type='hidden' class='priceLev' value='" + pricelevel + "'/>";
    //        items += "<input type='hidden' class='webhidden' value='" + weblink + "'/>";
    //        items += "<input type='hidden' id='latarray" + id + "' value='" + lat + "' class='reslat'><input type='hidden' id='lngarray" + id + "' value='" + lng + "' class='reslng'>";
    //        items += "<input type='hidden' id='isME" + id + "' value='" + manentry + "' class='resME'>";
    //        items += "<figure class='resort-img'><div class='tooltip'>Drag to add</div><img src='" + Photo + "' alt='' id='img_" + id + "' />";
    //        items += "</figure>";
    //        items += "<div class='resort-details'><div class='r-left'>";
    //        items += "<h3 id='h" + id + "'>" + Name + "  </h3><br/>";
    //        items += "<p style='height:40px; overflow-y:hidden;' id='p" + id + "'>" + Address + "</p>";
    //        if (weblink != "No site") {
    //            items += "<p style='height:18px; overflow-y:hidden;width:100%;text-overflow: ellipsis;white-space: nowrap;overflow: hidden;' id='web" + id + "'><b>Website : </b><a target='_blank' href='" + weblink + "' style='text-decoration: underline;'>" + weblink + "</a></p>";
    //        }
    //        items += "<ul id='rat" + id + "'><b>Ratings : </b>";
    //        if (att_or_res == '1') {
    //            for (var loopvar_rate = 1; loopvar_rate <= 5; loopvar_rate++) {
    //                if (Rating >= loopvar_rate) {
    //                    items += "<li><img src='../Content/images/coffee_full.png' /></li>";
    //                }
    //                else if (Rating > (loopvar_rate - 1) && Rating < loopvar_rate) {
    //                    items += "<li><img src='../Content/images/coffee_half.png' /></li>";
    //                }
    //                else {
    //                    items += "<li><img src='../Content/images/coffee_grey_full.png' /></li>";
    //                }
    //            }
    //        }
    //        else {
    //            for (var loopvar_rate = 1; loopvar_rate <= 5; loopvar_rate++) {
    //                if (Rating >= loopvar_rate) {
    //                    items += "<li><img src='../Content/images/tower_full.png' /></li>";
    //                }
    //                else if (Rating > (loopvar_rate - 1) && Rating < loopvar_rate) {
    //                    items += "<li><img src='../Content/images/tower_half.png' /></li>";
    //                }
    //                else {
    //                    items += "<li><img src='../Content/images/tower_grey_full.png' /></li>";
    //                }
    //            }
    //        }
    //        if (reviews != "No rev") {
    //            items += "<li>(" + reviews + " reviews)</li>";
    //        }
    //        items += "</ul>";
    //        if (pricelevel == 0) {
    //            items += "<p style='height: 18px;' id='price" + id + "'><b> Price Level:</b> $</p> ";
    //        }
    //        else if (pricelevel == 1) {
    //            items += "<p style='height: 18px;' id='price" + id + "'><b> Price Level:</b> $</p> ";
    //        }
    //        else if (pricelevel == 2) {
    //            items += "<p style='height: 18px;' id='price" + id + "'><b> Price Level:</b>  $$</p> ";
    //        }
    //        else if (pricelevel == 3) {
    //            items += "<p style='height: 18px;' id='price" + id + "'><b> Price Level:</b>  $$$</p> ";
    //        }
    //        else if (pricelevel == 4) {
    //            items += "<p style='height: 18px;' id='price" + id + "'><b> Price Level:</b>  $$$$</p> ";
    //        }
    //        if (att_or_res == '1') {
    //            items += "<div class='book-add'><span><a href='#' id='inner" + id + "' class='add-trip' onclick='return RestaurantToleft(\"" + id + "\");'>" + Trip + "</a></span></div>";
    //        }
    //        else if (att_or_res == '2') {
    //            items += "<div class='book-add'><span><a href='#' id='inner" + id + "' class='add-trip' onclick='return AttractionToleft(\"" + id + "\");'>" + Trip + "</a></span></div>";
    //        }
    //        items += "</div>";
    //        items += "<div class='r-right'>";
    //        items += " <ul><li><a href='#'><img src='../Content/images/del-icon.png' alt='' class='del-show' onclick='fun2'/></a></li>";
    //        if (manentry == '0') {
    //            items += "<li><a  class='fancybox' href='#infowindow' onclick='return info(\"" + id + "\",\"" + lat + "\",\"" + lng + "\",\"" + types + "\",\"" + att_or_res + "\");'><img src='../Content/images/info-icon.png' alt='' class='example1demo' /></a></li></ul>";
    //        }
    //        else if (manentry == '1') {
    //            if (rhaatype == "Attractions") {
    //                items += "<li><a class='fancybox' href='#infowindow' onclick='return infomanual(\"2\",\"" + id + "\");'><img src='../Content/images/info-icon.png' alt='' class='example1demo' /></a></li></ul>";
    //            }
    //            else if (rhaatype == "Restaurant") {
    //                items += "<li><a class='fancybox' href='#infowindow' onclick='return infomanual(\"3\",\"" + id + "\");'><img src='../Content/images/info-icon.png' alt='' class='example1demo' /></a></li></ul>";
    //            }
    //        }
    //        items += "</div></div></div>";
    //        items += "</aside>";
    //        $('#rhaa1').append(items);
    //    }
    //}
    intheTripRHAA(rhaatype);
    var noresultsAdd = 0;
    if (rhaatype == "Attractions" && IsFilter == 1) {
        AttractionFilters(restaurant_forsort,att_or_res);
        var visibleLen = $("#rhaa1").find(".resort-row:visible").length + $("#rhaa1").find(".resort-row2:visible").length;
        $("#NoResBlock").hide();
        if(visibleLen<=0)
        {
            $("#NoResBlock").show();
            noresultsAdd = 1;
        }
        else {
            $("#NoResBlock").hide();
        }
    }
    else if (rhaatype == "Restaurant" && IsFilter == 1) {
        AttractionFilters(restaurant_forsort, att_or_res);
        var visibleLen = $("#rhaa1").find(".resort-row:visible").length + $("#rhaa1").find(".resort-row2:visible").length;
        $("#NoResBlock").hide();
        if (visibleLen <= 0) {
            $("#NoResBlock").show();
            noresultsAdd = 1;
        }
        else {
            $("#NoResBlock").hide();
        }
    }
    if (Count <= 0 && getmanualEntryVal == 1)
    {
        apiStatus = 1;
        if (noresultsAdd == 0) {
            $('#rhaa1').html("No results found..");
        }
    }
}

function getpopup() {
    $("#hlcity").attr("href", "#dest-popup");
    return false;
}

function getActivityInfo(ActId,slctdCity) {
    $("#infowindow_hotels_activities").html("");
    var CityForActsReq = slctdCity;
    var items = "";
    var actsInCity = [];
    var chkForActdetails = 0;
    var CitiesHotelsLength = 0;
    for (var loopAct = 0, CitiesHotelsLength = ActsForCities.length; loopAct < CitiesHotelsLength; loopAct++) {
        if (ActsForCities[loopAct].cityName.trim() == CityForActsReq.trim()) {
            actsInCity = ActsForCities[loopAct].Acts.activities.slice();
            var foundAct = [];
            for(var inloopAct = 0, actsLength = actsInCity.length; inloopAct < actsLength; inloopAct++)
            {
                if (actsInCity[inloopAct].id == ActId) {
                    foundAct = actsInCity[inloopAct];
                   
                    items += "<div id='info_" + foundAct.id + "' class='info-left popup-bg'  style='width:94%;'>";
                    items += "<figure class='PopUp-CloseBtn' onclick='$.fancybox.close();'><img src='../Content/images/del_travel-bkp-7-5-15.png'></figure>";
                    items += "<figure class='resort-img'>";
                    items += "<img src='" + foundAct.imageUrl + "' alt='' />";
                    items += "<span>Price from <strong>" + foundAct.fromPrice + "</strong></span></figure>";
                    items += "<h2>" + foundAct.title + "</h2>";
                    items += "<ul><b>Ratings : </b>";
                    var rating = (foundAct.recommendationScore / 20).toFixed(1);
                    for (var loopvar_rate = 1; loopvar_rate <= 5; loopvar_rate++) {
                        if (rating >= loopvar_rate) {
                            items += "<li><img src='../Content/images/tennisball_full.png' /></li>";
                        }
                        else if (rating > (loopvar_rate - 1) && rating < loopvar_rate) {
                            items += "<li><img src='../Content/images/tennisball_half.png' /></li>";
                        }
                        else {
                            items += "<li><img src='../Content/images/tennisball_grey_full.png' /></li>";
                        }
                    }
                    items += "<li>(" + rating + ")</li></ul>";
                    if (foundAct.recommendationScore && foundAct.recommendationScore != null) {
                        items += "<p style='height:25px;'><b>Recommendation Score : </b>" + foundAct.recommendationScore + "</p>";
                    }
                    if (foundAct.shortDescription && foundAct.shortDescription != null) {
                        items += "<p id='pho_" + foundAct.id + "'><b>Description : </b>" + foundAct.shortDescription + "</p>";
                    }
                    if (foundAct.duration && foundAct.duration != null) {
                        items += "<p style='height:25px;'><b>Duration : </b>" + foundAct.duration + "</p>";
                    }
                    if (foundAct.fromOriginalPrice && foundAct.fromOriginalPrice != null) {
                        items += "<p style='height:25px;'><b>Original Price : </b>" + foundAct.fromOriginalPrice + "</p>";
                    }
                    if (foundAct.discountPercentage && foundAct.discountPercentage != null) {
                        items += "<p style='height:25px;'><b>Discount : </b>" + foundAct.discountPercentage + "%</p>";
                    }
                    if (foundAct.categories && foundAct.categories.length > 0) {
                        items+="<p style='height:25px;'><b>Categories : </b></p>";
                        for (var cnt = 0, lmt = foundAct.categories.length; cnt < lmt; cnt++) {
                            items += "<p style='height:25px;'>" + foundAct.categories[cnt] + "</p>";
                        }
                    }
                    items += "</div>";
                    $('#infowindow_hotels_activities').append(items);
                    break;
                }
            }
        }
    }
}

