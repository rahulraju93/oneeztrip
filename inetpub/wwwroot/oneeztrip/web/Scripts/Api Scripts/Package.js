﻿var attraction_array_addday = [], att_actarray_addday = [], activitiesfull_array_addday = [], oldatt_actarray_addday = [];
var otherattraction_addday = [], restlist_array_addday = [], rest_array_addday = [], hotelforadday_addday = [], oldotherattraction_addday=[];
var hotel_array_addday = [], hotel_arrayfull_addday = [], concatenate_array_addday = [], hotels_retrievedCities = [];
var secondClassAttr_addday = [], thirdClassAttr_addday = [], tmpOtherAttr_addday = [];
var act_arrayfull_addday = [], act_array_addday = [];
var Allcitiesarray = [], non_exists_city = [];
var increDetAddDay = 0, gotDetAddDay = 0, increOthDetAddDay = 0, gotOthDetAddDay = 0;
var increForNonExist = 0;

function chkrestaurant(id, keywords) {
    $('#RightSidePart').block({
        message: 'Please wait while data is loading...',
        css: {
            border: 'none',
            padding: '15px',
            float: 'right',
            'margin-right': '0%',

            backgroundColor: '#33c6ea',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        }
    });

    var tmpcntArray = [];
    prevRestTypes = "";
    $(".RestTypeCl:checked").each(function () {
        tmpcntArray.push($(this).next('input').val());
    });
    prevRestTypes = tmpcntArray.join(",");
    var tmpRestaurants = [];
    var setFlag = false;
    for (var loopvar = 0, LengthRes = ListRestaurants.length; loopvar < LengthRes; loopvar++) {
        if (ListRestaurants[loopvar].resType == keywords) {
            tmpRestaurants=(ListRestaurants[loopvar].restaurants).slice(0);
            setFlag = true;
            break;
        }
    }
    if (setFlag) {
        var countRes = 0;
        refkeywords = keywords;
        for (var countRes = 0, ResultsCount = tmpRestaurants.length; countRes < ResultsCount ; countRes++) {

            addfilResultrestaurant(tmpRestaurants[countRes], countRes, refkeywords);
        }
        if (countRes == tmpRestaurants.length) {
            SortRestaurant($('#show_hide_ressort').find('.selectContainerStyled').find('.select_box').val(), '1');
        }
    }
    else {
        var value = $('#chkres' + id).val();
        var Length = $('#dynamicrestype').parent().find('.rating-vote').length;
        Length += $('#dynamicrestype').parent().next().find('.rating-vote').length;
        if (value == 0) {
            var LengthVar = 0;
            for (var cnt = 1; cnt <= Length; cnt++) {
                if ($('#chkres' + cnt).val() == 1) {
                    ++LengthVar;
                }
            }
            if (LengthVar >= 1) {
                refkeywords = keywords;
                $('#chkres' + id).val("1");
                var pyrmont = new google.maps.LatLng($('#latarray').val(), $('#lngarray').val());
                map = new google.maps.Map(document.getElementById('map-canvas'), {
                    center: pyrmont,
                    zoom: 15
                });
                var request = {
                    location: pyrmont,
                    radius: 5000,
                    rankby: ['prominence'],
                    types: ['restaurant|food|bakery'],
                    keyword: [keywords]
                };
                infowindow = new google.maps.InfoWindow();
                var service = new google.maps.places.PlacesService(map);
                service.nearbySearch(request, function (response, status) { callbackfilrestaurant(response, status, keywords) });
            }
            else {
                refkeywords = keywords;
                $('#rhaa1').html("");
                $('#chkres' + id).val("1");
                var pyrmont = new google.maps.LatLng($('#latarray').val(), $('#lngarray').val());
                map = new google.maps.Map(document.getElementById('map-canvas'), {
                    center: pyrmont,
                    zoom: 15
                });
                var request = {
                    location: pyrmont,
                    radius: 5000,
                    rankby: ['prominence'],
                    types: ['restaurant|food|bakery'],
                    keyword: [keywords]
                };
                infowindow = new google.maps.InfoWindow();
                var service = new google.maps.places.PlacesService(map);
                service.nearbySearch(request, function (response, status) { callbackfilrestaurant(response, status, keywords) });
            }
        }
        else {
            $('#chkres' + id).val("0");
            var LengthVar = 0;
            for (var cnt = 1; cnt <= Length; cnt++) {
                if ($('#chkres' + cnt).val() == 1) {
                    ++LengthVar;
                }
            }
            if (LengthVar >= 1) {
                var results = $('#rhaa1').find('.resort-row').length;
                for (var length = 0; length < results; length++) {
                    var relid = $('#rhaa1').find('.hide').eq(length).attr("id");
                    var type = $('#' + relid).val();
                    if (type == keywords) {
                        $('#res_' + relid).hide();
                        $('#rates_' + relid).hide();
                        $('#rh' + relid).hide();
                        $('#' + relid).hide();
                        $('#ratefilter_' + relid).hide();
                    }
                }
            }
            else {
                $('#rhaa1').html("");
                Displayallrestaurant();
            }
        }
    }
}

function filterRestaurantType(id,keywords)
{
    filterManualRestTyp(keywords);
    $('#RightSidePart').block({
        message: 'Please wait while data is loading...',
        css: {
            border: 'none',
            padding: '15px',
            float: 'right',
            'margin-right': '0%',
            backgroundColor: '#33c6ea',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        }
    });
    var tmpRestaurants = [];
    var setFlag = false;
    var foundFlag = false;
    var arrTmp = [];
    for (var loopvar = 0, LengthRes = ListRestaurants.length; loopvar < LengthRes; loopvar++) {
        if (ListRestaurants[loopvar].resType == keywords) {
            tmpRestaurants = (ListRestaurants[loopvar].restaurants).slice(0);
            setFlag = true;
            break;
        }
    }

    if (setFlag) {
        gotfiltRestCnt++;
        var countRes = 0;
        refkeywords = keywords;
        for (countRes = 0, ResultsCount = tmpRestaurants.length; countRes < ResultsCount ; countRes++) {
            foundFlag = false;
            for (var findLoop = 0, ListLength = restDetails.length; findLoop < ListLength; findLoop++) {
                if (tmpRestaurants[countRes].place_id == restDetails[findLoop].plc_id && tmpRestaurants[countRes].rating) {
                    if (restDetails[findLoop].details) {
                        arrTmp = restDetails[findLoop].details;
                        foundFlag = true;
                        break;
                    }
                    
                }
            }
            if (foundFlag) {
                addfilResultrestaurant(arrTmp, 0, refkeywords);
            }
        }
        if (gotfiltRestCnt == filtRestCnt) {            
                SortRestaurant($('#show_hide_ressort').find('.selectContainerStyled').find('.select_box').val(), '1');
            }
    }
    else {
        refkeywords = keywords;
        var pyrmont = new google.maps.LatLng($('#latarray').val(), $('#lngarray').val());
        map = new google.maps.Map(document.getElementById('map-canvas'), {
            center: pyrmont,
            zoom: 15
        });
        var request = {
            location: pyrmont,
            radius: 5000,
            rankby: ['prominence'],
            types: ['restaurant|food|bakery'],
            keyword: [keywords]
        };
        infowindow = new google.maps.InfoWindow();
        var service = new google.maps.places.PlacesService(map);
        service.nearbySearch(request, function (response, status) { callbackfilrestaurant(response, status, keywords) });
    }
}

function callbackfilrestaurant(results, status, keywords) {
    var cnt = 0, ResultsCount = 0;
    gotfiltRestCnt++;
    if (status == google.maps.places.PlacesServiceStatus.OK) {
        ListRestaurants.push({
            resType: keywords,
            restaurants: results,
            cityname: $('#selectedcity').html()
        });
        for (cnt = 0, ResultsCount = results.length; cnt < ResultsCount ; cnt++) {
            if (results[cnt].rating) {
                AllRestColSugPart.push({
                    plc: results[cnt],
                    keyword: keywords
                });
                restDetails.push({
                    plc_id: results[cnt].place_id,
                    attrType: keywords
                });
                addfilResultrestaurant(results[cnt], 0, keywords);
            }
        }
    }
    if (gotfiltRestCnt == filtRestCnt) {
        TotalRests_SugPart = AllRestColSugPart.length;
        //filterManualRestTyp(keywords);
        getFiltRestDet();
    }
}

function getFiltRestDet()
{
    //if (TotalRests_SugPart == incrRests_SugPart) {
        SortRestaurant($('#show_hide_ressort').find('.selectContainerStyled').find('.select_box').val(), '1');
        //setTimeout(function () { SortRestaurant($('#show_hide_ressort').find('.selectContainerStyled').find('.select_box').val(), '1'); },1000);
    //}
    //else
    //{
    //    var plcId = AllRestColSugPart[incrRests_SugPart].plc.place_id;
    //    var restauranttype = AllRestColSugPart[incrRests_SugPart].keyword;
    //    var request = {
    //        placeId: plcId
    //    };
    //    var service = new google.maps.places.PlacesService(map);
    //    service.getDetails(request, function (place, status) {
    //        if (status == google.maps.places.PlacesServiceStatus.OK) {
    //            if (place.user_ratings_total) {
    //                allrestaurants_toleft.push(place);
    //                var userreview = place.user_ratings_total;
    //                for (var indxVal = 0, restDetVal = restDetails.length; indxVal < restDetVal; indxVal++) {
    //                    if (restDetails[indxVal].plc_id == place.place_id) {
    //                        restDetails[indxVal].UserReviews = userreview;
    //                        restDetails[indxVal].details = place;
    //                        break;
    //                    }
    //                }
    //                addfilResultrestaurant(place, 0, restauranttype);
    //            }
    //            incrRests_SugPart++;
    //            getFiltRestDet();
    //        }
    //        else {
    //            if (status == google.maps.GeocoderStatus.OVER_QUERY_LIMIT) {
    //                sleep(1000);
    //                getFiltRestDet();
    //            }
    //            else {
    //                incrRests_SugPart++;
    //                getFiltRestDet();
    //            }
    //        }
    //    });
    //}
}

function addfilResultrestaurant(place, indexValue, refkeywords) {
    if (jQuery.inArray(place.place_id, leftsidearray) !== -1) {
        var value = $.isArray(place.photos, place);
        var rating = Math.round(place.rating);
        var items = '';
        if (place.rating) {
            rating = place.rating;
            var ratefilter = parseInt(place.rating);
            items += "<div id='rh" + place.place_id + "'></div><input type='hidden' class='hide'  id=" + place.place_id + " value='" + refkeywords + "'><input type='hidden' id='rates_" + place.place_id + "' value='" + rating + "'><input type='hidden' id='ratefilter_" + place.place_id + "' value='" + ratefilter + "'><input type='hidden' id='latarray" + place.place_id + "' value='" + place.geometry.location.lat() + "'><input type='hidden' id='lngarray" + place.place_id + "' value='" + place.geometry.location.lng() + "'><aside class='resort-row' id='res_" + place.place_id + "'><div class='resort-row1' id='divres_" + place.place_id + "'>";
            items += "<input type='hidden' id='isME" + place.place_id + "' value='0' class='resME'>";
            if (place.user_ratings_total) {
                items += "<input type='hidden' class='reviews' value='" + place.user_ratings_total + "'/>";
            }
            else {
                items += "<input type='hidden' class='reviews' value='No rev'/>";
            }
            if (place.price_level) {
                items += "<input type='hidden' class='priceLev' value='" + place.price_level + "'/>";
            }
            else {
                items += "<input type='hidden' class='priceLev' value='0'/>";
            }
            if (place.website) {
                items += "<input type='hidden' class='webhidden' value='" + place.website + "'>";
            }
            else {
                items += "<input type='hidden' class='webhidden' value='No site'>";
            }
            if (value == true) {
                items += "<figure class='resort-img'><img src='" + place.photos[0].getUrl({ 'maxWidth': 182, 'maxHeight': 120 }) + "' alt='' id='img_" + place.place_id + "' />";
                items += "</figure>";
            }
            else {
                items += "<figure class='resort-img'><img src='../Content/images/AppImages/restaurant.png' alt='' id='img_" + place.place_id + "' />";
                items += "</figure>";
            }
            items += "<div class='resort-details'><div class='r-left'>";
            items += "<h3 id='h" + place.place_id + "'>" + place.name + "  </h3><br/>";
            items += "<p style='height:40px; overflow-y:hidden;' id='p" + place.place_id + "'>" + place.vicinity + "</p>";
            if (place.website) {
                items += "<p style='height:18px; overflow-y:hidden;width:100%;text-overflow: ellipsis;white-space: nowrap;overflow: hidden;' id='web" + place.place_id + "'><b>Website : </b><a target='_blank' href='" + place.website + "' style='text-decoration: underline;'>" + place.website + "</a></p>";
            }
            items += "<ul id='rat" + place.place_id + "'><b>Ratings : </b>";
            for (var loopvar_rate = 1; loopvar_rate <= 5; loopvar_rate++) {
                if (rating >= loopvar_rate) {
                    items += "<li><img src='../Content/images/coffee_full.png' /></li>";
                }
                else if (rating > (loopvar_rate - 1) && rating < loopvar_rate) {
                    items += "<li><img src='../Content/images/coffee_half.png' /></li>";
                }
                else {
                    items += "<li><img src='../Content/images/coffee_grey_full.png' /></li>";
                }
            }
            if (place.user_ratings_total) {
                //items += "<li>(" + place.user_ratings_total + " reviews)</li>";
            }
            items += "</ul>";
            if (place.price_level) {
                if (place.price_level == 0) {
                    items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b> $</p> ";
                }
                else if (place.price_level == 1) {
                    items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b> $</p> ";
                }
                else if (place.price_level == 2) {
                    items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b>  $$</p> ";
                }
                else if (place.price_level == 3) {
                    items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b>  $$$</p> ";
                }
                else if (place.price_level == 4) {
                    items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b>  $$$$</p> ";
                }
            }
            else {
                items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b> $</p> ";
            }
            items += "<div class='book-add'><span><a href='#' id='inner" + place.place_id + "' class='add-trip' onclick='return RestaurantToleft(\"" + place.place_id + "\");'>In the trip</a></span></div></div> ";
            items += "<div class='r-right'>";
            items += " <ul><li><a href='#'><img src='../Content/images/del-icon.png' alt='' class='del-show'/></a></li>";
            items += "<li><a  class='fancybox' href='#infowindow' onclick='return info(\"" + place.place_id + "\",\"" + place.geometry.location.lat() + "\",\"" + place.geometry.location.lng() + "\",\"" + refkeywords + "\");'><img src='../Content/images/info-icon.png' alt='' class='example1demo' /></a></li></ul>";
            items += "</div></div></div>";
            items += "</aside>";
            $('#rhaa1').append(items);
        }
    }
    else {
        var value = $.isArray(place.photos, place);
        var rating = Math.round(place.rating);
        var items = '';
        if (place.rating) {
            rating = place.rating;
            var ratefilter = parseInt(place.rating);
            items += "<div id='rh" + place.place_id + "'></div><input type='hidden' class='hide'  id=" + place.place_id + " value='" + refkeywords + "'><input type='hidden' id='rates_" + place.place_id + "' value='" + rating + "'><input type='hidden' id='ratefilter_" + place.place_id + "' value='" + ratefilter + "'><input type='hidden' id='latarray" + place.place_id + "' value='" + place.geometry.location.lat() + "'><input type='hidden' id='lngarray" + place.place_id + "' value='" + place.geometry.location.lng() + "'><aside class='resort-row' id='res_" + place.place_id + "'><div class='resort-row1' id='divres_" + place.place_id + "'>";
            items += "<input type='hidden' id='isME" + place.place_id + "' value='0' class='resME'>";
            if (place.user_ratings_total) {
                items += "<input type='hidden' class='reviews' value='" + place.user_ratings_total + "'/>"
            }
            else {
                items += "<input type='hidden' class='reviews' value='No rev'/>"
            }
            if (place.price_level) {
                items += "<input type='hidden' class='priceLev' value='" + place.price_level + "'/>"
            }
            else {
                items += "<input type='hidden' class='priceLev' value='0'/>"
            }
            if (place.website) {
                items += "<input type='hidden' class='webhidden' value='" + place.website + "'>";
            }
            else {
                items += "<input type='hidden' class='webhidden' value='No site'>";
            }
            if (value == true) {
                items += "<figure class='resort-img'><img src='" + place.photos[0].getUrl({ 'maxWidth': 182, 'maxHeight': 120 }) + "' alt='' id='img_" + place.place_id + "' />";
                items += "</figure>";
            }
            else {
                items += "<figure class='resort-img'><img src='../Content/images/AppImages/restaurant.png' alt='' id='img_" + place.place_id + "' />";
                items += "</figure>";
            }
            items += "<div class='resort-details'><div class='r-left'>";
            items += "<h3 id='h" + place.place_id + "'>" + place.name + "  </h3><br/>";
            items += "<p style='height:40px; overflow-y:hidden;' id='p" + place.place_id + "'>" + place.vicinity + "</p>";
            if (place.website) {
                items += "<p style='height:18px; overflow-y:hidden;width:100%;text-overflow: ellipsis;white-space: nowrap;overflow: hidden;' id='web" + place.place_id + "'><b>Website : </b><a target='_blank' href='" + place.website + "' style='text-decoration: underline;'>" + place.website + "</a></p>";
            }
            items += "<ul id='rat" + place.place_id + "'><b>Ratings : </b>";
            for (var loopvar_rate = 1; loopvar_rate <= 5; loopvar_rate++) {
                if (rating >= loopvar_rate) {
                    items += "<li><img src='../Content/images/coffee_full.png' /></li>";
                }
                else if (rating > (loopvar_rate - 1) && rating < loopvar_rate) {
                    items += "<li><img src='../Content/images/coffee_half.png' /></li>";
                }
                else {
                    items += "<li><img src='../Content/images/coffee_grey_full.png' /></li>";
                }
            }
            if (place.user_ratings_total) {
                //items += "<li>(" + place.user_ratings_total + " reviews)</li>";
            }
            items += "</ul>";
            if (place.price_level) {
                if (place.price_level == 0) {
                    items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b> $</p> ";
                }
                else if (place.price_level == 1) {
                    items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b> $</p> ";
                }
                else if (place.price_level == 2) {
                    items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b>  $$</p> ";
                }
                else if (place.price_level == 3) {
                    items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b>  $$$</p> ";
                }
                else if (place.price_level == 4) {
                    items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b>  $$$$</p> ";
                }
            }
            else {
                items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b> $</p> ";
            }
            items += "<div class='book-add'><span><a href='#' id='inner" + place.place_id + "' class='add-trip' onclick='return RestaurantToleft(\"" + place.place_id + "\");'>Add Trip</a></span></div></div> ";
            items += "<div class='r-right'>";
            items += " <ul><li><a href='#'><img src='../Content/images/del-icon.png' alt='' class='del-show' /></a></li>";
            items += "<li><a  class='fancybox' href='#infowindow' onclick='return info(\"" + place.place_id + "\",\"" + place.geometry.location.lat() + "\",\"" + place.geometry.location.lng() + "\",\"" + refkeywords + "\");'><img src='../Content/images/info-icon.png' alt='' class='example1demo' /></a></li></ul>";
            items += "</div></div></div>";
            items += "</aside>";
            $('#rhaa1').append(items);
        }
    }
}

function filterAttractionType(id, keywords) {
   
    filterManualAttrTyp(keywords);
    $('#RightSidePart').block({
        message: 'Please wait while data is loading...',
        css: {
            border: 'none',
            padding: '15px',
            float: 'right',
            'margin-right': '0%',

            backgroundColor: '#33c6ea',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        }
    });
    var tmpAttractions = [];
    var setFlag = false;
    var foundFlag = false;
    var someFlag = false;
    var arrTmp = [];
    for (var loopvar = 0, LengthRes = ListAttractions.length; loopvar < LengthRes; loopvar++) {
        if (ListAttractions[loopvar].attrType == keywords) {
            tmpAttractions = (ListAttractions[loopvar].attractions).slice(0);
            setFlag = true;
            break;
        }
    }
    if (setFlag) {
        gotfiltAttrCnt++;
        var countRes = 0, refkeywords = keywords;
        for (countRes = 0, ResultsLength = tmpAttractions.length; countRes < ResultsLength; countRes++) {
            foundFlag = false;
            for (var findLoop = 0, ListLength = attrDetails.length; findLoop < ListLength; findLoop++) {
                if (tmpAttractions[countRes].place_id == attrDetails[findLoop].plc_id && tmpAttractions[countRes].rating > 2.5) {
                    var needsExclusions = [];
                    needsExclusions = tmpAttractions[countRes].types;
                    someFlag = true;
                    for (var index1 = 0; index1 < excludeMe.length; index1++) {
                        if (needsExclusions.indexOf(excludeMe[index1]) !== -1) {
                            someFlag = false;
                            break;
                        }
                    }
                    if (someFlag) {
                        if ((tmpAttractions[countRes].name).toLowerCase().indexOf("college") >= 0) {
                            someFlag = false;
                        }
                    }
                    if (someFlag) {
                        if (attrDetails[findLoop].details) {
                            arrTmp = attrDetails[findLoop].details;
                            foundFlag = true;
                            break;
                        }
                    }                    
                }
            }
            if (foundFlag) {
                filterattractionResult(arrTmp, 0, refkeywords);
            }
        }
        if (countRes == tmpAttractions.length) {
            if (gotfiltAttrCnt == filtAttrCnt) {
                SortRestaurant($('#show_hide_attrsort').find('.selectContainerStyled').find('.select_box').val(), '2');
            }
        }
    }
    else {
        var refkeywords = keywords;
        var pyrmont = new google.maps.LatLng($('#latarray').val(), $('#lngarray').val());
        map = new google.maps.Map(document.getElementById('map-canvas'), {
            center: pyrmont,
            zoom: 15
        });
        var request = {
            location: pyrmont,
            radius: 20000,
            rankby: ['prominence'],
            types: [refkeywords]
        };
        infowindow = new google.maps.InfoWindow();
        var service = new google.maps.places.PlacesService(map);
        service.nearbySearch(request, function (response, status) { callbackfilattraction(response, status, keywords) });
    }
}

function chkamusement(id, keywords) {
    $('#RightSidePart').block({
        message: 'Please wait while data is loading...',
        css: {
            border: 'none',
            padding: '15px',
            float: 'right',
            'margin-right': '0%',

            backgroundColor: '#33c6ea',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        }
    });

    var tmpcntArray = [];
    prevAttrTypes = "";
    $(".AttrTypeCl:checked").each(function () {
        tmpcntArray.push($(this).next('input').val());
    });
    prevAttrTypes = tmpcntArray.join(",");

    var tmpAttractions = [];
    var tmpRestaurants = [];
    var setFlag = false;
    for (var loopvar = 0, LengthRes = ListAttractions.length; loopvar < LengthRes; loopvar++) {
        if (ListAttractions[loopvar].attrType == keywords) {
            tmpRestaurants = (ListAttractions[loopvar].attractions).slice(0);
            setFlag = true;
            break;
        }
    }
    if (setFlag) {
        var countRes = 0, refkeywords = keywords;
        for (countRes = 0, ResultsLength = tmpRestaurants.length; countRes < ResultsLength; countRes++) {
            filterattractionResult(tmpRestaurants[countRes], countRes, refkeywords);
        }
        if (countRes == tmpRestaurants.length) {
            SortRestaurant($('#show_hide_attrsort').find('.selectContainerStyled').find('.select_box').val(), '2');
        }
    }
    else {
        var value = $('#chkatt_' + id).val();
        var Length = $('#dynamicatttype').parent().find('.rating-vote').length;
        Length += $('#dynamicatttype').parent().next().find('.rating-vote').length;

        if (value == 0) {
            var LengthVar = 0;
            for (var cnt = 1; cnt <= Length; cnt++) {
                if ($('#chkatt_' + cnt).val() == 1) {
                    ++LengthVar;
                }
            }
            if (LengthVar >= 1) {
                $('#chkatt_' + id).val("1");
                var refkeywords = keywords;
                var pyrmont = new google.maps.LatLng($('#latarray').val(), $('#lngarray').val());
                map = new google.maps.Map(document.getElementById('map-canvas'), {
                    center: pyrmont,
                    zoom: 15
                });
                var request = {
                    location: pyrmont,
                    radius: 20000,
                    rankby: ['prominence'],
                    types: [refkeywords]
                };
                infowindow = new google.maps.InfoWindow();
                var service = new google.maps.places.PlacesService(map);
                service.nearbySearch(request, function (response, status) { callbackfilattraction(response, status, keywords) });
            }
            else {
                $('#rhaa1').html("");
                $('#chkatt_' + id).val("1");
                var refkeywords = keywords;
                var pyrmont = new google.maps.LatLng($('#latarray').val(), $('#lngarray').val());
                map = new google.maps.Map(document.getElementById('map-canvas'), {
                    center: pyrmont,
                    zoom: 15
                });
                var request = {
                    location: pyrmont,
                    radius: 20000,
                    rankby: ['prominence'],
                    types: [refkeywords]
                };
                infowindow = new google.maps.InfoWindow();
                var service = new google.maps.places.PlacesService(map);
                service.nearbySearch(request, function (response, status) { callbackfilattraction(response, status, keywords) });
            }
        }
        else {
            $('#chkatt_' + id).val("0");
            var LengthVar = 0, refkeywords = keywords;
            for (var cnt = 1; cnt <= Length; cnt++) {
                if ($('#chkatt_' + cnt).val() == 1) {
                    ++LengthVar;
                }
            }
            if (LengthVar >= 1) {
                var length = $('#rhaa1').find('.hide').length;
                for (var del = 0; del < length; del++) {
                    var relid = $('#rhaa1').find('.hide').eq(del).attr("id");
                    var type = $('#' + relid).val();
                    if (type == refkeywords) {
                        $('#res_' + relid).hide();
                        $('#rates_' + relid).hide();
                        $('#rh' + relid).hide();
                        $('#rhaa1').find('#' + relid).hide();
                        $('#ratefilter_' + relid).hide();
                    }
                }
            }
            else {
                $('#rhaa1').html("");
                Displayall();
            }
        }
    }
}

function callbackfilattraction(results, status, keywords) {
    var cnt = 0;
    gotfiltAttrCnt++;
    if (status == google.maps.places.PlacesServiceStatus.OK) {
        var needsExclusions = [];
        ListAttractions.push({
            attrType: keywords,
            attractions: results,
            cityname: $('#selectedcity').html()
        });
        for (var indexValue = 0, ResultsLength = results.length; indexValue < ResultsLength; indexValue++) {
            needsExclusions = results[indexValue].types;
            someFlag = true;
            for (var index1 = 0; index1 < excludeMe.length; index1++) {
                if (needsExclusions.indexOf(excludeMe[index1]) !== -1) {
                    someFlag = false;
                    break;
                }
            }
            if (someFlag) {
                if ((results[indexValue].name).toLowerCase().indexOf("college") >= 0) {
                    someFlag = false;
                }
            }
            if (someFlag) {
                if (results[indexValue].rating) {
                    if (results[indexValue].rating >= 1) {                        
                        AllAttrColSugPart.push({
                            plc: results[indexValue],
                            keyword: keywords
                        })
                        attrDetails.push({
                            plc_id: results[indexValue].place_id,
                            attrType: keywords
                        });
                        filterattractionResult(results[indexValue], 0, keywords);
                    }
                }
            }
        }
    }
    if (gotfiltAttrCnt == filtAttrCnt) {
        TotalAttrs_SugPart = AllAttrColSugPart.length;
        getFilterDet();
    }
}

function getFilterDet()
{
    //if (TotalAttrs_SugPart == incrAttrs_SugPart) {
        SortRestaurant($('#show_hide_attrsort').find('.selectContainerStyled').find('.select_box').val(), '2');
        //setTimeout(function () { SortRestaurant($('#show_hide_attrsort').find('.selectContainerStyled').find('.select_box').val(), '2'); }, 1000);
    //}
    //else
    //{
    //    var plcId = AllAttrColSugPart[incrAttrs_SugPart].plc.place_id;
    //    var attractiontype = AllAttrColSugPart[incrAttrs_SugPart].keyword;
    //    var request = {
    //        placeId: plcId
    //    };
    //    var service = new google.maps.places.PlacesService(map);
    //    service.getDetails(request, function (place, status) {
    //        if (status == google.maps.places.PlacesServiceStatus.OK) {
    //            if (place.user_ratings_total) {
    //                allattractions_toleft.push(place);
    //                var userreview = place.user_ratings_total;
    //                for (var indxVal = 0, attrDetVal = attrDetails.length; indxVal < attrDetVal; indxVal++) {
    //                    if (attrDetails[indxVal].plc_id == place.place_id) {
    //                        attrDetails[indxVal].UserReviews = userreview;
    //                        attrDetails[indxVal].details = place;
    //                        break;
    //                    }
    //                }
    //                filterattractionResult(place, 0, attractiontype);
    //            }
    //            incrAttrs_SugPart++;
    //            getFilterDet();
    //        }
    //        else
    //        {
    //            if (status == google.maps.GeocoderStatus.OVER_QUERY_LIMIT) {
    //                sleep(1000);
    //                getFilterDet();
    //            }
    //            else {
    //                incrAttrs_SugPart++;
    //                getFilterDet();
    //            }
    //        }
    //    });
    //}
}

function filterattractionResult(place, indexVal, refkeywords) {
    alert('filterattractionResult');
    if (jQuery.inArray(place.place_id, leftsidearray) !== -1) {
        var value = $.isArray(place.photos, place);
        if (value == true) {
            var rating = Math.round(place.rating);
            var items = '';
            if (place.rating) {
                rating = place.rating;
                var ratefilter = parseInt(place.rating);
                items += "<div id='rh" + place.place_id + "'></div><input type='hidden' class='hide'  id=" + place.place_id + " value='" + refkeywords + "'><input type='hidden' id='rates_" + place.place_id + "' value='" + rating + "'><input type='hidden' id='ratefilter_" + place.place_id + "' value='" + ratefilter + "'>";
                items += "<input type='hidden' id='latarray" + place.place_id + "' value='" + place.geometry.location.lat() + "'><input type='hidden' id='lngarray" + place.place_id + "' value='" + place.geometry.location.lng() + "'>";
                items += "<aside class='resort-row' id='res_" + place.place_id + "'><div class='resort-row1' id='divres_" + place.place_id + "'>";
                items += "<input type='hidden' id='isME" + place.place_id + "' value='0' class='resME'>";
                if (place.user_ratings_total) {
                    items += "<input type='hidden' class='reviews' value='" + place.user_ratings_total + "'/>"
                }
                else {
                    items += "<input type='hidden' class='reviews' value='No rev'/>"
                }
                if (place.price_level) {
                    items += "<input type='hidden' class='priceLev' value='" + place.price_level + "'/>"
                }
                else {
                    items += "<input type='hidden' class='priceLev' value='0'/>"
                }
                if (place.website) {
                    items += "<input type='hidden' class='webhidden' value='" + place.website + "'>";
                }
                else {
                    items += "<input type='hidden' class='webhidden' value='No site'>";
                }
                items += "<figure class='resort-img'><div class='tooltip'>Drag to add</div><img src='" + place.photos[0].getUrl({ 'maxWidth': 182, 'maxHeight': 120 }) + "' alt='' id='img_" + place.place_id + "' />";
                items +="</figure>";
                items += "<div class='resort-details'><div class='r-left'>";
                items += "<h3 id='h" + place.place_id + "'>" + place.name + "  </h3><br/>";
                items += "<p style='height:40px; overflow-y:hidden;' id='p" + place.place_id + "'>" + place.vicinity + "</p>";
                if (place.website) {
                items += "<p style='height:18px; overflow-y:hidden;width:100%;text-overflow: ellipsis;white-space: nowrap;overflow: hidden;' id='web" + place.place_id + "'><b>Website : </b><a target='_blank' href='" + place.website + "' style='text-decoration: underline;'>" + place.website + "</a></p>";
            }
            items += "<ul id='rat" + place.place_id + "'><b>Ratings : </b>";
            for (var loopvar_rate = 1; loopvar_rate <= 5; loopvar_rate++) {
                if (rating >= loopvar_rate) {
                    items += "<li><img src='../Content/images/tower_full.png' /></li>";
                }
                else if (rating > (loopvar_rate - 1) && rating < loopvar_rate) {
                    items += "<li><img src='../Content/images/tower_half.png' /></li>";
                }
                else {
                    items += "<li><img src='../Content/images/tower_grey_full.png' /></li>";
                }
            }
            if (place.user_ratings_total) {
                //items += "<li>(" + place.user_ratings_total + " reviews)</li>";
            }
            items += "</ul>";
            if (place.price_level) {
                if (place.price_level == 0) {
                    items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b> $</p> ";
                }
                else if (place.price_level == 1) {
                    items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b> $</p> ";
                }
                else if (place.price_level == 2) {
                    items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b>  $$</p> ";
                }
                else if (place.price_level == 3) {
                    items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b>  $$$</p> ";
                }
                else if (place.price_level == 4) {
                    items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b>  $$$$</p> ";
                }
            }
            else
            {
                items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b> $</p> ";
            }
                items += "<div class='book-add'><span><a href='#' id='inner" + place.place_id + "' class='add-trip' onclick='return RestaurantToleft(\"" + place.place_id + "\");'>In the trip</a></span></div></div> ";
                items += "<div class='r-right'>";
                items += " <ul><li><a href='#'><img src='../Content/images/del-icon.png' alt='' class='del-show' /></a></li>";
                items += "<li><a  class='fancybox' href='#infowindow' onclick='return info(\"" + place.place_id + "\",\"" + place.geometry.location.lat() + "\",\"" + place.geometry.location.lng() + "\",\"" + refkeywords + "\");'><img src='../Content/images/info-icon.png' alt='' class='example1demo' /></a></li></ul>";
                items += "</div></div></div>";
                items += "</aside>";
                $('#rhaa1').append(items);
            }
        }
        else {
            var rating = Math.round(place.rating);
            var items = '';
            if (place.rating) {
                rating = place.rating;
                var ratefilter = parseInt(place.rating);
                items += "<div id='rh" + place.place_id + "'></div><input type='hidden' class='hide'  id=" + place.place_id + " value='" + refkeywords + "'><input type='hidden' id='rates_" + place.place_id + "' value='" + rating + "'><input type='hidden' id='ratefilter_" + place.place_id + "' value='" + ratefilter + "'>";
                items += "<input type='hidden' id='latarray" + place.place_id + "' value='" + place.geometry.location.lat() + "'><input type='hidden' id='lngarray" + place.place_id + "' value='" + place.geometry.location.lng() + "'>";
                items += "<aside class='resort-row' id='res_" + place.place_id + "'><div class='resort-row1' id='divres_" + place.place_id + "'>";
                items += "<input type='hidden' id='isME" + place.place_id + "' value='0' class='resME'>";
                if (place.user_ratings_total) {
                    items += "<input type='hidden' class='reviews' value='" + place.user_ratings_total + "'/>"
                }
                else {
                    items += "<input type='hidden' class='reviews' value='No rev'/>"
                }
                if (place.price_level) {
                    items += "<input type='hidden' class='priceLev' value='" + place.price_level + "'/>"
                }
                else {
                    items += "<input type='hidden' class='priceLev' value='0'/>"
                }
                if (place.website) {
                    items += "<input type='hidden' class='webhidden' value='" + place.website + "'>";
                }
                else {
                    items += "<input type='hidden' class='webhidden' value='No site'>";
                }
                items += "<figure class='resort-img'><div class='tooltip'>Drag to add</div><img src='../Content/images/AppImages/attractions.jpg' alt='' id='img_" + place.place_id + "' />";
                items += "</figure>";
                items += "<div class='resort-details'><div class='r-left'>";
                items += "<h3 id='h" + place.place_id + "'>" + place.name + "  </h3><br/>";
                items += "<p style='height:40px; overflow-y:hidden;' id='p" + place.place_id + "'>" + place.vicinity + "</p>";
                if (place.website) {
                    items += "<p style='height:18px; overflow-y:hidden;width:100%;text-overflow: ellipsis;white-space: nowrap;overflow: hidden;' id='web" + place.place_id + "'><b>Website : </b><a target='_blank' href='" + place.website + "' style='text-decoration: underline;'>" + place.website + "</a></p>";
                }
                items += "<ul id='rat" + place.place_id + "'><b>Ratings : </b>";
                for (var loopvar_rate = 1; loopvar_rate <= 5; loopvar_rate++) {
                    if (rating >= loopvar_rate) {
                        items += "<li><img src='../Content/images/tower_full.png' /></li>";
                    }
                    else if (rating > (loopvar_rate - 1) && rating < loopvar_rate) {
                        items += "<li><img src='../Content/images/tower_half.png' /></li>";
                    }
                    else {
                        items += "<li><img src='../Content/images/tower_grey_full.png' /></li>";
                    }
                }
                if (place.user_ratings_total) {
                    //items += "<li>(" + place.user_ratings_total + " reviews)</li>";
                }
                items += "</ul>";
                if (place.price_level) {
                    if (place.price_level == 0) {
                        items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b> $</p> ";
                    }
                    else if (place.price_level == 1) {
                        items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b> $</p> ";
                    }
                    else if (place.price_level == 2) {
                        items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b>  $$</p> ";
                    }
                    else if (place.price_level == 3) {
                        items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b>  $$$</p> ";
                    }
                    else if (place.price_level == 4) {
                        items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b>  $$$$</p> ";
                    }
                }
                else {
                    items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b> $</p> ";
                }
                items += "<div class='book-add'><span><a href='#' id='inner" + place.place_id + "' class='add-trip' onclick='return RestaurantToleft(\"" + place.place_id + "\");'>In the trip</a></span></div></div> ";
                items += "<div class='r-right'>";
                items += " <ul><li><a href='#'><img src='../Content/images/del-icon.png' alt='' class='del-show' /></a></li>";
                items += "<li><a  class='fancybox' href='#infowindow' onclick='return info(\"" + place.place_id + "\",\"" + place.geometry.location.lat() + "\",\"" + place.geometry.location.lng() + "\",\"" + refkeywords + "\");'><img src='../Content/images/info-icon.png' alt='' class='example1demo' /></a></li></ul>";
                items += "</div></div></div>";
                items += "</aside>";
                $('#rhaa1').append(items);
            }
        }
    }
    else {
        var value = $.isArray(place.photos, place);

        if (value == true) {
            var rating = Math.round(place.rating);
            var items = '';
            if (place.rating) {
                rating = place.rating;
                var ratefilter = parseInt(place.rating);
                items += "<div id='rh" + place.place_id + "'></div><input type='hidden' class='hide'  id=" + place.place_id + " value='" + refkeywords + "'><input type='hidden' id='rates_" + place.place_id + "' value='" + rating + "'><input type='hidden' id='ratefilter_" + place.place_id + "' value='" + ratefilter + "'>";
                items += "<input type='hidden' id='latarray" + place.place_id + "' value='" + place.geometry.location.lat() + "'/><input type='hidden' id='lngarray" + place.place_id + "' value='" + place.geometry.location.lng() + "'/>";
                items += "<aside class='resort-row' id='res_" + place.place_id + "'><div class='resort-row1' id='divres_" + place.place_id + "'>";
                items += "<input type='hidden' id='isME" + place.place_id + "' value='0' class='resME'>";
                if (place.user_ratings_total) {
                    items += "<input type='hidden' class='reviews' value='" + place.user_ratings_total + "'/>"
                }
                else {
                    items += "<input type='hidden' class='reviews' value='No rev'/>"
                }
                if (place.price_level) {
                    items += "<input type='hidden' class='priceLev' value='" + place.price_level + "'/>"
                }
                else {
                    items += "<input type='hidden' class='priceLev' value='0'/>"
                }
                if (place.website) {
                    items += "<input type='hidden' class='webhidden' value='" + place.website + "'>";
                }
                else {
                    items += "<input type='hidden' class='webhidden' value='No site'>";
                }
                items += "<figure class='resort-img'><div class='tooltip'>Drag to add</div><img src='" + place.photos[0].getUrl({ 'maxWidth': 182, 'maxHeight': 120 }) + "' alt='' id='img_" + place.place_id + "' />";
                items += "</figure>";
                items += "<div class='resort-details'><div class='r-left'>";
                items += "<h3 id='h" + place.place_id + "'>" + place.name + "  </h3><br/>";
                items += "<p style='height:40px; overflow-y:hidden;' id='p" + place.place_id + "'>" + place.vicinity + "</p>";
                if (place.website) {
                    items += "<p style='height:18px; overflow-y:hidden;width:100%;text-overflow: ellipsis;white-space: nowrap;overflow: hidden;' id='web" + place.place_id + "'><b>Website : </b><a target='_blank' href='" + place.website + "' style='text-decoration: underline;'>" + place.website + "</a></p>";
                }
                items += "<ul id='rat" + place.place_id + "'><b>Ratings : </b>";
                for (var loopvar_rate = 1; loopvar_rate <= 5; loopvar_rate++) {
                    if (rating >= loopvar_rate) {
                        items += "<li><img src='../Content/images/tower_full.png' /></li>";
                    }
                    else if (rating > (loopvar_rate - 1) && rating < loopvar_rate) {
                        items += "<li><img src='../Content/images/tower_half.png' /></li>";
                    }
                    else {
                        items += "<li><img src='../Content/images/tower_grey_full.png' /></li>";
                    }
                }
                if (place.user_ratings_total) {
                    //items += "<li>(" + place.user_ratings_total + " reviews)</li>";
                }
                items += "</ul>";
                if (place.price_level) {
                    if (place.price_level == 0) {
                        items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b> $</p>";
                    }
                    else if (place.price_level == 1) {
                        items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b> $</p> ";
                    }
                    else if (place.price_level == 2) {
                        items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b>  $$</p> ";
                    }
                    else if (place.price_level == 3) {
                        items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b>  $$$</p> ";
                    }
                    else if (place.price_level == 4) {
                        items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b>  $$$$</p> ";
                    }
                }
                else {
                    items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b> $</p> ";
                }
                items += "<div class='book-add'><span><a href='#' id='inner" + place.place_id + "' class='add-trip' onclick='return RestaurantToleft(\"" + place.place_id + "\");'>Add Trip</a></span></div></div> ";
                items += "<div class='r-right'>";
                items += " <ul><li><a href='#'><img src='../Content/images/del-icon.png' alt='' class='del-show' /></a></li>";
                items += "<li><a  class='fancybox' href='#infowindow' onclick='return info(\"" + place.place_id + "\",\"" + place.geometry.location.lat() + "\",\"" + place.geometry.location.lng() + "\",\"" + refkeywords + "\");'><img src='../Content/images/info-icon.png' alt='' class='example1demo' /></a></li></ul>";
                items += "</div></div></div>";
                items += "</aside>";
                $('#rhaa1').append(items);
            }
        }
        else {
            var rating = Math.round(place.rating);
            var items = '';
            if (place.rating) {
                rating = place.rating;
                var ratefilter = parseInt(place.rating);
                items += "<div id='rh" + place.place_id + "'></div><input type='hidden' class='hide'  id=" + place.place_id + " value='" + refkeywords + "'><input type='hidden' id='rates_" + place.place_id + "' value='" + rating + "'> <input type='hidden' id='ratefilter_" + place.place_id + "' value='" + ratefilter + "'>";
                items += "<input type='hidden' id='latarray" + place.place_id + "' value='" + place.geometry.location.lat() + "'><input type='hidden' id='lngarray" + place.place_id + "' value='" + place.geometry.location.lng() + "'>";
                items += "<aside class='resort-row' id='res_" + place.place_id + "'><div class='resort-row1' id='divres_" + place.place_id + "'>";
                items += "<input type='hidden' id='isME" + place.place_id + "' value='0' class='resME'>";
                if (place.user_ratings_total) {
                    items += "<input type='hidden' class='reviews' value='" + place.user_ratings_total + "'/>"
                }
                else {
                    items += "<input type='hidden' class='reviews' value='No rev'/>"
                }
                if (place.price_level) {
                    items += "<input type='hidden' class='priceLev' value='" + place.price_level + "'/>"
                }
                else {
                    items += "<input type='hidden' class='priceLev' value='0'/>"
                }
                if (place.website) {
                    items += "<input type='hidden' class='webhidden' value='" + place.website + "'>";
                }
                else {
                    items += "<input type='hidden' class='webhidden' value='No site'>";
                }
                items += "<figure class='resort-img'><div class='tooltip'>Drag to add</div><img src='../Content/images/AppImages/attractions.jpg' alt='' id='img_" + place.place_id + "' />";
                items += "</figure>";
                items += "<div class='resort-details'><div class='r-left'>";
                items += "<h3 id='h" + place.place_id + "'>" + place.name + "  </h3><br/>";
                items += "<p style='height:40px; overflow-y:hidden;' id='p" + place.place_id + "'>" + place.vicinity + "</p>";
                if (place.website) {
                    items += "<p style='height:18px; overflow-y:hidden;width:100%;text-overflow: ellipsis;white-space: nowrap;overflow: hidden;' id='web" + place.place_id + "'><b>Website : </b><a target='_blank' href='" + place.website + "' style='text-decoration: underline;'>" + place.website + "</a></p>";
                }
                items += "<ul id='rat" + place.place_id + "'><b>Ratings : </b>";
                for (var loopvar_rate = 1; loopvar_rate <= 5; loopvar_rate++) {
                    if (rating >= loopvar_rate) {
                        items += "<li><img src='../Content/images/tower_full.png' /></li>";
                    }
                    else if (rating > (loopvar_rate - 1) && rating < loopvar_rate) {
                        items += "<li><img src='../Content/images/tower_half.png' /></li>";
                    }
                    else {
                        items += "<li><img src='../Content/images/tower_grey_full.png' /></li>";
                    }
                }
                if (place.user_ratings_total) {
                    //items += "<li>(" + place.user_ratings_total + " reviews)</li>";
                }
                items += "</ul>";
                if (place.price_level) {
                    if (place.price_level == 0) {
                        items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b> $</p> ";
                    }
                    else if (place.price_level == 1) {
                        items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b> $</p> ";
                    }
                    else if (place.price_level == 2) {
                        items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b>  $$</p> ";
                    }
                    else if (place.price_level == 3) {
                        items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b>  $$$</p> ";
                    }
                    else if (place.price_level == 4) {
                        items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b>  $$$$</p> ";
                    }
                }
                else {
                    items += "<p style='height: 18px;' id='price" + place.place_id + "'><b> Price Level:</b> $</p> ";
                }
                items += "<div class='book-add'><span><a href='#' id='inner" + place.place_id + "' class='add-trip' onclick='return RestaurantToleft(\"" + place.place_id + "\");'>Add Trip</a></span></div></div> ";
                items += "<div class='r-right'>";
                items += " <ul><li><a href='#'><img src='../Content/images/del-icon.png' alt='' class='del-show' /></a></li>";
                items += "<li><a  class='fancybox' href='#infowindow' onclick='return info(\"" + place.place_id + "\",\"" + place.geometry.location.lat() + "\",\"" + place.geometry.location.lng() + "\",\"" + refkeywords + "\");'><img src='../Content/images/info-icon.png' alt='' class='example1demo' /></a></li></ul>";
                items += "</div></div></div>";
                items += "</aside>";
                $('#rhaa1').append(items);
            }
        }
    }
}

/**********manual entry filter*************/

function filterManualRestTyp(kywrds)
{
    var tmpdata = [],notexst=0;
    for (var indx = 0, lmt = manualdata.length; indx < lmt; indx++) {
        if (manualdata[indx].ctyName == $('#selectedcity').html()) {
            tmpdata = manualdata[indx].restdata;
            break;
        }
    }
    if (tmpdata != "No Result") {
        for (var indx1 = 0, lmt1 = tmpdata.length; indx1 < lmt1; indx1++) {
            if (tmpdata[indx1].Type.toLowerCase() == kywrds) {
                var rating = tmpdata[indx1].Rating;
                var ratefilter = parseInt(tmpdata[indx1].Rating);
                var items = "<div id='rh" + tmpdata[indx1].Id + "'></div><input type='hidden' class='hide'  id=" + tmpdata[indx1].Id + " value='" + kywrds + "'><input type='hidden' id='rates_" + tmpdata[indx1].Id + "' value='" + rating + "'><input type='hidden' id='ratefilter_" + tmpdata[indx1].Id + "' value='" + ratefilter + "'><input type='hidden' id='latarray" + tmpdata[indx1].Id + "' value='" + tmpdata[indx1].RestLat + "'><input type='hidden' id='lngarray" + tmpdata[indx1].Id + "' value='" + tmpdata[indx1].RestLng + "'><aside class='resort-row' id='res_" + tmpdata[indx1].Id + "'><div class='resort-row1' id='divres_" + tmpdata[indx1].Id + "'>";
                items += "<input type='hidden' id='isME" + tmpdata[indx1].Id + "' value='1' class='resME'>";
                items += "<input type='hidden' class='reviews' value='0'/>";
                items += "<input type='hidden' class='priceLev' value='" + tmpdata[indx1].Price + "'/>";
                items += "<input type='hidden' class='webhidden' value='" + tmpdata[indx1].Website + "'>";
                items += "<figure class='resort-img'><img src='../Content/images/RestaurantImage/" + tmpdata[indx1].Photo + "' alt='' id='img_" + tmpdata[indx1].Id + "' />";
                items += "</figure>";
                items += "<div class='resort-details'><div class='r-left'>";
                items += "<h3 id='h" + tmpdata[indx1].Id + "'>" + tmpdata[indx1].Name + "  </h3><br/>";
                items += "<p style='height:40px; overflow-y:hidden;' id='p" + tmpdata[indx1].Id + "'>" + tmpdata[indx1].Street + "</p>";
                items += "<p style='height:18px; overflow-y:hidden;width:100%;text-overflow: ellipsis;white-space: nowrap;overflow: hidden;' id='web" + tmpdata[indx1].Id + "'><b>Website : </b><a target='_blank' href='" + tmpdata[indx1].Website + "' style='text-decoration: underline;'>" + tmpdata[indx1].Website + "</a></p>";
                items += "<ul id='rat" + tmpdata[indx1].Id + "'><b>Ratings : </b>";
                for (var loopvar_rate = 1; loopvar_rate <= 5; loopvar_rate++) {
                    if (rating >= loopvar_rate) {
                        items += "<li><img src='../Content/images/coffee_full.png'/></li>";
                    }
                    else if (rating > (loopvar_rate - 1) && rating < loopvar_rate) {
                        items += "<li><img src='../Content/images/coffee_half.png'/></li>";
                    }
                    else {
                        items += "<li><img src='../Content/images/coffee_grey_full.png'/></li>";
                    }
                }
                items += "</ul>";
                if (tmpdata[indx1].Price) {
                    if (tmpdata[indx1].Price == 0) {
                        items += "<p style='height: 18px;' id='price" + tmpdata[indx1].Id + "'><b> Price Level:</b>$</p>";
                    }
                    else if (tmpdata[indx1].Price == 1) {
                        items += "<p style='height: 18px;' id='price" + tmpdata[indx1].Id + "'><b> Price Level:</b>$</p>";
                    }
                    else if (tmpdata[indx1].Price == 2) {
                        items += "<p style='height: 18px;' id='price" + tmpdata[indx1].Id + "'><b> Price Level:</b>$$</p>";
                    }
                    else if (tmpdata[indx1].Price == 3) {
                        items += "<p style='height: 18px;' id='price" + tmpdata[indx1].Id + "'><b> Price Level:</b>$$$</p>";
                    }
                    else if (tmpdata[indx1].Price == 4) {
                        items += "<p style='height: 18px;' id='price" + tmpdata[indx1].Id + "'><b> Price Level:</b>$$$$</p>";
                    }
                }
                else {
                    items += "<p style='height: 18px;' id='price" + tmpdata[indx1].Id + "'><b> Price Level:</b>$</p>";
                }
                items += "<div class='book-add'><span><a href='#' id='inner" + tmpdata[indx1].Id + "' class='add-trip' onclick='return RestaurantToleft(\"" + tmpdata[indx1].Id + "\");'>Add trip</a></span></div></div> ";
                items += "<div class='r-right'>";
                items += " <ul><li><a href='#'><img src='../Content/images/del-icon.png' alt='' class='del-show' /></a></li>";
                items += "<li><a  class='fancybox' href='#infowindow' onclick='return infomanual(\"3\",\"" + tmpdata[indx1].Id + "\");'><img src='../Content/images/info-icon.png' alt='' class='example1demo' /></a></li></ul>";
                items += "</div></div></div>";
                items += "</aside>";
                $('#rhaa1').append(items);
            }
        }
    }
}

function filterManualAttrTyp(kywrds) {
    
    var tmpdata = [], notexst = 0;
    for (var indx = 0, lmt = manualdata.length; indx < lmt; indx++) {
        if (manualdata[indx].ctyName == $('#selectedcity').html()) {
            tmpdata = manualdata[indx].attrdata;
            break;
        }
    }
    if (tmpdata != "No Result") {
        for (var indx1 = 0, lmt1 = tmpdata.length; indx1 < lmt1; indx1++) {
            if (tmpdata[indx1].Type.toLowerCase() == kywrds) {
                var items = "";
                    var rating = tmpdata[indx1].rating;
                    var ratefilter = parseInt(rating);
                    items += "<div id='rh" + tmpdata[indx1].Id + "'></div><input type='hidden' class='hide'  id=" + tmpdata[indx1].Id + " value='" + kywrds + "'><input type='hidden' id='rates_" + tmpdata[indx1].Id + "' value='" + rating + "'><input type='hidden' id='ratefilter_" + tmpdata[indx1].Id + "' value='" + ratefilter + "'>";
                    items += "<input type='hidden' id='latarray" + tmpdata[indx1].Id + "' value='" + tmpdata[indx1].AttrLat + "'><input type='hidden' id='lngarray" + tmpdata[indx1].Id + "' value='" + tmpdata[indx1].AttrLng + "'>";
                    items += "<aside class='resort-row' id='res_" + tmpdata[indx1].Id + "'><div class='resort-row1' id='divres_" + tmpdata[indx1].Id + "'>";
                    items += "<input type='hidden' id='isME" + tmpdata[indx1].Id + "' value='1' class='resME'>";
                    items += "<input type='hidden' class='reviews' value='0'/>";
                    items += "<input type='hidden' class='priceLev' value='" + tmpdata[indx1].Price + "'/>";
                    items += "<input type='hidden' class='webhidden' value='" + tmpdata[indx1].Website + "'>";
                    items += "<figure class='resort-img'><div class='tooltip'>Drag to add</div><img src='../Content/images/AttractionImage/" + tmpdata[indx1].Photo + "' alt='' id='img_" + tmpdata[indx1].Id + "' />";
                    items += "</figure>";
                    items += "<div class='resort-details'><div class='r-left'>";
                    items += "<h3 id='h" + tmpdata[indx1].Id + "'>" + tmpdata[indx1].Name + "  </h3><br/>";
                    items += "<p style='height:40px; overflow-y:hidden;' id='p" + tmpdata[indx1].Id + "'>" + tmpdata[indx1].Street + "</p>";
                    items += "<p style='height:18px; overflow-y:hidden;width:100%;text-overflow: ellipsis;white-space: nowrap;overflow: hidden;' id='web" + tmpdata[indx1].Id + "'><b>Website : </b><a target='_blank' href='" + tmpdata[indx1].Website + "' style='text-decoration: underline;'>" + tmpdata[indx1].Website + "</a></p>";
                    items += "<ul id='rat" + tmpdata[indx1].Id + "'><b>Ratings : </b>";
                    for (var loopvar_rate = 1; loopvar_rate <= 5; loopvar_rate++) {
                        if (rating >= loopvar_rate) {
                            items += "<li><img src='../Content/images/tower_full.png' /></li>";
                        }
                        else if (rating > (loopvar_rate - 1) && rating < loopvar_rate) {
                            items += "<li><img src='../Content/images/tower_half.png' /></li>";
                        }
                        else {
                            items += "<li><img src='../Content/images/tower_grey_full.png' /></li>";
                        }
                    }
                    items += "</ul>";
                    if (tmpdata[indx1].Price == 0) {
                        items += "<p style='height: 18px;' id='price" + tmpdata[indx1].Id + "'><b> Price Level:</b> $</p> ";
                    }
                    else if (tmpdata[indx1].Price == 1) {
                        items += "<p style='height: 18px;' id='price" + tmpdata[indx1].Id + "'><b> Price Level:</b> $</p> ";
                    }
                    else if (tmpdata[indx1].Price == 2) {
                        items += "<p style='height: 18px;' id='price" + tmpdata[indx1].Id + "'><b> Price Level:</b>  $$</p> ";
                    }
                    else if (tmpdata[indx1].Price == 3) {
                        items += "<p style='height: 18px;' id='price" + tmpdata[indx1].Id + "'><b> Price Level:</b>  $$$</p> ";
                    }
                    else if (tmpdata[indx1].Price == 4) {
                        items += "<p style='height: 18px;' id='price" + tmpdata[indx1].Id + "'><b> Price Level:</b>  $$$$</p> ";
                    }
                    items += "<div class='book-add'><span><a href='#' id='inner" + tmpdata[indx1].Id + "' class='add-trip' onclick='return RestaurantToleft(\"" + tmpdata[indx1].Id + "\");'>Add trip</a></span></div></div> ";
                    items += "<div class='r-right'>";
                    items += " <ul><li><a href='#'><img src='../Content/images/del-icon.png' alt='' class='del-show' /></a></li>";
                    items += "<li><a  class='fancybox' href='#infowindow' onclick='return infomanual(\"2\",\"" + tmpdata[indx1].Id + "\");'><img src='../Content/images/info-icon.png' alt='' class='example1demo' /></a></li></ul>";
                    items += "</div></div></div>";
                    items += "</aside>";
                    $('#rhaa1').append(items);
            }
        }
    }
}

function getMnalAttrTyp()
{
    var kywrd = new Array();
    var existsVal = 0;
    var tmpdata = [];
    var cty = $('#selectedcity').html();
    for (var indx = 0, lmt = manualdata.length; indx < lmt; indx++) {
        if (manualdata[indx].ctyName == cty) {
            if (manualdata[indx].attrdata) {
                existsVal = 1;
                tmpdata = manualdata[indx].attrdata;
                break;
            }
        }
    }
    if(existsVal==1)
    {
        var Attraction = new Array();
        if (prevAttrTypes != "") {
            Attraction = prevAttrTypes.split(",");
            for (var loopvar_attraction = 0, AttractionsLength = Attraction.length; loopvar_attraction < AttractionsLength; loopvar_attraction++) {
                if (Attraction[loopvar_attraction] != "Nooption" && tmpdata != "No Result") {
                    filterManualAttrTyp(Attraction[loopvar_attraction]);
                }
                else {
                    GetManualEntry('2', $('#latarray').val(), $('#lngarray').val());
                }
            }
        }
        else
        {
            GetManualEntry('2', $('#latarray').val(), $('#lngarray').val());
        }
    }
    else {
        GetManualEntry('2', $('#latarray').val(), $('#lngarray').val());
    }
}

function getMnalRestTyp() {
    var kywrd = new Array();
    var existsVal = 0;
    var tmpdata = [];
    var cty = $('#selectedcity').html();
    for (var indx = 0, lmt = manualdata.length; indx < lmt; indx++) {
        if (manualdata[indx].ctyName == cty) {
            if (manualdata[indx].restdata) {
                existsVal = 1;
                tmpdata = manualdata[indx].restdata;
                break;
            }
        }
    }
    if (existsVal == 1) {
        var restaurant = new Array();
        if (prevRestTypes != "") {
            restaurant = prevRestTypes.split(",");
            for (var loopvar_rest = 0, RestaurantLength = restaurant.length; loopvar_rest < RestaurantLength; loopvar_rest++) {
                if (restaurant[loopvar_rest] != "Nooption" && tmpdata != "No Result") {
                    filterManualRestTyp(restaurant[loopvar_rest]);
                }
                else {
                    GetManualEntry('3', $('#latarray').val(), $('#lngarray').val());
                }
            }
        }
        else {
            GetManualEntry('3', $('#latarray').val(), $('#lngarray').val());
        }
    }
    else {
        GetManualEntry('3', $('#latarray').val(), $('#lngarray').val());
    }
}

function getMnalHotelType() {
    var existsVal = 0;
    var tmpdata = [], filtered = [], filtStr = "";
    var cty = $('#selectedcity').html();
    for (var indx = 0, lmt = manualdata.length; indx < lmt; indx++) {
        if (manualdata[indx].ctyName == cty) {
            if (manualdata[indx].hotldata) {
                existsVal = 1;
                tmpdata = manualdata[indx].hotldata;
                break;
            }
        }
    }
    $("input:checkbox[name=hotel_class]:checked").each(function () {
        if (filtStr != "")
        {
            filtStr += " || ";
        }
        if ($(this).val() == 1) {
            filtStr += "el.Hclass < 2 && el.Hclass >= 1";
        }
        else if ($(this).val() == 2) {
            filtStr += "el.Hclass < 3 && el.Hclass >= 2";
        }
        else if ($(this).val() == 3) {
            filtStr += "el.Hclass < 4 && el.Hclass >= 3";
        }
        else if ($(this).val() == 4) {
            filtStr += "el.Hclass < 5 && el.Hclass >= 4";
        }
        else if ($(this).val() == 5) {
            filtStr += "el.Hclass == 5";
        }
    });

    var slival1 = $("#slider-range").slider("values", 0);
    var slival2 = $("#slider-range").slider("values", 1);
    if (filtStr != "") {
        filtStr += " && ";
    }
    filtStr += "(el.Price>=" + slival1 + " && el.Price<=" + slival2 + ")";

    if (existsVal == 1) {
        if (tmpdata != "No Result") {
            filtered = tmpdata.filter(function (el) {
                return eval(filtStr);
            });
            for (var indx1 = 0, lmt1 = filtered.length; indx1 < lmt1; indx1++) {
                var items = '';
                items += "<aside class='resort-row' id='res_" + filtered[indx1].Id + "'><div class='resort-row1' id='divres_" + filtered[indx1].Id + "'>";
                items += "<input type='hidden' id='" + filtered[indx1].Id + "' value='" + filtered[indx1].Id + "' class='hide'><input type='hidden' id='latarray" + filtered[indx1].Id + "' value='" + filtered[indx1].HotlLat + "' class='reslat'/><input type='hidden' id='lngarray" + filtered[indx1].Id + "' value='" + filtered[indx1].HotlLng + "' class='reslng'/>";
                items += "<input type='hidden' id='isME" + filtered[indx1].Id + "' value='1' class='resME'>";
                items += "<input type='hidden' id='htlRt" + filtered[indx1].Id + "' value='" + filtered[indx1].Rating + "' class='htlrt'><input type='hidden' id='htlCls" + filtered[indx1].Id + "' value='" + filtered[indx1].Hclass + "' class='htlCls'>";
                items += "<input type='hidden' id='htlPrc" + filtered[indx1].Id + "' value='" + filtered[indx1].Price + "' class='htlPrc'>";
                items += "<figure class='resort-img'>";
                items += "<div class='tooltip'>Drag to add</div><img src='../Content/images/HotelImages/" + filtered[indx1].Photo + "' alt=''/>";
                items += "<span>Night from $<strong id='s" + filtered[indx1].Id + "'>" + filtered[indx1].Price + "</strong></span></figure>";
                items += "<div class='resort-details'><div class='r-left'><span class='hotel_identify' style='display:none'></span>";
                items += "<h3 id='h" + filtered[indx1].Id + "'>" + filtered[indx1].Name + "  </h3><ul><b>Hotel class :</b>";
                for (var loopvar_hc = 1; loopvar_hc <= 5; loopvar_hc++) {
                    if (filtered[indx1].Hclass >= loopvar_hc) {
                        items += "<li><img src='../Content/images/yellow-star1.png' /></li>";
                    }
                    else if (filtered[indx1].Hclass > (loopvar_hc - 1) && filtered[indx1].Hclass < loopvar_hc) {
                        items += "<li><img src='../Content/images/yellow-gray-starNew.png' /></li>";
                    }
                    else {
                        items += "<li><img src='../Content/images/grey-starNew.png' /></li>";
                    }
                }
                items += "(" + (parseFloat(filtered[indx1].Hclass)).toFixed(1) + ")</ul>";
                items += "<ul><b>Rating :</b>"
                for (var loopvar_rate = 1; loopvar_rate <= 5; loopvar_rate++) {
                    if (filtered[indx1].Rating >= loopvar_rate) {
                        items += "<li><img src='../Content/images/hotel-color.png' /></li>";
                    }
                    else if (filtered[indx1].Rating > (loopvar_rate - 1) && filtered[indx1].Rating < loopvar_rate) {
                        items += "<li><img src='../Content/images/hotel-half.png' /></li>";
                    }
                    else {
                        items += "<li><img src='../Content/images/hotel-gray.png' /></li>";
                    }
                }
                items += "<li>(" + (parseFloat(filtered[indx1].Rating)).toFixed(1) + "/5)</li></ul>";
                items += "<p id='pho_" + filtered[indx1].Id + "' style='height:40px; overflow-y:hidden;'>" + filtered[indx1].desc + "</p>";
                //items += "<p style='height:40px; overflow-y:hidden;' id='p" + filtered[indx1].Id + "'>" + filtered[indx1].Street + "</p>";
                //***Modified by Yuvapriya
                //items += "<div class='book-add'><span><a href='#' id='inner" + filtered[indx1].Id + "' class='add-trip' onclick='return hotelsToleft(\"" + filtered[indx1].Id + "\");'>Add Trip</a></span><span id='book" + filtered[indx1].Id + "' style='display:none;'><a href='#' class='book'>Book</a></span></div></div> ";
                items += "<div class='book-add'><span><a href='#' id='inner" + filtered[indx1].Id + "' class='add-trip' onclick='return hotelsToleft(\"" + filtered[indx1].Id + "\");'>Add Trip</a></span></div></div> ";
                //***End***
                items += "<div class='r-right'>";
                items += " <ul><li><a href='#'><img src='../Content/images/del-icon.png' alt='' class='del-show' onclick=''/></a></li>";
                items += "<li><a  class='fancybox' href='#infohomanu_" + filtered[indx1].Id + "'><img src='../Content/images/info-icon.png' alt='' class='example1demo' /></a></li></ul>";
                items += "</div></div></div>";
                items += "</aside>";
                items += "<div id='infohomanu_" + filtered[indx1].Id + "' style='display:none'><div class='info-left popup-bg' style='width: 112%;'><figure class='resort-img'><img src='../Content/images/HotelImages/" + filtered[indx1].Photo + "' alt='' /><span>Night from <strong>$" + filtered[indx1].Price + "</strong></span></figure>";
                items += "<h3>" + filtered[indx1].Name + "</h3>";
                items += "<ul><b>Hotel class : </b>";
                for (var loopvar_hc = 1; loopvar_hc <= 5; loopvar_hc++) {
                    if (filtered[indx1].Hclass >= loopvar_hc) {
                        items += "<li><img src='../Content/images/yellow-star1.png' /></li>";
                    }
                    else if (filtered[indx1].Hclass > (loopvar_hc - 1) && filtered[indx1].Hclass < loopvar_hc) {
                        items += "<li><img src='../Content/images/yellow-gray-starNew.png' /></li>";
                    }
                    else {
                        items += "<li><img src='../Content/images/grey-starNew.png' /></li>";
                    }
                }
                items += "(" + (parseFloat(filtered[indx1].Hclass)).toFixed(1) + ")</ul>";
                items += "<ul><b>Rating :</b>";
                for (var loopvar_rate = 1; loopvar_rate <= 5; loopvar_rate++) {
                    if (filtered[indx1].Rating >= loopvar_rate) {
                        items += "<li><img src='../Content/images/hotel-color.png' /></li>";
                    }
                    else if (filtered[indx1].Rating > (loopvar_rate - 1) && filtered[indx1].Rating < loopvar_rate) {
                        items += "<li><img src='../Content/images/hotel-half.png' /></li>";
                    }
                    else {
                        items += "<li><img src='../Content/images/hotel-gray.png' /></li>";
                    }
                }
                items += "<li>(" + (parseFloat(filtered[indx1].Rating)).toFixed(1) + "/5)</li></ul>";
                items += "<p><b>Short Description : </b>" + filtered[indx1].desc + "</p>";
                items += "<p><b>Address : </b>" + filtered[indx1].Street + "</p>";
                items += "<p><b>Rate starts from : </b>$" + filtered[indx1].Price + "</p>";
                if (filtered[indx1].BookURL != null && filtered[indx1].BookURL != "null") {
                    items += "<p><b>For booking :</b>" + filtered[indx1].BookURL + "</p>";
                }
                if (filtered[indx1].Amenities != null) {
                    items += "<p><b>Hotel Amenities : </b>" + filtered[indx1].Amenities + "</p>";
                }
                items += "</div>";
                items += "</div>";
                if (items == "") {
                    getmanualEntryVal = 1;
                }
                if (apiStatus == 1 && getmanualEntryVal == 1) {
                    $('#rhaa1').html("No results found..");
                }
                $('#rhaa1').append(items);
            }
        }
        else {
            GetManualEntry('1', $('#latarray').val(), $('#lngarray').val());
        }
    }
    else {
        GetManualEntry('1', $('#latarray').val(), $('#lngarray').val());
    }
}

function sortHotels()
{
    var htl_forsort = [];
   // if (HtlSrtArr.length == 0 && prevHtlSrt != $("#hotel_sortid_select").val() && prevSugCity != $('#selectedcity').html()) {
        var Count = $('#rhaa1').find('aside').length;
        for (var htlsCount = 0; htlsCount < Count; htlsCount++) {
            var id = $('#rhaa1').find('.hide').eq(htlsCount).attr('id');
            var Rating = $('#htlRt' + id).val();
            var HtlClass = Math.round($('#htlCls' + id).val() * 10) / 10;
            var lat = $('#latarray' + id).val();
            var lng = $('#lngarray' + id).val();
            var isManual = $('#isME' + id).val();
            var Name = $('#h' + id).html();
            var Address = $('#pho_' + id).html();
            var Photo = $('#divres_' + id).find('.resort-img').find('img').attr('src');
            var Trip = $('#inner' + id).html();
            var priceLev = $('#htlPrc' + id).val();
            htl_forsort.push({
                plc_name: Name,
                plc_id: id,
                plc_rating: Rating,
                plc_lat: lat,
                plc_lng: lng,
                plc_address: Address,
                plc_photo: Photo,
                plc_trip: Trip,
                plc_price: priceLev,
                plc_pricFloat: parseFloat(priceLev),
                plc_manual: isManual,
                plc_cls: HtlClass
            });
        }
        var hotelsortval = $("#hotel_sortid_select").val();
        if (hotelsortval == "ALPHA") {
            htl_forsort.sort(function (a, b) {
                if (a.plc_name < b.plc_name)
                    return -1;
                if (a.plc_name > b.plc_name)
                    return 1;
                return 0;
            });
        }
        else if (hotelsortval == "ALPHA_REVERSE") {
            htl_forsort.sort(function (a, b) {
                if (a.plc_name > b.plc_name)
                    return -1;
                if (a.plc_name < b.plc_name)
                    return 1;
                return 0;
            });
        }
        else if (hotelsortval == "PRICE") {
            htl_forsort.sort(function (a, b) {
                if (a.plc_pricFloat < b.plc_pricFloat)
                    return -1;
                if (a.plc_pricFloat > b.plc_pricFloat)
                    return 1;
                return 0;
            });
        }
        else if (hotelsortval == "PRICE_REVERSE") {
            htl_forsort.sort(function (a, b) {
                if (a.plc_pricFloat > b.plc_pricFloat)
                    return -1;
                if (a.plc_pricFloat < b.plc_pricFloat)
                    return 1;
                return 0;
            });
        }
        else if (hotelsortval == "QUALITY_REVERSE") {
            htl_forsort.sort(function (a, b) {
                if (a.plc_cls < b.plc_cls)
                    return -1;
                if (a.plc_cls > b.plc_cls)
                    return 1;
                return 0;
            });
        }
        else if (hotelsortval == "QUALITY") {
            htl_forsort.sort(function (a, b) {
                if (a.plc_cls > b.plc_cls)
                    return -1;
                if (a.plc_cls < b.plc_cls)
                    return 1;
                return 0;
            });
        }
        else if (hotelsortval == "RATING") {
            htl_forsort.sort(function (a, b) {
                if (a.plc_rating > b.plc_rating)
                    return -1;
                if (a.plc_rating < b.plc_rating)
                    return 1;
                return 0;
            });
        }
        else if (hotelsortval == "RATING_REVERSE") {
            htl_forsort.sort(function (a, b) {
                if (a.plc_rating < b.plc_rating)
                    return -1;
                if (a.plc_rating > b.plc_rating)
                    return 1;
                return 0;
            });
        }
    //    HtlSrtArr = htl_forsort.slice();
    //}
    //else
    //{
    //    htl_forsort = HtlSrtArr.slice();
    //}
    $("#rhaa1").html("");
    if (htl_forsort.length) {
        $.each(htl_forsort, function (keys, values) {
            addtoleft_hotelsfull.push(htl_forsort[keys]);
            var items = "";
            var descri = "";
            items += "<aside class='resort-row' id='res_" + values.plc_id + "'><div class='resort-row1' id='divres_" + values.plc_id + "'>";
            items += "<input type='hidden' id='" + values.plc_id + "' value='" + values.plc_id + "' class='hide'><input type='hidden' id='latarray" + values.plc_id + "' value='" + values.plc_lat + "' class='reslat'/><input type='hidden' id='lngarray" + values.plc_id + "' value='" + values.plc_lng + "' class='reslng'/>";
            items += "<input type='hidden' id='isME" + values.plc_id + "' value='" + values.plc_manual + "' class='resME'>";
            items += "<input type='hidden' id='htlRt" + values.plc_id + "' value='0' class='htlrt'><input type='hidden' id='htlCls" + values.plc_id + "' value='" + values.plc_cls + "' class='htlCls'>";
            items += "<input type='hidden' id='htlPrc" + values.plc_id + "' value='" + values.plc_price + "' class='htlPrc'>";
            items += "<figure class='resort-img'>";
            items += "<div class='tooltip'>Drag to add</div><img src='" + values.plc_photo + "' alt='' />";
            items += "<span>Night from $<strong id='s" + values.plc_id + "'>" + values.plc_price + "</strong></span></figure>";
            items += "<div class='resort-details'><div class='r-left'><span class='hotel_identify' style='display:none'></span>";
            items += "<h3 id='h" + values.plc_id + "'>" + values.plc_name + "  </h3><ul><b>Hotel class :</b>";
            for (var loopvar_hc = 1; loopvar_hc <= 5; loopvar_hc++) {
                if (values.plc_cls >= loopvar_hc) {
                    items += "<li><img src='../Content/images/yellow-star1.png' /></li>";
                }
                else if (values.plc_cls > (loopvar_hc - 1) && values.plc_cls < loopvar_hc) {
                    items += "<li><img src='../Content/images/yellow-gray-starNew.png' /></li>";
                }
                else {
                    items += "<li><img src='../Content/images/grey-starNew.png' /></li>";
                }
            }
            items += "(" + values.plc_cls + ")</ul>";
           // var desc = (values.shortDescription).replace('&lt;p&gt;&lt;b&gt;Property Location&lt;/b&gt; &lt;br /&gt;', '') + "...";
            items += "<p id='pho_" + values.plc_id + "' style='height:40px; overflow-y:hidden;'>" + values.plc_address + "</p>";
            //***Modified by Yuvapriya
            //if (values.plc_manual == 1) {
            //    items += "<div class='book-add'><span><a href='#' id='inner" + values.plc_id + "' class='add-trip' onclick='return addtoleft(\"" + values.plc_id + "\");'>Add Trip</a></span><span id='book" + values.plc_id + "' style='display:none;'><a href='#' class='book'>Book</a></span></div></div> ";
            //}
            //else {
            //    items += "<div class='book-add'><span><a href='#' id='inner" + values.plc_id + "' class='add-trip'  onclick='return addtoleft(" + values.plc_id + ")'>Add Trip</a></span><span id='book" + values.plc_id + "' style='display:none;'><a href='#' class='book'>Book</a></span></div></div> ";
            //}
            if (values.plc_manual == 1) {
                items += "<div class='book-add'><span><a href='#' id='inner" + values.plc_id + "' class='add-trip' onclick='return addtoleft(\"" + values.plc_id + "\");'>Add Trip</a></span></div></div> ";
            }
            else {
                items += "<div class='book-add'><span><a href='#' id='inner" + values.plc_id + "' class='add-trip'  onclick='return addtoleft(" + values.plc_id + ")'>Add Trip</a></span></div></div> ";
            }
            //***End***
            items += "<div class='r-right'>";
            items += " <ul><li><a href='#'><img src='../Content/images/del-icon.png' alt='' class='del-show' onclick=''/></a></li>";
            if (values.plc_manual==1) {
                items += "<li><a  class='fancybox' href='#infowindow_hotels_activities' onclick='GetMnlHtlinfo(\"" + values.plc_id + "\")'><img src='../Content/images/info-icon.png' alt='' class='example1demo' /></a></li></ul>";
            }
            else {
                items += "<li><a  class='fancybox' href='#infowindow_hotels_activities' onclick='GetApiHtlinfo(\"" + values.plc_id + "\")'><img src='../Content/images/info-icon.png' alt='' class='example1demo' /></a></li></ul>";
            }
            items += "</div></div></div>";
            items += "</aside>";
            $('#rhaa1').append(items);
        });
        intheTripRHAA("Hotel");
    }
    else {
        apiStatus = 1;
        if (getmanualEntryVal == 1 && apiStatus == 1) {
            $('#rhaa1').html("No results Found..");
        }
    }
}

/**********on click rating type***********/

function chkresrating(id) {
    var rating = id;
    var value = $('#chkresrating' + id).val();
    if (value == 0) {
        $('#chkresrating' + id).val("1");
        var results = $('#rhaa1').find('.resort-row').length + $('#rhaa1').find('.resort-row2').length;
        for (var length = 0; length < results; length++) {
            var relid = $('#rhaa1').find('.hide').eq(length).attr("id");
            if ($('#ratefilter_' + relid).val() != rating) {
                $('#rhaa1').find('#res_' + relid).hide();
                $('#rhaa1').find('#rates_' + relid).hide();
                $('#rhaa1').find('#rh' + relid).hide();
                $('#rhaa1').find('#' + relid).hide();
                $('#rhaa1').find('#ratefilter_' + relid).hide();
            }
        }
    }
    else {
        $('#chkresrating' + id).val("0");
        var results = $('#rhaa1').find('.resort-row').length + $('#rhaa1').find('.resort-row2').length;
        for (var length = 0; length < results; length++) {
            var relid = $('#rhaa1').find('.hide').eq(length).attr("id");
            if ($('#ratefilter_' + relid).val() != rating) {
                $('#rhaa1').find('#res_' + relid).show();
                $('#rhaa1').find('#rates_' + relid).show();
                $('#rhaa1').find('#rh' + relid).show();
                $('#rhaa1').find('#' + relid).show();
                $('#rhaa1').find('#ratefilter_' + relid).show();
            }
        }
    }
}

function chkattrating(id) {
    var rating = id;
    var value = $('#chkattrating' + id).val();
    if (value == 0) {
        $('#chkattrating' + id).val("1");
        var results = $('#rhaa1').find('.resort-row').length + $('#rhaa1').find('.resort-row2').length;
        for (var length = 0; length < results; length++) {
            var relid = $('#rhaa1').find('.hide').eq(length).attr("id");
            if ($('#ratefilter_' + relid).val() != rating) {
                $('#rhaa1').find('#res_' + relid).hide();
                $('#rhaa1').find('#rh' + relid).hide();
                $('#rhaa1').find('#' + relid).hide();
            }
        }
    }
    else {
        $('#chkattrating' + id).val("0");
        var results = $('#rhaa1').find('.resort-row').length + $('#rhaa1').find('.resort-row2').length;
        for (var length = 0; length < results; length++) {
            var relid = $('#rhaa1').find('.hide').eq(length).attr("id");
            if ($('#ratefilter_' + relid).val() != rating) {
                $('#rhaa1').find('#res_' + relid).show();
                $('#rhaa1').find('#rh' + relid).show();
                $('#rhaa1').find('#' + relid).show();
            }
        }
    }
}

/***********on click rating type end***********/

function change_all_to_place(ui_id) {
    var split_ui_id = ui_id.split("_");
    var plcid = "";
    if (split_ui_id.length > 2) {
        split_ui_id[1] = split_ui_id[1] + "_" + split_ui_id[2];
    }
    if ($("#prefer").html() == "Hotel Preferences") {
        if ($("#isME" + split_ui_id[1]).val() == 0) {
            var place = addtoleft_hotelsfull.filter(function (obj) {
                return obj.hotelId == split_ui_id[1];
            });
            plcid = place[0].hotelId;
            var placediv1 = "<div class='place' id='place" + place[0].hotelId + "'><div class='place-details' id=" + place[0].hotelId + " style ='height:86px;'><input type='hidden' value='1' class='frmApi'/><figure class='place-img'>";

            var iurl = '"http://media3.expedia.com"' + place[0].thumbNailUrl + '"';

            var obj = new Image();
            obj.src = iurl;

            if (obj.complete) {
                placediv1 += "<img src='http://media3.expedia.com" + place[0].thumbNailUrl + "' alt='' /></figure><div class='place-cont'>";// Do something now you know the image exists.
                //alert('worked');
            } else {
                placediv1 += "<img src='../Content/images/HotelImages/af5e-hotel.jpg' alt='' /></figure><div class='place-cont'>";
                
                //alert('doesnt work');
            }

           
            placediv1 += "<div class='p-left'><h4>" + place[0].name + "</h4><p style='height:26px; overflow-y:hidden;'>" + place[0].address1 + "</p><input type='hidden' class='hidlat' value='" + place[0].latitude + "'/><input type='hidden' class='hidlng' value='" + place[0].longitude + "'/><span>Hotel.</span>";
            placediv1 += "</div><div class='p-right'>";
            placediv1 += "<ul><li><a href='#'><img src='../Content/images/del-icon.png' class='deleteplace' alt='' /></a></li><li><a href='#infohotel_" + place[0].hotelId + "' class='fancybox'><img src='../Content/images/info-icon.png' alt='' /></a></li> </ul>";
            var descri = "";
            if (place[0].Amenities_Provided) {
                var amenityvalues = place[0].Amenities_Provided;
                var amenityrev = amenityvalues.split("").reverse().join("");
                var lengthis = amenityvalues.length;
                var splitamenity = amenityrev.split("");
                var amlength = splitamenity.length;
                var multiam = 0;
                var amenitiesare = "";
                for (var ji = 0; ji < amlength; ji++) {
                    multiam = splitamenity[ji] * (Math.pow(2, ji));
                    if (multiam == 8) {
                        amenitiesare += "<li>Internet Access Available</li>";
                    }
                    else if (multiam == 16) {
                        amenitiesare += "<li>Kids' Activities</li>";
                    }
                    else if (multiam == 32) {
                        amenitiesare += "<li>Kitchen or Kitchenette</li>";
                    }
                    else if (multiam == 64) {
                        amenitiesare += "<li>Pets Allowed</li>";
                    }
                    else if (multiam == 128) {
                        amenitiesare += "<li>Pool</li>";
                    }
                    else if (multiam == 256) {
                        amenitiesare += "<li>Restaurant On-site</li>";
                    }
                    else if (multiam == 512) {
                        amenitiesare += "<li>Spa On-site</li>";
                    }
                    else if (multiam == 2048) {
                        amenitiesare += "<li>Breakfast</li>";
                    }
                    else if (multiam == 16384) {
                        amenitiesare += "<li>Parking</li>";
                    }
                    else if (multiam == 32768) {
                        amenitiesare += "<li>Room Service</li>";
                    }
                    else if (multiam == 8388608) {
                        amenitiesare += "<li>Free Airport Shuttle</li>";
                    }
                    else if (multiam == 16777216) {
                        amenitiesare += "<li>Indoor Pool</li>";
                    }
                    else if (multiam == 33554432) {
                        amenitiesare += "<li>Outdoor Pool</li>";
                    }
                }
            }
            placediv1 += "<div id='infohotel_" + place[0].hotelId + "' class='info-left popup-bg' style='display:none;'>";
            placediv1 += "<figure class='resort-img'><img src='http://media3.expedia.com" + place[0].thumbNailUrl + "' alt='' /><span>Night from <strong>$149</strong></span></figure>";
            placediv1 += "<h3>" + place[0].name + "</h3>";
            placediv1 += "<ul><b>Ratings : </b>";
            for (var loopvar2 = 0; loopvar2 < 5; loopvar2++) {
                if (place[0].tripAdvisorRating < (loopvar2 + 0.5)) {
                    placediv1 += "<li><img src='../Content/images/grey-star.png' alt='' /></li>";
                } else {
                    placediv1 += "<li><img src='../Content/images/yellow-star.png' alt='' /></li>";
                }
            }
            placediv1 += "<li>(" + place[0].tripAdvisorRating + "/5)</li></ul>";
            descri = "<p><b>Short Description : </b></p>" + (place[0].shortDescription).replace('&lt;p&gt;&lt;b&gt;Property Location&lt;/b&gt; &lt;br /&gt;', '') + "...";
            placediv1 += "<p><b>Address : </b>" + place[0].address1;
            if (place[0].address2 != null && place[0].address2 != "") {
                placediv1 += "," + place[0].address2;
            }
            if (place[0].city != null && place[0].city != "") {
                placediv1 += "," + place[0].city;
            }
            if (place[0].postalCode != null && place[0].postalCode != "") {
                placediv1 += "-" + place[0].postalCode;
            }
            placediv1 += "</p><p><b>Location Description : </b>" + place[0].locationDescription + "</p><br/><p>" + descri + " </p>";
            placediv1 += "<p><b>Rate Currency Code : </b>" + place[0].rateCurrencyCode + "</p>";
            placediv1 += "<p><b>High Rate : </b>" + place[0].highRate + "</p>";
            placediv1 += "<p><b>Low Rate : </b>" + place[0].lowRate + "</p>";
            placediv1 += "<p><b>Deep Link : </b>" + place[0].deepLink + "</p>";
            if (amenitiesare != "") {
                placediv1 += "<br/><p><b>Hotel Amenities : </b>" + amenitiesare + "</p>";
            }
            placediv1 += "</div>";
            placediv1 += "</div> </div> </div>";
            placediv1 += "</div>";
            $('.place-box-right #' + ui_id).before(placediv1);
            $('.place-box-right #' + ui_id).remove();
        }
        else {
            var tmpdata = [];
            for (var indx = 0, lmt = manualdata.length; indx < lmt; indx++) {
                if (manualdata[indx].ctyName == $('#selectedcity').html()) {
                    tmpdata = manualdata[indx].hotldata;
                    break;
                }
            }
            var place = tmpdata.filter(function (obj) {
                return obj.Id == split_ui_id[1];
            });
            var placediv1 = "<div class='place' id='place" + place[0].Id + "'><div class='place-details' id=" + place[0].Id + " style ='height:86px;'><input type='hidden' value='0' class='frmApi'/><figure class='place-img'>";
            placediv1 += "<img src='../Content/images/HotelImages/" + place[0].Photo + "' alt='' /></figure><div class='place-cont'>";
            placediv1 += "<div class='p-left'><h4>" + place[0].Name + "</h4><p style='height:26px; overflow-y:hidden;'>" + place[0].Street + "</p><input type='hidden' class='hidlat' value='" + place[0].HotlLat + "'/><input type='hidden' class='hidlng' value='" + place[0].HotlLng + "'/><span>Hotel.</span>";
            placediv1 += "</div><div class='p-right'>";
            placediv1 += "<ul><li><a href='#'><img src='../Content/images/del-icon.png' class='deleteplace' alt='' /></a></li><li><a href='#infowindow_hotels_activities' onclick='return GetMnlHtlinfo(\"" + place[0].Id + "\")' class='fancybox'><img src='../Content/images/info-icon.png' alt='' /></a></li> </ul>";
            placediv1 += "</div> </div> </div>";
            placediv1 += "</div>";
            $('.place-box-right #' + ui_id).before(placediv1);
            $('.place-box-right #' + ui_id).remove();
        }
    }
    else if ($("#prefer").html() == "Activity Preferences") {
       // if ($("#isME" + split_ui_id[1]).val() == 0) {
            var place = activities_foradd.filter(function (obj) {
                return obj.id == split_ui_id[1];
            });
            plcid = place[0].id;
            var duration = place[0].durationInMillis;
            var minutes = duration / (1000 * 60);
            minutes = ((minutes / 30) * 100) / 2;
            if (minutes > 2000)
            {
                minutes = 1700;
            }
            var toleft = "<div class='place' id='place" + place[0].id + "'><div class='place-details' id=" + place[0].id + " style ='height:" + minutes + "px;'><input type='hidden' value='1' class='frmApi'/><figure class='place-img'>";

           

            var obj = new Image();
            obj.src = place[0].imageUrl;

            if (obj.complete) {
                toleft += "<img src='" + place[0].imageUrl + "'/></figure><div class='place-cont'>";// Do something now you know the image exists.
                //alert('worked');
            } else {
                toleft += "<img src='../Content/images/ActivitiesImage/5c37-semi.jpg' alt='' /></figure><div class='place-cont'>";
                
                //alert('doesnt work');
            }
          
            var ActivLat = place[0].latLng.split(',');
            toleft += "<div class='p-left'><h4>" + place[0].title + "</h4><p style='height:26px; overflow-y:hidden;'>" + place[0].duration + "</p><input type='hidden' class='hidlat' value='" + ActivLat[0] + "'/><input type='hidden' class='hidlng' value='" + ActivLat[1] + "'/><span>Activity.</span><br><span id='sp_" + place[0].id + "' class='sphrs'>";
            toleft += "</div><div class='p-right'>";
            toleft += "<ul><li><a href='#'><img src='../Content/images/del-icon.png' class='deleteplace' alt=''/></a></li><li><a href='#infowindow_hotels_activities' class='fancybox' onclick='getActivityInfo(\"" + place[0].id + "\",\"" + $("#selectedcity").html() + "\");'><img src='../Content/images/info-icon.png' alt='' /></a></li> </ul>";
            toleft += "</div> </div> </div>";
            toleft += "</div>";
            $('.place-box-right #' + ui_id).before(toleft);
            $('.place-box-right #' + ui_id).remove();
        //}
        //else
        //{
        //    var tmpdata = [];
        //    for (var indx = 0, lmt = manualdata.length; indx < lmt; indx++) {
        //        if (manualdata[indx].ctyName == $('#selectedcity').html()) {
        //            tmpdata = manualdata[indx].actidata;
        //            break;
        //        }
        //    }
        //    var place = tmpdata.filter(function (obj) {
        //        return obj.Id == split_ui_id[1];
        //    });
        //    var toleft = "<div class='place' id='place" + place[0].Id + "'><div class='place-details' id=" + place[0].Id + " style ='height:200px;'><input type='hidden' value='0' class='frmApi'/><figure class='place-img'>";
        //    toleft += "<img src='../Content/images/ActivitiesImage/" + place[0].Photo + "'/></figure><div class='place-cont'>";
        //    toleft += "<div class='p-left'><h4>" + place[0].Name + "</h4><p style='height:26px; overflow-y:hidden;'>" + place[0].Street + "</p><input type='hidden' class='hidlat' value='" + place[0].ActLat + "'/><input type='hidden' class='hidlng' value='" + place[0].ActLng + "'/><span>Activity.</span><br><span id='sp_" + place[0].Id + "' class='sphrs'>";
        //    toleft += "</div><div class='p-right'>";
        //    toleft += "<ul><li><a href='#'><img src='../Content/images/del-icon.png' class='deleteplace' alt=''/></a></li><li><a href='#infoActi_" + place[0].Id + "' class='fancybox'><img src='../Content/images/info-icon.png' alt='' /></a></li> </ul>";
        //    toleft += "<div id='infoActi_" + place[0].Id + "' class='info-left popup-bg' style='display: none;'>";
        //    toleft += "<figure class='resort-img'><img src='../Content/images/ActivitiesImage/" + place[0].Photo + "' alt='' /><span>From <strong>$" + place[0].Price + "</strong><br/>(" + place[0].desc + ")</span></figure>";
        //    toleft += "<h3>" + place[0].Name + "</h3>";
        //    if (place[0].Rating != "") {
        //        var actrating = parseFloat(place[0].Rating).toFixed(1);
        //        toleft += "<ul><b>Ratings : </b>";
        //        for (var loopvaria1 = 0; loopvaria1 < 5; loopvaria1++) {
        //            if (place[0].Rating < (loopvaria1 + 0.5)) {
        //                toleft += "<li><img src='../Content/images/grey-star.png' alt='' /></li>";
        //            }
        //            else {
        //                toleft += "<li><img src='../Content/images/yellow-star.png' alt='' /></li>";
        //            }
        //        }
        //        toleft += "<li>(" + actrating + "/5)</li></ul>";
        //    }
        //    toleft += "<p><b>Starting location : </b>" + place[0].Street + "</p>";
        //    toleft += "<p><b>Locations : </b>" + place[0].Street + "</p>";
        //    toleft += "<p><b>Url : </b><a href='" + place[0].Website + "'>" + place[0].Website + "</a></p>";
        //    toleft += "<p><b>Description : </b>" + place[0].desc + "</p>";
        //    toleft += "</div>"
        //    toleft += "</div> </div> </div>";
        //    toleft += "</div>";
        //    $('.place-box-right #' + ui_id).before(toleft);
        //    $('.place-box-right #' + ui_id).remove();
            
        //}
    }
    else if ($("#prefer").html() == "Attraction Preferences") {
        if ($("#isME" + split_ui_id[1]).val() == 0) {
            var place1 = attrDetails.filter(function (obj) {
                return obj.plc_id == split_ui_id[1];
            });
            var place = place1[0].details;
            plcid = place.place_id;
            var att_or_res = 2;
            var placediv1 = "<div class='place' id='place" + place.place_id + "'><div class='place-details' id=" + place.place_id + " style ='height:200px;'><input type='hidden' value=" + place.geometry.location.lat() + " class='placeatt_lat'/><input type='hidden' value=" + place.geometry.location.lng() + " class='placeatt_lng'/>";
            placediv1 += "<input type='hidden' value=" + place.name + " class='placeatt_name'/><input type='hidden' value='1' class='frmApi'/><figure class='place-img'>";
            if (!place.photos) {
                placediv1 += "<img src='../content/images/AppImages/attractions.jpg' alt='' /></figure><div class='place-cont'>";
            }
            else {
                placediv1 += "<img src='" + place.photos[0].getUrl({ 'maxWidth': 50, 'maxHeight': 50 }) + "'  /></figure><div class='place-cont'>";
            }
            placediv1 += "<div class='p-left'><h4>" + place.name + "</h4><p style='height:26px; overflow-y:hidden;'>" + place.vicinity + "</p><input type='hidden' class='hidlat' value='" + place.geometry.location.lat() + "'/><input type='hidden' class='hidlng' value='" + place.geometry.location.lng() + "'/><span>Attraction.</span><br><span id='sp_" + place.place_id + "' class='sphrs'>";
            placediv1 += "</div><div class='p-right'>";
            placediv1 += "<ul><li><a href='#'><img src='../Content/images/del-icon.png' class='deleteplace' alt=''/></a></li><li><a href='#infowindow' class='fancybox' onclick='return infoattraction(\"" + place.place_id + "\",\"" + place.geometry.location.lat() + "\",\"" + place.geometry.location.lng() + "\",\"" + att_or_res + "\");' ><img src='../Content/images/info-icon.png' alt='' /></a></li> </ul>";
            placediv1 += "</div> </div> </div>";
            placediv1 += "</div>";
            $('.place-box-right #' + ui_id).before(placediv1);
            $('.place-box-right #' + ui_id).remove();
        }
        else {
            var tmpdata = [];
            for (var indx = 0, lmt = manualdata.length; indx < lmt; indx++) {
                if (manualdata[indx].ctyName == $('#selectedcity').html()) {
                    tmpdata = manualdata[indx].attrdata;
                    break;
                }
            }
            var place = tmpdata.filter(function (obj) {
                return obj.Id == split_ui_id[1];
            });
            var att_or_res = 2;
            var placediv1 = "<div class='place' id='place" + place[0].Id + "'><div class='place-details' id=" + place[0].Id + " style ='height:200px;'><input type='hidden' value=" + place[0].AttrLat + " class='placeatt_lat'/><input type='hidden' value=" + place[0].AttrLng + " class='placeatt_lng'/>";
            placediv1 += "<input type='hidden' value='0' class='frmApi'/><input type='hidden' value=" + place[0].Name + " class='placeatt_name'/><input type='hidden' value='1' class='frmApi'/><figure class='place-img'>";
            placediv1 += "<img src='../Content/images/AttractionImage/" + place[0].Photo + "' alt='' /></figure><div class='place-cont'>";
            placediv1 += "<div class='p-left'><h4>" + place[0].Name + "</h4><p style='height:26px; overflow-y:hidden;'>" + place[0].Street + "</p><input type='hidden' class='hidlat' value='" + place[0].AttrLat + "'/><input type='hidden' class='hidlng' value='" + place[0].AttrLng + "'/><span>Attraction.</span><br><span id='sp_" + place[0].Id + "' class='sphrs'>";
            placediv1 += "</div><div class='p-right'>";
            placediv1 += "<ul><li><a href='#'><img src='../Content/images/del-icon.png' class='deleteplace' alt=''/></a></li><li><a href='#infowindow' class='fancybox' onclick='return infoattraction(\"" + place[0].Id + "\",\"" + place[0].AttrLat + "\",\"" + place[0].AttrLng + "\",\"" + att_or_res + "\");' ><img src='../Content/images/info-icon.png' alt='' /></a></li> </ul>";
            placediv1 += "</div> </div> </div>";
            placediv1 += "</div>";
            $('.place-box-right #' + ui_id).before(placediv1);
            $('.place-box-right #' + ui_id).remove();
        }
    }
    else if ($("#prefer").html() == "Restaurant Preferences") {
        if ($("#isME" + split_ui_id[1]).val() == 0) {
            var place = allrestaurants_toleft.filter(function (obj) {
                return obj.place_id == split_ui_id[1];
            });
            var att_or_res = 1;
            plcid = place[0].place_id;
            var placediv1 = "<div class='place' id='place" + place[0].place_id + "'><div class='place-details' id=" + place[0].place_id + " style ='height:100px;'><input type='hidden' value=" + place[0].geometry.location.lat() + " class='placeres_lat'/><input type='hidden' value=" + place[0].geometry.location.lng() + " class='placeres_lng'/>";
            placediv1 += "<input type='hidden' value='1' class='frmApi'/><input type='hidden' value=" + place[0].name + " class='placeres_name'/><figure class='place-img'>";
            if (!place[0].photos) {
                placediv1 += "<img src='../content/images/AppImages/restaurant.png' alt='' /></figure><div class='place-cont'>";
            }
            else {
                placediv1 += "<img src='" + place[0].photos[0].getUrl({ 'maxWidth': 50, 'maxHeight': 50 }) + "' /></figure><div class='place-cont'>";
            }
            placediv1 += "<div class='p-left'><h4>" + place[0].name + "</h4><p style='height:26px; overflow-y:hidden;'>" + place[0].vicinity + "</p><input type='hidden' class='hidlat' value='" + place[0].geometry.location.lat() + "'/><input type='hidden' class='hidlng' value='" + place[0].geometry.location.lng() + "'/><span>Restaurant.</span><br><span id='sp_" + place[0].place_id + "' class='sphrs'>";
            placediv1 += "</div><div class='p-right'>";
            placediv1 += "<ul><li><a href='#'><img src='../Content/images/del-icon.png' class='deleteplace' alt=''  /></a></li><li><a href='#infowindow' class='fancybox' onclick='return infoattraction(\"" + place[0].place_id + "\",\"" + place[0].geometry.location.lat() + "\",\"" + place[0].geometry.location.lng() + "\",\"" + att_or_res + "\");' ><img src='../Content/images/info-icon.png' alt='' /></a></li> </ul>";
            placediv1 += "</div> </div> </div>";
            placediv1 += "</div>";
            $('.place-box-right #' + ui_id).before(placediv1);
            $('.place-box-right #' + ui_id).remove();
        }
        else
        {
            var tmpdata = [];
            for (var indx = 0, lmt = manualdata.length; indx < lmt; indx++) {
                if (manualdata[indx].ctyName == $('#selectedcity').html()) {
                    tmpdata = manualdata[indx].restdata;
                    break;
                }
            }
            var place = tmpdata.filter(function (obj) {
                return obj.Id == split_ui_id[1];
            });
            var att_or_res = 1;
            var placediv1 = "<div class='place' id='place" + place[0].Id + "'><div class='place-details' id=" + place[0].Id + " style ='height:100px;'><input type='hidden' value=" + place[0].RestLat + " class='placeres_lat'/><input type='hidden' value=" + place[0].RestLng + " class='placeres_lng'/>";
            placediv1 += "<input type='hidden' value='0' class='frmApi'/><input type='hidden' value=" + place[0].Name + " class='placeres_name'/><figure class='place-img'>";
            placediv1 += "<img src='../content/images/AppImages/restaurant.png' alt='' /></figure><div class='place-cont'>";
            placediv1 += "<div class='p-left'><h4>" + place[0].Name + "</h4><p style='height:26px; overflow-y:hidden;'>" + place[0].Street + "</p><input type='hidden' class='hidlat' value='" + place[0].RestLat + "'/><input type='hidden' class='hidlng' value='" + place[0].RestLng + "'/><span>Restaurant.</span><br><span id='sp_" + place[0].Id + "' class='sphrs'>";
            placediv1 += "</div><div class='p-right'>";
            placediv1 += "<ul><li><a href='#'><img src='../Content/images/del-icon.png' class='deleteplace' alt=''  /></a></li><li><a href='#infowindow' class='fancybox' onclick='return infoattraction(\"" + place[0].place_id + "\",\"" + place[0].RestLat + "\",\"" + place[0].RestLng + "\",\"" + att_or_res + "\");' ><img src='../Content/images/info-icon.png' alt='' /></a></li> </ul>";
            placediv1 += "</div> </div> </div>";
            placediv1 += "</div>";
            $('.place-box-right #' + ui_id).before(placediv1);
            $('.place-box-right #' + ui_id).remove();
        }
    }
    else if ($("#prefer").html() == "Custom Places")
    {
        var place = CustomPlacesArray.filter(function (obj) {
            return obj.PlaceId == split_ui_id[1];
        });
        plcid = place[0].PlaceId;
        var placediv1 = "<div class='place' id='place" + place[0].PlaceId + "'><div class='place-details' id=" + place[0].PlaceId + " style ='height:100px;'><input type='hidden' value='nolatitude' class='placeres_lat'/><input type='hidden' value='nolongitude' class='placeres_lng'/>";
        placediv1 += "<input type='hidden' value='1' class='frmApi'/><input type='hidden' value=" + place[0].Place_Name + " class='placeres_name'/><figure class='place-img'>";
        placediv1 += "<img src='../Content/images/AppImages/small-img5.jpg' alt='' /></figure><div class='place-cont'>";
        placediv1 += "<div class='p-left'><h4>" + place[0].Place_Name + "</h4><p style='height:26px; overflow-y:hidden;'>" + place[0].Address_ + "</p><input type='hidden' class='hidlat' value='nolatitude'/><input type='hidden' class='hidlng' value='nolongitude'/><span>Custom place.</span><br><span id='sp_" + place[0].PlaceId + "' class='sphrs'>";
        placediv1 += "</div><div class='p-right'>";
        placediv1 += "<ul><li><a href='#'><img src='../Content/images/del-icon.png' class='deleteplace' alt=''  /></a></li><li><a  class='fancybox' href='#info_" + place[0].PlaceId + "'><img src='../Content/images/info-icon.png' alt='' /></a></li> </ul>";
        placediv1 += "</div> </div> </div>";
        placediv1 += "</div>";
        $('.place-box-right #' + ui_id).before(placediv1);
        $('.place-box-right #' + ui_id).remove();
    }
    onresizingplc();
}

function retrieve_package_from_db()
{
    setNumberForDays();
    var TopPlanId = 0;
    if ($("#IsfrmTopItin").val() == '1')
    {
        var PlanId = $("#TopItinryId").val();
        if (PlanId.indexOf("1EzTrip") >= 0) {
            var spltPlanId = PlanId.split("1EzTrip");
            TopPlanId = spltPlanId[1];
        }
    }

    $.getJSON('../trip/RetrievePackage', { TripId: TopPlanId }, function (data) {
        var itinerary = "";
        var increTravelMode = 0;
        var plcs_id_split = [], plcs_names_split = [], plcs_spendtime_split = [], plcs_addr_split = [], plcs_lat_split = [];
        var plcs_lng_split = [], travelmode_split = [], traveldur_split = [], plc_start_split = [],plc_cate_split=[],plc_thumbnailurl_split=[],plc_manual_sp=[];
        $.each(data, function (keys, values) {
            plcs_id_split = values.Spot_Id.split('||');
            plcs_names_split = values.Spot_Name.split('||');
            plcs_spendtime_split = values.Spot_SpendingTime.split('||');
            plcs_addr_split = values.Spot_Address.split('||');
            plcs_lat_split = values.Spot_Latitude.split('||');
            plcs_lng_split = values.Spot_Longitude.split('||');
            travelmode_split = values.Travel_Mode.split('||');
            traveldur_split = values.Travel_time.split('||');
            plc_start_split = values.Visit_StartTime.split('||');/**stored in database as top,left styles**/
            plc_cate_split = values.spot_category.split('||');
            plc_thumbnailurl_split = values.thumbnail_Url.split('|image_split|')
            plc_manual_sp = values.IsAPI_data.split('||');
            var att_or_res = 0;
            var daynum = values.Day_Number;
            var plc_bx_to_append = "";            
            if ($('.place-box-right').eq(daynum - 1).find('.place-box').attr('id') != null || $('.place-box-right').eq(daynum - 1).find('.place-box').attr('id') != "undefined") {
                plc_bx_to_append = $('.place-box-right').eq(daynum - 1).find('.place-box').attr('id');
                $("#" + plc_bx_to_append).empty();
                for (var loopvar = 0, PlacesLength = plcs_id_split.length; loopvar < PlacesLength; loopvar++) {
                    if (plcs_id_split[loopvar] == "pl_bx") {
                        itinerary = "<div class='place-box timing-details'></div>";
                    }
                    else {
                        var startingtime_parse = $.parseJSON(plc_start_split[loopvar]);
                        itinerary = "<div class='place' id='place" + plcs_id_split[loopvar] + "'><div class='place-details' id=" + plcs_id_split[loopvar] + " style ='height:" + plcs_spendtime_split[loopvar] + "px;'><input type='hidden' value=" + plcs_lat_split[loopvar] + " class='placeatt_lat'/><input type='hidden' value=" + plcs_lng_split[loopvar] + " class='placeatt_lng'/>";
                        if (plc_manual_sp[loopvar] == "Yes")
                        {
                            itinerary += "<input type='hidden' value='1' class='frmApi'/>";
                        }
                        else
                        {
                            itinerary += "<input type='hidden' value='0' class='frmApi'/>";
                        }
                        itinerary +="<input type='hidden' value=" + plcs_names_split[loopvar] + " class='placeatt_name'/><figure class='place-img'>";
                        itinerary += "<img src='" + plc_thumbnailurl_split[loopvar] + "' alt=''/></figure><div class='place-cont'>";
                        itinerary += "<div class='p-left'><h4>" + plcs_names_split[loopvar] + "</h4><p style='height:26px; overflow-y:hidden;'>" + plcs_addr_split[loopvar] + "</p><input type='hidden' class='hidlat' value='" + plcs_lat_split[loopvar] + "'/><input type='hidden' class='hidlng' value='" + plcs_lng_split[loopvar] + "'/><span>" + plc_cate_split[loopvar] + "</span>";
                        if (plc_cate_split[loopvar] != "Hotel.") {
                            itinerary += "<br><span id='sp_" + plcs_id_split[loopvar] + "' class='sphrs'>";
                        }
                        //if (plc_cate_split[loopvar] == "Hotel.") {

                        //    var found = jQuery.inArray(plcs_id_split[loopvar], inthetrip_array_hotels);
                        //    if (found == -1) {
                        //        itinerary += "<span id='Plcbook" + plcs_id_split[loopvar] + "' style='display: block;margin-left: 20px;'><a href='#' class='Plcbook'>Book</a></span>";
                        //    }
                        //}
                        itinerary += "</div><div class='p-right'>";
                        itinerary += "<ul><li><a href='#'><img src='../Content/images/del-icon.png' class='deleteplace' alt=''/></a></li>";
                        
                        if (plc_cate_split[loopvar] == "Restaurant.") {
                            att_or_res = 1;
                            if (plc_manual_sp[loopvar] == "Yes") {
                                itinerary += "<li><a href='#infowindow' class='fancybox' onclick='return infoattraction(\"" + plcs_id_split[loopvar] + "\",\"" + plcs_lat_split[loopvar] + "\",\"" + plcs_lng_split[loopvar] + "\",\"" + att_or_res + "\");' ><img src='../Content/images/info-icon.png' alt='' /></a></li> </ul>";
                            }
                            else
                            {
                                itinerary += "<li><a href='#infowindow' class='fancybox' onclick='return infomanual(\"3\",\"" + plcs_id_split[loopvar] + "\");'><img src='../content/images/info-icon.png' alt='' /></a></li>";
                            }
                            var found = jQuery.inArray(plcs_id_split[loopvar], inthetrip_array_restaurants);
                            if (found == -1) {
                                inthetrip_array_restaurants.push(plcs_id_split[loopvar]);
                            }
                        }
                        else if (plc_cate_split[loopvar] == "Attraction.") {
                            att_or_res = 2;
                            if (plc_manual_sp[loopvar] == "Yes") {
                                itinerary += "<li><a href='#infowindow' class='fancybox' onclick='return infoattraction(\"" + plcs_id_split[loopvar] + "\",\"" + plcs_lat_split[loopvar] + "\",\"" + plcs_lng_split[loopvar] + "\",\"" + att_or_res + "\");' ><img src='../Content/images/info-icon.png' alt='' /></a></li> </ul>";
                            }
                            else {
                                itinerary += "<li><a href='#infowindow' class='fancybox' onclick='return infomanual(\"2\",\"" + plcs_id_split[loopvar] + "\");'><img src='../content/images/info-icon.png' alt='' /></a></li>";
                            }
                            var found = jQuery.inArray(plcs_id_split[loopvar], inthetrip_array_attractions);
                            if (found == -1) {
                                inthetrip_array_attractions.push(plcs_id_split[loopvar]);
                            }
                        }
                        else if (plc_cate_split[loopvar] == "Activity.") {
                            att_or_res = 2;
                            //if (plc_manual_sp[loopvar] == "Yes") {
                            itinerary += "<li><a href='#infowindow_hotels_activities' class='fancybox' onclick='getActivityInfo(\"" + plcs_id_split[loopvar] + "\",\"" + values.City_Name + "\");' ><img src='../Content/images/info-icon.png' alt='' /></a></li> </ul>";
                            //}
                            //else {
                            //    itinerary += "<li><a href='#infowindow' class='fancybox' onclick='return infomanual(\"2\",\"" + plcs_id_split[loopvar] + "\");'><img src='../content/images/info-icon.png' alt='' /></a></li>";
                            //}
                            var found = jQuery.inArray(plcs_id_split[loopvar], inthetrip_array_activities);
                            if (found == -1) {
                                inthetrip_array_activities.push(plcs_id_split[loopvar]);
                            }
                        }
                        else if (plc_cate_split[loopvar] == "Hotel.") {
                            if (plc_manual_sp[loopvar] == "Yes") {
                                itinerary += "<li><a href='#infowindow' class='fancybox' onclick='return infohotel(\"" + plcs_id_split[loopvar] + "\");' ><img src='../Content/images/info-icon.png' alt='' /></a></li> </ul>";
                            }
                            else {
                                itinerary += "<li><a href='#infowindow_hotels_activities' class='fancybox' onclick='return GetMnlHtlinfo(\"" + plcs_id_split[loopvar] + "\");'><img src='../content/images/info-icon.png' alt='' /></a></li>";
                            }
                            var found = jQuery.inArray(plcs_id_split[loopvar], inthetrip_array_hotels);
                            if (found == -1) {
                                inthetrip_array_hotels.push(plcs_id_split[loopvar]);
                            }
                            var CitiesCnt = 0;
                            for (var loopvariable = 0, RetrievedHotels = hotels_retrievedCities.length; loopvariable < RetrievedHotels; loopvariable++)
                            {
                                if(hotels_retrievedCities[loopvariable].cityname.trim()==values.City_Name.trim())
                                {
                                    CitiesCnt++;
                                }
                            }
                            if(CitiesCnt==0)
                            {
                                hotels_retrievedCities.push({
                                    cityname: values.City_Name,
                                    hotelId: plcs_id_split[loopvar]
                                })
                            }
                        }
                        itinerary += "</div> </div> </div>";
                        itinerary += "</div>";
                        if (travelmode_split[loopvar].trim() !== "No travelmode") {
                            increTravelMode++;
                            var getTravelmode = travelmode_split[loopvar];
                            var TravelmodeSelected = "";
                            if (getTravelmode.indexOf("Cus~") != -1) {
                                var GetCustomTravelMode=travelmode_split[loopvar].split('~');
                                TravelmodeSelected = GetCustomTravelMode[1];
                                itinerary += "<div class='custom_travel' id='ltra_" + increTravelMode + "'>";
                                itinerary += "<a href='#' style='float:right;'><img src='../Content/images/del_travel.png' class='travedelimg'/></a>";
                                itinerary += "<p>Time taken by " + TravelmodeSelected + " is " + traveldur_split[loopvar] + "</p>";
                            }
                            else
                            {
                                TravelmodeSelected = travelmode_split[loopvar];
                                itinerary += "<div class='travel' id='ltra_" + increTravelMode + "'>";
                                itinerary += "<a href='#' style='float:right;'><img src='../Content/images/del_travel.png' class='travedelimg'/></a>";
                                itinerary += "<p>Time taken by " + TravelmodeSelected + " is " + traveldur_split[loopvar] + "<img src='../Content/images/edit.png' alt='' class='editimg' style='height: 15px;width: 15px;'></p>";
                            }                            
                            itinerary += "<ul>";
                            if (TravelmodeSelected == "car") {
                                itinerary += "<li><input type='hidden' class='hidcar' value='" + traveldur_split[loopvar] + "'/><a href='#' data-value='car' class='active'><img src='../Content/images/car-icon.png' alt=''/></a></li>";
                            }
                            else
                            {
                                itinerary += "<li><input type='hidden' class='hidcar' value=''/><a href='#' data-value='car'><img src='../Content/images/car-icon.png' alt=''/></a></li>";
                            }
                            if (TravelmodeSelected == "train") {
                                itinerary += "<li><input type='hidden' class='hidtrain'  value='" + traveldur_split[loopvar] + "'/><a href='#' data-value='train' class='active'><img src='../Content/images/train-icon.png' alt='' /></a></li>";
                            }
                            else
                            {
                                itinerary += "<li><input type='hidden' class='hidtrain' value=''/><a href='#' data-value='train'><img src='../Content/images/train-icon.png' alt='' /></a></li>";
                            }
                            if (TravelmodeSelected == "plane") {
                                itinerary += "<li><input type='hidden' class='hidplane'  value='" + traveldur_split[loopvar] + "'/><a href='#' data-value='plane' class='active'><img src='../Content/images/plane-icon.png' alt='' /></a></li>";
                            }
                            else
                            {
                                itinerary += "<li><input type='hidden' class='hidplane' value=''/><a href='#' data-value='plane'><img src='../Content/images/plane-icon.png' alt='' /></a></li>";
                            }
                            if (TravelmodeSelected == "walk")
                            {
                                itinerary += "<li><input type='hidden' class='hidwalk'  value='" + traveldur_split[loopvar] + "'/><a href='#' data-value='walk' class='active'><img src='../Content/images/walk-icon.png' alt='' /></a></li>";
                            }
                            else
                            {
                                itinerary += "<li><input type='hidden' class='hidwalk' value=''/><a href='#' data-value='walk'><img src='../Content/images/walk-icon.png' alt='' /></a></li>";
                            }
                            if (TravelmodeSelected == "bicycle")
                            {
                                itinerary += "<li class='last'><input type='hidden' class='hidcycle' value='" + traveldur_split[loopvar] + "'/><a href='#' data-value='bicycle' class='active'><img src='../Content/images/scooter-icon.png' alt='' /></a></li>";
                            }
                            else
                            {
                                itinerary += "<li class='last'><input type='hidden' class='hidcycle' value=''/><a href='#' data-value='bicycle'><img src='../Content/images/scooter-icon.png' alt='' /></a></li>";
                            }
                            itinerary += "<input type='hidden' class='hiddriving' value=''/>";
                            itinerary += "</ul></div>";                            
                        }
                    }
                    $("#" + plc_bx_to_append).append(itinerary);
                }
            }
        });
        onresizingplc();
        setTimeout(function () { CreatePackageForAddcity(); }, 5000);
        $('#wholepart').unblock();
        var size = $('#compl').find('.travel').length;
        TravelDivResize(size);
        var customTravelsize = $('#compl').find('.custom_travel').length;
        CustomTravelDivResize(customTravelsize);
        intheTripRHAA("Hotel");
    });
}

function infohotel(Id_for_info)
{
    $.getJSON('../Trip/HotelInformationRequest', { hotelId_for_info: Id_for_info }, function (data) {
        var descri = "";
        var amenitiesare = "";
        var response = data.HotelInformationResponse['HotelSummary'];
        var imgresponse = data.HotelInformationResponse['HotelImages'];
        var amenities = data.HotelInformationResponse['PropertyAmenities'];
        inf = "<div class='info-left popup-bg' style='width:94%;'><figure class='resort-img'><img src='" + imgresponse.HotelImage[0].url + "' alt='' /><span>Night from <strong>$" + response.highRate + "</strong></span></figure>";
        inf += "<h2>" + response.name + "</h2>";
        //inf += "<ul><b>Guest Ratings : </b>";
        //for (var loopvar_rate = 1; loopvar_rate <= 5; loopvar_rate++) {
        //    if (response.tripAdvisorRating >= loopvar_rate) {
        //        inf += "<li><img src='../Content/images/hotel-color.png' /></li>";
        //    }
        //    else if (response.tripAdvisorRating > (loopvar_rate - 1) && response.tripAdvisorRating < loopvar_rate) {
        //        inf += "<li><img src='../Content/images/hotel-half.png' /></li>";
        //    }
        //    else {
        //        inf += "<li><img src='../Content/images/hotel-gray.png' /></li>";
        //    }
        //}
        //inf += "<li>(" + response.tripAdvisorRating + "/5)</li></ul>";
        inf += "<ul><b>Hotel Class: </b>";
        for (var loopvar_hc = 1; loopvar_hc <= 5; loopvar_hc++) {
            if (response.hotelRating >= loopvar_hc) {
                inf += "<li><img src='../Content/images/yellow-star1.png' /></li>";
            }
            else if (response.hotelRating > (loopvar_hc - 1) && response.hotelRating < loopvar_hc) {
                inf += "<li><img src='../Content/images/yellow-gray-starNew.png' /></li>";
            }
            else {
                inf += "<li><img src='../Content/images/grey-starNew.png' /></li>";
            }
        }
        inf += "(" + response.hotelRating + ")</ul></p>";        
        inf += "<p><b>Address : </b>" + response.address1;
        if (response.address2 != null && response.address2 != "") {
            inf += "," + response.address2;
        }
        if (response.city != null && response.city != "") {
            inf += "," + response.city;
        }
        if (response.postalCode != null && response.postalCode != "") {
            inf += "-" + response.postalCode;
        }
        inf += "</p>";
        inf +="<p><b>Landmark : </b>" + response.locationDescription + "</p>";
        var propertycat = "";
        if (response.propertyCategory == 1) {
            propertycat = "hotel";
        }
        else if (response.propertyCategory == 2) {
            propertycat = "suite";
        }
        else if (response.propertyCategory == 3) {
            propertycat = "resort";
        }
        else if (response.propertyCategory == 4) {
            propertycat = "vacation rental/condo";
        }
        else if (response.propertyCategory == 5) {
            propertycat = "bed & breakfast";
        }
        else if (response.propertyCategory == 6) {
            propertycat = "all-inclusive ";
        }
        if (propertycat != "hotel") {
            inf += "</p><p><b>Property Category : </b>" + propertycat + "</p>";
        }
        inf += "<p><b>Rate starts from : </b>$" + response.highRate + "</p>";
        if (amenities.size != 0) {
            inf += "<br/><p><b>Hotel Amenities : </b>";
            var increAmenities = 0;
            for (var loopvarAmenities = 0, PropAmenitiesLength = amenities.PropertyAmenity.length; loopvarAmenities < PropAmenitiesLength && loopvarAmenities < 10; loopvarAmenities++)
            {
                inf += "<li>" + amenities.PropertyAmenity[loopvarAmenities].amenity + "</li>";
            }
            inf += "</p>";
        }
        inf += "</div>";
        $('#infowindow').html(inf);
    });
}

/************Add day on retrieve***************/

function GetApiForRetrieved_cities() {
    Allcitiesarray.length = 0;
    non_exists_city.length = 0;
    city_notsaved.length = 0, exist_city = 0;
    var newforaddday_array = foraddday_array.slice();
    $("#citypref li").each(function () {
        var getcityname = $(this).find("a").text();
        var citylat_lng = $(this).find('a').data('value').split('||');
        var citylatit = citylat_lng[0];
        var citylongi = citylat_lng[1];
        Allcitiesarray.push({
            cityname: getcityname,
            citylat: citylatit,
            citylng: citylongi
        });
        citieslist_array.push({
            city_latitude: citylatit,
            city_longitude: citylongi,
            city_name: getcityname
        });
    });

    for (var loopv = 0, AllcitiesLength = Allcitiesarray.length; loopv < AllcitiesLength; loopv++) {
        exist_city = 0;
        for (var loopv_in = 0, AddDayArrayLength = foraddday_array.length; loopv_in < AddDayArrayLength; loopv_in++) {
            if (Allcitiesarray[loopv].cityname.trim() == foraddday_array[loopv_in].city_name.trim()) {
                exist_city++;
                break;
            }
        }
        if (exist_city == 0) {
            non_exists_city.push({
                cityname: Allcitiesarray[loopv].cityname,
                citylati: Allcitiesarray[loopv].citylat,
                citylongi: Allcitiesarray[loopv].citylng
            });
        }
    }
    GetHotels("GetHotels");
    GetActivities("get");
    GetHotelsForRetrieved_cities();
}

function GetHotelsForRetrieved_cities()
{
    if(non_exists_city.length!=0)
    {
        
        add_day_citylatitude = non_exists_city[increForNonExist].citylati;
        add_day_citylongitude = non_exists_city[increForNonExist].citylongi;
        cityname_foradd_day = non_exists_city[increForNonExist].cityname;

        var hotelsExist = 0;
        for (var loopvarhotel = 0, CitiesHotelsLength = hotelsForCities.length; loopvarhotel < CitiesHotelsLength; loopvarhotel++) {
            if (hotelsForCities[loopvarhotel].cityName.trim() == cityname_foradd_day.trim()) {
                hotelsExist = 1;
            }
        }
        if (hotelsExist == 0) {
            $.ajax({
                type: 'GET',
                url: '../Trip/EanHotelRequest',
                dataType: 'json',
                data: { 'lat': add_day_citylatitude, 'lng': add_day_citylongitude },
                success: function (response) {
                    hotelsForCities.push({
                        cityName: cityname_foradd_day,
                        Hotels: response
                    });
                }
            });
        }
        var activityExist = 0;
        for (var loopvaract = 0, CitiesactsLength = ActsForCities.length; loopvaract < CitiesactsLength; loopvaract++) {
            if (ActsForCities[loopvaract].cityName.trim() == cityname_foradd_day.trim()) {
                activityExist = 1;
            }
        }
        if (activityExist == 0) {
            $.ajax({
                type: 'GET',
                url: '../Trip/LocalExpertAPI',
                dataType: 'json',
                data: { 'cityname': cityname_foradd_day },
                success: function (response) {
                    for (var loopActs = 0, actsLen = response.activities.length; loopActs < actsLen; loopActs++) {
                        response.activities[loopActs].Mnual = "0";
                    }                    
                    ActsForCities.push({
                        cityName: cityname_foradd_day,
                        Acts: response
                    });
                }
            });
        }

        attraction_array_addday.length = 0;
        att_actarray_addday.length = 0;
        otherattraction_addday.length = 0;
        restlist_array_addday.length = 0;
        rest_array_addday.length = 0;
        secondClassAttr_addday.length = 0;
        hotel_array_addday.length = 0;
        hotel_arrayfull_addday.length = 0;
        concatenate_array_addday.length = 0;
        cityhotel.length = 0;
        oldatt_actarray_addday.length = 0;
        oldotherattraction_addday.length = 0;
        act_arrayfull_addday.length = 0;
        act_array_addday.length = 0;
        gotDetAddDay = 0;
        increDetAddDay = 0;
        increOthDetAddDay = 0;
        gotOthDetAddDay = 0
        restStatus = 0;
        attrStatus = 0;
        AllAttrCollect.length = 0;
        TotalAttrs = 0;
        incrAttrs = 0;

        user_hotelcls = "";
        user_hotelrating = "";
        user_hStartprice = 10;
        user_hEndprice = 200;
        var spatt_new = [];
        var increCnt = 0;
        user_attr_pref = "Nooption";
        spatt_new.push("aquarium", "place_of_worship", "museum", "art_gallery", "amusement_park", "park");
        var attractionLength = spatt_new.length;
        attus_arr_len = Math.ceil(attractionLength / 6);
        var newarrayTmp = "";
        for (var loopvar = 0; loopvar < attractionLength; loopvar += 6) {
            increCnt++;
            if (increCnt < attus_arr_len || attractionLength % 6 == 0) {
                newarrayTmp = spatt_new[loopvar + 5] + "|" + spatt_new[loopvar + 4] + "|" + spatt_new[loopvar + 3] + "|" + spatt_new[loopvar + 2] + "|" + spatt_new[loopvar + 1] + "|" + spatt_new[loopvar];
                Attractioncall_back(newarrayTmp);
            }
        }
        var spatt_new1 = [];
        increCnt = 0;        
        spatt_new1.push("palace", "botanical garden", "stadium", "bridge", "cathedral", "museum", "beach", "island", "zoo", "square", "statue", "harbour", "pier", "tower", "historical building");
        attractionLength = spatt_new1.length;
        attus_arr_len1 = Math.ceil(attractionLength / 5);
        for (var loopvar = 0; loopvar < attractionLength; loopvar += 5) {
            increCnt++;
            if (increCnt < attus_arr_len1 || attractionLength % 5 == 0) {
                newarrayTmp = "(" + spatt_new1[loopvar + 4] + " OR " + spatt_new1[loopvar + 3] + " OR " + spatt_new1[loopvar + 2] + " OR " + spatt_new1[loopvar + 1] + " OR " + spatt_new1[loopvar] + ")";
                Attractioncall_back1(newarrayTmp);
            }
        }
        user_rest = "Nooption";
        increForNonExist++;
    }
}

function Attractioncall_back(att_category) {
    var pyrmont = "";
    pyrmont = new google.maps.LatLng(add_day_citylatitude, add_day_citylongitude);
    map = new google.maps.Map(document.getElementById('map-canvas'), {
        center: pyrmont,
        zoom: 15
    });
    var request = {
        location: pyrmont,
        radius: 20000,
        rankby: ['prominence'],
        types: [att_category]
    };
    var service = new google.maps.places.PlacesService(map);
    service.nearbySearch(request, function (response, status) { callback_attraction_addday(response, status, att_category) });
}

function callback_attraction_addday(results, status, att_category) {
    var newforaddday_array = foraddday_array.slice();
    if (status == google.maps.places.PlacesServiceStatus.OK) {
        ListAttractions.push({
            attrType: att_category,
            attractions: results,
            cityname: cityname_foradd_day
        });
        var needsExclusions = [];
        for (var loopvar = 0, ResultsLength = results.length; loopvar < ResultsLength; loopvar++) {
            if (results[loopvar].types) {
                needsExclusions = results[loopvar].types;
                someFlag = true;
                for (var index1 = 0, excludeLen = excludeMe.length; index1 < excludeLen; index1++) {
                    if (needsExclusions.indexOf(excludeMe[index1]) !== -1) {
                        someFlag = false;
                        break;
                    }
                }
                if (someFlag) {
                    if ((results[loopvar].name).toLowerCase().indexOf("college") >= 0) {
                        someFlag = false;
                    }
                }
            }
            else
            {
                someFlag = false;
            }
            if (someFlag && results[loopvar].rating) {
                if (results[loopvar].rating >= 1) {
                    AllAttrCollect.push(results[loopvar]);
                    attraction_array_addday.push(results[loopvar]);
                    att_actarray.push(results[loopvar]);
                    att_actarray_addday.push(results[loopvar]);
                    attrDetails.push({
                        plc_id: results[loopvar].place_id,
                        attrType: att_category,
                        details: results[loopvar]
                    });
                }
            }            
        }
    }
    attus_arr_len--;
    if (attus_arr_len == 0 && attus_arr_len1 == 0) {
        attrStatus = 1;
        TotalAttrs = AllAttrCollect.length;
        //GetAllAttrDetOnRetrieve();
        Add_day_Restaurant();
    }
}

function Attractioncall_back1(att_category1) {
    var pyrmont = "";
    pyrmont = new google.maps.LatLng(add_day_citylatitude, add_day_citylongitude);
    map = new google.maps.Map(document.getElementById('map-canvas'), {
        center: pyrmont,
        zoom: 15
    });
    var request = {
        location: pyrmont,
        radius: 20000,
        rankby: ['prominence'],
        keyword: [att_category1]
    };
    var service = new google.maps.places.PlacesService(map);
    service.nearbySearch(request, function (response, status) { callback_attraction_addday1(response, status, att_category1) });
}

function callback_attraction_addday1(results, status, att_category1) {
    var newforaddday_array = foraddday_array.slice();
    if (status == google.maps.places.PlacesServiceStatus.OK) {
        ListAttractions.push({
            attrType: att_category1,
            attractions: results,
            cityname: cityname_foradd_day
        });
        var needsExclusions = [];
        for (var loopvar = 0, ResultsLength = results.length; loopvar < ResultsLength; loopvar++) {
            if (results[loopvar].types) {
                needsExclusions = results[loopvar].types;
                someFlag = true;
                for (var index1 = 0; index1 < excludeMe.length; index1++) {
                    if (needsExclusions.indexOf(excludeMe[index1]) !== -1) {
                        someFlag = false;
                        break;
                    }
                }
                if (someFlag) {
                    if ((results[loopvar].name).toLowerCase().indexOf("college") >= 0) {
                        someFlag = false;
                    }
                }
            }
            else
            {
                someFlag = false;
            }
            if (someFlag) {
                if (results[loopvar].rating) {
                    if (results[loopvar].rating >= 1) {
                        AllAttrCollect.push(results[loopvar]);
                        attraction_array_addday.push(results[loopvar]);
                        att_actarray.push(results[loopvar]);
                        att_actarray_addday.push(results[loopvar]);
                        attrDetails.push({
                            plc_id: results[loopvar].place_id,
                            attrType: att_category1,
                            details: results[loopvar]
                        });
                    }
                }
            }
        }
    }
    attus_arr_len1--;
    if (attus_arr_len1 == 0 && attus_arr_len == 0) {
        attrStatus = 1;
        TotalAttrs = AllAttrCollect.length;
        //GetAllAttrDetOnRetrieve();
        Add_day_Restaurant();
    }
}

function GetAllAttrDetOnRetrieve()
{
    if (TotalAttrs == incrAttrs) {
        for (var arrIndex = 0, arrLen = oldatt_actarray_addday.length; arrIndex < arrLen; arrIndex++) {
            if (oldatt_actarray_addday[arrIndex].user_ratings_total) {
                if (oldatt_actarray_addday[arrIndex].user_ratings_total > 700) {
                    att_actarray_addday.push(oldatt_actarray_addday[arrIndex]);
                }
                else if (oldatt_actarray_addday[arrIndex].user_ratings_total > 200) {
                    secondClassAttr_addday.push(oldatt_actarray_addday[arrIndex]);
                }
                else if (oldatt_actarray_addday[arrIndex].user_ratings_total > 50) {
                    thirdClassAttr_addday.push(oldatt_actarray_addday[arrIndex]);
                }
                else {
                    otherattraction_addday.push(oldatt_actarray_addday[arrIndex]);
                }
            }
        }
        setTimeout("Add_day_Restaurant()",2000);
    }
    else {
        var gotFlag = true;
        var places = AllAttrCollect[incrAttrs];
        for (var loopVal = 0, attrLen = oldatt_actarray_addday.length; loopVal < attrLen; loopVal++) {
            if (oldatt_actarray_addday[loopVal].idPlc == places.place_id) {
                gotFlag = false;
            }
        }
        if (gotFlag) {
            oldatt_actarray_addday.push({
                plc_id: "attr||" + places.place_id,
                idPlc: places.place_id,
                plc_name: places.name,
                plc_lat: places.geometry.location.lat(),
                plc_lng: places.geometry.location.lng()
            });
        }

        var placeID = AllAttrCollect[incrAttrs].place_id;
        var request = {
            placeId: placeID
        };
        var service = new google.maps.places.PlacesService(map);
        service.getDetails(request, function (place, status) {
            if (status == google.maps.places.PlacesServiceStatus.OK) {
                if (place.user_ratings_total) {
                    allattractions_toleft.push(place);
                    for (var indxVal = 0, attrDetVal = attrDetails.length; indxVal < attrDetVal; indxVal++) {
                        if (attrDetails[indxVal].plc_id == place.place_id) {
                            attrDetails[indxVal].UserReviews = place.user_ratings_total;
                            attrDetails[indxVal].details = place;
                            break;
                        }
                    }
                    for (var arrIndex = 0, arrLen = oldatt_actarray_addday.length; arrIndex < arrLen; arrIndex++) {
                        if (oldatt_actarray_addday[arrIndex].idPlc == place.place_id) {
                            oldatt_actarray_addday[arrIndex].user_ratings_total = place.user_ratings_total;
                            if (place.opening_hours) {
                                oldatt_actarray_addday[arrIndex].plc_time = place.opening_hours;
                            }
                            else {
                                oldatt_actarray_addday[arrIndex].plc_time = "No time";
                            }
                            break;
                        }
                    }
                }
                incrAttrs++;
                GetAllAttrDetOnRetrieve();
            }
            else {
                if (status == google.maps.GeocoderStatus.OVER_QUERY_LIMIT) {
                    sleep(1000);
                    GetAllAttrDetOnRetrieve();
                }
                else {
                    incrAttrs++;
                    GetAllAttrDetOnRetrieve();
                }
            }
        });
    }
}

function Add_day_Restaurant() {
    var newforaddday_array = foraddday_array.slice();
    var sprest = user_rest.split(',');
    if (user_rest == "Nooption") {
        var arr_noopt_res = [];
        arr_noopt_res.push("mediterranean", "european", "mexican", "american", "french", "spanish", "italian", "indian", "japanese", "chinese", "thai", "asian","breakfast","pizza","dessert","bakery");
        var restauarntTypesLength = arr_noopt_res.length;
        resus_arr_len = Math.ceil(restauarntTypesLength / 4);
        var increCnt = 0;
        var newTmpRest = "";
        for (var loopvar = 0; loopvar < restauarntTypesLength; loopvar += 4) {
            increCnt++;            
            if (increCnt < resus_arr_len || restauarntTypesLength % 4 == 0) {
                newTmpRest = "(" + arr_noopt_res[loopvar + 3] + " OR " + arr_noopt_res[loopvar + 2] + " OR " + arr_noopt_res[loopvar + 1] + " OR " + arr_noopt_res[loopvar] + ")";
                Restaurantfunctioncall_addday(newTmpRest);
            }
        }
    }
}

function Restaurantfunctioncall_addday(rest_category) {
    var newforaddday_array = foraddday_array.slice();    
    var pyrmont = "";
    pyrmont = new google.maps.LatLng(add_day_citylatitude, add_day_citylongitude);
    map = new google.maps.Map(document.getElementById('map-canvas'), {
        center: pyrmont,
        zoom: 15
    });
    var request = {
        location: pyrmont,
        radius: 5000,
        rankby: ['prominence'],
        types: ['restaurant|food|bakery'],
        keyword: [rest_category]
    };
    var service = new google.maps.places.PlacesService(map);
    service.nearbySearch(request, function (response, status) { callback_restaurant_Addday(response,status,rest_category) });
}

function callback_restaurant_Addday(results, status,rest_category) {
    var newforaddday_array = foraddday_array.slice();
    if (status == google.maps.places.PlacesServiceStatus.OK) {
        var needsExclusions = [];
        ListRestaurants.push({
            resType: rest_category,
            restaurants: results,
            cityname: cityname_foradd_day
        });
        for (var loopvar = 0, ResultsLength = results.length; loopvar < ResultsLength; loopvar++) {
            if (results[loopvar].rating) {
                if (results[loopvar].types) {
                    needsExclusions = results[loopvar].types;
                    someFlag = false;
                    for (var index2 = 0; index2 < Rest_includeMe.length; index2++) {
                        if (needsExclusions.indexOf(Rest_includeMe[index2]) !== -1) {
                            someFlag = true;
                            break;
                        }
                    }
                    
                    if (someFlag) {
                        AllRestCollect.push(results[loopvar]);
                        restlist_array_addday.push(results[loopvar]);
                        rest_array.push(results[loopvar]);
                        rest_array_addday.push(results[loopvar]);
                        restDetails.push({
                            plc_id: results[loopvar].place_id,
                            resType: rest_category,
                            details: results[loopvar]
                        });
                    }
                }
            }
        }
    }
    resus_arr_len--;
    if (resus_arr_len == 0) {
        TotalRests = AllRestCollect.length;
        //GetAllRestDetOnRetrieve();
        restStatus = 1;
        GetHotelsAddDay();
    }
}

function GetAllRestDetOnRetrieve()
{
    if (TotalRests == incrRests) {
        for (var arrIndex = 0, arrLen = oldrest_array_addday.length; arrIndex < arrLen; arrIndex++) {
            if (oldrest_array_addday[arrIndex].user_ratings_total) {
                if (oldrest_array_addday[arrIndex].user_ratings_total > 50) {
                    rest_array_addday.push(oldrest_array_addday[arrIndex]);
                }
                else {
                    otherrestaurant_addday.push(oldrest_array_addday[arrIndex]);
                }
            }
        }
        GetHotelsAddDay();
    }
    else
    {
        var gotFlag = true;
        var places = AllRestCollect[incrRests];
        for (var loopVal = 0, restLen = oldrest_array_addday.length; loopVal < restLen; loopVal++) {
            if (oldrest_array_addday[loopVal].plc_id == places.place_id) {
                gotFlag = false;
            }
        }
        if (gotFlag) {
            oldrest_array_addday.push({
                plc_id: places.place_id,
                plc_name: places.name,
                plc_lat: places.geometry.location.lat(),
                plc_lng: places.geometry.location.lng()
            });
        }
        var placeID = AllRestCollect[incrRests].place_id;
        var request = {
            placeId: placeID
        };
        var service = new google.maps.places.PlacesService(map);
        service.getDetails(request, function (place, status) {
            if (status == google.maps.places.PlacesServiceStatus.OK) {
                if (place.user_ratings_total) {
                    for (var indxVal = 0, restDetVal = restDetails.length; indxVal < restDetVal; indxVal++) {
                        if (restDetails[indxVal].plc_id == place.place_id) {
                            restDetails[indxVal].UserReviews = place.user_ratings_total;
                            restDetails[indxVal].details = place;
                            break;
                        }
                    }
                    for (var arrIndex = 0, arrLen = oldrest_array_addday.length; arrIndex < arrLen; arrIndex++) {
                        if (oldrest_array_addday[arrIndex].plc_id == place.place_id) {
                            oldrest_array_addday[arrIndex].user_ratings_total = place.user_ratings_total;
                            if (place.opening_hours) {
                                oldrest_array_addday[arrIndex].plc_time = place.opening_hours;
                            }
                            else {
                                oldrest_array_addday[arrIndex].plc_time = "No time";
                            }
                            break;
                        }
                    }
                }
                incrRests++;
                GetAllRestDetOnRetrieve();
            }
            else {
                if (status == google.maps.GeocoderStatus.OVER_QUERY_LIMIT) {
                    sleep(1000);
                    GetAllRestDetOnRetrieve();
                }
                else {
                    incrRests++;
                    GetAllRestDetOnRetrieve();
                }
            }
        });
    }
}

function addrestaurant_to_array_addday(places) {
    if (places.rating) {
        if (places.rating >= 1) {
            rest_array_addday.push({
                plc_id: places.place_id,
                plc_name: places.name,
                plc_lat: places.geometry.location.lat(),
                plc_lng: places.geometry.location.lng()
            });
        }
    }
}

function UserpreferenceHotel_addday() {
    var newforaddday_array = foraddday_array.slice();
    var hotelArrayForFilter = [], finalfilterArrayfull_hotel=[];
    for (var loopvarHotels = 0, CitiesHotelsLength = hotelsForCities.length; loopvarHotels < CitiesHotelsLength ; loopvarHotels++) {
        if (hotelsForCities[loopvarHotels].cityName.trim() == cityname_foradd_day.trim()) {
            if (hotelsForCities[loopvarHotels].Hotels.HotelListResponse) {
                if (hotelsForCities[loopvarHotels].Hotels.HotelListResponse.HotelList) {
                    hotelArrayForFilter = hotelsForCities[loopvarHotels].Hotels.HotelListResponse.HotelList['HotelSummary'].slice();
                }
            }
        }
    }
    var hotelId="";

    for (var incrementVar = 0, RetrievedHotels = hotels_retrievedCities.length; incrementVar < RetrievedHotels; incrementVar++)
    {
        if(hotels_retrievedCities[incrementVar].cityname.trim()==cityname_foradd_day.trim())
        {
            hotelId = hotels_retrievedCities[incrementVar].hotelId;
        }
    }
    $.each(hotelArrayForFilter, function (keys, values) {
        if (values.hotelId == hotelId)
        {
            hotelforadday_addday.push({
                plc_id: values.hotelId,
                plc_name: values.name,
                plc_lat: values.latitude,
                plc_lng: values.longitude
            });
        }
    });

    for (var loopvarHotels = 0, CitiesHotelsLength = ActsForCities.length; loopvarHotels < CitiesHotelsLength; loopvarHotels++) {
        if (ActsForCities[loopvarHotels].cityName.trim() == cityname_foradd_day.trim()) {
            activitiesfull_array_addday = ActsForCities[loopvarHotels].Acts.activities.slice(0);
            act_array_addday = ActsForCities[loopvarHotels].Acts.activities.slice(0);
        }
    }

    for (var intriploop = 0, looplen = inthetrip_array_restaurants.length; intriploop < looplen; intriploop++) {
        rest_array_addday = rest_array_addday.filter(function (ele) {
            return ele.place_id !== inthetrip_array_restaurants[intriploop];
        });
    }
    for (var intriploop = 0, looplen = inthetrip_array_restaurants.length; intriploop < looplen; intriploop++) {
        otherrestaurant_addday = otherrestaurant_addday.filter(function (ele) {
            return ele.place_id !== inthetrip_array_restaurants[intriploop];
        });
    }
    for (var intriploop = 0, looplen = inthetrip_array_attractions.length; intriploop < looplen; intriploop++) {
        att_actarray_addday = att_actarray_addday.filter(function (ele) {
            return ele.place_id !== inthetrip_array_attractions[intriploop];
        });
    }
    //for (var intriploop = 0, looplen = inthetrip_array_attractions.length; intriploop < looplen; intriploop++) {
    //    att_actarray_addday = att_actarray_addday.filter(function (ele) {
    //        return ele.idPlc !== inthetrip_array_attractions[intriploop];
    //    });
    //}
    for (var intriploop = 0, looplen = inthetrip_array_attractions.length; intriploop < looplen; intriploop++) {
        secondClassAttr_addday = secondClassAttr_addday.filter(function (ele) {
            return ele.idPlc !== inthetrip_array_attractions[intriploop];
        });
    }
    for (var intriploop = 0, looplen = inthetrip_array_attractions.length; intriploop < looplen; intriploop++) {
        thirdClassAttr_addday = thirdClassAttr_addday.filter(function (ele) {
            return ele.idPlc !== inthetrip_array_attractions[intriploop];
        });
    }
    for (var intriploop = 0, looplen = inthetrip_array_attractions.length; intriploop < looplen; intriploop++) {
        otherattraction_addday = otherattraction_addday.filter(function (ele) {
            return ele.idPlc !== inthetrip_array_attractions[intriploop];
        });
    }
    for (var intriploop = 0, looplen = inthetrip_array_activities.length; intriploop < looplen; intriploop++) {
        act_array_addday = act_array_addday.filter(function (ele) {
            return ele.id !== inthetrip_array_activities[intriploop];
        });
    }
    concatenate_array_addday = att_actarray_addday.concat(secondClassAttr_addday);
    concatenate_array_addday = concatenate_array_addday.concat(otherattraction_addday);
    foraddday_array.push({
        city_name: cityname_foradd_day.slice(),
        hotels: hotelforadday_addday.slice(),
        hotelsfull: hotelArrayForFilter.slice(),
        attractionsfull: attraction_array_addday.slice(),
        FCattractions: att_actarray_addday.slice(),
        SCattractions: secondClassAttr_addday.slice(),
        TCattractions:thirdClassAttr_addday.slice(),
        otherattractions: otherattraction_addday.slice(),
        attractions: concatenate_array_addday.slice(),
        activitiesfull: activitiesfull_array_addday.slice(),
        restaurantsfull: restlist_array_addday.slice(),
        restaurants: rest_array_addday.slice(),
        otherrestaurants: otherrestaurant_addday.slice(),
        activitiesAddDay: act_array_addday.slice()
    });

    if (increForNonExist < non_exists_city.length)
    {
        GetHotelsForRetrieved_cities();
    }
}

function GetHotelsAddDay()
{
    if (increDetAddDay == gotDetAddDay && increOthDetAddDay == gotOthDetAddDay) {
        UserpreferenceHotel_addday();
        GetAttractionForFirst();
        GetHotels("GetHotels");
    }
    else {
        itineraryTimer = setTimeout("GetHotelsAddDay()", 2000);
    }
}

function UserpreferenceActInitialise_addday() {
    var userpref_act = "";
    var filtactcat = "", filtcntact = 0;
    var closest = 0, next_clslat = "", next_dist = "";
    var next_clslng = "", next_place = "";
    var src_lat = "", src_lng = "";
    var distanceis = "";
    var mindist = 99999;
    $.getJSON('../Trip/Getuserpreference', function (data) {
        $.each(data, function (key, values) {
            var spact = user_act.split(',');
            filtcntact = spact.length;
            for (var loopvar = 0, ActivityLength = spact.length; loopvar < ActivityLength; loopvar++) {
                if (spact[loopvar] === "Tours") {
                    filtactcat += "1,172";
                }
                else if (spact[loopvar] === "Concerts_Operas") {
                    filtactcat += "137";
                }
                else if (spact[loopvar] === "Shows_Musicals") {
                    filtactcat += "124,127,128";
                }
                else if (spact[loopvar] === "Meet_the_Locals") {
                    filtactcat += "34";
                }
                else if (spact[loopvar] === "Desert_Safaris") {
                    filtactcat += "37";
                }
                else if (spact[loopvar] === "Ski_Snowboard") {
                    filtactcat += "146";
                }
                else if (spact[loopvar] === "Sport_Events") {
                    filtactcat += "138";
                }
                else if (spact[loopvar] === "Shop") {
                    filtactcat += "20";
                }
                else if (spact[loopvar] === "Spa") {
                    filtactcat += "92,93";
                }
                else if (spact[loopvar] === "Movie") {
                    filtactcat += "126,22";
                }
                else if (spact[loopvar] === "Night") {
                    filtactcat += "111,112";
                }
                else if (spact[loopvar] === "Park") {
                    filtactcat += "36,131,136,159,192";
                }
                else if (spact[loopvar] === "Nooption") {
                    filtactcat += "1,172,137,124,127,128,34,37,146,138,20,92,93,126,22,111,112,36,131,136,159,192";
                }
                if (filtcntact != 0) {
                    filtactcat += "1";
                }
            }
        });
        var centrelat = "";
        var centrelng = "";
        var centrename = "";
        var centreid = "";
        $.ajax({
            type: "GET",
            dataType: "json",
            data: { filterdata: filtactcat },
            url: '../Trip/FilterActivitiesInit',
            success: function (response) {
                var itss = "";
                var tableData = JSON.stringify(response);
                var resdata = $.parseJSON(tableData);
                $.each(resdata, function (keys, values) {
                    // userpref_act+=values.title+"---"+values.lat+"=="+values.lng;
                    centrelat = values.start_Latitude;
                    centrelng = values.start_Longitude;
                    centrename = values.title;
                    centreid = values.id;

                    //*************************REQUIRED********************

                    //if(values.rating>4.9)
                    //{
                    //    att_actarray.push({
                    //        plc_id:"acti||"+centreid,
                    //        plc_name:centrename,
                    //        plc_lat:centrelat,
                    //        plc_lng:centrelng
                    //    });
                    //}
                    //else
                    //{
                    //    otherattraction.push({
                    //        plc_id:"acti||"+centreid,
                    //        plc_name:centrename,
                    //        plc_lat:centrelat,
                    //        plc_lng:centrelng
                    //    });
                    //}

                    //*************************END OF REQUIRED********************

                    activitiesfull_array.push(resdata[keys]);
                });
                if (onretrieve_addday_identify == 0) {
                    startcreatingItinerary();
                }
                else {
                    insert_into_foradd_day();
                }
            },
            error: function (response) {
                if (onretrieve_addday_identify == 0) {
                    startcreatingItinerary();
                }
                else {
                    insert_into_foradd_day();
                }
            }
        });
    });
}

/*************End of Add day on retrieve******************/

function chck_ItnraryExceedsDay()
{
    var incr_num_plcbx = 0, height_of_plcbx = 0, PlcsTm_height = 0;
    var alert_height = "";
    $(".place-box-right").each(function () {
        PlcsTm_height = 0;
        $(this).find(".place-box").each(function () {
            incr_num_plcbx++;
        });
        $(this).find(".place").each(function () {
            PlcsTm_height = PlcsTm_height+$(this).height();
        });
        $(this).find(".travel").each(function () {
            PlcsTm_height = PlcsTm_height + $(this).height();
        });
        PlcsTm_height = PlcsTm_height + ((incr_num_plcbx - 1) * 50);
        if (PlcsTm_height > 2200) {
            var id_todelete = "", height_todelete = 0;
            $(this).find(".travel").each(function () {
                if (height_todelete == 0) {
                    height_todelete = $(this).height();
                    id_todelete = $(this).attr('id');
                }
                else {
                    if (height_todelete < $(this).height()) {
                        height_todelete = $(this).height();
                        id_todelete = $(this).attr('id');
                    }
                }
            });
            alert_height += id_todelete + "\n";
            var attr_of_plc = $("#" + id_todelete).prev(".place").attr("id");
            //$("#" + id_todelete).next(".place").remove();
            deleteOnExceed = 1;
            $("#" + id_todelete).prev(".place").find(".deleteplace").click();
            //$("#" + id_todelete).remove();
            alert_height += attr_of_plc + "\n";
        }
        else {
            alert_height += "Fits\n";
        }
    });
}

function findCanFitPlace(stopplcbxrit)
{
    var incr_num_plcbx = 0, PlcsTm_height = 0;
    $("#" + stopplcbxrit).find(".place-box").each(function () {
            incr_num_plcbx++;
        });
    $("#" + stopplcbxrit).find(".place").each(function () {
            PlcsTm_height = PlcsTm_height + $(this).height();
        });
    $("#" + stopplcbxrit).find(".travel").each(function () {
            PlcsTm_height = PlcsTm_height + $(this).height();
        });
        PlcsTm_height = PlcsTm_height + ((incr_num_plcbx - 1) * 50);
        if (PlcsTm_height - 135 > 2150) {
            return 0;
        }
        else {
            return 1;
        }
}