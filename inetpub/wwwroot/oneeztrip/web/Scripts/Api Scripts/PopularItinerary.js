﻿function RetrieveFrmPopularItin() {
    setNumberForDays();
    var PlanId = $("#TopItinryId").val();
    var days_num = "", plcs_ids = "", plcs_names = "", plcs_lat = "", plcs_lons = "", plcs_addr = "", plcs_rat = "", plcs_types = "", plcs_imgs = "", tm_type = "", tm_dur = "";
    $.getJSON('../CMSPage/GetPackage', { PlanID: PlanId }, function (response) {
        if (response.data != "No Result") {
            packagedata = response;
            var increTravelMode = 0;
            $.each(packagedata, function (keys, values) {
                plcs_ids = values.Spot_Id.split('||');
                plcs_names = values.Spot_Name.split('||');
                plcs_lat = values.Spot_Latitude.split('||');
                plcs_lons = values.Spot_Longitude.split('||');
                plcs_addr = values.Spot_Address.split('||');
                plcs_rat = values.Spot_Rating.split('||');
                plcs_types = values.Spot_Category.split('||');
                plcs_imgs = values.thumbnail_URL.split('|image|');
                tm_type = values.Travel_Mode.split('||');
                tm_dur = values.Travel_time.split('||');
                days_num = values.Day_Number;
                var itineraryData = "";
                var att_or_res = 0;
                for (var loopvar = 0, PlacesLength = plcs_ids.length; loopvar < PlacesLength; loopvar++) {
                    if (plcs_ids[loopvar] != "") {
                        itineraryData += "<div class='place' id='" + plcs_ids[loopvar] + "'>";
                        if (plcs_types[loopvar] == "Attraction.") {
                            itineraryData += "<div class='place-details' id=" + plcs_ids[loopvar] + " style ='height:200px;'>";
                        }
                        else {
                            itineraryData += "<div class='place-details' id=" + plcs_ids[loopvar] + " style ='height:100px;'>";
                        }
                        itineraryData += "<input type='hidden' value=" + plcs_lat[loopvar] + " class='placeatt_lat'/><input type='hidden' value=" + plcs_lons[loopvar] + " class='placeatt_lng'/><input type='hidden' value=" + plcs_names[loopvar] + " class='placeatt_name'/>";
                        itineraryData += "<figure class='place-img'>";
                        itineraryData += "<img src='" + plcs_imgs[loopvar] + "' /></figure><div class='place-cont'>";
                        itineraryData += "<div class='p-left'><h4>" + plcs_names[loopvar] + "</h4><p style='height:26px; overflow-y:hidden;'>" + plcs_addr[loopvar] + "</p><input type='hidden' class='hidlat' value='" + plcs_lat[loopvar] + "'/><input type='hidden' class='hidlng' value='" + plcs_lons[loopvar] + "'/>";
                        itineraryData += "<span>" + plcs_types[loopvar] + "</span>";
                        if (plcs_types[loopvar] != "Hotel.") {
                            itineraryData += "<br><span id='sp_" + plcs_ids[loopvar] + "' class='sphrs'>";
                        }
                        itineraryData += "</div><div class='p-right'>";
                        itineraryData += "<ul><li><a href='#'><img src='../Content/images/del-icon.png' alt='' class='deleteplace' /></a></li>";
                        if (plcs_types[loopvar] == "Restaurant.") {
                            att_or_res = 1;
                            itineraryData += "<li><a href='#infowindow' class='fancybox' onclick='return infoattraction(\"" + plcs_ids[loopvar] + "\",\"" + plcs_lat[loopvar] + "\",\"" + plcs_lons[loopvar] + "\",\"" + att_or_res + "\");' ><img src='../Content/images/info-icon.png' alt='' /></a></li> </ul>";
                            var found = jQuery.inArray(plcs_ids[loopvar], inthetrip_array_restaurants);
                            if (found == -1) {
                                inthetrip_array_restaurants.push(plcs_ids[loopvar]);
                            }
                        }
                        else if (plcs_types[loopvar] == "Attraction.") {
                            att_or_res = 2;
                            itineraryData += "<li><a href='#infowindow' class='fancybox' onclick='return infoattraction(\"" + plcs_ids[loopvar] + "\",\"" + plcs_lat[loopvar] + "\",\"" + plcs_lons[loopvar] + "\",\"" + att_or_res + "\");' ><img src='../Content/images/info-icon.png' alt='' /></a></li> </ul>";
                            var found = jQuery.inArray(plcs_ids[loopvar], inthetrip_array_attractions);
                            if (found == -1) {
                                inthetrip_array_attractions.push(plcs_ids[loopvar]);
                            }
                        }
                        else if (plcs_types[loopvar] == "Hotel.") {
                            itineraryData += "<li><a href='#infowindow' class='fancybox' onclick='return infohotel(\"" + plcs_ids[loopvar] + "\");' ><img src='../Content/images/info-icon.png' alt='' /></a></li> </ul>";
                            var found = jQuery.inArray(plcs_ids[loopvar], inthetrip_array_hotels);
                            if (found == -1) {
                                inthetrip_array_hotels.push(plcs_ids[loopvar]);
                            }
                            var CitiesCnt = 0;
                            for (var loopvariable = 0, RetrievedHotels = hotels_retrievedCities.length; loopvariable < RetrievedHotels; loopvariable++) {
                                if (hotels_retrievedCities[loopvariable].cityname.trim() == values.City_Name.trim()) {
                                    CitiesCnt++;
                                }
                            }
                            if (CitiesCnt == 0) {
                                hotels_retrievedCities.push({
                                    cityname: values.City_Name,
                                    hotelId: plcs_ids[loopvar]
                                })
                            }
                        }                        
                        itineraryData += "</div> </div> </div>";
                        itineraryData += "</div>";
                        if (tm_type[loopvar].trim() !== "No travelmode") {
                            increTravelMode++;
                            itineraryData += "<div class='travel' id='ltra_" + increTravelMode + "'><a href='#' style='float:right;'><img src='../Content/images/del_travel.png' class='travedelimg'/></a>";
                            itineraryData += "<p>Travel time by " + tm_type[loopvar] + " is " + tm_dur[loopvar] + "<img src='../Content/images/edit.png' alt='' class='editimg' style='height: 15px;width: 15px;'></p>";
                            itineraryData += "<ul>";
                            if (tm_type[loopvar] == "car") {
                                itineraryData += "<li><input type='hidden' class='hidcar' value='" + tm_dur[loopvar] + "'/><a href='#' data-value='car' class='active'><img src='../Content/images/car-icon.png' alt=''/></a></li>";
                            }
                            else {
                                itineraryData += "<li><input type='hidden' class='hidcar' value=''/><a href='#' data-value='car'><img src='../Content/images/car-icon.png' alt=''/></a></li>";
                            }
                            if (tm_type[loopvar] == "train") {
                                itineraryData += "<li><input type='hidden' class='hidtrain'  value='" + tm_dur[loopvar] + "'/><a href='#' data-value='train' class='active'><img src='../Content/images/train-icon.png' alt='' /></a></li>";
                            }
                            else {
                                itineraryData += "<li><input type='hidden' class='hidtrain'  value=''/><a href='#' data-value='train'><img src='../Content/images/train-icon.png' alt='' /></a></li>";
                            }
                            if (tm_type[loopvar] == "plane") {
                                itineraryData += "<li><input type='hidden' class='hidplane'  value='" + tm_dur[loopvar] + "'/><a href='#' data-value='plane' class='active'><img src='../Content/images/plane-icon.png' alt='' /></a></li>";
                            }
                            else {
                                itineraryData += "<li><input type='hidden' class='hidplane'  value=''/><a href='#' data-value='plane'><img src='../Content/images/plane-icon.png' alt='' /></a></li>";
                            }
                            if (tm_type[loopvar] == "walk") {
                                itineraryData += "<li><input type='hidden' class='hidwalk'  value='" + tm_dur[loopvar] + "'/><a href='#' data-value='walk' class='active'><img src='../Content/images/walk-icon.png' alt='' /></a></li>";
                            }
                            else {
                                itineraryData += "<li><input type='hidden' class='hidwalk'  value=''/><a href='#' data-value='walk'><img src='../Content/images/walk-icon.png' alt='' /></a></li>";
                            }
                            if (tm_type[loopvar] == "bicycle") {
                                itineraryData += "<li class='last'><input type='hidden' class='hidcycle' value='" + tm_dur[loopvar] + "'/><a href='#' data-value='bicycle' class='active'><img src='../Content/images/scooter-icon.png' alt='' /></a></li>";
                            }
                            else {
                                itineraryData += "<li class='last'><input type='hidden' class='hidcycle' value=''/><a href='#' data-value='bicycle'><img src='../Content/images/scooter-icon.png' alt='' /></a></li>";
                            }
                            itineraryData += "<input type='hidden' class='hiddriving' value=''/></ul></div>";
                        }
                    }
                }
                $(".place-box-right").eq(values.Day_Number - 1).find('.place-box:first').append(itineraryData);
            });
        }
        $('#wholepart').unblock();
        Getplaces_details_forsave();
        localStorage.setItem('IsSavedinDB5', 'YES');
        //LoadImageEnd();
        onresizingplc();
        setTimeout(function () { CreatePackageForAddcity(); }, 5000);
        var size = $('#compl').find('.travel').length;
        TravelDivResize(size);
        var customTravelsize = $('#compl').find('.custom_travel').length;
        CustomTravelDivResize(customTravelsize);
        intheTripRHAA("Hotel");
    });
}

function setNumberForDays() {
    var incre_daynum = 1;
    $('.city-row').each(function () {
        var day_number = 0;
        if (incre_daynum >= 10) {
            day_number = incre_daynum;
        }
        else {
            day_number = "0" + incre_daynum;
        }
        $(this).find('.trip-date').find('.btn').text(day_number);
        incre_daynum++;
    });
}