﻿function InitialiseFacebook(appId) {

    window.fbAsyncInit = function () {
        FB.init({
            appId: appId,
            status: true,
            cookie: true,
            xfbml: true
        });

        FB.Event.subscribe('auth.login', function (response) {
            var credentials = { uid: response.authResponse.userID, accessToken: response.authResponse.accessToken };
            alert(response.authResponse.userID);
            SubmitLogin(credentials);
        });

        FB.getLoginStatus(function (response) {
            if (response.status === 'connected') {
                var notify_alert = noty({
                    layout: 'topRight',
                    text: 'Logged into fb',
                    type: 'success',
                    timeout: 3000,
                    maxVisible: 1,
                    animation: {
                        open: { height: 'toggle' },
                        close: { height: 'toggle' },
                        easing: 'swing',
                        speed: 0
                    }
                });
                //alert("user is logged into fb");
            }
            else if (response.status === 'not_authorized') {
                var notify_alert = noty({
                    layout: 'topRight',
                    text: 'User is not authorised',
                    type: 'success',
                    timeout: 3000,
                    maxVisible: 1,
                    animation: {
                        open: { height: 'toggle' },
                        close: { height: 'toggle' },
                        easing: 'swing',
                        speed: 0
                    }
                });
                //alert("user is not authorised");
            }
            else {
                var notify_alert = noty({
                    layout: 'topRight',
                    text: 'User is not conntected to facebook',
                    type: 'success',
                    timeout: 3000,
                    maxVisible: 1,
                    animation: {
                        open: { height: 'toggle' },
                        close: { height: 'toggle' },
                        easing: 'swing',
                        speed: 0
                    }
                });
                //alert("user is not conntected to facebook");
            }

        });

        function SubmitLogin(credentials) {
            $.ajax({
                url: "/trip/index",
                type: "POST",
                data: credentials,
                error: function () {
                    alert("error logging in to your facebook account.");
                },
                success: function () {

                    window.location.reload();
                }
            });
        }

    };

    (function (d) {
        var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement('script');
        js.id = id;
        js.async = true;
        js.src = "//connect.facebook.net/en_US/all.js";
        ref.parentNode.insertBefore(js, ref);
    }(document));

}