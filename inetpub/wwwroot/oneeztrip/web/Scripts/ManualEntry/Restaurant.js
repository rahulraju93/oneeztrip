﻿$(document).ready(function () {
    $("#btnRestypeDelete").hide();
});

function RestaurantType() {
    if ($.trim($("#txtrestype").val()) == "") {
        $("#spnrestaurant").text("Please Enter Restaurant type");
        return false;
    };
    var ManualEntry = {
        Type: $("#txtrestype").val(),
        Isactive: true
    };
    $.ajax({
        url: '../CMSpage/SaveResType',
        type: 'POST',
    async: false,
    data: {
        data: ManualEntry
    },
    success: function (data) {
        var notify_alert = noty({
            layout: 'topRight',
            text: 'Saved successfully',
            type: 'success',
            timeout: 3000,
            maxVisible: 1,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            }
        });       
        RestType();
    },
    error: function (result) {
        var notify_alert = noty({
            layout: 'topRight',
            text: 'Oops! Something went wrong, please try again some other time..',
            type: 'warning',
            timeout: 3000,
            maxVisible: 1,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            }
        });        
    }
});
$("#spnrestaurant").text('');
$("#txtrestype").val('');
}

function RestPlus() {
    $('#btnRestypeplus').hide();
    $('#divrestype').show();
    $('#btnRestypeminus').show();
    $("#btnRestypeDelete").hide();
}

function RestMinus() {
    $('#btnRestypeplus').show();
    $('#divrestype').hide();
    $('#btnRestypeminus').hide();
    $("#btnRestypeDelete").show();
}

/******************Save Restaurant Details******************/
function DDLResType() {
    $.getJSON('../CMSPage/ResTypeDropdown', {}, function (data) {
        var item = '';
        $("#ddlRestype").append($("<option></option>").val("0").html("---Restuarant Type---"));

        $.each(data, function (i, item) {
            $("#ddlRestype").append($("<option></option>").val(item.ResTypeID).html(item.Cuisine_Type));
        });
    });
}

function RestType() {
    $("#ddlRestype").empty();
    DDLResType();
}

function btnsaveRest() {
    $('#divRestaurant').show();
    $('#divResshow').hide();
    $('#divResSave').show();
    $('#btnRestshow').show();
    $('#btnRestAdd').hide();
    $('#btnRestUpdate').hide();
    $('#btnRestaurantsave').show();
    $('#imgRestaurant').attr('src', '');
    $('input#txtRestaurant').val('');
    $('input#txtRestrating').val('');
    $('input#txtMontime').val('');
    $('input#txtTuetime').val('');
    $('input#txtWedtime').val('');
    $('input#txtThurtime').val('');
    $('input#txtFriday').val('');
    $('input#txtSaturday').val('');
    $('input#txtSunday').val('');
    $('#txtResDescription').val('');
    $('#txtResAddress').val('');
    $('input#txtRescity').val('');
    $('input#txtReslatitude').val('');
    $('input#txtReslongitude').val('');

    $('input#txtlatitudeResta').val('');
    $('input#txtlongitudeResta').val('');
    $('input#txtResprice').val('');
    $('input#txtResUrl').val('');
    $('#chkRestauractive').prop('checked', false);
    $('#ddlRestype').val("0");
}

function btnshowRest() {
    $('#divRestaurant').show();
    $('#divResshow').show();
    $('#divResSave').hide();
    $('#btnRestshow').hide();
    $('#btnRestAdd').show();
}


function SaveRestaurant() {
    var formData = new FormData();
    var totalFiles = document.getElementById("fileToUpload").files.length;
    for (var i = 0; i < totalFiles; i++) {
        var file = document.getElementById("fileToUpload").files[i];
        var Extension = file.type.split('/').pop();
        formData.append("fileActupload", file);
        if (Extension == "bmp" || Extension == "gif" || Extension == "png" || Extension == "jpg" || Extension == "jpeg") {
            $.ajax({
                cach: false,
                async: false,
                type: "POST",
                url: '../CMSPage/RestaurantUpload',
                data: formData,
                dataType: 'json',
                contentType: false,
                processData: false,
                success: function (response) {
                },
                error: function (error) {
                }
            });
        }
        else {
            var notify_alert = noty({
                layout: 'topRight',
                text: 'Invalid File!..',
                type: 'warning',
                timeout: 3000,
                maxVisible: 1,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                }
            });
            return false;
        }
    }

    if ($.trim($("#txtRestaurant").val()) == "") {
        $("#spnrestaurant").text("Please Enter Restaurant Name");
        return false;
    }
    if ($.trim($("#txtRestrating").val()) == "") {
        $("#spnrestaurant").text("Please Enter Restaurant Rating");
        return false;
    }
    if ($.trim($("#ddlRestype").val()) == "0") {
        $("#spnrestaurant").text("Please Select Restaurant Type");
        return false;
    }

    if ($.trim($("#txtRescity").val()) == "") {
        $("#spnrestaurant").text("Please Enter City");
        return false;
    }
    if ($.trim($("#txtReslatitude").val()) == "") {
        $("#spnrestaurant").text("Please Enter Latitude ");
        return false;
    }
    if ($.trim($("#txtReslongitude").val()) == "") {
        $("#spnrestaurant").text("Please Enter Longitude");
        return false;
    }
    if (document.getElementById("fileToUpload").files.length == 0) {
        $("#spnrestaurant").text("Please Add Restaurant Image");
        return false;
    }
    var mon = $("#txtMontime").val(); var tue = $("#txtTuetime").val(); var wed = $("#txtWedtime").val();
    var thur = $("#txtThurtime").val(); var fri = $("#txtFriday").val(); var sat = $("#txtSaturday").val();
    var sun = $("#txtSunday").val();
    var Days = mon.concat('/' + tue + '/' + wed + '/' + thur + '/' + fri + '/' + sat + '/' + sun);

    var ManualEntry = {
        Name: $('[id$=txtRestaurant]').val(),
        User_rating: $('[id$=txtRestrating]').val(),
        Type: $("#ddlRestype option:selected").text(),
        OpeningTime: Days,
        Address: $('[id$=txtResAddress]').val(),
        City: $('[id$=txtRescity]').val(),
        description: $('[id$=txtResDescription]').val(),
        Latitude: $('[id$=txtReslatitude]').val(),
        Longitude: $('[id$=txtReslongitude]').val(),
        Hotellatitude: $('[id$=txtlatitudeResta]').val(),
        Hotellongitude: $('[id$=txtlongitudeResta]').val(),
        Imagepath: $("#fileToUpload").val(),
        Price: $('[id$=txtResprice]').val(),
        URL: $('[id$=txtResUrl]').val(),
        Isactive: $("#chkRestauractive").prop('checked')
    };

    $.ajax({
        url: '../CMSpage/SaveRestaurant',
        type: 'POST',
    async: false,
    data: {
        data: ManualEntry
    },
    success: function (data) {

        var notify_alert = noty({
            layout: 'topRight',
            text: 'Saved Successfully..!',
            type: 'success',
            timeout: 3000,
            maxVisible: 1,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            }
        });
        setTimeout(function () {
            window.location.reload(1);
        }, 1000);
    },
    error: function (result) {
        var notify_alert = noty({
            layout: 'topRight',
            text: 'Failed to save the record...',
            type: 'warning',
            timeout: 3000,
            maxVisible: 1,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            }
        });
       
    }

});
$('input#txtRestrating').val(''); $('input#txtRestaurant').val('');
$('input#txtAttrRating').val(''); $('input#txtResreview').val(''); $('input#txtResType').val(''); $('input#txtRestime').val('');
$('#txtResAddresss').val(''); $('input#txtRescountry').val(''); $('#txtResDescription').empty(); $('input#txtReslatitude').val('');
$('input#txtResprice').val(''); $('input#spnrestaurant').val('');
$('#chkRestauractive').prop('checked', false); $("#ddlRestype").val(0); $("#txtRescity").val(""); $("#ddlRescountry").val(0);
var input = $("#fileToUpload"); $('[id$=lblRest]').val("").text(""); $('input#txtReslongitude').val('');
input.replaceWith(input.val('').clone(true));
$('input#txtMontime').val(''); $('input#txtTuetime').val(''); $('input#txtWedtime').val(''); $('input#txtThurtime').val('');
$('input#txtFriday').val(''); $('input#txtSaturday').val(''); $('input#txtSunday').val(''); $('input#txtlatitudeResta').val('');
$('input#txtlongitudeResta').val(''); $('input#txtResUrl').val('');
}

/******************Edit Table******************/
function EditRestuarant(RowId) {
    var RestId = $("#tblRestheading").jqxGrid('getrowdata', RowId).RestaurantId;
    $('#btnRestUpdate').show();
    $('#btnRestaurantsave').hide();
    $('#divRestaurant').show();
    $('#divResshow').hide();
    $('#divResSave').show();
    $('#btnRestshow').show();
    $('#btnRestAdd').hide();
    $('[id$=hidRest]').val(RestId);
    $.getJSON('../CMSPage/GetRestuarantId', { UserName: RestId }, function (data) {
        $.each(data, function (i, d) {
            var RestName = d.Name;
            var RestRating = d.Guest_Rating;
            var RestType = d.RestaurantType;
            var Description = d.Description;
            var Address = d.Address;
            var City = d.City;
            var Latitude = d.Latitude;
            var Longitude = d.Longitude;
            var Res_Latitude = d.Restaurant_Latitude;
            var Res_Longitude = d.Restaurant_Longitude;
            var Image = "../Content/images/RestaurantImage/" + d.Image_Name;
            var Imagename = Image.slice(34, 120);
            var Price = d.Price;
            var Url = d.Booking_Url;
            var Isactive = d.Isactive;

            $('[id$=txtRestaurant]').val(RestName);
            $('[id$=txtRestrating]').val(RestRating);
            var SplitResult = d.Openingtime.split("/");
            var monday = SplitResult[0];
            var tuesday = SplitResult[1];
            var wednesday = SplitResult[2];
            var thursday = SplitResult[3];
            var friday = SplitResult[4];
            var saturday = SplitResult[5];
            var sunday = SplitResult[6];
            if (monday == null) {
                $('[id$=txtMontime').val('');
            }
            else {
                $('[id$=txtMontime').val(monday)
            }
            if (tuesday == null) {
                $('[id$=txtTuetime').val('');

            }
            else {
                $('[id$=txtTuetime').val(tuesday);
            }
            if (wednesday == null) {
                $('[id$=txtWedtime').val('');

            }
            else {
                $('[id$=txtWedtime').val(wednesday);
            }
            if (thursday == null) {
                $('[id$=txtThurtime').val('');

            }
            else {
                $('[id$=txtThurtime').val(thursday);
            }
            if (friday == null) {
                $('[id$=txtFriday').val('');

            }
            else {
                $('[id$=txtFriday').val(friday);
            }
            if (saturday == null) {
                $('[id$=txtSaturday').val('');

            }
            else {
                $('[id$=txtSaturday').val(saturday);
            }
            if (sunday == null) {
                $('[id$=txtSunday').val('');

            }
            else {
                $('[id$=txtSunday').val(sunday);
            }
            var type = document.getElementById("ddlRestype");
            for (var i = 0; i < type.options.length; i++) {
                if (type.options[i].text == RestType) {
                    type.selectedIndex = i;
                    break;
                }
            }
            $('[id$=txtRestime]').val(d.Openingtime);
            $('[id$=txtResAddress]').val(Address);
            $('[id$=txtResDescription]').val(Description);
            $('[id$=txtRescity]').val(City),
            $('[id$=txtReslatitude]').val(Latitude);
            $('[id$=txtReslongitude]').val(Longitude);
            $('[id$=txtlatitudeResta]').val(Res_Latitude);
            $('[id$=txtlongitudeResta]').val(Res_Longitude)
            $("#imgRestaurant").attr("src", Image);
            $('[id$=lblRest]').val(Imagename).text(Imagename);
            $('[id$=txtResprice]').val(Price);
            $('[id$=txtResUrl]').val(Url)
            if (Isactive == "true") {
                $('#chkRestauractive').prop('checked', Isactive);
            }
            else {
                $('#chkRestauractive').prop('checked', Isactive);
            }
        });
    });
}
/******************End Edit******************/

/******************Update Restaurant******************/
function UpdateRestuarant() {
    var formData = new FormData();
    var totalFiles = document.getElementById("fileToUpload").files.length;
    for (var i = 0; i < totalFiles; i++) {
        var file = document.getElementById("fileToUpload").files[i];
        formData.append("fileToUpload", file);
    }
    $.ajax({
        cach: false,
        async: false,
        type: "POST",
        url: '../CMSPage/RestaurantUpload',
        data: formData,
        dataType: 'json',
        contentType: false,
        processData: false,
        success: function (response) {
        },
        error: function (error) {
        }
    });

    if ($.trim($("#txtRestaurant").val()) == "") {
        $("#spnrestaurant").text("Please Enter Restaurant Name");
        return false;
    }
    if ($.trim($("#txtRestrating").val()) == "") {
        $("#spnrestaurant").text("Please Enter Restaurant Rating");
        return false;
    }
    if ($.trim($("#ddlRestype").val()) == "0") {
        $("#spnrestaurant").text("Please Select Restaurant Type");
        return false;
    }          
    if ($.trim($("#txtRescity").val()) == " ") {
        $("#spnrestaurant").text("Please Enter City");
        return false;
    }
    if ($.trim($("#txtReslatitude").val()) == "") {
        $("#spnrestaurant").text("Please Enter Latitude ");
        return false;
    }
    if ($.trim($("#txtReslongitude").val()) == "") {
        $("#spnrestaurant").text("Please Enter Longitude");
        return false;
    }
    var mon = $("#txtMontime").val(); var tue = $("#txtTuetime").val(); var wed = $("#txtWedtime").val();
    var thur = $("#txtThurtime").val(); var fri = $("#txtFriday").val(); var sat = $("#txtSaturday").val();
    var sun = $("#txtSunday").val();
    var Days = mon.concat('/' + tue + '/' + wed + '/' + thur + '/' + fri + '/' + sat + '/' + sun);
    var ManualEntry = {
        Id: $("#hidRest").val(),
        Name: $("#txtRestaurant").val(),
        Hotel_rating: $("#txtRestrating").val(),
        Type: $("#ddlRestype option:selected").text(),
        OpeningTime: Days,
        Address: $("#txtResAddress").val(),
        City: $("#txtRescity").val(),
        description: $("#txtResDescription").val(),
        Latitude: $("#txtReslatitude").val(),
        Longitude: $("#txtReslongitude").val(),
        Hotellatitude: $('[id$=txtlatitudeResta]').val(),
        Hotellongitude: $('[id$=txtlongitudeResta]').val(),
        Imagepath: $('[id$=lblRest]').val(),
        Price: $('[id$=txtResprice]').val(),
        URL: $('[id$=txtResUrl]').val(),
        Isactive: $("#chkRestauractive").prop('checked')
    };
    $.ajax({
        url: '../CMSpage/UpdateRestuarant',
        type: 'POST',
    async: false,
    data: {
        data: ManualEntry
    },
    success: function (data) {
        var notify_alert = noty({
            layout: 'topRight',
            text: 'Record Updated Successfully..!',
            type: 'success',
            timeout: 3000,
            maxVisible: 1,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            }
        });
        setTimeout(function () {
            window.location.reload(1);
        }, 1000);
    },
    error: function (result) {
        var notify_alert = noty({
            layout: 'topRight',
            text: 'Failed to update the record...',
            type: 'warning',
            timeout: 3000,
            maxVisible: 1,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            }
        });       
    }
});
$('input#txtRestrating').val(''); $('input#txtRestaurant').val('');
$('input#txtAttrRating').val(''); $('input#txtResreview').val(''); $('input#txtResType').val(''); $('input#txtRestime').val('');
$('#txtResAddress').val(''); $('input#txtRescountry').val(''); $('#txtResDescription').val(''); $('input#txtReslatitude').val('');
$('input#txtResprice').val(''); $('input#spnrestaurant').val('');
$('#chkRestauractive').prop('checked', false); $("#ddlRestype").val(0); $("#txtRescity").val(""); $("#ddlRescountry").val(0);
var input = $("#fileToUpload"); $('[id$=lblRest]').val("").text(""); $('input#txtReslongitude').val('');
input.replaceWith(input.val('').clone(true));
$('input#txtMontime').val(''); $('input#txtTuetime').val(''); $('input#txtWedtime').val(''); $('input#txtThurtime').val('');
$('input#txtFriday').val(''); $('input#txtSaturday').val(''); $('input#txtSunday').val(''); $('input#txtlatitudeResta').val('');
$('input#txtlongitudeResta').val(''); $('input#txtResUrl').val(''); $("#imgRestaurant").attr("src", "");
}

/******************End Update******************/

/******************Delete Restuarant Details******************/

function DeleteRestDetails(RowId) {    
    noty({
        layout: 'topRight',
        type: 'confirm',
        maxVisible: 1,
        modal: true,
        text: 'Are you sure you want to delete the selected restaurant?',
        buttons: [
            {
                addClass: 'btn btn-primary', text: 'Ok', onClick: function ($noty) {
                    $noty.close();
                    var Rest_Id = $("#tblRestheading").jqxGrid('getrowdata', RowId).RestaurantId;;
                    $.ajax({
                        url: '../CMSpage/DeleteRestuarant',
                        type: 'POST',
                        async: false,
                        data: {
                            Id: Rest_Id
                        },
                        success: function (data) {
                            var notify_alert = noty({
                                layout: 'topRight',
                                text: 'Record deleted successfully...!',
                                type: 'success',
                                timeout: 3000,
                                maxVisible: 1,
                                animation: {
                                    open: { height: 'toggle' },
                                    close: { height: 'toggle' },
                                    easing: 'swing',
                                    speed: 500
                                }
                            });

                            $('#List').trigger('change');

                            //setTimeout(function () {
                            //    window.location.reload(1);
                            //}, 1000);

                        },
                        error: function (result) {
                            var notify_alert = noty({
                                layout: 'topRight',
                                text: 'Failed to delete the record...!',
                                type: 'warning',
                                timeout: 3000,
                                maxVisible: 1,
                                animation: {
                                    open: { height: 'toggle' },
                                    close: { height: 'toggle' },
                                    easing: 'swing',
                                    speed: 500
                                }
                            });

                        }
                    });
                }
            },
            {
                addClass: 'btn btn-danger', text: 'Cancel', onClick: function ($noty) {
                    $noty.close();
                    return false;
                }
            }
        ]
    });  
}

function notyDeleteResAlert() {
    var restaurentType = $("#ddlRestype");
    var restaurentTypeIndex = $("#ddlRestype option:selected").index();
    var restaurentTypeValue = $("#ddlRestype option:selected").val();
    if (restaurentTypeIndex == 0) {
        var notify_alert = noty({
            layout: 'topRight',
            text: 'Please select restaurent type',
            type: 'warning',
            timeout: 3000,
            maxVisible: 1,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            }
        });
    }
    else {
        noty({
            layout: 'topRight',
            type: 'confirm',
            text: 'Are you confirm to delete the Restaurent?',
            buttons: [
                {
                    addClass: 'btn btn-primary', text: 'Yes', onClick: function ($noty) {

                        DeleteRestaurentType();

                        $noty.close();

                    }
                },
                {
                    addClass: 'btn btn-danger', text: 'No', onClick: function ($noty) {
                        $noty.close();

                    }
                }
            ]
        });
    }
}

function DeleteRestaurentType() {
    var restaurentType = $("#ddlRestype");
    var restaurentTypeIndex = $("#ddlRestype option:selected").index();
    var restaurentTypeValue = $("#ddlRestype option:selected").val();
    $.ajax({
        url: "../CMSPage/DeleteRestaurentType",
        type: "POST",
        async: false,
        data: JSON.stringify({ RestaurentTypeID: restaurentTypeValue }),
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        success: function (index, data) {
            RestType();
            $("#btnRestypeDelete").hide();
            var notify_alert = noty({
                layout: 'topRight',
                text: 'Restaurent type deleted successfully',
                type: 'warning',
                timeout: 3000,
                maxVisible: 1,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                }
            });
        },
        error: function (error) {
        }
    });
}

/******************Delete Restuarant Details Ends******************/

function SelectedIndexResChange() {
    var restaurentType = $("#ddlRestype");
    var restaurentTypeIndex = $("#ddlRestype option:selected").index();
    var restaurentTypeValue = $("#ddlRestype option:selected").val();
    if (restaurentTypeIndex > 0) {
        $("#btnRestypeDelete").show();
    }
}