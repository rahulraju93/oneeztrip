﻿/*****************Save Hotel Details Start*****************/
function btnsavehotel() {
    $('#divhotel').show();
    $('#divhotelshow').hide();
    $('#divhotelsave').show();
    $('#btnhotelshow').show();
    $('#btnhotelAdd').hide();
    $('#btnhotelupdate').hide();
    $('#btnhotelsave').show();
    $('input#txtUserrating').val(''); $('input#txtrating').val(''); $('input#txtHotelName').val(''); $('#txtaddress').val('');
    $('input#txtcity').val(''); $('input#txtcountry').val(''); $('#txtdescription').val(''); $('input#txtlatitude').val('');
    $('input#txtprice').val(''); $("#ddlcity").val(0); $("#txtlongitude").val(''); $("#spnarea").text('');
    $("#ddlcountry").val(0); $('input#txtlongitude').val('');
    $('#chkactive').prop('checked', false); $('#txtAmenties').val(''); $('input#txthotelUrl').val('');
    $('input#txthotellatitude').val(''); $('input#txthotellongitude').val('');
    $('#imghotel').attr('src', '');
    var input = $("#file"); $("#imghotel").attr("src", "");
    input.replaceWith(input.val('').clone(true));
}

function btnshowhotel() {
    $('#divhotel').show();
    $('#divhotelshow').show();
    $('#divhotelsave').hide();
    $('#btnhotelshow').hide();
    $('#btnhotelAdd').show();
    $('input#txtUserrating').val(''); $('input#txtrating').val(''); $('input#txtHotelName').val(''); $('#txtaddress').val('');
    $('input#txtcity').val(''); $('input#txtcountry').val(''); $('#txtdescription').val(''); $('input#txtlatitude').val('');
    $('input#txtprice').val(''); $("#ddlcity").val(0); $("#txtlongitude").val(''); $("#spnarea").text('');
    $("#ddlcountry").val(0); $('input#txtlongitude').val('');
    $('#chkactive').prop('checked', false); $('#txtAmenties').val(''); $('input#txthotelUrl').val('');
    $('input#txthotellatitude').val(''); $('input#txthotellongitude').val('');
    var input = $("#file"); $("#imghotel").attr("src", "");
    input.replaceWith(input.val('').clone(true));
}

function Savehotel() {
    var formData = new FormData();
    var totalFiles = document.getElementById("file").files.length;
    for (var i = 0; i < totalFiles; i++) {
        var file = document.getElementById("file").files[i];
        var Extension = file.type.split('/').pop();
        formData.append("file", file);
        if (Extension == "bmp" || Extension == "gif" || Extension == "png" || Extension == "jpg" || Extension == "jpeg") {
            $.ajax({
                cach: false,
                async: false,
                type: "POST",
                url: '../CMSPage/Upload',
                data: formData,
                dataType: 'json',
                contentType: false,
                processData: false,
                success: function () {

                },
                error: function (error) {

                }
            });
        }
        else {
            var notify_alert = noty({
                layout: 'topRight',
                text: 'Invalid File!..',
                type: 'warning',
                timeout: 3000,
                maxVisible: 1,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                }
            });
            return false;
        }
    }

    if ($.trim($("#txtHotelName").val()) == "") {
        $("#spnarea").text("Please Enter Hotel Name");
        return false;
    }
    if ($.trim($("#txtaddress").val()) == "") {
        $("#spnarea").text("Please Enter Address");
        return false;
    }
    if ($.trim($("#txtcity").val()) == "") {
        $("#spnarea").text("Please Enter city name");
        return false;
    }
    if ($.trim($("#txtlatitude").val()) == "") {
        $("#spnarea").text("Please Enter Latitude");
        return false;
    }
    if ($.trim($("#txtlongitude").val()) == "") {
        $("#spnarea").text("Please Enter Longitude");
        return false;
    }
    if ($.trim($("#txthotellatitude").val()) == "") {
        $("#spnarea").text("Please Enter Hotel Latitude");
        return false;
    }
    if ($.trim($("#txthotellongitude").val()) == "") {
        $("#spnarea").text("Please Enter Hotel Longitude");
        return false;
    }
    if (document.getElementById("file").files.length == 0) {
        $("#spnarea").text("Please Add Hotel Image");
        return false;
    }
    var ManualEntry = {
        Id: $('[id$=htnhotelId]').val(),
        Name: $('[id$=txtHotelName]').val(),
        Hotel_rating: $('[id$=txtrating]').val(),
        User_rating: $('[id$=txtUserrating]').val(),
        Amenities: $('[id$=txtAmenties]').val(),
        Address: $('[id$=txtaddress]').val(),
        Description: $('[id$=txtdescription]').val(),
        City: $('[id$=txtcity]').val(),
        Latitude: $('[id$=txtlatitude]').val(),
        Longitude: $('[id$=txtlongitude]').val(),
        Hotellatitude: $('[id$=txthotellatitude]').val(),
        Hotellongitude: $('[id$=txthotellongitude]').val(),
        Imagepath: $('[id$=lblshow]').val(),
        Price: $('[id$=txtprice]').val(),
        URL: $('[id$=txthotelUrl]').val(),
        Isactive: $("#chkactive").prop('checked')
    };
    $.ajax({
        url: '../CMSpage/ManualEntry',
        type: 'POST',
        async: false,
        data: {
            d: ManualEntry
        },
        success: function (data) {
            var notify_alert = noty({
                layout: 'topRight',
                text: 'Saved Successfully...!',
                type: 'success',
                timeout: 3000,
                maxVisible: 1,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                }
            });
            setTimeout(function () {
                window.location.reload(1);
            }, 1000);
        },
        error: function (result) {
            var notify_alert = noty({
                layout: 'topRight',
                text: 'Failed to saved..',
                type: 'warning',
                timeout: 3000,
                maxVisible: 1,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                }
            });
        }
    });
    $('input#txtUserrating').val(''); $('input#txtrating').val(''); $('input#txtHotelName').val(''); $('#txtaddress').val();
    $('input#txtcity').val(''); $('input#txtcountry').val(''); $('#txtdescription').val(); $('input#txtlatitude').val('');
    $('input#txtprice').val(''); $("#ddlcity").val(0); $("#txtlongitude").val(''); $("#spnarea").text('');
    $("#ddlcountry").val(0); $('input#txtlongitude').val(''); $('[id$=lblshow]').val('').text('');
    $('#chkactive').prop('checked', false); $('#txtAmenties').val(''); $('input#txthotelUrl').val('');
    $('input#txthotellatitude').val(''); $('input#txthotellongitude').val('');
    var input = $("#file");
    input.replaceWith(input.val('').clone(true));
}

/*****************Display Hotel Details*****************/

/*****************Edit Hotel*****************/
function EditHotel(RowId) {
    var HotelId = $("#tblhotelheading").jqxGrid('getrowdata', RowId).Hotel_id;
    $.getJSON('../CMSPage/GetHotelId', { UserName: HotelId }, function (data) {
        $.each(data, function (i, d) {  
            var HotelName = d.HotelName;
            var Hotelrating =  d.Hotel_Class;
            var UserRating =  d.Guest_Rating;
            var Amenities =  d.Hotel_Amenties;
            var Address =  d.Address;
            var Description =  d.Description;
            var City =  d.City;
            var Latitude =  d.Latitude;
            var Longitude =  d.Longitude;
            var Hotel_latitude =  d.Hotel_Latitude;
            var Hotel_Longitude =  d.Hotel_Longitude;
            var Image = "../Content/images/HotelImages/" + d.ThumbNail;
            var Imagename = Image.slice(30, 100);
            var Price =  d.Total_price;
            var Url =  d.Booking_Url;
            var Isactive =  d.Isactive;
            $('[id$=htnhotelId]').val(HotelId);
            $('[id$=txtHotelName]').val(HotelName);
            $('[id$=txtrating]').val(Hotelrating);
            $('[id$=txtUserrating]').val(UserRating);
            $('[id$=txtAmenties]').val(Amenities);
            $('[id$=txtaddress]').val(Address);
            $('[id$=txtdescription]').val(Description);
            $('[id$=txtcity]').val(City),
            $('[id$=txtlatitude]').val(Latitude);
            $('[id$=txtlongitude]').val(Longitude);
            $('[id$=txthotellatitude]').val(Hotel_latitude);
            $('[id$=txthotellongitude]').val(Hotel_Longitude);
            $("#imghotel").attr("src", Image);
            $("#lblshow").val(Imagename);
            $('[id$=txthotelUrl]').val(Url);
            $('[id$=txtprice]').val(Price);
            if (Isactive == "true") {
                $('#chkactive').prop('checked', Isactive);
            }
            else {
                $('#chkactive').prop('checked', Isactive);
            }
            $('#btnhotelupdate').show();
            $('#btnhotelsave').hide();
            $('#divhotel').show();
            $('#divhotelshow').hide();
            $('#divhotelsave').show();
            $('#btnhotelshow').show();
            $('#btnhotelAdd').hide();        
        });
    });
}
/*****************End Edit*****************/

/*****************Update Hotel*****************/
function UpdateHotel() {
    var formData = new FormData();
    var totalFiles = document.getElementById("file").files.length;
    for (var i = 0; i < totalFiles; i++) {
        var file = document.getElementById("file").files[i];
        formData.append("file", file);
    }
    $.ajax({
        cach: false,
        async: false,
        type: "POST",
        url: '../CMSPage/Upload',
        data: formData,
        dataType: 'json',
        contentType: false,
        processData: false,
        success: function () {
        },
        error: function (error) {
        }
    });

    if ($.trim($("#txtHotelName").val()) == "") {
        $("#spnarea").text("Please Enter Hotel Name");
        return false;
    }

    if ($.trim($("#txtaddress").val()) == "") {
        $("#spnarea").text("Please Enter Address");
        return false;
    }
    if ($.trim($("#txtcity").val()) == " ") {
        $("#spnarea").text("Please Enter city");
        return false;
    }
    if ($.trim($("#txtlatitude").val()) == "") {
        $("#spnarea").text("Please Enter Latitude");
        return false;
    }
    if ($.trim($("#txtlongitude").val()) == "") {
        $("#spnarea").text("Please Enter Longitude");
        return false;
    }
    var ManualEntry = {
        Id: $('[id$=htnhotelId]').val(),
        Name: $('[id$=txtHotelName]').val(),
        Hotel_rating: $('[id$=txtrating]').val(),
        User_rating: $('[id$=txtUserrating]').val(),
        Amenities: $('[id$=txtAmenties]').val(),
        Address: $('[id$=txtaddress]').val(),
        Description: $('[id$=txtdescription]').val(),
        City: $('[id$=txtcity]').val(),
        Latitude: $('[id$=txtlatitude]').val(),
        Longitude: $('[id$=txtlongitude]').val(),
        Hotellatitude: $('[id$=txthotellatitude]').val(),
        Hotellongitude: $('[id$=txthotellongitude]').val(),
        Imagepath: $('[id$=lblshow]').val(),
        Price: $('[id$=txtprice]').val(),
        URL: $('[id$=txthotelUrl]').val(),
        Isactive: $("#chkactive").prop('checked')
    };

    $.ajax({

        url: '../CMSpage/Update_HotelDetail',
        type: 'POST',
    async: false,
    data: {
        data: ManualEntry
    },
    success: function (data) {
        var notify_alert = noty({
            layout: 'topRight',
            text: 'Record Updated Successfully...!',
            type: 'success',
            timeout: 3000,
            maxVisible: 1,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            }
        });
       
        setTimeout(function () {
            window.location.reload(1);
        }, 1000);
    },
    error: function (result) {
        var notify_alert = noty({
            layout: 'topRight',
            text: 'Failed to update..!',
            type: 'warning',
            timeout: 3000,
            maxVisible: 1,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            }
        });     
    }
});
$('input#txtUserrating').val(''); $('input#txtrating').val(''); $('input#txtHotelName').val(''); $('#txtaddress').val('');
$('input#txtcity').val(''); $('input#txtcountry').val(''); $('#txtdescription').val(''); $('input#txtlatitude').val('');
$('input#txtprice').val(''); $("#ddlcity").val(0); $("#txtlongitude").val(''); $("#spnarea").text('');
$("#ddlcountry").val(0); $('input#txtlongitude').val('');
$('#chkactive').prop('checked', false); $('#txtAmenties').val(''); $('input#txthotelUrl').val('');
$('input#txthotellatitude').val(''); $('input#txthotellongitude').val('');
var input = $("#file"); $("#imghotel").attr("src", "");
input.replaceWith(input.val('').clone(true));
}

/*****************End Update*****************/

/*****************Delete Hotel Details*****************/

function DeleteHotelDetails(RowId) {
    noty({
        layout: 'topRight',
        type: 'confirm',
        maxVisible: 1,
        modal: true,
        text: 'Are you sure you want to delete the selected hotel?',
        buttons: [
            {
                addClass: 'btn btn-primary', text: 'Ok', onClick: function ($noty) {
                    $noty.close();
                    var Hotel_id = $("#tblhotelheading").jqxGrid('getrowdata', RowId).Hotel_id;
                    $.ajax({
                        url: '../CMSpage/DeleteHotel',
                        type: 'POST',
                        async: false,
                        data: {
                            Id: Hotel_id
                        },
                        success: function (data) {
                            var notify_alert = noty({
                                layout: 'topRight',
                                text: 'Record deleted successfully..!',
                                type: 'success',
                                timeout: 3000,
                                maxVisible: 1,
                                animation: {
                                    open: { height: 'toggle' },
                                    close: { height: 'toggle' },
                                    easing: 'swing',
                                    speed: 500
                                }
                            });
                            
                            $('#List').trigger('change');


                            //setTimeout(function () {
                            //    window.location.reload(1);
                            //}, 1000);
                        },
                        error: function (result) {
                            var notify_alert = noty({
                                layout: 'topRight',
                                text: 'Failed to delete..',
                                type: 'warning',
                                timeout: 3000,
                                maxVisible: 1,
                                animation: {
                                    open: { height: 'toggle' },
                                    close: { height: 'toggle' },
                                    easing: 'swing',
                                    speed: 500
                                }
                            });
                        }
                    });
                }
            },
            {
                addClass: 'btn btn-danger', text: 'Cancel', onClick: function ($noty) {
                    $noty.close();
                    return false;
                }
            }
        ]
    });
}
