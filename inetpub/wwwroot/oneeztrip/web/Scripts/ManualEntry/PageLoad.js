﻿$(document).ready(function () {
    DDLResType();
    DDLAttrType();
    DDLActType();
    $('#divhotel').hide();
    $('#divhotelshow').hide();
    $('#divRestaurant').hide();
    $('#divAttraction').hide();
    $('#divActivities').hide();

    $('#List').change(function () {

        var id = $('#List').val();

        /********On selecting Hotel Start************/
        if (id == "0") {
            $('#divhotel').hide();
            $('#divRestaurant').hide();
            $('#divAttraction').hide();
            $('#divActivities').hide();
            $('#divhotelshow').hide();
        }
        else if (id == "1") {

            $('#btnhotelshow').hide();
            $('#btnhotelAdd').show();
            $('#divhotel').show();
            $('#divhotelshow').show();
            $('#divhotelsave').hide();
            $('#divRestaurant').hide();
            $('#divAttraction').hide();
            $('#divActivities').hide();
            $('#btnplus').show();
            $('#btnminus').hide();
            $('#divcity').hide();
            $('#divcountry').hide();
            $('#btncityminus').hide();
            $('#btncityplus').show();


            $('input#txtUserrating').val(''); $('input#txtrating').val(''); $('input#txtHotelName').val(''); $('input#txtaddress').empty();
            $('input#txtcity').val(''); $('input#txtcountry').val(''); $('#txtdescription').val(); $('input#txtlatitude').val('');
            $('input#txtprice').val(''); $("#ddlcity").val(0); $("#txtlongitude").val(''); $("#spnarea").text('');
            $("#ddlcountry").val(0); $('input#txtlongitude').val(''); $('[id$=lblshow]').val('').text('');
            $('#chkactive').prop('checked', false);
            var input = $("#file");
            input.replaceWith(input.val('').clone(true));
            var xlocaldata;
            $.ajax({
                type: "POST",
                async: false,
                url: "../CMSPage/GetHotel",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                cache: false,
                success: function (data) {
                    if (data.d != "") {
                        xlocaldata = data;
                    }
                }
            });

            var source =
           {
               localdata: xlocaldata,
               datatype: "json",
               datafields:
                   [
                  { name: 'Hotel_id', type: 'string' },
                  { name: 'HotelName', type: 'string' },
                   { name: 'Address', type: 'string' },
                   { name: 'City', type: 'string' },
                   { name: 'ThumbNail', type: 'string' },
                   { name: 'Hotel_Class', type: 'string' },
                   { name: 'Total_price', type: 'string' }
                   ]
           };
            var Editrenderer = function (value) {
                return '<a href="#" style="color:Blue; text-align:center;float: left;margin-left: 25%;margin-top: 12%;" onclick="EditHotel(' + value + ');" id="btndelete' + value + '"><img src="../Content/images/Editicon.png" style="margin-top: 25%;margin-left: -6%;"></a>';
            }
            var imagerenderer = function (ThumbNail, datafields, value) {
                return '<img style="margin-left: 5px;margin-left: 15%;width: 68%;margin-top: 8%;border-radius: 8px;" height="60" width="50" src="../Content/images/HotelImages/' + value + '"/>';
            }
            var DeleteButtonrenderer = function (value) {
                return '<a href="#" style="color:Blue; text-align:center;float: left;margin-left: 25%;margin-top: 12%;" onclick="DeleteHotelDetails(' + value + ');" id="btndelete' + value + '"><img src="../Content/images/Deleteicon.png" style="margin-top: 25%;margin-left: -6%;"></a>';
            }

            var dataAdapter = new $.jqx.dataAdapter(source);
            $('#tblhotelheading').jqxGrid({
                width: '100%',
                source: dataAdapter,
                sortable: true,
                filterable: true,
                altrows: true,
                pageable: true,
                columnsresize: true,
                rowsheight: 75,
                columns: [
                {
                    text: 'Id', datafield: 'Hotel_id', width: '5%'
                },
                {
                    text: 'Name', datafield: 'HotelName', width: '15%'
                },
                 {
                     text: 'Address', datafield: 'Address', width: '25%'
                 },
                 {
                     text: 'City', datafield: 'City', width: '25%'
                 },
                 {
                     text: 'Hotel Class', datafield: 'Hotel_Class', width: '5%'
                 },
                  {
                      text: 'Price', datafield: 'Total_price', width: '5%'
                  },
                 {
                     text: 'Image', datafield: 'ThumbNail', width: '10%', cellsrenderer: imagerenderer
                 },

                 {
                     text: 'Edit', datafield: 'Edit', width: '5%', cellsrenderer: Editrenderer
                 },
                 {
                     text: 'Delete', datafield: 'Delete', width: '5%', cellsrenderer: DeleteButtonrenderer
                 }

                ]
            });
            $('#divhotelshow').show();
        }
            /********On selecting Hotel End************/

            /********On selecting Restaurant Start************/
        else if (id == "2") {

            $('#btnRestshow').hide();
            $('#btnRestAdd').show();
            $('#divResSave').hide();
            $('#divResshow').show();
            $('#divhotel').hide();
            $('#divRestaurant').show();
            $('#divAttraction').hide();
            $('#divActivities').hide();
            $('#btnRestypeplus').show();
            $('#btnRescityplus').show();
            $('#btnRescountryplus').show();
            $('#btnRestypeminus').hide();
            $('#btnRescityminus').hide();
            $('#btnRescountyminus').hide();
            $('#divrestype').hide();
            $('#divrescity').hide();
            $('#divrescountry').hide();

            $('input#txtRestrating').val(''); $('input#txtRestaurant').val('');
            $('input#txtAttrRating').val(''); $('input#txtResreview').val(''); $('input#txtResType').val(''); $('input#txtRestime').val('');
            $('input#txtResAddress').empty(); $('input#txtRescountry').val(''); $('input#txtResDescription').empty(); $('input#txtReslatitude').val('');
            $('input#txtResprice').val(''); $('input#spnrestaurant').val('');
            $('#chkRestauractive').prop('checked', false); $("#ddlRestype").val(0); $("#txtRescity").val(""); $("#ddlRescountry").val(0);
            var input = $("#fileToUpload"); $('[id$=lblRest]').val("").text(""); $('input#txtReslongitude').val('');
            input.replaceWith(input.val('').clone(true));
            var xlocaldata;
            $.ajax({
                type: "POST",
                async: false,
                url: "../CMSPage/GetRestuarant",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                cache: false,
                success: function (data) {
                    if (data.d != "") {
                        xlocaldata = data;
                    }
                }
            });

            var source =
           {
               localdata: xlocaldata,
               datatype: "json",
               datafields:
                   [
                  { name: 'RestaurantId', type: 'string' },
                  { name: 'Name', type: 'string' },
                   { name: 'RestaurantType', type: 'string' },
                   { name: 'Address', type: 'string' },
                   { name: 'City', type: 'string' },
                   { name: 'Price', type: 'string' },
                   { name: 'Image_Name', type: 'string' }
                   ]
           };
            var Editrenderer = function (value) {
                return '<a href="#" style="color:Blue; text-align:center;float: left;margin-left: 25%;margin-top: 12%;" onclick="EditRestuarant(' + value + ');" id="btndelete' + value + '"><img src="../Content/images/Editicon.png" style="margin-top: 25%;margin-left: -6%;"></a>';
            }
            var imagerenderer = function (Image_Name, datafields, value) {
                return '<img style="margin-left: 5px;margin-left: 15%;width: 68%;margin-top: 8%;border-radius: 8px;" height="60" width="50" src="../Content/images/RestaurantImage/' + value + '"/>';
            }


            var DeleteButtonrenderer = function (value) {
                return '<a href="#" style="color:Blue; text-align:center;float: left;margin-left: 25%;margin-top: 12%;" onclick="DeleteRestDetails(' + value + ');" id="btndelete' + value + '"><img src="../Content/images/Deleteicon.png" style="margin-top: 25%;margin-left: -6%;"></a>';
            }

            var dataAdapter = new $.jqx.dataAdapter(source);
            $('#tblRestheading').jqxGrid({
                width: '100%',
                source: dataAdapter,
                sortable: true,
                filterable: true,
                altrows: true,
                pageable: true,
                columnsresize: true,
                rowsheight: 75,
                columns: [
                {
                    text: 'Id', datafield: 'RestaurantId', width: '5%'
                },
                {
                    text: 'Name', datafield: 'Name', width: '15%'
                },
                 {
                     text: 'Type', datafield: 'RestaurantType', width: '15%'
                 },
                 {
                     text: 'Address', datafield: 'Address', width: '15%'
                 },
                 {
                     text: 'City', datafield: 'City', width: '20%'
                 },
                 {
                     text: 'Image', datafield: 'Image_Name', width: '10%', cellsrenderer: imagerenderer
                 },
                 {
                     text: 'Price', datafield: 'Price', width: '10%'
                 },
                 {
                     text: 'Edit', datafield: 'Edit', width: '5%', cellsrenderer: Editrenderer
                 },
                 {
                     text: 'Delete', datafield: 'Delete', width: '5%', cellsrenderer: DeleteButtonrenderer
                 }

                ]
            });
            $('#divResshow').show();
        }

            /********On selecting Restaurant Start************/

            /********On selecting Attraction Start************/

        else if (id == "3") {
            $('#divAttraction').show();
            $('#divAttrashow').show();
            $('#divAttraSave').hide();
            $('#btnAttrashow').hide();
            $('#btnAttraAdd').show();
            $('#divhotel').hide();
            $('#divRestaurant').hide();
            $('#divAttraction').show();
            $('#divActivities').hide();
            $('#btnAttrtypeplus').show();
            $('#btnAttrcityplus').show();
            $('#btnAttrcountryplus').show();
            $('#btnAttrtypeminus').hide();
            $('#btnAttrcityminus').hide();
            $('#btnAttrcountryminus').hide();
            $('#divAttrtype').hide();
            $('#divAttrcity').hide();
            $('#divAttrcountry').hide();

            $('input#txtAttraction').val('');
            $('input#txtAttrRating').val(''); $('input#txtAttrRating').val(''); $('input#txtAttrReview').val(''); $('input#txtAttrAddress').empty();
            $('input#txtAttrcountry').val(''); $('input#txtAttrDescription').empty(); $('input#txtAttrlatitude').val(''); $('input#txtAttrprice').val('');
            $('input#txtActurl').val(''); $('input#txtAttrType').val(''); $("#ddlAttrtype").val(0); $("#txtAttrcity").val("");
            $('#chkAttractive').prop('checked', false); $('[id$=lblAttract]').val("").text(""); $('input#txtAttrlongitude').val('');
            var input = $("#fileAttrupload");
            input.replaceWith(input.val('').clone(true));
            var xlocaldata;
            $.ajax({
                type: "POST",
                async: false,
                url: "../CMSPage/GetAttraction",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                cache: false,
                success: function (data) {
                    if (data.d != "") {
                        xlocaldata = data;
                    }
                }
            });

            var source =
           {
               localdata: xlocaldata,
               datatype: "json",
               datafields:
                   [
                  { name: 'AttractionId', type: 'string' },
                  { name: 'Name', type: 'string' },
                   { name: 'AttractionType', type: 'string' },
                   { name: 'Address', type: 'string' },
                   { name: 'City', type: 'string' },
                   { name: 'Price', type: 'string' },
                   { name: 'Image_Name', type: 'string' }
                   ]
           };
            var Editrenderer = function (value) {
                return '<a href="#" style="color:Blue; text-align:center;float: left;margin-left: 25%;margin-top: 12%;" onclick="EditAttraction(' + value + ');" id="btndelete' + value + '"><img src="../Content/images/Editicon.png" style="margin-top: 25%;margin-left: -6%;"></a>';
            }
            var imagerenderer = function (Image_Name, datafields, value) {
                return '<img style="margin-left: 5px;margin-left: 15%;width: 68%;margin-top: 8%;border-radius: 8px;" height="60" width="50" src="../Content/images/AttractionImage/' + value + '"/>';
            }


            var DeleteButtonrenderer = function (value) {
                return '<a href="#" style="color:Blue; text-align:center;float: left;margin-left: 25%;margin-top: 12%;" onclick="DeleteAttractionDetails(' + value + ');" id="btndelete' + value + '"><img src="../Content/images/Deleteicon.png" style="margin-top: 25%;margin-left: -6%;"></a>';
            }

            var dataAdapter = new $.jqx.dataAdapter(source);
            $('#tblAttraheading').jqxGrid({
                width: '100%',
                source: dataAdapter,
                sortable: true,
                filterable: true,
                altrows: true,
                pageable: true,
                columnsresize: true,
                rowsheight: 75,
                columns: [
                {
                    text: 'Id', datafield: 'AttractionId', width: '5%'
                },
                {
                    text: 'Name', datafield: 'Name', width: '15%'
                },
                 {
                     text: 'Type', datafield: 'AttractionType', width: '15%'
                 },
                 {
                     text: 'Address', datafield: 'Address', width: '15%'
                 },
                 {
                     text: 'City', datafield: 'City', width: '20%'
                 },
                 {
                     text: 'Image', datafield: 'Image_Name', width: '10%', cellsrenderer: imagerenderer
                 },
                 {
                     text: 'Price', datafield: 'Price', width: '10%'
                 },
                 {
                     text: 'Edit', datafield: 'Edit', width: '5%', cellsrenderer: Editrenderer
                 },
                 {
                     text: 'Delete', datafield: 'Delete', width: '5%', cellsrenderer: DeleteButtonrenderer
                 }

                ]
            });
            $('#divResshow').show();
        }
            /********On selecting Attraction End************/

            /********On selecting Activity Start************/
        else if (id == "4") {

            $('#divActivities').show();
            $('#divActshow').show();
            $('#divActSave').hide();
            $('#btnActshow').hide();
            $('#btnActAdd').show();
            $('#divhotel').hide();
            $('#divRestaurant').hide();
            $('#divAttraction').hide();
            $('#divActivities').show();
            $('#btnActtypeplus').show();
            $('#btnActcityplus').show();
            $('#btnActcountryplus').show();
            $('#btnActtypeminus').hide();
            $('#btnActcityminus').hide();
            $('#btnActcountryminus').hide();
            $('#divActtype').hide();
            $('#divActcity').hide();
            $('#divActcountry').hide();

            $('input#txtActivities').val('');
            $('input#txtActRating').val(''); $('input#txtActreview').val(''); $('input#txtActType').val(''); $('input#txtActAddress').empty();
            $('input#txtActDescription').empty(); $('input#txtActlatitude').val(''); $('input#txtActduration').val(''); $('input#txtActprice').val('');
            $('input#txtActurl').val(''); $('input#txtActtime').val(''); $('#chkActactive').prop('checked', false); $('input#spnActivity').val('');
            var input = $("#fileActupload"); $("#ddlActtype").val(0); $("#ddlActcity").val(0); $("#ddlActCountry").val(0);
            input.replaceWith(input.val('').clone(true)); $('[id$=lblAct]').val("").text("");
            var xlocaldata;
            $.ajax({
                type: "POST",
                async: false,
                url: "../CMSPage/GetActivity",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                cache: false,
                success: function (data) {
                    if (data.d != "") {
                        xlocaldata = data;
                    }
                }
            });

            var source =
           {
               localdata: xlocaldata,
               datatype: "json",
               datafields:
                   [
                  { name: 'ActivityID', type: 'string' },
                  { name: 'Name', type: 'string' },
                   { name: 'ActivityType', type: 'string' },
                   { name: 'Address', type: 'string' },
                   { name: 'City', type: 'string' },
                   { name: 'Price', type: 'string' },
                   { name: 'Image', type: 'string' }
                   ]
           };
            var Editrenderer = function (value) {
                return '<a href="#" style="color:Blue; text-align:center;float: left;margin-left: 25%;margin-top: 12%;" onclick="EditActivity(' + value + ');" id="btndelete' + value + '"><img src="../Content/images/Editicon.png" style="margin-top: 25%;margin-left: -6%;"></a>';
            }
            var imagerenderer = function (Image_Name, datafields, value) {
                return '<img style="margin-left: 5px;margin-left: 15%;width: 68%;margin-top: 8%;border-radius: 8px;" height="60" width="50" src="../Content/images/ActivitiesImage/' + value + '"/>';
            }
            var DeleteButtonrenderer = function (value) {
                return '<a href="#" style="color:Blue; text-align:center;float: left;margin-left: 25%;margin-top: 12%;" onclick="DeleteActivityDetails(' + value + ');" id="btndelete' + value + '"><img src="../Content/images/Deleteicon.png" style="margin-top: 25%;margin-left: -6%;"></a>';
            }
            var dataAdapter = new $.jqx.dataAdapter(source);
            $('#tblActheading').jqxGrid({
                width: '100%',
                source: dataAdapter,
                sortable: true,
                filterable: true,
                altrows: true,
                pageable: true,
                columnsresize: true,
                rowsheight: 75,
                columns: [
                {
                    text: 'Id', datafield: 'ActivityID', width: '5%'
                },
                {
                    text: 'Name', datafield: 'Name', width: '15%'
                },
                 {
                     text: 'Type', datafield: 'ActivityType', width: '15%'
                 },
                 {
                     text: 'Address', datafield: 'Address', width: '15%'
                 },
                 {
                     text: 'City', datafield: 'City', width: '20%'
                 },
                 {
                     text: 'Image', datafield: 'Image', width: '10%', cellsrenderer: imagerenderer
                 },
                 {
                     text: 'Price', datafield: 'Price', width: '10%'
                 },
                 {
                     text: 'Edit', datafield: 'Edit', width: '5%', cellsrenderer: Editrenderer
                 },
                 {
                     text: 'Delete', datafield: 'Delete', width: '5%', cellsrenderer: DeleteButtonrenderer
                 }

                ]
            });

        }
        /********On selecting Activity End************/
    });

    $('.Textbox').keypress(function (e) {
        if (e.which === 32 && !this.value.length && (e.which < 48 || e.which > 57))
            e.preventDefault();
    });

    //$('.Textboxnumberonly').keypress(function (e) {
    //    if (e.which === 32 && !this.value.length && e.which < 65 || e.which > 90)
    //        e.preventDefault();
    //});

    $("#txtActprice,#txtprice").on("keypress keyup blur", function (event) {
        $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
        if (event.which > 31 && (event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });

    $("#txtAttrprice,#txtResprice,#txtActduration").on("keypress keyup blur", function (event) {
        $(this).val($(this).val().replace(/[^\d].+/, ""));
        if (event.which > 31 && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });



});

function readURL(inputfile) {
    if (inputfile.files && inputfile.files[0]) {
        var filname = inputfile.files[0].name.split('.');
        var arlen = filname.length - 1;
        var allow = new Array("gif", "png", "jpg", "jpeg");
        if ($.inArray(filname[arlen], allow) == -1) {
            var noty_alert = noty({
                layout: 'topRight',
                text: 'Only image files are accepted..',
                type: 'warning',
                timeout: 3000,
                maxVisible: 1,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                }
            });
            return false;
        }
        var reader = new FileReader();
        reader.onload = function (e) {
            if ($('#List').val() == "1") {
                $('#imghotel').attr('src', e.target.result);
            }
            else if ($('#List').val() == "2") {
                $('#imgRestaurant').attr('src', e.target.result);
            }
            else if ($('#List').val() == "3") {
                $('#imgAttraction').attr('src', e.target.result);
            }
            else if ($('#List').val() == "4") {
                $('#imgActivity').attr('src', e.target.result);
            }
        }
        reader.readAsDataURL(inputfile.files[0]);
    }
}

$("#file").change(function () {
    readURL(this);
});

$("#fileActupload").change(function () {
    readURL(this);
});

$("#fileToUpload").change(function () {
    readURL(this);
});

$("#fileAttrupload").change(function () {
    readURL(this);
});