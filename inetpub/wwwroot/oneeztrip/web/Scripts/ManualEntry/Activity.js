﻿function ActPlus() {
    $('#btnActtypeplus').hide();
    $('#btnActtypeminus').show();
    $('#divActtype').show();
    $("#btnActtypeDelete").hide();
}

function ActMinus() {
    $('#btnActtypeplus').show();
    $('#btnActtypeminus').hide();
    $('#divActtype').hide();
    $("#btnActtypeDelete").show();
}

function ActiveType() {
    $("#ddlActtype").empty();
    DDLActType();
}

function btnsaveActivity() {
    $('#divActivities').show();
    $('#divActshow').hide();
    $('#divActSave').show();
    $('#btnActshow').show();
    $('#btnActAdd').hide();
    $('#btnActUpdate').hide();
    $('#btnActivitysave').show();
    $('#imgActivity').attr('src', '');
    $('input#txtActivities').val('');
    $('input#txtActRating').val(''); $('input#txtActreview').val(''); $('input#txtActType').val(''); $('#txtActAddress').val('');
    $('#txtActDescription').val(''); $('input#txtActlatitude').val(''); $('input#txtActduration').val(''); $('input#txtActprice').val('');
    $('input#txtActurl').val(''); $('input#txtActtime').val(''); $('#chkActactive').prop('checked', false); $('input#spnActivity').val('');
    var input = $("#fileActupload"); $("#ddlActtype").val(0); $("#txtActcity").val(""); $('input#txtActlongitude').val('');
    input.replaceWith(input.val('').clone(true)); $('input#txtPickUp').val('');
    $('input#txtlatitudeActivity').val(''); $('input#txtlongitudeActivity').val(''); $('input#txtActIncludes').val(''); $('input#txtActExcludes').val('');
    $('input#txtSupplierUrl').val(''); $('input#txtSupplierPhone').val(''); $('input#txtSupplierCompany').val(''); $('input#txtSuppliername').val('');
    $('input#lblAct').val(''); $('#imgActivity').attr('src', '');
}

function btnshowActivity() {
    $('#divActivities').show();
    $('#divActshow').show();
    $('#divActSave').hide();
    $('#btnActshow').hide();
    $('#btnActAdd').show();
    $('input#txtActivities').val('');
    $('input#txtActRating').val(''); $('input#txtActreview').val(''); $('input#txtActType').val(''); $('input#txtActAddress').empty();
    $('input#txtActDescription').empty(); $('input#txtActlatitude').val(''); $('input#txtActduration').val(''); $('input#txtActprice').val('');
    $('input#txtActurl').val(''); $('input#txtActtime').val(''); $('#chkActactive').prop('checked', false); $('input#spnActivity').val('');
    var input = $("#fileActupload"); $("#ddlActtype").val(0); $("#ddlActcity").val(0); $("#ddlActCountry").val(0);
    input.replaceWith(input.val('').clone(true)); $('[id$=lblAct]').val("").text("");
}
/***************Activities Details Save***************/
function SaveActivities() {
    var formData = new FormData();
    var totalFiles = document.getElementById("fileActupload").files.length;
    for (var i = 0; i < totalFiles; i++) {
        var file = document.getElementById("fileActupload").files[i];
        var Extension = file.type.split('/').pop();
        formData.append("fileActupload", file);
        if (Extension == "bmp" || Extension == "gif" || Extension == "png" || Extension == "jpg" || Extension == "jpeg") {
            $.ajax({
                cach: false,
                async: false,
                type: "POST",
                url: '../CMSPage/ActivityUpload',
                data: formData,
                dataType: 'json',
                contentType: false,
                processData: false,
                success: function (response) {
                },
                error: function (error) {
                }
            });
        }
        else {
            var notify_alert = noty({
                layout: 'topRight',
                text: 'Invalid File!..',
                type: 'warning',
                timeout: 3000,
                maxVisible: 1,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                }
            });
            return false;
        }
    }

    if ($.trim($("#txtActivities").val()) == "") {
        $("#spnActivity").text("Please enter activity name");
        return false;
    }
    if ($.trim($("#txtActRating").val()) == "") {
        $("#spnActivity").text("Please enter activity rating");
        return false;
    }
    if ($.trim($("#ddlActtype").val()) == "0") {
        $("#spnActivity").text("Please select activity type");
        return false;
    }
    if ($.trim($("#txtActcity").val()) == "") {
        $("#spnActivity").text("Please fill city name");
        return false;
    }
    if ($.trim($("#txtActlatitude").val()) == "") {
        $("#spnActivity").text("Please enter latitude");
        return false;
    }
    if ($.trim($("#txtActlongitude").val()) == "") {
        $("#spnActivity").text("Please enter longitude");
        return false;
    }
    if ($.trim($("#txtlatitudeActivity").val()) == "") {
        $("#spnActivity").text("Please enter activity latitude");
        return false;
    }
    if ($.trim($("#txtlongitudeActivity").val()) == "") {
        $("#spnActivity").text("Please enter activity longitude");
        return false;
    }
    if ($.trim($("#txtActduration").val()) == "") {
        $("#spnActivity").text("Please enter duration");
        return false;
    }    
    if (document.getElementById("fileActupload").files.length == 0) {
        $("#spnActivity").text("Please add activity image");
        return false;
    }
    if ($.trim($("#txtActprice").val()) == "") {
        $("#spnActivity").text("Please enter price");
        return false;
    }
    if ($.trim($("#txtActurl").val()) == "") {
        $("#spnActivity").text("Please enter reference url");
        return false;
    }

    function ValidateEmail(email) {
        var expr = /(http(s)?:\\)?([\w-]+\.)+[\w-]+[.com|.in|.org]+(\[\?%&=]*)?/;
        return expr.test(email);
    };

    if (!ValidateEmail($("#txtActurl").val())) {
        $("#spnActivity").text("Incorrect reference url");
        return false;
    }

    var ManualEntry = {
        Name: $('[id$=txtActivities]').val(),
        Hotel_rating: $('[id$=txtActRating]').val(),
        Type: $("#ddlActtype option:selected").text(),
        description: $('[id$=txtActDescription]').val(),
        Address: $('[id$=txtActAddress]').val(),
        City: $('[id$=txtActcity]').val(),
        Latitude: $('[id$=txtActlatitude]').val(),
        Longitude: $('[id$=txtActlongitude]').val(),
        Hotellatitude: $('[id$=txtlatitudeActivity]').val(),
        Hotellongitude: $('[id$=txtlongitudeActivity]').val(),
        Includes: $('[id$=txtActIncludes]').val(),
        Excludes: $('[id$=txtActExcludes]').val(),
        SupplierName: $('[id$=txtSuppliername]').val(),
        Company: $('[id$=txtSupplierCompany]').val(),
        Phone: $('[id$=txtSupplierPhone]').val(),
        SupplierUrl: $('[id$=txtSupplierUrl]').val(),
        pichup: $('[id$=txtPickUp').val(),
        Duration: $("#txtActduration").val(),
        Imagepath: $("#fileActupload").val(),
        Thumbnail: $("#fileActupload").val(),
        Price: $("#txtActprice").val(),
        URL: $("#txtActurl").val(),
        Isactive: $("#chkActactive").prop('checked')
    };

    $.ajax({
        url: '../CMSpage/SaveActivities',
        type: 'POST',
    async: false,
    data: {
        data: ManualEntry
    },
    success: function (data) {
        var notify_alert = noty({
            layout: 'topRight',
            text: 'Saved Successfully..!',
            type: 'success',
            timeout: 3000,
            maxVisible: 1,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            }
        });
        setTimeout(function () {
            window.location.reload(1);
        }, 1000);
    },
    error: function (result) {
        var notify_alert = noty({
            layout: 'topRight',
            text: 'Failed to save.',
            type: 'warning',
            timeout: 3000,
            maxVisible: 1,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            }
        });        
    }
});
$('input#txtActivities').val('');
$('input#txtActRating').val(''); $('input#txtActreview').val(''); $('input#txtActType').val(''); $('#txtActAddress').val('');
$('#txtActDescription').val(''); $('input#txtActlatitude').val(''); $('input#txtActduration').val(''); $('input#txtActprice').val('');
$('input#txtActurl').val(''); $('input#txtActtime').val(''); $('#chkActactive').prop('checked', false); $('input#spnActivity').val('');
var input = $("#fileActupload"); $("#ddlActtype").val(0); $("#txtActcity").val(""); $('input#txtActlongitude').val('');
input.replaceWith(input.val('').clone(true)); $('input#txtPickUp').val('');
$('input#txtlatitudeActivity').val(''); $('input#txtlongitudeActivity').val(''); $('input#txtActIncludes').val(''); $('input#txtActExcludes').val('');
$('input#txtSupplierUrl').val(''); $('input#txtSupplierPhone').val(''); $('input#txtSupplierCompany').val(''); $('input#txtSuppliername').val('');
$('input#lblAct').val('');
}

/***********Edit Activity******************/
function EditActivity(RowId) {
    var ActId = $("#tblActheading").jqxGrid('getrowdata', RowId).ActivityID;
    $.getJSON('../CMSPage/GetActivityId', { UserName: ActId }, function (data) {
        $.each(data, function (i, d) {
            var ActName=d.Name
            var ActRating = d.Rating;
            var ActType = d.ActivityType;
            var Description = d.Description;
            var Address = d.Address;
            var City = d.City;
            var Latitude = d.Latitude;
            var Longitude = d.Longitude;
            var Activity_Latitude = d.Activity_Latitude;
            var Activity_Longitude = d.Activity_Longitude;
            var Includes = d.Includes;
            var Excludes = d.Excludes;
            var Duration = d.Duration;
            var Supplier_name = d.Supplier_Name;
            var Company = d.Supplier_CompanyName;
            var Phone = d.Supplier_Phone;
            var Url = d.Supplier_Url;
            var Pickup = d.Pickup_Dropoff;
            var Image = "../Content/images/ActivitiesImage/"+d.Image;
            var Imagename = Image.slice(34, 120);
            var Price = d.Price;
            var Url = d.RedirectUrl;
            var Isactive = d.Isactive;
            $('[id$=hidAct]').val(ActId);
            $('[id$=txtActivities]').val(ActName);
            $('[id$=txtActRating]').val(ActRating);
            var type = document.getElementById("ddlActtype");
            for (var i = 0; i < type.options.length; i++) {
                if (type.options[i].text == ActType) {
                    type.selectedIndex = i;
                    break;
                }
            }
            $('[id$=txtActAddress]').val(Address);
            $('[id$=txtActDescription]').val(Description);
            $('[id$=txtActcity]').val(City)
            $('[id$=txtActlatitude]').val(Latitude);
            $('[id$=txtActlongitude]').val(Longitude);
            $('[id$=txtlatitudeActivity]').val(Activity_Latitude);
            $('[id$=txtlongitudeActivity]').val(Activity_Longitude);
            $('[id$=txtActIncludes]').val(Includes);
            $('[id$=txtActExcludes]').val(Excludes);
            $('[id$=txtActduration]').val(Duration);
            $('[id$=txtSuppliername]').val(Supplier_name);
            $('[id$=txtSupplierCompany]').val(Company);
            $('[id$=txtSupplierPhone]').val(Phone);
            $('[id$=txtSupplierUrl]').val(Url);
            $('[id$=txtPickUp]').val(Pickup);
            $("#imgActivity").attr("src", Image);
            $('[id$=lblAct]').val(Imagename).text(Imagename);
            $('[id$=txtActprice]').val(Price);
            $('[id$=txtActurl]').val(Url);
            if (Isactive == "true") {

                $('#chkActactive').prop('checked', Isactive);
            }
            else {
                $('#chkActactive').prop('checked', Isactive);
            }
            $('#btnActUpdate').show();
            $('#btnActivitysave').hide();
            $('#divActivities').show();
            $('#divActshow').hide();
            $('#divActSave').show();
            $('#btnActshow').show();
            $('#btnActAdd').hide();
        });
    });
}
/**********End Edit Activity**************/

/***********Update Activities****************/
function UpdateActivityDetails() {
    var formData = new FormData();
    var totalFiles = document.getElementById("fileActupload").files.length;
    for (var i = 0; i < totalFiles; i++) {
        var file = document.getElementById("fileActupload").files[i];

        formData.append("fileActupload", file);
    }
    $.ajax({
        cach: false,
        async: false,
        type: "POST",
        url: '@Url.Action("ActivityUpload", "CMSPage")',
        data: formData,
        dataType: 'json',
        contentType: false,
        processData: false,
        success: function (response) {


        },
        error: function (error) {

        }
    });

    if ($.trim($("#txtActivities").val()) == "") {
        $("#spnActivity").text("Please enter activity name");
        return false;
    }
    if ($.trim($("#txtActRating").val()) == "") {
        $("#spnActivity").text("Please enter activity rating");
        return false;
    }
    if ($.trim($("#ddlActtype").val()) == "0") {
        $("#spnActivity").text("Please select activity type");
        return false;
    }
    if ($.trim($("#txtActcity").val()) == "") {
        $("#spnActivity").text("Please fill city name");
        return false;
    }
    if ($.trim($("#txtActlatitude").val()) == "") {
        $("#spnActivity").text("Please enter latitude");
        return false;
    }
    if ($.trim($("#txtActlongitude").val()) == "") {
        $("#spnActivity").text("Please enter longitude");
        return false;
    }
    if ($.trim($("#txtlatitudeActivity").val()) == "") {
        $("#spnActivity").text("Please enter activity latitude");
        return false;
    }
    if ($.trim($("#txtlongitudeActivity").val()) == "") {
        $("#spnActivity").text("Please enter activity longitude");
        return false;
    }
    if ($.trim($("#txtActduration").val()) == "") {
        $("#spnActivity").text("Please enter duration");
        return false;
    }
    //if (document.getElementById("fileActupload").files.length == 0) {
    //    $("#spnActivity").text("Please add activity image");
    //    return false;
    //}
    if ($.trim($("#txtActprice").val()) == "") {
        $("#spnActivity").text("Please enter price");
        return false;
    }
    if ($.trim($("#txtActurl").val()) == "") {
        $("#spnActivity").text("Please enter reference url");
        return false;
    }

    function ValidateEmail(email) {
        var expr = /(http(s)?:\\)?([\w-]+\.)+[\w-]+[.com|.in|.org]+(\[\?%&=]*)?/;
        return expr.test(email);
    };

    if (!ValidateEmail($("#txtActurl").val())) {
        $("#spnActivity").text("Incorrect reference url");
        return false;
    }
    
    var ManualEntry = {
        Id: $('[id$=hidAct]').val(),
        Name: $('[id$=txtActivities]').val(),
        Hotel_rating: $('[id$=txtActRating]').val(),
        Type: $("#ddlActtype option:selected").text(),
        description: $('[id$=txtActDescription]').val(),
        Address: $('[id$=txtActAddress]').val(),
        City: $('[id$=txtActcity]').val(),
        Latitude: $('[id$=txtActlatitude]').val(),
        Longitude: $('[id$=txtActlongitude]').val(),
        Hotellatitude: $('[id$=txtlatitudeActivity]').val(),
        Hotellongitude: $('[id$=txtlongitudeActivity]').val(),
        Includes: $('[id$=txtActIncludes]').val(),
        Excludes: $('[id$=txtActExcludes]').val(),
        SupplierName: $('[id$=txtSuppliername]').val(),
        Company: $('[id$=txtSupplierCompany]').val(),
        Phone: $('[id$=txtSupplierPhone]').val(),
        SupplierUrl: $('[id$=txtSupplierUrl]').val(),
        pichup: $('[id$=txtPickUp').val(),
        Duration: $("#txtActduration").val(),
        Imagepath: $("#lblAct").val(),
        Price: $("#txtActprice").val(),
        URL: $("#txtActurl").val(),
        Isactive: $("#chkActactive").prop('checked')
    };

    $.ajax({
        url: '../CMSpage/UpadteActvity',
        type: 'POST',
    async: false,
    data: {
        data: ManualEntry
    },
    success: function (data) {
        var notify_alert = noty({
            layout: 'topRight',
            text: 'Record Updated Successfully...!',
            type: 'success',
            timeout: 3000,
            maxVisible: 1,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            }
        });
        setTimeout(function () {
            window.location.reload(1);
        }, 1000);
    },
    error: function (result) {
        var notify_alert = noty({
            layout: 'topRight',
            text: 'Failed to update the record..',
            type: 'warning',
            timeout: 3000,
            maxVisible: 1,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            }
        });
       
    }
});

$('input#txtActivities').val('');
$('input#txtActRating').val(''); $('input#txtActreview').val(''); $('input#txtActType').val(''); $('#txtActAddress').val('');
$('#txtActDescription').val(''); $('input#txtActlatitude').val(''); $('input#txtActduration').val(''); $('input#txtActprice').val('');
$('input#txtActurl').val(''); $('input#txtActtime').val(''); $('#chkActactive').prop('checked', false); $('input#spnActivity').val('');
var input = $("#fileActupload"); $("#ddlActtype").val(0); $("#txtActcity").val(""); $('input#txtActlongitude').val('');
input.replaceWith(input.val('').clone(true)); $('input#txtPickUp').val('');
$('input#txtlatitudeActivity').val(''); $('input#txtlongitudeActivity').val(''); $('input#txtActIncludes').val(''); $('input#txtActExcludes').val('');
$('input#txtSupplierUrl').val(''); $('input#txtSupplierPhone').val(''); $('input#txtSupplierCompany').val(''); $('input#txtSuppliername').val('');
$("#imgActivity").attr("src", "");
}
/*************End Activity Update******************/

/**********Delete Activity Details****************/
function DeleteActivityDetails(RowId) {
    noty({
        layout: 'topRight',
        type: 'confirm',
        maxVisible: 1,
        modal: true,
        text: 'Are you sure you want to delete the selected activity?',
        buttons: [
            {
                addClass: 'btn btn-primary', text: 'Ok', onClick: function ($noty) {
                    $noty.close();
                    var Act_Id = $("#tblActheading").jqxGrid('getrowdata', RowId).ActivityID;
                    $.ajax({
                        url: '../CMSpage/DeleteActivity',
                        type: 'POST',
                        async: false,
                        data: {
                            Id: Act_Id
                        },
                        success: function (data) {
                            var notify_alert = noty({
                                layout: 'topRight',
                                text: 'Record deleted successfully......!',
                                type: 'success',
                                timeout: 3000,
                                maxVisible: 1,
                                animation: {
                                    open: { height: 'toggle' },
                                    close: { height: 'toggle' },
                                    easing: 'swing',
                                    speed: 500
                                }
                            });

                            $('#List').trigger('change');

                            //setTimeout(function () {
                            //    window.location.reload(1);
                            //}, 1000);

                        },
                        error: function (result) {
                            var notify_alert = noty({
                                layout: 'topRight',
                                text: 'Failed to delete the record.....',
                                type: 'error',
                                timeout: 3000,
                                maxVisible: 1,
                                animation: {
                                    open: { height: 'toggle' },
                                    close: { height: 'toggle' },
                                    easing: 'swing',
                                    speed: 500
                                }
                            });
                        }
                    });
                }
            },
            {
                addClass: 'btn btn-danger', text: 'Cancel', onClick: function ($noty) {
                    $noty.close();
                    return false;
                }
            }
        ]
    });
}
/********End Delete ***********/

/**************Save Activities Type Dropdown************/
function ActivityType() {
    if ($.trim($("#txtActtype").val()) == "") {
        $("#spnActivity").text("Please Enter Activity type");
        return false;
    };
    var ManualEntry = {
        Type: $("#txtActtype").val(),
        Isactive: true
    };
    $.ajax({
        url: '../CMSpage/SaveActType',
        type: 'POST',
    async: false,
    data: {
        data: ManualEntry
    },
    success: function (data) {
        var notify_alert = noty({
            layout: 'topRight',
            text: 'Saved Successfully......!',
            type: 'success',
            timeout: 3000,
            maxVisible: 1,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            }
        });
        setTimeout(function () {
            window.location.reload(1);
        }, 1000);
        ActiveType();
    },
    error: function (result) {
        var notify_alert = noty({
            layout: 'topRight',
            text: 'Failed to save the record.....',
            type: 'error',
            timeout: 3000,
            maxVisible: 1,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            }
        });      
    }
});
$("#spnActivity").text('');
$("#txtActtype").val('');
}

function DDLActType() {
    $.getJSON('../CMSPage/ActTypeDropdown', {}, function (data) {
        var item = '';
        $("#ddlActtype").append($("<option></option>").val("0").html("---Activity Type---"));
        $.each(data, function (i, item) {
            $("#ddlActtype").append($("<option></option>").val(item.ActvityTypeID).html(item.Actity_Type));

        });
    });
}

$(document).ready(function () {
    $("#btnActtypeDelete").hide();

});

function SelectedIndexChange() {
    var restaurentType = $("#ddlActtype");
    var restaurentTypeIndex = $("#ddlActtype option:selected").index();
    var restaurentTypeValue = $("#ddlActtype option:selected").val();
    if (restaurentTypeIndex > 0) {
        $("#btnActtypeDelete").show();
    }
}

function notyDeleteAlert() {
    noty({
        text: 'Are you confirm to delete the activity?',
        buttons: [
            {
                addClass: 'btn btn-primary', text: 'Yes', onClick: function ($noty) {
                    DeleteActivityType();
                    $noty.close();
                }
            },
            {
                addClass: 'btn btn-danger', text: 'No', onClick: function ($noty) {
                    $noty.close();
                }
            }
        ]
    });
}

function DeleteActivityType() {
    var ActivityType = $("#ddlActtype");
    var ActivityTypeIndex = $("#ddlActtype option:selected").index();
    var ActivityTypeValue = $("#ddlActtype option:selected").val();
    $.ajax({
        url: "../CMSPage/DeleteActivityType",
        type: "POST",
        async: false,
        data: JSON.stringify({ ActivityTypeID: ActivityTypeValue }),
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        success: function (index, data) {
            ActiveType();
            $("#btnActtypeDelete").hide();
            var notify_alert = noty({
                layout: 'topRight',
                text: 'Activity type deleted successfully',
                type: 'success',
                timeout: 3000,
                maxVisible: 1,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                }
            });
        },
        error: function (error) {
            var notify_alert = noty({
                layout: 'topRight',
                text: 'Oops! Something went wrong! Please try again..',
                type: 'warning',
                timeout: 3000,
                maxVisible: 1,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                }
            });
        }
    });
}

function notyDeleteActivityactioAlert() {
    var ActivityType = $("#ddlActtype");
    var ActivityTypeIndex = $("#ddlActtype option:selected").index();
    var ActivityTypeValue = $("#ddlActtype option:selected").val();
    if (ActivityTypeValue == 0) {
        var notify_alert = noty({
            layout: 'topRight',
            text: 'please select activity type ',
            type: 'warning',
            timeout: 3000,
            maxVisible: 1,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            }
        });
    }
    else {
        noty({
            layout: 'topRight',
            type: 'confirm',
            text: 'Are you confirm to delete the activity?',
            buttons: [
                {
                    addClass: 'btn btn-primary', text: 'Yes', onClick: function ($noty) {
                        DeleteActivityType();
                        $noty.close();
                    }
                },
                {
                    addClass: 'btn btn-danger', text: 'No', onClick: function ($noty) {
                        $noty.close();
                    }
                }
            ]
        });
    }
}