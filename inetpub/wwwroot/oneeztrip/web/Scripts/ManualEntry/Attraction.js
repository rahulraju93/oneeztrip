﻿function AttrPlus() {
    $('#btnAttrtypeplus').hide();
    $('#btnAttrtypeminus').show();
    $('#divAttrtype').show();
    $("#btnAttrtypeDelete").hide();
}

$(document).ready(function () {
    $("#btnAttrtypeDelete").hide();
});

function AttrMinus() {
    $('#btnAttrtypeplus').show();
    $('#btnAttrtypeminus').hide();
    $('#divAttrtype').hide();
    $("#btnAttrtypeDelete").show();
}

function AttractType() {
    $("#ddlAttrtype").empty();
    DDLAttrType();
}

function btnsaveAttraction() {
    $('#divAttraction').show();
    $('#divAttrashow').hide();
    $('#divAttraSave').show();
    $('#btnAttrashow').show();
    $('#btnAttraAdd').hide();
    $('#btnAttrUpdate').hide();
    $('#btnAttrsave').show();
    $('input#txtAttraction').val('');
    $('input#txtAttrRating').val(''); $('input#txtAttrRating').val(''); $('input#txtAttrReview').val(''); $('#txtAttrAddress').val('');
    $('input#txtAttrcountry').val(''); $('#txtAttrDescription').val(''); $('input#txtAttrlatitude').val(''); $('input#txtAttrlongitude').val(''); $('input#txtAttrprice').val('');
    $('input#txtActurl').val(''); $('input#txtAttrType').val(''); $("#ddlAttrtype").val(0); $("#txtAttrcity").val('');
    $('#chkAttractive').prop('checked', false); $('input#txtlatitudeAttrac').val(''); $('input#txtlongitudeAttrac').val(''); $('input#txtAttrUrl').val('');
    $('#imgAttraction').attr('src', '');
    var input = $("#fileAttrupload");
    input.replaceWith(input.val('').clone(true));
}

function btnshowAttraction() {
    $('#divAttraction').show();
    $('#divAttrashow').show();
    $('#divAttraSave').hide();
    $('#btnAttrashow').hide();
    $('#btnAttraAdd').show();
    $('input#txtAttraction').val('');
    $('input#txtAttrRating').val(''); $('input#txtAttrRating').val(''); $('input#txtAttrReview').val(''); $('#txtAttrAddress').val('');
    $('input#txtAttrcountry').val(''); $('#txtAttrDescription').val(''); $('input#txtAttrlatitude').val(''); $('input#txtAttrlongitude').val(''); $('input#txtAttrprice').val('');
    $('input#txtActurl').val(''); $('input#txtAttrType').val(''); $("#ddlAttrtype").val(0); $("#txtAttrcity").val('');
    $('#chkAttractive').prop('checked', false); $('input#txtlatitudeAttrac').val(''); $('input#txtlongitudeAttrac').val(''); $('input#txtAttrUrl').val('');
    var input = $("#fileAttrupload");
    input.replaceWith(input.val('').clone(true));
}

function SaveAttractionList() {
    var formData = new FormData();
    var totalFiles = document.getElementById("fileAttrupload").files.length;
    for (var i = 0; i < totalFiles; i++) {
        var file = document.getElementById("fileAttrupload").files[i];
        var Extension = file.type.split('/').pop();
        formData.append("fileActupload", file);
        if (Extension == "bmp" || Extension == "gif" || Extension == "png" || Extension == "jpg" || Extension == "jpeg") {
            $.ajax({
                cach: false,
                async: false,
                type: "POST",
                url: '../CMSPage/AttractionUpload',
                data: formData,
                dataType: 'json',
                contentType: false,
                processData: false,
                success: function (response) {
                },
                error: function (error) {
                }
            });

        }
        else {
            var notify_alert = noty({
                layout: 'topRight',
                text: 'Invalid File!..',
                type: 'warning',
                timeout: 3000,
                maxVisible: 1,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                }
            });
            return false;
        }
    }
    if ($.trim($("#txtAttraction").val()) == "") {
        $("#spnAttraction").text("Please Enter Attraction Name");
        return false;
    }
    if ($.trim($("#ddlAttrtype").val()) == "0") {
        $("#spnAttraction").text("Please Select Attraction Type");
        return false;
    }
    if ($.trim($("#txtAttrcity").val()) == "") {
        $("#spnAttraction").text("Please Enter City");
        return false;
    }
    if ($.trim($("#txtAttrlatitude").val()) == "") {
        $("#spnAttraction").text("Please Enter Latitude");
        return false;
    }
    if ($.trim($("#txtAttrlongitude").val()) == "") {
        $("#spnAttraction").text("Please Enter  Longitude");
        return false;
    }
    if (document.getElementById("fileAttrupload").files.length == 0) {
        $("#spnAttraction").text("Please Add Activity Image");
        return false;
    }
    var ManualEntry = {
        Name: $('[id$=txtAttraction]').val(),
        Hotel_rating: $('[id$=txtAttrRating]').val(),
        Type: $("#ddlAttrtype option:selected").text(),
        Description: $('[id$=txtAttrDescription]').val(),
        Address: $('[id$=txtAttrAddress]').val(),
        City: $('[id$=txtAttrcity]').val(),
        Latitude: $('[id$=txtAttrlatitude]').val(),
        Longitude: $('[id$=txtAttrlongitude]').val(),
        Hotellatitude: $('[id$=txtlatitudeAttrac]').val(),
        Hotellongitude: $('[id$=txtlongitudeAttrac]').val(),
        Imagepath: $("#fileAttrupload").val(),
        Price: $('[id$=txtAttrprice]').val(),
        URL: $('[id$=txtAttrUrl').val(),
        Isactive: $("#chkAttractive").prop('checked')
    };
    $.ajax({
        url: '../CMSpage/SaveAttraction',
        type: 'POST',
    async: false,
    data: {
        data: ManualEntry
    },
    success: function (data) {
        var notify_alert = noty({
            layout: 'topRight',
            text: 'Saved Successfully.',
            type: 'success',
            timeout: 3000,
            maxVisible: 1,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            }
        });        
        setTimeout(function () {
            window.location.reload(1);
        }, 1000);
    },
    error: function (result) {
        var notify_alert = noty({
            layout: 'topRight',
            text: 'Failed to save the record',
            type: 'warning',
            timeout: 3000,
            maxVisible: 1,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            }
        });
        
    }
});
$('input#txtAttraction').val('');
$('input#txtAttrRating').val(''); $('input#txtAttrRating').val(''); $('input#txtAttrReview').val(''); $('#txtAttrAddress').val('');
$('input#txtAttrcountry').val(''); $('#txtAttrDescription').val(''); $('input#txtAttrlatitude').val(''); $('input#txtAttrlongitude').val(''); $('input#txtAttrprice').val('');
$('input#txtActurl').val(''); $('input#txtAttrType').val(''); $("#ddlAttrtype").val(0); $("#txtAttrcity").val('');
$('#chkAttractive').prop('checked', false); $('input#txtlatitudeAttrac').val(''); $('input#txtlongitudeAttrac').val(''); $('input#txtAttrUrl').val('');
var input = $("#fileAttrupload");
input.replaceWith(input.val('').clone(true));

}

/***********Edit Attraction**********/
function EditAttraction(RowId) {
    //debugger;

    var AttraId = $("#tblAttraheading").jqxGrid('getrowdata', RowId).AttractionId;
    $.getJSON('../CMSPage/GetAttractionId', { UserName: AttraId }, function (data) {
        $.each(data, function (i, d) {          
            var AttraName =d.Name ;
            var AttraRating = d.Rating;
            var AttraType =d.AttractionType ;
            var Description = d.Description;
            var Address =d.Address ;
            var City =d.City ;
            var Latitude = d.City_Latitude;
            var Longitude =d.City_Longitude ;
            var Attract_Latitude = d.Attraction_Latitude;
            var Attract_Longitude = d.Attraction_Longitude;
            var Image = "../Content/images/AttractionImage/" + d.Image_Name;
            var Imagename = Image.slice(34, 120);
            var Price =d.Price ;
            var Url =d.Reference_Url ;
            var Isactive = d.ISActive;
            $('[id$=hidAttract]').val(AttraId);
            $('[id$=txtAttraction]').val(AttraName);
            $('[id$=txtAttrRating]').val(AttraRating);
            var type = document.getElementById("ddlAttrtype");
            for (var i = 0; i < type.options.length; i++) {
                if (type.options[i].text == AttraType) {
                    type.selectedIndex = i;
                    break;
                }
            }
            $('[id$=txtAttrAddress]').val(Address);
            $('[id$=txtAttrDescription]').val(Description);
            $('[id$=txtAttrcity]').val(City);
            $('[id$=txtAttrlatitude]').val(Latitude);
            $('[id$=txtAttrlongitude]').val(Longitude);
            $('[id$=txtlatitudeAttrac]').val(Attract_Latitude);
            $('[id$=txtlongitudeAttrac]').val(Attract_Longitude);
            $("#imgAttraction").attr("src", Image)
            $('[id$=lblAttract]').val(Imagename).text(Imagename);
            $('[id$=txtAttrprice]').val(Price);
            $('[id$=txtAttrUrl]').val(Url);
            if (Isactive == "true") {

                $('#chkAttractive').prop('checked', Isactive);
            }
            else {
                $('#chkAttractive').prop('checked', Isactive);
            }
            $('#btnAttrUpdate').show();
            $('#btnAttrsave').hide();
            $('#divAttraction').show();
            $('#divAttrashow').hide();
            $('#divAttraSave').show();
            $('#btnAttrashow').show();
            $('#btnAttraAdd').hide();
        });
    });
    
}
/***********End Edit Attraction**********/

/************Update Attraction***********/
function UpdateAttraction() {
    var formData = new FormData();
    var totalFiles = document.getElementById("fileAttrupload").files.length;
    for (var i = 0; i < totalFiles; i++) {
        var file = document.getElementById("fileAttrupload").files[i];
        formData.append("fileAttrupload", file);
    }
    $.ajax({
        cach: false,
        async: false,
        type: "POST",
        url: '../CMSPage/AttractionUpload',
        data: formData,
        dataType: 'json',
        contentType: false,
        processData: false,
        success: function (response) {
        },
        error: function (error) {
        }
    });
    if ($.trim($("#txtAttraction").val()) == "") {
        $("#spnAttraction").text("Please Enter Attraction Name");
        return false;
    }
    if ($.trim($("#ddlAttrtype").val()) == "0") {
        $("#spnAttraction").text("Please Select Attraction Type");
        return false;
    }
    if ($.trim($("#txtAttrcity").val()) == " ") {
        $("#spnAttraction").text("Please Select City");
        return false;
    }
    if ($.trim($("#txtAttrlatitude").val()) == "") {
        $("#spnAttraction").text("Please Enter Latitude");
        return false;
    }
    if ($.trim($("#txtAttrlongitude").val()) == "") {
        $("#spnAttraction").text("Please Enter  Longitude");
        return false;
    }
    var ManualEntry = {
        Id: $("#hidAttract").val(),
        Name: $('[id$=txtAttraction]').val(),
        Hotel_rating: $('[id$=txtAttrRating]').val(),
        Type: $("#ddlAttrtype option:selected").text(),
        Description: $('[id$=txtAttrDescription]').val(),
        Address: $('[id$=txtAttrAddress]').val(),
        City: $('[id$=txtAttrcity]').val(),
        Latitude: $('[id$=txtAttrlatitude]').val(),
        Longitude: $("#txtAttrlongitude").val(),
        Hotellatitude: $('[id$=txtlatitudeAttrac]').val(),
        Hotellongitude: $('[id$=txtlongitudeAttrac]').val(),
        Imagepath: $("#lblAttract").val(),
        Price: $("#txtAttrprice").val(),
        URL: $('[id$=txtAttrUrl]').val(),
        Isactive: $("#chkAttractive").prop('checked')
    };
    $.ajax({
        url: '../CMSpage/UpdateAttraction',
        type: 'POST',
    async: false,
    data: {
        data: ManualEntry
    },
    success: function (data) {

        var notify_alert = noty({
            layout: 'topRight',
            text: 'Record Updated Successfully......!',
            type: 'success',
            timeout: 3000,
            maxVisible: 1,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            }
        });
        setTimeout(function () {
            window.location.reload(1);
        }, 1000);
    },
    error: function (result) {
        var notify_alert = noty({
            layout: 'topRight',
            text: 'Failed to update the record..',
            type: 'warning',
            timeout: 3000,
            maxVisible: 1,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            }
        });
      
    }
});
$('input#txtAttraction').val('');
$('input#txtAttrRating').val(''); $('input#txtAttrRating').val(''); $('input#txtAttrReview').val(''); $('#txtAttrAddress').val('');
$('input#txtAttrcountry').val(''); $('#txtAttrDescription').val(''); $('input#txtAttrlatitude').val(''); $('input#txtAttrprice').val('');
$('input#txtActurl').val(''); $('input#txtAttrType').val(''); $("#ddlAttrtype").val(0); $("#txtAttrcity").val(''); $('input#txtAttrlongitude').val('');
$('#chkAttractive').prop('checked', false); $('input#txtlatitudeAttrac').val(''); $('input#txtlongitudeAttrac').val(''); $('input#txtAttrUrl').val('');
var input = $("#fileAttrupload"); $('input#lblAttract').val(''); $("#imgAttraction").attr("src", "");
input.replaceWith(input.val('').clone(true));
}

/**********End Update Attraction*****************/

/*****************Delete Attraction Details*****************/
function DeleteAttractionDetails(RowId) {
    noty({
        layout: 'topRight',
        type: 'confirm',
        maxVisible: 1,
        modal: true,
        text: 'Are you sure you want to delete the selected attraction?',
        buttons: [
            {
                addClass: 'btn btn-primary', text: 'Ok', onClick: function ($noty) {
                    $noty.close();
                    var Hotel_id = $("#tblAttraheading").jqxGrid('getrowdata', RowId).AttractionId;
                    $.ajax({
                        url: '../CMSpage/DeleteAttraction',
                        type: 'POST',
                        async: false,
                        data: {
                            Id: Hotel_id
                        },
                        success: function (data) {
                            var notify_alert = noty({
                                layout: 'topRight',
                                text: 'Record deleted successfully..!',
                                type: 'success',
                                timeout: 3000,
                                maxVisible: 1,
                                animation: {
                                    open: { height: 'toggle' },
                                    close: { height: 'toggle' },
                                    easing: 'swing',
                                    speed: 500
                                }
                            });
                            //window.location.reload();
                            $('#List').trigger('change');
                            GetHotelDetails();
                        },
                        error: function (result) {
                            var notify_alert = noty({
                                layout: 'topRight',
                                text: 'Oops! Something went wrong, please try again some other time',
                                type: 'warning',
                                timeout: 3000,
                                maxVisible: 1,
                                animation: {
                                    open: { height: 'toggle' },
                                    close: { height: 'toggle' },
                                    easing: 'swing',
                                    speed: 500
                                }
                            });
                        }
                    });
                }
            },
            {
                addClass: 'btn btn-danger', text: 'Cancel', onClick: function ($noty) {
                    $noty.close();
                    return false;
                }
            }
        ]
    });
}
/*****************End delete attraction*****************/

/*****************Save Attraction type Dropdown value*****************/
function AttractionType() {
    if ($.trim($("#txtAttrtype").val()) == "") {
        $("#spnAttraction").text("Please Enter Attraction type");
        return false;
    };
    var ManualEntry = {
        Type: $("#txtAttrtype").val(),
        Isactive: true
    };
    $.ajax({
        url: '../CMSpage/SaveAttrType',
        type: 'POST',
    async: false,
    data: {
        data: ManualEntry
    },
    success: function (data) {
        var notify_alert = noty({
            layout: 'topRight',
            text: 'Saved successfully.',
            type: 'success',
            timeout: 3000,
            maxVisible: 1,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            }
        });
        AttractType();
    },
    error: function (result) {
        var notify_alert = noty({
            layout: 'topRight',
            text: 'Oops! Something went wrong, please try again some other time',
            type: 'warning',
            timeout: 3000,
            maxVisible: 1,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            }
        });
    }
});
$("#spnAttraction").text('');
$("#txtAttrtype").val('');
}

function notyDeleteAttractioAlert() {
    var attractionType = $("#ddlAttrtype");
    var attractionTypeIndex = $("#ddlAttrtype option:selected").index();
    var attractionTypeValue = $("#ddlAttrtype option:selected").val();
    if (attractionTypeValue == 0) {
        var notify_alert = noty({
            layout: 'topRight',
            text: 'Please select Attraction Type',
            type: 'warning',
            timeout: 3000,
            maxVisible: 1,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            }
        });
    } else {
        noty({
            layout: 'topRight',
            type: 'confirm',
            text: 'Are you confirm to delete the Attraction?',
            buttons: [
                {
                    addClass: 'btn btn-primary', text: 'Yes', onClick: function ($noty) {
                        DeleteAttrType();
                        $noty.close();
                    }
                },
                {
                    addClass: 'btn btn-danger', text: 'No', onClick: function ($noty) {
                        $noty.close();
                    }
                }
            ]
        });
    }
}

function DDLAttrType() {
    $.getJSON('../CMSPage/AttrTypeDropdown', {}, function (data) {
        var item = '';
        $("#ddlAttrtype").append($("<option></option>").val("0").html("---Attraction Type---"));
        $.each(data, function (i, item) {
            $("#ddlAttrtype").append($("<option></option>").val(item.AttrTypeID).html(item.Attraction_Type));
        });
    });
}

function DeleteAttrType() {
    var attractionType = $("#ddlAttrtype");
    var attractionTypeIndex = $("#ddlAttrtype option:selected").index();
    var attractionTypeValue = $("#ddlAttrtype option:selected").val();
    $.ajax({
        url: "../CMSPage/DeleteAttractionType",
        type: "POST",
        async: false,
        data: JSON.stringify({ AttractionTypeID: attractionTypeValue }),
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        success: function (index, data) {
            AttractType();
            $("#btnAttrtypeDelete").hide();
            var notify_alert = noty({
                layout: 'topRight',
                text: 'Attraction type deleted successfully',
                type: 'success',
                timeout: 3000,
                maxVisible: 1,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                }
            });
        },
        error: function (error) {
        }
    });
}

function SelectedIndexChange() {
    var restaurentType = $("#ddlAttrtype");
    var restaurentTypeIndex = $("#ddlAttrtype option:selected").index();
    var restaurentTypeValue = $("#ddlAttrtype option:selected").val();
    if (restaurentTypeIndex > 0) {
        $("#btnAttrtypeDelete").show();
    }
}